sed "s|'http://localhost:8088'|'http://3.141.239.34:8088/msp-app'|" ./msp-app/grails-app/assets/javascripts/application-vue.js > ./msp-app/grails-app/assets/javascripts/application-vue.prod.js

mv ./msp-app/grails-app/assets/javascripts/application-vue.prod.js ./msp-app/grails-app/assets/javascripts/application-vue.js

chmod u+x ./gradlew

./gradlew -D"grails.env"=dev assemble

mv ./msp-app/build/libs/msp-app.war.original /opt/tomcat/webapps/msp-app.war -f

git reset --hard