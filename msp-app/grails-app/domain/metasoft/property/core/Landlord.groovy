package metasoft.property.core

import grails.rest.Linkable

@Linkable
class Landlord extends Account {

    static hasMany = [holdings: Property
                      ,standingOrders: StandingOrder
                      ,contactPersons: ContactPerson
                      ,lessorPayments: LessorPayment]

    static mapping = {
        discriminator  value: 'landlord'
        holdings sort: 'id', order: 'asc'
    }

    /**
     * Returns lists of Approved accounts and also active accounts
     *
     * @return
     */
    public static List<Account> approvedList() {
        def query = where {
            checkStatus == CheckStatus.APPROVED;
        }

        return query.list();
    }

}

class LandlordAccount {

    Date dateCreated;

}
