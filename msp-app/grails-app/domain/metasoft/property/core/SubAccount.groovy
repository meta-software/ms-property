package metasoft.property.core

class SubAccount {

    String accountName;
    String accountNumber;
    String suspenseAccount;
    String description;
    String subAccountType;  //
    String incomeExpenditure;
    boolean hasCommission;
    boolean hasControl;
    boolean hasVat;
    boolean hasLedger;
    boolean suspense = false;

    Date dateCreated;
    Date lastUpdated;

    static transients = ['displayName']

    static constraints = {
        accountNumber unique: true
        accountName unique: true
        description nullable: true
        subAccountType maxSize: 1
        incomeExpenditure nullable: true
    }

    static mapping = {
        cache true
    }

    @Override
    String toString() {
        return accountName;
    }

    String getDisplayName() {
        return "${accountNumber}:${accountName}"
    }

    static List<SubAccount> listOptions() {
        SubAccount.where {
            suspense == false
        }.list([sort: 'accountumber', order: 'asc']);
    }
}
