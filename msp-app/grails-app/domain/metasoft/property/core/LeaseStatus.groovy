package metasoft.property.core

class LeaseStatus {

    String code;
    String name;
    Integer position = 0;

    static constraints = {
        code unique: true
        name unique: true
    }

    static mapping = {
        sort "position"
        cache true
    }

    @Override
    String toString() {
        return name
    }
}
