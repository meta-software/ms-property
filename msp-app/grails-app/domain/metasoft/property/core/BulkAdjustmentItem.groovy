package metasoft.property.core

class BulkAdjustmentItem {

    BulkAdjust bulkAdjustment;
    LeaseItem leaseItem;
    BigDecimal previousAmount;
    BigDecimal newAmount;

    Date dateCreated;

    static belongsTo = [bulkAdjustment: BulkAdjust]

    static constraints = {
        previousAmount shared: "numberConstraint"
        newAmount shared: "numberConstraint"
    }
}
