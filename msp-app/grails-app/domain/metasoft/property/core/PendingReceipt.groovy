package metasoft.property.core

import metasoft.property.core.Receipt.ReceiptStatus;

class PendingReceipt {

    String transactionReference;
    String narration;
    Date transactionDate;
    Tenant tenant;
    BigDecimal receiptAmount;
    ReceiptStatus receiptStatus = ReceiptStatus.OK;
    Date statusChangeDate = new Date();
    GeneralLedger debitAccount;

    Date dateCreated;
    Date lastUpdated;

    //BigDecimal amountAllocated = 0.0;
    Boolean allocated = false;
    Date dateAllocated;
    String allocatedBy;

    Currency transactionCurrency
    
    static hasMany = [receiptAllocations: PendingReceiptAllocation]

    static transients = ['amountAllocated']

    static constraints = {
        dateAllocated nullable: true
        allocatedBy nullable: true
        transactionReference unique: true
        //amountAllocated min: 0.0
    }

    static mapping = {
        transactionReference index: "pending_receipt_txn_ref_idx"
        debitAccount index: "pending_receipt_db_acct_idx"
        receiptStatus index: "pending_receipt_acct_idx"
        transactionDate sqlType: 'date', type: 'date'
    }

    BigDecimal getAmountAllocated() {
        if(this.id) {
            String hql = "select sum(allocatedAmount) as allocatedAmount from PendingReceiptAllocation a where a.pendingReceipt.id=:id"
            def result = PendingReceiptAllocation.executeQuery(hql, [id: this.id]);
            return result[0] ?: 0.0;
        }
        return 0.0;
    }

}
