package metasoft.property.core

class RecordStatus {

    String code;
    String name;
    Integer position = 0;

    static constraints = {
        code unique: true
        name unique: true
    }

    @Override
    String toString() {
        return name
    }
}
