package metasoft.property.core

class GlBankAccount {

    Bank bank;
    String accountName;
    String accountNumber;
    String branch;
    String branchCode;
    Currency currency

    Boolean active = true;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [generalLedger: GeneralLedger]

    static constraints = {
        generalLedger unique: true
        branch nullable: true
        currency nullable: true
    }

    @Override
    String toString() {
        return accountNumber
    }
}
