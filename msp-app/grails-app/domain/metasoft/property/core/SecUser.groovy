package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.plugins.orm.auditable.Auditable
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import org.springframework.validation.Errors

//t@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class SecUser implements Serializable, MakerCheckable<SecUser>, Loggable<SecUser>  {

    private static final long serialVersionUID = 1

    String username
    String password
    String firstName
    String lastName
    String fullName
    String emailAddress
    String employeeId

    boolean enabled = true
    boolean accountExpired = false
    boolean accountLocked = false
    boolean passwordExpired = false

    boolean initialLogin = true;

    String lockReason;
    Long loginAttemptCount = 0;
    Date passwordUpdateDate = new Date();

    SecRole userType;   //sort of the main user role
    Date lastLoginDate;
    Date dateCreated;
    Date lastUpdated;

    Set<SecRole> getAuthorities() {
        (SecUserSecRole.findAllBySecUser(this) as List<SecUserSecRole>)*.secRole as Set<SecRole>
    }

    // maker checker attributes
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    SecUserCheck checkEntity;

    static constraints = {

        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true
        checkComment nullable: true

        checkEntity(nullable: true)
        username nullable: false, blank: false, unique: true, minSize: 3, validator: { String val, SecUser obj, Errors errors ->
            def disallowedUsernames = ['sys', 'metasoft']

            if( disallowedUsernames.contains( val?.toLowerCase()) ) {
                Object[] args = new Object[1];
                args[0] = 0;
                errors.rejectValue('username', 'user.username.disallowed', args, 'The provided username is disallowed')
            }

        }
        password nullable: false, blank: false, password: true, minSize: 8, validator: { String value, SecUser object, Errors errors ->
            // check that the password conforms to the client's password policy
            if ( ! (value =~ /[\$`~&+,:;=?@#|'<>.^*()%!_-]/ && value =~ /[A-Za-z0-9]/ ) ) {
               Object[] errorArgs = [value]
               errors.rejectValue('password', 'user.password.specialChar.invalid', errorArgs,'Invalid: Password does not meet the password policy requirements')
            }

        }
        fullName nullable: true
        emailAddress nullable: true, email: true, unique: true
        employeeId nullable: true, unique: true
        lastLoginDate nullable: true
        loginAttemptCount nullable: true
        lockReason nullable: true
    }

    static mapping = {
	    password column: '`password`'
        sort "firstName"
    }

    static transients = ['shortName', 'fullName']

    String getShortName() {
        return "${firstName[0]}. ${lastName}";
    }

    String getFullName() {
        return "${firstName} ${lastName}";
    }

    def beforeInsert() {
        this.passwordUpdateDate = new Date();
    }

    def beforeValidate() {
        this.fullName = "${firstName} ${lastName}";
    }

    @Override
    String toString() {
        return username;
    }
}
