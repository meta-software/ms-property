package metasoft.property.core

class ReceiptBatch implements MakerCheckable<ReceiptBatch> {

    static enum ReceiptBatchType {
        AUTO, AD_HOC
    }

    ReceiptBatchType receiptBatchType = ReceiptBatchType.AD_HOC;
    TransactionType transactionType;
    CheckStatus checkStatus;
    String batchNumber;
    boolean autoCommitted = false;

    //MakerChecker fields;
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    Date dateCreated;
    Date lastUpdated;

    static hasMany = [receipts: Receipt];

    static transients = ['transactionsCount'];

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable:true
        checkDate nullable: true
        checkComment nullable: true
        batchNumber nullable: true
    }

    static mapping = {
        batchNumber index: "rcpt_batch_nbr_idx"
    }

    Long getTransactionsCount() {
        return ((Long)Receipt.countByReceiptBatch(this));
    }
}
