package metasoft.property.core

class Receipt {

    static enum ReceiptStatus {
        OK, CANCELLED, REFUNDED
    }

    String transactionReference;
    String narrative;
    Date transactionDate;
    Tenant tenant;
    ReceiptStatus receiptStatus = ReceiptStatus.OK;
    Date statusChangeDate = new Date();
    GeneralLedger debitAccount;

    Date dateCreated;
    Date lastUpdated;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal receiptAmount;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    static belongsTo = [receiptBatch: ReceiptBatch]
    static hasMany = [receiptItems: ReceiptItem]

    static constraints = {
        transactionReference unique: true
    }

    static mapping = {
        transactionReference index: "rct_txn_ref_idx"
        debitAccount index: "rcpt_db_acct_idx"
        receiptStatus index: "rcpt_db_acct_idx"
        transactionDate sqlType: 'date', type: 'date'
    }

}
