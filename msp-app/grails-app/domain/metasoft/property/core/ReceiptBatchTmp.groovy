package metasoft.property.core

class ReceiptBatchTmp implements Loggable<ReceiptBatchTmp> {

    ReceiptBatch.ReceiptBatchType receiptBatchType = ReceiptBatch.ReceiptBatchType.AD_HOC;
    TransactionType transactionType;
    CheckStatus checkStatus;
    String batchNumber;
    Boolean autoCommitted = false;
    Boolean posted = false;
    String postedBy;
    Date datePosted;
    Boolean postRequest = false;
    String postRequestBy;
    Date datePostRequest;
    Long batchCount = 0;

    String createdBy;
    Date dateCreated;
    Date lastUpdated;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal batchTotal = 0.0;

    static hasMany = [receiptTmps: ReceiptTmp];

    static fetchMode = [receiptTmps: 'lazy']

    static constraints = {
        createdBy nullable: true
        postedBy nullable: true
        datePosted nullable: true

        postRequestBy nullable: true
        datePostRequest nullable: true
        batchNumber nullable: true
        transactionCurrency nullable: true
    }

    static mapping = {
        receiptTmps cascade: 'all'
    }

    def afterInsert() {

        withTransaction {
            this.batchNumber = this.id as String;
            this.save();
        }

    }

}
