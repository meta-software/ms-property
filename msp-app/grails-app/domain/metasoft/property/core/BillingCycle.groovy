package metasoft.property.core

import org.springframework.validation.Errors

class BillingCycle implements MakerCheckeable, Loggable<BillingCycle>  {

    String name;
    String shortDescription;
    Boolean active = false;
    Boolean open = false;
    Date startDate;
    Date endDate;

    Date dateCreated;
    Date lastUpdated;

    String openedBy;
    Date dateOpened;

    String activatedBy;
    //Date dateActivated;

    String closedBy;
    //Date dateClosed;

    String deactivatedBy;
    //Date dateDeactivated;

    MakerChecker makerChecker
    static embedded = ['makerChecker'];
    
    //static belongsTo = [billingYear: BillingYear]

    static transients = ['billingCycleName']

    static constraints = {
        name unique: true
        shortDescription nullable: true
        openedBy nullable: true
        dateOpened nullable: true
        activatedBy nullable: true
        closedBy nullable: true
        deactivatedBy nullable: true
        makerChecker nullable: true
        endDate validator: { Date value, BillingCycle object, Errors errors ->
            if ((value && object.startDate) && value <= object.startDate) {
                Object[] args = (Object[])[value, object.startDate];
                errors.rejectValue("endDate","billingCycle.endDate.afterStart", args, "End Date should be after the Start Date value")
                return false; //the date parameters checks are wrong
            }

            return true;
        }
    }

    static mapping = {
        cache true
        sort "startDate"
    }

    Boolean isInCycle(Date queryDate) {
        if(queryDate) {
            return (this.startDate <= queryDate && queryDate <= this.endDate) ;
        }

        return false;
    }

    List<BillingCycle> open() {
        return findAllByOpen(true, [sort: "name", order: "asc"]);
    }

    static BillingCycle getActiveCycle() {
        return BillingCycle.findByActive(true);
    }

    static boolean isInOpenCycle(Date queryDate) {

        Long openCycles = BillingCycle.where {
            (startDate <= queryDate && endDate >= queryDate)
            open == true
        }.count();

        return (openCycles > 0);
    }

    String getBillingCycleName() {
        return name.substring(0, 4) + "/" + name.substring(4, 6);
    }

    def beforeInsert() {
        if(!this.shortDescription) {
            Date cycleDate = new Date().parse('yyyyMM', this.name);
            this.shortDescription = cycleDate.format("MMMM yyyy");
        }
    }

    String getShortDescription() {
        return this.shortDescription ?: this.billingCycleName;
    }

    @Override
    String toString() {
        return name;
    }
}
