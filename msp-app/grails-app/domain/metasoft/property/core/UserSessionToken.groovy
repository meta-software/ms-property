package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class UserSessionToken {

    String token;
    SecUser secUser;
    boolean used;
    Date dateCreated;

    static constraints = {
    }

    static mapping = {
        cache true
        token index: 'user_sess_token_idx'
    }

}
