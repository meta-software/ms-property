package metasoft.property.core

class AccountType {

    String code;
    String name;
    String description;
    Boolean active = true;

    static constraints = {
        code unique: true
        name unique: true
        description nullable: true
    }

    static mapping = {
        code column: '`code`'
        name column: '`name`'
    }

    List<AccountType> listActive() {
        return AccountType.where{
            active == true
        }.list()
    }

    @Override
    String toString() {
        return name;
    }

}
