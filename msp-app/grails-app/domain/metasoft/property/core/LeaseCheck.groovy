package metasoft.property.core

import org.springframework.validation.Errors

class LeaseCheck implements MakerCheck<LeaseCheck> {

    Lease entity;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        checker nullable: true, validator: { SecUser value, LeaseCheck object, Errors errors ->
            if(value && object.maker?.id == value?.id && object.checkStatus != CheckStatus.CANCELLED) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        checkDate nullable: true
        checkComment nullable: true
        jsonData maxSize: 8192
    }

    static mapping = {
    }

    @Override
    String toString() {
        return entity.toString();
    }
}
