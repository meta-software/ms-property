package metasoft.property.core

import grails.compiler.GrailsCompileStatic

//@GrailsCompileStatic
class ConfigSetting {

    ConfigType configType
    String value;
    Date startDate;
    Date endDate;
    boolean enabled = true;
    boolean currentActive = false;

    static constraints = {
        endDate nullable: true, validator: { val, obj, errors ->
            if(val != null && val.before(obj.startDate) ) {
                errors.reject("config.setting.endDate.beforeStart", "End Date cannot be before Start Date");
            }
            // the startDate should be after the endDate
        }
    }

    static mapping = {
        cache true
        configType lazy: false
    }

    @Override
    String toString() {
        return value;
    }
}
