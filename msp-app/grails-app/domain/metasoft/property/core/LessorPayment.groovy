package metasoft.property.core

class LessorPayment {

    Bank paycode;
    String payee;
    String bankAccountNumber;
    BigDecimal amount;
    Boolean percentage = false;
    String description;

    Boolean active = true;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [landlord: Landlord]

    static constraints = {
        description nullable: true
        amount nullable: true, shared: "numberConstraint", min: 0.0
    }

    @Override
    String toString() {
        return "${paycode}-${bankAccountNumber}"
    }
}
