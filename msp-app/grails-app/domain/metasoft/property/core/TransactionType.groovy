package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class TransactionType {

    String name;
    String transactionTypeCode;
    String transactionCategory;

    static constraints = {
        name unique: true
        transactionTypeCode maxSize: 2, unique: true
        transactionCategory nullable: true
    }

    static mapping = {
        cache 'read-only'
    }

    @Override
    String toString() {
        return name;
    }
}
