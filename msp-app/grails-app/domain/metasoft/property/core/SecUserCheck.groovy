package metasoft.property.core

import org.springframework.validation.Errors

class SecUserCheck implements MakerCheck<SecUserCheck> {

    SecUser entity;
    String username;

    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    static constraints = {
        checkDate nullable: true
        checker nullable: true, validator: { SecUser value, SecUserCheck object, Errors errors ->
            if(object.checkStatus != CheckStatus.CANCELLED && value && (object.maker?.id == value?.id) ) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        checkComment nullable: true
        jsonData maxSize: 4096
    }

    static mapping = {

    }

    @Override
    String toString() {
        return entity.toString();
    }

    def beforeUpdate() {
        this.beforeSave();
    }

    def beforeInsert() {
        this.beforeSave();
    }

    /**
     * Actions which are actioned before either a record insert/update
     *
     */
    def beforeSave() {
        this.entity.checkEntity = this;
    }
}
