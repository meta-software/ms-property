package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class PropertyTmp implements Loggable<PropertyTmp>  {

    String name;
    Address address;
    String description;
    BigDecimal area;
    BigDecimal commission;
    boolean chargeVat = false;
    boolean fullyConfigured = false;

    PropertyType propertyType;
    PropertyManager propertyManager;

    Date mandateDate;
    Date lastInspectionDate;
    Date terminatedDate;
    boolean terminated = false;

    String propertyCode
    String notes;
    String standNumber;

    boolean deleted = false;
    Date deletedDate;

    Date dateCreated;
    Date lastUpdated;

    Property property;

    UserAction userAction;

    boolean posted = false;
    String postedBy;
    Date datePosted;

    boolean postRequest = false;
    String postRequestBy;
    Date datePostRequest;

    String createdBy;

    static embedded = ['address']

    static hasMany  = [rentalUnits: RentalUnitTmp
                      ,propertyInsurances: PropertyInsurance]

    static belongsTo = [landlord: Landlord]

    static constraints = {

        datePostRequest nullable: true
        postRequestBy nullable: true

        postedBy nullable: true
        datePosted nullable: true

        userAction nullable: true
        property nullable: true /*can only be null when a new property is being created.*/
        area min: 0.0
        deletedDate nullable: true
        commission min: 0.0, max: 100.0
        terminatedDate nullable: true
        lastInspectionDate nullable: true
        notes maxSize: 1024, nullable: true
        propertyCode nullable: true
        mandateDate nullable: true
    }

    static mapping = {
        terminated column:'is_terminated'
        //sort name: 'asc'
    }

    def beforeValidate() {
        //@todo: move this into services.
        if(propertyType.code == "commercial") {
            chargeVat = true;
        }
    }

    boolean isAreaValid() {
        BigDecimal rentalUnitsArea = RentalUnitTmp.totalUnitsArea(this.id);

        return rentalUnitsArea == area;
    }

    @Override
    String toString() {
        return name;
    }

}
