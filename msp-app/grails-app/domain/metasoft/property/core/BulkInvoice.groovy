package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.plugins.orm.auditable.Auditable
import org.springframework.validation.Errors

@GrailsCompileStatic
class BulkInvoice implements MakerCheckable<BulkInvoice>, Loggable<BulkInvoice> {

    Date transactionDate;
    Property property;
    String transactionReference;
    TransactionType transactionType;
    SubAccount subAccount;
    BillingCycle billingCycle;
    String narrative;
    boolean posted = false;

    Date datePosted;
    String postedBy;
    Date dateCreated;

    Date lastUpdated;

    Currency transactionCurrency;
    BigDecimal amount;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    //List<BulkInvoiceItem> bulkInvoiceItems;
    static hasMany = [bulkInvoiceItems: BulkInvoiceItem]

    //MakerChecker fields;
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    //
    String checkComment

    static constraints = {
        narrative nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable:true, validator: { SecUser value, BulkInvoice object, Errors errors ->
            if(object.maker?.id == value?.id) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        checkDate nullable: true
        checkComment nullable: true
        transactionReference nullable: true
        amount min: 0.0, shared: "numberConstraint"
        datePosted nullable: true
        postedBy nullable: true
        transactionDate validator: { Date val, BulkInvoice obj, Errors errors ->
            Date now = new Date();

            //if transaction date is in the future
            if(val.after(now)) {
                errors.rejectValue("transactionDate", 'transaction.invalid.date.infuture')
                return
            }
        }
    }

}
