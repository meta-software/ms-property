package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class JobSchedule implements Serializable {

	private static final long serialVersionUID = 1546L

	String jobName;
	JobType jobType;
	String triggerName;
	String cronExpression = "0 0 0 * * ? *";	//by default run daily once at 1200AM
	String startDelay = 0;
	Boolean active = false; //this maps with the status values [enabled, disabled]
	Date lastRunTime;
	Date nextRunTime;
	//String lastRunStatus;
	String jobDescription;

	static constraints = {
		jobDescription nullable: true
		lastRunTime nullable: true
		nextRunTime nullable: true
		jobType nullable: true
	}

	static mapping = {
		cache true
	}

	@Override
	String toString() {
		return this.jobName;
	}
}

class JobType {
	String name;
}