package metasoft.property.core

import org.springframework.validation.Errors

class BulkAdjust implements MakerCheckable<BulkAdjust> , Loggable<BulkAdjust> {

    SubAccount subAccount;
    AdjustmentType adjustmentType;
    AmountType amountType;
    Date actionDate;
    String reason;
    PropertyType propertyType;

    boolean processed;
    Date dateProcessed;

    boolean cancelled = false;
    Date dateCancelled;

    Date dateCreated;
    Date lastUpdated;

    BigDecimal amount;
    Currency transactionCurrency;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    // to add the maker checker fields here.

    static hasMany = [bulkAdjustmentItems: BulkAdjustmentItem]

    static transients = ['transactionDate']

    // maker checker attributes
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    static constraints = {

        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true
        checkComment nullable: true

        dateCancelled nullable: true
        dateProcessed nullable: true
        amount min: 0.0, shared: "numberConstraint" ,validator: { BigDecimal value, BulkAdjust object, Errors errors ->
            // we do not want idempotent adjustments
            if(value == 0.0) {
                errors.rejectValue('amount', 'bulkAdjust.amount.zero', 'Adjust amount should be greater than zero')
                return;
            }
        }
    }

    @Override
    String toString() {
        return id;
    }

    Date getTransactionDate() {
        return this.actionDate;
    }
}
