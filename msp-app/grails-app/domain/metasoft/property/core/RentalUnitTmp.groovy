package metasoft.property.core

import org.springframework.validation.Errors

class RentalUnitTmp implements Loggable<RentalUnitTmp> {

    String name;
    String unitReference;
    BigDecimal area;
    RentalUnitStatus unitStatus;
    PropertyTmp property;
    RentalUnit rentalUnit;

    boolean deleted = false;

    static belongsTo = [property: PropertyTmp]

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        rentalUnit nullable: true
        unitStatus nullable: true
        area min:0.0, validator : { BigDecimal value, RentalUnitTmp object, Errors errors ->
            //@todo: verify that the new RentalUnitTmp value area does not exceed what is specified in the PropertyTmp object.

            PropertyTmp propertyTmp = object.property;
            BigDecimal totalUnitsArea = RentalUnitTmp.totalUnitsArea(object.property.id)
            if(!object.id) {
                totalUnitsArea += value;
            }

            if(propertyTmp.area < totalUnitsArea) {
                Object[] args = (Object[])[totalUnitsArea, propertyTmp.area];
                errors.rejectValue("area", "rentalUnitTmp.area.exceed", args, "Total Units area now exceeds Property configured area")
                return false; //the date parameters checks are wrong
            }
        }
    }

    static mapping = {
        property fetch: 'join'
        sort "property"
    }

    @Override
    String toString() {
        return name;
    }

    static BigDecimal totalUnitsArea(Long id) {
        String hql = "SELECT SUM(r.area) from RentalUnitTmp r join r.property p where r.deleted=:deleted AND p.id = :propertyId";

        BigDecimal rentalUnitTotalArea = (BigDecimal)RentalUnitTmp.executeQuery(hql, [propertyId: id, deleted: false])[0] ?: 0.0;

        return rentalUnitTotalArea;
    }

}
