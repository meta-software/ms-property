package metasoft.property.core

class Branch {
    String branchName;
    String branchCode;
    Bank bank;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [bank: Bank]

    static constraints = {
        branchName unique: true
        branchCode unique: true
    }

    static mapping = {
        cache true
    }

    @Override
    String toString() {
        return branchName;
    }
}
