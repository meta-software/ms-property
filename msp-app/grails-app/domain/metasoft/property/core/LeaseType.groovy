package metasoft.property.core

class LeaseType {

    String name;
    Integer position = 0;

    static constraints = {
        name unique: true
    }

    @Override
    String toString() {
        return name
    }
}
