package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class RoutineType {

    String name;

    static constraints = {
        name unique: true
    }

    @Override
    String toString() {
        return name
    }
}
