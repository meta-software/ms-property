package metasoft.property.core

class GeneralLedgerType {

    String name;
    Integer startRange;
    Integer endRange;

    static constraints = {
        startRange min: 1, validator: { val, obj ->
            if( ! (val <= obj.endRange) ) {
                errors.rejectValue("startRange", "Invalid startRange..endRange values")
            }
        }
        name unique: true
        endRange min: 1
    }

    static mapping = {
        cache 'read-only'
        version false
    }

    @Override
    String toString() {
        return name;
    }
}
