package metasoft.property.core

class LeaseReview implements Loggable<LeaseReview> {

    static enum ReviewStatus {
        PENDING,
        IGNORED,
        PROCESSED
    }

    Lease lease;
    LeaseReviewType leaseReviewType;
    ReviewStatus reviewStatus;
    Date lastReviewDate;
    Date reviewDate;
    String reviewedBy;

    Date dateCreated;
    Date lastUpdated;
    String createdBy;
    String updatedBy;

    static constraints = {
        reviewDate nullable: true
        createdBy nullable: true
        updatedBy nullable: true
        reviewedBy nullable: true
    }

    static mapping = {
        cache true
        sort "dateCreated"
    }

}

class LeaseReviewType {
    String name;

    @Override
    String toString() {
        return name;
    }
}
