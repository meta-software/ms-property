package metasoft.property.core

class AccountTerminateRequest implements MakerCheckeable {

    Account account;
    String actionReason; // the reason why the status must be in this new status
    Date actionDate;

    boolean processed = false;  // whether this request was processed or not.
    String processStatus;
    String processResponse;
    Date dateProcessed;

    Date dateCreated;
    Date lastUpdated;

    MakerChecker makerChecker
    static embedded = ['makerChecker'];

    static belongsTo = [lease: Lease]

    static constraints = {
        makerChecker nullable: true
        actionReason maxSize: 1024
        dateProcessed nullable: true
        processStatus nullable: true, inList: ['success', 'error']
        processResponse nullable: true
        actionDate validator: { val, obj, errors ->
            Date today = new Date().clearTime();
            if(val < today) {
                errors.rejectValue('actionDate', 'default.date.invalid.past', 'Date value should not be in the past')
            }
        }
    }

    static mapping = { }
}
