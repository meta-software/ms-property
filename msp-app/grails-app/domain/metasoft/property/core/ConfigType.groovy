package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class ConfigType {

    String code;
    String name;
    String classType;
    String description;

    static constraints = {
        code unique: true
        name unique: true
        description nullable: true
    }

    static mapping = {
        code column: '`code`'
        name column: '`name`'
        cache true
    }

    @Override
    String toString() {
        return name;
    }

}
