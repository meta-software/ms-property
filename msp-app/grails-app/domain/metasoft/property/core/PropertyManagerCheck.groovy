package metasoft.property.core

class PropertyManagerCheck {

    PropertyManager entity;

    String actionName;
    String entityName;
    String entityUrl;
    String jsonData;

    CheckStatus checkStatus = CheckStatus.PENDING; // default to pending
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    static constraints = {
        checker nullable: true
        checkDate nullable: true
        checkComment nullable: true
        jsonData maxSize: 2048
    }

    static mapping = {
        version false
    }

}
