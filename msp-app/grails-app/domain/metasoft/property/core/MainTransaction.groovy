package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import org.springframework.validation.Errors

//@GrailsCompileStatic
class MainTransaction {

    Date transactionDate;
    String accountNumberCr;     //the credit account number
    String subAccountNumberCr;  //the credit sub-account-number
    String accountNumberDb;     //debit account number
    String subAccountNumberDb;  //debit sub-account-number
    BigDecimal credit = 0.0;          //credit amount
    BigDecimal debit  = 0.0;           //debit amount
    String transactionReference;
    String description;
    boolean parentTransaction = false;
    String batchNumber;
    Property property;          //the property being paid for
    RentalUnit rentalUnit;      //the rental unit being paid for
    Lease lease;                //the lease being affected

    BillingCycle billingCycle; //billing cycle within which this is applicable.

    BigDecimal vat = 0.0;   //VAT percentage
    BigDecimal vatAmount = 0.0;

    Date datePosted;
    String postedBy;

    boolean reconciled = true;
    Date dateReconciled;

    boolean cancelled = false;
    Date dateCancelled;

    boolean refunded = false;
    Date dateRefunded;

    Date dateCreated;
    Date lastUpdated;

    TransactionType transactionType;
    TransactionBatch transactionBatch;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal vatTransactionAmount = 0.0;
    BigDecimal creditTransactionAmount;
    BigDecimal debitTransactionAmount;

    Currency reportingCurrency;
    BigDecimal vatReportingAmount;
    BigDecimal creditReportingAmount;
    BigDecimal debitReportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal vatOperatingAmount;
    BigDecimal creditOperatingAmount;
    BigDecimal debitOperatingAmount;
    ExchangeRate operatingExchangeRate;

    static transients = ['totalAmount']

    static belongsTo = [transactionBatch: TransactionBatch]

    static constraints = {
        dateCancelled nullable: true
        dateRefunded nullable: true
        dateReconciled nullable: true
        transactionBatch nullable: true
        billingCycle nullable: true
        property nullable: true
        rentalUnit nullable: true
        lease nullable: true
        description nullable: true, maxSize: 128
        datePosted nullable: true
        credit min: 0.0, shared: "numberConstraint"
        debit min: 0.0, shared: "numberConstraint"
        transactionReference maxSize: 32
        transactionDate validator: { Date val, MainTransaction obj, Errors errors ->
            Date now = new Date();

            //if transaction date is in the future
            if(val.after(now)) {
                errors.rejectValue("transactionDate", 'transaction.invalid.date.infuture')
                return
            }
        }
        vat shared: "numberConstraint"
        vatAmount nullable: true, shared: "numberConstraint"
    }

    static mapping = {
        table 'ms_transaction'
        accountNumberCr index: "acct_nbr_cr_idx"
        subAccountNumberCr index: "sub_acct_nbr_cr_idx"
        accountNumberDb index: "acct_nbr_db_idx"
        transactionReference index: 'trans_ref_idx'
        transactionDate sqlType: 'date', index: 'trans_date_idx'
        sort "id"
    }

    void beforeValidate() {
        datePosted = new Date();
        batchNumber = transactionBatch?.batchNumber;
    }

    //@todo: to fix and normalise the operation
    void beforeInsert() {
        if(this.reconciled) {
            this.dateReconciled = new Date();
        }
    }

    void beforeUpdate() {
        if(this.reconciled && this.dateReconciled == null) {
            this.dateReconciled = new Date();
        }
    }

    BigDecimal getTransactionAmount() {
        return (credit > 0) ? credit : debit
    }

    BigDecimal getTotalAmount() {
        return (credit + debit)
    }

}

