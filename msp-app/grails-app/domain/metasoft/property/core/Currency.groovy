package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Currency {

    String id;
    /**
     * ISO 4217 currency code for this currency.
     */
    String currencyCode;

    /**
     * ISO 4217 numeric code for this currency.
     * Set from currency data tables.
     */
    String numericCode;
    String currencyName;
    String currencySign;
    String shortDescription;

    static constraints = {
        currencyCode unique: true
        numericCode  unique: true
        currencySign maxSize: 2
    }

    static mapping = {
        cache usage: 'read-only'
        id generator: 'assigned'
    }

    def beforeInsert() {
        this.id = this.currencyCode;
    }

    @Override
    String toString() {
        return shortDescription;
    }
}
