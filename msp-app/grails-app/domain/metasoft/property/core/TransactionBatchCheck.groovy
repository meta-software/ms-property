package metasoft.property.core

import org.springframework.validation.Errors

class TransactionBatchCheck implements MakerCheck<TransactionBatchCheck> {

    TransactionBatch entity;
    TransactionBatchTmp transactionSource;
    TransactionType transactionType;
    String batchReference;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        checker nullable: true, validator: { SecUser value, TransactionBatchCheck object, Errors errors ->
            if(value && object.maker?.id == value?.id && object.checkStatus != CheckStatus.CANCELLED) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        checkDate nullable: true
        checkComment nullable: true
        jsonData maxSize: 32

        entity nullable: true
    }

    @Override
    String toString() {
        return entity?.toString();
    }
}
