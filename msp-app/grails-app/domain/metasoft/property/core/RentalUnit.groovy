package metasoft.property.core

class RentalUnit implements  MakerCheckable<RentalUnit>, Loggable<RentalUnit> {

    String name;
    String unitReference;
    BigDecimal area;
    RentalUnitStatus unitStatus;
    Property property;
    boolean deleted = false;
    List<Lease> leases = [];

    Date statusDate;
    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [property: Property]
    static hasMany = [leases: Lease]
    static transients = ['activeLease', 'isOccupied', 'landlord']

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true
        statusDate nullable: true
        unitStatus nullable: true
        area min: 0.0, shared: "numberConstraint"
    }

    static mapping = {
        leases sort: 'id', order: 'asc', lazy: true
        property fetch: 'join'
        sort "property"
    }

    @Override
    String toString() {
        return name;
    }

    Lease getActiveLease() {
        Date now = new Date().clearTime();

        return Lease.where {
            rentalUnit == this
            expired == false
            terminated == false
            leaseStatus.code in ['active']
            checkStatus == CheckStatus.APPROVED
        }.get();
    }

    Lease getActiveLeaseByDate(Date transactionDate) {
        transactionDate = transactionDate.clearTime();

        return Lease.where {
            rentalUnit == this
            leaseExpiryDate >= transactionDate
            leaseStartDate <= transactionDate
            checkStatus == CheckStatus.APPROVED
        }.get();
    }

    Boolean getIsOccupied() {
        return (unitStatusId == 1);
    }

    Landlord getLandlord() {
        return this.property.landlord;
    }

    static def selectVacantList() {
        String today = new Date().format('yyyy-MM-dd HH:mm:ss');

        def hql = "select r.id as id, concat(p.name,' [ ', r.name, ' : ', r.unitReference, ' ]') as rentalUnitName" +
                " from RentalUnit r join r.property p " +
                " where r.checkStatus='APPROVED'" +
                " and p.checkStatus='APPROVED'"
                " and not exists (select l.rentalUnit.id from Lease l" +
                    " where r.id=l.rentalUnit.id" +
                    " and l.checkStatus = 'APPROVED'" +
                    " and '"+ today +"' between l.leaseStartDate and l.leaseExpiryDate)" ;

        def results = executeQuery(hql);
        List<Map> records = []
        results.each { rec ->
            def id = rec[0];
            def name = rec[1];

            records<< ['id': id, 'rentalUnitName': name ]
        }

        return records;
    }

}
