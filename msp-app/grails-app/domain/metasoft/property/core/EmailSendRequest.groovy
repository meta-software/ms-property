package metasoft.property.core

class EmailSendRequest {

    enum SendStatus {
        PENDING, SENT, FAILED
    }

    String recipient;
    String sender;
    String recipientCC;
    String subject;
    String message;
    boolean multipart = false;
    String attachmentFilename;
    String attachmentFile;
    SendStatus sendStatus;
    String sendResponse;

    Date dateCreated;
    Date dateSent;

    static constraints = {
        recipientCC nullable: true
        attachmentFilename nullable: true
        attachmentFile nullable: true
        sendResponse nullable: true, maxSize: 1024
        dateSent nullable: true
        sendStatus nullable: true
    }

    static mapping = {
        table "email_send_request"
        message type: 'text'
        sendStatus index: "send_status_idx"
    }

    def beforeUpdate() {
        if( isDirty("sendStatus") ){
            this.dateSent = new Date();
        }
    }

    def beforeInsert() {
        sendStatus = SendStatus.PENDING;
    }

}
