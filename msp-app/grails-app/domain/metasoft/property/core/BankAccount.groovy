package metasoft.property.core

class BankAccount implements Loggable<BankAccount> {

    Bank bank;
    String accountName;
    String accountNumber;
    String branch;
    String branchCode;

    Boolean active = true;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [account: Account]

    static constraints = {
        account unique: true
    }

    @Override
    String toString() {
        return accountNumber
    }
}
