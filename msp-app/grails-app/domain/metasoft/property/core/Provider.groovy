package metasoft.property.core

import grails.rest.Linkable

@Linkable
class Provider extends Account {

    static mapping = {
        discriminator  value: 'provider'
    }

}

class ProviderAccount {

    Date dateCreated;

}
