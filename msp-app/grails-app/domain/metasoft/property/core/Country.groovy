package metasoft.property.core

class Country {

    String countryName;
    String countryCode

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        countryName unique: true
        countryCode unique: true
    }

    static mapping = {
        cache true
    }

    @Override
    String toString() {
        return countryName;
    }
}
