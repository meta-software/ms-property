package metasoft.property.core

class LeaseStatusRequest {

    Lease lease;
    LeaseStatus leaseStatus;
    String reason; // the reason why the status must be in this new status

    //Approval status
    String approveStatus = 'pending'; // [pending, approved, rejected]
    String approveResponse;
    Date   dateApproved;
    String approvedBy;

    SecUser requestedBy;    //the person who initiated the request.

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [lease: Lease]

    static constraints = {
        leaseStatus()
        reason maxSize: 1024
        approveResponse maxSize: 1024, nullable: true
        approveStatus nullable: true
        dateApproved nullable: true
        approvedBy nullable: true
    }

    static mapping = { }

}
