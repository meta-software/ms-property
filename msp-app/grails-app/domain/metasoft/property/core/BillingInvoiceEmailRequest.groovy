package metasoft.property.core

class BillingInvoiceEmailRequest {

    enum ActionStatus {
        SENT, NOT_SENT
    }

    enum RequestMethod {
        AUTO, MANUAL
    }

    String emailAddress;
    ActionStatus actionStatus;
    String actionReason;
    BillingInvoice billingInvoice;
    RequestMethod requestMethod;

    Date dateCreated;

    static constraints = {
        emailAddress(maxSize: 128, nullable: true)
    }

    static mapping = {
        table "invoice_email_request"
        billingInvoice cascade: "none"
    }

}
