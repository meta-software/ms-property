package metasoft.property.core

class Notification {

    String entity;
    String urlParams;
    String message;
    String notificationType;
    Boolean isRead = false;

    SecUser readBy;
    Date readDate;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        notificationType nullable: true
        readBy nullable: true
        readDate nullable: true
    }

    static mapping = {
        table 'ms_notification'
        version false
    }

}
