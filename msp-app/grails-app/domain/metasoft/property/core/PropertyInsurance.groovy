package metasoft.property.core

class PropertyInsurance implements MakerCheckable<Property>, Loggable<PropertyInsurance>  {

    String insurerName;
    String insuranceNumber;
    Date renewalDate;
    BigDecimal sumAssured;
    Boolean active = false;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [property: Property]

    static constraints = {

        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        sumAssured min: 0.0
        //@todo: need to configure for duplicate insurers on a given record
    }

    static mapping = {
        sort "insurerName"
    }
}
