package metasoft.property.core

class PropertyType {

    String code;
    String name;
    String description;
    Boolean active = true;

    static constraints = {
        code unique: true
        name unique: true
        description nullable: true
    }

    static mapping = {
        code column: '`code`'
        name column: '`name`'
        cache true
    }

    @Override
    String toString() {
        return name;
    }


}
