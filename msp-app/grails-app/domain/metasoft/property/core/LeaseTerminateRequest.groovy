package metasoft.property.core

class LeaseTerminateRequest implements MakerCheckable<LeaseTerminateRequest>, Loggable<LeaseTerminateRequest> {

    Lease lease;
    String actionReason; // the reason why the status must be in this new status
    Date actionDate;

    boolean processed = false;  // whether this request was processed or not.
    String processStatus;
    String processResponse;
    Date dateProcessed;

    Date dateCreated;
    Date lastUpdated;

    static transients = ['loggable']
    static belongsTo = [lease: Lease]

    LeaseTerminateRequestTmp leaseTerminateRequestTmp;

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        actionReason maxSize: 1024
        dateProcessed nullable: true
        processStatus nullable: true, inList: ['success', 'error']
        processResponse nullable: true
        actionDate validator: { val, obj, errors ->
            Date today = new Date().clearTime();
            if(val < today) {
                // errors.rejectValue('actionDate', 'default.date.invalid.past', 'Date value should not be in the past')
            }
        }
    }

    static mapping = { }

}
