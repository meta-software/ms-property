package metasoft.property.core

class LoginAttempt {
    String actor;
    String attemptStatus
    String description
    String ipAddress;
    String channel;

    Date dateCreated;

    static constraints = {
        attemptStatus nullable: true
        description nullable: true
        ipAddress nullable: true
        channel nullable: true
    }

    static mapping = {
        cache false, readOnly: true
    }

    @Override
    String toString() {
        return "[actor: $actor]";
    }
}
