package metasoft.property.core

import org.springframework.validation.Errors
import metasoft.property.core.ReceiptBatch.ReceiptBatchType

class ReceiptBatchCheck implements MakerCheck<ReceiptBatchCheck> {

    ReceiptBatch entity;
    ReceiptBatchType receiptBatchType = ReceiptBatchType.AD_HOC;
    ReceiptBatchTmp transactionSource;
    TransactionType transactionType;
    String batchNumber;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        checker nullable: true, validator: { SecUser value, ReceiptBatchCheck object, Errors errors ->
            if(value && object.maker?.id == value?.id && object.checkStatus != CheckStatus.CANCELLED) {
                //errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        checkDate nullable: true
        checkComment nullable: true
        entity nullable: true
    }

    @Override
    String toString() {
        return entity?.toString();
    }
}
