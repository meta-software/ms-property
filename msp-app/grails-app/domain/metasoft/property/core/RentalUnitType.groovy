package metasoft.property.core

class RentalUnitType {

    String name;
    String description;
    Boolean active = true;

    static constraints = {
        name unique: true
        description nullable: true
    }

    static mapping = {
        name column: '`name`'
    }

    @Override
    String toString() {
        return name;
    }

}
