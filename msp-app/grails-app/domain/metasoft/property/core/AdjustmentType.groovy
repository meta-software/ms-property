package metasoft.property.core

class AdjustmentType {

    String code;
    String name;

    static constraints = {
        code unique: true
        name unique: true
    }

    static mapping = {
        code column: '`code`'
        name column: '`name`'
    }

    @Override
    String toString() {
        return name;
    }

}
