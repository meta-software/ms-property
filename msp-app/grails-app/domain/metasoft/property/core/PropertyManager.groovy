package metasoft.property.core

class PropertyManager {

    String firstName;
    String lastName;
    String phoneNumber;
    String emailAddress;
    String notes;
    Boolean active = true;
    Date appointmentDate;

    Date dateCreated;
    Date lastUpdated;

    //static hasMany = [properties: Property]

    static transients = ['fullName', 'properties']

    static constraints = {
        notes nullable: true
        emailAddress nullable: true, email: true
        phoneNumber nullable: true
        appointmentDate nullable: false
    }

    static mapping = {
        sort "firstName"
    }

    String getFullName() {
        return "${this.firstName} ${this.lastName}";
    }

    @Override
    String toString() {
        return getFullName();
    }

    List<Property> getBuildings() {
        Property.where{
            propertyManager == this
        }.list()
    }
}
