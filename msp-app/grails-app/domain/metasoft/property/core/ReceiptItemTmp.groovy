package metasoft.property.core

class ReceiptItemTmp implements Loggable<ReceiptItemTmp> {

    Lease lease;
    SubAccount subAccount;
    ReceiptTmp receiptTmp;

    Date dateCreated;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal allocatedAmount;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    static belongsTo = [receiptTmp: ReceiptTmp]

    static constraints = {
        allocatedAmount min: 0.0
    }

    static mapping = {
        version false
    }

}
