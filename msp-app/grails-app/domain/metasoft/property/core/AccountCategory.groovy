package metasoft.property.core

class AccountCategory {

    String code;
    String name;
    String description;
    String accountPrefix;

    static constraints = {
        code unique: true
        name unique: true
        description nullable: true
    }

    static mapping = {
        code column: '`code`'
        name column: '`name`'
    }

    @Override
    String toString() {
        return name;
    }


}
