package metasoft.property.core

class LeaseRenewalRequest implements MakerCheckable<LeaseRenewalRequest>, Loggable<LeaseRenewalRequest> {

    Lease lease;
    String actionReason; // the reason why the status must be in this new status
    Date leaseExpireDate; // the new date which the lease expire on

    boolean processed = false;  // whether this request was processed or not
    String processStatus;
    String processResponse;
    Date dateProcessed;

    Date dateCreated;
    Date lastUpdated;

    static transients = ['loggable']
    static belongsTo = [lease: Lease]

    LeaseRenewalRequestTmp leaseRenewalRequestTmp;

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        actionReason maxSize: 1024
        dateProcessed nullable: true
        processStatus nullable: true, inList: ['success', 'error']
        processResponse nullable: true
        leaseExpireDate validator: { val, obj, errors ->
            Date today = new Date().clearTime();
            if(val < today) {
                errors.rejectValue('leaseExpireDate', 'default.date.invalid.past', 'Date value should not be in the past')
            }
            if(val < obj.lease.leaseExpiryDate) {
                errors.rejectValue('leaseExpireDate', 'lease.renewal.expireDate.invalid', 'The new expire date should be after the current expire date')
            }
        }
    }

    static mapping = {
        leaseExpireDate type: 'date'
    }

}
