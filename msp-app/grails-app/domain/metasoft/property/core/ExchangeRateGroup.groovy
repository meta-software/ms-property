package metasoft.property.core

class ExchangeRateGroup implements MakerCheckable<ExchangeRateGroup> {

    Date startDate
    Date endDate

    Boolean active = false
    Date dateActivated

    Boolean deleted = false;
    Date dateDeleted;

    Date dateCreated;

    static hasMany = [exchangeRates: ExchangeRate];

    // maker checker attributes
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    static constraints = {

        // maker checker constraints
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true
        checkComment nullable: true

        dateDeleted nullable: true
        dateActivated nullable: true
    }

    static mapping = {
        startDate sqlType: 'date', index: 'exchange_rate_grp_start_date_idx'
        endDate sqlType: 'date', index: 'exchange_rate_grp_end_date_idx'
        exchangeRates cascade: 'all'
    }
}