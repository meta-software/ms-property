package metasoft.property.core

import groovy.time.TimeCategory
import groovy.time.TimeDuration
import org.springframework.validation.Errors

//@GrailsCompileStatic
class RoutineRequest implements MakerCheckable<RoutineRequest>, Loggable<RoutineRequest> {

    static enum RequestStatus {
        PENDING,
        RUNNING,
        SUCCESS,
        ERROR
    }

    RoutineType routineType;
    BillingCycle billingCycle;
    Date runDate;
    Date valueDate;
    boolean emailAlert = false;
    boolean smsAlert = false;

    boolean processed = false;
    String requestStatus = 'pending';

    boolean cancelled = false;
    SecUser cancelledBy;
    String reason;
    String narrative;

    TransactionBatch transactionBatch;

    SecUser requestedBy;
    Date startTime;
    Date endTime;
    Date dateCreated;
    Date lastUpdated;

    RoutineRequestTmp routineRequestTmp
    //this entity does not really need to know where it came from.

    static constraints = {

        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        reason nullable: true, maxSize: 1024
        narrative nullable: true, maxSize: 255
        routineRequestTmp nullable: true

        requestStatus inList: ['pending', 'running', 'success', 'error', 'cancelled']
        transactionBatch nullable: true
        cancelledBy nullable: true
        requestedBy nullable: true
        startTime nullable: true
        endTime nullable: true
        duration nullable: true
        valueDate validator: { Date value, RoutineRequest object, Errors errors ->
            if(value && object.billingCycle) {
                if(!object.billingCycle.isInCycle(value)) {
                    errors.rejectValue("billingCycle", "routineRequest.valueDate.invalid", "The value date does not match with the provided Billing Cycle")
                }
            }
        }
        routineType validator: { RoutineType value, RoutineRequest object, Errors errors->

            Long objectCount = RoutineRequest.where {
                if(object.id) {
                    id != object.id
                }
                billingCycle == object.billingCycle
                routineType  == object.routineType
                cancelled == false
                (requestStatus in ['pending', 'success', 'running'])
            }.count();

            if(objectCount) {
                errors.rejectValue("billingCycle", "routineRequest.billingCycle.exists", "A Request with specified Billing Cycle already exists")
            }
        }
        runDate validator: { Date val, RoutineRequest bj, Errors errors ->
            Date today = new Date().clearTime();

            if(val < today) {
                Object[] args = new Object[2];
                args[0] = "runDate"
                args[1] = val.format("yyyy-MM-dd HH:mm:ss")
                errors.rejectValue("runDate", "default.date.invalid.past", args, "Run Date value should not be in the past")
            }
        }
    }

    static mapping = {
    }

    Number getDuration(def type = null) {
        Long duration = 0L;

        if(endTime == null || startTime == null) {
            return duration;
        }

        use(TimeCategory) {
            TimeDuration timeDiff = endTime - startTime;
            duration = timeDiff.seconds;
        }

        return duration;
    }

    @Override
    String toString() {
        return id;
    }
}
