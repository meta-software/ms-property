package metasoft.property.core

class BillingInvoiceItem implements Loggable<BillingInvoiceItem> {

    BillingInvoice billingInvoice;
    MainTransaction mainTransaction;

    BigDecimal amount;
    Date dateCreated;

    static belongsTo = [billingInvoice: BillingInvoice]

    static constraints = {
        amount min: 0.0, shared: "numberConstraint"
    }

}
