package metasoft.property.core

import grails.rest.Linkable

@Linkable
class GeneralLedger implements Loggable<GeneralLedger>  {

    String accountNumber;
    String glAccountNumber;
    String accountName;
    Boolean active = true;  //is this account active?
    Boolean system = false; //is this a system ledger?

    SubAccount subAccount;
    GeneralLedgerType generalLedgerType;

    //Timestamp parameters
    Date dateCreated;
    Date lastUpdated;

    static hasOne = [bankAccount : GlBankAccount]

    static constraints = {
        bankAccount nullable: true
        generalLedgerType nullable: false
        subAccount nullable: false
        accountName nullable: false, blank: false
        accountNumber nullable: false, unique: true /*, validator: { val, obj ->
            Integer accountNumberInt = Integer.parseInt(val)
            if(accountNumberInt < obj.generalLedgerType.startRange || accountNumberInt > obj.generalLedgerType.endRange ) {
                errors.rejectValue("accountNumber", "Invalid value-range for accountNumber");
            }
        }*/
    }

    static mapping = {
        accountNumber index: 'gl_acc_nbr_idx'
        glAccountNumber index: 'gl_sys_acc_nbr_idx'
        accountName index: 'gl_acc_name_idx'
        system column: 'is_system'
        active column: 'is_active'

        cache true
    }

    def beforeValidate() {
        this.glAccountNumber = String.format("100%s", accountNumber);
    }

    @Override
    String toString() {
        return accountName;
    }
}
