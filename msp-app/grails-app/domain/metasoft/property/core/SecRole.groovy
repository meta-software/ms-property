package metasoft.property.core

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class SecRole implements Serializable {

	private static final long serialVersionUID = 1

	String authority;
	String name;
	String comments;

    SortedSet<SecPermission> permissions;

	static hasMany = [permissions: SecPermission]

	static constraints = {
		authority nullable: false, blank: false, unique: true
		comments nullable: true
	}

	static mapping = {
		cache true
	}

	@Override
	String toString() {
		return this.name;
	}

	public static List<SecRole> listRoles(Map params = [:]) {

		return SecRole.where{
			!(authority in ['ROLE_API_USER',  'ROLE_SUPERUSER'])
		}.list(params);

	}
}
