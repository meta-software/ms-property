package metasoft.property.core

import grails.rest.Linkable

@Linkable
class Account implements MakerCheckable<Account> , Loggable<Account> {

    Title title;
    AccountType accountType; /* Individual, Company */
    AccountCategory accountCategory; /* Tenant, Landlord, Supplier, etc... */

    String accountNumber;
    String accountName;

    Boolean locked     = false;
    Boolean terminated = false;
    Date dateTerminated;

    Address physicalAddress;
    Address postalAddress;
    Address businessAddress;
    String phoneNumber; //the mobile number
    String faxNumber;
    String emailAddress;
    String bpNumber;
    String vatNumber;
    BankAccount bankAccount;
    BigDecimal accountBalance;

    // Stampable attributes
    Date dateCreated;
    Date lastUpdated;

    //AccountStatus accountStatus; //@todo: active, active-teminate request, active-renewal request, terminated

    static hasOne = [bankAccount: BankAccount]

    static embedded = ['businessAddress', 'physicalAddress', 'postalAddress']

    static constraints = {

        accountBalance nullable: true, shared: "numberConstraint"

        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        dateTerminated nullable: true
        bankAccount nullable: true
        accountNumber nullable: true
        physicalAddress nullable: true
        postalAddress nullable: true
        businessAddress nullable: true
        phoneNumber nullable: true, maxSize: 16, matches: "\\d+"
        faxNumber nullable: true, maxSize: 16, matches: "\\d+"
        title nullable: true
        vatNumber nullable: true
        bpNumber nullable: true
        emailAddress nullable: true, email: true
        accountCategory nullable: true
    }

    static mapping = {
        table "ms_account"
        discriminator column: 'data_type', index: 'data_type_idx'
        accountNumber index: 'acct_nbr_uidx'
        terminated column: 'is_terminated'
        checkStatus index: 'acc_chk_status_idx'
        sort "accountName"
    }

    public static List<Account> listByCategory(String categoryCode) {
        def query = Account.where {
          accountCategory.code == categoryCode
        };
        return query.list();
    }

    /**
     * Returns lists of Approved accounts and also active accounts
     *
     * @return
     */
    public static List<Account> approvedList() {
        def query = where {
            checkStatus == CheckStatus.APPROVED;
        }

        return query.list();
    }

    public static List findByMakerCheckerStatus(CheckStatus status, Map args = [:]) {
        def query = where {
            checkStatus == status
        }

        return query.list(args);
    }

    @Override
    String toString() {
        return accountName;
    }
}
