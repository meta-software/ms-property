package metasoft.property.core

class BillingInvoice implements Loggable<BillingInvoice>{

    Tenant tenant;
    Lease lease;
    String invoiceNumber;
    BillingCycle billingCycle;
    BigDecimal openingBalance;

    Date invoiceDate = new Date();
    Date dueDate;
    String invoiceFileName;

    boolean pdfGenerated = false;
    boolean emailSent = false;
    boolean smsSent = false;
    Date emailDate = new Date();
    Date smsDate;

    Date dateCreated;

    List<BillingInvoiceItem> billingInvoiceItems;

    static hasMany = [billingInvoiceItems: BillingInvoiceItem]
    static belongsTo = [tenant: Tenant, lease: Lease]

    static constraints = {
        emailDate nullable: true
        smsDate nullable: true;
        invoiceNumber nullable: true
        invoiceFileName nullable: true
        openingBalance shared: 'numberConstraint'
    }

    static mapping = {
        invoiceNumber index: "invoice_number_idx"
        billingCycle cascade: "none"
        lease cascade: "none"
        tenant cascade: "none"
    }
}
