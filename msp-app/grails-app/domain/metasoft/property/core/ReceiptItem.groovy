package metasoft.property.core

class ReceiptItem {

    Lease lease;
    Receipt receipt;
    SubAccount subAccount;
    String narrative;

    Date dateCreated;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal allocatedAmount;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    static belongsTo = [receipt: Receipt]

    static constraints = {
        allocatedAmount min: 0.0
    }

    static mapping = {
        version false
    }

}
