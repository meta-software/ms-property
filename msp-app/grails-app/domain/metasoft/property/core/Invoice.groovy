package metasoft.property.core

class Invoice {

    public static enum InvoiceStatus {
        OPEN, CLOSED, CANCELLED
    }

    Tenant tenant;
    SubAccount subAccount;
    BigDecimal subTotal;		    //[excluding vat, including every other tax component if any]
    BigDecimal vatAmount;		    //[the dollar figure amount]
    BigDecimal vatRate;		        //[the rate at which vat was effected]
    BigDecimal invoiceTotal;	    //[the invoice subTotal + taxAmount]
    BigDecimal balance; 		    //[the pending invoice balance as per the receipts from client]
    BigDecimal allocatedAmount;		//[the receipts amounts allocated thus far to this invoice]
    InvoiceStatus invoiceStatus;	//[the invoice status {open/closed/cancelled}
    String transactionReference;	//[similar to the transactionReference]
    Date invoiceDate;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
    }

    static mapping = {
        transactionReference index: "inv_txn_ref_idx"
        invoiceDate sqlType: 'date'
    }

}
