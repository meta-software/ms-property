package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class Settings {

    String name;
    String description;
    String category;
    String value;
    String classType;

    static constraints = {
        name unique: true
        classType inList: ['java.lang.String', 'java.lang.Boolean', 'java.lang.Integer', 'java.lang.Long', 'java.util.Date']
    }

    static mapping = {
        cache true
    }

    @Override
    String toString() {
        return value;
    }

}
