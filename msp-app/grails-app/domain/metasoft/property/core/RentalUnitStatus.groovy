package metasoft.property.core

class RentalUnitStatus {

    String code;
    String name;
    String description;

    static constraints = {
        code unique: true
        name unique: true
        description nullable: true
    }

    static mapping = {
        code column: '`code`'
        name column: '`name`'
    }

    @Override
    String toString() {
        return name;
    }


}
