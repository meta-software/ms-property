package metasoft.property.core

import groovy.time.TimeCategory

class RoutineLog {

    BillingCycle period; //The billing cycle for which this run was for

    String routineType;

    Date startTime = new Date();
    Date endTime;

    String createdBy;
    Date dateCreated;

    static transients = ['duration']

    static constraints = {
        period unique: 'routineType'
        routineType inList: ['interest', 'rent', 'op-cos']
        createdBy nullable: true
        endTime nullable: true
    }

    static mapping = {
        table "auto_routine_log"
        sort id: "asc"
    }

    Number getDuration(def type = null) {

        def duration = -1;

        if(endTime == null) {
            return duration;
        }

        use(TimeCategory) {
            def timeDiff = endTime - startTime;
            duration = timeDiff.seconds;
        }

        return duration;
    }

}
