package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@GrailsCompileStatic
@EqualsAndHashCode(includes='name')
@ToString(includes='name', includeNames=true, includePackage=false)
class SecPermission implements Serializable, Comparable<SecPermission> {

	private static final long serialVersionUID = 1L

	String name;
	String displayName;

	static constraints = {
		name nullable: false, blank: false, unique: true
		displayName nullable: true
	}

	static mapping = {
		sort name: 'asc'
		cache true
	}

	@Override
	String toString() {
		return displayName ?: name;
	}

    int compareTo(SecPermission obj) {
        name.compareTo(obj.name);
    }
}
