package metasoft.property.core

class LeaseTerminateRequestTmp implements Loggable<LeaseTerminateRequestTmp> {

    Lease lease;
    String actionReason; // the reason why the status must be in this new status
    Date actionDate;

    boolean processed = false;  // whether this request was processed or not.
    String processStatus;
    String processResponse;
    Date dateProcessed;

    Date dateCreated;
    Date lastUpdated;

    static transients = ['loggable']
    static belongsTo = [lease: Lease]

    LeaseTerminateRequest entity;
    UserAction userAction;

    static constraints = {
        actionReason maxSize: 1024
        entity nullable: true
        dateProcessed nullable: true
        processStatus nullable: true, inList: ['success', 'error']
        processResponse nullable: true
    }

    static mapping = { }

}
