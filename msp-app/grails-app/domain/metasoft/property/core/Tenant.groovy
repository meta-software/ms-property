package metasoft.property.core

import grails.rest.Linkable

@Linkable
class Tenant extends Account {

    // The cbz bank account number identifier
    String bankAccountNumber;

    static hasMany = [leases: Lease]

    static transients = ['activeLeases']

    static constraints = {
        bankAccountNumber nullable: true, unique: true
    }

    static mapping = {
        discriminator  value: 'tenant'
        leases  sort: 'leaseNumber', order: 'desc'
    }

    /**
     * Returns lists of Approved accounts and also active accounts
     *
     * @return
     */
    public static List<Account> approvedList() {
        def query = where {
            checkStatus == CheckStatus.APPROVED;
        }

        return query.list();
    }

}

class TenantAccount {

    Date dateCreated;

}
