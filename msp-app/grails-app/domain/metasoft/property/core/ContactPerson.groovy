package metasoft.property.core

import grails.plugins.orm.auditable.Auditable

/**
 * Created by lebohof on 8/2/2018.
 */
class ContactPerson implements Auditable {
    String firstName;
    String lastName;
    String address;
    String phoneNumber;
    String emailAddress;
    String skypeId;

    String createdBy;
    String updatedBy;

    static constraints = {
        skypeId nullable: true
        address nullable: true
        emailAddress nullable: true
        createdBy nullable: true
        updatedBy nullable: true
    }

    static belongsTo = [account: Account]

    @Override
    String toString() {
        return "${firstName} ${lastName}";
    }
}
