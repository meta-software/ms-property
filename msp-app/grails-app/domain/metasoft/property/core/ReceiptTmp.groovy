package metasoft.property.core

import static metasoft.property.core.Receipt.ReceiptStatus

class ReceiptTmp {

    String transactionReference;
    String narrative;
    Date transactionDate;
    Tenant tenant;

    ReceiptStatus receiptStatus;
    Date statusChangeDate ;
    GeneralLedger debitAccount;
    Receipt receipt;

    Date dateCreated;
    UserAction userAction;
    ReceiptBatchTmp receiptBatchTmp;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal receiptAmount;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    static hasMany = [receiptItemTmps: ReceiptItemTmp]
    static belongsTo = [receiptBatchTmp: ReceiptBatchTmp]

    static constraints = {
        transactionReference unique: true
        receipt nullable: true
        receiptBatchTmp nullable: true
    }

    static mapping = {
        transactionReference index: "rct_txn_ref_idx"
        debitAccount index: "rcpt_db_acct_idx"
        receiptStatus index: "rcpt_db_acct_idx"
        transactionDate sqlType: 'date'
        receiptItemTmps cascade: "all-delete-orphan"
    }

}
