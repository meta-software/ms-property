package metasoft.property.core

import org.springframework.validation.Errors

class BillingYear implements MakerCheckeable, Loggable<BillingYear>  {

    String name;
    String shortDescription;
    Boolean active = false;
    Date startDate;
    Date endDate;

    Date dateCreated;
    Date lastUpdated;

    String activatedBy;
    Date dateActivated;

    String deactivatedBy;
    Date dateDeactivated;

    MakerChecker makerChecker
    static embedded = ['makerChecker'];

    static hasMany = [billingCycles: BillingCycle]

    static constraints = {
        name unique: true
        shortDescription nullable: true
        deactivatedBy nullable: true
        dateDeactivated nullable: true
        dateActivated nullable: true
        makerChecker nullable: true
        endDate validator: { Date value, BillingYear object, Errors errors ->
            if ((value && object.startDate) && value <= object.startDate) {
                Object[] args = (Object[])[value, object.startDate];
                errors.rejectValue("endDate","billingCycle.endDate.afterStart", args, "End Date should be after the Start Date value")
                return false; //the date parameters checks are wrong
            }

            return true;
        }
    }

    static mapping = {
        cache true
        sort "yyyy"
    }

    def beforeInsert() {
        if(!this.shortDescription) {
            this.shortDescription = "Billing year : " + this.name;
        }
    }

    @Override
    String toString() {
        return name;
    }
}
