package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class SecUserPreviousPassword implements Serializable {

    private static final long serialVersionUID = 1

    SecUser secUser;
    String password;

    Date dateCreated;

    static constraints = {

    }

    static mapping = {
	    password column: '`password`'
        sort "id"
        order "desc"
    }

}
