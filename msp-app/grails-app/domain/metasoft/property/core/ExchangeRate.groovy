package metasoft.property.core

class ExchangeRate {

    Currency fromCurrency;  //we're converting from this currency to the toCurrency (our reference currency)
    Currency toCurrency;    //this should always be the base currency.
    BigDecimal rate;        //the exchange rate value
    Date startDate
    Date endDate

    Boolean active = false
    Date dateActivated

    Boolean deleted = false;
    Date dateDeleted;

    Date dateCreated;

    static belongsTo = [exchangeRateGroup: ExchangeRateGroup]

    static constraints = {
        rate min: 0.0
        dateDeleted nullable: true
        dateActivated nullable: true
        exchangeRateGroup nullable: true
    }

    static mapping = {
        cache true
        startDate sqlType: 'date', index: 'exchange_rate_start_date_idx'
        endDate sqlType: 'date', index: 'exchange_rate_end_date_idx'
    }
}