package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.plugins.orm.auditable.Auditable
import org.springframework.validation.Errors

@GrailsCompileStatic
class BulkInvoiceItem implements Loggable<BulkInvoiceItem> {

    Property property;
    RentalUnit rentalUnit;
    Lease lease;
    BigDecimal amount;
    TransactionType transactionType;
    Date transactionDate;
    String transactionReference;

    boolean posted = false;
    Date datePosted;
    String postedBy;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [bulkInvoice: BulkInvoice]

    static constraints = {
        amount min: 0.0, shared: "numberConstraint"
        datePosted nullable: true
        postedBy nullable: true
    }

    static mapping = {
    }

    def beforeValidate() {
        this.rentalUnit = lease.rentalUnit;
    }

}
