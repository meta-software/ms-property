package metasoft.property.core

class StandingOrder {

    Bank bank;
    BigDecimal amount;
    Boolean percentage = false;
	String description;
    String reference;

    Boolean active = true;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [landlord: Landlord]

    static constraints = {
    }

    @Override
    String toString() {
        return "${bank}-${reference}"
    }

}
