package metasoft.property.core

class LeaseItem implements MakerCheckable<LeaseItem>, Loggable<LeaseItem>  {

    SubAccount subAccount;
    BigDecimal amount;
    Date startDate;
    Date endDate;

    // termination fields.
    boolean terminated;
    Date dateTerminated;

    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [lease: Lease]
    static transients = ['valid']

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        amount min: 0.0, shared: "numberConstraint"
        dateTerminated nullable: true
        endDate nullable: true, validator: { val, obj, errors ->
            if( val && (obj.startDate > val) ) {
                errors.rejectValue('endDate', 'Start Date cannot be after End Date value');
            }
        }
        subAccount validator: { val, obj ->
            //check if an already active similar chargeCode exists for the lease please, check also for conflict.
        }
    }

    static mapping = {
        sort id: "asc"
        terminated column: 'is_terminated'
        lease fetch: 'join'
    }

    Boolean isValid() {
        Date today = (new Date() + 1).clearTime();

        if (this.endDate != null && this.active && this.endDate < today) {
            return false;
        }

        return true;
    }

}
