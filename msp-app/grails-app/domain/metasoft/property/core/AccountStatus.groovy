package metasoft.property.core

class AccountStatus {

    String code;
    String name;

    static constraints = {
        code unique: true
        name unique: true
    }

    @Override
    String toString() {
        return name
    }
}
