package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import org.springframework.validation.Errors

@GrailsCompileStatic
class TransactionTmp implements Loggable<TransactionTmp> {

    String entryNumber;
    Date transactionDate;
    String creditAccount;
    String debitAccount;
    String creditSubAccount;
    String debitSubAccount;
    String bankAccount;
    BigDecimal amount;
    String transactionReference;
    String description;
    SubAccount subAccount;
    TransactionBatchTmp transactionBatchTmp;

    //Extra fields to add clarity to transaction data
    Lease lease;
    RentalUnit rentalUnit;
    Property property;
    BillingCycle billingCycle;
    TransactionType transactionType;
    boolean reconciled = true;

    Date dateCreated;

    //the currencies setup
    Currency transactionCurrency;
    BigDecimal transactionAmount;

    Currency reportingCurrency;
    BigDecimal reportingAmount;
    ExchangeRate reportingExchangeRate;

    Currency operatingCurrency;
    BigDecimal operatingAmount;
    ExchangeRate operatingExchangeRate;

    static belongsTo = [transactionBatchTmp: TransactionBatchTmp]

    static constraints = {
        transactionBatchTmp nullable: true
        description nullable: false, maxSize: 128
        bankAccount nullable: true
        entryNumber nullable: true
        debitAccount nullable: true
        debitSubAccount nullable: true
        creditSubAccount nullable: true
        creditAccount nullable: false,  blank: false
        transactionDate validator: { Date date, def obj, Errors errors ->
            if(!BillingCycle.isInOpenCycle(date)) {
                Object[] args = [date.format('YYYY-MM-dd')]
                errors.rejectValue("transactionDate","billingCycle.posted.invalid", args, "Invalid transaction date")
            }
        }
        transactionReference nullable: true, maxSize: 32, validator: { String value, TransactionTmp obj, Errors errors ->
            if( !(obj.transactionType?.transactionTypeCode in ['RF', 'CN']) && obj.id == null ) {
                Long sameRefTransactionCount = (Long)MainTransaction.where {
                    transactionReference == value
                }.count();

                if(sameRefTransactionCount > 0) {
                    Object[] args = [value]
                    errors.rejectValue("transactionReference","transaction.reference.exists", args, "Transaction with reference \"${value}\" already exists")
                }
            }
        }
        lease nullable: true
        rentalUnit nullable: true
        property nullable: true
        billingCycle nullable: true
        amount shared: "numberConstraint"
    }

    static mapping = {
        table 'ms_transaction_tmp'
        transactionReference index: 'txn_tmp_ref_idx'
        sort "id"
    }

    void beforeValidate() {
        if(this.lease != null) {
            this.rentalUnit = this.lease.rentalUnit;
            this.property = this.lease.property;
        }
    }

}

