package metasoft.property.core

import org.springframework.validation.Errors

class ExchangeRateGroupCheck implements MakerCheck<ExchangeRateGroupCheck> {

    ExchangeRateGroup entity;

    Date dateCreated;
    Date lastUpdated;

    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    static constraints = {
        entity nullable: true
        checkDate nullable: true
        checker nullable: true, validator: { SecUser value, ExchangeRateGroupCheck object, Errors errors ->
            if(object.checkStatus != CheckStatus.CANCELLED && value && (object.maker?.id == value?.id) ) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        checkComment nullable: true
        jsonData maxSize: 4096
    }

    static mapping = {

    }

    @Override
    String toString() {
        return entity?.toString();
    }
}
