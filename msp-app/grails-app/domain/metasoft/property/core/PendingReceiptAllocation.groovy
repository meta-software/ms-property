package metasoft.property.core

class PendingReceiptAllocation {

    PendingReceipt pendingReceipt;
    Lease lease;
    //Receipt receipt;
    BigDecimal allocatedAmount;
    SubAccount subAccount;
    String narrative;

    Date dateCreated;

    static belongsTo = [pendingReceipt: PendingReceipt]

    static constraints = {
        narrative nullable: true
        allocatedAmount min: 0.0
    }

    static mapping = {
        version false
    }

}
