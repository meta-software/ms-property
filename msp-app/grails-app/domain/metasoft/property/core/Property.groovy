package metasoft.property.core

import org.springframework.validation.Errors

class Property implements MakerCheckable<Property>, Loggable<Property>  {

    String name;
    Address address;
    String description;
    BigDecimal area;
    BigDecimal commission;
    boolean chargeVat = false;
    boolean fullyConfigured = false;

    PropertyType propertyType;
    PropertyManager propertyManager;

    Date mandateDate;
    Date lastInspectionDate;
    Date terminatedDate;
    boolean terminated = false;

    String propertyCode
    String notes;
    String standNumber;

    boolean deleted = false;
    Date deletedDate;

    Date dateCreated;
    Date lastUpdated;

    //for use during editing or action request phases.
    PropertyTmp propertyTmp;

    static embedded = ['address']

    static hasMany = [rentalUnits: RentalUnit
                      ,propertyInsurances: PropertyInsurance]

    static belongsTo = [landlord: Landlord]

    static transients =['propertyInsurance'];

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        propertyTmp nullable: true
        area min: 0.0, validator: { BigDecimal value, Property object, Errors errors ->
            //BigDecimal totalUnitsArea = RentalUnit
        }
        deletedDate nullable: true
        commission min: 0.0, max: 100.0
        terminatedDate nullable: true
        lastInspectionDate nullable: true
        notes maxSize: 1024, nullable: true
        propertyCode nullable: true
        mandateDate nullable: true
    }

    static mapping = {
        terminated column: 'is_terminated'
        checkStatus index: 'prop_chk_status_idx'
        sort name: 'asc'
    }

    def beforeValidate() {
        //@todo: move this into services.
        if(propertyType?.code == "commercial") {
            chargeVat = true;
        }
    }

    @Override
    String toString() {
        return name;
    }

    PropertyInsurance getPropertyInsurance() {
        PropertyInsurance propertyInsurance = PropertyInsurance.where {
            property == this
            active == true
        }.get()

        return propertyInsurance;
    }

    Long rentalUnitsCount() {

        def query = RentalUnit.where {
            property == this
            deleted == false
            //checkStatus == CheckStatus.APPROVED
        }

        return query.count();
    }

    /**
     * Returns lists of Approved accounts and also active accounts
     *
     * @return
     */
    public static approvedList() {
        def query = where {
            checkStatus == CheckStatus.APPROVED;
        }

        return query.list();
    }

    public static List findByMakerCheckerStatus(CheckStatus status, Map args = [:]) {
        def query = where {
            checkStatus == status
        }

        return query.list(args);
    }

}
