package metasoft.property.core

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class TransactionBatchTmp implements Loggable<TransactionBatchTmp> {

    TransactionType transactionType;
    String batchReference;

    boolean autoCommitted = false;
    boolean posted = false;
    String postedBy;
    Date datePosted;

    boolean postRequest = false;
    String postRequestBy;
    Date datePostRequest;

    Date dateCreated;
    Date lastUpdated;

    String createdBy;

    static hasMany = [transactionTmps: TransactionTmp]

    static transients = ['batchNumber', 'transactionsCount','transactionsTotal']

    static fetchMode = [transactionTmps: 'lazy']

    static constraints = {
        createdBy nullable: true
        postedBy nullable: true
        datePosted nullable: true

        batchReference nullable: true
        postRequestBy nullable: true
        datePostRequest nullable: true
    }

    static mapping = {
        transactionTmps  cascade: "all"
    }

    // the transaction batch number
    String getBatchNumber() {
        String year = new Date().format("YY");
        return String.format("%s%09d", year, id);
    }

    Long getTransactionsCount() {
        return ((Long)TransactionTmp.countByTransactionBatchTmp(this));
    }

    BigDecimal getTransactionsTotal() {
        String hqlQuery = "select sum(t.amount) from TransactionTmp t join t.transactionBatchTmp h where h.id=:id"
        BigDecimal transactionTotal = (BigDecimal)TransactionTmp.executeQuery(hqlQuery, [id: this.id])[0] ?: 0.00

        return transactionTotal;
    }
    
    def afterInsert() {
        this.batchReference = String.format("B%09d", this.id)
        this.save();
    }

}
