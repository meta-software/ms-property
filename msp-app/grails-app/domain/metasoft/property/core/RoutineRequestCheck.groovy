package metasoft.property.core

import org.springframework.validation.Errors

class RoutineRequestCheck implements MakerCheck<RoutineRequestCheck> {

    RoutineRequest entity;
    RoutineRequestTmp transactionSource;

    Date dateCreated;
    Date lastUpdated;

    static transients = ['userAction']

    static constraints = {
        checker nullable: true,validator: { SecUser value, RoutineRequestCheck object, Errors errors ->
            if(value && object.checkStatus != CheckStatus.CANCELLED && object.maker?.id == value?.id) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker');
            }
        }
        entity nullable: true
        checkDate nullable: true
        checkComment nullable: true
        jsonData maxSize: 8192
    }

    static mapping = {
    }

    UserAction getUserAction() {
        return transactionSource.userAction;
    }

    @Override
    String toString() {
        return entity.toString();
    }
}
