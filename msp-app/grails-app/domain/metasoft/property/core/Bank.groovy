package metasoft.property.core

class Bank implements Loggable<Bank> {
    String bankName;
    String bankCode;
    boolean active = true;

    Date dateCreated;
    Date lastUpdated;

    static hasMany = [branches: Branch]

    static constraints = {
        bankName unique: true
        bankCode unique: true
    }

    static mapping = {
        cache true
    }

    @Override
    String toString() {
        return bankName;
    }
}
