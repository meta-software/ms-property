package metasoft.property.core

class TransactionBatch implements MakerCheckable<TransactionBatch>, Loggable<TransactionBatch> {

    TransactionType transactionType;
    CheckStatus checkStatus;
    String batchNumber;
    boolean autoCommitted = false;
    Date dateCreated;
    Date lastUpdated;

    //MakerChecker fields;
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    //
    String checkComment

    static hasMany = [transactions: MainTransaction];

    static transients = ['transactionsCount']

    static constraints = {
        dirtMaker nullable: true
        maker nullable: true
        makeDate nullable: true
        checker nullable:true
        checkDate nullable: true
        checkComment nullable: true

        batchNumber unique: true
    }

    Long getTransactionsCount() {
        return ((Long)MainTransaction.countByTransactionBatchAndParentTransaction(this, true));
    }

}
