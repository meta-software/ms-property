package metasoft.property.core

import grails.rest.Linkable
import groovy.time.TimeCategory

@Linkable
class Lease implements MakerCheckable<Lease>, Loggable<Lease> {

    Landlord landlord;
    Tenant tenant
    String lesseeAccount;  //tenantAccount
    String leaseNumber;    //leaseNumber @todo: autocreated

    /*
    BigDecimal rentCharge;
    BigDecimal depositCharge;
     */

    Currency leaseCurrency;
    Currency invoicingCurrency;

    RentalUnit rentalUnit;  //The rental Unit under which is being leased
    Property property;

    Date signedDate;            // agreement date
    Date leaseStartDate;        // commencement date
    Date leaseExpiryDate;       // the lease expiry date

    Boolean expired = false;
    Date expiredDate;

    Boolean deleted = false;
    Date dateDeleted;

    Boolean terminated = false;
    Date dateTerminated;

    LeaseStatus leaseStatus;    // new/active/inactive/suspended, etc...
    LeaseType leaseType;        // standard

    boolean pendingReview = false; //has pending lease review.
    Date lastReviewDate;
    Date nextReviewDate;

    //Timestamp
    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [tenant: Tenant, rentalUnit: RentalUnit];
    static hasMany = [leaseItems: LeaseItem]
    static transients = ['leaseRenewals', 'active', 'status', 'activated', 'landlords', 'editable'];

    static constraints = {
        dirtMaker nullable: true

        //rentCharge min: 0.0
        //depositCharge min: 0.0

        maker nullable: true
        makeDate nullable: true
        checker nullable: true
        checkDate nullable: true

        lastReviewDate nullable: true
        nextReviewDate nullable: true
        leaseNumber nullable: true
        dateTerminated nullable: true
        leaseType nullable: true
        landlord nullable: true
        property nullable: true
        signedDate nullable: true
        expiredDate nullable: true
        dateDeleted nullable: true
        leaseStartDate validator: { val, obj, errors ->
            return

            if(val != null) {
                if( !val.before(obj?.leaseExpiryDate) ) {
                    Object[] errArgs = new Object[4]

                    errArgs[0] = 'leaseStartDate'
                    errArgs[1] = obj.leaseStartDate
                    errArgs[2] = 'leaseExpiryDate'
                    errArgs[3] = obj.leaseExpiryDate

                    String message = "Invalid dates Start Date:${obj.leaseStartDate.format('yyyy/MM/dd')}, should be before Expiry Date : ${obj.leaseExpiryDate.format('yyyy/MM/dd')}"

                    errors.rejectValue("leaseStartDate", "default.invalid.value.before", errArgs,message)
                    return;
                }
            }

            def objId = obj.id
            def objStartDate  = obj.leaseStartDate;
            def objExpiryDate = obj.leaseExpiryDate;

            if(obj == null) {
                println("it is null")
            }

            def countConflicts =  Lease.where {
                if(objId) { // we want to exclude this particular object entity
                    id != objId
                }

                rentalUnit.id == obj.rentalUnit.id
                leaseStatus.code in ['new', 'active']
                checkStatus  in [CheckStatus.PENDING, CheckStatus.APPROVED]
                (
                       (leaseStartDate <= objStartDate && leaseExpiryDate >= objStartDate ) 	// 4
                    || (leaseStartDate <= objExpiryDate && leaseExpiryDate >= objExpiryDate)	// 2
                    || (leaseStartDate >= objStartDate && leaseStartDate <= objExpiryDate )		// 0
                    || (leaseExpiryDate >= objStartDate && leaseExpiryDate <= objExpiryDate) 	//
                )
            }.count()

            if(countConflicts) {
                errors.rejectValue("leaseStartDate", "lease.invalid.date.conflict")
                return;
            }

        }
    }

    static mapping = {
        table 'ms_lease'
        leaseStartDate sqlType: 'date'
        leaseExpiryDate sqlType: 'date'
        signedDate sqlType: 'date'
        lastReviewDate sqlType: 'date'
        nextReviewDate sqlType: 'date'
        leaseNumber index: 'lease_nbr_idx_u'
        leaseItems sort: 'id', order: 'asc', cascade: "all-delete-orphan"
        terminated column: 'is_terminated'
        rentalUnit updateable: false
        sort "leaseNumber"
    }

    static namedQueries = {
        leaseReviews {
            Date now = new Date().clearTime()
            lt 'nextReviewDate', now + 14
            eq 'terminated', false
            eq 'deleted', false
            eq 'expired', false
        }
    }

    def beforeValidate() {
        // set the lesseeAccount
        this.lesseeAccount = this?.tenant?.accountNumber;

        // set the property
        this.property = this?.rentalUnit?.property;

        // set the landlord
        this.landlord = this?.property?.landlord;

        //update the lease status
        this.beforeSave();
    }

    List<LeaseRenewalRequest> getLeaseRenewals() {

        return LeaseRenewalRequest.where {
            lease == this
            makerChecker.status 'approved' //== CheckStatus.APPROVED
            processed == true
        }.list([order: 'actionDate', sort: 'desc']);

    }

    Landlord getLandlord() {
        return this?.property?.landlord;
    }

    Boolean isActive() {
        return ( leaseStatus?.code == 'active' && this.terminated == false);
    }

    Boolean isActivated() {
        return ['active', 'terminated'].contains(leaseStatus?.code);
    }

    Boolean isTerminated() {
        return leaseStatus?.code == 'terminated';
    }

    Boolean isStatus(String... status) {
        return status.contains(leaseStatus?.code)
    }

    @Override
    String toString() {
        return leaseNumber;
    }

    Boolean isExpired() {
        Date today = new Date().clearTime();

        use(TimeCategory) {
            today += 1.days
        }
        return this.leaseExpiryDate?.before(today);
    }

    /**
     * Returns lists of Approved accounts and also active accounts
     *
     * @return
     */
    public static approvedList() {
        def query = Lease.where {
            checkStatus == CheckStatus.APPROVED
        }

        return query.list();
    }

    public static List findByMakerCheckerStatus(CheckStatus makerCheckerStatus, Map args = [:]) {
        def query = Lease.where {
            checkStatus == makerCheckerStatus
        }

        return query.list(args);
    }

    public LeaseStatusRequest getActiveRequest() {
        return LeaseStatusRequest.where {
            lease == this
            approveStatus == "pending"
        }.get([order: 'dateCreated', sort: 'desc'])
    }

    public boolean isEditable() {
        if(isChecked()) {
            return true; //@todo: this must be updated.
        }

        return true;
    }

    def beforeSave() {

        if(this.isDirty('leaseStatus')) {
            switch(this.leaseStatus.code) {
                case 'expired':
                    this.expired = true;
                    this.expiredDate = new Date();

                    //also update the state of the rental unit
                    this.rentalUnit.unitStatus = RentalUnitStatus.findByCode('un-occupied')
                    break;
                case 'terminated':
                    this.terminated = true;
                    this.dateTerminated = new Date()

                    //also update the state of the rental unit
                    this.rentalUnit.unitStatus = RentalUnitStatus.findByCode('un-occupied')
                    break;
                case 'active':
                    //also update the state of the rental unit
                    this.rentalUnit.unitStatus = RentalUnitStatus.findByCode('occupied')
                    break;
            }
        }
    }

}

/**
 * The LeaseNumber manager entity
 */
class LeaseNumber {
    Date dateCreated;
}