package metasoft.property.api

import metasoft.property.core.Lease
import metasoft.property.core.Loggable

class TransactionRequest implements Loggable<TransactionRequest> {

    String accountNumber;
    String accountName;     // the tenant account name
    String transactionType; // the transaction type  [payment/deposit/etc...]
    String subAccountCode   // the subAccount transaction code {Rent, Deposit, Repairs, etc...}
    String transactionReference;    // the flexcube transaction reference
    String accountBr;               // the cbz account account branch receiving
    String accountNumberBr;         // the cbz account number where funds are deposited into.
    String description;             // free text description of transaction
    String rentalUnitCode;          //
    String rentalUnitName;
    String leaseNumber;             // the lease for which payment is being made
    Date transactionDate;           // the date of transaction
    BigDecimal amount;              // the transaction amount

    String requestBody;             // the POST request body
    String createdBy;               // the user who created the record.

    Boolean processed = false;      // whether the record has been processed to the main transaction.
    Date dateProcessed;             // the date & time the record was processed
    String processStatus;           // the status of the process exercise.
    String processedBy;             // the user who actioned the processed to occur
    String responseStatus;          // ???
    String response;                // ??

    Date dateCreated;               // the created date
    Date lastUpdated;               // the last updated date of the record

    static constraints = {
        rentalUnitName nullable: true
        rentalUnitCode nullable: true

        transactionType nullable: false
        accountBr nullable: false
        accountNumberBr nullable: false
        amount min: 0.0
        requestBody maxSize: 4096
        leaseNumber nullable: false, validator: { value, object, errors ->
            //@todo check if the accountNumber is a valid account number and return as such
            if( !Lease.findByLeaseNumber(value) ) {
                Object[] errorArgs = [value];
                errors.rejectValue('leaseNumber', 'transactionApi.accountNumber.notExist', errorArgs, "The specified account number '${value}' does not exist");
            }
        }
        accountName nullable: false
        transactionReference unique: true

        requestBody nullable: true
        createdBy nullable: true
        processed nullable: true
        dateProcessed nullable: true
        processStatus nullable: true
        processedBy nullable: true
        responseStatus nullable: true
        response nullable: true
    }

    static mapping = {
        //table "`api_trans_request`"
        leaseNumber index: 'lease_nbr_idx'
        requestBody type: 'text'
        transactionDate sqlType: 'date'
        transactionReference index: 'transaction_ref_uidx'
    }

}
