package metasoft.property.api

import metasoft.property.api.v1.TransactionRequestStatusCodeEnum
import metasoft.property.core.Currency

class ReceiptRequest {

    static loggable = false;

    TransactionRequestStatusCodeEnum requestStatus = TransactionRequestStatusCodeEnum.SUCCESS;
    String requestMessage;
    String bankAccountNumber;
    String accountName;             // the tenant account name
    String transactionReference;    // the flexcube transaction reference
    String narration;               // free text description of transaction
    Date transactionDate;           // the date of transaction
    BigDecimal amount;              // the transaction amount
    BigDecimal allocatedAmount;     // the amount allocated so far to items
    Currency transactionCurrency;

    String trustAccountName;
    String trustAccountNumber;

    String requestBody;             // the POST request body
    String createdBy;               // the user who created the record.

    Boolean processed = false;      // whether the record has been processed to the main transaction.

    Date dateProcessed;             // the date & time the record was processed
    String processStatus;           // the status of the process exercise.
    String processedBy;             // the user who actioned the processed to occur
    String responseStatus;          // ???
    String response;                // ??

    Date dateCreated;               // the created date
    Date lastUpdated;               // the last updated date of the record

    static constraints = {

        bankAccountNumber nullable: false
        accountName nullable: false
        amount min: 0.0
        requestBody maxSize: 4096
        transactionReference nullable: false
        narration nullable: false
        requestBody nullable: true
        createdBy nullable: true
        processed nullable: true
        dateProcessed nullable: true
        processStatus nullable: true
        processedBy nullable: true
        responseStatus nullable: true
        response nullable: true
        requestStatus nullable: true
        requestMessage nullable: true
        transactionCurrency nullable: true
    }

    static mapping = {
        bankAccountNumber index: 'receipt_request_bank_acc_nbr_idx'
        transactionReference index: 'receipt_request_txn_ref_idx'
        requestStatus index: 'receipt_request_status_idx'
        requestBody type: 'text'
        transactionDate sqlType: 'date'
        //allocatedAmount formula: "(SELECT SUM(a.allocated_amount) from receipt_request_allocation a where a.receipt_request_id=id)"
    }

}
