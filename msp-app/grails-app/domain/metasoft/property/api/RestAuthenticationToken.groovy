package metasoft.property.api

import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class RestAuthenticationToken {

    String username;
    String authToken;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        authToken maxSize: 1024
        username unique: true
    }

    static mapping = {
        version false
    }
}
