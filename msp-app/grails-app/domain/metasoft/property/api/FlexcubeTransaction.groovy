
package metasoft.property.api

import metasoft.property.core.Loggable

class FlexcubeTransaction implements Loggable<FlexcubeTransaction> {

    String transactionReference; //transaction reference
    Date transactionDate;        //the transaction date
    String accountNumberCr;      //the credit account number
    String accountNumberDb;      //debit account number
    BigDecimal transactionAmount;//mainTransaction.[credit/debit]
    String transactionDetails;   //mainTransaction.description
    String currency;
    String zwlCurrency;
    String debitAccountBranch;
    String creditAccountBranch;
    String receivingAccount;
    String receivingBank;

    boolean posted;
    Date datePosted;
    String postedBy;

    Date dateCreated;

    static constraints = {
        transactionReference unique: true
        datePosted nullable: true
        postedBy nullable: true
    }

    static mapping = {
        // mainTransaction fetch: 'join'
        accountNumberCr index: 'flx_acct_nbr_cr_idx'
        accountNumberDb index: 'flx_acct_nbr_db_idx'
        receivingAccount index: 'flx_rcv_acct_idx'
    }

}
