package metasoft.property.api

import metasoft.property.core.Loggable

class FlexcubeTransactionRequest implements Loggable<FlexcubeTransactionRequest> {

    FlexcubeTransaction flexcubeTransaction;
    String xmlRequest;
    String xmlResponse;
    String responseStatus;

    Date dateCreated;
    Date lastUpdated;

    static constraints = {
        xmlResponse nullable: true
        xmlResponse nullable: true
        responseStatus nullable: true
    }

    static mapping = {
        xmlRequest type: 'text'
        xmlResponse type: 'text'
    }

}
