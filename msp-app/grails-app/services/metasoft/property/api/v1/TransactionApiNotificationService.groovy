package metasoft.property.api.v1

import grails.compiler.GrailsCompileStatic
import grails.events.annotation.Subscriber
import grails.gorm.transactions.Transactional
import metasoft.property.api.TransactionRequest
import metasoft.property.core.MainTransaction
import metasoft.property.core.TransactionBatch

@GrailsCompileStatic
class TransactionApiNotificationService {

    @Transactional
    @Subscriber("saveTransactionApiRequest")
    void onSaveTransactionApiRequest(TransactionRequest transactionApiRequest) {
        log.info("transactionApiRequest saved successfully : " + transactionApiRequest.properties);
    }

    @Transactional
    @Subscriber("transactionBatch:post")
    void onCreateTransaction(TransactionBatch transactionBatch) {
        // @todo: in this case we create a
    }

}
