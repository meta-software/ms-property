package metasoft.property.api.v1

import grails.core.GrailsApplication
import grails.events.annotation.Publisher
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.databinding.DataBinder
import groovy.json.JsonBuilder
import metasoft.property.api.ReceiptRequest
import metasoft.property.api.TransactionRequest
import metasoft.property.core.BillingCycle
import metasoft.property.core.BillingCycleService
import metasoft.property.core.Currency
import metasoft.property.core.GeneralLedger
import metasoft.property.core.GeneralLedgerService
import metasoft.property.core.GlBankAccount
import metasoft.property.core.Lease
import metasoft.property.core.PendingReceipt
import metasoft.property.core.ReceiptBatch
import metasoft.property.core.ReceiptBatchCheckService
import metasoft.property.core.ReceiptBatchTmp
import metasoft.property.core.ReceiptBatchTmpService
import metasoft.property.core.RentalUnit
import metasoft.property.core.SubAccount
import metasoft.property.core.Tenant
import metasoft.property.core.TransactionBatchTmp
import metasoft.property.core.TransactionBatchTmpService

import metasoft.property.core.TransactionTmp
import metasoft.property.core.TransactionTmpService
import metasoft.property.core.TransactionType
import org.grails.core.exceptions.GrailsException
import org.grails.web.util.WebUtils

import javax.servlet.http.HttpServletRequest

//@GrailsCompileStatic
@ReadOnly
class TransactionApiService implements DataBinder {

    SpringSecurityService springSecurityService;
    GrailsApplication grailsApplication;

    TransactionBatchTmpService transactionBatchTmpService
    TransactionTmpService transactionTmpService
    BillingCycleService billingCycleService

    private HttpServletRequest getRequestObject() {
        HttpServletRequest request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()

        return request;
    }

    @Transactional
    @Publisher("saveTransactionApiRequest")
    Map createBankDepositPayment(Map transactionParams, Map requestData) {
        //
        Lease lease = Lease.findByLeaseNumber(transactionParams['accountNumber'], [readOnly:true]);

        TransactionRequest transactionApiRequest = new TransactionRequest();
        bindData(transactionApiRequest, transactionParams)
        transactionApiRequest.transactionType = "RC";   // the payment(receipt) transactionTypeCode
        transactionApiRequest.subAccountCode = transactionParams.get('transactionTypeCode', 'RE'); // default to rent
        transactionApiRequest.accountNumber = lease.tenant?.accountNumber;
        transactionApiRequest.leaseNumber = transactionParams['accountNumber'];
        transactionApiRequest.requestBody = new JsonBuilder(requestData).toString();

        Map returnResult = [:];

        // check for errors
        if(!transactionApiRequest.validate()) {
            if(transactionApiRequest.errors['transactionType']?.code) {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.UNKNOWN_ERROR;
                returnResult['transactionRequest'] = transactionApiRequest.errors[0]?.message;
            }
            if(transactionApiRequest.errors['accountNumber']?.code == 'nullable') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.MISSING_ACCOUNT_NUMBER;
                returnResult['transactionRequest'] = transactionApiRequest;
            } else if(transactionApiRequest.errors['accountNumber']?.code == 'nullable') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.NONEXIST_ACCOUNT_NUMBER
                returnResult['transactionRequest'] = transactionApiRequest;
            } else if(transactionApiRequest.errors['transactionReference']?.code == 'nullable') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.TRANSACTION_REFERENCE_MISSING
                returnResult['transactionRequest'] = transactionApiRequest;
            } else if(transactionApiRequest.errors['amount']?.code == 'min.notmet') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.INVALID_TRANSACTION_AMOUNT
                returnResult['transactionRequest'] = transactionApiRequest;
            } else if(transactionApiRequest.errors['amount']?.code == 'nullable') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.MISSING_TRANSACTION_AMOUNT
                returnResult['transactionRequest'] = transactionApiRequest;
            } else if (transactionApiRequest.errors['transactionReference']?.code == 'unique') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.TRANSACTION_REFERENCE_DUPLICATE
                returnResult['transactionRequest'] = transactionApiRequest;
            } else if (transactionApiRequest.errors['accountBr']?.code == 'nullable') {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.MISSING_ACCOUNT_BR
                returnResult['transactionRequest'] = transactionApiRequest;
            } else {
                returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.UNKNOWN_ERROR
                returnResult['transactionRequest'] = transactionApiRequest;
            }

            return returnResult;
        }

        try {
            this.saveTransaction(transactionApiRequest);
            returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.SUCCESS
            returnResult['transactionRequest'] = transactionApiRequest;
        } catch (ValidationException e) {
            log.error("Failed to save transactionRequest object: ${transactionApiRequest.properties}, ${e.message} ");
            e.printStackTrace();

            returnResult['requestStatus'] = TransactionRequestStatusCodeEnum.UNKNOWN_ERROR
            returnResult['transactionRequest'] = transactionApiRequest;
        }

        return returnResult;
    }

    ReceiptBatchTmpService receiptBatchTmpService;
    ReceiptBatchCheckService receiptBatchCheckService;

    GeneralLedgerService generalLedgerService;

    @Transactional
    @Publisher("saveTransactionApiRequest:rtgs")
    Map createRtgsPayment(TransactionApiRequestCommand transactionApiRequestCommand, Map requestData) {

        ReceiptRequest receiptRequest = new ReceiptRequest();
        bindData(receiptRequest, transactionApiRequestCommand.properties)
        receiptRequest.transactionCurrency = Currency.get(transactionApiRequestCommand.currency)
        receiptRequest.bankAccountNumber = transactionApiRequestCommand.accountNumber;
        receiptRequest.requestBody = new JsonBuilder(requestData).toString();
        receiptRequest.requestMessage = "Success";
        receiptRequest.allocatedAmount = 0.00;

        //Check if the transaction reference already exists
        if(ReceiptRequest.countByTransactionReferenceAndRequestStatus(receiptRequest.transactionReference, TransactionRequestStatusCodeEnum.SUCCESS)) {
            receiptRequest.requestStatus = TransactionRequestStatusCodeEnum.TRANSACTION_REFERENCE_DUPLICATE;
            receiptRequest.requestMessage = "request already exists for the transactionReference=${receiptRequest.transactionReference}";
            receiptRequest.save();
            return [requestStatus: receiptRequest.requestStatus, receiptRequest: receiptRequest];
        }

        //Check if the trustAccount exists
        //retrieve the generalLedger by bankAccount
        GlBankAccount glBankAccount = GlBankAccount.findByAccountNumber(receiptRequest.trustAccountNumber, [readOnly: true])
        if(!glBankAccount) {
            receiptRequest.requestStatus = TransactionRequestStatusCodeEnum.INVALID_TRUST_ACCOUNT_NUMBER;
            receiptRequest.requestMessage = receiptRequest.requestStatus.message
            receiptRequest.save();
            return [requestStatus: receiptRequest.requestStatus, receiptRequest: receiptRequest];
        }

        GeneralLedger trustAccount  = glBankAccount.generalLedger;

        Tenant tenant = Tenant.where {
            bankAccountNumber == receiptRequest.bankAccountNumber
        }.get([readOnly: true]);

        //Check if the tenant account
        if(tenant == null) {
            receiptRequest.requestStatus = TransactionRequestStatusCodeEnum.INVALID_TENANT_ACCOUNT_NUMBER;
            receiptRequest.requestMessage = receiptRequest.requestStatus.message
            receiptRequest.save();
            return [requestStatus: receiptRequest.requestStatus, receiptRequest: receiptRequest];
        }

        //Check if the amount is valid
        if(transactionApiRequestCommand.amount < 0.0) {
            receiptRequest.requestStatus = TransactionRequestStatusCodeEnum.INVALID_TRANSACTION_AMOUNT
            receiptRequest.requestMessage = receiptRequest.requestStatus.message
            receiptRequest.save();
            return [requestStatus: receiptRequest.requestStatus, receiptRequest: receiptRequest];
        }

        // check if the reference already exists in the database for the receipts
        if(PendingReceipt.countByTransactionReference(receiptRequest.transactionReference)) {
            receiptRequest.requestStatus = TransactionRequestStatusCodeEnum.TRANSACTION_REFERENCE_DUPLICATE
            receiptRequest.requestMessage = "request already exists in Unallocated Receipts for the transactionReference=${receiptRequest.transactionReference}";
            receiptRequest.save();
            return [requestStatus: receiptRequest.requestStatus, receiptRequest: receiptRequest];
        }

        //we are now saving the receipt request object
        receiptRequest.save();

        PendingReceipt pendingReceipt  = new PendingReceipt();
        bindData(pendingReceipt, receiptRequest.properties);
        pendingReceipt.tenant          = tenant;
        pendingReceipt.transactionReference = receiptRequest.transactionReference;
        pendingReceipt.transactionDate = receiptRequest.transactionDate;
        pendingReceipt.receiptAmount   = receiptRequest.amount;
        pendingReceipt.narration       = receiptRequest.narration;
        pendingReceipt.debitAccount    = trustAccount;
        pendingReceipt.save(flush: true);

        return [requestStatus: TransactionRequestStatusCodeEnum.SUCCESS, receiptRequest: receiptRequest];
    }

    @Transactional
    TransactionRequest saveTransaction(TransactionRequest transactionApiRequest) {
        String user = (String) springSecurityService.currentUser.username;

        transactionApiRequest.requestBody = transactionApiRequest.requestBody ?: "TransactionRequest::requestBody()";
        transactionApiRequest.createdBy = user;
        return transactionApiRequest.save(failOnError: true);
    }
}

enum TransactionRequestStatusCodeEnum {

    SUCCESS(10, 'Success'),
    INVALID_TENANT_ACCOUNT_NUMBER(21, 'Invalid tenant account number provided.'),
    INVALID_TRUST_ACCOUNT_NUMBER(22, "Invalid trust account number provided"),
    TRANSACTION_REFERENCE_MISSING(30, 'Transaction reference is missing'),
    TRANSACTION_REFERENCE_DUPLICATE(31, 'Duplicate transaction reference. A transaction with provided reference already exists'),
    INVALID_TRANSACTION_AMOUNT(40, 'Invalid transaction amount provided for selected transaction type'),
    MISSING_TRANSACTION_AMOUNT(41, 'Transaction amount is not specified'),
    INVALID_TRANSACTION_DATE(50, 'Invalid transaction date: not within any open Billing Cycle'),
    INVALID_TRANSACTION_DATE_EXG(51, 'No valid exchange rate exists for the provided parameters'),
    UNKNOWN_ERROR(100, 'Unexpected error occurred')

    final Integer code;
    final String message;
    static final Map map

    static {
        map = [:] as TreeMap
        values().each { statusCodeEnum ->
            map.put(statusCodeEnum.code, statusCodeEnum.message)
        }
    }

    private TransactionRequestStatusCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
