package metasoft.property.api.v1

import com.sun.org.apache.xpath.internal.operations.Bool
import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.core.BankAccountService
import metasoft.property.api.FlexcubeTransaction
import metasoft.property.api.FlexcubeTransactionRequest
import metasoft.property.core.BankAccount
import metasoft.property.core.GlBankAccount
import metasoft.property.core.GlBankAccountService
import metasoft.property.core.MainTransaction
import metasoft.property.core.TransactionBatch
import metasoft.property.core.TransactionType
import wslite.soap.SOAPClient
import wslite.soap.SOAPClientException
import wslite.soap.SOAPFaultException
import wslite.soap.SOAPResponse
import wslite.soap.SOAPVersion

@Slf4j
@GrailsCompileStatic
class FlexcubeTransactionService {

    GrailsApplication grailsApplication
    BankAccountService bankAccountService;
    GlBankAccountService glBankAccountService;

    List<FlexcubeTransaction> pending(Map args){
        return FlexcubeTransaction.where{
            posted == false
        }.list(args);
    };

    List<FlexcubeTransaction> posted(Map args){

        String ref = args['txref'];
        Boolean transPosted = false;

        return FlexcubeTransaction.where{
            posted == transPosted
            if(ref) {transactionReference ==~ "%${ref}%"}

        }.list(args);
    };

    @Transactional
    //@Subscriber("transactionBatch:post")
    void onPostTransactionBatch(TransactionBatch transactionBatch) {
        log.info("starting the posting of the transaction-batch {} to flexcube", transactionBatch.batchNumber);

        if(grailsApplication.config.get('metasoft.app.flexcube.sync.enabled', Boolean.class) == false) {
            log.info("the configuration [metasoft.app.flexcube.sync.enabled] is disabled. No flexcube transactions will be executed.");
            return;
        }

        // create flexcube transactions only for the valid transactions
        TransactionType transactionType = transactionBatch.transactionType;
        // @todo: how to handle reversal (cancellation of payments)
        // Payment, Refund, Landlord Payment, and Cancel transaction types
        if(transactionType.transactionTypeCode in ['PM', 'RF', 'CN', 'LP']) {

            for(MainTransaction mainTransaction: transactionBatch.transactions) {

                if(mainTransaction.parentTransaction) {
                    // handle instance if transaction is a Cancellation.
                    if(transactionType.transactionTypeCode == 'CN') {
                        String originalReference = mainTransaction.transactionReference.substring(0, mainTransaction.transactionReference.lastIndexOf('-'));

                        // If this is a cancellation, check and confirm that we are cancelling either a payment or a receipt
                        MainTransaction cancelledTransaction = MainTransaction.where {
                            transactionReference == originalReference
                            parentTransaction == true
                        }.join('transactionType').get();

                        if( !(cancelledTransaction.transactionType.transactionTypeCode in ['PM', 'RC', 'LP', 'DP']) ) {
                            continue;// if not a receipt or payment cancellation then we skip and exit method
                        }
                    }
                    //@todo: should work to make this configurable and flexible in future.
                    String currency    = "ZWL";
                    String zwlCurrency = "ZWL";

                    String debitAccountBranch  = null;
                    String creditAccountBranch = null;

                    String accountNumberCr  = mainTransaction.accountNumberCr;
                    String accountNumberDb  = mainTransaction.accountNumberDb;
                    String receivingAccount = mainTransaction.accountNumberDb;
                    String receivingBank    = null; // @to be set

                    // @todo: retrieve the bank account number associated with the provided debit accountNumber
                    if( mainTransaction.accountNumberDb.startsWith("100")) {
                        GlBankAccount bankAccountDb = glBankAccountService.getGlBankAccountByAccountNumber(mainTransaction.accountNumberDb);
                        if(bankAccountDb == null) {
                            log.warn("the account [{}] does not have a configured bank account", mainTransaction.accountNumberDb);
                        } else {
                            accountNumberDb  = bankAccountDb.accountNumber;
                            receivingAccount = bankAccountDb.accountNumber;
                            receivingBank = bankAccountDb.bank.bankName;
                            debitAccountBranch = bankAccountDb.branchCode;
                        }
                    } else {
                        BankAccount bankAccountDb = bankAccountService.getBankAccountByAccountNumber(mainTransaction.accountNumberDb);

                        if(bankAccountDb == null) {
                            log.warn("the account [{}] does not have a configured bank account", mainTransaction.accountNumberDb);
                        } else {
                            accountNumberDb  = bankAccountDb.accountNumber;
                            receivingAccount = bankAccountDb.accountNumber;
                            receivingBank = bankAccountDb.bank.bankName;
                            debitAccountBranch = bankAccountDb.branchCode;
                        }
                    }

                    // @todo: retrieve the bank account number associated with the provided credit account number;
                    if( mainTransaction.accountNumberCr.startsWith("100")) {
                        GlBankAccount bankAccountCr = glBankAccountService.getGlBankAccountByAccountNumber(mainTransaction.accountNumberCr);
                        accountNumberCr = bankAccountCr.accountNumber;
                        creditAccountBranch = bankAccountCr.branchCode;
                    } else {
                        BankAccount bankAccountCr = bankAccountService.getBankAccountByAccountNumber(mainTransaction.accountNumberCr);
                        accountNumberCr = bankAccountCr.accountNumber;
                        creditAccountBranch = bankAccountCr.branchCode;
                    }

                    if(!accountNumberDb) {
                        log.warn("The account #${mainTransaction.accountNumberDb} does not have a configured bank account");
                        continue;
                    }

                    if(!accountNumberCr) {
                        log.warn("The account #${mainTransaction.accountNumberCr} does not have a configured bank account");
                        continue;
                    }

                    // handle the reversal and cancellation transaction types.
                    if(transactionType.transactionTypeCode in ['CN', 'RF']) {
                        String tmpAccountNumber = accountNumberDb;
                        accountNumberDb  = accountNumberCr;
                        receivingAccount = accountNumberCr;
                        accountNumberCr  = tmpAccountNumber;
                    }

                    FlexcubeTransaction flexcubeTransaction = new FlexcubeTransaction(
                            mainTransaction: mainTransaction,
                            transactionReference: mainTransaction.transactionReference,
                            transactionDate: mainTransaction.transactionDate,
                            accountNumberCr: accountNumberCr,
                            accountNumberDb: accountNumberDb,
                            transactionAmount: mainTransaction.transactionAmount,
                            transactionDetails: mainTransaction.description,
                            currency: currency,
                            zwlCurrency: zwlCurrency,
                            debitAccountBranch: debitAccountBranch,
                            creditAccountBranch: creditAccountBranch,
                            receivingAccount: receivingAccount,
                            receivingBank: receivingBank,
                            posted: false
                    );

                    // save the object.
                    flexcubeTransaction.save(failOnError: true);
                }
            }
        }
    }

    @Transactional
    FlexcubeTransaction create(FlexcubeTransaction flexcubeTransaction) {
        log.info("creating the flexcube transaction {}", flexcubeTransaction);
        flexcubeTransaction.save(failOnError: true);
        return flexcubeTransaction;
    }

    /**
     * Initiates the posting of all pending flexcube transactions
     *
     */
    void postTransactions() {
        log.info("Initiate the posting of all flexcube transactions to the flexcube web service");

        if(grailsApplication.config.getProperty('metasoft.app.flexcube.sync.enabled', Boolean.class) == false) {
            log.warn("the configuration [metasoft.app.flexcube.sync.enabled] is disabled. No flexcube transactions will be posted.");
            return;
        }

        // retrieve all unposted flexcube transactions.
        List<FlexcubeTransaction> flexcubeTransactions = FlexcubeTransaction.where {
            posted == false
        }.list();

        for (FlexcubeTransaction flexcubeTransaction : flexcubeTransactions) {
            log.info("posting the flexcube transaction request {} to the flexcube web-service", flexcubeTransaction.transactionReference);
            try{
                this.sendTransactionRequest(flexcubeTransaction);
            } catch(ValidationException ve) {
                log.error ve.message;
            }
        }

    }

    @Transactional
    FlexcubeTransaction sendTransactionRequest(FlexcubeTransaction flexcubeTransaction) {

        String apiUrl = grailsApplication.config.get('metasoft.config.cbz.flexcube.apiUrl');

        // prepare hte requestXml
        String xmlRequest = buildRequestXmlBody(flexcubeTransaction);

        // initiate the soap client
        SOAPClient client = new SOAPClient("${apiUrl}?wsdl");
        SOAPResponse response = null;  //prepare the response

        // 1. create the transaction request
        FlexcubeTransactionRequest flexcubeTransactionRequest = new FlexcubeTransactionRequest(
                flexcubeTransaction: flexcubeTransaction,
                xmlRequest: xmlRequest );

        try {
            flexcubeTransactionRequest.save(failOnError: true);

            // make the soap request now.
            log.info("sending transaction request ${flexcubeTransaction.transactionReference} to the flexcube bus.");
            response = client.send(SOAPVersion.V1_2, xmlRequest);

            flexcubeTransactionRequest.xmlResponse = response?.body;
            flexcubeTransactionRequest.responseStatus = response?.httpResponse?.statusCode;
            flexcubeTransactionRequest.save(failOnError: true);

            // update the flexcubeTransaction to posted
            flexcubeTransaction.posted = true;
            flexcubeTransaction.datePosted = new Date();
            flexcubeTransaction.save(failOnError: true);

            log.info("finished sending the flexcube transaction request {}", flexcubeTransaction.transactionReference);
            // do the work to write the response to the request model object

            return flexcubeTransaction;
        } catch (SOAPFaultException sfe) {
            log.error("{} error occured when sending transaction {} with message {}", sfe.class.name, flexcubeTransaction.transactionReference, sfe.message) ;
            throw sfe;
        } catch (SOAPClientException sce) {
            log.error("{} error occured when sending transaction {} with message {}", sce.class.name, flexcubeTransaction.transactionReference, sce.message) ;
            throw sce
        } catch (ValidationException ve) {
            log.error("Validation error occured while trying to send transaction {} to the flexcube bus", flexcubeTransaction.transactionReference);
            log.error ve.message
            throw ve;
        }
    }

    String buildRequestXmlBody(FlexcubeTransaction flexcubeTransaction) {

        // @todo: retrieve this information from a configuration source
        String username = grailsApplication.config.get('metasoft.config.cbz.flexcube.username');
        String password = grailsApplication.config.get('metasoft.config.cbz.flexcube.password');
        String system   = grailsApplication.config.get('metasoft.config.cbz.flexcube.system') ?: "Flexcube" ;
        String transactionType = grailsApplication.config.get('metasoft.config.cbz.flexcube.transactionType');

        // @todo: have these in a dynamic manner.
        String product = "FTPM";
        String currency = "ZWL";

        String receivingAccount = flexcubeTransaction.receivingAccount;   // the receiving account
        String receivingBank = flexcubeTransaction.receivingBank;         //
        String creditAccount = flexcubeTransaction.accountNumberCr;       //
        String debitAccount  = flexcubeTransaction.accountNumberDb;
        String creditAccountBranch = flexcubeTransaction.creditAccountBranch;
        String debitAccountBranch = flexcubeTransaction.debitAccountBranch;
        String transactionAmount = flexcubeTransaction.transactionAmount;

        String requestXml = """
<FCUBS_REQ_ENV><FCUBS_HEADER>
<SOURCE>TASMAN</SOURCE>
<UBSCOMP>FCUBS</UBSCOMP>
<USERID>PROPERTY</USERID>
<BRANCH>${debitAccountBranch}</BRANCH>
<MODULEID>FT</MODULEID>
<SERVICE>FCUBSFTService</SERVICE>
<OPERATION>CreateFTContract</OPERATION>
<FUNCTIONID>FTDCONON</FUNCTIONID>
<ACTION>NEW</ACTION>
<MSGSTAT>SUCCESS</MSGSTAT>
</FCUBS_HEADER><FCUBS_BODY>
<Contract-Details-IO>
<PROD>${product}</PROD>
<BNKOPRCD>CRED</BNKOPRCD>
<DRCCY>${currency}</DRCCY>
<CRCCY>${currency}</CRCCY>
<DRAMT>${transactionAmount}</DRAMT>
<CRAMT>${transactionAmount}</CRAMT>
<DRACCBRN>${debitAccountBranch}</DRACCBRN>
<CRACCBRN>${creditAccountBranch}</CRACCBRN>
<DRACC>${debitAccount}</DRACC>
<CRACC>${creditAccount}</CRACC>
<DRVDT>${flexcubeTransaction.transactionDate.format("yyyy-MM-dd")}</DRVDT>
<CRVDT>${flexcubeTransaction.transactionDate.format("yyyy-MM-dd")}</CRVDT>
<SPRDCD>1</SPRDCD>
<CHGWHOM>U</CHGWHOM>
<MSGASOF>B</MSGASOF>
<RTASOF>B</RTASOF>
<ACTGASOF>M</ACTGASOF>
<AFTRTCHG>N</AFTRTCHG>
<RECIEVER /><ACWTHINST1 />
<REMITMSG>N</REMITMSG>
<PMTDET1>//trf:${flexcubeTransaction.transactionDetails}</PMTDET1>
<ULTBEN1>${receivingAccount}</ULTBEN1>
<ULTBEN2>${receivingBank}</ULTBEN2>
<USRREF>${flexcubeTransaction.transactionReference}</USRREF>
<XRATEDT /><XRATESER />
<Taxdetails><Taxrule-Detail>
<ESN /><TAXAMT />
<TAXCCY>ZWL</TAXCCY>
<VALDT>${flexcubeTransaction.transactionDate.format("yyyy-MM-dd")}</VALDT>
<COMPDT>${flexcubeTransaction.dateCreated.format("yyyy-MM-dd")}</COMPDT>
</Taxrule-Detail></Taxdetails>
<Charge-Claim-Details>
<CHGREF>String</CHGREF>
<CHGCCY>ZWL</CHGCCY><CHGAMT />
</Charge-Claim-Details>
</Contract-Details-IO>
</FCUBS_BODY></FCUBS_REQ_ENV>
""";

        String xmlEnvelope = """<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/">
   <soap:Header/>
   <soap:Body>
      <tem:SubmitTransaction>
         <tem:Username>${username}</tem:Username>
         <tem:Password>${password}</tem:Password>
         <tem:aSystem>${system}</tem:aSystem>
         <tem:TransactionType>${transactionType}</tem:TransactionType>
         <tem:RequestXml>
            <![CDATA[${requestXml}]]>
         </tem:RequestXml>
         <tem:Reference>${flexcubeTransaction.transactionReference}</tem:Reference>
      </tem:SubmitTransaction>
   </soap:Body>
</soap:Envelope>""";

        return xmlEnvelope;
    }

}
