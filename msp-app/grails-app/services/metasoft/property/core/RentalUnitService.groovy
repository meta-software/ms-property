package metasoft.property.core

import grails.gorm.DetachedCriteria
import grails.gorm.transactions.Transactional
import org.hibernate.SQLQuery
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired

class RentalUnitService {

    List<RentalUnit> newRentalUnits() {
        RentalUnit.where {
            property.checkStatus == CheckStatus.APPROVED
        }.list();
    }

    RentalUnit get(Serializable id) {
        return RentalUnit.get(id);
    }

    List<RentalUnit> availableUnits(Map args) {

        def hql = "select * from RentalUnit r where not exists (select * from Lease l where l.rentalUnitId != r.id and l.leaseStatus.code='terminated')"

        def propertyId = args?.propertyId;

        def query = RentalUnit.where{
            property.checkStatus == CheckStatus.APPROVED
            if(propertyId) {
                property.id == propertyId
            }
        }.join('property')
         .join('lease')
         .join('lease.tenant')

        return query.list(args)
    }

    Long getVacantUnitsCountByProperty(Property propertyUnit) {
        def hql = "select count(r) as vacantUnitsCount from RentalUnit r where r.deleted=false and r.unitStatus.code = 'un-occupied' " +
                " and r.property.id = ${propertyUnit.id} " +
                " order by r.name asc" ;

        return RentalUnit.where {
            unitStatus.code == 'un-occupied'
            property == propertyUnit
            deleted == false
        }.count() as Long
    }

    /**
     * Retrieves the list of undeleted rentalUnits for a given property.
     *
     * @param args
     * @return
     */
    List<RentalUnit> list(Map args = [:]) {

        def propertyArg = args?.property;
        def statusArg = args?.status;
        def q = args?.q

        // order by the rentalUnit.name field
        args['order'] = args.get('order','asc');
        args['sort'] = args.get('sort', 'name');

        def query = RentalUnit.where {
            deleted == false
            property.checkStatus == CheckStatus.APPROVED
            if(propertyArg) {property.id == propertyArg}
            if(statusArg) {unitStatus.id == statusArg}

            if(q) {
                ( name ==~ "%${q}%" || unitReference ==~ "%${q}%" ) //|| property.name ==~ "%${q}%")
            }
        }.join('properties');

        return query.list(args)
    }

    /**
     * Retrieves the list of undeleted rentalUnits for a given property.
     *
     * @param args
     * @return
     */
    List<RentalUnit> listByProperty(Long propertyIdArg, Map args = [:]) {

        def q = args?.query
        def isVacant = args?.isVacant

        // order by the rentalUnit.name field
        args['order'] = args.get('order','asc');
        args['sort'] = args.get('sort', 'name');

        DetachedCriteria subQuery = Lease.where {
            setAlias "lease"
            expired == false
            !(leaseStatus.code in ['terminated', 'expired', 'new'])
            eqProperty("rentalUnit.id", "this.id")

            projections {
                property "id"
            }
        }

        def results = RentalUnit.createCriteria().list{
            createAlias('property', 'property')
            eq("property.id", propertyIdArg)
            eq("property.checkStatus", CheckStatus.APPROVED)
            eq("deleted", false)
            if(isVacant == "yes") {
                not {
                    exists(subQuery)
                }
            }
            if(q) {
                or{
                    ilike("name", "%${q}%")
                    ilike("unitReference", "%${q}%")
                }
            }
            order("name")
        } ;

        return results as List<RentalUnit>
    }

    Long count(Map args) {

        def propertyArg = args?.property;
        def statusArg = args?.status;
        def q = args?.q

        return RentalUnit.where {
            deleted == false
            property.checkStatus == CheckStatus.APPROVED
            if(propertyArg) {property.id == propertyArg}
            if(statusArg) {unitStatus.id == statusArg}

            if(q) {
                ( name ==~ "%${q}%" || unitReference ==~ "%${q}%" )
            }
        }.count() as Long;
    }

    List<RentalUnit> listOptions(Map args = [:]) {

        def propertyId = args?.propertyId;
        def q = args?.q

        // order by the rentalUnit.name field
        args['order'] = 'asc';
        args['sort'] = 'name';

        def query = RentalUnit.where {
            deleted == false
            property.checkStatus == CheckStatus.APPROVED
            if(propertyId) {property.id == propertyId}

            if(q) { (name ==~ "%${q}%" || unitReference ==~ "%${q}%") }
        }

        return query.list(args)
    }

    Long count(Property property) {

        def query = RentalUnit.where{
            property == property
        }

        return query.count();
    }

    Long count() {
        return RentalUnit.count();
    }

}
