package metasoft.property.core

import grails.core.GrailsApplication
import grails.events.annotation.Publisher
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import metasoft.property.api.v1.FlexcubeTransactionService
import metasoft.property.core.exceptions.EmptyTransactionBatchException
import org.springframework.validation.BindException

//@GrailsCompileStatic
class TransactionBatchTmpService {

    GrailsApplication grailsApplication
    SpringSecurityService springSecurityService
    TransactionTmpService transactionTmpService
    GeneralLedgerService generalLedgerService
    SubAccountService subAccountService
    ConfigSettingService configSettingService;
    ExchangeRateService exchangeRateService

    TransactionBatchTmp get(Serializable id) {
        return TransactionBatchTmp.get(id);
    }

    List<TransactionBatchTmp> list(Map args) {

        def query = TransactionBatchTmp.where {
            1 == 1
        }

        List<TransactionBatchTmp> mainTransactionTmpList = query.list();

        return mainTransactionTmpList;
    }

    List<TransactionBatchTmp> listPending(Map args){

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        args['posted'] = false;
        String batchNumber = args.get('batchNumber')
        String transactionType = args.get('transactionType')

        return TransactionBatchTmp.where{
            posted == false
            postRequest == false
            createdBy == currentUser.username

            if(batchNumber) {
                batchReference =~ "%${batchNumber}%"
            }

        }.list(args)
    }

    Long count(Map params = [:]) {
        return TransactionBatchTmp.count();
    }

    //@todo: Add a transaction method for each different transaction with the necessary legs as specified.
    @Transactional
    TransactionBatchTmp save(TransactionBatchTmp transactionBatchTmp) {
        transactionBatchTmp.save(failOnError: true)
        return transactionBatchTmp;
    }

    @Transactional
    void delete(Serializable id) {

        TransactionBatchTmp batchTmp = TransactionBatchTmp.get(id);

        TransactionTmp.where{
            transactionBatchTmp == batchTmp
        }.deleteAll();

        batchTmp.delete(flush: true, failOnError: true);
    }

    @Transactional
    TransactionBatchTmp createNewBatch() {
        TransactionBatchTmp transactionBatchTmp = new TransactionBatchTmp();
        SecUser secUser = (SecUser)springSecurityService.currentUser;
        transactionBatchTmp.createdBy = secUser?.username ?: 'system';
        transactionBatchTmp.save(failOnError: true, flush: true)

        // this should be on the event listener
        transactionBatchTmp.batchReference = transactionBatchTmp.batchNumber;
        transactionBatchTmp.save();

        return transactionBatchTmp;
    }

    @Transactional
    TransactionBatchTmp create(TransactionBatchTmp transactionBatchTmp) {
        SecUser secUser = (SecUser)springSecurityService.currentUser;
        transactionBatchTmp.createdBy = secUser?.username ?: "system";
        transactionBatchTmp.save(failOnError: true)

        // this should be on the event listener
        transactionBatchTmp.batchReference = transactionBatchTmp.batchNumber;
        transactionBatchTmp.save(flush: true);

        return transactionBatchTmp;
    }

    @Transactional
    TransactionBatchCheck postRequest(TransactionBatchTmp transactionBatchTmp) {
        // configure if transaction is already loaded
        if(transactionBatchTmp.posted) {
            BindException errors = new BindException(transactionBatchTmp, "transactionBatchTmp")
            errors.rejectValue("posted", "transactionBatchTmp.posted.already", "Transactions Batch with reference# ${transactionBatchTmp.batchNumber} is already posted.")
            def message = "Invalid Action: Transactions Batch reference=${transactionBatchTmp.batchNumber} is already posted."
            throw new ValidationException(message, errors)
        }

        if(transactionBatchTmp.transactionsCount == 0) {
            BindException errors = new BindException(transactionBatchTmp, "transactionBatchTmp")
            errors.rejectValue("transactionTmps", "transactionBatchTmp.transactions.empty", "No transactions defined for batchNumber=${transactionBatchTmp.batchNumber}.")
            def message = "No transactions defined for batchNumber=${transactionBatchTmp.batchNumber}."
            throw new ValidationException(message, errors)
        }

        //Create a new TransactionBatchCheck
        TransactionBatchCheck batchCheck = new TransactionBatchCheck();
        batchCheck.transactionType = transactionBatchTmp.transactionType;
        batchCheck.batchReference = transactionBatchTmp.batchReference;
        batchCheck.maker = springSecurityService.currentUser as SecUser;
        batchCheck.makeDate = new Date();
        batchCheck.entityName = "TRANSACTION_BATCH";
        batchCheck.actionName = "CREATE";
        batchCheck.entityUrl = "#";
        batchCheck.entity;
        batchCheck.transactionSource = transactionBatchTmp;
        def jsonBuilder = toJson(transactionBatchTmp)
        batchCheck.jsonData = "{}" //jsonBuilder.toString()
        batchCheck.save(failOnError: true)   // save the transientValue

        transactionBatchTmp.postRequest = true;
        transactionBatchTmp.postRequestBy = springSecurityService.currentUser;
        transactionBatchTmp.datePostRequest = new Date();

        return batchCheck;
    }

    @Publisher("transactionBatch:post")
    //@Transactional
    TransactionBatch postTransactionBatch(TransactionBatchTmp transactionBatchTmp) {

        // configure if transaction is already loaded
        if(transactionBatchTmp.posted) {
            BindException errors = new BindException(transactionBatchTmp, "transactionBatchTmp")
            errors.rejectValue("posted", "transactionBatchTmp.posted.already", "Transactions Batch with reference# ${transactionBatchTmp.batchNumber} is already posted.")
            def message = "Invalid Action: Transactions Batch reference=${transactionBatchTmp.batchNumber} is already posted."
            throw new ValidationException(message, errors)
        }

        if(transactionBatchTmp.transactionsCount == 0) {
            BindException errors = new BindException(transactionBatchTmp, "transactionBatchTmp")
            errors.rejectValue("transactionTmps", "transactionBatchTmp.transactions.empty", "No transactions defined for batchNumber=${transactionBatchTmp.batchNumber}.")
            def message = "No transactions defined for batchNumber=${transactionBatchTmp.batchNumber}."
            throw new EmptyTransactionBatchException(message, errors)
        }

        //@todo: remove this code, this is just to make something else work
        if(!transactionBatchTmp.attached) {
            transactionBatchTmp.attach();
            transactionBatchTmp.refresh();
        }

        //Create a new TransactionBatch
        TransactionBatch transactionBatch = new TransactionBatch();
        transactionBatch.transactionType = transactionBatchTmp.transactionType;
        transactionBatch.batchNumber = transactionBatchTmp.batchNumber;
        transactionBatch.maker = springSecurityService.currentUser as SecUser;
        transactionBatch.makeDate = new Date();
        transactionBatch.checker = springSecurityService.currentUser as SecUser;
        transactionBatch.dirtMaker = springSecurityService.currentUser as SecUser;
        transactionBatch.checkDate = new Date();
        transactionBatch.checkStatus = CheckStatus.APPROVED;
        transactionBatch.dirtyStatus = CheckStatus.APPROVED;
        transactionBatch.save(failOnError: true); // save the transactionBatch

        //Now we add the transaction items to the transactionBatch, based on the transactionCategory it belongs to.
        for(TransactionTmp transactionTmp : transactionBatchTmp.transactionTmps) {
            TransactionType transactionType = transactionTmp.transactionType;

            switch (transactionType.transactionCategory) {
                /*
                case 'receipt':
                    receipting(transactionTmp, transactionBatch);
                    break;
                 */

                case 'invoice':
                    invoicing(transactionType, transactionTmp, transactionBatch);
                    break;

                case 'payment':
                    payment(transactionType, transactionTmp, transactionBatch);
                    break;

                case 'balance':
                    openingBalance(transactionType, transactionTmp, transactionBatch);
                    break;

                case 'transfer':
                    transfer(transactionType, transactionTmp, transactionBatch);
                    break;

                case 'refund':
                    refund(transactionTmp, transactionBatch);
                    break;

                case 'cancel':
                    cancel(transactionTmp, transactionBatch);
                    break;

                case 'deposit-payment':
                    depositPayment(transactionTmp, transactionBatch);
                    break;

                case 'landlord-payment':
                    landlordPayment(transactionTmp, transactionBatch);
                    break;

                default:
                    processTmpTransaction(transactionTmp, transactionBatch);
                    break;
            }

            //@todo: vat leg can be handled here globally.

        } // end for

        //update the temporary transaction header to posted.
        transactionBatchTmp.posted = true;

        try {
            transactionBatchTmp.save(failOnError: true, flush: true);
        } catch (ValidationException e){
            log.error(e.message, e)
            throw e
        }

        // @todo: clean this code, this is bad code.
        transactionBatch.refresh();
        //@todo: ressuccitate this: flexcubeTransactionService.onPostTransactionBatch(transactionBatch);

        //return the saved transactionBatch
        return transactionBatch;
    }

    /**
     * Processes the temporary transaction legs (OpeningBalance) into the main transaction table
     *
     * @param transactionTmp
     */
    @Transactional
    void openingBalance(TransactionType transactionType, TransactionTmp transactionTmp, TransactionBatch transactionBatch) {

        // @todo: Add logic to retrieve the appropriate control account based on the transactionType
        SubAccount suspenseSubAccount = subAccountService.findByAccount(transactionTmp.subAccount.suspenseAccount);
        GeneralLedger controlAccount = generalLedgerService.findControlAccount(transactionTmp.creditAccount);

        // if ledger or personal account
        if(transactionTmp.creditAccount.startsWith("100")) { // if this is a ledger account
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.parentTransaction = true;
            mainTransaction.transactionBatch = transactionBatch;
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.accountNumberDb = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberDb = '0';
            mainTransaction.subAccountNumberCr = '0';

            if(transactionTmp.amount < 0) {
                mainTransaction.credit = transactionTmp.amount?.abs()
            } else {
                mainTransaction.debit = transactionTmp.amount?.abs();
            }

            //@
            mainTransaction.debitTransactionAmount = mainTransaction.debit;
            mainTransaction.creditTransactionAmount = mainTransaction.credit;
            mainTransaction = applyExchangeRates(mainTransaction, mainTransaction.operatingCurrency, mainTransaction.reportingCurrency);

            mainTransaction.save(failOnError: true)
        } else { // personal account
            //@ leg 1
            log.debug("processing the control part of transaction leg batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.parentTransaction = true;
            mainTransaction.transactionBatch = transactionBatch;
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberCr = suspenseSubAccount.suspenseAccount; // @todo: for the why
            mainTransaction.accountNumberDb = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberDb = '0';

            if(transactionTmp.amount < 0) {
                mainTransaction.debit = transactionTmp.amount?.abs();
            } else {
                mainTransaction.credit = transactionTmp.amount?.abs();
            }
            mainTransaction.debitTransactionAmount = mainTransaction.debit;
            mainTransaction.creditTransactionAmount = mainTransaction.credit;
            mainTransaction = applyExchangeRates(mainTransaction, mainTransaction.operatingCurrency, mainTransaction.reportingCurrency);
            mainTransaction.save(failOnError: true);

            //@ leg 2 do the control leg of the transaction
            log.debug("processing the control part of transaction leg batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            controlTransaction.accountNumberCr = controlAccount.accountNumber;
            controlTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

            // Set the debit account number
            controlTransaction.accountNumberDb = transactionTmp.debitAccount;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            if(transactionTmp.amount < 0) {
                controlTransaction.debit = transactionTmp.amount?.abs();
            } else {
                controlTransaction.credit = transactionTmp.amount?.abs();
            }

            controlTransaction.debitTransactionAmount = controlTransaction.debit;
            controlTransaction.creditTransactionAmount = controlTransaction.credit;
            controlTransaction = applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);
            controlTransaction.save(failOnError: true);

            // perform the suspense logs if the account is a tenant account
            if(transactionTmp.creditAccount.startsWith("001")) {
                //@Leg 3rd suspense 10, amount
                MainTransaction mainTrxSuspense = transactionTmpService.toMainTransaction(transactionTmp);
                mainTrxSuspense.parentTransaction = false;
                mainTrxSuspense.transactionBatch = transactionBatch;
                mainTrxSuspense.accountNumberCr = transactionTmp.creditAccount;
                mainTrxSuspense.subAccountNumberCr = suspenseSubAccount.accountNumber;
                mainTrxSuspense.accountNumberDb = transactionTmp.creditAccount;
                mainTrxSuspense.subAccountNumberDb = '0';

                if(transactionTmp.amount < 0) {
                    mainTrxSuspense.credit = transactionTmp.amount?.abs();
                } else {
                    mainTrxSuspense.debit = transactionTmp.amount?.abs();
                }

                //@
                mainTrxSuspense.debitTransactionAmount = mainTrxSuspense.debit;
                mainTrxSuspense.creditTransactionAmount = mainTrxSuspense.credit;
                mainTrxSuspense = applyExchangeRates(mainTrxSuspense, mainTrxSuspense.operatingCurrency, mainTrxSuspense.reportingCurrency);
                mainTrxSuspense.save(failOnError: true);

                // do the control leg of the transaction
                log.debug("processing the control part of transaction leg batch #${transactionBatch.batchNumber}");

                //@ 4th tenant control sub-zero
                MainTransaction suspenseCtrlTxn = transactionTmpService.toMainTransaction(transactionTmp);
                suspenseCtrlTxn.transactionBatch = transactionBatch;

                // Set the credit account number
                suspenseCtrlTxn.accountNumberCr = controlAccount.accountNumber;
                suspenseCtrlTxn.subAccountNumberCr = suspenseSubAccount.suspenseAccount;

                // Set the debit account number
                suspenseCtrlTxn.accountNumberDb = transactionTmp.debitAccount;
                suspenseCtrlTxn.subAccountNumberDb = '0';

                // Set the credit/debit amounts
                if(transactionTmp.amount < 0) {
                    suspenseCtrlTxn.credit = transactionTmp.amount?.abs();
                } else {
                    suspenseCtrlTxn.debit = transactionTmp.amount?.abs();
                }

                suspenseCtrlTxn.debitTransactionAmount = suspenseCtrlTxn.debit;
                suspenseCtrlTxn.creditTransactionAmount = suspenseCtrlTxn.credit;
                suspenseCtrlTxn = applyExchangeRates(suspenseCtrlTxn, suspenseCtrlTxn.operatingCurrency, suspenseCtrlTxn.reportingCurrency);
                suspenseCtrlTxn.save(failOnError: true);
            }
        }
    }
    /**
     * Generate refund transaction from the supplied reference number.
     *
     * @param transactionTmp
     */

    @Transactional
    void refund(TransactionTmp transactionTmp, TransactionBatch transactionBatch) {
        List<MainTransaction> mainTransactions = MainTransaction.findAllByTransactionReference(transactionTmp.transactionReference);

        for(MainTransaction transaction : mainTransactions) {
            MainTransaction newTransaction = new MainTransaction(transaction.properties)
            newTransaction.transactionDate = transactionTmp.transactionDate;
            newTransaction.transactionType = transactionTmp.transactionType;
            newTransaction.transactionReference += "-RF";
            newTransaction.transactionBatch = transactionBatch;

            BigDecimal tmpValue = newTransaction.credit;
            newTransaction.credit = newTransaction.debit;
            newTransaction.debit = tmpValue;
            newTransaction.save();

            // we ignore this if any changes were effected.
            transaction.refunded = true;
            transaction.dateRefunded = new Date();
            transaction.save(failOnError: true)
        }
    }

    @Transactional
    void cancel(TransactionTmp transactionTmp, TransactionBatch transactionBatch) {
        List<MainTransaction> mainTransactions = MainTransaction.findAllByTransactionReference(transactionTmp.transactionReference);

        for(MainTransaction transaction : mainTransactions) {
            MainTransaction newTransaction = new MainTransaction(transaction.properties)
            newTransaction.transactionDate = transactionTmp.transactionDate;
            newTransaction.transactionType = transactionTmp.transactionType;
            newTransaction.transactionReference += "-CN";
            newTransaction.transactionBatch = transactionBatch;

            BigDecimal tmpValue = newTransaction.credit;
            newTransaction.credit = newTransaction.debit;
            newTransaction.debit = tmpValue;
            newTransaction.save();

            // we ignore this if any changes were effected.
            transaction.cancelled = true;
            transaction.dateCancelled = new Date();
            transaction.save(failOnError: true)
        }
    }

    /**
     * Processes the temporary transaction legs (Internal Transfer) into the main transaction table
     *
     * @param transactionTmp
     */
    @Transactional
    void transfer(TransactionType transactionType, TransactionTmp transactionTmp, TransactionBatch transactionBatch) {

        // @todo: Add logic to retrieve the appropriate control account based on the transactionType
        SubAccount suspenseSubAccount = subAccountService.findByAccount(transactionTmp.subAccount.suspenseAccount);
        SubAccount subAccount = transactionTmp.subAccount;
        GeneralLedger controlAccount = generalLedgerService.findControlAccount(transactionTmp.creditAccount);

        // 1. save the main transaction
        MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        mainTransaction.parentTransaction = true;
        mainTransaction.transactionBatch = transactionBatch;
        mainTransaction.accountNumberCr = transactionTmp.creditAccount;
        mainTransaction.accountNumberDb = transactionTmp.debitAccount;
        mainTransaction.subAccountNumberDb = '0'; // None
        mainTransaction.subAccountNumberCr = '0'; // None
        mainTransaction.credit = transactionTmp.amount?.abs()
        mainTransaction.debit = 0.0

        mainTransaction.debitTransactionAmount = mainTransaction.debit
        mainTransaction.creditTransactionAmount = mainTransaction.credit
        mainTransaction = applyExchangeRates(mainTransaction, mainTransaction.operatingCurrency, mainTransaction.reportingCurrency);

        mainTransaction.save(failOnError: true);

        // 2. save the debit transaction
        MainTransaction debitTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        debitTransaction.parentTransaction = false;
        debitTransaction.transactionBatch = transactionBatch;
        debitTransaction.accountNumberCr = transactionTmp.debitAccount;
        debitTransaction.accountNumberDb = transactionTmp.creditAccount;
        debitTransaction.subAccountNumberDb = '0'; // None
        debitTransaction.subAccountNumberCr = '0'; // None
        debitTransaction.debit = transactionTmp.amount?.abs()
        debitTransaction.credit = 0.0

        debitTransaction.debitTransactionAmount = debitTransaction.debit
        debitTransaction.creditTransactionAmount = debitTransaction.credit
        debitTransaction = applyExchangeRates(debitTransaction, debitTransaction.operatingCurrency, debitTransaction.reportingCurrency);

        debitTransaction.save(failOnError: true);

    }

    /**
     * Processes the temporary transaction legs (Payment) into the main transaction table
     *
     * @param transactionTmp
     */
    void payment(TransactionType transactionType, TransactionTmp transactionTmp, TransactionBatch transactionBatch) {
        Property property = transactionTmp.property;

        // @todo: Add logic to retrieve the appropriate control account based on the transactionType
        SubAccount subAccount = transactionTmp.subAccount;
        GeneralLedger controlAccount = generalLedgerService.findControlAccount(transactionTmp.debitAccount);

        // 1. do the main transaction leg [Ledger leg]
        MainTransaction mainTrans = transactionTmpService.toMainTransaction(transactionTmp);
        mainTrans.parentTransaction = true;
        mainTrans.transactionBatch = transactionBatch;

        // set the credit account number;
        mainTrans.accountNumberCr = transactionTmp.creditAccount;
        mainTrans.subAccountNumberCr = subAccount.accountNumber;

        // set the debit transaction details
        mainTrans.accountNumberDb = transactionTmp.debitAccount;
        mainTrans.subAccountNumberDb = '0'; // None

        // this is a credit transaction
        mainTrans.credit = transactionTmp.amount;
        mainTrans.debit = 0.0;

        mainTrans.debitTransactionAmount = mainTrans.debit
        mainTrans.creditTransactionAmount = mainTrans.credit
        mainTrans = applyExchangeRates(mainTrans, mainTrans.operatingCurrency, mainTrans.reportingCurrency);

        mainTrans.save(failOnError: true);

        // 2. do the debit transaction leg [Personal account leg]
        log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
        MainTransaction debitTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        debitTransaction.transactionBatch = transactionBatch;

        // Set the credit account number
        debitTransaction.accountNumberCr = transactionTmp.debitAccount;
        debitTransaction.subAccountNumberCr = subAccount.suspenseAccount;

        // Set the debit account number
        debitTransaction.accountNumberDb = transactionTmp.creditAccount;
        debitTransaction.subAccountNumberDb = '0';

        // Set the credit/debit amounts
        debitTransaction.debit = transactionTmp.amount;
        debitTransaction.credit = 0.0;

        debitTransaction.debitTransactionAmount = debitTransaction.debit
        debitTransaction.creditTransactionAmount = debitTransaction.credit
        debitTransaction = applyExchangeRates(debitTransaction, debitTransaction.operatingCurrency, debitTransaction.reportingCurrency);

        debitTransaction.save(failOnError: true);

        // 3. configure for the control leg [control leg]
        log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        controlTransaction.transactionBatch = transactionBatch;

        // Set the credit account number
        controlTransaction.accountNumberCr = controlAccount.accountNumber;
        controlTransaction.subAccountNumberCr = subAccount.suspenseAccount;

        // Set the debit account number
        controlTransaction.accountNumberDb = transactionTmp.debitAccount;
        controlTransaction.subAccountNumberDb = '0';    // None

        // Set the credit/debit amounts
        controlTransaction.debit = transactionTmp.amount;
        controlTransaction.credit = 0.0;

        controlTransaction.debitTransactionAmount = controlTransaction.debit
        controlTransaction.creditTransactionAmount = controlTransaction.credit
        controlTransaction = applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);

        controlTransaction.save(failOnError: true);
    }

    /**
     * Processes the temporary transaction legs (DepositPayment) into the main transaction table
     *
     * @param transactionTmp
     */
    void depositPayment(TransactionTmp transactionTmp, TransactionBatch transactionBatch) {

        // @todo: Add logic to retrieve the appropriate control account based on the transactionType
        SubAccount subAccount = transactionTmp.subAccount;
        GeneralLedger controlAccount = generalLedgerService.findControlAccount(transactionTmp.debitAccount);
        SubAccount depositHeldSuspenseSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_HELD_ACCOUNT_NUMBER);
        SubAccount depositChargeSuspenseSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_SUSPENSE_ACCOUNT_NUMBER);

        // 1. do the main transaction leg [Ledger leg]
        MainTransaction mainTrans = transactionTmpService.toMainTransaction(transactionTmp);
        mainTrans.parentTransaction = true;
        mainTrans.transactionBatch = transactionBatch;
        mainTrans.accountNumberCr = transactionTmp.creditAccount;
        mainTrans.subAccountNumberCr = subAccount.accountNumber;
        mainTrans.accountNumberDb = transactionTmp.creditAccount;
        mainTrans.subAccountNumberDb = '0'; // None
        mainTrans.credit = transactionTmp.amount;
        mainTrans.debit = 0.0;
        mainTrans.debitTransactionAmount = mainTrans.debit
        mainTrans.creditTransactionAmount = mainTrans.credit
        mainTrans = applyExchangeRates(mainTrans, mainTrans.operatingCurrency, mainTrans.reportingCurrency);
        mainTrans.save(failOnError: true);

        // 2. do the control transaction leg [Personal account leg]
        //log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
        MainTransaction leg2 = transactionTmpService.toMainTransaction(transactionTmp);
        leg2.transactionBatch = transactionBatch;
        // Set the credit account number
        leg2.accountNumberCr = controlAccount.accountNumber;
        leg2.subAccountNumberCr = subAccount.accountNumber;
        leg2.accountNumberDb = transactionTmp.debitAccount;
        leg2.subAccountNumberDb = '0';
        leg2.credit = transactionTmp.amount;
        leg2.debit = 0.0;
        leg2.debitTransactionAmount = leg2.debit
        leg2.creditTransactionAmount = leg2.credit
        leg2 = applyExchangeRates(leg2, leg2.operatingCurrency, leg2.reportingCurrency);
        leg2.save(failOnError: true);

        // 3. deposit suspense transaction leg
        //log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction leg3 = transactionTmpService.toMainTransaction(transactionTmp);
        leg3.transactionBatch = transactionBatch;
        leg3.accountNumberCr    = transactionTmp.debitAccount;
        leg3.subAccountNumberCr = depositChargeSuspenseSubAccount.accountNumber;
        leg3.accountNumberDb = transactionTmp.debitAccount;
        leg3.subAccountNumberDb = '0';    // None
        leg3.debit = transactionTmp.amount;
        leg3.credit = 0.0;
        leg3.debitTransactionAmount = leg3.debit
        leg3.creditTransactionAmount = leg3.credit
        leg3 = applyExchangeRates(leg3, leg3.operatingCurrency, leg3.reportingCurrency);
        leg3.save(failOnError: true);

        // 4. desposit suspense control leg
        //log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction leg4 = transactionTmpService.toMainTransaction(transactionTmp);
        leg4.transactionBatch = transactionBatch;
        leg4.accountNumberCr = controlAccount.accountNumber;
        leg4.subAccountNumberCr = depositChargeSuspenseSubAccount.accountNumber;
        leg4.accountNumberDb = transactionTmp.debitAccount;
        leg4.subAccountNumberDb = '0';    // None
        leg4.debit = transactionTmp.amount;
        leg4.credit = 0.0;
        leg4.debitTransactionAmount = leg4.debit
        leg4.creditTransactionAmount = leg4.credit
        leg4 = applyExchangeRates(leg4, leg4.operatingCurrency, leg4.reportingCurrency);
        leg4.save(failOnError: true);

        // 5. deposit held transaction leg
        //log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction leg5 = transactionTmpService.toMainTransaction(transactionTmp);
        leg5.transactionBatch = transactionBatch;
        leg5.accountNumberCr = transactionTmp.debitAccount;
        leg5.subAccountNumberCr = depositHeldSuspenseSubAccount.accountNumber;
        leg5.accountNumberDb = transactionTmp.debitAccount;
        leg5.subAccountNumberDb = '0';    // None
        leg5.credit = transactionTmp.amount;
        leg5.debit = 0.0;
        leg5.debitTransactionAmount = leg5.debit
        leg5.creditTransactionAmount = leg5.credit
        leg5 = applyExchangeRates(leg5, leg5.operatingCurrency, leg5.reportingCurrency);
        leg5.save(failOnError: true);

        // 6. control for deposit held transaction leg
        //log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction leg6 = transactionTmpService.toMainTransaction(transactionTmp);
        leg6.transactionBatch = transactionBatch;
        leg6.accountNumberCr = controlAccount.accountNumber;
        leg6.subAccountNumberCr = depositHeldSuspenseSubAccount.accountNumber;
        leg6.accountNumberDb = transactionTmp.debitAccount
        leg6.subAccountNumberDb = '0';    // None
        leg6.credit = transactionTmp.amount;
        leg6.debit = 0.0;
        leg6.debitTransactionAmount = leg6.debit
        leg6.creditTransactionAmount = leg6.credit
        leg6 = applyExchangeRates(leg6, leg6.operatingCurrency, leg6.reportingCurrency);
        leg6.save(failOnError: true);

        // 7. configure for the control leg [bank-leg control]
        log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction leg7 = transactionTmpService.toMainTransaction(transactionTmp);
        leg7.transactionBatch = transactionBatch;
        leg7.accountNumberCr = transactionTmp.bankAccount;
        leg7.subAccountNumberCr = subAccount.accountNumber;
        leg7.accountNumberDb = transactionTmp.creditAccount;
        leg7.subAccountNumberDb = '0';    // None
        leg7.debit = transactionTmp.amount;
        leg7.credit = 0.0;
        leg7.debitTransactionAmount = leg7.debit
        leg7.creditTransactionAmount = leg7.credit
        leg7 = applyExchangeRates(leg7, leg7.operatingCurrency, leg7.reportingCurrency);
        leg7.save(failOnError: true);
    }

    /**
     * Processes the temporary transaction legs (DepositPayment) into the main transaction table
     *
     * @param transactionTmp
     */
    void landlordPayment(TransactionTmp transactionTmp, TransactionBatch transactionBatch) {
        Property property = transactionTmp.property;

        // @todo: Add logic to retrieve the appropriate control account based on the transactionType
        SubAccount subAccount = transactionTmp.subAccount;
        GeneralLedger bankAccount = GeneralLedger.findByAccountNumber(transactionTmp.bankAccount);
        GeneralLedger supplierControl = generalLedgerService.findControlAccount(transactionTmp.creditAccount);
        GeneralLedger landlordControl = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');

        // #1. do the main transaction leg [Ledger leg]
        MainTransaction mainTrans = transactionTmpService.toMainTransaction(transactionTmp);
        mainTrans.parentTransaction = true;
        mainTrans.transactionBatch = transactionBatch;

        // set the credit account number;
        mainTrans.accountNumberCr = bankAccount.accountNumber;
        mainTrans.subAccountNumberCr = subAccount.accountNumber;

        // set the debit transaction details
        mainTrans.accountNumberDb = transactionTmp.creditAccount;
        mainTrans.subAccountNumberDb = '0'; // None

        // this is a credit transaction
        mainTrans.credit = transactionTmp.amount;
        mainTrans.debit = 0.0;
        mainTrans.debitTransactionAmount = mainTrans.debit
        mainTrans.creditTransactionAmount = mainTrans.credit
        mainTrans = applyExchangeRates(mainTrans, mainTrans.operatingCurrency, mainTrans.reportingCurrency);

        mainTrans.save(failOnError: true);

        // #2. do the supplier -> bank leg
        log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
        MainTransaction debitTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        debitTransaction.transactionBatch = transactionBatch;

        // Set the credit account number
        debitTransaction.accountNumberCr = transactionTmp.creditAccount;
        debitTransaction.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        debitTransaction.accountNumberDb = transactionTmp.bankAccount;
        debitTransaction.subAccountNumberDb = '0';

        // Set the credit/debit amounts
        debitTransaction.debit = transactionTmp.amount;
        debitTransaction.credit = 0.0;
        debitTransaction.debitTransactionAmount = debitTransaction.debit
        debitTransaction.creditTransactionAmount = debitTransaction.credit
        debitTransaction = applyExchangeRates(debitTransaction, debitTransaction.operatingCurrency, debitTransaction.reportingCurrency);

        debitTransaction.save(failOnError: true);

        // 3. configure for the supplier->bank leg : control leg [control leg]
        log.debug("processing the control part of landlord leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        controlTransaction.transactionBatch = transactionBatch;

        // Set the credit account number
        controlTransaction.accountNumberCr = supplierControl.accountNumber;
        controlTransaction.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        controlTransaction.accountNumberDb = transactionTmp.creditAccount;
        controlTransaction.subAccountNumberDb = '0';    // None

        // Set the credit/debit amounts
        controlTransaction.debit = transactionTmp.amount;
        controlTransaction.credit = 0.0;
        controlTransaction.debitTransactionAmount = controlTransaction.debit
        controlTransaction.creditTransactionAmount = controlTransaction.credit
        controlTransaction = applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);

        controlTransaction.save(failOnError: true);

        // 4. configure for the
        MainTransaction leg4 = transactionTmpService.toMainTransaction(transactionTmp);
        leg4.transactionBatch = transactionBatch;

        // Set the credit account number
        leg4.accountNumberCr = supplierControl.accountNumber;
        leg4.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        leg4.accountNumberDb = transactionTmp.creditAccount;
        leg4.subAccountNumberDb = '0';    // None

        // Set the credit/debit amounts
        leg4.credit = transactionTmp.amount;
        leg4.debit = 0.0;
        leg4.debitTransactionAmount = leg4.debit
        leg4.creditTransactionAmount = leg4.credit
        leg4 = applyExchangeRates(leg4, leg4.operatingCurrency, leg4.reportingCurrency);

        leg4.save(failOnError: true);

        // 5. configure for the
        MainTransaction leg5 = transactionTmpService.toMainTransaction(transactionTmp);
        leg5.transactionBatch = transactionBatch;

        // Set the credit account number
        leg5.accountNumberCr = transactionTmp.creditAccount;
        leg5.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        leg5.accountNumberDb = transactionTmp.debitAccount;
        leg5.subAccountNumberDb = '0';    // None

        // Set the credit/debit amounts
        leg5.credit = transactionTmp.amount;
        leg5.debit = 0.0;
        leg5.debitTransactionAmount = leg5.debit
        leg5.creditTransactionAmount = leg5.credit
        leg5 = applyExchangeRates(leg5, leg5.operatingCurrency, leg5.reportingCurrency);
        leg5.save(failOnError: true);

        // 6. configure for the
        MainTransaction leg6 = transactionTmpService.toMainTransaction(transactionTmp);
        leg6.transactionBatch = transactionBatch;

        // Set the credit account number
        leg6.accountNumberCr = landlordControl.accountNumber;
        leg6.subAccountNumberCr = '0';

        // Set the debit account number
        leg6.accountNumberDb = transactionTmp.debitAccount;
        leg6.subAccountNumberDb = '0';    // None

        // Set the credit/debit amounts
        leg6.debit = transactionTmp.amount;
        leg6.credit = 0.0;
        leg6.debitTransactionAmount = leg6.debit
        leg6.creditTransactionAmount = leg6.credit
        leg6 = applyExchangeRates(leg6, leg6.operatingCurrency, leg6.reportingCurrency);
        leg6.save(failOnError: true);

        // 7. configure for the
        MainTransaction leg7 = transactionTmpService.toMainTransaction(transactionTmp);
        leg7.transactionBatch = transactionBatch;

        // Set the credit account number
        leg7.accountNumberCr = transactionTmp.debitAccount;
        leg7.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        leg7.accountNumberDb = transactionTmp.creditAccount;
        leg7.subAccountNumberDb = '0';    // None

        // Set the credit/debit amounts
        leg7.debit = transactionTmp.amount;
        leg7.credit = 0.0;
        leg7.debitTransactionAmount = leg7.debit
        leg7.creditTransactionAmount = leg7.credit
        leg7 = applyExchangeRates(leg7, leg7.operatingCurrency, leg7.reportingCurrency);
        leg7.save(failOnError: true);
    }

    MainTransaction applyExchangeRates(MainTransaction mainTransaction, Currency operatingCurrency, Currency reportingCurrency) {
        log.debug("correction, should be temporary until the unused field is removed")

        mainTransaction.debitTransactionAmount = mainTransaction.debit
        mainTransaction.creditTransactionAmount = mainTransaction.credit

        //retrieve the invoice(operating) currency
        mainTransaction.creditOperatingAmount = exchangeRateService.convert(mainTransaction.creditTransactionAmount, mainTransaction.operatingExchangeRate);
        mainTransaction.debitOperatingAmount = exchangeRateService.convert(mainTransaction.debitTransactionAmount, mainTransaction.operatingExchangeRate);
        mainTransaction.vatOperatingAmount = exchangeRateService.convert(mainTransaction.vatTransactionAmount, mainTransaction.operatingExchangeRate);

        //retrieve the reporting currency
        mainTransaction.creditReportingAmount = exchangeRateService.convert(mainTransaction.creditTransactionAmount, mainTransaction.reportingExchangeRate);
        mainTransaction.debitReportingAmount = exchangeRateService.convert(mainTransaction.debitTransactionAmount, mainTransaction.reportingExchangeRate);
        mainTransaction.vatReportingAmount = exchangeRateService.convert(mainTransaction.vatTransactionAmount, mainTransaction.reportingExchangeRate);

        return mainTransaction;
    }



    /**
     * Processes the temporary transaction legs (Invoicing) into the main transaction table
     *
     * @param transactionTmp
     */
    void invoicing(TransactionType transactionType, TransactionTmp transactionTmp, TransactionBatch transactionBatch) {
        Property property = transactionTmp.property;

        GeneralLedger tenantTrustAccount = GeneralLedger.findByAccountName('LESSEES TRUST CONTROL');
        SubAccount suspenseSubAccount = subAccountService.findByAccount(transactionTmp.subAccount.suspenseAccount);
        SubAccount subAccount = transactionTmp.subAccount;

        BigDecimal vatPercentage = new BigDecimal(configSettingService.getValue("vat.global.rate", transactionTmp.transactionDate));
        BigDecimal vat = vatPercentage/100.00;

        // do the tenant leg of the transaction
        if(transactionTmp.creditAccount.startsWith("001")) {
            // 1. process main transaction (suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.parentTransaction = true;
            mainTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            mainTransaction.accountNumberDb = transactionTmp.debitAccount;
            mainTransaction.subAccountNumberDb = '0'; // None

            // Set the credit/debit amounts
            mainTransaction.debit = transactionTmp.amount;
            mainTransaction.credit = 0.0;

            mainTransaction.debitTransactionAmount = mainTransaction.debit
            mainTransaction.creditTransactionAmount = mainTransaction.credit

            // 1.1 Perform VAT tax component legs, if property is configured to be commercial
            if(subAccount.hasVat && property?.chargeVat) {
                mainTransaction.vat = vat;
                mainTransaction.vatAmount = vat * mainTransaction.transactionAmount
                mainTransaction.vatTransactionAmount = mainTransaction.vatAmount
                this.vatInvoice(transactionTmp, transactionBatch);
            }

            //now sort out the currencies/exchange-rates issue
            mainTransaction = applyExchangeRates(mainTransaction, mainTransaction.operatingCurrency, mainTransaction.reportingCurrency);
            mainTransaction.save(failOnError: true);

            // 2.
            if(subAccount.hasControl) {
                log.info("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number
                controlTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
                controlTransaction.subAccountNumberCr = subAccount.accountNumber;

                // Set the debit account number
                controlTransaction.accountNumberDb = transactionTmp.creditAccount;
                controlTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

                // Set the credit/debit amounts
                controlTransaction.debit = transactionTmp.amount;
                controlTransaction.credit = 0.0;

                controlTransaction.debitTransactionAmount = controlTransaction.debit
                controlTransaction.creditTransactionAmount = controlTransaction.credit
                controlTransaction = applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);

                controlTransaction.save(failOnError: true);
            }
        }

        // do the tenant leg of the transaction (Suspense leg)
        if(transactionTmp.creditAccount.startsWith("001")) {
            // 1. process main transaction (suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

            // Set the debit account number
            mainTransaction.accountNumberDb = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            mainTransaction.credit = transactionTmp.amount;
            mainTransaction.debit = 0.0;

            mainTransaction.debitTransactionAmount = mainTransaction.debit
            mainTransaction.creditTransactionAmount = mainTransaction.credit
            mainTransaction = applyExchangeRates(mainTransaction, mainTransaction.operatingCurrency, mainTransaction.reportingCurrency);

            mainTransaction.save(failOnError: true);

            // 2.
            if(subAccount.hasControl) {
                log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number
                controlTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
                controlTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

                // Set the debit account number
                controlTransaction.accountNumberDb = transactionTmp.creditAccount;
                controlTransaction.subAccountNumberDb = '0';

                // Set the credit/debit amounts
                controlTransaction.credit = transactionTmp.amount;
                controlTransaction.debit = 0.0;
                controlTransaction.debitTransactionAmount  = controlTransaction.debit
                controlTransaction.creditTransactionAmount = controlTransaction.credit
                controlTransaction = applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);

                controlTransaction.save(failOnError: true);
            }
        }

    }

    @Transactional
    void vatInvoice(TransactionTmp transactionTmp, TransactionBatch transactionBatch) {

        SubAccount vatSubAccount = SubAccount.findByAccountName('VAT on Rent')
        GeneralLedger tenantControlAccount = generalLedgerService.findControlAccount(transactionTmp.creditAccount);

        BigDecimal vatPercentage = new BigDecimal(configSettingService.getValue("vat.global.rate", transactionTmp.transactionDate));
        BigDecimal vat = vatPercentage/100.00; // @todo: configure this and extract from the database

        BigDecimal vatAmount = transactionTmp.amount * vat;

        // 1. first leg of the VAT Transaction
        log.info("generate the VAT transaction legs for transaction ref=#${transactionTmp.transactionReference}");
        MainTransaction vatLeg1 = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg1.transactionBatch = transactionBatch;
        vatLeg1.accountNumberCr = transactionTmp.creditAccount;
        vatLeg1.accountNumberDb = transactionTmp.creditAccount;
        vatLeg1.subAccountNumberCr = vatSubAccount.accountNumber;
        vatLeg1.subAccountNumberDb = '0';
        vatLeg1.debit = vatAmount;
        vatLeg1.credit = 0.0;

        vatLeg1.debitTransactionAmount = vatLeg1.debit
        vatLeg1.creditTransactionAmount = vatLeg1.credit
        vatLeg1 = applyExchangeRates(vatLeg1, vatLeg1.operatingCurrency, vatLeg1.reportingCurrency);

        vatLeg1.save(failOnError: true);

        // 2. VAT control leg transaction
        MainTransaction vatLeg2 = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg2.transactionBatch = transactionBatch;
        vatLeg2.accountNumberCr = tenantControlAccount.accountNumber;
        vatLeg2.accountNumberDb = transactionTmp.creditAccount;
        vatLeg2.subAccountNumberCr = vatSubAccount.accountNumber;
        vatLeg2.subAccountNumberDb = '0';
        vatLeg2.debit = vatAmount;
        vatLeg2.credit = 0.0;

        vatLeg2.debitTransactionAmount = vatLeg2.debit
        vatLeg2.creditTransactionAmount = vatLeg2.credit
        vatLeg2 = applyExchangeRates(vatLeg2, vatLeg2.operatingCurrency, vatLeg2.reportingCurrency);

        vatLeg2.save(failOnError: true);

        // 3. suspense transaction of the VAT Transaction
        MainTransaction vatLeg3 = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg3.transactionBatch = transactionBatch;
        vatLeg3.accountNumberCr = transactionTmp.creditAccount;
        vatLeg3.accountNumberDb = transactionTmp.creditAccount;
        vatLeg3.subAccountNumberCr = vatSubAccount.suspenseAccount;
        vatLeg3.subAccountNumberDb = '0';
        vatLeg3.debit  = 0.0;
        vatLeg3.credit = vatAmount;

        vatLeg3.debitTransactionAmount = vatLeg3.debit
        vatLeg3.creditTransactionAmount = vatLeg3.credit
        vatLeg3 = applyExchangeRates(vatLeg3, vatLeg3.operatingCurrency, vatLeg3.reportingCurrency);

        vatLeg3.save(failOnError: true);

        // 3. suspense transaction of the VAT Transaction
        MainTransaction vatLeg4 = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg4.transactionBatch = transactionBatch;
        vatLeg4.accountNumberCr = tenantControlAccount.accountNumber;
        vatLeg4.subAccountNumberCr = vatSubAccount.suspenseAccount;
        vatLeg4.accountNumberDb = transactionTmp.creditAccount;
        vatLeg4.subAccountNumberDb = '0';
        vatLeg4.debit  = 0.0;
        vatLeg4.credit = vatAmount;

        vatLeg4.debitTransactionAmount = vatLeg4.debit
        vatLeg4.creditTransactionAmount = vatLeg4.credit
        vatLeg4 = applyExchangeRates(vatLeg4, vatLeg4.operatingCurrency, vatLeg4.reportingCurrency);

        vatLeg4.save(failOnError: true);
    }

    @Transactional
    void vatReceipt(TransactionTmp transactionTmp, TransactionBatch transactionBatch, Landlord landlord, BigDecimal commissionAmount) {
        log.info("perform the receipt vat legs for the transactionRef = #${transactionTmp.transactionReference}");

        SubAccount vatRentSubAccount = SubAccount.findByAccountName('VAT on Rent')
        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMMISSION');

        BigDecimal vatPercentage = new BigDecimal(configSettingService.getValue("vat.global.rate", transactionTmp.transactionDate));
        BigDecimal vat = vatPercentage/(vatPercentage + 100);
        BigDecimal vatAmount = transactionTmp.amount * vat;

        // 1. first leg of the VAT Transaction
        log.info("generate the VAT transaction legs for transactionRef=#${transactionTmp.transactionReference}");
        MainTransaction vatLeg1 = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg1.transactionBatch = transactionBatch;
        vatLeg1.accountNumberCr = landlord.accountNumber;
        vatLeg1.accountNumberDb = commissionLedger.accountNumber;
        vatLeg1.subAccountNumberCr = vatRentSubAccount.suspenseAccount;
        vatLeg1.subAccountNumberDb = '0';
        vatLeg1.debit = vatAmount;
        vatLeg1.credit = 0.0;
        vatLeg1.save(failOnError: true);

        // 2. VAT control leg transaction
        MainTransaction vatLeg2 = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg2.transactionBatch = transactionBatch;
        vatLeg2.accountNumberCr = landlordTrustAccount.accountNumber;
        vatLeg2.accountNumberDb = landlord.accountNumber
        vatLeg2.subAccountNumberCr = vatRentSubAccount.suspenseAccount;
        vatLeg2.subAccountNumberDb = '0';
        vatLeg2.debit = vatAmount;
        vatLeg2.credit = 0.0;
        vatLeg2.save(failOnError: true);

        GeneralLedger vatLedger = GeneralLedger.findByAccountName('VAT RENT ACCOUNT');
        // 3. suspense transaction of the VAT Transaction
        MainTransaction vatLeg3  = transactionTmpService.toMainTransaction(transactionTmp);
        vatLeg3.transactionBatch = transactionBatch;
        vatLeg3.accountNumberCr = vatLedger.accountNumber;
        vatLeg3.accountNumberDb = landlord.accountNumber;
        vatLeg3.subAccountNumberCr = vatRentSubAccount.accountNumber;
        vatLeg3.subAccountNumberDb = '0';
        vatLeg3.debit  = 0.0;
        vatLeg3.credit = vatAmount;
        vatLeg3.save(failOnError: true);
    }

    @Transactional
    void vatCommission(TransactionTmp transactionTmp, TransactionBatch transactionBatch, Landlord landlord, BigDecimal commissionAmount) {

        log.info("perform the vat on commission legs for the transactionRef = #${transactionTmp.transactionReference}");

        SubAccount vatCommissionSubAccount = SubAccount.findByAccountName('VAT on Commission')
        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMMISSION'); //retrieve the commission general ledger account details        SubAccount vatRentSubAccount = SubAccount.findByAccountName('VAT on Rent')

        BigDecimal vatPercentage = new BigDecimal(configSettingService.getValue("vat.global.rate", transactionTmp.transactionDate));
        BigDecimal vat = vatPercentage/(vatPercentage + 100.0);

        BigDecimal vatCommissionAmount = commissionAmount * vat;

        // Need to do the second part of the vat transaction legs. [the VAT on commission legs]
        // 1. first leg of the VAT Transaction
        log.info("generate the VAT transaction legs for transaction ref=#${transactionTmp.transactionReference}");
        MainTransaction vatCommissionLeg1 = transactionTmpService.toMainTransaction(transactionTmp);
        vatCommissionLeg1.transactionBatch = transactionBatch;
        vatCommissionLeg1.accountNumberCr = landlord.accountNumber;
        vatCommissionLeg1.accountNumberDb = commissionLedger.accountNumber;
        vatCommissionLeg1.subAccountNumberCr = vatCommissionSubAccount.suspenseAccount;
        vatCommissionLeg1.subAccountNumberDb = '0';
        vatCommissionLeg1.debit = vatCommissionAmount;
        vatCommissionLeg1.credit = 0.0;
        vatCommissionLeg1.save(failOnError: true);

        // 2. VAT control leg transaction
        MainTransaction vatCommissionLeg2 = transactionTmpService.toMainTransaction(transactionTmp);
        vatCommissionLeg2.transactionBatch = transactionBatch;
        vatCommissionLeg2.accountNumberCr = landlordTrustAccount.accountNumber;
        vatCommissionLeg2.accountNumberDb = landlord.accountNumber
        vatCommissionLeg2.subAccountNumberCr = vatCommissionSubAccount.suspenseAccount;
        vatCommissionLeg2.subAccountNumberDb = '0';
        vatCommissionLeg2.debit = vatCommissionAmount;
        vatCommissionLeg2.credit = 0.0;
        vatCommissionLeg2.save(failOnError: true);

        GeneralLedger vatCommissionLedger = GeneralLedger.findByAccountName('VAT COMMISSION ACCOUNT');
        // 3. suspense transaction of the VAT Transaction
        MainTransaction vatCommissionLeg3  = transactionTmpService.toMainTransaction(transactionTmp);
        vatCommissionLeg3.transactionBatch = transactionBatch;
        vatCommissionLeg3.accountNumberCr = vatCommissionLedger.accountNumber;
        vatCommissionLeg3.accountNumberDb = landlord.accountNumber;
        vatCommissionLeg3.subAccountNumberCr = vatCommissionSubAccount.accountNumber;
        vatCommissionLeg3.subAccountNumberDb = '0';
        vatCommissionLeg3.debit  = 0.0;
        vatCommissionLeg3.credit = vatCommissionAmount;
        vatCommissionLeg3.save(failOnError: true);
    }

    /**
     * Processes the temporary transaction legs (Receipting) into the main transaction table
     *
     * @param transactionTmp
     */
    void receipting(TransactionTmp transactionTmp, TransactionBatch transactionBatch) {
        Property property = transactionTmp.property;
        Landlord landlord = property.landlord;

        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger tenantTrustAccount = GeneralLedger.findByAccountName('LESSEES TRUST CONTROL');
        SubAccount subAccount = transactionTmp.subAccount;
        SubAccount suspenseSubAccount = subAccountService.findByAccount(subAccount.suspenseAccount);

        BigDecimal commissionAmount = 0.0;

        //leg 1: the tenant transaction leg
        log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
        MainTransaction tenantTransaction = transactionTmpService.toMainTransaction(transactionTmp);
        tenantTransaction.parentTransaction = true;
        tenantTransaction.transactionBatch = transactionBatch;

        // Set the credit account number
        tenantTransaction.accountNumberCr = transactionTmp.creditAccount;
        tenantTransaction.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        tenantTransaction.accountNumberDb = transactionTmp.debitAccount;
        tenantTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

        // Set the credit/debit amounts
        tenantTransaction.debit  = 0.0;
        tenantTransaction.credit = transactionTmp.amount;
        tenantTransaction.save(failOnError: true);

        //leg 2: the tenant control leg
        log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction tenantControlLeg = transactionTmpService.toMainTransaction(transactionTmp);
        tenantControlLeg.transactionBatch = transactionBatch;

        // Set the credit account number
        tenantControlLeg.accountNumberCr = tenantTrustAccount.accountNumber;
        tenantControlLeg.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        tenantControlLeg.accountNumberDb = transactionTmp.creditAccount;
        tenantControlLeg.subAccountNumberDb = '0'; //@todo: need to add logic here.

        // Set the credit/debit amounts
        tenantControlLeg.debit  = 0.0;
        tenantControlLeg.credit = transactionTmp.amount;
        tenantControlLeg.save(failOnError: true);

        //START: The commission legs for the sub account. 3 LEGS
        if(subAccount.hasCommission) {
            commissionAmount = transactionTmp.amount * property.commission/100.0;
            GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMMISSION'); //retrieve the commission general ledger account details

            // 1. do the main part of the commission leg
            log.debug("processing the main part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction transaction = transactionTmpService.toMainTransaction(transactionTmp);
            transaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            transaction.accountNumberCr = commissionLedger.accountNumber;
            transaction.subAccountNumberCr = commissionLedger.subAccount.accountNumber;

            // Set the debit account number
            transaction.accountNumberDb = landlord.accountNumber;
            transaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            transaction.credit = commissionAmount;
            transaction.debit = 0.0;
            transaction.save(failOnError: true)

            // 2. do the suspense leg of the commission
            log.debug("processing the control part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlSuspenseTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            controlSuspenseTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission control leg
            controlSuspenseTransaction.accountNumberCr = landlord.accountNumber;
            controlSuspenseTransaction.subAccountNumberCr = commissionLedger.subAccount.suspenseAccount;

            // Set the debit account number
            controlSuspenseTransaction.accountNumberDb = commissionLedger.accountNumber;
            controlSuspenseTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlSuspenseTransaction.debit = commissionAmount ;
            controlSuspenseTransaction.credit = 0.0;
            controlSuspenseTransaction.save(failOnError: true)

            // 3. do the control part of the commission
            log.debug("processing the control part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission control leg
            controlTransaction.accountNumberCr = landlordTrustAccount.accountNumber;
            controlTransaction.subAccountNumberCr = '0';

            // Set the debit account number
            controlTransaction.accountNumberDb = landlord.accountNumber;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlTransaction.debit = commissionAmount ;
            controlTransaction.credit = 0.0;
            controlTransaction.save(failOnError: true)

            // perform the vat on commission legs
            if(commissionAmount > 0 ) {
                this.vatCommission(transactionTmp, transactionBatch, landlord, commissionAmount);
            }

        }
        //END: Finish the 3 commission legs of the transaction

        // configure if the transaction has ledger (bank, etc.., known transaction type)
        if(subAccount.hasLedger) {
            // 1. do the main part of the ledger leg
            log.debug("processing the main part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

            // Set the debit account number
            mainTransaction.accountNumberDb = landlord.accountNumber;
            mainTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            mainTransaction.credit = 0.0;
            mainTransaction.debit  = transactionTmp.amount;
            mainTransaction.save(failOnError: true);

            // 2. add logic for the control leg
            log.debug("processing the control part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            controlTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
            controlTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

            // Set the debit account number
            controlTransaction.accountNumberDb = transactionTmp.creditAccount;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlTransaction.credit = 0.0;
            controlTransaction.debit = transactionTmp.amount;
            controlTransaction.save(failOnError: true);

            // 3. the alternate transaction for double entry purposes
            log.debug("processing the main(alternate) part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction2 = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction2.transactionBatch = transactionBatch;

            // Set the
            mainTransaction2.accountNumberCr = transactionTmp.debitAccount;
            mainTransaction2.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            mainTransaction2.accountNumberDb = transactionTmp.creditAccount;
            mainTransaction2.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            mainTransaction2.debit = transactionTmp.amount;
            mainTransaction2.credit = 0.0;
            mainTransaction2.save(failOnError: true);
        }

        //START: do the landlord legs of the transaction
        if(landlord?.accountNumber != null && subAccount.accountName != 'Deposit') {
            // 1. process main transaction (suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction landlordTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            landlordTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            landlordTransaction.accountNumberCr = landlord.accountNumber;
            landlordTransaction.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            landlordTransaction.accountNumberDb = transactionTmp.creditAccount;
            landlordTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            landlordTransaction.debit = 0.0;
            landlordTransaction.credit = transactionTmp.amount;
            landlordTransaction.save(failOnError: true);

            // 2. the landlord control-account leg
            log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            controlTransaction.accountNumberCr = landlordTrustAccount.accountNumber;
            controlTransaction.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            controlTransaction.accountNumberDb = landlord.accountNumber;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlTransaction.credit = transactionTmp.amount;
            controlTransaction.debit = 0.0;
            controlTransaction.save(failOnError: true);
        }
        //END: finish the landlord legs of the transaction

        //START: do the deposit Charge specific transaction legs
        if(subAccount.accountName == 'Deposit') {
            SubAccount depositHeldSuspenseSubAccount = SubAccount.findByAccountName('Deposit Held Suspense');

            // 1. process main transaction (deposit charge suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction depositChargeSuspenseTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            depositChargeSuspenseTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            depositChargeSuspenseTransaction.accountNumberCr = transactionTmp.creditAccount;
            depositChargeSuspenseTransaction.subAccountNumberCr = depositHeldSuspenseSubAccount.accountNumber;

            // Set the debit account number
            depositChargeSuspenseTransaction.accountNumberDb = transactionTmp.creditAccount;
            depositChargeSuspenseTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            depositChargeSuspenseTransaction.debit = 0.0;
            depositChargeSuspenseTransaction.credit = transactionTmp.amount;
            depositChargeSuspenseTransaction.save(failOnError: true);

            // 2. process the deposit held suspense transaction leg
            log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction depositHeldSuspenseTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            depositHeldSuspenseTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            depositHeldSuspenseTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
            depositHeldSuspenseTransaction.subAccountNumberCr = depositHeldSuspenseSubAccount.accountNumber;

            // Set the debit account number
            depositHeldSuspenseTransaction.accountNumberDb = transactionTmp.creditAccount;
            depositHeldSuspenseTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            depositHeldSuspenseTransaction.debit = 0.0;
            depositHeldSuspenseTransaction.credit = transactionTmp.amount;
            depositHeldSuspenseTransaction.save(failOnError: true);
        }
        //END: finish the deposit Charge specific transaction legs

        if(subAccount.hasVat && property?.chargeVat) {
            //@ todo: Add VAT application logic here, or rather add a method to help with that.
            this.vatReceipt(transactionTmp, transactionBatch, landlord, commissionAmount);
        }
    }

    /**
     * Processes the temporary transaction legs into the main transaction table
     *
     * @param transactionTmp
     */
    void processTmpTransaction(TransactionTmp transactionTmp, TransactionBatch transactionBatch){

        if(true) {
            throw new IllegalStateException("uimplemented transaction type ");
        }
        
        TransactionType transactionType = transactionTmp.transactionType;
        Property property = transactionTmp.property;
        Landlord landlord = property.landlord;

        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger tenantTrustAccount = GeneralLedger.findByAccountName('LESSEES TRUST CONTROL');
        SubAccount suspenseSubAccount = SubAccount.findByAccountNumber(transactionType.suspenseAccount);

        // configure if the transaction commission
        if(transactionType.commission) {
            BigDecimal commissionAmount = transactionTmp.amount * property.commission/100.0;
            //retrieve the commission general ledger account details
            GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMM EARNED-PROP MNGMNT');

            // 1. do the main part of the commission leg
            log.debug("processing the main part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction transaction = transactionTmpService.toMainTransaction(transactionTmp);
            transaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            transaction.accountNumberCr = commissionLedger.accountNumber;
            transaction.subAccountNumberCr = commissionLedger.subAccount.accountNumber;

            // Set the debit account number
            transaction.accountNumberDb = landlord.accountNumber;
            transaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            transaction.credit = commissionAmount;
            transaction.debit = 0.0;
            transaction.save(failOnError: true)

            // 2. do the suspense leg of the commission
            if(transactionType.suspense) {
                log.debug("processing the control part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number for commission control leg
                controlTransaction.accountNumberCr = landlord.accountNumber;
                controlTransaction.subAccountNumberCr = commissionLedger.subAccount.suspenseAccount;

                // Set the debit account number
                controlTransaction.accountNumberDb = commissionLedger.accountNumber;
                controlTransaction.subAccountNumberDb = '0';

                // Set the credit/debit amounts
                controlTransaction.debit = commissionAmount ;
                controlTransaction.credit = 0.0;
                controlTransaction.save(failOnError: true)
            }
            // 3. do the control part of the commission
            if(transactionType.control) {
                log.debug("processing the control part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number for commission control leg
                controlTransaction.accountNumberCr = landlordTrustAccount.accountNumber;
                controlTransaction.subAccountNumberCr = '0';

                // Set the debit account number
                controlTransaction.accountNumberDb = landlord.accountNumber;
                controlTransaction.subAccountNumberDb = '0';

                // Set the credit/debit amounts
                controlTransaction.debit = commissionAmount ;
                controlTransaction.credit = 0.0;
                controlTransaction.save(failOnError: true)
            }
        }

        // configure if the transaction has ledger (bank, etc.., known transaction type)
        if(transactionType.ledger) {
            // 1. do the main part of the ledger leg
            log.debug("processing the main part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

            // Set the debit account number
            mainTransaction.accountNumberDb = transactionTmp.debitAccount;
            mainTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            mainTransaction.credit = transactionTmp.amount;
            mainTransaction.debit = 0.0;
            mainTransaction.save(failOnError: true);

            // 2. the alternate transaction for double entry purposes
            log.debug("processing the main(alternate) part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction2 = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction2.transactionBatch = transactionBatch;

            // Set the
            mainTransaction2.accountNumberCr = transactionTmp.debitAccount;
            mainTransaction2.subAccountNumberCr = '0'; //@todo: need to add logic here.

            // Set the debit account number
            mainTransaction2.accountNumberDb = transactionTmp.creditAccount;
            mainTransaction2.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            mainTransaction2.debit = transactionTmp.amount;
            mainTransaction2.credit = 0.0;
            mainTransaction2.save(failOnError: true);

            // 3. add logic for the control leg
            if(transactionType.control) {
                log.debug("processing the control part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number for commission
                controlTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
                controlTransaction.subAccountNumberCr = '0'; //@todo: need to add logic here

                // Set the debit account number
                controlTransaction.accountNumberDb = transactionTmp.creditAccount;
                controlTransaction.subAccountNumberDb = '0';

                // Set the credit/debit amounts
                controlTransaction.credit = transactionTmp.amount;
                controlTransaction.debit = 0.0;
                controlTransaction.save(failOnError: true);
            }
        }

        // do the tenant leg of the transaction
        if(transactionTmp.creditAccount.startsWith("001")) {
            // 1. process main transaction (suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            mainTransaction.accountNumberCr = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberCr = suspenseSubAccount.suspenseAccount;

            // Set the debit account number
            mainTransaction.accountNumberDb = landlord.accountNumber;
            mainTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            mainTransaction.debit = transactionTmp.amount;
            mainTransaction.credit = 0.0;
            mainTransaction.save(failOnError: true);

            // 2.
            if(transactionType.control) {
                log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number
                controlTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
                controlTransaction.subAccountNumberCr = '0'; //@todo: need to add logic here.

                // Set the debit account number
                controlTransaction.accountNumberDb = transactionTmp.creditAccount;
                controlTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

                // Set the credit/debit amounts
                controlTransaction.debit = transactionTmp.amount;
                controlTransaction.credit = 0.0;
                controlTransaction.save(failOnError: true);
            }
        }

        // do the landlord leg of the transaction
        if(landlord?.accountNumber != null ) {
            // 1. process main transaction (suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction = transactionTmpService.toMainTransaction(transactionTmp);
            mainTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            mainTransaction.accountNumberCr = landlord.accountNumber;
            mainTransaction.subAccountNumberCr = '0';

            // Set the debit account number
            mainTransaction.accountNumberDb = transactionTmp.creditAccount;
            mainTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            mainTransaction.debit = 0.0;
            mainTransaction.credit = transactionTmp.amount;
            mainTransaction.save(failOnError: true);

            // 2.
            if(transactionType.control) {
                log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction controlTransaction = transactionTmpService.toMainTransaction(transactionTmp);
                controlTransaction.transactionBatch = transactionBatch;

                // Set the credit account number
                controlTransaction.accountNumberCr = landlordTrustAccount.accountNumber;
                controlTransaction.subAccountNumberCr = '0';

                // Set the debit account number
                controlTransaction.accountNumberDb = landlord.accountNumber;
                controlTransaction.subAccountNumberDb = '0';

                // Set the credit/debit amounts
                controlTransaction.credit = transactionTmp.amount;
                controlTransaction.debit = 0.0;
                controlTransaction.save(failOnError: true);
            }
        }
    }

    JsonBuilder toJson(TransactionBatchTmp transactionBatchTmp) {
        def jsonBuilder = new JsonBuilder();

        def root = jsonBuilder {
            id transactionBatchTmp.id
            transactionType {
                id transactionBatchTmp.transactionType.id
            }
            batchReference transactionBatchTmp.batchReference
            createdBy transactionBatchTmp.createdBy
            transactionTmps (transactionBatchTmp.transactionTmps) { TransactionTmp transaction ->
                id transaction.id
                transactionType {
                    id transaction.transactionType.id
                }
                entryNumber transaction.entryNumber
                transactionDate transaction.transactionDate
                creditAccount transaction.creditAccount
                debitAccount transaction.debitAccount
                creditSubAccount transaction.creditSubAccount
                debitSubAccount transaction.debitSubAccount
                amount transaction.amount
                transactionReference transaction.transactionReference
                description transaction.description
                subAccount {
                    id transaction.subAccount.id
                }
                if(transaction.lease){
                    lease {
                        id transaction.lease?.id
                    }
                }
                if (transaction.rentalUnit) {
                    rentalUnit {
                        id transaction.rentalUnit.id
                    }

                    property {
                        id transaction.property?.id
                    }
                }
                billingCycle {
                    id transaction.billingCycle?.id
                }
            }
        };

        return jsonBuilder;
    }

}

class TransactionBatchEmptyException extends RuntimeException {

    TransactionBatchEmptyException(String message) {
        super(message)
    }
}
