package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService

class PropertyManagerCheckService {

    SpringSecurityService springSecurityService;

    PropertyManagerCheck get(Serializable id) {
        PropertyManagerCheck.get(id)
    }

    List<PropertyManagerCheck> list(Map args) {
        PropertyManagerCheck.list(args);
    }

    Long count() {
        PropertyManagerCheck.count();
    }

    @Transactional
    PropertyManagerCheck approve(PropertyManagerCheck propertyManagerCheck) {
        propertyManagerCheck.checkStatus = CheckStatus.APPROVED;
        propertyManagerCheck.checker = springSecurityService.currentUser as SecUser;
        propertyManagerCheck.checkDate = new Date();
        propertyManagerCheck.checkComment = "Approved";
        propertyManagerCheck.save(failOnError: true);

        PropertyManager propertyManager = propertyManagerCheck.entity;
        propertyManager.setProperties(propertyManagerCheck.jsonData);
        propertyManager.save(failOnError: true);

        return propertyManagerCheck;
    }

    PropertyManagerCheck reject(PropertyManagerCheck propertyManagerCheck) {

    }

}