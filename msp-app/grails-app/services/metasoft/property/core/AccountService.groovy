package metasoft.property.core

import grails.events.EventPublisher
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.mapping.LinkGenerator
import groovy.json.JsonBuilder
import groovy.util.logging.Slf4j
import org.apache.commons.lang.NotImplementedException
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class AccountService implements EventPublisher {

    @Autowired
    LinkGenerator grailsLinkGenerator
    SpringSecurityService springSecurityService;

    //@todo: bad code alert
    TenantService tenantService;
    ProviderService providerService;
    LandlordService landlordService
    AccountCheckService accountCheckService

    Account get(Serializable id) {
        return Account.get(id);
    }

    @Transactional
    Account create(Account account, Map args) {

        // prepare the maker-checker attributes
        account.maker = springSecurityService.currentUser as SecUser;
        account.makeDate = new Date();
        save(account);

        // create the url
        String url = grailsLinkGenerator.link(controller: 'account', action: 'show', id: account.id, absolute: false);

        // generate the maker-checker
        AccountCheck accountCheck = new AccountCheck(
            accountCategory: account.accountCategory,
            entity: account,
            entityName: "ACCOUNT",
            actionName: "CREATE",
            maker: springSecurityService.currentUser as SecUser,
            entityUrl: url
        );

        //args = [:]
        accountCheck.jsonData = new JsonBuilder(args).toString();
        accountCheck.makeDate = new Date();

        try{
            accountCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the check method")
            println(e.message)
            throw e
        }

        return account;
    }
    
    @Transactional
    Account update(Account account, Map args) {

        // create the url
        String url = grailsLinkGenerator.link(controller: 'account', action: 'show', id: account.id, absolute: false);

        // generate the maker-checker
        AccountCheck accountCheck = getAccountCheck(account);
        if(accountCheck == null ) {
            accountCheck = new AccountCheck()
        }
        accountCheck.accountCategory = account.accountCategory;
        accountCheck.entity = account;
        accountCheck.entityName = "ACCOUNT";
        accountCheck.actionName = "UPDATE";
        accountCheck.maker = springSecurityService.currentUser as SecUser;
        accountCheck.entityUrl = url;
        accountCheck.jsonData = getUpdateMap(account, args).toString(); //new JsonBuilder(args).toString();
        accountCheck.makeDate = new Date();

        // prepare the maker-checker attributes
        account = Account.get(account.id);
        account.dirtMaker = springSecurityService.currentUser as SecUser;
        account.dirtyStatus = CheckStatus.EDITING;
        account.save();

        try{
            accountCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the check method")
            println(e.message)
            throw e
        }

        return account;
    }

    String getUpdateMap(Account account, Map args) {

        Map bankAccount = args['bankAccount']
        args.remove('bankAccount');

        account.setProperties(args);    //@todo: update to user bindData from the web*** class
        List<String> dirtyPropertyNames = account.getDirtyPropertyNames();
        Map dirtyProperties = [:]

        //@todo: bad code, always dirty the bank
        if(bankAccount) {
            dirtyProperties['bankAccount'] = bankAccount;
        }

        dirtyPropertyNames.each { dirtyProperty ->
            dirtyProperties[dirtyProperty] = args[dirtyProperty];
        }

        account.discard();
        return new JsonBuilder(dirtyProperties).toString();
    }

    String entityLinkGenerator(Account account) {

        String controller = 'account'; // just in case

        if(account instanceof Tenant) {
            controller = 'tenant'
        } else if (account instanceof Provider) {
            controller = 'provider'
        } else if (account instanceof Landlord) {
            controller = 'landlord'
        }

        return grailsLinkGenerator.link(controller: controller, action: 'show', id: account.id, absolute: false);
    }

    @Transactional
    Account save(Account account) {

        if(account.accountNumber == null) {
            account.accountNumber = accountNumberGenerator(account);
        }

        // save the entity
        account.save(failOnError: true)

        return account
    }

    /**
     * Retrieve the control account-number of the parameter account number
     *
     * @param accountNumber
     * @return
     */
    String getControlAccountNumber(String accountNumber) {

        log.info("retrieve the controlAccount for accountNumber=${accountNumber}")
        String controlAccount = null;

        if(accountNumber?.startsWith("001")) { //if accountNumber is tenant range
            controlAccount = "2"
        } else if (accountNumber?.startsWith("002")) { //if accountNumber is landlord range
            controlAccount = "3"
        } else if (accountNumber?.startsWith("005")) { //if accountNumber is trust creditor range
            controlAccount = "5"
        }

        return controlAccount
    }

    /**
     * For use by the rest-api for selection of accounts during transaction creation
     *
     * @param args
     * @return
     */
    List listAllAccounts(Map args) {
        List queryResults = []

        if(args?.glAccount == "1") {
            String hql = "select id, accountNumber, accountName from GeneralLedger where 1=1"

            if(args?.q) {
                hql += " and (accountName like '%${args.q}%' or accountNumber like '${args?.q}%' )"
            }

            hql += " order by accountNumber asc"
            queryResults = GeneralLedger.executeQuery(hql, [], [offset: 0, max: 8])
        } else { // We want non gl-accounts
            String hql = "select a.id, a.accountNumber, a.accountName from Account a where 1=1"
            hql += " and a.checkStatus = '${CheckStatus.APPROVED}'"

            if(args?.q) {
                hql += " and (a.accountName like'%${args.q}%' or a.accountNumber like '%${args?.q}%' )"
            }

            if( args.get('ac') in ['tenant', 'landlord', 'provider','supplier'] ) {
                if(args.get('ac') == 'supplier')
                    args.put('ac', 'provider');

                hql += " and a.accountCategory.code ='${args.ac}'"
                println(hql);

            }

            if(args?.ac == 'tenant') { // t for tenant only
                hql += " and a.accountCategory.code ='tenant'"
            }

            hql += " order by accountNumber asc"
            queryResults = Account.executeQuery(hql, [], [offset: 0, max: 8])
        }

        List returnResults = []
        queryResults.each { row ->
            returnResults << [objectId: row[0], text: "${row[1]} : ${row[2]}", id: row[0], accountNumber: row[1]]
        }

        return returnResults

    }

    /**
     * For use by the rest-api for selection of accounts during transaction creation (select-box)
     *
     * @param args
     * @return
     */
    List<Account> listOptions(Map args) {

        // filter arguments
        String accountCategoryArg = args?.cat;
        String q = args?.q;

        //
        args['offset'] = 0;
        args['max'] = 10;
        args['order'] = 'asc';
        args['sort'] = 'accountNumber';

        List<Account> accounts = Account.where{
            checkStatus == CheckStatus.APPROVED
            if(accountCategoryArg) {
                accountCategory.code == accountCategoryArg
            }
            if(q){
                (accountName ==~ "%${q}%" || accountNumber ==~ "%${q}%")
            }

        }.list(args);

        return accounts;
    }

    String accountNumberGenerator(Account account) {
        String accountNumber = "N/A";

        if(account instanceof Tenant) {
            accountNumber = tenantService.accountNumberGenerator();
        } else if (account instanceof Provider) {
            accountNumber = providerService.accountNumberGenerator();
        } else if (account instanceof Landlord) {
            accountNumber = landlordService.accountNumberGenerator();
        }

        return accountNumber;
    }

    String accountNumberGenerator() {
        throw new NotImplementedException("method accountNumberGenerator not implemented for ${Account.class} object")
    }

    /**
     * Check and verify if an account number is a personal Account or not
     *
     * returns true if supplied account number is a personal Account and false if it is not a personal account
     *
     * @param accountNumber
     */
    Boolean isPersonalAccount(String accountNumber) {

        if (accountNumber?.startsWith("00")) { // if accountNumber starts with 00 then account is personal.
            return true
        }

        // return false if accountNumber is not a personal account.
        return false;
    }

    @Transactional
    Account delete(Serializable id) {

        Account account = Account.get(id);

        if(!account?.terminated) {
            account.terminated = true;
            account.save(failOnError: true);
        }

        return account;
    }

    @Transactional
    Account terminate(Serializable id) {
        Account account = Account.get(id);

        if(!account?.terminated) {
            account.terminated = true;
            account.save(failOnError: true);
        }

        return account;
    }

    Account getForEdit(Long id) {
        Account account = Account.read(id);

        AccountCheck accountCheck = getAccountCheck(account);

        if(accountCheck) {
            return accountCheckService.getEntity(accountCheck);
        }

        return account;
    }

    AccountCheck getAccountCheck(Account account) {
        List<AccountCheck> results = (List<AccountCheck>)AccountCheck.executeQuery("select a from AccountCheck a where entity.id=:entityId and checkStatus in (:statuses)",
                [entityId: account.id, statuses: [CheckStatus.PENDING, CheckStatus.NEW]], [max: 1]);

        if(results) {
            return results[0];
        }
        return null;
    }
}