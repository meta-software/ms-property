package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.LeaseTerminateRequestCommand
import org.springframework.validation.BindException

@Slf4j
class LeaseTerminateRequestService {

    MakerCheckerService  makerCheckerService;
    SpringSecurityService springSecurityService;
    LeaseService leaseService;
    LeaseTerminateRequestCheckService leaseTerminateRequestCheckService;

    LeaseTerminateRequest get(Serializable id) {
        return LeaseTerminateRequest.get(id)
    }

    List<LeaseTerminateRequest> listByLease(Long leaseIdArg, Map args) {

        List <LeaseTerminateRequest> leaseTerminateRequestList = LeaseTerminateRequest.where {
            lease.id == leaseIdArg
            checkStatus == CheckStatus.APPROVED
        }.list(args)

        return leaseTerminateRequestList;
    }

    List<LeaseTerminateRequest> list(Map args) {

        return LeaseTerminateRequest.where {
            checkStatus == CheckStatus.APPROVED
        }.list(args)
    }

    Long count() {
        return LeaseTerminateRequest.count();
    }

    @Transactional
    LeaseTerminateRequest save(LeaseTerminateRequest leaseTerminateRequest) {

        Long leaseId = leaseTerminateRequest.lease.id
        //configure if a pending request already exists for the specified lease

        String hql = "select count(*) from LeaseTerminateRequest ltr where makerChecker.status='pending' or (makerChecker.status='approved' and processed=false)"
        Boolean leaseTerminateRequestExists = LeaseTerminateRequest.executeQuery(hql)[0] > 0;


        if(leaseTerminateRequestExists) {
            Object[] errorArgs = []
            println(errorArgs)
            BindException errors = new BindException(leaseTerminateRequest, leaseTerminateRequest.class.name);
            errors.rejectValue('lease', 'leaseTerminateRequest.lease.exists', errorArgs,"A pending termination request already exists for the lease")
            def ve = new ValidationException("A pending termination request already exists for the lease", errors);
            println("errors: ${ve.errors}")
            throw ve
        }

        leaseTerminateRequest.save(failOnError: true);
    }

    @Transactional
    def updateMakerChecker(LeaseTerminateRequest leaseTerminateRequest) {
        makerCheckerService.update(leaseTerminateRequest);

        if(leaseTerminateRequest.makerChecker.status == 'approved') {
            leaseTerminateRequest.makerChecker.comment = leaseTerminateRequest.makerChecker.comment ?: 'Approved';
        }

        leaseTerminateRequest.save(failOnError: true);
        return leaseTerminateRequest;
    }

    def processRequests() {
        Date today = new Date().clearTime();

        List<LeaseTerminateRequest> leaseTerminateRequests = LeaseTerminateRequest.where {
            checkStatus == CheckStatus.APPROVED
            processed == false
            actionDate == today
        }.list();

        leaseTerminateRequests.each { LeaseTerminateRequest terminateRequest ->
            this.processRequest(terminateRequest);
        }
    }

    @Transactional
    def processRequest(LeaseTerminateRequest leaseTerminateRequest) {
        Lease lease = leaseTerminateRequest.lease;
        log.info("processing terminating the lease #$lease.leaseNumber");

        try{
            leaseService.terminateLease(lease);

            leaseTerminateRequest.processed = true;
            leaseTerminateRequest.processStatus = 'success';
            leaseTerminateRequest.dateProcessed = new Date();
            leaseTerminateRequest.processResponse = 'Processed';
            leaseTerminateRequest.save(failOnError: true);
            log.info("finished terminating the lease #$lease.leaseNumber");
        } catch (ValidationException e) {
            log.error("error occured while trying to terminate the lease #${lease.leaseNumber}");
            log.error(e.message, e);

            leaseTerminateRequest.processed = true;
            leaseTerminateRequest.processStatus = 'error';
            leaseTerminateRequest.processResponse = e.getMessage();
            leaseTerminateRequest.save(failOnError: true)
        }
    }

    @Transactional
    LeaseTerminateRequestTmp createRequest(LeaseTerminateRequestCommand command) {
        // perform checks to see if the request is acceptable
        LeaseTerminateRequestTmp leaseTerminateRequestTmp = new LeaseTerminateRequestTmp(command.properties);
        //leaseTerminateRequestTmp.requestedBy = springSecurityService.currentUser as SecUser;
        leaseTerminateRequestTmp.userAction = UserAction.CREATE;

        // also create a routineRequestCheck entity the one to be used for the maker/checking management
        leaseTerminateRequestCheckService.createCheckRecord(leaseTerminateRequestTmp);

        return leaseTerminateRequestTmp.save(failOnError: true);
    }

}