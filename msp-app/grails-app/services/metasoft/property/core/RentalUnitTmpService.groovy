package metasoft.property.core

import grails.gorm.transactions.Transactional

class RentalUnitTmpService {

    RentalUnitTmp get(Serializable id) {
        return RentalUnitTmp.get(id);
    }

    List<RentalUnitTmp> list(Map args) {
        def propertyId = args['propertyId']

        args['sort'] = ['name': 'desc']

        return RentalUnitTmp.where{
            property.id == propertyId
        }.list(args);
    }

    Long count() {
        return RentalUnitTmp.count();
    }

    @Transactional
    void delete(RentalUnitTmp rentalUnitTmp) {

        if(rentalUnitTmp.rentalUnit) {
            rentalUnitTmp.deleted = true;
            rentalUnitTmp.save(failOnError: true);
        }
        else {
            rentalUnitTmp.delete(flush:true);
        }
    }

    @Transactional
    RentalUnitTmp save(RentalUnitTmp rentalUnitTmp) {
        return rentalUnitTmp.save(failOnError: true);
    }

}