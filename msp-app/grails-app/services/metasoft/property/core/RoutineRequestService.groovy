package metasoft.property.core

import grails.async.Promise
import grails.async.Promises
import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
//@GrailsCompileStatic
class RoutineRequestService {

    SpringSecurityService springSecurityService;
    TransactionTmpService transactionTmpService
    TransactionBatchTmpService transactionBatchTmpService
    BillingCycleService billingCycleService;
    UserService userService;
    RoutineRequestCheckService routineRequestCheckService
    ConfigSettingService configSettingService;
    BillingInvoiceService billingInvoiceService;
    CurrencyService currencyService

    @Autowired
    GrailsApplication grailsApplication;

    def dataSource;

    RoutineRequest get(Serializable id) {
        return RoutineRequest.get(id);
    }

    List<RoutineRequest> list(Map args) {
        args['sort'] = ['id': 'desc']
        return RoutineRequest.list(args);
    }

    Long count(def params = [:]) {
        return RoutineRequest.count();
    }

    @Transactional
    RoutineRequest cancel(Serializable id) {
        RoutineRequest routineRequest = RoutineRequest.get(id);

        if (routineRequest.requestStatus != 'pending') {
            log.info("routine request ${routineRequest.id} cannot be cancelled, has already started")
            throw new RuntimeException("Routine Request has already started running")
        }

        // cancel the request
        routineRequest.requestStatus = 'canceled';
        routineRequest.cancelled = true;
        routineRequest.cancelledBy = springSecurityService.currentUser as SecUser;
        routineRequest.save(failOnError: true)

        return routineRequest;
    }

    @Transactional
    RoutineRequest save(RoutineRequest routineRequest) {
        routineRequest.requestedBy = springSecurityService.currentUser as SecUser;
        return routineRequest.save(failOnError: true);
    }

    @Transactional
    RoutineRequestTmp createRequest(RoutineRequestTmp routineRequestTmp) {
        // perform checks to see if the request is acceptable
        routineRequestTmp.requestedBy = springSecurityService.currentUser as SecUser;
        routineRequestTmp.userAction = UserAction.CREATE;
        // also create a routineRequestCheck entity the one to be used for the maker/checking management
        routineRequestCheckService.createCheckRecord(routineRequestTmp);

        return routineRequestTmp.save(failOnError: true);
    }

    void processRequests() {
        log.debug("start processing the ::processRequests method for configured routineTypes")

        Date today = new Date().clearTime() + 1;

        // take list of all currently pending requests
        List<RoutineRequest> routineRequests = RoutineRequest.where {
            requestStatus == 'pending'
            processed == false
            cancelled == false
            runDate < today
        }.join('billingCycle').list();

        for (RoutineRequest routineRequest : routineRequests) {
            if (routineRequest.requestStatus != 'pending') {
                log.warn("the request #[${routineRequest.id}] is not longer applicable for executing, status is now [${routineRequest.requestStatus}]");
                continue
            }

            if (!routineRequest.billingCycle.open) {
                log.error("the billing cycle [${routineRequest.billingCycle.name}] is closed. Transactions cannot be processed on a closed cycle");
                continue;
            }

            //Otherwise we continue execute the request, set the requestStatus to running
            RoutineRequest.withTransaction {
                routineRequest.attach();
                routineRequest.requestStatus = 'running';
                routineRequest.startTime = new Date();
                routineRequest.save(failOnError: true, flush: true);
            }
            try {
                switch (routineRequest.routineType.name) {
                    case 'Rent Run':
                        rentRun(routineRequest);
                        break;
                    case 'Interest Run':
                        interestRun(routineRequest)
                        break;
                    case 'OpCos Run':
                        opCosRun(routineRequest)
                        break;
                }

                // if we get to here, the routine ran successfully
                RoutineRequest.withTransaction {
                    routineRequest.attach();
                    routineRequest.requestStatus = 'success';
                    routineRequest.endTime = new Date();
                    routineRequest.save(failOnError: true)
                }

            } catch (RuntimeException e) {
                log.error("exception during the processing of routine requests message=[{}]", e.message);

                // routine rate but with errors.
                RoutineRequest.withTransaction {
                    routineRequest.attach();
                    routineRequest.requestStatus = 'error';
                    routineRequest.endTime = new Date();
                    routineRequest.save(failOnError: true)
                }

                e.printStackTrace();
            }
        }
    }

    /**
     * Prepares the rent-due-run for the current billing cycle
     *
     * The methods requests the routineRequest for rent and also for the op-cos run
     */
    void executeRentDueRun() {

        // Prepare the RentRun
        RoutineType rentRunRoutineType = RoutineType.findByName("Rent Run", [readOnly: true]);
        executeRoutine(rentRunRoutineType);

        // Prepare the OpCosRun
        RoutineType opCosRoutineType = RoutineType.findByName("OpCos Run", [readOnly: true]);
        executeRoutine(opCosRoutineType);

        Promise p = Promises.task {
            BillingInvoice.withNewSession {
                billingInvoiceService.prepareBillingInvoicesAuto();

                billingInvoiceService.emailInvoicesAuto();
            }
        }

        p.onComplete {}

        p.onError {}

        //@todo: create a notification, of successful finishing of rent-due-run
    }

    /**
     * Creates a routine request
     *
     * @return RoutineRequest
     */
    void executeRoutine(RoutineType routineType) {
        routineType.discard(); //we do not need to persist this record.

        Date valueDate = new Date().clearTime();
        Date today = new Date().clearTime();

        // the automatic routine works only within the current month
        BillingCycle currentCycle = billingCycleService.getCurrentCycle();

        //@todo: this is probably wrong
        valueDate = currentCycle.startDate;

        log.info("start executing the routine-type [{}] for the billing-cycle [{}]", routineType.name, currentCycle.name);

        RoutineRequest routineRequest = null;
        try {
            RoutineRequest.withTransaction {
                //start by creating the request first.
                routineRequest = new RoutineRequest(
                        routineType: routineType,
                        billingCycle: currentCycle,
                        runDate: today,
                        valueDate: valueDate,
                        emailAlert: false,
                        smsAlert: false,
                        processed: false,
                        requestStatus: 'running',
                        requestedBy: userService.getSystemUser(),
                );

                routineRequest.save(failOnError: true);
            }
        } catch (ValidationException ve) {
            log.error("validation error occured when trying to create RoutineRequest for the RoutineType:[${routineType.name}]:[${ve.message}]");
            return;
        }

        try {
            switch (routineType.name) {
                case 'Rent Run':
                    rentRun(routineRequest);
                    break;
                case 'Interest Run':
                    interestRun(routineRequest)
                    break;
                case 'OpCos Run':
                    opCosRun(routineRequest)
                    break;
            }

            // if we get to here, the routine ran successfully
            RoutineRequest.withTransaction {
                routineRequest.attach();
                routineRequest.requestStatus = 'success';
                routineRequest.endTime = new Date();
                routineRequest.save(failOnError: true)
            }
        } catch (ValidationException ve) {
            log.error("validation error occured while executing routineRequest=[{}], message=[{}]", routineType.name, ve.message);
            //@todo: add application exception handling code here.
        }
    }

    @Transactional
    void rentRun(RoutineRequest routineRequest) {
        log.info("running the rent-run routine for request #${routineRequest.id}, for billingCycle=${routineRequest.billingCycle} @${new Date().format('yyyy-MM-dd HH:mm:ss')}");

        BillingCycle billingCycle = routineRequest.billingCycle;

        SubAccount rentSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.SA_RENT_CHARGE);

        def leases = Lease.where {
            deleted == false
            terminated == false
            checkStatus == CheckStatus.APPROVED
            (leaseStartDate <= billingCycle.startDate || leaseStartDate < billingCycle.endDate)
            (leaseExpiryDate >= billingCycle.endDate || leaseExpiryDate > billingCycle.startDate)
        }.id()

        // get a list of all the applicable (active) leaseItems for the supplied billingCycle
        def leaseItems = LeaseItem.where {
            lease in leases
            startDate <= billingCycle.startDate
            endDate >= billingCycle.endDate
            subAccount.id == rentSubAccount.id
            //subAccount.suspenseAccount == rentSuspenseAccount.accountNumber
        }.join('lease').list();

        // now prepare the temporary transaction section
        TransactionBatchTmp transactionBatchTmp = new TransactionBatchTmp()
        transactionBatchTmp.transactionType = TransactionType.findByTransactionTypeCode('IV');
        // this is an invoice batch being created.
        transactionBatchTmp.autoCommitted = true;

        // prepare the batch transactions
        Long counter = 0;

        //@this is hte generic application reporting currency
        Currency reportingCurrency = currencyService.getReportingCurrency();

        for (LeaseItem leaseItem : leaseItems) {
            String dateTimeString = new Date().format("HHmmssSS");

            TransactionTmp transactionTmp = new TransactionTmp();
            transactionTmp.transactionDate = new Date();
            transactionTmp.creditAccount = leaseItem.lease.tenant.accountNumber;
            transactionTmp.debitAccount = leaseItem.lease.tenant.accountNumber;
            transactionTmp.amount = leaseItem.amount;
            transactionTmp.transactionAmount = leaseItem.amount;
            transactionTmp.transactionReference = String.format("RR.%s.%s.%06d", billingCycle.name, dateTimeString, ++counter);
            transactionTmp.description = "Rent for ${billingCycle.billingCycleName}"
            transactionTmp.subAccount = rentSubAccount;
            transactionTmp.transactionBatchTmp = transactionBatchTmp;

            //Extra fields to add clarity to transaction data
            transactionTmp.lease = leaseItem.lease;
            transactionTmp.rentalUnit = leaseItem.lease.rentalUnit;
            transactionTmp.property = leaseItem.lease.property;
            //Property property;
            transactionTmp.billingCycle = billingCycle;
            transactionTmp.transactionType = transactionBatchTmp.transactionType;
            transactionTmp.transactionDate = routineRequest.valueDate;

            //update the currencies
            Lease lease = leaseItem.lease;
            lease.discard()
            transactionTmp.transactionCurrency = lease.leaseCurrency
            transactionTmp = transactionTmpService.transactionCurrencies(transactionTmp, lease.invoicingCurrency, reportingCurrency)
            transactionBatchTmp.addToTransactionTmps(transactionTmp);
            counter++;
        }

        transactionBatchTmp.save(failOnError: true);

        // now we post the transaction.
        TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(transactionBatchTmp);
        routineRequest.transactionBatch = transactionBatch;
        routineRequest.save(failOnError: true);
    }

    @Transactional
    void opCosRun(RoutineRequest routineRequest) {
        log.info("running the opCos-run routine for request #[${routineRequest.id}], for billingCycle=[${routineRequest.billingCycle}] @${new Date().format('yyyy-MM-dd HH:mm:ss')}");
        BillingCycle billingCycle = routineRequest.billingCycle;

        SubAccount rentChargeSubAccount = SubAccount.findByAccountName('Rent');

        def leases = Lease.where {
            deleted == false
            terminated == false
            checkStatus == CheckStatus.APPROVED
            lease.leaseStatus.code == 'active'
            (leaseStartDate <= billingCycle.startDate || leaseStartDate < billingCycle.endDate)
            (leaseExpiryDate >= billingCycle.endDate || leaseExpiryDate > billingCycle.startDate)
        }.id()

        // get a list of all the applicable (active) leaseItems for the supplied billingCycle
        def leaseItems = LeaseItem.where {
            lease in leases
            startDate <= billingCycle.startDate
            endDate >= billingCycle.endDate
            subAccount.accountNumber != rentChargeSubAccount.accountNumber
        }.join('lease').list();

        if (leaseItems.size() == 0) {
            log.warn("No lease-items were found for the opcos-run for requestId = [${routineRequest.id}] with billingCycle = [${billingCycle.name}] ");
            return;
        }

        // now prepare the temporary transaction section
        TransactionBatchTmp transactionBatchTmp = new TransactionBatchTmp()
        transactionBatchTmp.transactionType = TransactionType.findByTransactionTypeCode('IV');
        // this is an invoice batch being created.
        transactionBatchTmp.autoCommitted = true;

        // prepare the batch transactions
        Long counter = 0;

        //@this is hte generic application reporting currency
        Currency reportingCurrency = currencyService.getReportingCurrency();

        for (LeaseItem leaseItem : leaseItems) {
            String dateTimeString = new Date().format("HHmmssSS");

            TransactionTmp transactionTmp = new TransactionTmp()
            transactionTmp.transactionDate = new Date();
            transactionTmp.creditAccount = leaseItem.lease.tenant.accountNumber;
            transactionTmp.debitAccount = leaseItem.lease.tenant.accountNumber;
            transactionTmp.amount = leaseItem.amount;
            transactionTmp.transactionAmount = leaseItem.amount;
            transactionTmp.transactionReference = String.format("OR.%s.%s.%07d", billingCycle.name, dateTimeString, ++counter);
            transactionTmp.description = "OPCOS for ${billingCycle.billingCycleName}"
            transactionTmp.subAccount = leaseItem.subAccount;
            transactionTmp.transactionBatchTmp = transactionBatchTmp

            //Extra fields to add clarity to transaction data
            transactionTmp.lease = leaseItem.lease;
            transactionTmp.rentalUnit = leaseItem.lease.rentalUnit;
            transactionTmp.property = leaseItem.lease.property;
            //Property property;
            transactionTmp.billingCycle = billingCycle;
            transactionTmp.transactionType = transactionBatchTmp.transactionType;
            transactionTmp.transactionDate = routineRequest.valueDate;

            //update the currencies
            Lease lease = leaseItem.lease;
            lease.discard()
            transactionTmp.transactionCurrency = lease.leaseCurrency
            transactionTmp = transactionTmpService.transactionCurrencies(transactionTmp, lease.invoicingCurrency, reportingCurrency)

            transactionBatchTmp.addToTransactionTmps(transactionTmp)
        }

        TransactionBatchTmp.withTransaction {
            transactionBatchTmp.save(failOnError: true);
        }

        // now we post the transaction.
        TransactionBatchTmp.withTransaction {
            routineRequest.attach();
            TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(transactionBatchTmp);
        }
    }

    List<MainTransaction> getRoutineTransactions(RoutineRequest routineRequest) {

        Map args = [:] //['sort': ['id': 'desc']]

        List<MainTransaction> transactions = MainTransaction.where {
            transactionBatch == routineRequest.transactionBatch
            parentTransaction == true
        }.list(args);

        return transactions;
    }

    @Transactional
    void interestRun(RoutineRequest routineRequest) {
        log.info("running the interest-run routine for request #${routineRequest.id}, for billingCycle=${routineRequest.billingCycle} @${new Date().format('yyyy-MM-dd HH:mm:ss')}");
        BillingCycle billingCycle = routineRequest.billingCycle;
        SubAccount subAccount = SubAccount.findByAccountName('Interest');

        BigDecimal interestRate = new BigDecimal(configSettingService.getValue("interest.global.rate", billingCycle.startDate));
        log.info("the annual interestRate being utilised: [{}%] ", interestRate)
        interestRate = (interestRate / 100.00) / 12.0; //per annum interest

        // 1. prepare a query which extracts all tenant accounts with balances.
        Sql sql = new Sql(dataSource);
        String hql = """
            SELECT 
                account_number_cr, 
                lease_id,
                sum(credit_reporting_amount-debit_reporting_amount) balance 
            FROM 
                ms_transaction A 
            INNER JOIN
                ms_account c on A.account_number_cr=c.account_number
            INNER JOIN  
                account_category g on g.id = c.account_category_id
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE 
                g.code = 'tenant'
                AND S.account_number not in (4, 12, 13)
                AND S.suspense = 0
                AND A.transaction_date < :limitDate
            GROUP BY 
                account_number_cr,
                lease_id
            HAVING 
                SUM(credit_reporting_amount-debit_reporting_amount) > 0
                """;

        List<GroovyRowResult> results = sql.rows(hql, [limitDate: routineRequest.valueDate]);

        if (results.size() == 0) {
            log.info("there are no accounts with balances for interest transactions for the billingCycle [{}]", routineRequest.billingCycle);
            // nothing to post.
            return;
        }

        // 2. for each tenant account, with a negative balance, apply the interest on the balances.
        // now prepare the temporary transaction section
        TransactionBatchTmp transactionBatchTmp = new TransactionBatchTmp();
        transactionBatchTmp.transactionType = TransactionType.findByTransactionTypeCode('IV');
        // this is an invoice batch being created.
        transactionBatchTmp.autoCommitted = true;

        // prepare the batch transactions
        Long counter = 0;

        //@this is hte generic application reporting currency
        Currency reportingCurrency = currencyService.getReportingCurrency();

        for (GroovyRowResult result : results) {
            Long leaseId = result[1] as Long;
            Lease lease = Lease.read(leaseId)

            String dateTimeString = new Date().format("HHmmssSS")
            //vat is not included when adding to the temporary tables.
            BigDecimal amount = (BigDecimal) result[2] * interestRate; //@todo: make the interest updateable

            TransactionTmp transactionTmp = new TransactionTmp()
            transactionTmp.transactionDate = billingCycle.startDate;
            transactionTmp.creditAccount = result[0];
            transactionTmp.debitAccount = result[0];
            transactionTmp.amount = amount;
            transactionTmp.transactionReference = String.format("IR.%s.%s.%07d", billingCycle.name, dateTimeString, ++counter);

            transactionTmp.description = "Interest for ${billingCycle.billingCycleName}"
            transactionTmp.subAccount = subAccount;
            transactionTmp.transactionBatchTmp = transactionBatchTmp

            //Extra fields to add clarity to transaction data
            transactionTmp.lease = lease
            transactionTmp.rentalUnit = lease?.rentalUnit;
            transactionTmp.property = lease?.property;
            transactionTmp.billingCycle = billingCycle;
            transactionTmp.transactionType = transactionBatchTmp.transactionType;
            transactionTmp.transactionDate = routineRequest.valueDate;

            //update the currencies
            transactionTmp.transactionCurrency = lease.leaseCurrency
            transactionTmp = transactionTmpService.transactionCurrencies(transactionTmp, lease.invoicingCurrency, reportingCurrency)

            transactionBatchTmp.addToTransactionTmps(transactionTmp)
        }

        TransactionBatchTmp.withTransaction {
            transactionBatchTmp.save(failOnError: true);
        }

        // 3. post the transactionBatch
        TransactionBatchTmp.withTransaction {
            routineRequest.attach()
            TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(transactionBatchTmp);

            routineRequest.transactionBatch = transactionBatch;
            routineRequest.save();
        }
    }
}