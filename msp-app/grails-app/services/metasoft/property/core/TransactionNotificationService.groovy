package metasoft.property.core

import grails.events.annotation.Subscriber
import grails.gorm.transactions.Transactional

class TransactionNotificationService {

    @Transactional
    @Subscriber
    def onTransactionPosted(TransactionBatch transactionBatch) {

        if(transactionBatch) {
            log.info "transaction posted notification : ${transactionBatch.batchNumber}"
            String shortDescription = "create transaction for ${transactionBatch.batchNumber}"

            TransactionNotification transactionNotification = new TransactionNotification();
            transactionNotification.reference = transactionBatch.batchNumber;
            transactionNotification.description = shortDescription;
            transactionNotification.shortDescription = shortDescription;

            transactionNotification.save();
        }
    }

    def list(Map args) {
        TransactionNotification.list(args);
    }

    def get(Serializable id) {
        TransactionNotification.get(id);
    }

    Long count() {
        return TransactionNotification.count();
    }
}
