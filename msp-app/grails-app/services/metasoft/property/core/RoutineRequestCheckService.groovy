package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.springframework.validation.BindException

class RoutineRequestCheckService {
    
    SpringSecurityService springSecurityService
    RoutineRequestService routineRequestService;

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    @Transactional
    RoutineRequestCheck createCheckRecord(RoutineRequestTmp routineRequestTmp) {
        RoutineRequestCheck routineRequestCheck = new RoutineRequestCheck()

        routineRequestCheck.transactionSource = routineRequestTmp;
        routineRequestCheck.actionName = "CREATE";

        routineRequestCheck.maker = currentUser;
        routineRequestCheck.makeDate = new Date();
        routineRequestCheck.entityName = "ROUTINE_REQUEST";
        routineRequestCheck.actionName = routineRequestTmp.userAction;
        routineRequestCheck.entityUrl = "#";
        routineRequestCheck.transactionSource = routineRequestTmp;
        def jsonBuilder = new JsonBuilder([:])
        routineRequestCheck.jsonData = jsonBuilder.toString()
        routineRequestCheck.save(failOnError: true)   // save the transientValue

        /*
        routineRequestTmp.postRequest = true;
        routineRequestTmp.postRequestBy = springSecurityService.currentUser;
        routineRequestTmp.datePostRequest = new Date();
        */

        return save(routineRequestCheck);
    }

    Long count() {
        RoutineRequestCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    RoutineRequestCheck get(Serializable id) {
        RoutineRequestCheck.get(id);
    }

    RoutineRequest getEntity(RoutineRequestCheck routineRequestCheck) {
        RoutineRequest routineRequest = routineRequestCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(routineRequestCheck.jsonData);
        routineRequest.setProperties(json);

        return routineRequest;
    }

    List<RoutineRequestCheck> list(Map args) {
        RoutineRequestCheck.list();
    }

    List<RoutineRequestCheck> listInbox(Map args) {

        String categoryArg = args.get('cat')
        String query = args.get('q')

        RoutineRequestCheck.where {
            if(query) {
                transactionSource.routineType.name ==~ "%${query}%"
            }

            checkStatus == CheckStatus.PENDING
        }.join('entity').list(args);
    }

    List<RoutineRequestCheck> listOutbox(Map args) {
        RoutineRequestCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    RoutineRequestCheck save(RoutineRequestCheck routineRequestCheck) {
        routineRequestCheck.save(failOnError: true);
    }

    @Transactional
    RoutineRequestCheck approve(RoutineRequestCheck routineRequestCheck) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(routineRequestCheck.isChecked()) {
            BindException errors = new BindException(routineRequestCheck, "routineRequestCheck")
            errors.rejectValue("checkStatus", "routineRequestTmp.checked.already", "Routine Request is already checked.")
            def message = "Routine Request is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        routineRequestCheck.checkStatus = CheckStatus.APPROVED;
        routineRequestCheck.checker = currentUser;
        routineRequestCheck.checkDate = new Date();
        routineRequestCheck.checkComment = "Approved";
        routineRequestCheck.save(failOnError: true);

        postRoutineRequest(routineRequestCheck.transactionSource);
        return routineRequestCheck;
    }

    @Transactional
    RoutineRequestCheck reject(RoutineRequestCheck routineRequestCheck, Map params) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(routineRequestCheck.isChecked()) {
            // already checked.
            return routineRequestCheck;
        }

        // the check entity
        routineRequestCheck.checkStatus = CheckStatus.REJECTED;
        routineRequestCheck.checker = currentUser;
        routineRequestCheck.checkDate = new Date();
        routineRequestCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(routineRequestCheck.actionName == 'CREATE') {
            RoutineRequest routineRequest = routineRequestCheck.entity;
            routineRequest.checkStatus = CheckStatus.REJECTED;
            routineRequest.save();
        } else {
            RoutineRequest routineRequest = routineRequestCheck.entity;
            routineRequest.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            routineRequest.save();
        }

        return routineRequestCheck.save(failOnError: true);
    }

    @Transactional
    RoutineRequestCheck cancel(RoutineRequestCheck routineRequestCheck, Map params) {
        if(routineRequestCheck.isChecked()) {
            BindException errors = new BindException(routineRequestCheck, "routineRequestCheck")
            errors.rejectValue("checkStatus", "routineRequestTmp.checked.already", "Routine Request is already checked.")
            def message = "Property Request is already checked."
            throw new ValidationException(message, errors);
        }

        // the check entity
        routineRequestCheck.checkStatus = CheckStatus.CANCELLED;
        routineRequestCheck.checker = currentUser;
        routineRequestCheck.checkDate = new Date();
        routineRequestCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        RoutineRequestTmp routineRequestTmp = routineRequestCheck.transactionSource;
        //routineRequestTmp.posted = true;
        routineRequestTmp.save();

        return routineRequestCheck.save(failOnError: true);
    }

    @Transactional
    RoutineRequest postRoutineRequest(RoutineRequestTmp routineRequestTmp) {
        // configure if transaction is already loaded
        /*
        if(routineRequestTmp.posted) {
            BindException errors = new BindException(routineRequestTmp, "routineRequestTmp")
            errors.rejectValue("posted", "routineRequestTmp.posted.already", "Routine Request is already posted.")
            def message = "Invalid Action: Routine Request ${routineRequestTmp} is already posted."
            throw new ValidationException(message, errors)
        }
        */

        // if the posting is new, create a new Property object
        RoutineRequest routineRequest = retrieveRoutineRequest(routineRequestTmp);
        routineRequest.maker = springSecurityService.currentUser as SecUser;
        routineRequest.makeDate = new Date();
        routineRequest.checker = springSecurityService.currentUser as SecUser;
        routineRequest.dirtMaker = routineRequest.requestedBy;
        routineRequest.checkDate = new Date();
        routineRequest.checkStatus = CheckStatus.APPROVED;
        routineRequest.dirtyStatus = CheckStatus.APPROVED;
        routineRequest.routineRequestTmp = null;    // it is in final state for now.
        return routineRequest.save(failOnError: true);
    }

    RoutineRequest retrieveRoutineRequest(RoutineRequestTmp routineRequestTmp) {
        RoutineRequest routineRequest = routineRequestTmp.entity ?: new RoutineRequest();

        routineRequest.with {
            routineType = routineRequestTmp.routineType
            billingCycle = routineRequestTmp.billingCycle
            runDate = routineRequestTmp.runDate
            valueDate = routineRequestTmp.valueDate
            emailAlert = routineRequestTmp.emailAlert
            smsAlert = routineRequestTmp.smsAlert
            requestedBy = routineRequestTmp.requestedBy
        }
        routineRequest.routineRequestTmp = routineRequestTmp;
        return routineRequest;
    }

}