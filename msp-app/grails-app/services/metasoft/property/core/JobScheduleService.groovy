package metasoft.property.core

class JobScheduleService {

    JobSchedule get(Serializable id) {
        return JobSchedule.get(id);
    }

    List<JobSchedule> list(Map args) {
        String q = args.get('jobName');

        return JobSchedule.where{
            if(q){jobName ==~ "%$q%"}
        }.list(args);
    }

    Long count() {
        return JobSchedule.count();
    }

    JobSchedule save(JobSchedule jobSchedule) {
        return jobSchedule.save(failOnError: true);
    }

}