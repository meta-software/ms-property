package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import metasoft.property.GlBankAccountCommand

@GrailsCompileStatic
class GlBankAccountService implements DataBinder {

    //@Transactional(readOnly = true)
    List<GlBankAccount> list(Map args) {
        args['sort'] = 'accountName'
        args['order'] = 'asc'
        List<GlBankAccount> glBankAccounts = GlBankAccount.list(args);
        return glBankAccounts;
    }

    List<GlBankAccount> listOptions(Map args) {
        args['sort'] = 'accountName'
        args['order'] = 'asc'

        return GlBankAccount.list(args);
    }

    GlBankAccount get(Serializable id) {
        return GlBankAccount.get(id);
    }

    @Transactional
    GlBankAccount save(GlBankAccountCommand glBankAccountCommand) {

        GlBankAccount glBankAccount = new GlBankAccount()
        bindData(glBankAccount, glBankAccountCommand)

        if(!glBankAccount.validate()) {
            //log.error(glBankAccount.errors)
        }

        glBankAccount.save(failOnError: true);
    }

    @Transactional
    GlBankAccount update(Long id, GlBankAccountCommand glBankAccountCommand) {

        GlBankAccount glBankAccount = GlBankAccount.get(id)

        if(glBankAccount == null) {
            //@todo: add code to throw exception
        }

        bindData(glBankAccount, glBankAccountCommand)
        if(!glBankAccount.validate()) {
            //log.error(glBankAccount.errors)
        }

        glBankAccount.save(failOnError: true);
    }

    /**
     * Retrives the bank account configuration for a give nominal account number.
     *
     * @param accountNumber
     * @return
     */
    GlBankAccount getGlBankAccountByAccountNumber(String accountNumber) {
        GlBankAccount.where {
            active == true
            generalLedger.accountNumber == accountNumber
        }.get()
    }
}
