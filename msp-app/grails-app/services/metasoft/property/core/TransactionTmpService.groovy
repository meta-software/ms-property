package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.databinding.DataBinder
import groovy.util.logging.Slf4j
import metasoft.property.CancelCommand
import metasoft.property.DepositPaymentCommand
import metasoft.property.InvoiceCommand
import metasoft.property.LandlordPaymentCommand
import metasoft.property.PaymentCommand
import metasoft.property.RefundCommand
import metasoft.property.TransferCommand
import metasoft.property.core.exceptions.ExchangeRateNotFoundException
import org.springframework.validation.BindException

@Slf4j
@GrailsCompileStatic
class TransactionTmpService implements DataBinder{

    BillingCycleService billingCycleService;
    SpringSecurityService springSecurityService;
    GrailsApplication grailsApplication;
    CurrencyService currencyService
    ExchangeRateService exchangeRateService

    /**
     * TransactionTmp postInsert operations.
     *
     * @param transactionTmp
     * @return
     */
    @Transactional
    TransactionTmp generateReference(TransactionTmp transactionTmp) {

        transactionTmp.billingCycle = billingCycleService.getBillingCycle(transactionTmp.transactionDate);
        if(!transactionTmp.transactionReference) {
            transactionTmp.transactionReference = String.format("%s%s%07d",
                    transactionTmp.transactionType.transactionTypeCode,
                    transactionTmp.transactionDate.format("yyMM"),
                    transactionTmp.id);

        }
        transactionTmp.save(failOnError: true);

        return transactionTmp;
    }

    TransactionTmp get(Serializable id) {
        return TransactionTmp.get(id);
    }

    def listByBatch(Long transactionBatchId) {
        return TransactionTmp.executeQuery("""select 
            new map(t.id as id, 
                    t.creditAccount as creditAccount,
                    coalesce(ac.accountName, gc.accountName) as creditAccountName,
                    t.debitAccount as debitAccount,
                    coalesce(ad.accountName, gd.accountName) as debitAccountName,
                    t.amount as amount,
                    tt.name as transactionType,
                    sa.accountName as subAccountName,
                    sa.accountNumber as subAccountNumber,
                    t.description as description,
                    t.transactionReference as transactionReference,
                    t.transactionDate as transactionDate,
                    t.transactionCurrency.id as transactionCurrency,
                    t.reportingCurrency.id as reportingCurrency
                    ) 
            from TransactionTmp t 
            inner join TransactionType tt on tt.id = t.transactionType.id
            inner join SubAccount sa on sa.id = t.subAccount.id
            left join GeneralLedger gc on gc.accountNumber=t.creditAccount
            left join GeneralLedger gd on gd.accountNumber=t.debitAccount
            left join Account ac on ac.accountNumber=t.creditAccount
            left join Account ad on ad.accountNumber=t.debitAccount
            where t.transactionBatchTmp.id=:batchId
            order by t.id asc""", [batchId: transactionBatchId], [order: 't.id', sort: 'asc'])
    }

    @Transactional
    TransactionTmp create(TransactionTmp transactionTmp) {

        switch(transactionTmp.transactionType.transactionTypeCode) {
            case 'RC':
            case 'IT':
            case 'BL':
                transactionTmp.subAccount = transactionTmp.subAccount ?: SubAccount.findByAccountNumber('0');
            break;
        }

        //retrieve the active lease by date
        Lease lease = transactionTmp.rentalUnit.getActiveLeaseByDate(transactionTmp.transactionDate);
        transactionTmp.lease = lease;

        //retrieve the billingCycle
        BillingCycle billingCycle = billingCycleService.getBillingCycle(transactionTmp.transactionDate);
        transactionTmp.billingCycle = billingCycle;

        transactionTmp.save(failOnError: true);

        transactionTmp.transactionReference = String.format("%s%s%09d",
                transactionTmp.transactionType.transactionTypeCode,
                transactionTmp.billingCycle.name,
                transactionTmp.id);

        transactionTmp.save(failOnError: true);
        log.info("successfully saved the transaction ref://{}", transactionTmp.transactionReference)

        return transactionTmp;
    }

    @Transactional
    TransactionTmp refund(RefundCommand refundCommand) {

        MainTransaction mainTransaction = MainTransaction.findByTransactionReference(refundCommand.transactionReference);

        // load the transactionTmp
        TransactionTmp transactionTmp = toTransactionTmp(mainTransaction);
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('RF');

        bindData(transactionTmp, refundCommand)
        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp cancel(CancelCommand cancelCommand) {

        MainTransaction mainTransaction = MainTransaction.findByTransactionReference(cancelCommand.transactionReference);

        //@todo: check if the transaction is cancelleable

        // load the transactionTmp
        TransactionTmp transactionTmp = toTransactionTmp(mainTransaction);
        bindData(transactionTmp, cancelCommand)
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('CN');

        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp payment(PaymentCommand paymentCommand) {

        Currency reportingCurrency = currencyService.getReportingCurrency();

        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp();
        bindData(transactionTmp, paymentCommand)
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('PM');
        transactionTmp.transactionAmount = transactionTmp.amount;
        transactionTmp = transactionCurrencies(transactionTmp, reportingCurrency, reportingCurrency)
        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    TransactionTmp transactionCurrencies(TransactionTmp transactionTmp, Currency operatingCurrency, Currency reportingCurrency) {
        try {
            log.info("transaction-currencies [operatingCurrency={}, reportingCurrency={}]", operatingCurrency, reportingCurrency)

            Currency transactionCurrency = transactionTmp.transactionCurrency;

            //retrieve the invoice(operating) currency
            ExchangeRate operatingExchangeRate   = exchangeRateService.getActiveExchangeRate(transactionCurrency, operatingCurrency, transactionTmp.transactionDate);
            transactionTmp.operatingCurrency     = operatingCurrency;
            transactionTmp.operatingAmount       = exchangeRateService.convert(transactionTmp.transactionAmount, operatingExchangeRate);
            transactionTmp.operatingExchangeRate = operatingExchangeRate;

            //retrieve the reporting currency
            ExchangeRate reportingExchangeRate = exchangeRateService.getActiveExchangeRate(transactionCurrency, reportingCurrency, transactionTmp.transactionDate);
            transactionTmp.reportingCurrency = reportingCurrency;
            transactionTmp.reportingAmount = exchangeRateService.convert(transactionTmp.transactionAmount, reportingExchangeRate);
            transactionTmp.reportingExchangeRate = reportingExchangeRate;

            return transactionTmp;
        } catch (ExchangeRateNotFoundException enfe) {
            //convert the exception into a ValidationException
            BindException errors = new BindException(transactionTmp, "transactionTmp")
            errors.reject("exchangeRate.notExist", "Exchange Rate does not exist for Transaction Date.")
            throw new ValidationException(errors.getMessage(), errors);
        }
    }

    @Transactional
    TransactionTmp depositPayment(DepositPaymentCommand depositPaymentCommand) {
        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp();
        bindData(transactionTmp, depositPaymentCommand)
        transactionTmp.transactionAmount = transactionTmp.amount
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('DP');

        transactionTmp = transactionCurrencies(transactionTmp, currencyService.reportingCurrency, currencyService.reportingCurrency)

        // transactionTmp.subAccount = SubAccount.findByAccountName('Repairs'); // always this is when a deposit is happening.
        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp landlordPayment(LandlordPaymentCommand landlordPaymentCommand) {
        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp();
        bindData(transactionTmp, landlordPaymentCommand)
        transactionTmp.transactionAmount = transactionTmp.amount
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('LP');
        transactionTmp = transactionCurrencies(transactionTmp, transactionTmp.transactionCurrency, currencyService.reportingCurrency)

        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp invoice(InvoiceCommand invoiceCommand) {

        Currency reportingCurrency = currencyService.getReportingCurrency();
        Currency transactionCurrency = Currency.get(invoiceCommand.transactionCurrency);

        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp()
        bindData(transactionTmp, invoiceCommand);
        Lease lease = transactionTmp.lease;

        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('IV');
        transactionTmp.transactionAmount = invoiceCommand.amount;

        try{
            //retrieve the invoice(operating) currency
            transactionTmp = transactionCurrencies(transactionTmp, lease.invoicingCurrency, reportingCurrency)
        } catch(ExchangeRateNotFoundException exnfe) {
            //convert the exception into a ValidationException
            BindException errors = new BindException(transactionTmp, "transactionTmp")
            errors.reject("exchangeRate.notExist", "Exchange Rate does not exist for Transaction Date.")
            throw new ValidationException(errors.getMessage(), errors);
        }

        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp receipt(Map args) {

        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp(args);
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('RC');
        transactionTmp.transactionBatchTmp = (TransactionBatchTmp)args['transactionBatchTmp'];
        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp transfer(TransferCommand transferCommand) {

        Currency reportingCurrency = currencyService.getReportingCurrency();

        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp();
        bindData(transactionTmp, transferCommand)
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('IT');
        transactionTmp.subAccount = SubAccount.findByAccountNumber('0'); // None
        transactionTmp.transactionAmount = transactionTmp.amount;

        transactionTmp = transactionCurrencies(transactionTmp, reportingCurrency, reportingCurrency)
        transactionTmp.save(failOnError: true)
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    TransactionTmp balance(Map args) {

        Currency reportingCurrency = currencyService.getReportingCurrency();

        // load the transactionTmp
        TransactionTmp transactionTmp = new TransactionTmp(args);
        transactionTmp.transactionType = TransactionType.findByTransactionTypeCode('BL');
        transactionTmp.subAccount = SubAccount.findByAccountNumber('2'); // None

        transactionTmp.transactionAmount = transactionTmp.amount;
        transactionTmp = transactionCurrencies(transactionTmp, reportingCurrency, reportingCurrency)
        transactionTmp.save(failOnError: true);

        //generate the transactionReference
        generateReference(transactionTmp);

        return transactionTmp;
    }

    @Transactional
    void delete(Serializable id) {
        TransactionTmp transactionTmp = get(id);
        transactionTmp.delete(failOnError: true, flush: true)
    }

    MainTransaction toMainTransaction(TransactionTmp transactionTmp) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        MainTransaction mainTransaction = new MainTransaction();
        bindData(mainTransaction, transactionTmp);

        mainTransaction.operatingCurrency = transactionTmp.operatingCurrency
        mainTransaction.transactionCurrency = transactionTmp.transactionCurrency
        mainTransaction.reportingCurrency = transactionTmp.reportingCurrency

        mainTransaction.operatingExchangeRate = transactionTmp.operatingExchangeRate
        mainTransaction.reportingExchangeRate = transactionTmp.reportingExchangeRate

        mainTransaction.transactionType = transactionTmp.transactionType;
        mainTransaction.transactionReference = transactionTmp.transactionReference;
        mainTransaction.transactionDate = transactionTmp.transactionDate;
        mainTransaction.description = transactionTmp.description;
        mainTransaction.parentTransaction = false;
        mainTransaction.property = transactionTmp.property;
        mainTransaction.lease = transactionTmp.lease;
        mainTransaction.rentalUnit = transactionTmp.rentalUnit;
        mainTransaction.billingCycle = transactionTmp.billingCycle;
        mainTransaction.datePosted = new Date();
        mainTransaction.postedBy = currentUser?.username ?: grailsApplication.config.get('metasoft.app.audit.defaultUser');

        return mainTransaction;
    }

    TransactionTmp toTransactionTmp(MainTransaction mainTransaction) {
        TransactionTmp transactionTmp = new TransactionTmp();

        bindData(transactionTmp, mainTransaction);

        transactionTmp.transactionType = mainTransaction.transactionType;
        transactionTmp.transactionReference = mainTransaction.transactionReference;
        transactionTmp.transactionDate = mainTransaction.transactionDate;
        transactionTmp.description = mainTransaction.description;
        transactionTmp.property = mainTransaction.property;
        transactionTmp.lease = mainTransaction.lease;
        transactionTmp.rentalUnit = mainTransaction.rentalUnit;
        transactionTmp.billingCycle = mainTransaction.billingCycle;
        transactionTmp.creditAccount = mainTransaction.accountNumberCr
        transactionTmp.debitAccount = mainTransaction.accountNumberDb
        transactionTmp.subAccount = SubAccount.findByAccountNumber(mainTransaction.subAccountNumberCr);
        transactionTmp.amount = mainTransaction.credit ?: mainTransaction.debit;
        transactionTmp.operatingAmount = mainTransaction.creditOperatingAmount ?: mainTransaction.debitOperatingAmount;
        transactionTmp.reportingAmount = mainTransaction.creditReportingAmount ?: mainTransaction.debitReportingAmount;
        transactionTmp.transactionAmount = mainTransaction.creditTransactionAmount ?: mainTransaction.debitTransactionAmount;
        transactionTmp.reportingExchangeRate = mainTransaction.reportingExchangeRate
        transactionTmp.operatingExchangeRate = mainTransaction.operatingExchangeRate
        transactionTmp.transactionCurrency = mainTransaction.transactionCurrency
        transactionTmp.operatingCurrency = mainTransaction.operatingCurrency
        transactionTmp.reportingCurrency = mainTransaction.reportingCurrency


        return transactionTmp;
    }

}
