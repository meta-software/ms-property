package metasoft.property.core

import org.springframework.security.authentication.BadCredentialsException

class SharedSessionAuthenticatorService {

    SharedSessionService sharedSessionService;

    UserSessionToken validateToken(String sharedToken) {

        UserSessionToken userSessionToken = null;
        UserSessionToken.withTransaction {
            userSessionToken = sharedSessionService.validateToken(sharedToken);
        }

        return userSessionToken;
    }

    void invalidateToken(String sharedToken) {
        sharedSessionService.invalidateToken(sharedToken);
    }

}