package metasoft.property.core

import grails.core.GrailsApplication
import grails.core.support.GrailsApplicationAware
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.mapping.LinkGenerator
import org.grails.web.json.JSONObject
import org.grails.web.util.WebUtils

import javax.servlet.http.HttpServletRequest

class PropertyManagerService implements GrailsApplicationAware {

    GrailsApplication grailsApplication
    SpringSecurityService springSecurityService
    LinkGenerator grailsLinkGenerator

    HttpServletRequest getRequest() {
        return WebUtils.retrieveGrailsWebRequest().getCurrentRequest();
    }

    PropertyManager get(Serializable id) {
        return PropertyManager.get(id)
    }

    List<PropertyManager> list(Map args) {
        def name = args['name']?.trim();

        return PropertyManager.where {
            if(name) {
                (firstName ==~ "%$name%" || lastName ==~ "%$name%")
            }
        }.list(args)
    }

    Long count() {
        return PropertyManager.count()
    }

    Long count(Map args) {
        def name = args['name']?.trim();

        return PropertyManager.where {
            if(name) {
                (firstName ==~ "%name%" || lastName ==~ "%name%")
            }
        }.count()
    }

    @Transactional
    PropertyManager save(PropertyManager propertyManager) {
        propertyManager.save(failOnError: true);
    }

    @Transactional
    PropertyManager create(PropertyManager propertyManager, Map args) {

        // flush & persist the object
        propertyManager.save(failOnError: true, flush: true);

        // generate the maker-checker
        PropertyManagerCheck propertyManagerCheck = new PropertyManagerCheck()
        propertyManagerCheck.entity = propertyManager;
        propertyManagerCheck.entityName = "PROPERTY_MANAGER";
        propertyManagerCheck.actionName = "CREATE";
        propertyManagerCheck.maker = springSecurityService.currentUser as SecUser;

        String url = grailsLinkGenerator.link(controller: 'property-manager', action: 'show', id: propertyManager.id, absolute: false);
        propertyManagerCheck.entityUrl = url;
        propertyManagerCheck.jsonData = args;

        println("request data: ${request.getJSON().toString()}")

        propertyManagerCheck.makeDate = new Date();
        try{
            propertyManagerCheck.save(failOnError: true)
        } catch (ValidationException e) {
            println(e.message)
            throw e
        }

        return propertyManager;
    }

    @Transactional
    PropertyManager update(PropertyManager propertyManager, Map args) {

        // flush & persist the object
        propertyManager.save(failOnError: true, flush: true);

        // clean the stored object
        args.remove('_method');

        // generate the maker-checker
        PropertyManagerCheck propertyManagerCheck = new PropertyManagerCheck()
        propertyManagerCheck.entity = propertyManager;
        propertyManagerCheck.entityName = "PROPERTY_MANAGER";
        propertyManagerCheck.actionName = "CREATE";
        propertyManagerCheck.maker = springSecurityService.currentUser as SecUser;

        String url = grailsLinkGenerator.link(controller: 'property-manager', action: 'show', id: propertyManager.id, absolute: false);
        propertyManagerCheck.entityUrl = url;
        propertyManagerCheck.jsonData = JSONObject.newInstance(args).toString();

        propertyManagerCheck.makeDate = new Date();
        try{
            propertyManagerCheck.save(failOnError: true)
        } catch (ValidationException e) {
            println(e.message)
            throw e
        }

        return propertyManager;
    }

}