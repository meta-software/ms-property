package metasoft.property.core

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent
import org.grails.datastore.mapping.engine.event.PostDeleteEvent
import org.grails.datastore.mapping.engine.event.PostInsertEvent
import org.grails.datastore.mapping.engine.event.PreUpdateEvent
import org.grails.datastore.mapping.model.PersistentEntity
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Propagation

@Slf4j
class EventLoggingService {

    @Autowired
    SpringSecurityService springSecurityService;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void logEvent(AbstractPersistenceEvent event, String eventName, String propertyName) {

        Loggable entity = (Loggable)event.getEntityObject();
        entity.discard();

        if(!entity?.loggable) {
            return;
        }

        PersistentEntity persistentEntity = event.getEntity();
        List<String> entityPropertyNames = persistentEntity.persistentPropertyNames;
        entityPropertyNames.removeAll(['loggable', 'dateCreated', 'lastUpdated']);

        Map<String, Object> originalObject = [:]
        Map<String, Object> dirtyObject = [:]

        if(event instanceof PostInsertEvent) {
            for (String key : entityPropertyNames) {
                Object propertyValue = entity.metaClass.getProperty(entity, key);

                if( propertyValue.hasProperty("id" )) {
                    dirtyObject[key] = [id: propertyValue.metaClass.getProperty(propertyValue, "id")]
                } else {

                    if(propertyValue instanceof Collection) {
                        dirtyObject[key] = "#Collection"
                    } else {
                        dirtyObject[key] = propertyValue;
                    }
                }
            }
        } else if (event instanceof PreUpdateEvent) {
            for (String key : entity.listDirtyPropertyNames()) {
                Object propertyValue = entity.metaClass.getProperty(entity, key);
                Object originalValue = entity.getPersistentValue(key);

                if( propertyValue.hasProperty("id" )) {
                    dirtyObject[key] = [id: propertyValue.metaClass.getProperty(propertyValue, "id")]
                    originalObject[key] = [id: originalValue==null ? null : originalValue.metaClass.getProperty(originalValue, "id")]
                } else {
                    if(propertyValue instanceof Collection) {
                        dirtyObject[key] = "#Collection"
                        originalObject[key] = "#Collection"
                    } else {
                        dirtyObject[key] = propertyValue;
                        originalObject[key] = originalValue == null ? null : originalValue
                    }
                }
            }

        } else if (event instanceof PostDeleteEvent) {
        }

        def dirtyObjectJson = dirtyObject as JSON; //new JsonBuilder(dirtyObject);
        def originalObjectJson = originalObject as JSON; //new JsonBuilder(dirtyObject);

        EventLogging eventLogging = new EventLogging(
                actor: currentPrincipal,
                persistedObjectId: entity.ident(),
                className: entity.class.name,
                eventName: eventName,
                propertyName: propertyName,
                uri: currentURI,
                persistedObjectVersion: 0,
                newState: dirtyObjectJson?.toString(),
                oldState: originalObjectJson?.toString()
        );

        try {
            eventLogging.save(failOnError: true);
            //log.debug("created the eventLogging record for event [{}] for entity ${entity.toString()}", event);
        } catch (Exception e) {
            log.error ("error occured while saving eventLogging record for event [{}] , $e.message", event);
            e.printStackTrace();
        }

    }

    String getCurrentPrincipal() {
        return springSecurityService.principal?.username ?: 'system';
    }

    String getCurrentURI() {
        GrailsWebRequest request = GrailsWebRequest.lookup()
        request?.request?.requestURI
    }

}
