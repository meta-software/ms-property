package metasoft.property.core

import grails.gorm.transactions.Transactional
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired

import javax.sql.DataSource

class LandlordService extends AccountService  {

    @Autowired
    DataSource dataSource
    BillingCycleService billingCycleService

    BigDecimal getCurrentReceipts(Long landlordId) {
        Landlord landlord = Landlord.get(landlordId);

        def results = executeDashlet('current-receipts', [account: landlord.accountNumber]);
        return results['totalReceipts'];
    }

    BigDecimal getAccountBalance(Landlord landlord) {
        Sql sql = new Sql(dataSource);
        String hql = """
            SELECT 
                sum(debit_reporting_amount-credit_reporting_amount) as balance 
            FROM 
                ms_transaction A 
            INNER JOIN
                ms_account c on A.account_number_cr=c.account_number
            INNER JOIN  
                account_category g on g.id = c.account_category_id
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE 
                g.code = 'landlord'
                AND S.account_number not in (4, 12, 13)
                AND S.suspense=0
                AND A.account_number_cr = :accountNumber
                """;

        List<GroovyRowResult> results = sql.rows(hql, [accountNumber: landlord.accountNumber]);

        return (BigDecimal) (results[0][0] ?: 0.0);
    }

    Long standingOrdersCount (Landlord landlord) {
        def query = StandingOrder.where{
            landlord == landlord
        }

        return query.count();
    }

    Long lessorPaymentsCount (Landlord landlord) {
        def query = LessorPayment.where{
            landlord == landlord
        }

        return query.count();
    }

    List<Property> getPropertiesByLandlordAccount(String accountNumber) {

        //retrieve the account
        Landlord landlord = Landlord.findByAccountNumber(accountNumber);

        Map args = [max: 10, offset: 0]

        List<Property> properties = Property.where{
            landlord.accountNumber == accountNumber
            checkStatus == CheckStatus.APPROVED
        }.list(args)

        return properties;
    }

    List<Property> propertiesByLandlord(Serializable landlordId, Map args = [:]) {

        String queryParam = args.get('query')

        List<Property> properties = Property.where{
            landlord.id == landlordId

            if(queryParam) {
                name ==~ "%${queryParam}%"
            }
        }.join('propertyType').list(args)

        return properties;
    }

    Long buildingsCount(Landlord landlordArg) {
        def query = Property.where{
            landlord == landlordArg
            checkStatus == CheckStatus.APPROVED
        }

        return query.count();
    }

    Landlord get(Serializable id) {

        Landlord landlord = Landlord.where {
            id == id
        }.join('bankAccount')
         .join('accountCategory')
         .join('accountType')
         .get();

        return landlord;
    }

    /**
     * @todo add comments here.
     *
     * @param args
     * @return
     */
    List<Landlord> fetchList(args) {

        def q = args['q'];

        def query = Landlord.where{
            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.checkStatus && args?.checkStatus != 'all') {
                checkStatus == args.checkStatus
            }

            if(q) {
                ( accountNumber =~ "${q}%" || accountName ==~ "%${q}%")
            }
        }

        return query.list(args)
    }

    List<Landlord> list(Map args) {

        def query = Landlord.where{
            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
            if(args?.checkStatus && args?.checkStatus != 'all') {
                checkStatus == args.checkStatus
            }
        }

        return query.list(args)
    }

    List<Landlord> listApproved(Map args) {
        def query = Landlord.where {
            checkStatus == CheckStatus.APPROVED

            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
        }.join('accountType')
         .join('bankAccount')

        return query.list(args);
    }

    Long count() {
        def query = Landlord.where { 1 == 1}

        return query.count();
    }

    Long count(params) {
        def query = Landlord.where { 1 == 1}

        return query.count(params);
    }

    @Transactional
    String accountNumberGenerator() {
        LandlordAccount landlordAccount = new LandlordAccount()
        landlordAccount.save(failOnError:true)

        String accountNumber = String.format("002%06d", landlordAccount.id);

        return accountNumber;
    }

//    @Transactional
    @Override
    Landlord save(Account account) {
        super.save(account)

        def urlParams = [id: account.id, action: 'show', controller: 'landlord'];
        notify("app:notify", [object: account, urlParams: urlParams, entity: account.class.getCanonicalName()]);

        return (Landlord)account;
    }

    @Override
    Landlord create(Account account, Map args) {
        account.accountCategory = AccountCategory.findByCode('landlord');

        super.create(account, args);

        def urlParams = [id: account.id, action: 'show', controller: 'landlord'];
        notify("app:notify", [object: account, urlParams: urlParams, entity: account.class.getCanonicalName()]);

        return (Landlord)account;
    }

    @Override
    Landlord update(Account account, Map args) {
        super.update(account, args);
        return (Landlord)account;
    }

    def executeDashlet(String report, def params) {
        Sql sql = new Sql(dataSource);

        String landlordAccount = params.account;
        BillingCycle billingCycle = billingCycleService.getCurrentCycle();

        String hql = """
            SELECT 
                sum(debit_reporting_amount-credit_reporting_amount) as balance 
            FROM 
                ms_transaction A 
            INNER JOIN
                ms_account c on A.account_number_cr=c.account_number
            INNER JOIN  
                account_category g on g.id = c.account_category_id
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE 
                g.code = 'landlord'
                AND S.account_number not in (4, 12, 13)
                AND S.suspense=0
                AND A.account_number_cr = :accountNumber
                AND A.billing_cycle_id = :cycleId
                """;

        List<GroovyRowResult> results = sql.rows(hql, [accountNumber: landlordAccount, cycleId: billingCycle.id]);


        return [totalReceipts: (BigDecimal) (results[0][0] ?: 0.0)];
    }

}