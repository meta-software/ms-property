package metasoft.property.core

import grails.events.EventPublisher
import grails.gorm.transactions.Transactional

class AuthService implements EventPublisher {

    SharedSessionService sharedSessionService;

    @Transactional
    UserSessionToken authenticate(String token) {

        //check if valid token exists in database
        UserSessionToken userSessionToken = sharedSessionService.validateToken(token);

        // if the token is non existent then we throw a InvalidUserSessionTokenException
        if(userSessionToken == null) {
            throw new InvalidUserSessionTokenException("Invalid user session $token")
        }

        // authenticate the user in the SpringSecurity plugin


        return userSessionToken;
    }

}

class InvalidUserSessionTokenException extends Exception {
    public InvalidUserSessionTokenException(String message) {
        super(message)
    }
}