package metasoft.property.core

import grails.events.EventPublisher
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.mapping.LinkGenerator
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import org.hibernate.SQLQuery
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.BindException
import org.springframework.validation.Errors

class PropertyService implements EventPublisher {

    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    LinkGenerator grailsLinkGenerator;

    SpringSecurityService springSecurityService;

    Property get(Serializable objectId) {
        return Property.where {
            id == objectId
        }
        .join('propertyType')
        .join('landlord')
        .join('landlord.bankAccount')
        .join('propertyManager')
        .get();
    }

    Property configure(Serializable id) {

        Property property = get(id);
        // configure the total space configured for rental units

        if(property.fullyConfigured) {
            return property;
        }

        BigDecimal rentalUnitTotalArea = (BigDecimal)RentalUnit.executeQuery("select sum(area) from RentalUnit u where u.property.id=:id", [id: id])[0] ?: 0.0;

        // if these two are not equal then, something is wrong for sure
        if(rentalUnitTotalArea != property.area) {
            Errors errors = new BindException(property, "property")
            errors.rejectValue("area", "property.area.nottally", "Property area ${property.area} does not tally with the RentalUnits sum area ${rentalUnitTotalArea}")
            throw new ValidationException("The area of the property does not tally with sum area of its rental units.", errors);
        }

        Property.withTransaction {
            property.fullyConfigured = true;
            property.save(failOnError: true)
        }

        return property;
    }

    List<Property> list(Map args) {

        def mcStatus = args['mc-status'];

        def query = Property.where {
            if(args?.propertyType) { propertyType.id == args.propertyType }
            if(args?.propertyManager) { propertyManager.id == args.propertyManager }
            if(args?.name) {name ==~ "%${args.name}%" }
            if(args?.landlord) {landlord.accountName ==~ "%${args.landlord}%" }
            checkStatus == CheckStatus.APPROVED
        }.join('propertyType')
        .join('landlord')
        .join('landlord.bankAccount')

        List<Property> propertyList = query.list(args);

        return propertyList;
    }

    List<Property> listOptions(Map params) {

        params.max = 200;
        String q = params?.q;

        // do for checkStatusArg
        CheckStatus checkStatusArg = CheckStatus.APPROVED;

        def query = Property.where {
            if(q) { name ==~ "${q}%" }
            checkStatus == checkStatusArg;
        }

        return query.list(params);
    }

    Long count(def args = [:]) {

        def query = Property.where {
            if(args?.propertyType) { propertyType.id == args.propertyType }
            if(args?.propertyManager) { propertyManager.id == args.propertyManager }
            if(args?.name) {name ==~ "%${args.name}%" }
            if(args?.landlord) {landlord.accountName ==~ "%${args.landlord}%" }
            checkStatus == CheckStatus.APPROVED
        }

        return query.count();
    }

    @Transactional
    Property delete(Serializable id) {
        Property property = Property.get(id)
        property.deleted = true;
        property.save(failOnError:true)

        return property;
    }

    @Transactional
    Property save(Property property) {

        property.markDirty('address');
        property.save(failOnError: true);

        def urlParams = [id: property.id, action: 'show', controller: 'property'];
        notify("app:notify", [object: property, urlParams: urlParams, entity: property.class.getCanonicalName()]);

        return property;
    }

    @Transactional
    Property create(Property property, Map args) {

        property.markDirty('address');
        // clear these, they are handled in the approve method
        property.rentalUnits.clear();
        property.rentalUnits = [];
        // prepare the maker-checker attributes
        property.maker = springSecurityService.currentUser as SecUser;
        property.dirtMaker = springSecurityService.currentUser as SecUser;
        property.makeDate = new Date();
        property.save(failOnError: true, flush: true);// save the entity

        saveCheckData(property, args, "CREATE");

        def urlParams = [id: property.id, action: 'show', controller: 'property'];
        notify("app:notify", [object: property, urlParams: urlParams, entity: property.class.getCanonicalName()]);

        return property;
    }

    @Transactional
    Property update(Property property, Map args) {
        // prepare the maker-checker attributes
        saveCheckData(property, args, "UPDATE");

        def urlParams = [id: property.id, action: 'show', controller: 'property'];
        notify("app:notify", [object: property, urlParams: urlParams, entity: property.class.getCanonicalName()]);

        return property;
    }

    @Transactional
    PropertyCheck saveCheckData(Property property, Map args, String action) {

        // create the url
        String url = grailsLinkGenerator.link(controller: 'property', action: 'show', id: property.id, absolute: false);

        // generate the maker-checker
        PropertyCheck propertyCheck = new PropertyCheck()
        propertyCheck.entity = property;
        propertyCheck.entityName = "PROPERTY";
        propertyCheck.actionName = action;
        propertyCheck.maker = springSecurityService.currentUser as SecUser;
        propertyCheck.entityUrl = url;
        propertyCheck.jsonData = new JsonBuilder(args).toString() ; //getUpdateMap(property, args, action);
        propertyCheck.makeDate = new Date();
        propertyCheck.save();

        // discard property changes before saving
        property.dirtMaker = propertyCheck.maker;
        property.dirtyStatus = CheckStatus.EDITING;
        property.save();

        try {
            propertyCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the check entity method") ;
            throw e
        }

        return propertyCheck;
    }

    //@Transactional
    String getUpdateMap(Property property, Map args, String action) {

        if(action == 'CREATE') {
            return toJson(args).toString();
        }

        property.setProperties(args);
        List<String> dirtyPropertyNames = property.getDirtyPropertyNames();
        Map dirtyProperties = [:]

        dirtyPropertyNames.each { dirtyProperty ->
            println "property -> $dirtyProperty"
            dirtyProperties[dirtyProperty] = args[dirtyProperty];
        }

        //
        dirtyProperties['rentalUnits'] = args['rentalUnits'];

        // discard
        property.discard();
        property.refresh();

        return toJson(dirtyProperties).toString(); //JsonBuilder(dirtyProperties).toString();
    }

    @Transactional(readOnly = true)
    BigDecimal getOccupiedSpace(Property property, BillingCycle billingCycle) {
        String hql = """from Lease as l
                    join l.property as p
                    join l.leaseStatus as ls 
                 where p.id = :propertyId
                 and l.checkStatus = 'APPROVED'
                 and ls.code = 'active' 
                 and ( 
                    (l.leaseStartDate <= :cycleStartDate and l.leaseExpiryDate between :cycleStartDate and :cycleEndDate)
                    or (l.leaseStartDate <= :cycleStartDate and l.leaseExpiryDate >= :cycleEndDate)
                    or (l.leaseStartDate >= :cycleStartDate and l.leaseExpiryDate <= :cycleEndDate)
                    or (l.leaseStartDate between :cycleStartDate and :cycleEndDate and l.leaseExpiryDate >= :cycleStartDate)
                    )
                 and l.expired = false
                 and l.deleted = false
                 and l.terminated = false
              """

        //@todo: this is really bad code, ought to be improved on
        List<Lease> leases = Lease.findAll(hql, [
                propertyId: property.id,
                cycleStartDate: billingCycle.startDate,
                cycleEndDate: billingCycle.endDate ]) as List<Lease>;

        BigDecimal occupiedSpace = 0.0;
        leases.each { def lease ->
            Lease leaseRecord = Lease.where{ id == lease.id}.join("rentalUnit").get()
            occupiedSpace = occupiedSpace + leaseRecord.rentalUnit.area;
        }

        return occupiedSpace;
    }

    JsonBuilder toJson(Map property) {

        def jsonBuilder = new JsonBuilder();

        jsonBuilder {
            if(property.id)
                id property?.id
            if(property.name)
                name property.name
            if(property.landlord) {
                landlord {
                    id property.landlord.id
                }
            }
            if(property.address)
                address property.address
            if(property.description)
                description property.description
            if(property.area)
                area property.area
            if(property.commission)
                commission property.commission
            if(property.chargeVat)
                chargeVat property.chargeVat
            if(property.propertyType) {
                propertyType {
                    id property.propertyType?.id
                }
            }
            if(property.propertyManager) {
                propertyManager {
                    id property.propertyManager?.id
                }
            }
            if(property.mandateDate)
                mandateDate property.mandateDate
            if(property.lastInspectionDate)
                lastInspectionDate property.lastInspectionDate
            if(property.terminated) {
                terminated property.terminated
                //terminatedDate property.terminatedDate
            }
            if(property.propertyCode)
                propertyCode property.propertyCode
            if(property.notes)
                notes property.notes
            if(property.standNumber)
                standNumber property.standNumber
            if(property.deleted) {
                deleted property.deleted
                //deletedDate property.deletedDate
            }
            if(property.rentalUnits) {
                rentalUnits (property.rentalUnits) { rentalUnit ->
                    if(rentalUnit.id) {
                        id rentalUnit.id
                    }
                    name rentalUnit.name
                    unitReference rentalUnit.unitReference
                    area rentalUnit.area
                    if(rentalUnit.deleted)
                        deleted rentalUnit.deleted
                }
            }
        };

        return jsonBuilder;
    }

    @Transactional
    PropertyTmp prepareForEditing(Property property) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(property.dirtyStatus == CheckStatus.EDITING && property.propertyTmp) {
            return property.propertyTmp;
        }

        PropertyTmp propertyTmp = new PropertyTmp();
        propertyTmp.with {
            name = property.name;
            address = property.address;
            description = property.description
            area = property.area
            commission = property.commission;
            chargeVat = property.chargeVat
            //fullyConfigured = property.fullyConfigured

            propertyType = property.propertyType
            propertyManager = property.propertyManager

            mandateDate = property.mandateDate
            lastInspectionDate = property.lastInspectionDate
            terminatedDate = property.terminatedDate
            terminated = property.terminated

            propertyCode = property.propertyCode
            notes = property.notes
            standNumber = property.standNumber

            deleted = property.deleted
            deletedDate = property.deletedDate

            landlord = property.landlord
            userAction = UserAction.UPDATE
            createdBy = currentUser.username;
        }
        propertyTmp.property = property;
        propertyTmp.save(failOnError: true);

        // prepare the rentalUnits for the temporary object
        // now we update the rentalUnits.
        property.rentalUnits.each { RentalUnit rentalUnit ->

            if(rentalUnit.deleted)
                return;

            RentalUnitTmp rentalUnitTmp = new RentalUnitTmp();
            rentalUnitTmp.with {
                loggable = false
                name = rentalUnit.name
                unitReference = rentalUnit.unitReference
                area = rentalUnit.area
                unitStatus = rentalUnit.unitStatus
                deleted = false
            }

            rentalUnitTmp.rentalUnit = rentalUnit;
            rentalUnitTmp.property = propertyTmp;
            //propertyTmp.addToRentalUnits(rentalUnitTmp);
            rentalUnitTmp.save();
        }


        // save the property record
        property.dirtMaker = currentUser;
        property.dirtyStatus = CheckStatus.EDITING;
        property.propertyTmp = propertyTmp;
        property.save();

        return propertyTmp;
    }

    Map getOccupancyStats(Property propertyUnit) {

        def queryString = """
        select 
            sum(case when rs.code = 'un-occupied' then 1 else 0 end) vacant_units,
            sum(case when rs.code = 'un-occupied' then r.area else 0 end) vacant_area,
            sum(1) total_units,
            sum(r.area) total_area,
            max(p.area) property_area,
            sum(case when rs.code = 'occupied' then 1 else 0 end) occupied_units,
            sum(case when rs.code = 'occupied' then r.area else 0 end) occupied_area
        from rental_unit r 
        inner join rental_unit_status rs on rs.id=r.unit_status_id
        inner join property p on p.id=r.property_id 
        where p.id = ${propertyUnit.id}
        """
        SQLQuery sqlQuery = sessionFactory.currentSession.createSQLQuery(queryString);
        List results = sqlQuery.list()[0];

        Long totalUnits = results[2] as Long;
        Long occupiedUnits = results[5] as Long;
        BigDecimal occupancyRate = 0.0;

        if(occupiedUnits != null && totalUnits != null) {
            occupancyRate = occupiedUnits / totalUnits;
        }

        return [
                vacantUnits: results[0] as Long,
                vacantArea: results[1] as BigDecimal,
                totalUnits: totalUnits,
                totalArea: results[3] as BigDecimal,
                propertyArea: results[4] as BigDecimal,
                occupiedUnits: occupiedUnits,
                occupiedArea: results[6] as BigDecimal,
                occupancyRate: occupancyRate,
                occupancyRatePerc: occupancyRate * 100.0
        ]
    }

}