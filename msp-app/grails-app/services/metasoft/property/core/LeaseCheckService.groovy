package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.json.JsonSlurper
import org.grails.web.json.JSONObject

class LeaseCheckService {
    
    SpringSecurityService springSecurityService
    LeaseService leaseService;

    SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser
    }

    Long count() {
        LeaseCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    LeaseCheck get(Serializable id) {
        LeaseCheck.get(id);
    }

    Lease getEntity(LeaseCheck leaseCheck) {
        Lease lease = leaseCheck.entity;

        // prepare t
        lease.leaseItems.clear();
        lease.leaseItems = []

        // update the entity with the json data
        def json = new JsonSlurper().parseText(leaseCheck.jsonData);
        lease.setProperties(json);

        return lease;
    }

    List<LeaseCheck> list(Map args) {
        LeaseCheck.list();
    }

    List<LeaseCheck> listInbox(Map args) {
        LeaseCheck.where{
            checkStatus == CheckStatus.PENDING
        }.join('entity').list();
    }

    List<LeaseCheck> listOutbox(Map args) {
        LeaseCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    LeaseCheck save(LeaseCheck leaseCheck) {
        leaseCheck.save(failOnError: true);
    }

    @Transactional
    LeaseCheck approve(LeaseCheck leaseCheck) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(leaseCheck.isChecked()) {
            // already checked.
            return leaseCheck;
        }

        // the check entity
        leaseCheck.checkStatus = CheckStatus.APPROVED;
        leaseCheck.checker = currentUser;
        leaseCheck.checkDate = new Date();
        leaseCheck.checkComment = "Approved";
        leaseCheck.save(failOnError: true);

        // Lease entity object.
        Lease leaseObject = leaseCheck.entity;

        if(leaseCheck.actionName in ['CREATE']) {
            // retrieve the jsonData
            Map args = new JsonSlurper().parseText(leaseCheck.jsonData) as JSONObject;
            leaseObject.setProperties(args);

            // if this is a create action request.
            if(leaseCheck.actionName == 'CREATE' && leaseObject.leaseNumber == null) {
                // retrieve the leaseNumber for the new Lease and prepare the lease
                leaseObject.leaseNumber  = leaseService.generateLeaseNumber();
                leaseObject.leaseStatus = LeaseStatus.findByCode('active');
                leaseObject.lastReviewDate = leaseObject.leaseStartDate;
            }
        } else if(leaseCheck.actionName in ['UPDATE']) {
            // retrieve the jsonData
            Map args = new JsonSlurper().parseText(leaseCheck.jsonData) as JSONObject;

            // remove these ones.
            LeaseItem.where{lease == leaseObject}.deleteAll();
            leaseObject.leaseItems.clear();

            args['leaseItems'].each { it ->
                leaseObject.addToLeaseItems( it );
            }
            leaseObject.lastReviewDate = new Date();
        }

        leaseObject.checkStatus = CheckStatus.APPROVED;
        leaseObject.dirtyStatus = leaseCheck.checkStatus;
        leaseObject.checkDate = new Date();
        leaseObject.checker = currentUser;
        leaseObject.save(); // save the lease

        return leaseCheck;
    }

    @Transactional
    LeaseCheck cancel(LeaseCheck leaseCheck, Map params) {

        if(leaseCheck.isChecked()) {
            // already checked.
            return leaseCheck;
        }
        log.info "is it here where am getting ??"

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        // the check entity
        leaseCheck.checkStatus = CheckStatus.CANCELLED;
        leaseCheck.checker = currentUser;
        leaseCheck.checkDate = new Date();
        leaseCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(leaseCheck.actionName == 'CREATE') {
            Lease lease = leaseCheck.entity;
            lease.checkStatus = CheckStatus.CANCELLED;
            lease.dirtyStatus = CheckStatus.CANCELLED;
            lease.save(failOnError: true);
        } else {
            Lease lease = leaseCheck.entity;
            lease.checkStatus = CheckStatus.APPROVED;
            lease.dirtyStatus = CheckStatus.APPROVED; // go back to the original status
            lease.save(failOnError: true);
        }
        leaseCheck.save(failOnError: true);
        return leaseCheck;
    }

    @Transactional
    LeaseCheck reject(LeaseCheck leaseCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(leaseCheck.isChecked()) {
            // already checked.
            return leaseCheck;
        }

        // the check entity
        leaseCheck.checkStatus = CheckStatus.REJECTED;
        leaseCheck.checker = currentUser;
        leaseCheck.checkDate = new Date();
        leaseCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(leaseCheck.actionName == 'CREATE') {
            Lease lease = leaseCheck.entity;
            lease.checkStatus == CheckStatus.REJECTED;
            lease.save(failOnError: true);
        } else {
            Lease lease = leaseCheck.entity;
            lease.checkStatus == CheckStatus.APPROVED;
            lease.dirtyStatus = CheckStatus.APPROVED; // go back to the original status
            lease.save(failOnError: true);
        }
        leaseCheck.save(failOnError: true);
        return leaseCheck;
    }


}