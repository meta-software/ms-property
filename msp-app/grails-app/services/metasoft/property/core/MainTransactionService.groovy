package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional

//@GrailsCompileStatic
@Transactional
class MainTransactionService {

    AccountService accountService;

    MainTransaction get(Serializable id) {
        return MainTransaction.get(id);
    }

    List<MainTransaction> listByTransactionBatch(Long id) {

        Map args = [:]
        args['order'] = 'asc';
        args['sort'] = 't.id';

        String whereClause = "where t.transactionBatch.id = '${id}'";

        return MainTransaction.executeQuery("""
            select new map(
                t.accountNumberCr as creditAccount,
                t.credit as credit,
                t.debit as debit,
                t.credit + t.debit as transactionAmount,
                tt.name as transactionType,
                t.transactionReference as transactionReference,
                t.description as description,
                t.transactionDate as transactionDate,
                coalesce(ga.accountName, ca.accountName) as creditAccountName,
                t.accountNumberDb as debitAccount,
                coalesce(gd.accountName, da.accountName) as debitAccountName,
                sc.accountNumber as subAccountNumberCr,
                sc.accountName as subAccountNameCr,
                sd.accountNumber as subAccountNumberDb,
                sd.accountName as subAccountNameDb
            )
            from MainTransaction t 
            inner join TransactionType tt on tt.id=t.transactionType.id
            left join Account ca on ca.accountNumber = t.accountNumberCr
            left join GeneralLedger ga on ga.accountNumber = t.accountNumberCr
            left join Account da on da.accountNumber = t.accountNumberDb
            left join GeneralLedger gd on gd.accountNumber = t.accountNumberDb
            inner join SubAccount sc on sc.accountNumber=t.subAccountNumberCr
            inner join SubAccount sd on sd.accountNumber=t.subAccountNumberDb
            ${whereClause}
        """.toString(), [:], args)

    }

    List<MainTransaction> list(Map args) {
        return MainTransaction.list(args);
    }

    List<MainTransaction> listDev(Map args) {

        args['order'] = 'asc';
        args['sort'] = 'transactionDate';

        String txRef = args['txref'];
        String cracc = args['cracc'];
        String dbacc = args['dbacc'];

        Date txnDate = MetasoftUtil.parseDate((String)args['txndate'])?.clearTime();

        String whereClause = "where 1=1 ";
        if(txnDate) { whereClause += " and t.transactionDate = '${txnDate.format('yyyy-MM-dd')}'" }
        if(txRef) { whereClause += " and t.transactionReference LIKE '${txRef}%'" }
        if(cracc) { whereClause += " and t.accountNumberCr like '${cracc}%'" }
        if(dbacc) { whereClause += " and t.accountNumberDb like '${dbacc}%'" }

        if(!txRef) {
            return []
        }

        return MainTransaction.executeQuery("""
            select new map(
                t.accountNumberCr as creditAccount,
                t.transactionCurrency.id as transactionCurrency, 
                t.credit as credit,
                t.debit as debit,
                t.transactionCurrency.id as transactionCurrency,
                t.credit as credit,
                t.debit as debit,
                t.credit + t.debit as transactionAmount,
                t.operatingCurrency.id as operatingCurrency,
                t.creditOperatingAmount as creditOperatingAmount,
                t.debitOperatingAmount as debitOperatingAmount,
                t.reportingCurrency.id as reportingCurrency,
                t.creditReportingAmount as creditReportingAmount,
                t.debitReportingAmount as debitReportingAmount,
                tt.name as transactionType,
                t.transactionReference as transactionReference,
                t.description as description,
                t.transactionDate as transactionDate,
                coalesce(ga.accountName, ca.accountName) as creditAccountName,
                t.accountNumberDb as debitAccount,
                coalesce(gd.accountName, da.accountName) as debitAccountName,
                sc.accountNumber as subAccountNumberCr,
                sc.accountName as subAccountNameCr,
                sd.accountNumber as subAccountNumberDb,
                sd.accountName as subAccountNameDb
            )
            from MainTransaction t 
            inner join TransactionType tt on tt.id=t.transactionType.id
            left join Account ca on ca.accountNumber = t.accountNumberCr
            left join GeneralLedger ga on ga.accountNumber = t.accountNumberCr
            left join Account da on da.accountNumber = t.accountNumberDb
            left join GeneralLedger gd on gd.accountNumber = t.accountNumberDb
            inner join SubAccount sc on sc.accountNumber=t.subAccountNumberCr
            inner join SubAccount sd on sd.accountNumber=t.subAccountNumberDb
            ${whereClause}
        """.toString(), [:], args)
    }

    List<MainTransaction> searchTransactions(Map args) {

        args['order'] = 'asc';
        args['sort'] = 'transactionDate';

        String subacc = args['subacc'];
        subacc = subacc == '-1' ? null : subacc;
        String txRef = args['txref'];
        String cracc = args['cracc'];
        String dbacc = args['dbacc'];

        Date startDate = MetasoftUtil.parseDate((String)args['startdate']);
        Date endDate = MetasoftUtil.parseDate((String)args['enddate']);
        Date txnDate = MetasoftUtil.parseDate((String)args['txndate'])?.clearTime();

        String whereClause = "where t.parentTransaction = true";
        if(txnDate) { whereClause += " and t.transactionDate = '${txnDate.format('yyyy-MM-dd')}'" }
        if(txRef) { whereClause += " and t.transactionReference LIKE '${txRef}%'" }
        if(cracc) { whereClause += " and t.accountNumberCr like '${cracc}%'" }
        if(dbacc) { whereClause += " and t.accountNumberDb like '${dbacc}%'" }
        if(subacc) { whereClause += " and t.subAccountNumberCr = '$subacc'" }

        return MainTransaction.executeQuery("""
            select new map(
                t.accountNumberCr as creditAccount,
                t.transactionCurrency.id as transactionCurrency, 
                t.credit as credit,
                t.debit as debit,
                t.transactionCurrency.id as transactionCurrency,
                t.credit as credit,
                t.debit as debit,
                t.credit + t.debit as transactionAmount,
                t.operatingCurrency.id as operatingCurrency,
                t.creditOperatingAmount as creditOperatingAmount,
                t.debitOperatingAmount as debitOperatingAmount,
                t.reportingCurrency.id as reportingCurrency,
                t.creditReportingAmount as creditReportingAmount,
                t.debitReportingAmount as debitReportingAmount,
                tt.name as transactionType,
                t.transactionReference as transactionReference,
                t.description as description,
                t.transactionDate as transactionDate,
                coalesce(ga.accountName, ca.accountName) as creditAccountName,
                t.accountNumberDb as debitAccount,
                coalesce(gd.accountName, da.accountName) as debitAccountName,
                sc.accountNumber as subAccountNumberCr,
                sc.accountName as subAccountNameCr,
                sd.accountNumber as subAccountNumberDb,
                sd.accountName as subAccountNameDb
            )
            from MainTransaction t 
            inner join TransactionType tt on tt.id=t.transactionType.id
            left join Account ca on ca.accountNumber = t.accountNumberCr
            left join GeneralLedger ga on ga.accountNumber = t.accountNumberCr
            left join Account da on da.accountNumber = t.accountNumberDb
            left join GeneralLedger gd on gd.accountNumber = t.accountNumberDb
            inner join SubAccount sc on sc.accountNumber=t.subAccountNumberCr
            inner join SubAccount sd on sd.accountNumber=t.subAccountNumberDb
            ${whereClause}
        """.toString(), [:], args)
    }

    List<MainTransaction> listCancellableTransactions(Map args) {
        args['order'] = 'asc';
        args['sort'] = 'transactionReference';
        args['max'] = args.max ?: 10;

        String reference = args?.ref;

        def query = MainTransaction.where {
            !(transactionType.transactionTypeCode in ["CN", "REF"])
            transactionReference ==~ "${reference}%"
            parentTransaction == true
            cancelled == false
            refunded == false
        }
        List<MainTransaction> propertyList = query.list(args);

        return propertyList;
    }

    List<MainTransaction> listReceipts(Map args) {

        args['order'] = 'asc';
        args['sort'] = 'transactionReference';
        args['max'] = args.max ?: 10;

        String reference = args?.ref;

        def query = MainTransaction.where {
            transactionType.transactionTypeCode == "RC"
            transactionReference ==~ "${reference}%"
            parentTransaction == true
            cancelled == false
            refunded == false
        }
        List<MainTransaction> transactions = query.list(args);

        return transactions;
    }

    Boolean existsByTransactionReference(txnReference) {
        def result = MainTransaction.where {
            transactionReference == txnReference
            cancelled == false
        }.count();

        return (result > 0);
    }

    List<MainTransaction> listAccountInvoices(Long accountId) {

        Account account = accountService.get(accountId);

        Map args = [max: 10, order: 'asc', sort: 'dateCreated']

        List<MainTransaction> transactions = MainTransaction.where {
            transactionType.transactionTypeCode == "IV"
            accountNumberCr == account?.accountNumber
            parentTransaction == true
        }.list(args)

        return transactions;
    }

    List<MainTransaction> listAccountReceipts(Long accountId) {

        Account account = accountService.get(accountId);

        Map args = [max: 10, order: 'desc', sort: 'dateCreated']

        List<MainTransaction> transactions = MainTransaction.where {
            transactionType.transactionTypeCode == "RC"
            accountNumberCr == account?.accountNumber
            parentTransaction == true
        }.list(args)

        return transactions;
    }

    List<MainTransaction> listAccountTransactions(Long accountId) {

        Account account = accountService.get(accountId);

        Map args = [max: 20, order: 'desc', sort: 'transactionDate']

        List<MainTransaction> transactions = MainTransaction.where {
            accountNumberCr == account?.accountNumber
            parentTransaction == true
        }.list(args)

        return transactions;
    }

    Long count(def params = [:]) {
        return MainTransaction.count();
    }

    //@todo: Add a transaction method for each different transaction with the necessary legs as specified.
    @Transactional
    MainTransaction save(MainTransaction mainTransaction) {
        mainTransaction.save(failOnError: true)
        return mainTransaction;
    }

}
