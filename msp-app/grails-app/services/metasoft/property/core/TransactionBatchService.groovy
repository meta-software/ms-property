package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.web.databinding.DataBinder

@GrailsCompileStatic
class TransactionBatchService implements DataBinder{

    SubAccountService subAccountService
    ConfigSettingService configSettingService
    TransactionBatchTmpService transactionBatchTmpService

    TransactionBatch get(Serializable id) {
        return TransactionBatch.get(id);
    }

    List<TransactionBatch> list(Map args) {
        return TransactionBatch.list(args);
    }

    List<TransactionBatch> searchBatches(Map args) {

        args['order'] = 'desc';
        args['sort']  = 'id';
        args['max']   = 50;

        String txRef = args['txref'];
        String trantype = args['trantype'];

        Date startDate = MetasoftUtil.parseDate((String)args['startdate']);
        Date endDate = MetasoftUtil.parseDate((String)args['enddate']);
        Date txnDate = MetasoftUtil.parseDate((String)args['txndate'])?.clearTime();

        String whereClause = "where 1 = 1";
        if(txnDate) { whereClause += " and t.dateCreated = '${txnDate}'" }
        if(txRef) { whereClause += " and t.batchNumber LIKE '%${txRef}'" }
        if(trantype) { whereClause += " and t.transactionType.id = ${trantype}" }

        return (List<TransactionBatch>)TransactionBatch.executeQuery("""
            select t
            from TransactionBatch t 
            ${whereClause} """.toString(), [:], args)
    }

    void receipting(ReceiptItem receiptItem, TransactionBatch transactionBatch) {

        Receipt receipt = receiptItem.receipt;

        Property property = receiptItem.lease.property
        Landlord landlord = property.landlord;

        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger tenantTrustAccount = GeneralLedger.findByAccountName('LESSEES TRUST CONTROL');
        SubAccount subAccount = receiptItem.subAccount;
        SubAccount suspenseSubAccount = subAccountService.findByAccount(subAccount.suspenseAccount);

        BigDecimal commissionAmount = 0.0;
        BigDecimal vatAmount = 0.0;

        //leg 1: the tenant transaction leg
        log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
        MainTransaction tenantTransaction = toMainTransaction(receiptItem);
        tenantTransaction.parentTransaction = true;
        tenantTransaction.transactionBatch = transactionBatch;

        // Set the credit account number
        tenantTransaction.accountNumberCr = receipt.tenant.accountNumber;
        tenantTransaction.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        tenantTransaction.accountNumberDb = receipt.debitAccount.accountNumber;
        tenantTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

        // Set the credit/debit amounts
        tenantTransaction.debit  = 0.0;
        tenantTransaction.credit = receiptItem.allocatedAmount;

        //@ calculate the VAT amount value sub-leg
        if(subAccount.hasVat && property?.chargeVat) {
            //@ todo: Add VAT application logic here, or rather add a method to help with that.
            Map vatTransaction = this.vatReceipt(receiptItem, transactionBatch, landlord);

            tenantTransaction.vatAmount = vatTransaction['vatAmount'] as BigDecimal;
            tenantTransaction.vatTransactionAmount = vatTransaction['vatAmount'] as BigDecimal;
            tenantTransaction.vat = vatTransaction['vat'] as BigDecimal;
        }

        tenantTransaction = transactionBatchTmpService.applyExchangeRates(tenantTransaction, tenantTransaction.operatingCurrency, tenantTransaction.reportingCurrency);
        tenantTransaction.save();

        //leg 2: the tenant control leg
        log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
        MainTransaction tenantControlLeg = toMainTransaction(receiptItem);
        tenantControlLeg.transactionBatch = transactionBatch;

        // Set the credit account number
        tenantControlLeg.accountNumberCr = tenantTrustAccount.accountNumber;
        tenantControlLeg.subAccountNumberCr = subAccount.accountNumber;

        // Set the debit account number
        tenantControlLeg.accountNumberDb = receipt.tenant.accountNumber;
        tenantControlLeg.subAccountNumberDb = '0'; //@todo: need to add logic here.

        // Set the credit/debit amounts
        tenantControlLeg.debit  = 0.0;
        tenantControlLeg.credit = receiptItem.allocatedAmount;
        tenantControlLeg = transactionBatchTmpService.applyExchangeRates(tenantControlLeg, tenantControlLeg.operatingCurrency, tenantControlLeg.reportingCurrency);
        tenantControlLeg.save();

        //START: The commission legs for the sub account. 3 LEGS
        if(subAccount.hasCommission) {
            commissionAmount = receiptItem.allocatedAmount * property.commission/100.0;
            GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMMISSION'); //retrieve the commission general ledger account details

            // 1. do the main part of the commission leg
            log.debug("processing the main part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction transaction = toMainTransaction(receiptItem);
            transaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            transaction.accountNumberCr = commissionLedger.accountNumber;
            transaction.subAccountNumberCr = commissionLedger.subAccount.accountNumber;

            // Set the debit account number
            transaction.accountNumberDb = landlord.accountNumber;
            transaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            transaction.credit = commissionAmount;
            transaction.debit = 0.0;

            transaction = transactionBatchTmpService.applyExchangeRates(transaction, transaction.operatingCurrency, transaction.reportingCurrency);

            transaction.save(failOnError: true)

            // 2. do the suspense leg of the commission
            log.debug("processing the control part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlSuspenseTransaction = toMainTransaction(receiptItem);
            controlSuspenseTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission control leg
            controlSuspenseTransaction.accountNumberCr = landlord.accountNumber;
            controlSuspenseTransaction.subAccountNumberCr = commissionLedger.subAccount.suspenseAccount;

            // Set the debit account number
            controlSuspenseTransaction.accountNumberDb = commissionLedger.accountNumber;
            controlSuspenseTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlSuspenseTransaction.debit = commissionAmount ;
            controlSuspenseTransaction.credit = 0.0;
            controlSuspenseTransaction = transactionBatchTmpService.applyExchangeRates(controlSuspenseTransaction, controlSuspenseTransaction.operatingCurrency, controlSuspenseTransaction.reportingCurrency);
            controlSuspenseTransaction.save(failOnError: true)

            // 3. do the control part of the commission
            log.debug("processing the control part of transaction commission leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = toMainTransaction(receiptItem);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission control leg
            controlTransaction.accountNumberCr = landlordTrustAccount.accountNumber;
            controlTransaction.subAccountNumberCr = '0';

            // Set the debit account number
            controlTransaction.accountNumberDb = landlord.accountNumber;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlTransaction.debit = commissionAmount ;
            controlTransaction.credit = 0.0;

            controlTransaction = transactionBatchTmpService.applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);

            controlTransaction.save(failOnError: true)

            // perform the vat on commission legs
            if(commissionAmount > 0 ) {
                this.vatCommission(receiptItem, transactionBatch, landlord, commissionAmount);
            }

        }
        //END: Finish the 3 commission legs of the transaction

        // configure if the transaction has ledger (bank, etc.., known transaction type)
        if(subAccount.hasLedger) {

            //if this is a deposit transaction then do a deposit held transaction, otherwise send the money to the landlord
            if(subAccount.accountNumber == MetasoftUtil.DEPOSIT_ACCOUNT_NUMBER) {

            } else {
                // 1. do the main part of the ledger leg
                log.debug("processing the main part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
                MainTransaction mainTransaction = toMainTransaction(receiptItem);
                mainTransaction.transactionBatch = transactionBatch;

                // Set the credit account number for commission
                mainTransaction.accountNumberCr = receipt.tenant.accountNumber;
                mainTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

                // Set the debit account number
                mainTransaction.accountNumberDb = landlord.accountNumber;
                mainTransaction.subAccountNumberDb = '0'; //@todo: need to add logic here.

                // Set the credit/debit amounts
                mainTransaction.credit = 0.0;
                mainTransaction.debit  = receiptItem.allocatedAmount;

                mainTransaction = transactionBatchTmpService.applyExchangeRates(mainTransaction, mainTransaction.operatingCurrency, mainTransaction.reportingCurrency);

                mainTransaction.save(failOnError: true);
            }

            // 2. add logic for the control leg
            log.debug("processing the control part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = toMainTransaction(receiptItem);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            controlTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
            controlTransaction.subAccountNumberCr = suspenseSubAccount.accountNumber;

            // Set the debit account number
            controlTransaction.accountNumberDb = receipt.tenant.accountNumber;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlTransaction.credit = 0.0;
            controlTransaction.debit = receiptItem.allocatedAmount;

            controlTransaction = transactionBatchTmpService.applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);
            controlTransaction.save(failOnError: true);

            // 3. the alternate transaction for double entry purposes
            log.debug("processing the main(alternate) part of transaction ledger leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction mainTransaction2 = toMainTransaction(receiptItem);
            mainTransaction2.transactionBatch = transactionBatch;

            // Set the
            mainTransaction2.accountNumberCr = receipt.debitAccount.accountNumber;
            mainTransaction2.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            mainTransaction2.accountNumberDb = receipt.tenant.accountNumber;
            mainTransaction2.subAccountNumberDb = '0'; //@todo: need to add logic here.

            // Set the credit/debit amounts
            mainTransaction2.debit = receiptItem.allocatedAmount;
            mainTransaction2.credit = 0.0;

            mainTransaction2 = transactionBatchTmpService.applyExchangeRates(mainTransaction2, mainTransaction2.operatingCurrency, mainTransaction2.reportingCurrency);
            mainTransaction2.save(failOnError: true);
        }

        //START: do the deposit Charge specific transaction legs
        if(subAccount.accountNumber == MetasoftUtil.DEPOSIT_ACCOUNT_NUMBER) {
            SubAccount depositSuspenseSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_SUSPENSE_ACCOUNT_NUMBER);

            // 1. process main transaction (deposit charge suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction depositSuspenseTransaction = toMainTransaction(receiptItem);
            depositSuspenseTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            depositSuspenseTransaction.accountNumberCr = receipt.tenant.accountNumber;
            depositSuspenseTransaction.subAccountNumberCr = depositSuspenseSubAccount.accountNumber;

            // Set the debit account number
            depositSuspenseTransaction.accountNumberDb = receipt.tenant.accountNumber;
            depositSuspenseTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            depositSuspenseTransaction.credit = 0.0;
            depositSuspenseTransaction.debit = receiptItem.allocatedAmount;
            depositSuspenseTransaction = transactionBatchTmpService.applyExchangeRates(depositSuspenseTransaction, depositSuspenseTransaction.operatingCurrency, depositSuspenseTransaction.reportingCurrency);

            depositSuspenseTransaction.save(failOnError: true);

            // 2. process the deposit held suspense transaction leg
            log.debug("processing the control part of depositSuspenseTransaction leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction depositHeldSuspenseTransaction = toMainTransaction(receiptItem);
            depositHeldSuspenseTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            depositHeldSuspenseTransaction.accountNumberCr = tenantTrustAccount.accountNumber;
            depositHeldSuspenseTransaction.subAccountNumberCr = MetasoftUtil.DEPOSIT_ACCOUNT_NUMBER;

            // Set the debit account number
            depositHeldSuspenseTransaction.accountNumberDb    = receipt.tenant.accountNumber;
            depositHeldSuspenseTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            depositHeldSuspenseTransaction.debit = 0.0;
            depositHeldSuspenseTransaction.credit  = receiptItem.allocatedAmount;
            depositHeldSuspenseTransaction = transactionBatchTmpService.applyExchangeRates(depositHeldSuspenseTransaction, depositHeldSuspenseTransaction.operatingCurrency, depositHeldSuspenseTransaction.reportingCurrency);
            depositHeldSuspenseTransaction.save(failOnError: true);

            //Do the depositHeld transaction leg
            // 3. perform the deposit held transaction here.
            SubAccount depositHeldAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_HELD_ACCOUNT_NUMBER)
            log.debug("processing the depositHeld part of transaction leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction depositHeldTransaction = toMainTransaction(receiptItem);
            depositHeldTransaction.transactionBatch = transactionBatch;

            // Set the credit account number for commission
            depositHeldTransaction.accountNumberCr = receipt.tenant.accountNumber;
            depositHeldTransaction.subAccountNumberCr = depositHeldAccount.accountNumber;

            // Set the debit account number
            depositHeldTransaction.accountNumberDb = receipt.tenant.accountNumber;
            depositHeldTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            depositHeldTransaction.credit = receiptItem.allocatedAmount;
            depositHeldTransaction.debit  = 0.0;

            depositHeldTransaction = transactionBatchTmpService.applyExchangeRates(depositHeldTransaction, depositHeldTransaction.operatingCurrency, depositHeldTransaction.reportingCurrency);
            depositHeldTransaction.save(failOnError: true);
        }
        //END: finish the deposit Charge specific transaction legs

        //START: do the landlord legs of the transaction
        if(landlord?.accountNumber != null && subAccount.accountName != 'Deposit') {

            BigDecimal landlordAmount = (receiptItem.allocatedAmount - commissionAmount - vatAmount);

            // 1. process main transaction (suspense leg)
            log.debug("processing the nominal part of transaction transaction batch #${transactionBatch.batchNumber}");
            MainTransaction landlordTransaction = toMainTransaction(receiptItem);
            landlordTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            landlordTransaction.accountNumberCr = landlord.accountNumber;
            landlordTransaction.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            landlordTransaction.accountNumberDb = receipt.tenant.accountNumber;
            landlordTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            landlordTransaction.debit = 0.0;
            landlordTransaction.credit = landlordAmount //receiptItem.allocatedAmount;

            landlordTransaction = transactionBatchTmpService.applyExchangeRates(landlordTransaction, landlordTransaction.operatingCurrency, landlordTransaction.reportingCurrency);
            landlordTransaction.save();

            // 2. the landlord control-account leg
            log.debug("processing the control part of tenant leg transaction batch #${transactionBatch.batchNumber}");
            MainTransaction controlTransaction = toMainTransaction(receiptItem);
            controlTransaction.transactionBatch = transactionBatch;

            // Set the credit account number
            controlTransaction.accountNumberCr = landlordTrustAccount.accountNumber;
            controlTransaction.subAccountNumberCr = subAccount.accountNumber;

            // Set the debit account number
            controlTransaction.accountNumberDb = landlord.accountNumber;
            controlTransaction.subAccountNumberDb = '0';

            // Set the credit/debit amounts
            controlTransaction.credit = landlordAmount //receiptItem.allocatedAmount;
            controlTransaction.debit = 0.0;

            controlTransaction = transactionBatchTmpService.applyExchangeRates(controlTransaction, controlTransaction.operatingCurrency, controlTransaction.reportingCurrency);
            controlTransaction.save(failOnError: true);
        }
        //END: finish the landlord legs of the transaction

    }

    @Transactional
    void vatCommission(ReceiptItem receiptItem, TransactionBatch transactionBatch, Landlord landlord, BigDecimal commissionAmount) {
        Receipt receipt = receiptItem.receipt;

        log.info("perform the vat on commission legs for the transactionRef = #${receipt.transactionReference}");

        SubAccount vatCommissionSubAccount = SubAccount.findByAccountName('VAT on Commission')
        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMMISSION'); //retrieve the commission general ledger account details        SubAccount vatRentSubAccount = SubAccount.findByAccountName('VAT on Rent')

        BigDecimal vatPercentage = new BigDecimal(configSettingService.getValue("vat.global.rate", receipt.transactionDate));
        BigDecimal vat = vatPercentage/(vatPercentage + 100.0);

        BigDecimal vatCommissionAmount = commissionAmount * vat;

        // Need to do the second part of the vat transaction legs. [the VAT on commission legs]
        // 1. first leg of the VAT Transaction
        log.info("generate the VAT transaction legs for transaction ref=#${receipt.transactionReference}");
        MainTransaction vatCommissionLeg1 = toMainTransaction(receiptItem);
        vatCommissionLeg1.transactionBatch = transactionBatch;
        vatCommissionLeg1.accountNumberCr = landlord.accountNumber;
        vatCommissionLeg1.accountNumberDb = commissionLedger.accountNumber;
        vatCommissionLeg1.subAccountNumberCr = vatCommissionSubAccount.suspenseAccount;
        vatCommissionLeg1.subAccountNumberDb = '0';
        vatCommissionLeg1.debit = vatCommissionAmount;
        vatCommissionLeg1.credit = 0.0;
        vatCommissionLeg1 = transactionBatchTmpService.applyExchangeRates(vatCommissionLeg1, vatCommissionLeg1.operatingCurrency, vatCommissionLeg1.reportingCurrency);

        vatCommissionLeg1.save(failOnError: true);

        // 2. VAT control leg transaction
        MainTransaction vatCommissionLeg2  = toMainTransaction(receiptItem);
        vatCommissionLeg2.transactionBatch = transactionBatch;
        vatCommissionLeg2.accountNumberCr = landlordTrustAccount.accountNumber;
        vatCommissionLeg2.accountNumberDb = landlord.accountNumber
        vatCommissionLeg2.subAccountNumberCr = vatCommissionSubAccount.suspenseAccount;
        vatCommissionLeg2.subAccountNumberDb = '0';
        vatCommissionLeg2.debit = vatCommissionAmount;
        vatCommissionLeg2.credit = 0.0;
        vatCommissionLeg2 = transactionBatchTmpService.applyExchangeRates(vatCommissionLeg2, vatCommissionLeg2.operatingCurrency, vatCommissionLeg2.reportingCurrency);
        vatCommissionLeg2.save(failOnError: true);

        GeneralLedger vatCommissionLedger = GeneralLedger.findByAccountName('VAT COMMISSION ACCOUNT');
        // 3. suspense transaction of the VAT Transaction
        MainTransaction vatCommissionLeg3  = toMainTransaction(receiptItem);
        vatCommissionLeg3.transactionBatch = transactionBatch;
        vatCommissionLeg3.accountNumberCr = vatCommissionLedger.accountNumber;
        vatCommissionLeg3.accountNumberDb = landlord.accountNumber;
        vatCommissionLeg3.subAccountNumberCr = vatCommissionSubAccount.accountNumber;
        vatCommissionLeg3.subAccountNumberDb = '0';
        vatCommissionLeg3.debit  = 0.0;
        vatCommissionLeg3.credit = vatCommissionAmount;
        vatCommissionLeg3 = transactionBatchTmpService.applyExchangeRates(vatCommissionLeg3, vatCommissionLeg3.operatingCurrency, vatCommissionLeg3.reportingCurrency);
        vatCommissionLeg3.save(failOnError: true);
    }

    @Transactional
    Map vatReceipt(ReceiptItem receiptItem, TransactionBatch transactionBatch, Landlord landlord) {
        Receipt receipt = receiptItem.receipt;
        log.info("perform the receipt vat legs for the transactionRef = #${receipt.transactionReference}");

        SubAccount vatRentSubAccount = SubAccount.findByAccountName('VAT on Rent')
        GeneralLedger landlordTrustAccount = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        GeneralLedger commissionLedger = GeneralLedger.findByAccountName('COMMISSION');

        BigDecimal vatPercentage = new BigDecimal(configSettingService.getValue("vat.global.rate", receipt.transactionDate));
        BigDecimal vat = vatPercentage/(vatPercentage + 100);
        BigDecimal vatAmount = receiptItem.allocatedAmount * vat;

        // 1. first leg of the VAT Transaction
        log.info("generate the VAT transaction legs for transactionRef=#${receipt.transactionReference}");
        MainTransaction vatLeg1 = toMainTransaction(receiptItem);
        vatLeg1.transactionBatch = transactionBatch;
        vatLeg1.accountNumberCr = landlord.accountNumber;
        vatLeg1.accountNumberDb = commissionLedger.accountNumber;
        vatLeg1.subAccountNumberCr = vatRentSubAccount.suspenseAccount;
        vatLeg1.subAccountNumberDb = '0';
        vatLeg1.debit = vatAmount;
        vatLeg1.credit = 0.0;
        vatLeg1 = transactionBatchTmpService.applyExchangeRates(vatLeg1, vatLeg1.operatingCurrency, vatLeg1.reportingCurrency);
        vatLeg1.save(failOnError: true);

        // 2. VAT control leg transaction
        MainTransaction vatLeg2 = toMainTransaction(receiptItem);
        vatLeg2.transactionBatch = transactionBatch;
        vatLeg2.accountNumberCr = landlordTrustAccount.accountNumber;
        vatLeg2.accountNumberDb = landlord.accountNumber
        vatLeg2.subAccountNumberCr = vatRentSubAccount.suspenseAccount;
        vatLeg2.subAccountNumberDb = '0';
        vatLeg2.debit = vatAmount;
        vatLeg2.credit = 0.0;
        vatLeg2 = transactionBatchTmpService.applyExchangeRates(vatLeg2, vatLeg2.operatingCurrency, vatLeg2.reportingCurrency);
        vatLeg2.save(failOnError: true);

        GeneralLedger vatLedger = GeneralLedger.findByAccountName('VAT RENT ACCOUNT');
        // 3. suspense transaction of the VAT Transaction
        MainTransaction vatLeg3  = toMainTransaction(receiptItem);
        vatLeg3.transactionBatch = transactionBatch;
        vatLeg3.accountNumberCr = vatLedger.accountNumber;
        vatLeg3.accountNumberDb = landlord.accountNumber;
        vatLeg3.subAccountNumberCr = vatRentSubAccount.accountNumber;
        vatLeg3.subAccountNumberDb = '0';
        vatLeg3.debit  = 0.0;
        vatLeg3.credit = vatAmount;
        vatLeg3 = transactionBatchTmpService.applyExchangeRates(vatLeg3, vatLeg3.operatingCurrency, vatLeg3.reportingCurrency);
        vatLeg3.save(failOnError: true);

        return [vat: vatPercentage, vatAmount: vatAmount];
    }

    SpringSecurityService springSecurityService
    BillingCycleService billingCycleService
    GrailsApplication grailsApplication

    MainTransaction toMainTransaction(ReceiptItem receiptItem) {
        Receipt receipt = receiptItem.receipt;

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        MainTransaction mainTransaction = new MainTransaction();
        bindData(mainTransaction, receiptItem.properties)
        mainTransaction.transactionType = TransactionType.findByTransactionTypeCode("RC");
        mainTransaction.transactionReference = receipt.transactionReference;
        mainTransaction.transactionDate = receipt.transactionDate;
        mainTransaction.description = receipt.narrative;
        mainTransaction.parentTransaction = false;
        mainTransaction.property = receiptItem.lease.property;
        mainTransaction.lease = receiptItem.lease;
        mainTransaction.rentalUnit = receiptItem.lease.rentalUnit;
        mainTransaction.billingCycle = billingCycleService.getBillingCycle(receipt.transactionDate);
        mainTransaction.datePosted = new Date();
        mainTransaction.postedBy = currentUser?.username ?: grailsApplication.config.get('metasoft.app.audit.defaultUser');

        return mainTransaction;
    }

}
