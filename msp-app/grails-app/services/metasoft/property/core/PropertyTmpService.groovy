package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import org.springframework.validation.BindException

class PropertyTmpService {

    SpringSecurityService springSecurityService;

    @Transactional
    PropertyCheck postRequestProperty(PropertyTmp propertyTmp) {

        // configure if transaction is already loaded
        if(!propertyTmp.isAreaValid()) {
            BindException errors = new BindException(propertyTmp, "propertyTmp")
            errors.rejectValue("area", "propertyTmp.area.nomatch", "Property area and rentalUnits area do not match.")
            def message = "Invalid Action: Property area and rentalUnits area do not match."
            propertyTmp.setErrors(errors);
            throw new ValidationException(message, errors)
        }

        PropertyCheck propertyCheck = new PropertyCheck();
        propertyCheck.maker = springSecurityService.currentUser as SecUser;
        propertyCheck.makeDate = new Date();
        propertyCheck.entityName = "PROPERTY";
        propertyCheck.actionName = propertyTmp.userAction;
        propertyCheck.entityUrl = "#";
        propertyCheck.transactionSource = propertyTmp;
        def jsonBuilder = new JsonBuilder([:])
        propertyCheck.jsonData = jsonBuilder.toString()
        propertyCheck.save(failOnError: true)   // save the transientValue

        propertyTmp.postRequest = true;
        propertyTmp.postRequestBy = springSecurityService.currentUser;
        propertyTmp.datePostRequest = new Date();

        return propertyCheck;
    }

    def toJson() {
        return new JsonBuilder([:]);
    }

    BigDecimal getTotalUnitsArea(PropertyTmp propertyTmp) {
        BigDecimal rentalUnitTotalArea = RentalUnitTmp.totalUnitsArea(propertyTmp.id);
        return rentalUnitTotalArea;
    }

    PropertyTmp get(Serializable id) {
        return PropertyTmp.get(id);
    }

    List<PropertyTmp> list(Map args) {
        return PropertyTmp.where{
            posted == false
            deleted == false
        }.list();
    }

    List<PropertyTmp> listPending(Map args){

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        args['posted'] = false;
        List<PropertyTmp> propertyTmpList = PropertyTmp.where{
            posted == false
            deleted == false
        }.list(args)

        return propertyTmpList;
    }

    Long count(Map args){

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        args['posted'] = false;
        return PropertyTmp.where{
            posted == false
            //createdBy == currentUser.username
            deleted == false
        }.count()
    }

    Long count() {
        return PropertyTmp.where {
            deleted == false
        }.count();
    }

    @Transactional
    PropertyTmp create(PropertyTmp propertyTmp) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        propertyTmp.markDirty('address');
        propertyTmp.userAction = UserAction.CREATE;
        propertyTmp.createdBy = currentUser.username;

        // clear these, they are handled in the approve method
        propertyTmp.save(failOnError: true, flush: true);
        return propertyTmp;
    }

    @Transactional
    Property postProperty(PropertyTmp propertyTmp) {
        // configure if transaction is already loaded
        if(propertyTmp.posted) {
            BindException errors = new BindException(propertyTmp, "propertyTmp")
            errors.rejectValue("posted", "propertyTmp.posted.already", "Property Request is already posted.")
            def message = "Invalid Action: Property Request ${propertyTmp} is already posted."
            throw new ValidationException(message, errors)
        }

        // configure if transaction is already loaded
        /*
        if(propertyTmp.isAreaValid()) {
            BindException errors = new BindException(propertyTmp, "propertyTmp")
            errors.rejectValue("area", "propertyTmp.area.nomatch", "Property area and rentalUnits area do not match.")
            def message = "Invalid Action: Property area and rentalUnits area do not match."
            throw new ValidationException(message, errors)
        } */

        // if the posting is new, create a new Property object
        Property property = retrieveProperty(propertyTmp);
        property.maker = springSecurityService.currentUser as SecUser;
        property.makeDate = new Date();
        property.checker = springSecurityService.currentUser as SecUser;
        property.dirtMaker = springSecurityService.currentUser as SecUser;
        property.checkDate = new Date();
        property.checkStatus = CheckStatus.APPROVED;
        property.dirtyStatus = CheckStatus.APPROVED;
        property.propertyTmp = null;

        // now we update the rentalUnits.
        propertyTmp.rentalUnits.each { RentalUnitTmp rentalUnitTmp ->
            RentalUnit rentalUnit = rentalUnitTmp.rentalUnit ?: new RentalUnit();
            rentalUnit.with {
                name = rentalUnitTmp.name
                unitReference = rentalUnitTmp.unitReference
                area = rentalUnitTmp.area
                unitStatus = rentalUnitTmp.unitStatus
                deleted = rentalUnitTmp.deleted
            }
            property.addToRentalUnits(rentalUnit);
        }

        return property.save(failOnError: true);
    }

    Property retrieveProperty(PropertyTmp propertyTmp) {
        Property property = propertyTmp.property ?: new Property();

        property.with {
            name = propertyTmp.name;
            address = propertyTmp.address;
            description = propertyTmp.description
            area = propertyTmp.area
            commission = propertyTmp.commission;
            chargeVat = propertyTmp.chargeVat
            //fullyConfigured = propertyTmp.fullyConfigured

            propertyType = propertyTmp.propertyType
            propertyManager = propertyTmp.propertyManager

            mandateDate = propertyTmp.mandateDate
            lastInspectionDate = propertyTmp.lastInspectionDate
            terminatedDate = propertyTmp.terminatedDate
            terminated = propertyTmp.terminated

            propertyCode = propertyTmp.propertyCode
            notes = propertyTmp.notes
            standNumber = propertyTmp.standNumber

            deleted = propertyTmp.deleted
            deletedDate = propertyTmp.deletedDate

            landlord = propertyTmp.landlord
        }
        property.propertyTmp = propertyTmp;

        return property;
    }

    @Transactional
    void discardRecord(PropertyTmp propertyTmp) {

        if(!propertyTmp.deleted) {
            propertyTmp.deleted = true;
            propertyTmp.deletedDate = new Date();
            propertyTmp.save(failOnError: true);
        }

    }

}