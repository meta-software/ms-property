package metasoft.property.core

import grails.validation.ValidationException
import metasoft.property.ReceiptAllocationCommand
import org.springframework.validation.BindException

class ReceiptAllocationService {

    PendingReceiptAllocation get(Serializable id) {
        return PendingReceiptAllocation.get(id);
    }

    List<PendingReceiptAllocation> findAllByReceipt(Long receiptId, Map args) {
        return PendingReceiptAllocation.where {
            pendingReceipt.id == receiptId
        }.list(args)
    }

    void delete(Serializable id) {

        PendingReceiptAllocation receiptRequestAllocation = PendingReceiptAllocation.get(id);

        //check validity
        if(receiptRequestAllocation) {
            receiptRequestAllocation.delete(flush: true);
        }
    }

    PendingReceiptAllocation saveAllocation(Long id, ReceiptAllocationCommand receiptAllocationCommand) {
        //@todo: check if we can save
        PendingReceipt pendingReceipt = PendingReceipt.read(id);

        if(pendingReceipt) {

            if( receiptAllocationCommand.allocatedAmount > (pendingReceipt.receiptAmount - pendingReceipt.amountAllocated) ) {
                BindException errors = new BindException(receiptAllocationCommand, "receiptAllocationCommand")
                errors.rejectValue("allocatedAmount", "receiptRequestAllocation.allocatedAmount.greater", "Allocated amount is greater than the receipt balance")
                def message = "Allocated amount is greater than the receipt balance"
                throw new ValidationException(message, errors);
            }

            PendingReceiptAllocation receiptRequestAllocation = receiptAllocationCommand as PendingReceiptAllocation;
            receiptRequestAllocation.pendingReceipt = pendingReceipt;
            receiptRequestAllocation.narrative = pendingReceipt.narration;
            return receiptRequestAllocation.save();
        }
        // throw an exception something is wrong

        return null;
    }

}