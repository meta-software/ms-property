package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.LeaseRenewalRequestCommand
import org.springframework.validation.BindException

@Slf4j
class LeaseRenewalRequestService {

    SpringSecurityService springSecurityService;
    LeaseRenewalRequestCheckService leaseRenewalRequestCheckService;
    LeaseService leaseService;

    LeaseRenewalRequest get(Serializable id) {
        return LeaseRenewalRequest.get(id)
    }

    List<LeaseRenewalRequest> list(Map args) {
        return LeaseRenewalRequest.where {
            dirtyStatus == CheckStatus.PENDING
        }.list(args)
    }

    Long count() {
        return LeaseRenewalRequest.count();
    }

    @Transactional
    LeaseRenewalRequest save(LeaseRenewalRequest leaseRenewalRequest) {
        String hql = "select count(*) from LeaseRenewalRequest lrr where dirtyStatus=:pending || (checkStatus=:approved and processed=false)"
        Boolean leaseTerminateRequestExists = LeaseRenewalRequest.executeQuery(hql, [pending: CheckStatus.PENDING, approved: CheckStatus.APPROVED])[0] > 0;

        if(leaseTerminateRequestExists) {
            Object[] errorArgs = []
            BindException errors = new BindException(leaseRenewalRequest, LeaseRenewalRequest.class.name);
            errors.rejectValue('lease', 'leaseRenewalRequest.lease.exists', errorArgs,"A pending renewal request already exists for the lease")
            ValidationException ve = new ValidationException("A pending renewal request already exists for the lease", errors);
            throw ve
        }

        leaseRenewalRequest.save(failOnError: true);
    }

    @Transactional
    LeaseRenewalRequestTmp createRequest(LeaseRenewalRequestCommand command) {

        LeaseRenewalRequest leaseRenewalRequest = LeaseRenewalRequest.where{
            lease == command.lease
            processed == false
        }.get();

        if(leaseRenewalRequest) {
            Object[] errorArgs = []
            BindException errors = new BindException(command, LeaseRenewalRequestCommand.class.name);
            errors.rejectValue('lease', 'leaseRenewalRequest.lease.exists', errorArgs,"A pending renewal request already exists for the lease")
            ValidationException ve = new ValidationException("A pending renewal request already exists for the lease", errors);
            throw ve
        }

        // perform checks to see if the request is acceptable
        LeaseRenewalRequestTmp leaseRenewalRequestTmp = new LeaseRenewalRequestTmp(command.properties);
        //leaseRenewalRequestTmp.requestedBy = springSecurityService.currentUser as SecUser;
        leaseRenewalRequestTmp.userAction = UserAction.CREATE;

        // also create a routineRequestCheck entity the one to be used for the maker/checking management
        leaseRenewalRequestCheckService.createCheckRecord(leaseRenewalRequestTmp);

        return leaseRenewalRequestTmp.save(failOnError: true);
    }

}