package metasoft.property.core

import grails.gorm.transactions.Transactional

class SubAccountService {

    SubAccount get(Serializable id) {
        return SubAccount.get(id);
    }

    List<SubAccount> list(Map args) {
        return SubAccount.list(args);
    }

    List<SubAccount> listExpenses(Map args) {
        return SubAccount.where{
            subAccountType in ['E']
        }.list(args);
    }

    List<SubAccount> listCosts(Map args) {
        return SubAccount.where{
            subAccountType in ['C']
        }.list(args);
    }

    /**
     *
     * For the sake of listing subAccounts we filter out all the suspense account records.
     *
     * @param args
     * @return
     */
    List<SubAccount> listOptions(Map args) {
        args.max = 10;

        String query = args['q']
        args['sort']= 'accountNumber'
        args['order'] = 'asc'

        return SubAccount.where{
            if(query) {
                (accountName ==~ "${query}%" || accountNumber ==~ "${query}%")
            }
            suspense == false
        }.list(args);
    }

    Long count() {
        return SubAccount.count();
    }

    @Transactional
    SubAccount save(SubAccount subAccount) {
        return subAccount.save(failOnError: true)
    }

    SubAccount findByAccount(String accNum) {
        return SubAccount.where {
            accountNumber == accNum
        }.get();
    }

}