package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.springframework.validation.BindException

class LeaseTerminateRequestCheckService {
    
    SpringSecurityService springSecurityService
    LeaseService leaseService;
    LeaseTerminateRequestService leaseTerminateRequestService

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    @Transactional
    LeaseTerminateRequestCheck createCheckRecord(LeaseTerminateRequestTmp leaseTerminateRequestTmp) {
        LeaseTerminateRequestCheck leaseTerminateRequestCheck = new LeaseTerminateRequestCheck()

        leaseTerminateRequestCheck.transactionSource = leaseTerminateRequestTmp;
        leaseTerminateRequestCheck.actionName = "CREATE";

        leaseTerminateRequestCheck.maker = currentUser;
        leaseTerminateRequestCheck.makeDate = new Date();
        leaseTerminateRequestCheck.entityName = "LEASE_TERMINATE_REQUEST";
        leaseTerminateRequestCheck.actionName = leaseTerminateRequestTmp.userAction;
        leaseTerminateRequestCheck.entityUrl = "#";
        leaseTerminateRequestCheck.transactionSource = leaseTerminateRequestTmp;
        def jsonBuilder = new JsonBuilder([:])
        leaseTerminateRequestCheck.jsonData = jsonBuilder.toString()
        leaseTerminateRequestCheck.save(failOnError: true)   // save the transientValue

        return save(leaseTerminateRequestCheck);
    }

    Long count() {
        LeaseTerminateRequestCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    LeaseTerminateRequestCheck get(Serializable id) {
        LeaseTerminateRequestCheck.get(id);
    }

    LeaseTerminateRequest getEntity(LeaseTerminateRequestCheck leaseTerminateRequestCheck) {
        LeaseTerminateRequest leaseTerminateRequest = leaseTerminateRequestCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(leaseTerminateRequestCheck.jsonData);
        leaseTerminateRequest.setProperties(json);

        return leaseTerminateRequest;
    }

    List<LeaseTerminateRequestCheck> list(Map args) {
        LeaseTerminateRequestCheck.list();
    }

    List<LeaseTerminateRequestCheck> listInbox(Map args) {

        String query = args.get('q')

        def leaseQry = Lease.where {
            tenant.accountName ==~ "%${query}%"
        }.id()

        LeaseTerminateRequestCheck.where {

            if(query) {
                ( transactionSource.lease.leaseNumber ==~ "%${query}%" || transactionSource.lease in (leaseQry))
            }

            checkStatus == CheckStatus.PENDING
        }.join('entity').list(args);
    }

    List<LeaseTerminateRequestCheck> listOutbox(Map args) {
        LeaseTerminateRequestCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    LeaseTerminateRequestCheck save(LeaseTerminateRequestCheck leaseTerminateRequestCheck) {
        leaseTerminateRequestCheck.save(failOnError: true);
    }

    @Transactional
    LeaseTerminateRequestCheck approve(LeaseTerminateRequestCheck leaseTerminateRequestCheck) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(leaseTerminateRequestCheck.isChecked()) {
            BindException errors = new BindException(leaseTerminateRequestCheck, "leaseTerminateRequestCheck")
            errors.rejectValue("checkStatus", "leaseTerminateRequestTmp.checked.already", "Lease Terminate Request is already checked.")
            def message = "Lease Terminate Request is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        leaseTerminateRequestCheck.checkStatus = CheckStatus.APPROVED;
        leaseTerminateRequestCheck.checker = currentUser;
        leaseTerminateRequestCheck.checkDate = new Date();
        leaseTerminateRequestCheck.checkComment = "Approved";

        postLeaseTerminateRequest(leaseTerminateRequestCheck.transactionSource);

        return leaseTerminateRequestCheck.save(failOnError: true, flush: true);;
    }

    @Transactional
    LeaseTerminateRequestCheck reject(LeaseTerminateRequestCheck leaseTerminateRequestCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(leaseTerminateRequestCheck.isChecked()) {
            return leaseTerminateRequestCheck;
        }

        // the check entity
        leaseTerminateRequestCheck.checkStatus = CheckStatus.REJECTED;
        leaseTerminateRequestCheck.checker = currentUser;
        leaseTerminateRequestCheck.checkDate = new Date();
        leaseTerminateRequestCheck.checkComment = params['comment'];

        return leaseTerminateRequestCheck.save(failOnError: true);
    }

    @Transactional
    LeaseTerminateRequestCheck cancel(LeaseTerminateRequestCheck leaseTerminateRequestCheck, Map params) {

        if(leaseTerminateRequestCheck.isChecked()) {
            BindException errors = new BindException(leaseTerminateRequestCheck, "leaseTerminateRequestCheck")
            errors.rejectValue("checkStatus", "leaseTerminateRequestTmp.checked.already", "Lease Terminate Request is already checked.")
            def message = "Lease Terminate Request is already checked."
            throw new ValidationException(message, errors);
        }

        // the check entity
        leaseTerminateRequestCheck.checkStatus = CheckStatus.CANCELLED;
        leaseTerminateRequestCheck.checker = currentUser;
        leaseTerminateRequestCheck.checkDate = new Date();
        leaseTerminateRequestCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        LeaseTerminateRequestTmp leaseTerminateRequestTmp = leaseTerminateRequestCheck.transactionSource;
        //leaseTerminateRequestTmp.posted = true;
        leaseTerminateRequestTmp.save(flush: true);

        return leaseTerminateRequestCheck.save(failOnError: true);
    }

    @Transactional
    LeaseTerminateRequest postLeaseTerminateRequest(LeaseTerminateRequestTmp leaseTerminateRequestTmp) {
        // if the posting is new, create a new Property object
        LeaseTerminateRequest leaseTerminateRequest = retrieveLeaseTerminateRequest(leaseTerminateRequestTmp);
        leaseTerminateRequest.checkStatus = CheckStatus.APPROVED;
        leaseTerminateRequest.dirtyStatus = CheckStatus.APPROVED;
        leaseTerminateRequest.save(failOnError: true, flush: true);

        // the lease will be terminated when the terminate process executes.
        // if date is in the past then terminate the lease right then and there.
        if(leaseTerminateRequest.actionDate <= MetasoftUtil.today()) {
            leaseTerminateRequestService.processRequest(leaseTerminateRequest)
        }

        return leaseTerminateRequest;
    }

    LeaseTerminateRequest retrieveLeaseTerminateRequest(LeaseTerminateRequestTmp leaseTerminateRequestTmp) {
        LeaseTerminateRequest leaseTerminateRequest = leaseTerminateRequestTmp.entity ?: new LeaseTerminateRequest();
        leaseTerminateRequest.setProperties(leaseTerminateRequestTmp.properties);

        leaseTerminateRequest.lease = leaseTerminateRequestTmp.lease;
        leaseTerminateRequest.actionDate = leaseTerminateRequestTmp.actionDate;
        leaseTerminateRequest.actionReason = leaseTerminateRequestTmp.actionReason;
        leaseTerminateRequest.leaseTerminateRequestTmp = leaseTerminateRequestTmp;

        return leaseTerminateRequest;
    }

}