package metasoft.property.core

import grails.converters.JSON
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.NotTransactional
import grails.web.databinding.DataBinder
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import metasoft.property.ExchangeRateGroupCommand
import metasoft.property.core.exceptions.RecordNotFoundException

class ExchangeRateGroupCheckService implements DataBinder {
    
    SpringSecurityService springSecurityService
    //ExchangeRateGroupService exchangeRateGroupService;

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    ExchangeRateGroupCheck get(Serializable id) {

        ExchangeRateGroupCheck exchangeRateGroupCheck = ExchangeRateGroupCheck.get(id);

        if(!exchangeRateGroupCheck) {
            return null;
        }

        exchangeRateGroupCheck.entity = getEntity(exchangeRateGroupCheck)
        return exchangeRateGroupCheck
    }

    ExchangeRateGroup getEntity(ExchangeRateGroupCheck exchangeRateGroupCheck) {
        ExchangeRateGroup exchangeRateGroup = exchangeRateGroupCheck.entity ?: new ExchangeRateGroup()
        def jsonObject = new JsonSlurper().parseText(exchangeRateGroupCheck.jsonData);
        bindData(exchangeRateGroup, jsonObject);    //update the current object in the check object
        return exchangeRateGroup;
    }

    @NotTransactional
    List<ExchangeRateGroupCheck> listInbox(Map args) {
        List<ExchangeRateGroupCheck> exchangeRateGroupChecks = ExchangeRateGroupCheck.where {
            checkStatus == CheckStatus.PENDING
        }.join('entity').list();

        // format the items by repopulating the entity class object.
        exchangeRateGroupChecks.each { ExchangeRateGroupCheck exchangeRateGroupCheck ->
            exchangeRateGroupCheck.entity = exchangeRateGroupCheck.entity ?: new ExchangeRateGroup()
            def jsonObject = new JsonSlurper().parseText(exchangeRateGroupCheck.jsonData);
            bindData(exchangeRateGroupCheck.entity, jsonObject);    //update the current object in the check object
        }

    }

    Long countInbox(Map args) {
        ExchangeRateGroupCheck.where {
            checkStatus == CheckStatus.PENDING
        }.count() as Long;
    }

    List<ExchangeRateGroupCheck> listOutbox(Map args) {
        List<ExchangeRateGroupCheck> exchangeRateGroupChecks = ExchangeRateGroupCheck.where {
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();

        // format the items by repopulating the entity class object.
        exchangeRateGroupChecks.each { ExchangeRateGroupCheck exchangeRateGroupCheck ->
            exchangeRateGroupCheck.entity = exchangeRateGroupCheck.entity ?: new ExchangeRateGroup()
            def jsonObject = new JsonSlurper().parseText(exchangeRateGroupCheck.jsonData);
            bindData(exchangeRateGroupCheck.entity, jsonObject);    //update the current object in the check object
        }

    }

    Long countOutbox(Map args) {
        ExchangeRateGroupCheck.where {
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.count() as Long;
    }

    @Transactional
    ExchangeRateGroupCheck save(ExchangeRateGroupCommand exchangeRateGroupCommand) {
        ExchangeRateGroupCheck exchangeRateGroupCheck = new ExchangeRateGroupCheck();
        exchangeRateGroupCheck.actionName = "CREATE";
        exchangeRateGroupCheck.entityUrl = "#";
        exchangeRateGroupCheck.maker = currentUser;
        exchangeRateGroupCheck.makeDate = new Date();
        exchangeRateGroupCheck.entityName = "ExchangeRateGroupCheck"

        //Register Review domain for JSON rendering
        JSON.registerObjectMarshaller(ExchangeRateGroupCommand) { ExchangeRateGroupCommand cmd ->
            def output = [:]
            output['startDate'] = cmd.startDate.format("yyyy-MM-dd")
            output['endDate'] = cmd.endDate.format("yyyy-MM-dd")
            output['exchangeRates'] = cmd.exchangeRates
            output['active'] = cmd.active

            return output;
        }




        JSON jsonData = new JSON(exchangeRateGroupCommand)
        jsonData.setExcludes(['errors', 'this$0'])
        exchangeRateGroupCheck.jsonData = jsonData.toString()

        //bindData(exchangeRateGroupCheck, exchangeRateGroupCommand)
        exchangeRateGroupCheck.save(failOnError: true);
    }

    @Transactional
    ExchangeRateGroupCheck approve(Long exchangeRateGroupCheckId) {

        ExchangeRateGroupCheck exchangeRateGroupCheck = ExchangeRateGroupCheck.get(exchangeRateGroupCheckId);

        if(!exchangeRateGroupCheck) {
            throw new RecordNotFoundException()
        }

        if(exchangeRateGroupCheck.isChecked()) {
            // already checked.
            return exchangeRateGroupCheck;
        }

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        // the check entity
        exchangeRateGroupCheck.checkStatus = CheckStatus.APPROVED;
        exchangeRateGroupCheck.checker = currentUser;
        exchangeRateGroupCheck.checkDate = new Date();
        exchangeRateGroupCheck.checkComment = "Approved";

        // retrieve the jsonData
        def args = new JsonSlurper().parseText(exchangeRateGroupCheck.jsonData)

        // ExchangeRateGroup entity object.
        ExchangeRateGroup exchangeRateGroup = new ExchangeRateGroup() //exchangeRateGroupCheck.entity;
        bindData(exchangeRateGroup, args)
        exchangeRateGroup.checkStatus = CheckStatus.APPROVED;
        exchangeRateGroup.dirtyStatus = CheckStatus.APPROVED;
        exchangeRateGroup.checkDate = new Date();
        exchangeRateGroup.checker = currentUser;
        exchangeRateGroup.active = true;// activate the exchange rate on approval.

        //adjust the dates on the exchange rates
        for(ExchangeRate exchangeRate : exchangeRateGroup.exchangeRates) {
            exchangeRate.startDate = exchangeRateGroup.startDate
            exchangeRate.endDate = exchangeRateGroup.endDate
            exchangeRate.active = true
        }

        exchangeRateGroup.save();
        //return the approved check object
        return exchangeRateGroupCheck.save(flush: true);
    }

    @Transactional
    ExchangeRateGroupCheck reject(Long exchangeRateGroupCheckId, Map params) {

        ExchangeRateGroupCheck exchangeRateGroupCheck = ExchangeRateGroupCheck.get(exchangeRateGroupCheckId)

        if(exchangeRateGroupCheck.isChecked()) {
            // already checked.
            return exchangeRateGroupCheck;
        }
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        // the check entity
        exchangeRateGroupCheck.checkStatus = CheckStatus.REJECTED;
        exchangeRateGroupCheck.checker = currentUser;
        exchangeRateGroupCheck.checkDate = new Date();
        exchangeRateGroupCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(exchangeRateGroupCheck.actionName != 'CREATE') {
            ExchangeRateGroup exchangeRateGroup = exchangeRateGroupCheck.entity;
            exchangeRateGroup.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            exchangeRateGroup.save();
        }

        return exchangeRateGroupCheck.save();
    }

    @Transactional
    ExchangeRateGroupCheck cancel(Serializable exchangeRateGroupCheckId, Map params) {

        ExchangeRateGroupCheck exchangeRateGroupCheck = ExchangeRateGroupCheck.get(exchangeRateGroupCheckId)

        if(exchangeRateGroupCheck.isChecked()) {
            // already checked.
            return exchangeRateGroupCheck;
        }
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        // the check entity
        exchangeRateGroupCheck.checkStatus = CheckStatus.CANCELLED;
        exchangeRateGroupCheck.checker = currentUser;
        exchangeRateGroupCheck.checkDate = new Date();
        exchangeRateGroupCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(exchangeRateGroupCheck.actionName != 'CREATE') {
            ExchangeRateGroup exchangeRateGroup = exchangeRateGroupCheck.entity;
            exchangeRateGroup.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            exchangeRateGroup.save();
        }
        return exchangeRateGroupCheck.save();
    }

}