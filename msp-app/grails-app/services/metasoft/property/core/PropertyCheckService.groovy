package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.databinding.DataBinder
import groovy.json.JsonSlurper
import org.grails.web.json.JSONObject
import org.springframework.validation.BindException

class PropertyCheckService implements DataBinder{
    
    SpringSecurityService springSecurityService
    PropertyService propertyService;
    PropertyTmpService propertyTmpService

    PropertyTmp getEntitySource(PropertyCheck propertyCheck) {
        PropertyTmp transactionBatchTmp = propertyCheck.transactionSource;

        return transactionBatchTmp;
    }

    SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser
    }

    Long count() {
        PropertyCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    PropertyCheck get(Serializable id) {
        PropertyCheck.get(id);
    }

    Property getEntity(PropertyCheck propertyCheck) {
        Property property = propertyCheck.entity;

        //@ not knowing whether this will hold water for long.
        property.rentalUnits.clear();
        property.rentalUnits = [];

        // update the entity with the json data
        def json = new JsonSlurper().parseText(propertyCheck.jsonData);
        property.setProperties(json);

        return property;
    }

    List<PropertyCheck> list(Map args) {
        PropertyCheck.list();
    }

    List<PropertyCheck> listInbox(Map args) {
        PropertyCheck.where{
            checkStatus == CheckStatus.PENDING
        }.list();
    }

    List<PropertyCheck> listOutbox(Map args) {
        PropertyCheck.where{
            checkStatus in [ CheckStatus.PENDING, CheckStatus.EDITING ]
            maker == currentUser
        }.join('transactionSource').list();
    }

    @Transactional
    PropertyCheck save(PropertyCheck propertyCheck) {
        propertyCheck.save(failOnError: true);
    }

    @Transactional
    PropertyCheck approve(PropertyCheck propertyCheck) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(propertyCheck.isChecked()) {
            BindException errors = new BindException(propertyCheck, "propertyCheck")
            errors.rejectValue("checkStatus", "propertyTmp.checked.already", "Property Request is already checked.")
            def message = "Property Request is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        propertyCheck.checkStatus = CheckStatus.APPROVED;
        propertyCheck.checker = currentUser;
        propertyCheck.checkDate = new Date();
        propertyCheck.checkComment = "Approved";
        propertyCheck.save(failOnError: true);

        propertyTmpService.postProperty(propertyCheck.transactionSource);

        return propertyCheck;
    }

    @Transactional
    PropertyCheck cancel(PropertyCheck propertyCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(propertyCheck.isChecked()) {
            BindException errors = new BindException(propertyCheck, "propertyCheck")
            errors.rejectValue("checkStatus", "propertyTmp.checked.already", "Property Request is already checked.")
            def message = "Property Request is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        propertyCheck.checkStatus = CheckStatus.CANCELLED;
        propertyCheck.checker = currentUser;
        propertyCheck.checkDate = new Date();
        propertyCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        PropertyTmp propertyTmp = propertyCheck.transactionSource;
        propertyTmp.posted = true;
        propertyTmp.save();

        propertyCheck.save(failOnError: true);
        return propertyCheck;
    }
    
    @Transactional
    PropertyCheck reject(PropertyCheck propertyCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(propertyCheck.isChecked()) {
            // already checked.
            //return propertyCheck;
        }

        // the check entity
        propertyCheck.checkStatus = CheckStatus.REJECTED;
        propertyCheck.checker = currentUser;
        propertyCheck.checkDate = new Date();
        propertyCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(propertyCheck.actionName == 'CREATE') {
            PropertyTmp propertyTmp = propertyCheck.transactionSource;
            propertyTmp.posted = true;
            propertyTmp.save();
        }
        propertyCheck.save(failOnError: true);
        return propertyCheck;
    }

}