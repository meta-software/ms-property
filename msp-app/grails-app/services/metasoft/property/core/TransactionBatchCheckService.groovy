package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonSlurper
import org.springframework.validation.BindException

class TransactionBatchCheckService {
    
    SpringSecurityService springSecurityService
    TransactionBatchTmpService transactionBatchTmpService;

    SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser
    }

    Long count() {
        TransactionBatchCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    TransactionBatchCheck get(Serializable id) {
        TransactionBatchCheck.get(id);
    }

    TransactionBatch getEntity(TransactionBatchCheck transactionBatchCheck) {
        TransactionBatch transactionBatch = transactionBatchCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(transactionBatchCheck.jsonData);
        transactionBatch.setProperties(json);

        return transactionBatch;
    }

    TransactionBatchTmp getEntitySource(TransactionBatchCheck transactionBatchCheck) {
        TransactionBatchTmp transactionBatchTmp = transactionBatchCheck.transactionSource;

        // update the entity with the json data
        // def json = new JsonSlurper().parseText(transactionBatchCheck.jsonData);
        // transactionBatchTmp.setProperties(json);

        return transactionBatchTmp;
    }

    List<TransactionBatchCheck> list(Map params) {
        TransactionBatchCheck.list(params);
    }

    List<TransactionBatchCheck> listInbox(Map params) {

        String query = params.get('q');

        TransactionBatchCheck.where{
            if(query) {
                batchReference ==~ "%${query}%"
            }
            checkStatus == CheckStatus.PENDING
        }.join('entity').list(params);
    }

    List<TransactionBatchCheck> listOutbox(Map params) {

        String query = params.get('q')

        TransactionBatchCheck.where{

            if(query) {
                batchReference ==~ "%${query}%"
            }

            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list(params);
    }

    @Transactional
    TransactionBatchCheck save(TransactionBatchCheck transactionBatchCheck) {
        transactionBatchCheck.save(failOnError: true);
    }

    @Transactional
    TransactionBatchCheck approve(TransactionBatchCheck transactionBatchCheck) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(transactionBatchCheck.isChecked()) {
            BindException errors = new BindException(transactionBatchCheck, "transactionBatchCheck")
            errors.rejectValue("checkStatus", "transactionBatchTmp.checked.already", "Transactions Batch with reference# ${transactionBatchCheck.transactionSource.batchNumber} is already checked.")
            def message = "Transactions Batch Request [reference=${transactionBatchCheck.batchReference}] is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        transactionBatchCheck.checkStatus = CheckStatus.APPROVED;
        transactionBatchCheck.checker = currentUser;
        transactionBatchCheck.checkDate = new Date();
        transactionBatchCheck.checkComment = "Approved";
        transactionBatchCheck.save(failOnError: true);

        // TransactionBatchTmp source entity object.
        TransactionBatchTmp transactionBatchTmp = transactionBatchCheck.transactionSource;

        transactionBatchTmpService.postTransactionBatch(transactionBatchTmp);

        return transactionBatchCheck;
    }

    @Transactional
    TransactionBatchCheck cancel(TransactionBatchCheck transactionBatchCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(transactionBatchCheck.isChecked()) {
            BindException errors = new BindException(transactionBatchCheck, "transactionBatchCheck")
            errors.rejectValue("checkStatus", "transactionBatchTmp.checked.already", "Transactions Batch with reference# ${transactionBatchTmp.batchNumber} is already checked.")
            def message = "Transactions Batch Request [reference=${transactionBatchCheck.batchReference}] is already approved."
            throw new ValidationException(message, errors);
            // already checked.
        }

        // the check entity
        transactionBatchCheck.checkStatus = CheckStatus.CANCELLED;
        transactionBatchCheck.checker = currentUser;
        transactionBatchCheck.checkDate = new Date();
        transactionBatchCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(transactionBatchCheck.actionName == 'CREATE' && transactionBatchCheck.entity) {
            TransactionBatch transactionBatch = transactionBatchCheck.entity;
            transactionBatch.checkStatus == CheckStatus.CANCELLED;
            transactionBatch.save();
        }
        transactionBatchCheck.save(failOnError: true);
        return transactionBatchCheck;
    }
    
    @Transactional
    TransactionBatchCheck reject(TransactionBatchCheck transactionBatchCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(transactionBatchCheck.isChecked()) {
            BindException errors = new BindException(transactionBatchCheck, "transactionBatchCheck")
            errors.rejectValue("checkStatus", "transactionBatchTmp.checked.already", "Transactions Batch with reference# ${transactionBatchTmp.batchNumber} is already checked.")
            def message = "Transactions Batch Request [reference=${transactionBatchCheck.batchReference}] is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        transactionBatchCheck.checkStatus = CheckStatus.REJECTED;
        transactionBatchCheck.checker = currentUser;
        transactionBatchCheck.checkDate = new Date();
        transactionBatchCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(transactionBatchCheck.actionName == 'CREATE' && transactionBatchCheck.entity) {
            TransactionBatch transactionBatch = transactionBatchCheck.entity;
            transactionBatch.checkStatus == CheckStatus.REJECTED;
            transactionBatch.save();
        }
        transactionBatchCheck.save(failOnError: true);
        return transactionBatchCheck;
    }

}