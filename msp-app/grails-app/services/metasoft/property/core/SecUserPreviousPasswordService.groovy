package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService

class SecUserPreviousPasswordService {

    SpringSecurityService springSecurityService;

    /**
     * @param secUser [the user object]
     * @param password [the password must already be encoded]
     * @return
     */
    @Transactional
    def create(SecUser secUser, String password) {
        String encodedPassword = springSecurityService.encodePassword(password);
        SecUserPreviousPassword secUserPreviousPassword = new SecUserPreviousPassword(
                secUser : secUser,
                password: encodedPassword
        )

        secUserPreviousPassword.save(failOnError: true);
    }

    boolean passwordExists(Long id, String password) {
        String encodedPassword = springSecurityService.encodePassword(password);

        def c = SecUserPreviousPassword.where {
            secUser.id == id
            password == encodedPassword
        }

        return (c.count() > 0 );
    }

    List<SecUserPreviousPassword> retrievePrevious(Long secUserId, Integer max = 12) {

        SecUserPreviousPassword.where {
            secUser.id == secUserId
        }.list([max: max])

    }
}
