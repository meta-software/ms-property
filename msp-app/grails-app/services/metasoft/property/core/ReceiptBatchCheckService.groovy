package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonSlurper
import org.springframework.validation.BindException

class ReceiptBatchCheckService {
    
    SpringSecurityService springSecurityService
    ReceiptBatchTmpService receiptBatchTmpService;

    SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser
    }

    ReceiptBatchCheck get(Serializable id) {
        ReceiptBatchCheck.get(id);
    }

    ReceiptBatch getEntity(ReceiptBatchCheck ReceiptBatchCheck) {
        ReceiptBatch receiptBatch = ReceiptBatchCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(ReceiptBatchCheck.jsonData);
        receiptBatch.setProperties(json);

        return receiptBatch;
    }

    List<ReceiptBatchCheck> list(Map params) {
        ReceiptBatchCheck.list(params);
    }

    List<ReceiptBatchCheck> listInbox(Map params) {
        ReceiptBatchCheck.where{
            checkStatus == CheckStatus.PENDING
            transactionSource.posted == false
        }.join('entity').list(params);
    }

    List<ReceiptBatchCheck> listOutbox(Map params) {
        ReceiptBatchCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list(params);
    }

    @Transactional
    ReceiptBatchCheck save(ReceiptBatchCheck ReceiptBatchCheck) {
        ReceiptBatchCheck.save(failOnError: true);
    }

    @Transactional
    ReceiptBatchCheck approve(ReceiptBatchCheck receiptBatchCheck) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(receiptBatchCheck.isChecked()) {
            BindException errors = new BindException(receiptBatchCheck, "ReceiptBatchCheck")
            errors.rejectValue("checkStatus", "receiptBatchTmp.checked.already", "Transactions Batch with reference# ${receiptBatchCheck.transactionSource.batchNumber} is already checked.")
            def message = "Transactions Batch Request [reference=${receiptBatchCheck.batchNumber}] is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        receiptBatchCheck.checkStatus = CheckStatus.APPROVED;
        receiptBatchCheck.checker = currentUser;
        receiptBatchCheck.checkDate = new Date();
        receiptBatchCheck.checkComment = "Approved";
        receiptBatchCheck.save(failOnError: true);

        // ReceiptBatchTmp source entity object.
        receiptBatchTmpService.postReceiptBatchTmp(receiptBatchCheck.transactionSource);

        return receiptBatchCheck;
    }

    @Transactional
    ReceiptBatchCheck cancel(ReceiptBatchCheck ReceiptBatchCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(ReceiptBatchCheck.isChecked()) {
            BindException errors = new BindException(ReceiptBatchCheck, "ReceiptBatchCheck")
            errors.rejectValue("checkStatus", "receiptBatchTmp.checked.already", "Transactions Batch with reference# ${receiptBatchTmp.batchNumber} is already checked.")
            def message = "Transactions Batch Request [reference=${ReceiptBatchCheck.batchReference}] is already approved."
            throw new ValidationException(message, errors);
            // already checked.
        }

        // the check entity
        ReceiptBatchCheck.checkStatus = CheckStatus.CANCELLED;
        ReceiptBatchCheck.checker = currentUser;
        ReceiptBatchCheck.checkDate = new Date();
        ReceiptBatchCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(ReceiptBatchCheck.actionName == 'CREATE' && ReceiptBatchCheck.entity) {
            ReceiptBatch receiptBatch = ReceiptBatchCheck.entity;
            receiptBatch.checkStatus == CheckStatus.CANCELLED;
            receiptBatch.save();
        }
        ReceiptBatchCheck.save(failOnError: true);
        return ReceiptBatchCheck;
    }
    
    @Transactional
    ReceiptBatchCheck reject(ReceiptBatchCheck ReceiptBatchCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(ReceiptBatchCheck.isChecked()) {
            BindException errors = new BindException(ReceiptBatchCheck, "ReceiptBatchCheck")
            errors.rejectValue("checkStatus", "receiptBatchTmp.checked.already", "Transactions Batch with reference# ${receiptBatchTmp.batchNumber} is already checked.")
            def message = "Transactions Batch Request [reference=${ReceiptBatchCheck.batchReference}] is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        ReceiptBatchCheck.checkStatus = CheckStatus.REJECTED;
        ReceiptBatchCheck.checker = currentUser;
        ReceiptBatchCheck.checkDate = new Date();
        ReceiptBatchCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(ReceiptBatchCheck.actionName == 'CREATE' && ReceiptBatchCheck.entity) {
            ReceiptBatch receiptBatch = ReceiptBatchCheck.entity;
            receiptBatch.checkStatus == CheckStatus.REJECTED;
            receiptBatch.save();
        }
        ReceiptBatchCheck.save(failOnError: true);
        return ReceiptBatchCheck;
    }

    ReceiptBatchTmp getEntitySource(ReceiptBatchCheck receiptBatchCheck) {
        ReceiptBatchTmp receiptBatchTmp = receiptBatchCheck.transactionSource;
        return receiptBatchTmp;
    }

}