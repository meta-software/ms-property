package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional
import metasoft.property.core.BankAccount

@GrailsCompileStatic
class BankAccountService {

    List<BankAccount> list(Map args) {
        return BankAccount.list(args);
    }

    BankAccount get(Serializable id) {
        return BankAccount.get(id);
    }

    @Transactional
    BankAccount save(BankAccount bankAccount) {

        if(!bankAccount.validate()) {
            println(bankAccount.errors)
        }

        bankAccount.save(failOnError: true);
    }

    @Transactional
    BankAccount update(BankAccount bankAccount) {
        bankAccount.save(failOnError: true);
    }

    /**
     * Retrives the bank account configuration for a give nominal account number.
     *
     * @param accountNumber
     * @return
     */
    BankAccount getBankAccountByAccountNumber(String accountNumber) {
        BankAccount.where {
            active == true
            account.accountNumber == accountNumber
        }.get()
    }
}
