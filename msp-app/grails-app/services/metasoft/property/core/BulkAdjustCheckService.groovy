package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.json.JsonSlurper

class BulkAdjustCheckService {
    
    SpringSecurityService springSecurityService
    BulkAdjustService bulkAdjustService;

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    Long count() {
        BulkAdjustCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    BulkAdjustCheck get(Serializable id) {
        BulkAdjustCheck.get(id);
    }

    BulkAdjust getEntity(BulkAdjustCheck bulkAdjustCheck) {
        BulkAdjust bulkAdjust = bulkAdjustCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(bulkAdjustCheck.jsonData);
        bulkAdjust.setProperties(json);

        return bulkAdjust;
    }

    List<BulkAdjustCheck> list(Map args) {
        BulkAdjustCheck.list();
    }

    List<BulkAdjustCheck> listInbox(Map args) {

        String categoryArg = args?.cat

        BulkAdjustCheck.where {
            checkStatus == CheckStatus.PENDING
        }.join('entity').list();
    }

    Long count(Map args) {
        BulkAdjustCheck.where{
            checkStatus == CheckStatus.PENDING
        }.join('entity').count();
    }

    List<BulkAdjustCheck> listOutbox(Map args) {
        BulkAdjustCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    BulkAdjustCheck save(BulkAdjustCheck bulkAdjustCheck) {
        bulkAdjustCheck.save(failOnError: true);
    }

    @Transactional
    BulkAdjustCheck approve(BulkAdjustCheck bulkAdjustCheck) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(bulkAdjustCheck.isChecked()) {
            // already checked.
            return bulkAdjustCheck;
        }

        // the check entity
        bulkAdjustCheck.checkStatus = CheckStatus.APPROVED;
        bulkAdjustCheck.checker = currentUser;
        bulkAdjustCheck.checkDate = new Date();
        bulkAdjustCheck.checkComment = "Approved";
        bulkAdjustCheck.save(failOnError: true, flush: true);

        // retrive the jsonData
        Map args = new JsonSlurper().parseText(bulkAdjustCheck.jsonData)

        // BulkAdjust entity object.
        BulkAdjust bulkAdjust = bulkAdjustCheck.entity;
        bulkAdjust.setProperties(args);
        bulkAdjust.checkStatus = CheckStatus.APPROVED;
        bulkAdjust.dirtyStatus = CheckStatus.APPROVED;
        bulkAdjust.checkDate = new Date();
        bulkAdjust.checker = currentUser;
        bulkAdjust.save(flush: true); // save the bulkAdjust

        println("this is what we just approved: ${bulkAdjust.id}")

        return bulkAdjustCheck;
    }

    @Transactional
    BulkAdjustCheck reject(BulkAdjustCheck bulkAdjustCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(bulkAdjustCheck.isChecked()) {
            // already checked.
            //return bulkAdjustCheck;
        }

        // the check entity
        bulkAdjustCheck.checkStatus = CheckStatus.REJECTED;
        bulkAdjustCheck.checker = currentUser;
        bulkAdjustCheck.checkDate = new Date();
        bulkAdjustCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(bulkAdjustCheck.actionName == 'CREATE') {
            BulkAdjust bulkAdjust = bulkAdjustCheck.entity;
            bulkAdjust.checkStatus = CheckStatus.REJECTED;
            bulkAdjust.save();
        } else {
            BulkAdjust bulkAdjust = bulkAdjustCheck.entity;
            bulkAdjust.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            bulkAdjust.save();
        }

        return bulkAdjustCheck.save(failOnError: true);
    }

    @Transactional
    BulkAdjustCheck cancel(BulkAdjustCheck bulkAdjustCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(bulkAdjustCheck.isChecked()) {
            // already checked.
            //return bulkAdjustCheck;
        }

        // the check entity
        bulkAdjustCheck.checkStatus = CheckStatus.CANCELLED;
        //bulkAdjustCheck.checker = currentUser;
        bulkAdjustCheck.checkDate = new Date();
        bulkAdjustCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(bulkAdjustCheck.actionName == 'CREATE') {
            BulkAdjust bulkAdjust = bulkAdjustCheck.entity;
            bulkAdjust.checkStatus == CheckStatus.CANCELLED;
            bulkAdjust.save();
        } else {
            BulkAdjust bulkAdjust = bulkAdjustCheck.entity;
            bulkAdjust.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            bulkAdjust.save();
        }
        return bulkAdjustCheck.save(failOnError: true);
    }

}