package metasoft.property.core

import grails.gorm.services.Service

@Service(LessorPayment)
interface LessorPaymentService {

    StandingOrder get(Serializable id)

    List<StandingOrder> list(Map args)

    Long count()

    void delete(Serializable id)

    StandingOrder save(StandingOrder standingOrder)

}