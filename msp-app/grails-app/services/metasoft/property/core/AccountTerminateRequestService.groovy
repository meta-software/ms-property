package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import org.springframework.validation.BindException

class AccountTerminateRequestService {

    MakerCheckerService  makerCheckerService;
    SpringSecurityService springSecurityService;
    AccountService accountService;

    AccountTerminateRequest get(Serializable id) {
        return AccountTerminateRequest.get(id)
    }

    List<AccountTerminateRequest> list(Map args) {

        return AccountTerminateRequest.where {
            makerChecker.status == 'pending'
        }.list(args)
    }

    Long count() {
        return AccountTerminateRequest.count();
    }

    @Transactional
    AccountTerminateRequest save(AccountTerminateRequest accountTerminateRequest) {

        Long accountId = accountTerminateRequest.account.id
        //configure if a pending request already exists for the specified account

        String hql = "select count(*) from AccountTerminateRequest ltr where makerChecker.status='pending' or (makerChecker.status='approved' and processed=false)"
        Boolean accountTerminateRequestExists = AccountTerminateRequest.executeQuery(hql)[0] > 0;

        if(accountTerminateRequestExists) {
            Object[] errorArgs = []
            BindException errors = new BindException(accountTerminateRequest, accountTerminateRequest.class.name);
            errors.rejectValue('account', 'accountTerminateRequest.account.exists', errorArgs,"A pending termination request already exists for the account")
            def ve = new ValidationException("A pending termination request already exists for the account", errors);
            throw ve
        }

        accountTerminateRequest.save(failOnError: true);
    }

    @Transactional
    def updateMakerChecker(AccountTerminateRequest accountTerminateRequest) {
        makerCheckerService.update(accountTerminateRequest);

        if(accountTerminateRequest.makerChecker.status == 'approved') {
            accountTerminateRequest.makerChecker.comment = accountTerminateRequest.makerChecker.comment ?: 'Approved';
        }

        accountTerminateRequest.save(failOnError: true);
        return accountTerminateRequest;
    }

    def processRequests() {
        Date today = new Date().clearTime() + 1

        List<AccountTerminateRequest> accountTerminateRequests = AccountTerminateRequest.where {
            makerChecker.status == 'approved'
            processed == false
            actionDate == today
        }.list()

        accountTerminateRequests.each { AccountTerminateRequest terminateRequest ->
            this.processRequest(terminateRequest);
        }
    }

    @Transactional
    def processRequest(AccountTerminateRequest accountTerminateRequest) {
        Account account = accountTerminateRequest.account;

        try{
            accountService.terminate(account);

            accountTerminateRequest.processed = true;
            accountTerminateRequest.processStatus = 'success';
            accountTerminateRequest.dateProcessed = new Date();
            accountTerminateRequest.processResponse = 'Processed';
            accountTerminateRequest.save(failOnError: true);
        } catch (ValidationException e) {
            log.error("error occured while trying to terminate the account #${account.accountNumber}");
            log.error(e.message, e);

            accountTerminateRequest.processed = true;
            accountTerminateRequest.processStatus = 'error';
            accountTerminateRequest.processResponse = e.getMessage();
            accountTerminateRequest.save(failOnError: true)
        }
    }

}