package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional

@GrailsCompileStatic
class AutomaticRoutineLogService {

    LeaseService leaseService;
    TransactionBatchService transactionBatchService;
    MainTransactionService mainTransactionService;

    def getListByRoutineType(def routineType) {

        return RoutineLog.where{
            routineType == routineType
        }.list([max: 20]);
    }

    @Transactional
    RoutineLog run(RoutineLog routineLog) {

        routineLog.save(failOnError: true);

        switch(routineLog.routineType) {
            case 'interest':
                //interestRun(routineLog.period);
                break;
            case 'op-cos':
                //interestRun(routineLog.period);
                break;
            case 'rent':
                rentRun(routineLog.period);
                break;
        }

        routineLog.endTime = new Date();
        routineLog.save(failOnError: true);

        return routineLog;
    }

    @Transactional
    void interestRun(BillingCycle billingCycle) {

        /*
        //Get all the active leases for the given billingCycle
        List<Lease> activeLeases = leaseService.activeLeases(billingCycle);

        //create the new batch transaction
        TransactionHeader transactionHeader = transactionBatchService.createBatch();

        // For each lease loop the lease items and do the charges
        activeLeases.each{ lease ->

            Tenant tenant = lease.tenant;
            ChargeCode chargeCode = ChargeCode.findByName("RENT");

            // for each lease item do a charge to the main transactions table
            lease.leaseItems.each{ leaseItem ->

                println("processing leaseItem ${leaseItem}")

                String transactionReference = String.format("RR%010d%s", leaseItem.id, new Date().format("yyyyMMddHHmmss"));
                String transactionDescription = "rent charge for ${billingCycle.name}";
                sleep(1000);

                try {
                    mainTransactionService.chargeTenant(
                            transactionHeader,
                            chargeCode,
                            tenant.accountNumber,
                            new Date(),
                            leaseItem.amount,
                            transactionReference,
                            lease,
                            new Date(),
                            transactionDescription,
                            'flexcube');
                } catch(MspValidationException ve) {
                    ve.printStackTrace();
                }

            }

        }

        println("running interest for ${activeLeases.size()} leases");
        //*/
    }

    @Transactional
    void rentRun(BillingCycle billingCycle) {

        /*
        //Get all the active leases for the given billingCycle
        List<Lease> activeLeases = leaseService.activeLeases(billingCycle);

        //create the new batch transaction
        TransactionHeader transactionHeader = transactionBatchService.createBatch();

        // For each lease loop the lease items and do the charges
        activeLeases.each{ lease ->

            Tenant tenant = lease.tenant;
            ChargeCode chargeCode = ChargeCode.findByName("RENT");

            // for each lease item do a charge to the main transactions table
            lease.leaseItems.each{ leaseItem ->

                log.info("Processing run for lease-item ${leaseItem}");

                String transactionReference = String.format("RR%06d%s", leaseItem.id, new Date().format("yyyyMMddHHmmss"));
                String transactionDescription = "RENT CHARGE FOR ${billingCycle.name}";

                try {
                    mainTransactionService.chargeTenant(
                            transactionHeader,
                            chargeCode,
                            tenant.accountNumber,
                            new Date(),
                            leaseItem.amount,
                            transactionReference,
                            lease,
                            new Date(),
                            transactionDescription,
                            'flexcube');
                } catch(MspValidationException ve) {
                    log.error("Error occured while chargin tenant : ${ve.message}");
                    ve.printStackTrace();
                }
            }

        }

        log.info("Finished running rent charge for ${activeLeases.size()} leases");
        //*/
    }

}
