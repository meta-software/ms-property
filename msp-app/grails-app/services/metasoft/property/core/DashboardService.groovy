package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.cache.Cacheable
import grails.plugin.springsecurity.SpringSecurityService
import groovy.util.logging.Slf4j
import org.hibernate.SQLQuery
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowire
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
@Transactional
class DashboardService {

    LeaseService leaseService;
    TenantService tenantService;
    SpringSecurityService springSecurityService;
    BillingCycleService billingCycleService;
    PendingReceiptService pendingReceiptService;
    SecUserCheckService secUserCheckService

    @Autowired
    SessionFactory sessionFactory;

    def executiveDashboard() {

        def results = [:]
        results['leaseList'] = leaseService.expiringLeases();

        results['latestTenants'] = tenantService.latestTenants();

        return results;
    }

    def getCheckerInboxState() {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        List results = []

        // get the pending Properties count
        Long propertyCount = PropertyCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long tenantCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'tenant'
        }.count();

        Long landlordCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'landlord'
        }.count();

        Long providerCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'trust'
        }.count();

        Long leaseCount = LeaseCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long bulkInvoiceCount = BulkInvoice.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long transactionsCount = TransactionBatchCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long exchangeRateCount = ExchangeRateGroupCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long unallocatedReceiptsCount = PendingReceipt.where{
            allocated == false
        }.count();

        /*
        * landlord
        * property
        * units
        * tenant
        * lease
        * supplier
        */

        results << [pos: 0, name: 'Transaction Batches', count: transactionsCount, url: '/transaction-batch-check/app'];
        results << [pos: 1, name: 'Landlord', count: landlordCount, url: '/account-check-inbox?cat=landlord'];
        results << [pos: 2, name: 'Properties', count: propertyCount, url: '/property-check-inbox/app'];
        results << [pos: 3, name: 'Tenants', count: tenantCount, url: '/account-check-inbox?cat=tenant'];
        results << [pos: 4, name: 'Leases', count: leaseCount, url: '/lease-check-inbox'];
        results << [pos: 5, name: 'Suppliers', count: providerCount, url: '/account-check-inbox?cat=trust'];
        results << [pos: 6, name: 'Bulk Invoices', count: bulkInvoiceCount, url: '/bulk-invoice/app'];
        results << [pos: 7, name: 'Exchange Rates', count: exchangeRateCount, url: '/exchange-rate-group-check/inbox'];
        results << [pos: -1, name: 'Unallocated Receipts', count: unallocatedReceiptsCount, url: '/pending-receipt'];
//        results['transactions'] = 0;

        return results;
    }

    def getMakerOutboxState() {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        List results = []

        // get the pending Properties count
        Long propertyCount = PropertyCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.count();

        Long tenantCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'tenant'
            maker == currentUser
        }.count();

        Long landlordCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'landlord'
            maker == currentUser
        }.count();

        Long providerCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'trust'
            maker == currentUser
        }.count();

        Long leaseCount = LeaseCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.count();

        Long bulkInvoiceCount = BulkInvoice.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.count();

        Long transactionsCount = TransactionBatchCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.count();

        results << [pos: 0, name: 'Transaction Batches', count: transactionsCount, url: '/transaction-batch-check/app#/outbox'];
        results << [pos: 1, name: 'Landlord', count: landlordCount, url: '/account-check-outbox?cat=landlord'];
        results << [pos: 2, name: 'Properties', count: propertyCount, url: '/property-check-outbox/app'];
        results << [pos: 3, name: 'Tenants', count: tenantCount, url: '/account-check-outbox?cat=tenant'];
        results << [pos: 4, name: 'Leases', count: leaseCount, url: '/lease-check/outbox'];
        results << [pos: 5, name: 'Suppliers', count: providerCount, url: '/account-check-outbox?cat=trust'];
        results << [pos: 6, name: 'Bulk Invoices', count: bulkInvoiceCount, url: '/bulk-invoice/app#/outbox'];
//        results['transactions'] = 0;

        return results;
    }

    @Cacheable(value='checkInboxCount')
    def getCheckerInboxStates() {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        Map results = [:]

        // get the pending Properties count
        Long propertyCount = PropertyCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long tenantCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'tenant'
        }.count();

        Long landlordCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'landlord'
        }.count();

        Long providerCount = AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            accountCategory.code == 'trust'
        }.count();

        Long leaseCount = LeaseCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        Long bulkInvoiceCount = BulkInvoice.where{
            checkStatus == CheckStatus.PENDING
        }.count();

        results['properties'] = [name: 'Properties', count: propertyCount, url: '/property-check'];
        results['tenants'] = [name: 'Tenants', count: tenantCount, url: '/account-check?cat=tenant'];
        results['landlords'] = [name: 'Landlord', count: landlordCount, url: '/account-check?cat=landlord'];
        results['providers'] = [name: 'Service Providers', count: providerCount, url: '/account-check?cat=trust'];
        results['leases'] = [name: 'Leases', count: leaseCount, url: '/lease-check'];
        results['bulkInvoices'] = [name: 'Bulk Invoices', count: bulkInvoiceCount];
        results['transactions'] = [name: 'Transactions', count: 0];

        return results;
    }

    def executeDashlet(String dashlet, def params = [:]) {
        switch(dashlet) {
            case 'expiring-leases':
                return leaseService.expiringLeases();
                break;
            case 'occupancy':
                return occupancy();
                break;
            case 'outstanding-balances':
                return outstandingBalances();
                break;
            case 'commission':
                return commission(params)
            case 'ds-invoice':
                return invoice(params)
            case 'ds-receipt':
                return receipt(params)
            case 'pending-receipt':
                return pendingReceipt(params)
            case 'pending-users':
                return pendingUsersCount(params)
            default:
                log.warn("dashlet code [{}] was not found.", dashlet);
                break;
        }
    }

    def commission(Map params) {

        Date startDate = params['startDate'];
        Date endDate = params['endDate'];

        if(params['period'] == 'current') {
            BillingCycle billingCycle = billingCycleService.getCurrentCycle();

            startDate = billingCycle.startDate
            endDate = billingCycle.endDate
        }

        String hql = """
            SELECT
                sum(debit_reporting_amount-credit_reporting_amount) as balance
            FROM
                MainTransaction a
            JOIN
                SubAccount s ON a.subAccountNumberCr= s.accountNumber
            WHERE s.accountNumber in :subAccNums
                AND a.transactionDate between :startDate and :endDate
            """

        def results = MainTransaction.executeQuery(hql, [subAccNums: ['1'], startDate: startDate, endDate: endDate])

        return [commissionAmount: results[0]];

    }

    def invoice(Map params) {

        Date startDate = params['startDate'];
        Date endDate = params['endDate'];

        if(params['period'] == 'current') {
            BillingCycle billingCycle = billingCycleService.getCurrentCycle();

            startDate = billingCycle.startDate
            endDate = billingCycle.endDate
        } else if(params['period'] == 'previous') {
            BillingCycle billingCycle = billingCycleService.getCurrentCycle();
            billingCycle = billingCycleService.getBillingCycle( (billingCycle.getStartDate() - 10) )

            startDate = billingCycle.startDate
            endDate = billingCycle.endDate
        }

        String hql = """
            SELECT
                sum(debit_reporting_amount-credit_reporting_amount) as amount
            FROM
                MainTransaction a
            JOIN
                SubAccount s ON a.subAccountNumberCr= s.accountNumber
            WHERE s.accountNumber in :subAccNums
                AND a.parentTransaction = true
                AND a.transactionDate between :startDate and :endDate
            """

        def results = MainTransaction.executeQuery(hql, [subAccNums: ['2'], startDate: startDate, endDate: endDate]);
        return [invoicesAmount: results[0]];
    }

    def receipt(Map params) {

        Date startDate = params['startDate'];
        Date endDate = params['endDate'];

        if(params['period'] == 'current') {
            BillingCycle billingCycle = billingCycleService.getCurrentCycle();

            startDate = billingCycle.startDate
            endDate = billingCycle.endDate
        } else if(params['period'] == 'previous') {
            BillingCycle billingCycle = billingCycleService.getCurrentCycle();
            billingCycle = billingCycleService.getBillingCycle( (billingCycle.getStartDate() - 10) )

            startDate = billingCycle.startDate
            endDate = billingCycle.endDate
        }

        String hql = """
            SELECT
                sum(ifnull(debit,0)-ifnull(credit,0)) as amount
            FROM
                MainTransaction a
            JOIN
                SubAccount s ON a.subAccountNumberCr= s.accountNumber
            WHERE a.parentTransaction = true
                AND a.transactionDate between :startDate and :endDate
                AND a.transactionType.transactionTypeCode='RC'
            """

        def results = MainTransaction.executeQuery(hql, [startDate: startDate, endDate: endDate]);
        return [receiptsAmount: results[0]];
    }

    def pendingReceipt(Map params) {

        Long pendingReceiptsCount = pendingReceiptService.countPendingReceipts();
        return [pendingReceiptsCount: pendingReceiptsCount]
    }

    def pendingUsersCount(Map params) {
        Long countPendingUsers = secUserCheckService.countPending(params);
        return [pendingUsers: countPendingUsers]
    }

    def occupancy() {
        def queryString = """
            select 
                sum(case when rs.code ='occupied' then 1 else 0 end) occupied, 
                sum(case when rs.code ='un-occupied' then 1 else 0 end) not_occupied, 
                sum(1) total_units 
            from rental_unit a 
            inner join rental_unit_status rs on rs.id = a.unit_status_id 
            inner join property p on a.property_id = p.id 
            left join (
                select ls.name, ls.code, l.* 
                from ms_lease l 
                inner join lease_status ls on ls.id=l.lease_status_id 
                where ls.code = 'active'
            ) as al on al.rental_unit_id=a.id  
            where p.is_terminated = 0 and p.deleted = 0 
              and a.deleted = 0 """;

        SQLQuery sqlQuery = sessionFactory.currentSession.createSQLQuery(queryString);
        def queryResults = sqlQuery.list();

        Map returnResults = [
                occupied: queryResults[0][0],
                notOccupied: queryResults[0][1],
                totalUnits: queryResults[0][2]
        ]

        return returnResults;
    }

    def outstandingBalances() {
        def queryString = """
            SELECT 
                sum(ifnull(debit_reporting_amount,0)-ifnull(credit_reporting_amount,0)) balance 
            FROM 
                ms_transaction A 
            INNER JOIN 
                ms_account c on A.account_number_cr=c.account_number 
            INNER JOIN 
                account_category g on g.id = c.account_category_id 
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number 
            WHERE 
                g.code = 'tenant'
                AND S.account_number not in (4, 13)
                AND S.suspense = 0 """

        SQLQuery sqlQuery = sessionFactory.currentSession.createSQLQuery(queryString);
        def queryResults = sqlQuery.list();

        Map returnResults = [
            outstandingBalances: queryResults[0]
        ]

        return returnResults;
    }
}
