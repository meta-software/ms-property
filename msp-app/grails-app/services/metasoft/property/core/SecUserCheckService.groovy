package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.mapping.LinkGenerator
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import metasoft.property.core.exceptions.RecordNotFoundException
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class SecUserCheckService {

    @Autowired
    LinkGenerator grailsLinkGenerator
    SpringSecurityService springSecurityService
    UserService userService;

    @Autowired
    SessionFactory sessionFactory

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    Long count() {
        SecUserCheck.where {
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    SecUserCheck get(Serializable id) {
        SecUserCheck.get(id);
    }

    List<SecRole> getAuthorities(SecUserCheck secUserCheck) {

        // update the entity with the json data
        def json = new JsonSlurper().parseText(secUserCheck.jsonData);

        def authorities = json['authorities'] ?: [];

        if(! (authorities instanceof List) ) {
            authorities = [authorities];
        }

        def roles = [];

        authorities.each { roleId ->
            roles << SecRole.get(roleId)
        }

        return roles;
    }

    SecUser getEntity(SecUserCheck secUserCheck) {
        SecUser secUser = secUserCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(secUserCheck.jsonData);
        secUser.setProperties(json);

        return secUser;
    }

    List<SecUserCheck> list(Map args) {
        SecUserCheck.list();
    }

    List<SecUserCheck> listInbox(Map args) {

        String categoryArg = args?.cat
        String q = args['q']

        SecUserCheck.where {
            checkStatus == CheckStatus.PENDING

            if(q){
                ( username ==~ "%${q}%" )
            }
        }.join('entity').list();
    }

    List<SecUserCheck> listOutbox(Map args) {
        SecUserCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    SecUserCheck save(SecUserCheck secUserCheck) {
        secUserCheck.save(failOnError: true);
    }

    @Transactional
    SecUserCheck approve(Serializable id) {
        SecUserCheck secUserCheck = SecUserCheck.lock(id);

        if(secUserCheck == null) {
            throw new RecordNotFoundException();
        }

        switch(secUserCheck.actionName) {
            case 'PASSWORD-RESET':
                approvePasswordReset(secUserCheck);
                break;

            default:
                approveRecordSave(secUserCheck);
                break;
        }

        return secUserCheck;
    }

    def approveRecordSave(SecUserCheck secUserCheck) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;
        //evict from the Hibernate cache
        Session session = sessionFactory.currentSession;

        if(secUserCheck.isChecked()) {
            // already checked.
            //return secUserCheck;
        }

        // the check entity
        secUserCheck.checkStatus = CheckStatus.APPROVED;
        secUserCheck.checker = currentUser;
        secUserCheck.checkDate = new Date();
        secUserCheck.checkComment = "Approved";
        secUserCheck.save(failOnError: true, flush: true);

        // retrieve the jsonData
        Map args = new JsonSlurper().parseText(secUserCheck.jsonData)
        args.remove('password'); //gotta remove the password

        // SecUser entity object.
        SecUser secUser = secUserCheck.entity;
        secUser.setProperties(args);
        secUser.checkStatus = CheckStatus.APPROVED;
        secUser.dirtyStatus = CheckStatus.APPROVED;
        secUser.checkDate = new Date();
        secUser.checker = currentUser;
        //secUser.enabled = true;
        //secUser.accountLocked = false;
        secUser.lockReason = null;
        secUser.checkEntity = null;
        secUser.loginAttemptCount = 0L;
        // set the user authorities if they exists
        userService.save(secUser, getAuthorities(secUserCheck));
        secUser.save(flush: true); // save the secUser
        session.evict(secUser);

        return secUser.refresh();
    }

    def approvePasswordReset(SecUserCheck secUserCheck) {
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        //evict from the Hibernate cache
        Session session = sessionFactory.currentSession;

        // the check entity
        secUserCheck.checkStatus = CheckStatus.APPROVED;
        secUserCheck.checker = currentUser;
        secUserCheck.checkDate = new Date();
        secUserCheck.checkComment = "Approved";
        secUserCheck.save(failOnError: true, flush: true);

        // retrieve the jsonData
        Map args = new JsonSlurper().parseText(secUserCheck.jsonData)

        SecUser.where {
            id == secUserCheck.entity.id
        }.updateAll([
            checkEntity: null,
            lockReason: null,
            enabled: true,
            checker: currentUser,
            checkDate: new Date(),
            checkStatus: CheckStatus.APPROVED,
            dirtyStatus: CheckStatus.APPROVED,
            password: args.get('password'),
            loginAttemptCount: 0L,
            initialLogin: true
        ]);

        SecUser secUser = SecUser.get(secUserCheck.entity.id)
        session.evict(secUser);

        return secUser;
    }

    @Transactional
    SecUserCheck reject(SecUserCheck secUserCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(secUserCheck.isChecked()) {
            // already checked.
            //return secUserCheck;
        }

        // the check entity
        secUserCheck.checkStatus = CheckStatus.REJECTED;
        secUserCheck.checker = currentUser;
        secUserCheck.checkDate = new Date();
        secUserCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(secUserCheck.actionName == 'CREATE') {
            String uid = "XXXREJ.${new Date().format('yyyyMMddSS')}";

            SecUser secUser = secUserCheck.entity;
            secUser.checkStatus = CheckStatus.REJECTED;
            secUser.username = "${uid}.[$secUser.username]"
            secUser.emailAddress = "${uid}.${secUser.emailAddress}"
            secUser.checkEntity = null;
            secUser.save();
        } else {
            SecUser secUser = secUserCheck.entity;
            secUser.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            secUser.checkEntity = null;
            secUser.save();
        }

        secUserCheck.save(failOnError: true);
        return secUserCheck;
    }

    @Transactional
    SecUserCheck cancel(SecUserCheck secUserCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(secUserCheck.isChecked()) {
            // already checked.
            //return secUserCheck;
        }

        // the check entity
        secUserCheck.checkStatus = CheckStatus.CANCELLED;
        //secUserCheck.checker = currentUser;
        secUserCheck.checkDate = new Date();
        secUserCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(secUserCheck.actionName == 'CREATE') {
            SecUser secUser = secUserCheck.entity;
            secUser.checkStatus == CheckStatus.CANCELLED;
            secUser.save();
        } else {
            SecUser secUser = secUserCheck.entity;
            secUser.dirtyStatus == CheckStatus.APPROVED;
            secUser.checkEntity = null;
            secUser.save();
        }

        return secUserCheck.save(failOnError: true);
    }

    Long countPending(Map args) {
        return SecUserCheck.where {
            checkStatus == CheckStatus.PENDING
        }.count();
    }

}