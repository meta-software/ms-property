package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import metasoft.property.core.BillingInvoiceEmailRequest.RequestMethod
import metasoft.property.core.BillingInvoiceEmailRequest.ActionStatus
import org.apache.commons.lang.NotImplementedException
import org.springframework.beans.factory.annotation.Autowired

class BillingInvoiceEmailRequestService {

    @Autowired
    BillingInvoiceService billingInvoiceService;

    BillingInvoiceEmailRequest get(Serializable id) {
        return BillingInvoiceEmailRequest.get(id);
    }

    @Transactional
    BillingInvoiceEmailRequest create(BillingInvoiceEmailRequestCommand command) {
        BillingInvoiceEmailRequest billingInvoiceEmailRequest = command as BillingInvoiceEmailRequest;
        return billingInvoiceEmailRequest.save();
    }

    List<BillingInvoiceEmailRequest> list(Map args) {
        return BillingInvoiceEmailRequest.list(args);
    }

    BillingInvoiceEmailRequest show(Long id) {
        return BillingInvoiceEmailRequest.get(id);
    }
}

class BillingInvoiceEmailRequestCommand {
    String emailAddress;
    ActionStatus actionStatus;
    String actionReason;
    BillingInvoice billingInvoice;
    RequestMethod requestMethod;

    Object asType(Class type) {
        if(type == BillingInvoiceEmailRequest.class) {
            BillingInvoiceEmailRequest billingInvoiceEmailRequest = new BillingInvoiceEmailRequest(
                    emailAddress: this.emailAddress,
                    actionStatus: this.actionStatus,
                    actionReason: this.actionReason,
                    billingInvoice: this.billingInvoice,
                    requestMethod: this.requestMethod
            );

            return billingInvoiceEmailRequest;
        }
    }
}