package metasoft.property.core

import grails.async.Promise
import grails.async.Promises
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import grails.plugins.jasper.JasperExportFormat
import grails.plugins.jasper.JasperReportDef
import grails.plugins.jasper.JasperService
import grails.plugins.mail.MailService
import grails.util.Environment
import groovy.util.logging.Slf4j
import metasoft.property.BillingInvoiceReport
import metasoft.property.core.BillingInvoiceEmailRequest.RequestMethod
import org.apache.commons.io.FileUtils
import org.springframework.beans.factory.annotation.Autowired

//@GrailsCompileStatic
@Slf4j
class BillingInvoiceService {

    EmailSendRequestService emailSendRequestService;
    BillingInvoiceEmailRequestService billingInvoiceEmailRequestService;
    BillingCycleService billingCycleService;
    TenantService tenantService;
    JasperService jasperService;
    GrailsApplication grailsApplication;

    /**
     * generates the monthly billingCycle accountStatement for the given account for the provided billing cycle
     *
     * @param lease
     * @param billingCycle
     * @return BillingInvoice
     */
    //@Transactional
    BillingInvoice generateInvoice(Lease lease, BillingCycle billingCycle) {
        log.info("generating the account statement for the leaseNumber=[{}] and billingCycle=[{}]", lease.leaseNumber, billingCycle.name);

        String hql = """ select t
            from MainTransaction t 
                inner join SubAccount s on t.subAccountNumberCr = s.accountNumber and s.suspense = 0 
                inner join Account a on t.accountNumberCr = a.accountNumber 
            where t.parentTransaction = true
                and t.accountNumberCr = :accountNumber 
                and t.transactionType.transactionTypeCode = :typeCode
                and t.transactionDate >= :queryDate
            """;

        List<MainTransaction> mainTransactions = (List<MainTransaction>)MainTransaction.executeQuery(hql, [
                typeCode: 'IV',
                accountNumber: lease.lesseeAccount,
                queryDate: billingCycle.startDate
        ]);

        Date paymentDueDate = billingCycle.startDate + 7;
        BigDecimal leaseBalance = tenantService.getLeaseAccountBalance(lease.tenant, lease, billingCycle.startDate);
        BillingInvoice billingInvoice = new BillingInvoice(
                billingCycle: billingCycle,
                lease: lease,
                tenant: lease.tenant,
                openingBalance: leaseBalance,
                dueDate: paymentDueDate
                //invoiceDate: billingCycle.startDate
        );

        // add the billingInvoiceItem to the billingInvoice
        for(MainTransaction mainTransaction : mainTransactions) {
            BillingInvoiceItem billingInvoiceItem = new BillingInvoiceItem()
            billingInvoiceItem.mainTransaction = mainTransaction;
            billingInvoiceItem.amount = mainTransaction.transactionAmount;
            billingInvoice.addToBillingInvoiceItems(billingInvoiceItem);
        }

        // persist the statement to the database.
        billingInvoice.save(flush: true);
        billingInvoice.invoiceNumber = String.format("%d", billingInvoice.id);
        billingInvoice.save(flush: true);

        return billingInvoice;
    }

    List<BillingInvoiceReport> listInvoices(Map args) {

        String hql = """
            select i, sum(it.amount) as invoiceSubTotal, sum(t.vatAmount) as vatTotal 
                from BillingInvoice i 
                inner join i.billingInvoiceItems it
                inner join it.mainTransaction t
                inner join i.lease l
                %s
                group by i, l
            """
        String whereClause = "where 1=1";
        String tenantArg = args['tenant']?.trim();
        String cycleArg = args['cycle']?.trim();

        if(tenantArg) {
            whereClause += " and (i.tenant.accountName like '%${tenantArg}%' OR i.tenant.accountNumber like '%${tenantArg}%')"
        }
        if(cycleArg) {
            whereClause += " and i.billingCycle.name = '${cycleArg}'";
        }

        def invoices = BillingInvoice.executeQuery(String.format(hql, whereClause), [max: 20]);
        List<BillingInvoiceReport> billingInvoices = []
        invoices.each { row ->
            BillingInvoiceReport billingInvoiceReport = BillingInvoiceReport.fromBillingInvoice(row[0]);
            billingInvoiceReport.setInvoiceTotal(row[1]);
            billingInvoiceReport.setVatTotal(row[2]);

            billingInvoices.add(billingInvoiceReport);
        }

        return billingInvoices;
    }

    BillingInvoice get(Serializable id) {
        return BillingInvoice.get(id);
    }

    @Transactional(readOnly = true)
    BillingInvoiceReport getReport(Serializable id) {
        BillingInvoice billingInvoice = get(id);
        BillingInvoiceReport billingInvoiceReport = new BillingInvoiceReport();
        billingInvoiceReport.properties.each { prop ->
            if(billingInvoice.hasProperty(prop.key) && prop.key != "class")
                billingInvoiceReport.setProperty(prop.key, billingInvoice.getProperty(prop.key));
        }

        //update the totals;
        BigDecimal invoiceTotal = 0.0;
        BigDecimal vatTotal = 0.0;25
        BigDecimal discountTotal = 0.0;

        billingInvoiceReport.billingInvoiceItems.each { BillingInvoiceItem item ->
            invoiceTotal += item.amount;
            vatTotal += item.mainTransaction.vatAmount;
        }

        billingInvoiceReport.invoiceTotal = invoiceTotal;
        billingInvoiceReport.vatTotal = vatTotal;
        billingInvoiceReport.discountTotal = discountTotal;

        return billingInvoiceReport;
    }

    @Transactional
    void generateBillingInvoices(BillingCycle billingCycle) {
        String hql = """ from Lease i where exists(select t.accountNumberCr
            from MainTransaction t 
            inner join SubAccount s on t.subAccountNumberCr = s.accountNumber and s.suspense = 0
            inner join Account a on t.accountNumberCr = a.accountNumber 
            where t.lease.id=i.id 
            and t.transactionType.transactionTypeCode = :txnTypeCode
            and t.transactionDate between :startDate and :endDate )
        """

        // Retrieve the list of all transactions for the billing period
        List<Lease> leases = (List<Lease>) Lease.executeQuery(hql,
                [txnTypeCode: 'IV', startDate: billingCycle.startDate, endDate: billingCycle.endDate],
                [readOnly: true]);

        for(Lease lease : leases) {
            try {
                this.generateInvoice(lease, billingCycle);
            } catch(RuntimeException re) {
                log.error("Failed to generate BillingLeaseInvoice for leaseNumber=[{}], for billingCycle=[{}]; {}",
                        lease.leaseNumber, billingCycle.name, re.message);
                re.printStackTrace();
                throw re;
            }
        }
    }

    @Transactional
    void generateBillingInvoices() {
        log.debug("preparing the generation of account/lease billing invoices");

        BillingCycle currentBillingCycle = billingCycleService.getCurrentCycle();
        log.info("preparing to generate billingInvoices for the billingCycle=[{}]", currentBillingCycle.name);

        generateBillingInvoices(currentBillingCycle);
    }

    File printInvoice(BillingInvoice billingInvoice) {

        if(billingInvoice.pdfGenerated) {
            String invoicesDir = grailsApplication.config.get("metasoft.config.invoices.dir");
            String invoicesCycleDir = invoicesDir + "/${billingInvoice.billingCycle.name}";
            File invoiceFile = new File(invoicesCycleDir + "/" + billingInvoice.invoiceFileName);

            if(invoiceFile.exists()) {
                return invoiceFile;
            }
        }

        // in the event the file was not generated or the file was not found
        Map<String, Object> reportParams = [
                P_BILLING_INVOICE: billingInvoice.id,
                SUBREPORT_DIR: grailsApplication.config.get("jasper.dir.reports")
        ]

        JasperReportDef jasperReportDef = new JasperReportDef(name: 'ms-billing-invoice-report',
                fileFormat: JasperExportFormat.PDF_FORMAT,
                parameters: reportParams);

        ByteArrayOutputStream reportOutputStream = jasperService.generateReport(jasperReportDef);

        String invoicesDir = grailsApplication.config.get("metasoft.config.invoices.dir");
        String invoicesCycleDir = invoicesDir + "/${billingInvoice.billingCycle.name}";
        File invoiceFile = new File(invoicesCycleDir + "/" + billingInvoice.invoiceFileName);
        FileUtils.writeByteArrayToFile(invoiceFile, reportOutputStream.toByteArray());

        billingInvoice.pdfGenerated = true;
        billingInvoice.save(flush: true);

        return invoiceFile;
    }

    void prepareBillingInvoicesAuto() {
        BillingCycle billingCycle = billingCycleService.getCurrentCycle();
        log.info("preparing the billing invoices for the billing cycle [{}]", billingCycle.name);

        this.generateBillingInvoices(billingCycle);

        this.printAccountStatementsPdf();
    }

    void emailInvoicesAuto() {
        BillingCycle billingCycle = billingCycleService.getCurrentCycle();
        log.info("preparing the billing invoices for the billing cycle [{}]", billingCycle.name);

        List<BillingInvoice> billingInvoices = getUnEmailedInvoices(billingCycle);
        billingInvoices.each{ BillingInvoice billingInvoice  ->

            try{

                if(billingInvoice.tenant.emailAddress && !billingInvoice.tenant.emailAddress.isEmpty()) {
                    emailBillingInvoice(billingInvoice, RequestMethod.AUTO);
                } else {
                    log.warn("tenant [{}] has no email-address set, invoice not sent by email", billingInvoice.tenant.accountName);

                    BillingInvoiceEmailRequestCommand command = new BillingInvoiceEmailRequestCommand(
                            actionStatus: BillingInvoiceEmailRequest.ActionStatus.NOT_SENT,
                            actionReason: "NO EMAIL ADDRESS SET FOR TENANT",
                            billingInvoice: billingInvoice,
                            emailAddress: billingInvoice.tenant.emailAddress,
                            requestMethod: RequestMethod.AUTO
                    );

                    billingInvoiceEmailRequestService.create(command);
                }

            } catch(Exception e) {
                log.error("error while sending bulk emails, ${e.message}");
                e.printStackTrace();
                throw e;
            }

        }

        this.printAccountStatementsPdf();
    }

    /**
     * Prints pdf account (Invoices) for all generated Invoices
     *
     * @param billingCycle
     */
    @Transactional
    void printAccountStatementsPdf() {
        BillingCycle billingCycle = billingCycleService.getCurrentCycle();
        printAccountStatementsPdf(billingCycle)
    }

    void printAccountStatementsPdf(BillingCycle billingCycle) {
        log.info("printing all billing invoices for the billing cycle [{}]", billingCycle.name);

        String invoicesDir = grailsApplication.config.get("metasoft.config.invoices.dir");
        String invoicesCycleDir = invoicesDir + "/${billingCycle.name}";

        List<BillingInvoice> billingInvoices = BillingInvoice.where {
            billingCycle == billingCycle
            pdfGenerated == false
        }.list();

        for(BillingInvoice billingInvoice : billingInvoices) {
            log.debug("printing billing-voice PDF for invoiceNumber=[{}]", billingInvoice.invoiceNumber);
            Map<String, Object> reportParams = [
                    P_BILLING_INVOICE: billingInvoice.id,
                    SUBREPORT_DIR: grailsApplication.config.get("jasper.dir.reports")
            ]

            JasperReportDef jasperReportDef = new JasperReportDef(name: 'ms-billing-invoice-report',
                    fileFormat: JasperExportFormat.PDF_FORMAT,
                    parameters: reportParams);

            ByteArrayOutputStream reportOutputStream = jasperService.generateReport(jasperReportDef);
            String fileName = "ms.invoice.${billingCycle.name}-${billingInvoice.invoiceNumber}.pdf";
            String filePath = "${invoicesCycleDir}/${fileName}";

            try {
                FileUtils.writeByteArrayToFile(new File(filePath), reportOutputStream.toByteArray());

                billingInvoice.invoiceFileName = fileName;
                billingInvoice.pdfGenerated = true;
                billingInvoice.save(flush:true);
            } catch(IOException e) {
                log.error("IOException while printing billing invoice [{}] {}", billingInvoice.invoiceNumber, e.message);
                e.printStackTrace();
            }
        }
    }

    void emailBillingInvoice(final BillingInvoice billingInvoice, RequestMethod method = RequestMethod.AUTO) {
        File invoiceFile = printInvoice(billingInvoice);
        emailBillingInvoice(billingInvoice, invoiceFile.absolutePath, method);
    }

    void emailBillingInvoice(final BillingInvoice billingInvoice, String filePath, RequestMethod method) {

        Tenant tenant = billingInvoice.tenant;
        String accountNumber = tenant.accountNumber;
        String leaseNumber = billingInvoice.lease.leaseNumber;

        String emailSender = grailsApplication.config.get("grails.mail.default.from");

        File invoiceFile = new File(filePath)
        if(invoiceFile.exists()) {
            //@todo: should utilize JMS or a message-broker service

            String recipientAddress = tenant.emailAddress;

            log.info("sending email for billing-invoice [{}] to [{}]", billingInvoice.invoiceNumber, tenant.emailAddress);

            EmailSendRequestCommand requestCommand = new EmailSendRequestCommand(
                    multipart: true,
                    sender: emailSender,
                    recipient: recipientAddress,
                    subject: "Invoice - CBZ Properties Tenant Number [${accountNumber}]",
                    message: """Dear Customer

Kindly find attached, your invoice.

---
CBZ Properties
""",
                    attachmentFilename: "Invoice - ${billingInvoice.invoiceNumber}",
                    attachmentFile: invoiceFile.absolutePath
            )
            emailSendRequestService.create(requestCommand)

            BillingInvoice invoice = BillingInvoice.get(billingInvoice.id);
            invoice.emailSent = true;
            invoice.save(flush: true);

            BillingInvoiceEmailRequestCommand command = new BillingInvoiceEmailRequestCommand(
                    actionStatus: BillingInvoiceEmailRequest.ActionStatus.SENT,
                    actionReason: "REQUEST SENT",
                    emailAddress: tenant.emailAddress,
                    billingInvoice: invoice,
                    requestMethod: method
            );

            billingInvoiceEmailRequestService.create(command);
        } else {
            log.error("the billing invoice pdf file @[{}] was not found.", filePath);
        }
    }

    List<BillingInvoice> getUnEmailedInvoices(BillingCycle cycle) {
        return BillingInvoice.where {
            emailSent == false
            billingCycle == cycle
        }.join('tenant').list();
    }

}
