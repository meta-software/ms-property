package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.util.logging.Slf4j

@Slf4j
class ReceiptBatchService {

    SpringSecurityService springSecurityService
    ReceiptService receiptService;

    ReceiptBatch get(Serializable id) {
        return ReceiptBatch.get(id);
    }

    List<ReceiptBatch> list(Map args) {
        List<ReceiptBatch> receiptBatches = ReceiptBatch.list(args);

        log.info("number of batches {}", receiptBatches.size())

        return receiptBatches;
    }

    List<ReceiptBatch> listPending(Map args){
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        args['sort'] = [id: 'asc'];

        return ReceiptBatch.list(args);
    }

    //@Transactional
    ReceiptBatch postReceiptBatch(ReceiptBatch receiptBatch) {

        /*
        if(receiptBatch.posted) {
            BindException errors = new BindException(receiptBatch, "receiptBatch");
            errors.rejectValue("posted", "receiptBatch.posted.already", "Receipt Batch with reference# ${receiptBatch.batchNumber} is already posted.");
            def message = "Invalid: Receipt Batch reference=${receiptBatch.batchNumber} is already posted.";
            throw new ValidationException(message, errors);
        }*/

        def currentUser = springSecurityService.currentUser as SecUser;

        TransactionBatch transactionBatch = new TransactionBatch();
        transactionBatch.transactionType  = receiptBatch.transactionType;
        transactionBatch.checkStatus      = CheckStatus.APPROVED;
        transactionBatch.batchNumber      = receiptBatch.batchNumber;
        transactionBatch.autoCommitted    = receiptBatch.autoCommitted;
        transactionBatch.maker     = currentUser;
        transactionBatch.makeDate  = new Date();
        transactionBatch.checker   = currentUser;
        transactionBatch.dirtMaker = currentUser;
        transactionBatch.checkDate = new Date();
        transactionBatch.checkStatus = CheckStatus.APPROVED;
        transactionBatch.dirtyStatus = CheckStatus.APPROVED;
        transactionBatch.save(); // save the transactionBatch

        for(Receipt receipt : receiptBatch.receipts) {
            receiptService.postReceipt(receipt, transactionBatch);
        }

        //receiptBatch.posted = true;
        //receiptBatch.save();
        transactionBatch.save(flush: true);

        return receiptBatch.refresh();
    }
}
