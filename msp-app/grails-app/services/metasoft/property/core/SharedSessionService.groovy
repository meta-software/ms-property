package metasoft.property.core

import grails.gorm.transactions.Transactional

class SharedSessionService {

    UserSessionToken create(SecUser secUser) {
        UserSessionToken token = new UserSessionToken()
        token.secUser = secUser;
        token.token = generateToken(secUser);

        token.save(failOnError: true, flush: true);
    }

    UserSessionToken validateToken(String sessionToken) {
        return UserSessionToken.where {
            token == sessionToken
            used == false
        }.join('secUser').get();
    }

    @Transactional
    void invalidateToken(String sharedToken) {
        UserSessionToken userSessionToken = UserSessionToken.findByToken(sharedToken);
        if(userSessionToken){
            userSessionToken.used = true;
            userSessionToken.save(flush: true);
        }
    }

    /**
     * Generate the session controller on the fly
     *
     * @param secUser
     * @return
     */
    String generateToken(SecUser secUser) {
        String token = UUID.randomUUID().toString();

        //user token component
        //4.times{ token = token + UUID.randomUUID().toString(); }

        return token;
    }
}
