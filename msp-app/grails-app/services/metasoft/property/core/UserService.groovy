package metasoft.property.core

import grails.core.GrailsApplication
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.mapping.LinkGenerator
import groovy.json.JsonBuilder
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.security.core.userdetails.UserCache

@ReadOnly
class UserService {

    @Autowired
    UserCache userCache;
    @Autowired
    LinkGenerator grailsLinkGenerator

    GrailsApplication grailsApplication
    SpringSecurityService springSecurityService
    SecUserPreviousPasswordService secUserPreviousPasswordService;

    // Auto inject SessionFactory we can use
    // to get the current Hibernate session.
    @Autowired
    SessionFactory sessionFactory

    @Transactional
    SecUser updateLoginData(Long id) {
        SecUser secUser =  SecUser.get(id);
        secUser.lastLoginDate = new Date();
        secUser.loginAttemptCount = 0; // clear the attempts;
        return secUser.save(flush: true); // update the records
    }

    SecUser get(Serializable id) {
        return SecUser.get(id);
    }

    List<SecUser> list(Map args) {

        def q = args['q']
        def pending = args['pending']
        def usertype = args['usertype']

        return SecUser.where{
            if(usertype) {
                userType.id == usertype
            }
            if(q){
                ( username ==~ "%${q}%" || firstName ==~ "%${q}%" || lastName ==~ "%${q}%" )
            }
            if(pending == '1') {
                dirtyStatus in [CheckStatus.EDITING, CheckStatus.PENDING]
            } else {
                checkStatus == CheckStatus.APPROVED
            }
        }.list(args);
    }

    Long count() {
        return SecUser.count();
    }

    Long count(Map args) {

        def q = args['q']
        def pending = args['pending']
        def usertype = args['usertype']

        SecUser.where{
            if(usertype) {
                userType.id == usertype
            }
            if(q){
                ( username ==~ "%${q}%" || firstName ==~ "%${q}%" || lastName ==~ "%${q}%" )
            }
            if(pending == '1') {
                dirtyStatus in [CheckStatus.EDITING, CheckStatus.PENDING]
            } else {
                checkStatus == CheckStatus.APPROVED
            }
        }.count() as Long;
    }

    @Transactional
    SecUserCheck create(SecUser secUser, Map args) {

        // prepare the maker-checker attributes
        secUser.maker = springSecurityService.currentUser as SecUser;
        secUser.makeDate = new Date();
        secUser.checkStatus = CheckStatus.PENDING;
        secUser.dirtyStatus = CheckStatus.PENDING;
        secUser.enabled = false;
        secUser.accountExpired = false;
        secUser.passwordExpired = false;
        secUser.accountLocked = true;
        secUser.lockReason = "Account not yet approved.";
        secUser.save(failOnError: true)

        // create the url
        String url = grailsLinkGenerator.link(controller: 'user', action: 'show', id: secUser.id, absolute: false);

        // generate the maker-checker
        SecUserCheck secUserCheck = new SecUserCheck()
        secUserCheck.username = secUser.username;
        secUserCheck.entity = secUser;
        secUserCheck.entityName = "User";
        secUserCheck.actionName = "CREATE";
        secUserCheck.maker = springSecurityService.currentUser as SecUser;
        secUserCheck.entityUrl = url;

        // encode the password
        args['password'] = springSecurityService.encodePassword(args['password'], '123456');
        secUserCheck.jsonData = new JsonBuilder(args).toString();
        secUserCheck.makeDate = new Date();

        try{
            secUserCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the check method")
            throw e
        }

        return secUserCheck;
    }

    @Transactional
    SecUserCheck update(SecUser secUser, Map args) {

        // prepare the maker-checker attributes
        secUser.maker = springSecurityService.currentUser as SecUser;
        if(!secUser.isRecordDirty()) {
            secUser.dirtyStatus = CheckStatus.EDITING;
            secUser.save(failOnError: true)
        }

        // create the url
        String url = grailsLinkGenerator.link(controller: 'user', action: 'show', id: secUser.id, absolute: false);

        // generate the maker-checker
        SecUserCheck secUserCheck = secUser.checkEntity ?: new SecUserCheck();
        secUserCheck.username = secUser.username;
        secUserCheck.entity = secUser;
        secUserCheck.entityName = "User";
        secUserCheck.actionName = "UPDATE";
        secUserCheck.maker = springSecurityService.currentUser as SecUser;
        secUserCheck.entityUrl = url;

        // encode the password
        // args['password'] = secUser.password; // springSecurityService.encodePassword(args['password'], '123456');
        // remove the password if it exists
        args.remove('password');

        //loop the boolean attributes.
        ['enabled', 'accountExpired', 'accountLocked', 'passwordExpired'].each { keyName ->
            if(!(keyName in args.keySet())) {
                args[keyName] = false;
            } else {
                args[keyName] = args[keyName]?.toBoolean();
            }
        }

        secUserCheck.jsonData = new JsonBuilder(args).toString();
        secUserCheck.makeDate = new Date();

        try{
            secUserCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the check method: {}", e.message)
            e.printStackTrace();
            throw e
        }

        return secUserCheck;
    }

    @Transactional
    SecUser save(SecUser secUser) {
        secUser.save(failOnError: true)

        return secUser;
    }

    @Transactional
    SecUser save(SecUser secUser, List<SecRole> roles) {

        //persist the user object first
        secUser.save(failOnError: true)

        //remove all the roles that this user is associated with
        SecUserSecRole.removeAll(secUser);

        //Now add the roles to the user
        roles.each { SecRole role ->
            SecUserSecRole.create(secUser, role);
        }

        //Let us also add the previous password object
        SecUserPreviousPassword secUserPreviousPassword = new SecUserPreviousPassword(
                secUser: secUser,
                password: secUser.password
        );
        secUserPreviousPassword.save(failOnError: true);

        return secUser;
    }

    @Transactional
    SecUser updatePassword(SecUser secUser, String password) {
        //evict from the Hibernate cache
        Session session = sessionFactory.currentSession;

        secUser.password = password;
        secUser.passwordUpdateDate = new Date();

        secUser.initialLogin = false;
        // also un-expire the password
        secUser.passwordExpired = false;
        secUser.save(failOnError: true, flush: true);

        // also save the password in the previous model
        secUserPreviousPasswordService.create(secUser, password);

        // attach the entity to the current session.
        secUser.attach();
        // evict the record from the session cache
        session.evict(secUser);

        return secUser;
    }

    @Transactional
    SecUser updateUserPassword(SecUser secUser, String password) {

        secUser.dirtyStatus = CheckStatus.EDITING;
        secUser.dirtMaker = springSecurityService.currentUser as SecUser;

        Map args = [password: springSecurityService.encodePassword(password), initLogin: false];

        // create the url
        String url = grailsLinkGenerator.link(controller: 'user', action: 'show', id: secUser.id, absolute: false);

        // generate the maker-checker
        SecUserCheck secUserCheck = new SecUserCheck();
        secUserCheck.username = secUser.username;
        secUserCheck.entity = secUser;
        secUserCheck.entityName = "User";
        secUserCheck.actionName = "PASSWORD-RESET";
        secUserCheck.maker = springSecurityService.currentUser as SecUser;
        secUserCheck.entityUrl = url;
        secUserCheck.jsonData = new JsonBuilder(args).toString();
        secUserCheck.makeDate = new Date();
        secUserCheck.save(failOnError: true)

        return secUser;
    }

    List<SecUser> getExpiredPasswordUsers() {

        //read from the configurations in future.
        Integer expirePeriod = Integer.parseInt(grailsApplication.config.get('metasoft.app.password.expirePeriod'));

        Date cutoffDate = new Date() - expirePeriod;

        List<SecUser> secUsers = SecUser.where {
            passwordUpdateDate <= cutoffDate
            passwordExpired == false
            !(username in ['flexcube', 'system'])
        }.list();

        return secUsers;
    }

    /**
     * Gets list of all users whose passwords have expired and expires the passwords.
     *
     * @return Long returns the number of user entities processed.
     */
    @Transactional
    Long expirePasswords() {
        List<SecUser> secUsers = getExpiredPasswordUsers();

        Long processedRecords = 0;
        for(SecUser secUser in secUsers) {

            try {
                secUser.passwordExpired = true;
                secUser.save();
                secUser.initialLogin = true;
                userCache.removeUserFromCache(secUser.username);
                processedRecords++;
            } catch (Exception e) {
                log.error("problem expiring password for user: $secUser.username", e)
            }

        }

        return processedRecords;
    }

    @Cacheable("systemUser")
    SecUser getSystemUser() {
        return SecUser.findByUsername('system');
    }

    List<SecRole> getUserRoles(boolean showHidden = false) {
        return SecRole.where {
            if(!showHidden) {
                !(authority in ['ROLE_SUPERUSER'])
            }
        }.list([sort: 'authority', order: 'asc'])
    }

    List<SecRole> listUserRoleOptions(boolean showHidden = false) {
        return SecRole.where {
            if(!showHidden) {
                !(authority in ['ROLE_SUPERUSER', 'ROLE_API_USER'])
            }
        }.list([sort: 'authority', order: 'asc'])
    }

}