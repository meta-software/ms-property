package metasoft.property.core

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.cache.Cacheable

@ReadOnly
class CurrencyService {

    SettingsService settingsService

    Currency get(Serializable id) {
        return Currency.get(id);
    }

    List<Currency> list(Map args) {
        return Currency.list(args)
    }

    List<Currency> listOptions(Map args) {
        return Currency.list(args)
    }

    Long count() {
        return Currency.count()
    }

    @Transactional
    Currency save(Currency currency) {
        return currency.save()
    }

    Currency getReportingCurrency() {
        String currency = settingsService.getValue('haka.app.financials.reportingCurrency')
        return this.get(currency);
    }

}