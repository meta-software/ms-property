package metasoft.property.core

import grails.gorm.services.Service

@Service(PropertyInsurance)
interface PropertyInsuranceService {

    PropertyInsurance get(Serializable id)

    List<PropertyInsurance> list(Map args)

    Long count()

    void delete(Serializable id)

    PropertyInsurance save(PropertyInsurance propertyInsurance)

}