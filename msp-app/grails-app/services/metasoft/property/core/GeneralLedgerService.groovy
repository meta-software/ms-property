package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.cache.Cacheable
import grails.plugin.springsecurity.SpringSecurityService

//@GrailsCompileStatic
class GeneralLedgerService {

    SpringSecurityService springSecurityService;
    SettingsService settingsService;

    @Cacheable("isAccountsSameCurrency")
    @ReadOnly
    Boolean isAccountsSameCurrency(String accountNoA, String accountNoB) {
        GlBankAccount glBankAccountA = GlBankAccount.where {
                generalLedger.accountNumber == accountNoA
        }.get();

        GlBankAccount glBankAccountB = GlBankAccount.where {
            generalLedger.accountNumber == accountNoB
        }.get()

        log.info("what are they ${accountNoA} == ${glBankAccountB?.currency}")

        return (
                (glBankAccountA && glBankAccountA) &&
                (glBankAccountA?.currency == glBankAccountB?.currency)
        );
    }

    @Cacheable("isAccountValidCurrency")
    @ReadOnly
    Boolean isAccountValidCurrency(String accountNo, String currencyArg) {
        return GlBankAccount.where {
            currency.id == currencyArg
            generalLedger.accountNumber == accountNo
        }.count() > 0
    }

    GeneralLedger get(Serializable id) {
        return GeneralLedger.get(id)
    }

    @ReadOnly
    List<GeneralLedger> list(Map args) {
        println("this si awesoem ${args}")
        args['sort'] = 'accountName'
        args['order'] = 'asc'

        def query = GeneralLedger.where {
            def gl = GeneralLedger
            if(args?.accountName) {
                accountName ==~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
            if(args?.generalLedgerType) {
                generalLedgerType.id == args.generalLedgerType
            }
            if(args?.accountType) {
                generalLedgerType.name ==~ "%${args.accountType}%"
            }
            if(args?.currency) {
                print("Are we getting this ${args.currency}")
                /* exists GlBankAccount.where {
                    generalLedger.id == gl.id
                    currency.id == args.currency
                }.id()  */
            }
        }

        return query.list(args)
    }

    List<GeneralLedger> outbox(Map args) {

        Long currentUserId = (Long)springSecurityService.currentUserId;

        args['sort'] = 'accountName'
        args['order'] = 'asc'

        def query = GeneralLedger.where {
//            checkStatus in [CheckStatus.EDITING, CheckStatus.PENDING]
//            maker.id == currentUserId

            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
            if(args?.generalLedgerType) {
                generalLedgerType.id == args.generalLedgerType
            }
        }

        return query.list(args)
    }

    @ReadOnly
    List<GeneralLedger> getBanks() {
        Map args = [order: 'asc', 'sort': 'accountName']

        def query = GeneralLedger.where {
            generalLedgerType.name in ['Bank', 'Cash']
        }.join('generalLedgerType')
        .join('bankAccount');

        return (List<GeneralLedger>)query.list(args);
    }

    Long count() {
        return GeneralLedger.count()
    }

    Long countOutbox() {
        return GeneralLedger.count()
    }

    @Transactional
    void delete(Serializable id) {
        GeneralLedger generalLedger = GeneralLedger.get(id)

        if(generalLedger) {
            generalLedger.delete();
        }
    }

    @Transactional
    GeneralLedger save(GeneralLedger generalLedger) {
        generalLedger.save(failOnError: true)
    }

    @ReadOnly
    GeneralLedger findControlAccount(String accountNumber, String subAccountNumber = null) {
        GeneralLedger generalLedger = null;

        // if the account is a tenant account
        if(accountNumber.startsWith("001")) { // if tenant
            generalLedger = GeneralLedger.findByAccountName('LESSEES TRUST CONTROL');
        } else if (accountNumber.startsWith("002")) { // if landlord
            generalLedger = GeneralLedger.findByAccountName('LESSORS TRUST CONTROL');
        } else if (accountNumber.startsWith("003")) { // if supplier
            generalLedger = GeneralLedger.findByAccountName('TRADE CREDITORS CONTROL');
        } else if (accountNumber.startsWith("100")) { // if GeneralLedger account (Nominal account)
            generalLedger = GeneralLedger.where {
                accountNumber == "${accountNumber}"
                if(subAccountNumber) {
                    subAccount.accountNumber == subAccountNumber
                }
            }.get(); // @todo: this needs a relook.
        }

        return generalLedger;
    }
}