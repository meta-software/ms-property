package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugins.mail.MailService

class EmailSendRequestService {

    MailService mailService;

    void processPendingRequests() {
        List<EmailSendRequest> emailSendRequests = EmailSendRequest.where {
            sendStatus == EmailSendRequest.SendStatus.PENDING
        }.list();

        emailSendRequests.each { EmailSendRequest emailSendRequest ->
            try {
                sendRequest(emailSendRequest);

                EmailSendRequest.withTransaction {
                    emailSendRequest.sendStatus = EmailSendRequest.SendStatus.SENT;
                    emailSendRequest.save(flush: true);
                }
            } catch(Exception e) {
                EmailSendRequest.withTransaction {
                    emailSendRequest.sendStatus = EmailSendRequest.SendStatus.FAILED;
                    emailSendRequest.sendResponse = e.message;
                    emailSendRequest.save(flush: true);
                }
            }
        }
    }

    EmailSendRequest sendRequest(EmailSendRequest emailSendRequest) {

        if(emailSendRequest.multipart) {
            File attachmentFile = new File(emailSendRequest.attachmentFile);

            mailService.sendMail {
                multipart emailSendRequest.multipart

                from emailSendRequest.sender
                to emailSendRequest.recipient
                subject emailSendRequest.subject
                html emailSendRequest.message
                attach emailSendRequest.attachmentFilename, attachmentFile
            }
        } else {
            mailService.sendMail {
                from emailSendRequest.sender
                to emailSendRequest.recipient
                subject emailSendRequest.subject
                html emailSendRequest.message
            }
        }

        return emailSendRequest;
    }

    @Transactional
    EmailSendRequest create(EmailSendRequestCommand command) {
        EmailSendRequest emailSendRequest = command as EmailSendRequest;
        return emailSendRequest.save();
    }

}

class EmailSendRequestCommand {
    String recipient;
    String sender;
    String recipientCC;
    String subject;
    String message;
    boolean multipart = false;
    String attachmentFilename;
    String attachmentFile;
    EmailSendRequest.SendStatus sendStatus;
    String sendResponse;

    Object asType(Class type) {
        if(type == EmailSendRequest.class) {
            EmailSendRequest emailSendRequest = new EmailSendRequest(
                    recipient: this.recipient,
                    sender: this.sender,
                    recipientCC: this.recipientCC,
                    subject: this.subject,
                    message: this.message,
                    multipart: this.multipart,
                    attachmentFilename: this.attachmentFilename,
                    attachmentFile: this.attachmentFile
            );
            return emailSendRequest;
        }
    }
}
