package metasoft.property.core

import grails.events.annotation.Publisher
import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import groovy.time.TimeCategory
import org.springframework.validation.BindException

class BillingCycleService {

    @Publisher('billingCycle:activated')
    @Transactional
    BillingCycle activateNewBillingCycle() {
        BillingCycle currentCycle = getCurrentCycle();
        currentCycle.active = false; // deactivate the current cycle
        currentCycle.save();

        Date nextDate = currentCycle.endDate + 2;
        BillingCycle newBillingCycle = getBillingCycle(nextDate);

        // this should never ever return empty otherwise we have a serious problem
        if(newBillingCycle == null) {
            // how about creating the new billing cycle
            throw new InvalidBillingCycleException("New billing cycle does not exists or was not configured correctly");
        }

        newBillingCycle.active = true;
        newBillingCycle.open = true;
        return newBillingCycle.save(flush: true);
    }

    /**
     * This will close all other billing cycles which are not the active billing cycle. Regardless of how or when they were opened.
     *
     */
    @Transactional
    @Publisher("billingCycle:previousClosed")
    BillingCycle closePreviousBillingCycle() {

        //get the currentBilling cycle;
        BillingCycle currentCycle = getCurrentCycle();
        currentCycle.discard() //we do not want to have this updated at all
        BillingCycle previousCycle = getBillingCycle(currentCycle.startDate - 5);
        previousCycle.open = false;

        return previousCycle.save(flush: true);
    }

    BillingCycle get(Serializable id) {
        return BillingCycle.get(id)
    }

    List<BillingCycle> list(Map args) {
        args['order'] = 'desc'
        args['sort'] = 'name'
        String query = args.get('query')
        String isOpen = args.get('open')
        Boolean isOpenBool = isOpen == '1'

        return BillingCycle.where {
            if(query) { name ==~ "%$query%" }
            if(isOpen in ['1', '0']){ open == isOpenBool }
        } .list(args)
    }

    Long count(Map args) {

        String query = args.get('query')
        String isOpen = args.get('open')

        return BillingCycle.where {
            if(query) { name ==~ "%$query%" }
            if(isOpen){ open == isOpen }
        } .count();
    }

    List<BillingCycle> listOptions(Map params) {

        String query = params['q'];
        params['sort'] = 'name'
        params['order'] = 'desc'

        return BillingCycle.where{
            open == true
            if(query) {
                name ==~ "${query}%"
            }

        }.list(params)
    }

    Long count() {
        return BillingCycle.count();
    }

    @Transactional
    BillingCycle save(BillingCycle billingCycle) {
        return billingCycle.save()
    }

    @Transactional
    BillingCycle update(Long id, BillingCycle billingCycle) {
        return billingCycle.save()
    }

    //@CacheEvict('openCyclesCache')
    @Transactional
    BillingCycle closeCycle(BillingCycle billingCycle) {

        // closing an active cycle is forbidden
        if(billingCycle.active) {
            Object[] errorArgs = []
            BindException errors = new BindException(billingCycle, BillingCycle.class.name);
            errors.rejectValue('active', 'billingCycle.illegal.action', errorArgs,"Closing an active billing cycle is not allowed")
            billingCycle.errors = errors;
            throw new IllegalBillingCycleActionException("Closing an active billing cycle is not allowed", errors);
        }

        //Close current cycle
        billingCycle.active = false
        billingCycle.open = false
        return billingCycle.save();
    }

    //@Cacheable('openCyclesCache')
    BillingCycle getCurrentCycle() {
        BillingCycle.where {
            active == true
        }.find();
    }

    BillingCycle getBillingCycle(Date eventDate) {
        BillingCycle billingCycle = BillingCycle.where{
            startDate <= eventDate
            endDate >= eventDate
        }.get();
    }

    Boolean conflictExists(Date startDate, Date endDate) {
        //@todo: add application code
        return false
    }

    @Transactional
    BillingCycle activateCycle(BillingCycle billingCycle) {

        if(billingCycle == null) {
            log.error("billing cycle was not found or does not exist");

            BindException errors = new BindException()
            String message = 'The specified billing cycle was not found or does not exist'
            errors.reject('billingCycle.not.exists', message)
            throw new ValidationException(message, errors);
        }

        if(billingCycle.active) {
            log.info("Trying to activate an already active cycle, no action implemented");
            return billingCycle;
        }

        // deactivate whoever is currently set as active
        BillingCycle.where {
            active == true
        }.updateAll([active: false])

        // now set the provided billingCycle as the active cycle
        billingCycle.active = true;
        billingCycle.open = true;
        return billingCycle.save(flush: true);
    }

    @Transactional
    BillingCycle openCycle(BillingCycle billingCycle) {

        if(billingCycle == null) {
            log.error("billing cycle was not found or does not exist");

            BindException errors = new BindException()
            String message = 'The specified billing cycle was not found or does not exist'
            errors.reject('billingCycle.not.exists', message)
            throw new ValidationException(message, errors);
        }

        if(billingCycle.open) {
            log.info("Trying to open an already open cycle, no action implemented");
            return billingCycle;
        }

        // now set the provided billingCycle as the active cycle
        billingCycle.open = true;
        return billingCycle.save(flush: true);
    }

}