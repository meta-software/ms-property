package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.time.TimeCategory

import static metasoft.property.core.LeaseReview.ReviewStatus

//@GrailsCompileStatic
class LeaseReviewService {

    GrailsApplication grailsApplication;
    SpringSecurityService springSecurityService;

    Map dashboard() {
        Map dashboard = [reviewCount: 0]
        dashboard['reviewCount'] = LeaseReview.where{reviewStatus == ReviewStatus.PENDING}.count();

        return dashboard;
    }

    List<LeaseReview> list(Map args) {
        return LeaseReview.list(args);
    }

    List<LeaseReview> pending(Map args) {

        String query = args['query'];

        return LeaseReview.where{

            if(query) {
                ( lease.leaseNumber =~ "%$query%" || lease.tenant.accountName ==~ "%$query%" )
            }

            reviewStatus == LeaseReview.ReviewStatus.PENDING
        }.list(args);
    }

    List<LeaseReview> reviewed(Map args) {
        return LeaseReview.where{
            reviewStatus != LeaseReview.ReviewStatus.PENDING
        }.list(args);
    }

    LeaseReview get(Serializable id) {
        return LeaseReview.get(id);
    }

    @Transactional
    LeaseReview save(LeaseReview leaseReview) {

        if(leaseReview.createdBy == null) {
            SecUser currentUser = springSecurityService.currentUser as SecUser;
            String currentUsername = currentUser == null ? grailsApplication.config.get("metasoft.app.audit.defaultUser") : currentUser.username;
            leaseReview.createdBy = currentUsername;
        }

        leaseReview.save(failOnError: true);
    }

    @Transactional
    LeaseReview update(LeaseReview leaseReview) {

        if(leaseReview.updatedBy == null) {
            SecUser currentUser = springSecurityService.currentUser as SecUser;
            String currentUsername = currentUser == null ? grailsApplication.config.get("metasoft.app.audit.defaultUser") : currentUser.username;
            leaseReview.updatedBy = currentUsername;
            leaseReview.reviewedBy = currentUsername;
        }

        leaseReview.save(failOnError: true);
    }

    void processReviewLeases() {
        Date queryDate = new Date().clearTime();
        use(TimeCategory) {
            queryDate = queryDate - 3.months;
        }

        // retrieve all leases required to be reviewed
        List<Lease> leases = Lease.where {
            terminated == false
            expired == false
            pendingReview == false
            lastReviewDate <= queryDate
        }.list();

        for(Lease lease: leases) {
            LeaseReview leaseReview = new LeaseReview();
            leaseReview.lease = lease;
            leaseReview.lastReviewDate = lease.lastReviewDate;
            leaseReview.reviewStatus = ReviewStatus.PENDING;
            leaseReview.leaseReviewType = LeaseReviewType.findByName('Lease Review');

            Lease.withTransaction {
                lease.pendingReview = true;
                lease.save(failOnError: true);
            }

            save(leaseReview);
        }
    }

    @Transactional
    LeaseReview ignore(Long id) {
        LeaseReview leaseReview = get(id);

        leaseReview.reviewStatus = LeaseReview.ReviewStatus.IGNORED;
        leaseReview.reviewDate = new Date();

        //also update the affected lease
        Lease lease = leaseReview.lease;
        lease.lastReviewDate = leaseReview.lastReviewDate;
        lease.save(failOnError: true);
        //update the next review date
        update(leaseReview);
    }
}
