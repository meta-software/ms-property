package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.validation.ValidationException
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import metasoft.property.ExchangeRateConvertCommand
import metasoft.property.core.exceptions.ExchangeRateNotFoundException
import org.springframework.validation.BindException

@Slf4j
@ReadOnly
//@GrailsCompileStatic
class ExchangeRateService {

    GrailsApplication grailsApplication
    CurrencyService currencyService

    //@Cacheable('exchangeRate')
    ExchangeRate getActiveExchangeRate(Currency sourceCurrency, Currency destCurrency, Date rateDate) {
        log.debug("retrieving active currency [from={}, to={}, date={}]", sourceCurrency, destCurrency, rateDate);
        ExchangeRate exchangeRate = ExchangeRate.where {
            fromCurrency == sourceCurrency
            toCurrency == destCurrency
            active == true
            if(destCurrency != sourceCurrency) {
                (startDate <= rateDate && endDate >= rateDate)
            }
        }.get();

        if(!exchangeRate) {
            String message = String.format("Exchange rate was not found for SourceCurrency=${sourceCurrency.id}, DestCurrency=${destCurrency.id} on ${rateDate?.format('yyyy-MM-dd')}")
            throw new ExchangeRateNotFoundException(message)
        }

        return exchangeRate
    }

    ExchangeRate get(Serializable id) {
        return ExchangeRate.get(id)
    }

    List<ExchangeRate> list(Map args) {

        args['order'] = 'asc'
        args['sort'] = 'id'

        def activeParam = args.get('active', true)
        def deletedParam = args.get('deleted', false)

        return ExchangeRate.where {
            deleted == deletedParam
            active == activeParam
        }.list(args);
    }

    List<ExchangeRate> listActive(Map args) {

        args['order'] = 'desc'
        args['sort']  = 'endDate'
        args['max']   = 6

        def groupId = args['groupId']

        return ExchangeRate.where{
            deleted == false
            active == true
            exchangeRateGroup != null

            if(groupId) {
                exchangeRateGroup.id == groupId
            }

        }.list(args);
    }

    Long count(Map args) {
        return (Long)ExchangeRate.where {
            deleted == false
            active == true
        }.count()
    }

    Boolean exists(Currency fromCurrency, Currency toCurrency, Date date) {
        ExchangeRate activeExchangeRate = getActiveExchangeRate(fromCurrency, toCurrency, date)
    }

    Map convertToReporting(BigDecimal value, String fromCurrency, Date date) {

        ExchangeRateConvertCommand command = new ExchangeRateConvertCommand()

        Currency reportingCurrency = currencyService.getReportingCurrency();
        Currency sourceCurrency = currencyService.get(fromCurrency);
        ExchangeRate activeExchangeRate = null
        try {
            activeExchangeRate = getActiveExchangeRate(sourceCurrency, reportingCurrency, date)
        } catch(ExchangeRateNotFoundException e) {
            BindException errors = new BindException(command, "ExchangeRateConvert")
            String message = e.getMessage()
            Object[] errorArgs = new Object[3];
            errorArgs[0] = sourceCurrency.currencyCode
            errorArgs[1] = reportingCurrency.currencyCode
            errorArgs[2] = date?.format('yyyy-MM-dd')
            errors.reject('exchangeRate.notFound', errorArgs, message)

            throw new ValidationException(message, errors);
        }

        BigDecimal convertedValue = convert(value, activeExchangeRate);

        Map resultMap = [
                value: convertedValue,
                currency: reportingCurrency.id
        ]

        return resultMap
    }

    BigDecimal convert(BigDecimal amount, ExchangeRate exchangeRate) {
        log.debug("converting the exchange for values [exchangeRate={}, amount={}]", exchangeRate, amount);

        if(amount == BigDecimal.ZERO) {
            return amount;
        }

        BigDecimal baseValue = amount;

        if(exchangeRate.fromCurrency != exchangeRate.toCurrency) {
            baseValue = amount * exchangeRate.rate;
        }

        return baseValue
    }

    BigDecimal convertTo(BigDecimal amount, Currency fromCurrency, Currency toCurrency, Date date) {
        ExchangeRate exchangeRate = getActiveExchangeRate(fromCurrency, toCurrency, date);

        //@todo: throw exception if the exchange rate does not exist
        if(exchangeRate == null) {
            throw new RuntimeException("ExchangeRate was not found for [fromCurrency=${fromCurrency} and toCurrency=${toCurrency}]")
        }

        return convert(amount, exchangeRate)
    }

    @Transactional
    void delete(Serializable id) {
        ExchangeRate exchangeRate = ExchangeRate.get(id)

        if(exchangeRate && !exchangeRate.deleted) {
            exchangeRate.deleted = true;
            exchangeRate.dateDeleted = new Date();
            exchangeRate.save();
        }
    }

    @Transactional
    ExchangeRate save(ExchangeRate exchangeRate) {
        return exchangeRate.save()
    }

    @Transactional
    ExchangeRate save(Long id, ExchangeRate exchangeRate) {
        return exchangeRate.save()
    }

    @GrailsCompileStatic(TypeCheckingMode.SKIP)
    def transactionCurrencies(def transactionTmp, Currency operatingCurrency, Currency reportingCurrency) {

        Currency transactionCurrency = transactionTmp.transactionCurrency;

        //retrieve the invoice(operating) currency
        ExchangeRate operatingExchangeRate   = exchangeRateService.getActiveExchangeRate(transactionCurrency, operatingCurrency, transactionTmp.transactionDate);
        transactionTmp.operatingCurrency     = operatingCurrency;
        transactionTmp.operatingAmount       = exchangeRateService.convert(transactionTmp.amount, operatingExchangeRate);
        transactionTmp.operatingExchangeRate = operatingExchangeRate;

        //retrieve the reporting currency
        ExchangeRate reportingExchangeRate   = exchangeRateService.getActiveExchangeRate(transactionCurrency, reportingCurrency, transactionTmp.transactionDate);
        transactionTmp.reportingCurrency     = reportingCurrency;
        transactionTmp.reportingAmount       = exchangeRateService.convert(transactionTmp.amount, reportingExchangeRate);
        transactionTmp.reportingExchangeRate = reportingExchangeRate;

        return transactionTmp;
    }

    @GrailsCompileStatic(TypeCheckingMode.SKIP)
    private ExchangeRateService getExchangeRateService() {
        return (ExchangeRateService)grailsApplication.mainContext.exchangeRateService;
    }

}