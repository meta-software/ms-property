package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import metasoft.property.core.exceptions.ExchangeRateNotFoundException
import org.springframework.validation.BindException

class BulkInvoiceService {

    CurrencyService currencyService
    ExchangeRateService exchangeRateService
    RentalUnitService rentalUnitService;
    PropertyService propertyService;
    LeaseService leaseService;
    BillingCycleService billingCycleService;
    MakerCheckerService makerCheckerService;
    TransactionTmpService transactionTmpService

    SpringSecurityService springSecurityService

    TransactionBatchTmpService transactionBatchTmpService;

    SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    BulkInvoice get(Serializable id) {
        return BulkInvoice.get(id);
    }

    List<BulkInvoice> list(Map args) {
        String refParam = args?.ref;

        return BulkInvoice.where{
            if(refParam) {
                transactionReference ==~ "%${refParam}%"
            }
        }.list(args);
    }

    List<BulkInvoice> listInbox(Map args) {
        String refParam = args?.ref;
        String postedArg = args?.posted;

        return BulkInvoice.where{
            checkStatus == CheckStatus.PENDING
            if(refParam) {
                transactionReference ==~ "${refParam}%"
            }
            if(postedArg) {
                posted == (postedArg == '1')
            }
        }.list(args);
    }

    List<BulkInvoice> listOutbox(Map args) {
        String refParam = args?.ref;

        return BulkInvoice.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
            if(refParam) {
                transactionReference ==~ "%${refParam}%"
            }
        }.list(args);
    }

    List<BulkInvoice> listCancelled(Map args) {
        String refParam = args?.ref;

        return BulkInvoice.where{
            checkStatus == CheckStatus.CANCELLED
            maker == currentUser
            if(refParam) {
                transactionReference ==~ "%${refParam}%"
            }
        }.list(args);
    }

    Long count() {
        return BulkInvoice.count()
    }

    Long count(params) {
        return BulkInvoice.count()
    }

    void delete(Serializable id) {
        BulkInvoice bulkInvoice = get(id)
        bulkInvoice.delete(failOnError: true);
    }

    @Transactional
    BulkInvoice save(BulkInvoice bulkInvoice) {

        // update the billingCycle
        //bulkInvoice.billingCycle = billingCycleService.getBillingCycle(bulkInvoice.transactionDate);

        // prepare the bulkInvoiceItems and save them;

        bulkInvoice.save(failOnError: true);
    }

    @Transactional
    BulkInvoice create(BulkInvoice bulkInvoice) {

        Currency reportingCurrency = currencyService.reportingCurrency;

        bulkInvoice.transactionType = TransactionType.findByTransactionTypeCode('IV');

        // makerChecker information preparation
        bulkInvoice.maker = currentUser;
        bulkInvoice.dirtMaker = currentUser;
        bulkInvoice.dirtyStatus = CheckStatus.PENDING;
        bulkInvoice.makeDate = new Date();
        try {
            bulkInvoice = (BulkInvoice) exchangeRateService.transactionCurrencies(bulkInvoice, reportingCurrency, reportingCurrency)
        } catch (ExchangeRateNotFoundException e) {
            BindException errors = new BindException(bulkInvoice, "BulkInvoice")
            String message = e.getMessage()
            Object[] errorArgs = new Object[3];
            errorArgs[0] = bulkInvoice.transactionCurrency.currencyCode
            errorArgs[1] = reportingCurrency.currencyCode
            errorArgs[2] = bulkInvoice.transactionDate?.format('yyyy-MM-dd')
            errors.reject('exchangeRate.notFound', errorArgs, message)
            throw new ValidationException(message, errors);
        }
        bulkInvoice.save(failOnError: true);

        // generate the transactionReference for the bulkInvoice
        bulkInvoice.transactionReference = String.format("BI%s%06d", bulkInvoice.transactionDate.format("yyMM"), bulkInvoice.id);
        bulkInvoice.save(failOnError: true);

        List<BulkInvoiceItem> bulkInvoiceItems = this.generateInvoiceItems(bulkInvoice.property
                ,bulkInvoice.amount
                ,bulkInvoice.transactionDate
                ,bulkInvoice.transactionReference
                ,bulkInvoice.billingCycle
                ,bulkInvoice.subAccount);

        bulkInvoiceItems.each { BulkInvoiceItem bulkInvoiceItem ->
            bulkInvoiceItem.bulkInvoice = bulkInvoice;
            bulkInvoice.addToBulkInvoiceItems(bulkInvoiceItem)
            bulkInvoiceItem.save(failOnError: true);

            // generate the transactionReference for the bulkInvoiceItem
            bulkInvoiceItem.transactionReference = String.format("BI%s%06d", bulkInvoice.transactionDate.format("yyMM"), bulkInvoiceItem.id);
            bulkInvoiceItem.save(failOnError: true);
        }
        //*/

        return bulkInvoice
    }

    List<BulkInvoiceItem> generateInvoiceItems(Property property, BigDecimal totalAmount, Date transactionDate, String transactionReference, BillingCycle billingCycle, SubAccount subAccount) {
        //retrieve the amount of the occupied space
        BigDecimal occupiedSpace = propertyService.getOccupiedSpace(property, billingCycle);

        // retrieve all the active leases for the specified property
        List<Lease> leases = leaseService.listByPropertyAndBillingCycle(property.id, billingCycle);

        // now generate a list of all the invoiceItems
        List<BulkInvoiceItem> bulkInvoiceItems = []

        leases.each { Lease lease->
            RentalUnit rentalUnit = lease.rentalUnit;

            BigDecimal rentalUnitAmount = occupiedSpace == 0 ? 0 : ( ( rentalUnit.area / (occupiedSpace) ) * totalAmount );

            BulkInvoiceItem bulkInvoiceItem = new BulkInvoiceItem(
                property: property,
                rentalUnit: rentalUnit,
                lease: lease,
                amount: rentalUnitAmount,
                transactionType: TransactionType.findByTransactionTypeCode('IV'),
                transactionDate: transactionDate,
                transactionReference: transactionReference
            )

            bulkInvoiceItems.add(bulkInvoiceItem);

        }

        return bulkInvoiceItems;
    }

    @Transactional
    BulkInvoice approve(BulkInvoice bulkInvoice) {
        validateInvoice(bulkInvoice);

        // the check entity
        bulkInvoice.checkStatus = CheckStatus.APPROVED;
        bulkInvoice.checker = currentUser;
        bulkInvoice.checkDate = new Date();
        bulkInvoice.checkComment = "Approved";
        bulkInvoice.save(failOnError: true, flush: true);

        //@todo: move this into it's own thread and make the thing faster.
        postInvoice(bulkInvoice);

        return bulkInvoice;
    }

    @Transactional
    BulkInvoice reject(BulkInvoice bulkInvoice, Map params) {
        validateInvoice(bulkInvoice);

        // the check entity
        bulkInvoice.checkStatus = CheckStatus.REJECTED;
        bulkInvoice.checker = currentUser;
        bulkInvoice.checkDate = new Date();
        bulkInvoice.checkComment = params['comment'];

        bulkInvoice.save(failOnError: true);

        //@todo: write code to send email to maker informing that the request has been rejected.

        return bulkInvoice;
    }

    @Transactional
    BulkInvoice cancel(BulkInvoice bulkInvoice, Map params) {
        validateInvoice(bulkInvoice);

        // the check entity
        bulkInvoice.checkStatus = CheckStatus.CANCELLED;
        bulkInvoice.checker = currentUser;
        bulkInvoice.checkDate = new Date();
        bulkInvoice.checkComment = params['comment'];

        bulkInvoice.save(failOnError: true);
        return bulkInvoice;
    }

    @Transactional
    BulkInvoice postInvoice(BulkInvoice bulkInvoice) {

        Currency reportingCurrency = currencyService.getReportingCurrency();

        bulkInvoice.posted = true;
        bulkInvoice.postedBy = currentUser?.username;
        bulkInvoice.save();

        // Create the TransactionBatch
        TransactionBatch transactionBatch = new TransactionBatch();
        transactionBatch.transactionType = bulkInvoice.transactionType;
        transactionBatch.maker = bulkInvoice.maker;
        transactionBatch.makeDate = bulkInvoice.makeDate;
        transactionBatch.checkStatus = CheckStatus.APPROVED;
        transactionBatch.checker = currentUser;
        transactionBatch.checkDate = new Date();
        transactionBatch.checkComment = bulkInvoice.checkComment;
        transactionBatch.batchNumber = bulkInvoice.transactionReference;
        transactionBatch.save(failOnError:true, flush:true);

        // generate the invoice items
        Long index = 0;
        for(BulkInvoiceItem bulkInvoiceItem : bulkInvoice.bulkInvoiceItems) {
            TransactionTmp transactionTmp = new TransactionTmp();

            transactionTmp.transactionType = bulkInvoice.transactionType;
            transactionTmp.transactionDate = bulkInvoice.transactionDate;
            transactionTmp.entryNumber = "0000";
            transactionTmp.transactionDate = bulkInvoice.transactionDate;
            transactionTmp.creditAccount = bulkInvoiceItem.lease.tenant.accountNumber;
            transactionTmp.debitAccount = bulkInvoiceItem.lease.tenant.accountNumber;
            transactionTmp.amount = bulkInvoiceItem.amount;
            transactionTmp.transactionReference = bulkInvoice.transactionReference+index;
            transactionTmp.description = "Invoice for ${bulkInvoice.subAccount.accountName}://${bulkInvoice.billingCycle.name}"
            transactionTmp.subAccount = bulkInvoice.subAccount;
            transactionTmp.lease = bulkInvoiceItem.lease;
            transactionTmp.rentalUnit = bulkInvoiceItem.rentalUnit;
            transactionTmp.property = bulkInvoice.property;
            transactionTmp.billingCycle = bulkInvoice.billingCycle;

            //set the currency details
            transactionTmp.transactionCurrency = bulkInvoice.transactionCurrency
            transactionTmp.transactionAmount = bulkInvoiceItem.amount

            //retrieve the invoice(operating) currency
            transactionTmp = transactionTmpService.transactionCurrencies(transactionTmp, transactionTmp.lease.invoicingCurrency, reportingCurrency)

            // we do not want to persist this object
            transactionTmp.skipValidation(true);
            transactionTmp.discard();
            // now generate the transaction
            transactionBatchTmpService.invoicing(bulkInvoice.transactionType, transactionTmp, transactionBatch);

            //Extra fields to add clarity to transaction data
            index++;
        }

        // save the transactionBatch
        transactionBatch.save(failOnError: true);

        // create the list of transactions here.
        return bulkInvoice
    }

    boolean validateInvoice(BulkInvoice bulkInvoice) {

        if(bulkInvoice.isChecked()) {
            BindException errors = new BindException(bulkInvoice, "bulkInvoice")
            errors.rejectValue("posted", "bulkInvoice.posted.already", "Bulk Invoice with reference# ${bulkInvoice.transactionReference} is already checked.")
            def message = "Invalid Action: Bulk Invoice with reference# ${bulkInvoice.transactionReference} is already checked."
            throw new ValidationException(message, errors)
        }

        return true;
    }



}