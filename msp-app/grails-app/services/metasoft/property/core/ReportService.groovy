package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import groovy.sql.Sql

@GrailsCompileStatic
class ReportService {

    Sql groovySql

    def tenantStatement(Map args) {

        def accountNumber = args['tenant'];

        def statementSql = """
            select 
                sum(ifnull(debit,0)-ifnull(credit,0)) balance 
            from ms_transaction 
                where account_number='${accountNumber}'
                    """ as String

        //Retrieve the single tenant details
        def tenantDetails = groovySql.firstRow("""
                select 
                    *
                from 
                    ms_account 
                where 
                    data_type='tenant'
                    and account_number='${accountNumber}' """);

        def statementQuery = groovySql.rows(statementSql);

        println(statementQuery)

        return [tenantDetails: tenantDetails, statementResultSet: statementQuery]
    }

}
