package metasoft.property.core

import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional
import metasoft.property.InvoiceCommand
import metasoft.property.ReceiptCreateCommand
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.transaction.annotation.Propagation

@Transactional
class MigrationService {

    GrailsApplication grailsApplication

    TransactionTmpService transactionTmpService;
    TransactionBatchTmpService transactionBatchTmpService;
    ReceiptTmpService receiptTmpService
    CurrencyService currencyService

    @Autowired
    ReceiptBatchTmpService receiptBatchTmpService
    ReceiptBatchCheckService receiptBatchCheckService


    @Transactional
    def loadTenantDeposits() {

        def processInvoice = true;

        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def depositFilepath = "resources/fixtures/tenant-deposits.txt";
        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${depositFilepath}");

        def counter = 0;
        File errorsFile = new File(System.getProperty('user.home')+"/tenant-deposit.errors.txt")

        // retrieve the deposit SubAccount
        def depositSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_ACCOUNT_NUMBER);

        fixturesResource.file.splitEachLine(";") { fields ->
            counter++;

            String bankAccountNumber = fields[0]?.trim()
            String unitRef = fields[1]?.trim()
            BigDecimal amount = 0

            try {
                amount = fields[4]?.trim() as BigDecimal
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }

            Tenant tenantArg = Tenant.findByBankAccountNumber(bankAccountNumber)

            //get the tenant information
            Lease lease = Lease.where({
                tenant == tenantArg
                rentalUnit.unitReference == unitRef
            }).get();

            if(tenantArg == null) {
                log.error "[tenant] the tenant [${bankAccountNumber}] was not found for the tenant.accountName = [${bankAccountNumber}]"
                errorsFile <<"[tenant] the tenant [${bankAccountNumber}] was not found for the tenant.accountName = [${bankAccountNumber}]\n"
                return
            }

            if(lease == null) {
                log.error "no [lease] was found for the tenant [${bankAccountNumber}] and rentalUnit.unitReference = [${unitRef}]";
                errorsFile << "no [lease] was found for the tenant [${bankAccountNumber}] and rentalUnit.unitReference = [${unitRef}]\n"
                return
            }

            RentalUnit rentalUnit = lease.rentalUnit
            Currency zwlCurrency = currencyService.get("ZWL"); // Currency.get("ZWL");

            zwlCurrency.discard();
            lease.discard();
            tenantArg.discard();

            //items to discard
            zwlCurrency.discard();
            lease.discard();
            tenantArg.discard();


            if(true) {

                // generate the deposit invoice for the tenant (Put each in each batch)
                TransactionType transactionType = TransactionType.get(2); // invoice
                TransactionBatchTmp batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
                batch.save(flush: true)

                //retrieve the rentalUnit
                def transactionParams = [
                        //rentalUnit: [id: rentalUnit.id],
                        lease: lease,
                        //transactionType: transactionType,
                        //entryNumber: null,
                        transactionDate:  new Date().parse("yyyy-MM-dd", "2022-08-31"),
                        creditAccount: tenantArg.accountNumber,
                        debitAccount: tenantArg.accountNumber,
                        subAccount: depositSubAccount,
                        amount: amount,
                        transactionCurrency: "ZWL",
                        //bankAccount: "100603",
                        description: "Deposit Mgt Invoice - ${rentalUnit.unitReference}",
                        transactionBatchTmp: batch
                ];
                InvoiceCommand depositInvoiceCommand = new InvoiceCommand(transactionParams)

                transactionTmpService.invoice(depositInvoiceCommand);

                //*/
                //now post the batch here.
                //post the transactionBatch.
                batch.refresh();
                log.info "posting the transaction batch with items-count: ${batch.transactionTmps}"
                TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(batch);
                log.info "finished with transaction-batch ::: [${transactionBatch.id}] ";

                batch.autoCommitted = true;
                batch.postedBy = "system";
                batch.datePosted = new Date();
                batch.save(flush: true);

                log.info "SUCCESSFULLY FINISHED deposit invoice loading the tenant details for [${bankAccountNumber}]"

            }

        }

    }

    @Autowired
    SessionFactory sessionFactory

    @Transactional
    def loadTenantDepositReceiptsTaks(String tenantRef, String unitRef, String currencyArg, BigDecimal amount, Integer counter) {

        File errorsFile = new File(System.getProperty('user.home')+"/tenant-deposit.errors.txt")

        // retrieve the deposit SubAccount
        SubAccount depositSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_ACCOUNT_NUMBER);

        GeneralLedger generalLedger = GeneralLedger.findByAccountNumber("100603");
        //items to discard

        Tenant tenantArg = Tenant.findByBankAccountNumber(tenantRef)

        String bankAccountNumber = tenantRef;

        //get the tenant information
        Lease lease = Lease.where({
            tenant == tenantArg
            rentalUnit.unitReference == unitRef
        }).get();

        if(tenantArg == null) {
            log.error "[tenant] the tenant [${bankAccountNumber}] was not found for the tenant.accountName = [${bankAccountNumber}]"
            errorsFile <<"[tenant] the tenant [${bankAccountNumber}] was not found for the tenant.accountName = [${bankAccountNumber}]\n"
            return
        }

        if(lease == null) {
            log.error "no [lease] was found for the tenant [${bankAccountNumber}] and rentalUnit.unitReference = [${unitRef}]";
            errorsFile << "no [lease] was found for the tenant [${bankAccountNumber}] and rentalUnit.unitReference = [${unitRef}]\n"
            return
        }

        RentalUnit rentalUnit = lease.rentalUnit
        Currency zwlCurrency = currencyService.get("ZWL");

        ReceiptBatchTmp depositReceiptBatchTmp = new ReceiptBatchTmp()
        depositReceiptBatchTmp.transactionType = TransactionType.findByTransactionTypeCode("RC")
        depositReceiptBatchTmp.checkStatus = CheckStatus.NEW
        depositReceiptBatchTmp.createdBy   = 'system'
        depositReceiptBatchTmp.save();

        ReceiptCreateCommand receiptCreateCommand = new ReceiptCreateCommand()
        receiptCreateCommand.transactionReference = String.format("RCD2%05d", counter)
        receiptCreateCommand.narrative = "MGT://Deposit Rcpt for - ${rentalUnit.unitReference}"
        receiptCreateCommand.transactionDate = new Date().parse("yyyy-MM-dd", '2022-08-31')
        receiptCreateCommand.tenant = tenantArg
        receiptCreateCommand.receiptAmount = amount
        receiptCreateCommand.debitAccount = generalLedger;
        receiptCreateCommand.transactionCurrency = zwlCurrency
        receiptCreateCommand.receiptBatchTmp = depositReceiptBatchTmp
        ReceiptItemTmp receiptItemTmp = new ReceiptItemTmp()
        receiptItemTmp.subAccount = depositSubAccount
        receiptItemTmp.allocatedAmount = amount
        receiptItemTmp.lease = lease
        receiptCreateCommand.receiptItemTmps.add(receiptItemTmp)
        receiptTmpService.createReceipt(receiptCreateCommand)
        ReceiptBatchCheck receiptBatchCheck = this.receiptBatchTmpService.postReceiptBatchRequest(depositReceiptBatchTmp)



//        depositReceiptBatchTmp.refresh();
        receiptBatchCheckService = (ReceiptBatchCheckService)grailsApplication.parentContext.getBean("receiptBatchCheckService")
        receiptBatchCheckService.approve(receiptBatchCheck);
        log.warn("###############FINISHED LOADING THE DEPOSIT RECEIPT..... ${unitRef} -> ${receiptBatchCheck}");
        return;
    }

    @Transactional
    def loadTenantDepositReceipts() {

        //Read the sample.fix fixtures file for testing purposes.
        def depositFilepath = "resources/fixtures/tenant-deposits.txt";
        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${depositFilepath}");

        def counter = 0;

        fixturesResource.file.splitEachLine(";") { fields ->
            counter++;

            String bankAccountNumber = fields[0]?.trim()
            String unitRef = fields[1]?.trim()
            BigDecimal amount = 0

            try {
                amount = fields[4]?.trim() as BigDecimal
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }

            this.loadTenantDepositReceiptsTaks(bankAccountNumber, unitRef, "ZWL", amount, counter)
        }

    }

}