package metasoft.property.core

class BulkInvoiceItemService {

    List<BulkInvoiceItem> listByBulkInvoice(Long bulkInvoiceId, Map args) {
        return BulkInvoiceItem.where {
            bulkInvoice.id == bulkInvoiceId
        }.list(args)
    }

    Long countByBulkInvoice(Long bulkInvoiceId) {
        return BulkInvoiceItem.where {
            bulkInvoice.id == bulkInvoiceId
        }.count();
    }

}