package metasoft.property.core

import grails.plugin.springsecurity.SpringSecurityService

class MakerCheckerService {

    SpringSecurityService springSecurityService;

    /**
     * Update the makerChecker object with the checkUser and the update date
     *
     * @param makerChecker
     * @return MakerCheckeable
     */
    MakerCheckeable update(MakerCheckeable makerCheckeable) {

        //get the checker to update
        //configure if the person trying to configure is the same maker.
        MakerChecker makerChecker = makerCheckeable.makerChecker;
        makerChecker.checker = springSecurityService?.currentUser as SecUser;
        makerChecker.checkDate = new Date();

        //because this is an embedded property
        makerCheckeable.markDirty('makerChecker');

        if(makerCheckeable.makerChecker.status == 'approved') {
            makerCheckeable.makerChecker.comment = 'Approved';
        }

        return makerCheckeable;
    }

    /**
     * Checks if the object already has makerChecker entity if not creates one, to be used during a MakerCheckeable entity creation.
     *
     * @param makerCheckeable
     * @return MakerCheckeable
     */
    MakerCheckeable initMakerChecker(MakerCheckeable makerCheckeable) {

        if(makerCheckeable.makerChecker == null) {
            MakerChecker makerChecker = new MakerChecker();
            makerChecker.status = "pending";
            makerChecker.maker  = springSecurityService?.currentUser as SecUser;

            makerCheckeable.makerChecker = makerChecker;
        }

        return makerCheckeable;
    }
}