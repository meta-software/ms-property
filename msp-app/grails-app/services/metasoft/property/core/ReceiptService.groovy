package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import groovy.util.logging.Slf4j

@ReadOnly
@Slf4j
@GrailsCompileStatic
class ReceiptService implements DataBinder{

    Receipt get(Serializable id) {
        return Receipt.get(id);
    }

    List<Receipt> list(Map args) {
        List<Receipt> receipts = Receipt.list(args);
        return receipts;
    }

    TransactionBatchService transactionBatchService;

    //@Transactional
    def postReceipt(Receipt receipt, TransactionBatch transactionBatch) {

        for(ReceiptItem receiptItem : receipt.receiptItems) {
            transactionBatchService.receipting(receiptItem, transactionBatch)
        }

    }

    Receipt fromReceiptTmp(ReceiptTmp receiptTmp) {
        Receipt receipt = new Receipt();
        bindData(receipt, receiptTmp.properties)
        receipt.transactionReference = receiptTmp.transactionReference;
        receipt.narrative = receiptTmp.narrative;
        receipt.transactionDate = receiptTmp.transactionDate;
        receipt.tenant = receiptTmp.tenant;
        receipt.receiptAmount = receiptTmp.receiptAmount;
        receipt.receiptStatus = Receipt.ReceiptStatus.OK;
        receipt.debitAccount = receiptTmp.debitAccount;

        for(ReceiptItemTmp receiptItemTmp : receiptTmp.receiptItemTmps) {
            ReceiptItem receiptItem = new ReceiptItem();
            bindData(receiptItem, receiptItemTmp.properties);

            receiptItem.receipt = receipt;
            receiptItem.lease = receiptItemTmp.lease;
            receiptItem.allocatedAmount = receiptItemTmp.allocatedAmount;
            receiptItem.subAccount = receiptItemTmp.subAccount;
            receiptItem.narrative = receiptTmp.narrative;

            receipt.addToReceiptItems(receiptItem);
            log.info("added an item to the receipt object.");
        }

        return receipt;
    }
}
