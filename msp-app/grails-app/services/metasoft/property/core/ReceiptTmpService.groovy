package metasoft.property.core

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import groovy.util.logging.Slf4j
import metasoft.property.ReceiptCreateCommand

@ReadOnly
@Slf4j
class ReceiptTmpService implements DataBinder{

    CurrencyService currencyService
    ExchangeRateService exchangeRateService
    SettingsService settingsService

    ReceiptTmp get(Serializable id) {
        return ReceiptTmp.get(id);
    }

    List<ReceiptTmp> list(Map args) {
        List<ReceiptTmp> receiptTmps = ReceiptTmp.list(args);
        return receiptTmps;
    }

    List<ReceiptTmp> listByBatch(Long batchId, Map args) {
        return ReceiptTmp.where {
            receiptBatchTmp.id == batchId
        }.list(args);
    }

    //@Transactional
    ReceiptTmp createReceipt(ReceiptCreateCommand receiptCreateCommand) {
        log.info("creating the temporary receipt object from the command {} ", receiptCreateCommand);

        ReceiptTmp receiptTmp = new ReceiptTmp();
        bindData(receiptTmp, receiptCreateCommand)

        ReceiptBatchTmp receiptBatchTmp = receiptTmp.receiptBatchTmp;
        receiptTmp.receiptStatus = Receipt.ReceiptStatus.OK;
        receiptTmp.statusChangeDate = new Date();
        receiptTmp.userAction = UserAction.CREATE;
        receiptTmp.narrative = receiptCreateCommand.narrative;

        //fix the receiptTmp Items
        receiptTmp = transactionCurrencies(receiptTmp, currencyService.reportingCurrency, currencyService.reportingCurrency);

        receiptBatchTmp.batchTotal += receiptTmp.receiptAmount;
        receiptBatchTmp.batchCount +=1;
        receiptBatchTmp.addToReceiptTmps(receiptTmp);
        receiptBatchTmp.save();

        return receiptTmp //.save(flush: true);
    }

    ReceiptTmp transactionCurrencies(ReceiptTmp receiptTmp, Currency operatingCurrency, Currency reportingCurrency) {

        Currency transactionCurrency = receiptTmp.transactionCurrency;

        //retrieve the invoice(operating) currency
        ExchangeRate operatingExchangeRate = exchangeRateService.getActiveExchangeRate(transactionCurrency, operatingCurrency, receiptTmp.transactionDate);
        receiptTmp.operatingCurrency     = operatingCurrency;
        receiptTmp.operatingAmount       = exchangeRateService.convert(receiptTmp.receiptAmount, operatingExchangeRate);
        receiptTmp.operatingExchangeRate = operatingExchangeRate;

        //retrieve the reporting currency
        ExchangeRate reportingExchangeRate = exchangeRateService.getActiveExchangeRate(transactionCurrency, reportingCurrency, receiptTmp.transactionDate);
        receiptTmp.reportingCurrency = reportingCurrency;
        receiptTmp.reportingAmount = exchangeRateService.convert(receiptTmp.receiptAmount, reportingExchangeRate);
        receiptTmp.reportingExchangeRate = reportingExchangeRate;

        receiptTmp.receiptItemTmps.each{ ReceiptItemTmp receiptItemTmp ->
            Lease lease = receiptItemTmp.lease
            receiptItemTmp.transactionCurrency = receiptTmp.transactionCurrency;
            receiptItemTmp.reportingCurrency = reportingCurrency;
            transactionCurrencies(receiptItemTmp, lease.invoicingCurrency, reportingCurrency);
        }

        return receiptTmp;
    }

    ReceiptItemTmp transactionCurrencies(ReceiptItemTmp receiptItemTmp, Currency operatingCurrency, Currency reportingCurrency) {

        ReceiptTmp receiptTmp = receiptItemTmp.receiptTmp
        Currency transactionCurrency = receiptItemTmp.transactionCurrency;

        //retrieve the invoice(operating) currency
        ExchangeRate operatingExchangeRate = exchangeRateService.getActiveExchangeRate(transactionCurrency, operatingCurrency, receiptTmp.transactionDate);
        receiptItemTmp.operatingCurrency     = operatingCurrency;
        receiptItemTmp.operatingAmount       = exchangeRateService.convert(receiptItemTmp.allocatedAmount, operatingExchangeRate);
        receiptItemTmp.operatingExchangeRate = operatingExchangeRate;

        //retrieve the reporting currency
        ExchangeRate reportingExchangeRate = exchangeRateService.getActiveExchangeRate(transactionCurrency, reportingCurrency, receiptTmp.transactionDate);
        receiptItemTmp.reportingCurrency = reportingCurrency;
        receiptItemTmp.reportingAmount = exchangeRateService.convert(receiptItemTmp.allocatedAmount, reportingExchangeRate);
        receiptItemTmp.reportingExchangeRate = reportingExchangeRate;

        return receiptItemTmp;
    }


    @Transactional
    void delete(Long id) {
        ReceiptTmp receiptTmp = get(id);

        if(receiptTmp) {
            receiptTmp.delete();

            ReceiptBatchTmp receiptBatchTmp = receiptTmp.receiptBatchTmp;
            receiptBatchTmp.batchTotal -= receiptTmp.receiptAmount;
            receiptBatchTmp.batchCount -=1;
            receiptBatchTmp.save();
        }
    }
}
