package metasoft.property.core

import grails.converters.JSON
import grails.events.Event
import grails.events.annotation.Subscriber
import grails.gorm.transactions.Transactional
class NotificationService {

    Notification get(Serializable id) {
        return Notification.get(id);
    }

    List<Notification> listUnreadNotifications(Map args) {

        def q = args['q']

        def query = Notification.where{
            isRead == false

            if(q) {
                message ==~ "%${q}%"
            }
        }

        return query.list(args)
    }

    List<Notification> list(Map args) {
        Notification.list(args);
    }

    Long count() {
        Notification.count();
    }

    @Transactional
    Notification save(Notification notification) {
        notification.save(failOnError: true)

        return notification;
    }

    @Transactional
    Notification markRead(Notification notification, SecUser secUser) {

        notification.readBy = secUser;
        notification.readDate = new Date();
        notification.save(failOnError: true)

        return notification;
    }

    @Transactional
    @Subscriber("app:notify")
    Notification createNotification(Event<Map> event) {

        final def data = event.data;
        final def urlParams = data['urlParams'] as JSON;

        Notification notification = new Notification();
        notification.entity = data['entity'] ?: '';
        notification.urlParams = urlParams.toString();
        notification.message = data['message'] ?: String.format("app:notify auto message")
        notification.notificationType = data['notificationType'] ?: 'app:notification'
        save(notification);
        
        return notification;
    }

}
