package metasoft.property.core

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.time.TimeCategory

class TenantService extends AccountService {

    AccountCheckService accountCheckService;
    def dataSource

    BigDecimal getAccountBalance(Tenant tenant) {
        Sql sql = new Sql(dataSource);
        String hql = """
            SELECT 
                sum(debit_reporting_amount-credit_reporting_amount) as balance 
            FROM 
                ms_transaction A 
            INNER JOIN
                ms_account c on A.account_number_cr=c.account_number
            INNER JOIN  
                account_category g on g.id = c.account_category_id
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE 
                g.code = 'tenant'
                AND S.account_number not in (4, 12, 13)
                AND S.suspense = 0
                AND A.account_number_cr = :accountNumber
                """;

        List<GroovyRowResult> results = sql.rows(hql, [accountNumber: tenant.accountNumber]);
        //def results = MainTransaction.executeQuery(hql, [accountNumber: tenant.accountNumber]);

        return (BigDecimal) (results[0][0] ?: 0.0);
    }
    BigDecimal getOpcostBalance(Tenant tenant) {
        Sql sql = new Sql(dataSource);
        String hql = """
            SELECT 
                sum(debit_reporting_amount-credit_reporting_amount) as balance 
            FROM 
                ms_transaction A 
            INNER JOIN
                ms_account c on A.account_number_cr=c.account_number
            INNER JOIN  
                account_category g on g.id = c.account_category_id
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE 
                g.code = 'tenant'
                AND S.account_number not in (2, 4, 12, 13)
                AND S.suspense = 0
                AND A.account_number_cr = :accountNumber
                """;

        List<GroovyRowResult> results = sql.rows(hql, [accountNumber: tenant.accountNumber]);
        //def results = MainTransaction.executeQuery(hql, [accountNumber: tenant.accountNumber]);

        return (BigDecimal) (results[0][0] ?: 0.0);
    }
    BigDecimal getRentalBalance(Tenant tenant) {
        Sql sql = new Sql(dataSource);
        String hql = """
            SELECT 
                sum(debit_reporting_amount-credit_reporting_amount) as balance 
            FROM 
                ms_transaction A 
            INNER JOIN
                ms_account c on A.account_number_cr=c.account_number
            INNER JOIN  
                account_category g on g.id = c.account_category_id
            INNER JOIN 
                sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE 
                g.code = 'tenant'
                AND S.account_number in (2)
                AND S.suspense = 0
                AND A.account_number_cr = :accountNumber
                """;

        List<GroovyRowResult> results = sql.rows(hql, [accountNumber: tenant.accountNumber]);
        //def results = MainTransaction.executeQuery(hql, [accountNumber: tenant.accountNumber]);

        return (BigDecimal) (results[0][0] ?: 0.0);
    }

    BigDecimal getAccountDepositBalance(Tenant tenant) {
        Sql sql = new Sql(dataSource);
        String hql = """
                SELECT 
                    sum(debit_reporting_amount-credit_reporting_amount) as balance 
                FROM 
                    ms_transaction A 
                INNER JOIN 
                    sub_account S ON A.sub_account_number_cr=S.account_number
                where S.account_number in (4, 12, 13)
                AND A.account_number_cr = :accountNumber
                AND s.suspense=0
                """;

        List<GroovyRowResult> results = sql.rows(hql, [accountNumber: tenant.accountNumber]);
        BigDecimal balance = (BigDecimal) (results[0][0] ?: 0.0)

        return balance;
    }

    BigDecimal getLeaseAccountBalance(Tenant tenant, Lease lease, Date limitDate) {
        String hql = """
            select SUM(debit_reporting_amount-credit_reporting_amount) as balance
            from MainTransaction t 
            inner join SubAccount s on s.accountNumber=t.subAccountNumberCr 
            where t.accountNumberCr = :accountNumber
                AND s.accountNumber not in (4, 12, 13)
                and s.suspense=0
                and t.lease.id = :leaseId
                and t.transactionDate < :limitDate
            """
        def results = MainTransaction.executeQuery(hql,
                [accountNumber: tenant.accountNumber, leaseId: lease.id, limitDate: limitDate]);

        return (BigDecimal) (results[0] ?: 0.0);
    }

    /**
     *
     * Update this to return the leaseId on the tenantId for flexcube's sake and progress
     *
     * @return
     */
    List<Lease> shareTenantLeases() {
        return Lease.where {
            checkStatus == CheckStatus.APPROVED
        }.list();
    }

    Long activeLeaseCount(Serializable id) {

        Date today = new Date().clearTime();

        return Lease.where {
            tenant.id == id
            deleted == false
            leaseStatus.code == 'active'
            leaseStartDate <= today
            leaseExpiryDate >= today
            checkStatus == CheckStatus.APPROVED
        }.count() ?: 0;
    }

    Tenant get(Serializable tenantId) {
        return Tenant.where {
            id == tenantId
        }.join('bankAccount')
         .join('accountType').get();
    }

    List<Tenant> listByProperty(Serializable propertyId) {
        String hql = "select t from Tenant t join t.leases l where l.property.id='${propertyId}' and l.terminated=${false}";
        return Tenant.executeQuery(hql);
    }

    List<Tenant> list(Map args) {

        return Tenant.where {
            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
        }.join('bankAccount')
         .join('accountType')
         .list(args)
    }

    List<Tenant> listApproved(Map args) {
        return Tenant.where {
            checkStatus == CheckStatus.APPROVED

            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                (accountNumber =~ "%${args.accountNumber}%" || bankAccountNumber =~ "%${args.accountNumber}%")
            }
        }.join('bankAccount')
         .join('accountType')
         .list(args)
    }

    Long countApproved(Map args = [:]) {
        return Tenant.where {
            checkStatus == CheckStatus.APPROVED
            if (args?.accountType) { accountType.id == args.accountType }
            if(args?.accountName) {accountName =~ "%${args.accountName}%"}
            if(args?.accountNumber) {(accountNumber =~ "%${args.accountNumber}%" || bankAccountNumber =~ "%${args.accountNumber}%")}
        }.count()
    }

    Long count(def args = [:]) {
        return Tenant.where { 
            1 == 1
        }.count();
    }

    AccountCategory getTenantCategory() {
        return AccountCategory.findByCode('tenant');
    }

    List<RentalUnit> getRentalUnitsByAccount(String accountNumber) {
        String hql = "select r from RentalUnit r join r.leases l where l.lesseeAccount='${accountNumber}'";
        return RentalUnit.executeQuery(hql);
    }

    String accountNumberGenerator() {
        TenantAccount tenantAccount = new TenantAccount()
        tenantAccount.save()
        return String.format("001%06d", tenantAccount.id);
    }

    List<RentalUnit> getRentalUnitsByTenantAccount(String accountNumber) {

        Date now = new Date();
        String hql = "select r from RentalUnit r where exists(select l from Lease l where l.checkStatus='${CheckStatus.APPROVED}' and l.rentalUnit.id= r.id and l.lesseeAccount = '${accountNumber}')" ;
        return RentalUnit.executeQuery(hql);
    }

    @Override
    Tenant save(Account account) {
        account.accountCategory = AccountCategory.findByCode('tenant');
        super.save(account);

        def urlParams = [id: account.id, action: 'show', controller: 'tenant'];
        notify("app:notify", [object: account, urlParams: urlParams, entity: account.class.getCanonicalName()]);

        return (Tenant)account;
    }

    Tenant create(Account account, Map args) {
        account.accountCategory = AccountCategory.findByCode('tenant');

        super.create(account, args);

        def urlParams = [id: account.id, action: 'show', controller: 'tenant'];
        notify("app:notify", [object: account, urlParams: urlParams, entity: account.class.getCanonicalName()]);
        return (Tenant)account;
    }

    Tenant create(Account account, Map args, Map params) {
        create(account, args)

        if(params['autoCommit'] == true) {
            AccountCheck accountCheck = AccountCheck.findByEntity(account);
            accountCheckService.approve(accountCheck, true);
        }

        return (Tenant)account;
    }

    @Override
    Tenant update(Account account, Map args) {
        super.update(account, args);
        return (Tenant)account;
    }

    List<Tenant> latestTenants() {
        Date today = new Date().clearTime();
        Date weekStart = new Date().clearTime();

        use(TimeCategory) {
            today = today + 1.days
            weekStart = weekStart - 7.days
        }

        return Tenant.where {
            dateCreated > weekStart
            dateCreated < today
        }.list();
    }

}