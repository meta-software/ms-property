package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.springframework.validation.BindException

class LeaseRenewalRequestCheckService {
    
    SpringSecurityService springSecurityService
    LeaseService leaseService;

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    @Transactional
    LeaseRenewalRequestCheck createCheckRecord(LeaseRenewalRequestTmp leaseRenewalRequestTmp) {
        LeaseRenewalRequestCheck leaseRenewalRequestCheck = new LeaseRenewalRequestCheck()

        leaseRenewalRequestCheck.transactionSource = leaseRenewalRequestTmp;
        leaseRenewalRequestCheck.actionName = "CREATE";

        leaseRenewalRequestCheck.maker = currentUser;
        leaseRenewalRequestCheck.makeDate = new Date();
        leaseRenewalRequestCheck.entityName = "LEASE_RENEWAL_REQUEST";
        leaseRenewalRequestCheck.actionName = leaseRenewalRequestTmp.userAction;
        leaseRenewalRequestCheck.entityUrl = "#";
        leaseRenewalRequestCheck.transactionSource = leaseRenewalRequestTmp;
        def jsonBuilder = new JsonBuilder([:])
        leaseRenewalRequestCheck.jsonData = jsonBuilder.toString()
        leaseRenewalRequestCheck.save(failOnError: true)   // save the transientValue

        return save(leaseRenewalRequestCheck);
    }

    Long count() {
        LeaseRenewalRequestCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    LeaseRenewalRequestCheck get(Serializable id) {
        LeaseRenewalRequestCheck.get(id);
    }

    LeaseRenewalRequest getEntity(LeaseRenewalRequestCheck leaseRenewalRequestCheck) {
        LeaseRenewalRequest leaseRenewalRequest = leaseRenewalRequestCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(leaseRenewalRequestCheck.jsonData);
        leaseRenewalRequest.setProperties(json);

        return leaseRenewalRequest;
    }

    List<LeaseRenewalRequestCheck> list(Map args) {
        LeaseRenewalRequestCheck.list();
    }

    List<LeaseRenewalRequestCheck> listInbox(Map args) {

        String query = args.get('q')

        LeaseRenewalRequestCheck.where {

            if(query) {
                ( transactionSource.lease.leaseNumber ==~ "%${query}%" )
            }

            checkStatus == CheckStatus.PENDING
        }.join('entity').list(args);
    }

    List<LeaseRenewalRequestCheck> listOutbox(Map args) {
        LeaseRenewalRequestCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    LeaseRenewalRequestCheck save(LeaseRenewalRequestCheck leaseRenewalRequestCheck) {
        leaseRenewalRequestCheck.save(failOnError: true);
    }

    @Transactional
    LeaseRenewalRequestCheck approve(LeaseRenewalRequestCheck leaseRenewalRequestCheck) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        if(leaseRenewalRequestCheck.isChecked()) {
            BindException errors = new BindException(leaseRenewalRequestCheck, "leaseRenewalRequestCheck")
            errors.rejectValue("checkStatus", "leaseRenewalRequestTmp.checked.already", "Lease Renewal Request is already checked.")
            def message = "Lease Renewal Request is already approved."
            throw new ValidationException(message, errors);
        }

        // the check entity
        leaseRenewalRequestCheck.checkStatus = CheckStatus.APPROVED;
        leaseRenewalRequestCheck.checker = currentUser;
        leaseRenewalRequestCheck.checkDate = new Date();
        leaseRenewalRequestCheck.checkComment = "Approved";
        leaseRenewalRequestCheck.save(failOnError: true);

        postLeaseRenewalRequest(leaseRenewalRequestCheck.transactionSource);

        return leaseRenewalRequestCheck;
    }

    @Transactional
    LeaseRenewalRequestCheck reject(LeaseRenewalRequestCheck leaseRenewalRequestCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(leaseRenewalRequestCheck.isChecked()) {
            return leaseRenewalRequestCheck;
        }

        // the check entity
        leaseRenewalRequestCheck.checkStatus = CheckStatus.REJECTED;
        leaseRenewalRequestCheck.checker = currentUser;
        leaseRenewalRequestCheck.checkDate = new Date();
        leaseRenewalRequestCheck.checkComment = params['comment'];

        return leaseRenewalRequestCheck.save(failOnError: true);
    }

    @Transactional
    LeaseRenewalRequestCheck cancel(LeaseRenewalRequestCheck leaseRenewalRequestCheck, Map params) {

        if(leaseRenewalRequestCheck.isChecked()) {
            BindException errors = new BindException(leaseRenewalRequestCheck, "leaseRenewalRequestCheck")
            errors.rejectValue("checkStatus", "leaseRenewalRequestTmp.checked.already", "Lease Renewal Request is already checked.")
            def message = "Lease Renewal Request is already checked."
            throw new ValidationException(message, errors);
        }

        // the check entity
        leaseRenewalRequestCheck.checkStatus = CheckStatus.CANCELLED;
        leaseRenewalRequestCheck.checker = currentUser;
        leaseRenewalRequestCheck.checkDate = new Date();
        leaseRenewalRequestCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        LeaseRenewalRequestTmp leaseRenewalRequestTmp = leaseRenewalRequestCheck.transactionSource;
        //leaseRenewalRequestTmp.posted = true;
        leaseRenewalRequestTmp.save();

        return leaseRenewalRequestCheck.save(failOnError: true);
    }


    @Transactional
    LeaseRenewalRequest postLeaseRenewalRequest(LeaseRenewalRequestTmp leaseRenewalRequestTmp) {

        // if the posting is new, create a new Property object
        LeaseRenewalRequest leaseRenewalRequest = retrieveLeaseRenewalRequest(leaseRenewalRequestTmp);
        leaseRenewalRequest.checkStatus = CheckStatus.APPROVED;
        leaseRenewalRequest.dirtyStatus = CheckStatus.APPROVED;
        leaseRenewalRequest.save(failOnError: true)

        //renew the lease
        leaseService.renewLease(leaseRenewalRequest.lease, leaseRenewalRequest);

        return leaseRenewalRequest;
    }

    LeaseRenewalRequest retrieveLeaseRenewalRequest(LeaseRenewalRequestTmp leaseRenewalRequestTmp) {
        LeaseRenewalRequest leaseRenewalRequest = leaseRenewalRequestTmp.entity ?: new LeaseRenewalRequest();
        leaseRenewalRequest.setProperties(leaseRenewalRequestTmp.properties);

        leaseRenewalRequest.lease = leaseRenewalRequestTmp.lease
        leaseRenewalRequest.leaseExpireDate = leaseRenewalRequestTmp.leaseExpireDate
        leaseRenewalRequest.actionReason = leaseRenewalRequestTmp.actionReason
        leaseRenewalRequest.leaseRenewalRequestTmp = leaseRenewalRequestTmp;

        print "#############\n"
        println(leaseRenewalRequest.properties);
        print "#############\n"

        return leaseRenewalRequest;
    }

}