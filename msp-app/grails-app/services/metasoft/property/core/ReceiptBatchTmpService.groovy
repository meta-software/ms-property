package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import org.springframework.validation.BindException

class ReceiptBatchTmpService {

    SpringSecurityService springSecurityService
    ReceiptService receiptService;
    ReceiptBatchService receiptBatchService

    ReceiptBatchTmp get(Serializable id) {
        return ReceiptBatchTmp.get(id);
    }

    List<ReceiptBatchTmp> list(Map args) {
        List<ReceiptBatchTmp> mainReceiptTmpList = ReceiptBatchTmp.list();
        return mainReceiptTmpList;
    }

    List<ReceiptBatchTmp> listPending(Map args){
        SecUser currentUser = springSecurityService.currentUser as SecUser;

        args['sort'] = [id: 'asc'];

        List<ReceiptBatchTmp> receiptBatchTmps = ReceiptBatchTmp.where{
            createdBy == currentUser.username
            postRequest == false
        }.list(args)

        return receiptBatchTmps;
    }

    @Transactional
    ReceiptBatchTmp create(ReceiptBatchTmp receiptBatchTmp) {
        SecUser secUser = (SecUser)springSecurityService.currentUser;

        receiptBatchTmp.transactionType = TransactionType.findByTransactionTypeCode("RC");
        receiptBatchTmp.checkStatus = CheckStatus.NEW;
        receiptBatchTmp.createdBy = secUser?.username ?: 'system';
        receiptBatchTmp.save(failOnError: true, flush: true);

        return receiptBatchTmp;
    }

    @Transactional
    ReceiptBatchTmp createNewBatch() {
        SecUser secUser = (SecUser)springSecurityService.currentUser;

        ReceiptBatchTmp receiptBatchTmp = new ReceiptBatchTmp();
        receiptBatchTmp.transactionType = TransactionType.findByTransactionTypeCode("RC");
        receiptBatchTmp.checkStatus = CheckStatus.NEW;
        receiptBatchTmp.createdBy = secUser?.username ?: 'system';
        return receiptBatchTmp.save(failOnError: true, flush: true);
    }

    @Transactional
    void delete(Long batchId) {
        ReceiptBatchTmp batchTmp = get(batchId);

        if(batchTmp) {
            batchTmp.delete(flush: true);
        }
    }
    
    //@Transactional
    def postReceiptBatchRequest(ReceiptBatchTmp receiptBatchTmp) {
        if(receiptBatchTmp.posted || receiptBatchTmp.postRequest) {
            //@todo: throw exception, we are not posting this twice
            return;
        }

        //Create a new ReceiptBatchCheck
        ReceiptBatchCheck receiptBatchCheck = new ReceiptBatchCheck();
        receiptBatchCheck.transactionType = receiptBatchTmp.transactionType;
        receiptBatchCheck.batchNumber = receiptBatchTmp.batchNumber;
        receiptBatchCheck.maker = springSecurityService.currentUser as SecUser;
        receiptBatchCheck.makeDate = new Date();
        receiptBatchCheck.entityName = "RECEIPT_BATCH";
        receiptBatchCheck.actionName = "CREATE";
        receiptBatchCheck.entityUrl = "#";
        receiptBatchCheck.entity;
        receiptBatchCheck.transactionSource = receiptBatchTmp;
        receiptBatchCheck.jsonData = "{}" //jsonBuilder.toString()
        receiptBatchCheck.save()   // save the transientValue

        //action the posting of the batch
        receiptBatchTmp.postRequest = true;
        receiptBatchTmp.datePostRequest = new Date();
        receiptBatchTmp.postRequestBy = ((SecUser)(springSecurityService.currentUser))?.username;
        //@todo: add the configuration to check if we are not maker checking
        receiptBatchTmp.autoCommitted = true;

        receiptBatchTmp.save();

        // only if the maker checker flag is turned off, then this does not work.
        if(false) {
            postReceiptBatchTmp(receiptBatchCheck.transactionSource);
        }

        return receiptBatchCheck;
    }

    //@Transactional
    ReceiptBatch postReceiptBatchTmp(ReceiptBatchTmp receiptBatchTmp) {
        
        // configure if transaction is already loaded
        if(receiptBatchTmp.posted) {
            BindException errors = new BindException(receiptBatchTmp, "receiptBatchTmp");
            errors.rejectValue("posted", "receiptBatchTmp.posted.already", "Receipt Batch with reference# ${receiptBatchTmp.batchNumber} is already posted.");
            def message = "Invalid: Receipt Batch reference=${receiptBatchTmp.batchNumber} is already posted.";
            throw new ValidationException(message, errors);
        }
        
        //Create a new ReceiptBatch
        ReceiptBatch receiptBatch = new ReceiptBatch();
        receiptBatch.transactionType = receiptBatchTmp.transactionType;
        receiptBatch.batchNumber = receiptBatchTmp.batchNumber;
        receiptBatch.maker = springSecurityService.currentUser as SecUser;
        receiptBatch.makeDate = new Date();
        receiptBatch.checker = springSecurityService.currentUser as SecUser;
        receiptBatch.dirtMaker = springSecurityService.currentUser as SecUser;
        receiptBatch.checkDate = new Date();
        receiptBatch.checkStatus = CheckStatus.APPROVED;
        receiptBatch.dirtyStatus = CheckStatus.APPROVED;
        receiptBatch.save();

        //Now we add the transaction items to the receiptBatch, based on the transactionCategory it belongs to.
        for(ReceiptTmp receiptTmp : receiptBatchTmp.receiptTmps) {
            Receipt receipt = receiptService.fromReceiptTmp(receiptTmp);
            receipt.receiptBatch = receiptBatch;
            receipt.save();
            receiptBatch.addToReceipts(receipt);
        } // end for
        //receiptBatch.save(flush: true); // save the receiptBatch

        //update the temporary transaction header to posted.
        receiptBatchTmp.posted = true;

        try {
            receiptBatchTmp.save(failOnError: true, flush: true);
        } catch (ValidationException e) {
            log.error(e.message, e)
            throw e
        }

        receiptBatchService.postReceiptBatch(receiptBatch);
        return receiptBatch.refresh();
    }
}
