package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional
import metasoft.property.core.Bank

@GrailsCompileStatic
class BankService {

    List<Bank> list(Map args) {
        String name = args.get('bankName')
        String code = args.get('bankCode')

        return Bank.where{
            if (name) {bankName ==~ "%${name}%"}
            if (code) {bankName ==~ "%${code}%"}
        }.list(args);
    }

    List<Bank> listOptions(Map args) {
        return Bank.list(args);
    }

    Bank get(Serializable id) {
        return Bank.get(id);
    }

    @Transactional
    Bank save(Bank bank) {
        bank.save(failOnError: true);
    }

    @Transactional
    Bank update(Bank bank) {
        bank.save(failOnError: true);
    }
}
