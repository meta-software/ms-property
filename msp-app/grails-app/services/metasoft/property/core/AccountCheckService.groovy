package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import groovy.json.JsonSlurper

class AccountCheckService {

    UserService userService;
    SpringSecurityService springSecurityService
    AccountService accountService;

    private SecUser getCurrentUser() {
        return springSecurityService.currentUser as SecUser;
    }

    Long count() {
        AccountCheck.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    AccountCheck get(Serializable id) {
        AccountCheck.get(id);
    }

    Account getEntity(AccountCheck accountCheck) {
        Account account = accountCheck.entity;

        // update the entity with the json data
        def json = new JsonSlurper().parseText(accountCheck.jsonData);
        account.setProperties(json);

        return account;
    }

    List<AccountCheck> list(Map args) {
        AccountCheck.list();
    }

    List<AccountCheck> listInbox(Map args) {

        String categoryArg = args?.cat

        AccountCheck.where {
            checkStatus == CheckStatus.PENDING

            if(categoryArg) {
                accountCategory.code == categoryArg
            }

        }.join('entity').list(args);
    }

    List<AccountCheck> listOutbox(Map args) {
        AccountCheck.where{
            checkStatus == CheckStatus.PENDING
            maker == currentUser
        }.join('entity').list();
    }

    @Transactional
    AccountCheck save(AccountCheck accountCheck) {
        accountCheck.save(failOnError: true);
    }

    @Transactional
    AccountCheck approve(AccountCheck accountCheck, Boolean autoCommited = false) {

        SecUser checker = autoCommited ? userService.getSystemUser() : springSecurityService.currentUser as SecUser;

        if(accountCheck.isChecked()) {
            // already checked.
            //return accountCheck;
        }

        // the check entity
        accountCheck.checkStatus = CheckStatus.APPROVED;
        accountCheck.checker = checker;
        accountCheck.checkDate = new Date();
        accountCheck.checkComment = "Approved";
        accountCheck.save(failOnError: true, flush: true);

        // retrive the jsonData
        Map args = new JsonSlurper().parseText(accountCheck.jsonData)

        // Account entity object.
        Account account = accountCheck.entity;
        account.setProperties(args);
        account.checkStatus = CheckStatus.APPROVED;
        account.dirtyStatus = CheckStatus.APPROVED;
        account.checkDate = new Date();
        account.checker = checker;
        account.save(flush: true); // save the account

        return accountCheck;
    }

    @Transactional
    AccountCheck reject(AccountCheck accountCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(accountCheck.isChecked()) {
            // already checked.
            //return accountCheck;
        }

        // the check entity
        accountCheck.checkStatus = CheckStatus.REJECTED;
        accountCheck.checker = currentUser;
        accountCheck.checkDate = new Date();
        accountCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(accountCheck.actionName == 'CREATE') {
            Account account = accountCheck.entity;
            account.checkStatus = CheckStatus.REJECTED;
            account.save();
        } else {
            Account account = accountCheck.entity;
            account.dirtyStatus = CheckStatus.APPROVED; //@todo: need to be fixed this one.
            account.save();
        }

        accountCheck.save(failOnError: true);
        return accountCheck;
    }

    @Transactional
    AccountCheck cancel(AccountCheck accountCheck, Map params) {

        SecUser currentUser = springSecurityService.currentUser as SecUser;
        if(accountCheck.isChecked()) {
            // already checked.
            //return accountCheck;
        }

        // the check entity
        accountCheck.checkStatus = CheckStatus.CANCELLED;
        //accountCheck.checker = currentUser;
        accountCheck.checkDate = new Date();
        accountCheck.checkComment = params['comment'];

        // only if this is a CREATE action do we care about also rejecting the entity.
        if(accountCheck.actionName == 'CREATE') {
            Account account = accountCheck.entity;
            account.checkStatus == CheckStatus.CANCELLED;
            account.save();
        } else {
            Account account = accountCheck.entity;
            account.checkStatus == CheckStatus.APPROVED;
            account.save();
        }
        accountCheck.save(failOnError: true);
        return accountCheck;
    }

}