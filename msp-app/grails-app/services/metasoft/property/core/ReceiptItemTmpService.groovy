package metasoft.property.core

class ReceiptItemTmpService {

    ReceiptItemTmp get(Serializable id) {
        return ReceiptItemTmp.get(id);
    }

    List<ReceiptTmp> listByReceipt(Long receiptId, Map args) {
        return ReceiptItemTmp.where {
            receiptTmp.id == receiptId
        }.list(args);
    }

}
