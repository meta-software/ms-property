package metasoft.property.core

import grails.events.EventPublisher
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.databinding.DataBinder
import grails.web.mapping.LinkGenerator
import groovy.json.JsonBuilder
import groovy.util.logging.Slf4j
import org.hibernate.SQLQuery
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired

@ReadOnly
@Slf4j
class LeaseService implements EventPublisher, DataBinder {

    @Autowired
    SessionFactory sessionFactory
    @Autowired
    LinkGenerator grailsLinkGenerator;
    SpringSecurityService springSecurityService;
    LeaseCheckService leaseCheckService
    CurrencyService currencyService
    SettingsService settingsService

    List<Lease> getAllByTenantAccount(String accountNumber) {
        return Lease.where {
            checkStatus == CheckStatus.APPROVED
            lesseeAccount == accountNumber
        }.list();
    }

    Map getReviewsDashboard() {
        Map<String, Object> results = [:]

        // retrieve count of leases requiring to be reviews based on the nextReviewDate
        results['leaseReviewCount'] = (Long)Lease.leaseReviews.count();
        return results;
    }

    List<Lease> getReviews(Map args) {
        return Lease.leaseReviews.list(args);
    }

    Lease get(Serializable leaseId) {
        Lease.where{
            id == leaseId
        }.join('tenant')
        .join('property')
        .join('rentalUnit')
        .get();
    }

    Map getLeaseStats(Serializable leaseId) {

        String queryString = """
            SELECT 
                sum(case when s.account_number not in (4, 12, 13) then debit_reporting_amount-credit_reporting_amount else 0 end) as rental_balance,
                sum(case when s.account_number in (4, 12, 13) then debit_reporting_amount-credit_reporting_amount else 0 end) as deposit_balance,
                sum(debit_reporting_amount-credit_reporting_amount) total_balance
            FROM  ms_transaction A 
            INNER JOIN ms_account c on A.account_number_cr=c.account_number
            INNER JOIN sub_account S ON A.sub_account_number_cr=S.account_number
            WHERE c.data_type = 'tenant'
                AND S.suspense = 0
                AND A.lease_id=${leaseId}
"""

        def sqlQuery = sessionFactory.currentSession.createSQLQuery(queryString)
        def results = sqlQuery.list()[0]

        return [
                rentBalance: results[0] as BigDecimal,
                //opcosBalance: results[1] as BigDecimal,
                totalBalance: results[2] as BigDecimal,
                depositBalance: results[1] as BigDecimal,
        ]
    }

    List<Lease> listByProperty(Serializable propertyId) {
        String hql = "select l from Lease l join l.tenant join l.rentalUnit join l.property p where p.id='${propertyId}' and l.leaseStatus.code not in ('terminated', 'new') order by l.id";
        def results = Lease.executeQuery(hql);
        return results;
    }

    List<Lease> listByPropertyAndBillingCycle(Serializable propertyId, BillingCycle cycle) {
        String hql = "select l from Lease l join l.property p where p.id=:propertyId and l.leaseStartDate <= :endDate and l.leaseExpiryDate >= :startDate and l.checkStatus = '${CheckStatus.APPROVED}'";
        def results = Lease.executeQuery(hql, [propertyId: propertyId, startDate: cycle.startDate, endDate: cycle.endDate]);
        return results;
    }

    List<Lease> leaseHistoryByRentalUnit(Serializable rentalUnitId) {
        def results = Lease.where {
            rentalUnit.id == rentalUnitId
        }.join("leaseStatus")
         .list([sort: 'leaseNumber', order: 'desc'])

        return results;
    }

    List<Lease> list(Map args) {

        def tenantArg = args['tenant']

        return Lease.where {
            checkStatus == CheckStatus.APPROVED

            if(args?.leaseNumber) {
                leaseNumber =~ "%${args.leaseNumber}%"
            }
            if(tenantArg) {
                (tenant.accountNumber =~ "%${tenantArg}%" || tenant.accountName ==~ "%${tenantArg}%")
            }
            if(args?.tenantId) {
                tenant.id == args.tenantId
            }
            if(args?.tenantAccount) {
                tenant.accountNumber == args.tenantAccount
            }
            if(args?.leaseStatus) {
                leaseStatus.code == args.leaseStatus
            }
            if(args?.currency) {
                leaseCurrency.id == args.currency
            }
        }.list(args)
    }

    Long count() {
        return Lease.count();
    }

    Long count(Map args) {

        def tenantArg = args['tenant']
        return Lease.where {
            checkStatus == CheckStatus.APPROVED

            if(args?.leaseNumber){
                leaseNumber =~ "%${args.leaseNumber}%"
            }

            if(tenantArg) {
                (tenant.accountNumber =~ "%${args.tenant}%" || tenant.accountName ==~ "%${tenantArg}%")
            }
            if(args?.tenantId) {
                tenant.id == args.tenantId
            }
            if(args?.tenantAccount) {
                tenant.accountNumber == "${args.tenantAccount}"
            }
            if(args?.leaseStatus) {
                leaseStatus.code == "${args.leaseStatus}"
            }
        }.count()

    }

    @Transactional
    Lease delete(Serializable id) {
        Lease lease = Lease.get(id);

        if(!lease?.deleted) {
            lease.deleted = true
            lease.dateDeleted = new Date();
            lease.save(failOnError: true)
        }

        return lease;
    }

    @Transactional
    Lease save(Lease lease) {
        //@todo: Add side effects here

        // retrieve the leaseNumber for the new Lease and prepare the lease
        LeaseNumber leaseNumber = new LeaseNumber().save(failOnError: true);
        lease.leaseNumber  = this.getLeaseNumber(leaseNumber);
        lease.save(failOnError: true);

        def urlParams = [id: lease.id, action: 'show', controller: 'lease'];
        notify("app:notify", [object: lease, urlParams: urlParams, entity: lease.class.getCanonicalName()]);
        return lease;
    }

    @Transactional
    Lease terminateLease(Lease lease) {
        // terminate the lease only if the lease is active, otherwise just ignore and pretend all is well.

        if(!lease.terminated) {
            LeaseStatus leaseStatus = LeaseStatus.findByCode('terminated');

            lease.leaseStatus = leaseStatus;
            lease.terminated = true;
            lease.dateTerminated = new Date();
            lease.save(failOnError: true);

            log.debug("lease leaseNumber={} was terminated successfully", lease.leaseNumber);
        }

        return lease;
    }

    @Transactional
    Lease renewLease(Lease lease, LeaseRenewalRequest leaseRenewalRequest) {
        //configure that the lease qualifies.

        LeaseStatus leaseStatus = LeaseStatus.findByCode('active');
        lease.leaseStatus = leaseStatus;
        lease.leaseExpiryDate = leaseRenewalRequest.leaseExpireDate;
        lease.save(failOnError: true)

        return lease;
    }

    String getLeaseNumber(LeaseNumber leaseNumber) {
        return String.format("110%06d", leaseNumber.id);
    }

    String generateLeaseNumber() {
        LeaseNumber leaseNumber = new LeaseNumber().save(failOnError: true, flush: true);
        return getLeaseNumber(leaseNumber);
    }

    List<Lease> activeLeases(BillingCycle billingCycle) {

        def query = Lease.where{
            deleted == false
            lease.leaseStatus.code == 'active'
            leaseStartDate <= billingCycle.startDate
            leaseExpiryDate >= billingCycle.endDate
        }

        return query.list();
    }

    def expiringLeases(Map args) {
        final session = sessionFactory.currentSession;

        String sqlQuery = """
select 
  sum(case when datediff(lease_expiry_date, current_date()) <= 30 then 1 else 0 end ) as le_30,
  sum(case when datediff(lease_expiry_date, current_date()) between 31  and 90  then 1 else 0 end ) as le_31_90,
  sum(case when datediff(lease_expiry_date, current_date()) between 91  and 120 then 1 else 0 end ) as le_91_180,
  sum(case when datediff(lease_expiry_date, current_date()) between 121 and 180 then 1 else 0 end ) as le_121_180
from ms_lease a 
inner join lease_status ls on ls.id=a.lease_status_id
  where expired = 0
  and ls.code in ('active')
""";

        SQLQuery leaseList = session.createSQLQuery(sqlQuery);
        def queryResults = leaseList.list();

        Map returnResults = [
                30: queryResults[0][0] ?: 0.0,
                90: queryResults[0][1] ?: 0.0,
                120: queryResults[0][2] ?: 0.0,
                180: queryResults[0][3] ?: 0.0,
        ]

        return returnResults;
    }

    Boolean isConflict(Lease lease) {

        def checkCount = Lease.where{
            rentalUnit == lease?.rentalUnit
            ( (leaseStartDate >= lease.leaseStartDate && leaseStartDate <= lease.leaseExpiryDate) ||
            (leaseExpiryDate <= lease.leaseExpiryDate && leaseExpiryDate <= lease.leaseExpiryDate) )
        }.count();

        return checkCount > 0;

    }

    @Transactional
    def updateMakerChecker(Lease lease) {
        lease.save(failOnError: true);
        return lease;
    }

    @Transactional
    void processExpiredLeases() {
        log.info("START processing the expired leases")
        Date today = new Date().clearTime() + 1 ;
        LeaseStatus expiredStatus = LeaseStatus.findByCode('expired');
        Map args = [sort: 'leaseExpiryDate', order: 'asc']
        List<Lease> leaseList = Lease.where {
            //deleted == false, even if deleted, let us also expire the lease to be on the safe side.
            // we are not expiring terminated leases
            //expired == false
            checkStatus == CheckStatus.APPROVED
            leaseExpiryDate < today
            !(leaseStatus.code in ['new', 'terminated'])
        }.list(args)

        log.trace("count of leases to expire = {}", leaseList.size())

        leaseList.each { Lease lease ->
            log.info("expiring the lease [leaseNumber={}]", lease.leaseNumber);
            lease.expired = true;
            lease.expiredDate = new Date();
            lease.leaseStatus = expiredStatus;
            lease.save(failOnError: true);
        }
    }

    Lease fromTemplate(Map args) {
        Lease lease = new Lease(args)

        //set the reporting currency
        Currency invoicingCurrency = currencyService.getReportingCurrency()
        Currency leaseCurrency = currencyService.get(settingsService.getValue("haka.app.financials.leaseCurrency"));

        //lease setup
        lease.leaseCurrency = leaseCurrency
        lease.invoicingCurrency = invoicingCurrency

        return lease;
    }

    @Transactional
    Lease create(Lease lease, Map args, String json) {
        // clear these, they are handled in the approve method
        lease.leaseItems.clear();

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        // prepare the maker-checker attributes
        lease.maker = currentUser ;
        lease.dirtMaker = currentUser ;
        lease.makeDate = new Date() ;
        lease.leaseStatus = LeaseStatus.findByCode('new');

//        lease.invoicingCurrency = Currency.get((String)args['invoicingCurrency']['id'])
//        lease.leaseCurrency = Currency.get((String)args['leaseCurrency']['id'])

        println("doing the invoiceCurrency: ${lease.invoicingCurrency}")
        println("doing the leaseCurrency: ${lease.leaseCurrency}")

        //lease.save(failOnError: true, flush: true);// save the entity
        saveCheckData(lease, json, "CREATE");

        def urlParams = [id: lease.id, action: 'show', controller: 'lease'];
        notify("app:notify", [object: lease, urlParams: urlParams, entity: lease.class.getCanonicalName()]);
        return lease;
    }

    LeaseCheck saveCheckData(Lease lease, String json, String action) {

        // create the url
        String url = grailsLinkGenerator.link(controller: 'lease', action: 'show', id: lease.id, absolute: false);

        // generate the maker-checker
        LeaseCheck leaseCheck = new LeaseCheck()
        leaseCheck.entity = lease;
        leaseCheck.entityName = "LEASE";
        leaseCheck.actionName = action;
        leaseCheck.maker = springSecurityService.currentUser as SecUser;
        leaseCheck.entityUrl = url;
        leaseCheck.jsonData = json;
        leaseCheck.makeDate = new Date();

        try {
            return leaseCheck.save(failOnError: true, flush: true)
        } catch (ValidationException e) {
            log.error("failed to create the lease check object. ", e.getMessage())
            e.printStackTrace()
            throw e
        }

    }

    @Transactional
    Lease update(Lease lease, Map args) {
        // prepare the maker-checker attributes
        saveCheckData(lease, args, "UPDATE");

        def urlParams = [id: lease.id, action: 'show', controller: 'lease'];
        notify("app:notify", [object: lease, urlParams: urlParams, entity: lease.class.getCanonicalName()]);
        return lease;
    }

    @Transactional
    Lease updateReview(Lease lease, Map args) {
        // prepare the maker-checker attributes
        saveCheckData(lease, args, "REVIEW");

        def urlParams = [id: lease.id, action: 'show', controller: 'lease'];
        notify("app:notify", [object: lease, urlParams: urlParams, entity: lease.class.getCanonicalName()]);
        return lease;
    }

    LeaseCheck saveCheckData(Lease lease, Map args, String action) {

        // create the url
        String url = grailsLinkGenerator.link(controller: 'lease', action: 'show', id: lease.id, absolute: false);

        // generate the maker-checker
        LeaseCheck leaseCheck = new LeaseCheck()
        leaseCheck.entity = lease;
        leaseCheck.entityName = "LEASE";
        leaseCheck.actionName = action;
        leaseCheck.maker = springSecurityService.currentUser as SecUser;
        leaseCheck.entityUrl = url;
        leaseCheck.jsonData = new JsonBuilder(args).toString();
        leaseCheck.makeDate = new Date();

        // discard lease changes before saving
        lease.dirtMaker = leaseCheck.maker;
        lease.dirtyStatus = CheckStatus.EDITING;
        lease.save();

        try {
            leaseCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the Lease check entity, {}", e.message)
            throw e
        }

        return leaseCheck;
    }

    String getUpdateMap(Lease lease, Map args) {

        lease.setProperties(args);    //@todo: update to user bindData from the web*** class

        List<String> dirtyLeaseNames = lease.getDirtyPropertyNames();
        Map dirtyProperties = [:]

        dirtyLeaseNames.each { dirtyLease ->
            dirtyProperties[dirtyLease] = args[dirtyLease];
        }

        //
        dirtyProperties['leaseItems'] = args['leaseItems'];

        lease.discard();
        lease.refresh();

        return new JsonBuilder(dirtyProperties).toString();
    }

    //
    Lease getForEdit(Long id) {
        Lease lease = Lease.read(id);

        LeaseCheck leaseCheck = getLeaseCheck(lease);

        if(leaseCheck) {
            return leaseCheckService.getEntity(leaseCheck);
        }

        return lease;
    }

    LeaseCheck getLeaseCheck(Lease lease) {
        List<LeaseCheck> results = (List<LeaseCheck>)LeaseCheck.executeQuery("select a from LeaseCheck a where entity.id=:entityId and checkStatus in (:statuses)",
                [entityId: lease.id, statuses: [CheckStatus.PENDING, CheckStatus.NEW]], [max: 1]);

        if(results) {
            return results[0];
        }
        return null;
    }

}