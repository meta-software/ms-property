package metasoft.property.core

import groovy.util.logging.Slf4j

@Slf4j
class TenantInvoiceEmailService {
    
    BillingInvoiceService billingInvoiceService;
    BillingCycleService billingCycleService;

    void sendTenantBillingInvoices() {
        BillingCycle currentCycle = billingCycleService.getCurrentCycle();
        List<BillingInvoice> billingInvoices = billingInvoiceService.getUnEmailedInvoices(currentCycle);

        billingInvoices.each { BillingInvoice billingInvoice ->
            try{

                if(billingInvoice.tenant.emailAddress && !billingInvoice.tenant.emailAddress.isEmpty()) {
                    billingInvoiceService.emailBillingInvoice(billingInvoice);
                } else {
                    log.warn("tenant [{}] has not set email-address, invoice not sent by email", billingInvoice.tenant.accountName);
                }

            } catch(Exception e) {
                log.error("error while sending bulk emails, ${e.message}");
                e.printStackTrace();
                throw e;
            }
        }

    }
}