package metasoft.property.core

import grails.core.GrailsApplication
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j
import metasoft.property.ConfigSettingCommand
import metasoft.property.core.exceptions.RecordNotFoundException
import org.springframework.beans.factory.annotation.Autowired

//@GrailsCompileStatic
@Slf4j
@ReadOnly
class ConfigSettingService {

    @Autowired
    GrailsApplication grailsApplication;

    List<ConfigSetting> list(Map args) {
        def configTypeId = args.get('configType')
        args = [sort: 'configType.id', order: 'asc']


        return ConfigSetting.where{
            if(configTypeId) {configType.id == configTypeId}
        }.list(args);
    }

    ConfigSetting get(Serializable id) {
        return ConfigSetting.get(id);
    }

    @Transactional
    ConfigSetting activate(Serializable id) {
        ConfigSetting configSetting = ConfigSetting.get(id)

        if(configSetting == null) {
            throw new RecordNotFoundException()
        }

        if(configSetting) {
            //@todo: activate the config setting while also disabling everything else
            configSetting.currentActive = true;
            configSetting.save();

            ConfigSetting.where {
                id != configSetting.id
                configType == configSetting.configType
                currentActive == true
            }.updateAll([currentActive: false, endDate: new Date() ])
        }

        return configSetting;
    }

    @Transactional
    ConfigSetting save(ConfigSettingCommand configSettingCommand) {
        //@todo: verify that there is no conflict with other existing configurations
        ConfigSetting configSetting = configSettingCommand as ConfigSetting;
        return configSetting.save(failOnError: true);
    }

    String getValue(String configName, Date dateParam) {

        log.debug("retrieve the configSetting value for configName={}, dateParam={}", configName, dateParam)

        ConfigSetting configSetting = ConfigSetting.where {
            configType.code == configName
            ( (startDate <= dateParam && endDate >= dateParam) || ((startDate <= dateParam && endDate == null) && currentActive == true) )
            enabled == true
        }.get();

        if(configSetting == null) {
            throw new RecordNotFoundException()
        }

        return configSetting.value;
    }

    String getValue(String configName) {
        return getValue(configName, new Date().clearTime());
    }

}
