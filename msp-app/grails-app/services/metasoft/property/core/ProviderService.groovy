package metasoft.property.core

import grails.core.GrailsApplication
import grails.gorm.transactions.Transactional

class ProviderService extends AccountService {

    GrailsApplication grailsApplication;
    
    Provider get(Serializable id) {
        return Provider.get(id);
    }

    Map getAccountStats(Serializable provideId) {

        def invoices = 0.0;
        def receipts = 0.0;
        def balance = 0.0;

        Map returnResults = [
                receipts: receipts,
                invoices: invoices,
                balance: balance
        ]
        return returnResults;
    }

    Map getCurrentAccountStats(Serializable providerId) {

        def invoices = 0.0;
        def receipts = 0.0;
        def balance = 0.0;

        Map returnResults = [
                receipts: receipts,
                invoices: invoices,
                balance: balance
        ]
        return returnResults;
    }

    /**
     * @todo add comments here.
     *
     * @param args
     * @return
     */
    List<Provider> fetchList(args) {

        def q = args['q'];

        def query = Provider.where{
            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.checkStatus && args?.checkStatus != 'all') {
                checkStatus == args.checkStatus
            }

            if(q) {
                ( accountNumber =~ "${q}%" || accountName ==~ "%${q}%")
            }
        }

        return query.list(args)
    }

    List<Provider> list(Map args) {

        def query = Provider.where{
            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
            if(args?.checkStatus && args?.checkStatus != 'all') {
                checkStatus == args.checkStatus
            }
        }

        return query.list(args)
    }

    List<Provider> listApproved(Map args) {
        def query = Provider.where {
            checkStatus == CheckStatus.APPROVED

            if (args?.accountType) {
                accountType.id == args.accountType
            }
            if(args?.accountName) {
                accountName =~ "%${args.accountName}%"
            }
            if(args?.accountNumber) {
                accountNumber =~ "%${args.accountNumber}%"
            }
        }

        return query.list(args)
    }
    
    Long count() {
        def query = Provider.where { 1 == 1}

        return query.count();
    }

    Long count(params) {
        def query = Provider.where { 1 == 1}

        return query.count(params);
    }

    @Transactional
    String accountNumberGenerator() {
        ProviderAccount providerAccount = new ProviderAccount()
        providerAccount.save(failOnError:true)

        String accountNumber = String.format("003%06d", providerAccount.id);

        return accountNumber;
    }

    @Override
    Provider create(Account account, Map args) {
        account.accountCategory = AccountCategory.findByCode('trust');

        super.create(account, args);

        def urlParams = [id: account.id, action: 'show', controller: 'provider'];
        notify("app:notify", [object: account, urlParams: urlParams, entity: account.class.getCanonicalName()]);

        return (Provider)account;
    }

    @Override
    Provider update(Account account, Map args) {
        super.update(account, args);
        return (Provider)account;
    }

}