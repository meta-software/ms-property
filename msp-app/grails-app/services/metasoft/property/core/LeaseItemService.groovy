package metasoft.property.core

import grails.gorm.transactions.Transactional

class LeaseItemService {

    MakerCheckerService makerCheckerService;

    LeaseItem get(Serializable id) {
        return LeaseItem.get(id);
    }

    List<LeaseItem> list(Map args) {

        def leaseId = args['leaseId'];

        def query = LeaseItem.where {
            if(leaseId) { lease.id == leaseId}
        }

        return query.list(args);
    }

    Long count() {
        return LeaseItem.count();
    }

    void delete(Serializable id) {
        //
    }

    @Transactional
    LeaseItem save(LeaseItem leaseItem) {
        leaseItem.save(failOnError: true)

        return leaseItem;
    }
}