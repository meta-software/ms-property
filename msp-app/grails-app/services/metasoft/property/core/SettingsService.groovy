package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.core.GrailsApplication
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
@ReadOnly
@GrailsCompileStatic
class SettingsService {

    @Autowired
    GrailsApplication grailsApplication;

    List<Settings> list(Map args) {
        String query = args.get('query')

        return Settings.createCriteria().list( {
            if(query) {
                or {
                    like("name", "%$query%")
                    like("description", "%$query%")
                    like("category", "%$query%")
                }
            }
            order('category')
        }) as List<Settings>
    }

    Settings get(Serializable id) {
        return Settings.get(id);
    }

    @Transactional
    Settings update(Settings settings) {
        settings.save(failOnError: true);
    }

    @Transactional
    List<Settings> updateSettings(Map args){
        List<Settings> settingsList = (List<Settings>)args;

        for(Settings settings : settingsList) {
            println("doing it for ${settings}")
            //update(settings);
        }

        return list([:]);
    }

    String getValue(String paramName) {
         String configValue = grailsApplication.config.get(paramName);

         if(!configValue) {
             Settings setting = Settings.findByName(paramName)
             configValue = setting?.value;
         }

        return configValue;
    }

    void reloadSettings() {
        List<Settings> settingsList = Settings.list();

        for(Settings settings: settingsList) {
            String configValue = settings.value;
            String configName = settings.value;

            switch(settings.name) {
                case "haka.app.timeZone":
                    // set the default time zone
                    TimeZone.setDefault(TimeZone.getTimeZone(configValue))
                    log.info("Set the application default timeZone to '{}'", configValue)
                    break;
                default:
                    log.info("Set the application configuration [{}] to [{}]", configName, configValue)
                    grailsApplication.config.put(configName, configValue);
                    break;
            }
        }
    }

}
