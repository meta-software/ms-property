package metasoft.property.core

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.mapping.LinkGenerator
import groovy.json.JsonBuilder
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired

@ReadOnly
@Slf4j
class BulkAdjustService {

    @Autowired
    LinkGenerator grailsLinkGenerator
    SpringSecurityService springSecurityService
    CurrencyService currencyService
    ExchangeRateService exchangeRateService
    BulkAdjustCheckService bulkAdjustCheckService

    @Transactional
    BulkAdjust approve(BulkAdjust bulkAdjust) {

        BulkAdjustCheck bulkAdjustCheck = BulkAdjustCheck.where {
            entity == bulkAdjust
        }.get();

        if(bulkAdjustCheck != null) {
            bulkAdjustCheckService.approve(bulkAdjustCheck);
        }

        return bulkAdjust;
    }

    @Transactional
    BulkAdjust reject(BulkAdjust bulkAdjust, Map params) {
        BulkAdjustCheck bulkAdjustCheck = BulkAdjustCheck.where {
            entity == bulkAdjust
        }.get();

        if(bulkAdjustCheck != null) {
            bulkAdjustCheckService.reject(bulkAdjustCheck, params);
        }

        return bulkAdjust;
    }    
    
    BulkAdjust get(Serializable id) {
        return BulkAdjust.get(id)
    }

    List<BulkAdjust> inbox(Map args) {
        return BulkAdjust.where{
            checkStatus == CheckStatus.PENDING
        }.list(args);
    }

    List<BulkAdjust> list(Map args) {
        return BulkAdjust.where{
            checkStatus == CheckStatus.PENDING
        }.list(args);
    }

    List<BulkAdjust> cancelled(Map args) {
        return BulkAdjust.where{
            checkStatus == CheckStatus.CANCELLED
        }.list(args);
    }

    List<BulkAdjust> approved(Map args) {
        return BulkAdjust.where{
            checkStatus == CheckStatus.APPROVED
        }.list(args);
    }

    Long countApproved() {
        return BulkAdjust.where{
            checkStatus == CheckStatus.APPROVED
        }.count();
    }

    List<BulkAdjust> outbox(Map args) {
        Long currentUserId = (Long) springSecurityService.currentUserId;;

        return BulkAdjust.where{
            checkStatus == CheckStatus.PENDING
            maker.id == currentUserId
        }.list(args);
    }

    Long count() {
        return BulkAdjust.count();
    }

    Long countInbox() {
        return BulkAdjust.where{
            checkStatus == CheckStatus.PENDING
        }.count();
    }

    Long countOutbox() {
        Long currentUserId = (Long) springSecurityService.currentUserId;;

        return BulkAdjust.where{
            checkStatus == CheckStatus.PENDING
            maker.id == currentUserId
        }.count();
    }

    Long countCancelled() {
        Long currentUserId = (Long) springSecurityService.currentUserId;;

        return BulkAdjust.where{
            checkStatus == CheckStatus.CANCELLED
            maker.id == currentUserId
        }.count();
    }

    @Transactional
    BulkAdjust save(BulkAdjust bulkAdjust) {
        return bulkAdjust.save(failOnError: true)
    }

    @Transactional
    BulkAdjust create(BulkAdjust bulkAdjust, Map args) {

        Currency reportingCurrency = currencyService.reportingCurrency;

        // prepare the maker-checker attributes
        bulkAdjust.maker = springSecurityService.currentUser as SecUser;
        bulkAdjust.makeDate = new Date();

        bulkAdjust = (BulkAdjust) exchangeRateService.transactionCurrencies(bulkAdjust, reportingCurrency, reportingCurrency)

        bulkAdjust.save(failOnError: true)

        // create the url
        String url = grailsLinkGenerator.link(controller: 'bulkAdjust', action: 'show', id: bulkAdjust.id, absolute: false);

        // generate the maker-checker
        BulkAdjustCheck bulkAdjustCheck = new BulkAdjustCheck()
        bulkAdjustCheck.subAccount = bulkAdjust.subAccount;
        bulkAdjustCheck.adjustmentType = bulkAdjust.adjustmentType;
        bulkAdjustCheck.entity = bulkAdjust;
        bulkAdjustCheck.entityName = "BulkAdjust";
        bulkAdjustCheck.actionName = "CREATE";
        bulkAdjustCheck.maker = springSecurityService.currentUser as SecUser;
        bulkAdjustCheck.entityUrl = url;
        bulkAdjustCheck.jsonData = new JsonBuilder(args).toString();
        bulkAdjustCheck.makeDate = new Date();

        try {
            bulkAdjustCheck.save(failOnError: true)
        } catch (ValidationException e) {
            log.error("failed to create the check method")
            throw e
        }

        return bulkAdjust;
    }

    @Transactional
    void processAdjustments() {
        Date today = new Date().clearTime();

        // take list of all currently pending requests
        List<BulkAdjust> bulkAdjusts = BulkAdjust.where {
            processed == false
            cancelled == false
            actionDate == today
            checkStatus == CheckStatus.APPROVED
        }.list();

        for(BulkAdjust bulkAdjust : bulkAdjusts) {
            log.info("processing the bulkAdjust.id = #${bulkAdjust.id}");
            processAdjustment(bulkAdjust)
        }

    }

    @Transactional
    void processAdjustment(BulkAdjust bulkAdjust) {
        // list all active leaseItems and effect the adjustments
        List<LeaseItem> leaseItems = LeaseItem.where {
            terminated == false
            lease.checkStatus == CheckStatus.APPROVED
            lease.expired == false
            lease.terminated == false
            lease.deleted == false
            if(bulkAdjust.propertyType) {
                lease.property.propertyType == bulkAdjust.propertyType
            }
            (lease.leaseStartDate <= bulkAdjust.actionDate && lease.leaseExpiryDate >= bulkAdjust.actionDate)
            subAccount == bulkAdjust.subAccount
        }.join('lease').list();

        for(LeaseItem leaseItem : leaseItems) {
            //@todo: create the logic to update the leaseItem and generate the bulkAdjustItems
            log.info("updating the leaseItem.id=${leaseItem.id} for bulkAdjust #${bulkAdjust.id}");

            BigDecimal leaseItemOldAmount = leaseItem.amount;

            // calculate the new amount;
            BigDecimal changeAmount = bulkAdjust.amount;
            if(bulkAdjust.amountType.code == 'perc') {
                changeAmount = leaseItem.amount * (bulkAdjust.amount / 100);
            } else {

                Currency leaseCurrency = leaseItem.lease.leaseCurrency;
                //we want to convert the bulkAdjust.amount into the leaseCurrency_amount
                ExchangeRate exchangeRate = exchangeRateService.getActiveExchangeRate(bulkAdjust.transactionCurrency, leaseCurrency, bulkAdjust.actionDate);
                BigDecimal newChangeAmount = exchangeRateService.convert(changeAmount, exchangeRate);

                log.debug("adjusting the value of [amount=${changeAmount}, toValue=${newChangeAmount}]")
                changeAmount = newChangeAmount
            }

            BigDecimal newAmount = leaseItem.amount;

            if(bulkAdjust.adjustmentType.code == 'I') {
                newAmount = newAmount + changeAmount;
            } else {
                newAmount = newAmount - changeAmount;
            }

            leaseItem.amount = newAmount;
            leaseItem.save();

            BulkAdjustmentItem bulkAdjustmentItem = new BulkAdjustmentItem()
            bulkAdjustmentItem.bulkAdjustment = bulkAdjust;
            bulkAdjustmentItem.leaseItem = leaseItem;
            bulkAdjustmentItem.previousAmount = leaseItemOldAmount;
            bulkAdjustmentItem.newAmount = newAmount;
            bulkAdjustmentItem.save();
        }

        // set adjust as processed
        if(!bulkAdjust.isAttached()){
            bulkAdjust.attach()
        }
        bulkAdjust.processed = true;
        bulkAdjust.dateProcessed = new Date();
        bulkAdjust.save(failOnError: true, flush: true);
    }
}
