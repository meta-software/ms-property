package metasoft.property.core

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import groovy.util.logging.Slf4j
import metasoft.property.ExchangeRateGroupCommand

@Slf4j
@ReadOnly
//@GrailsCompileStatic
class ExchangeRateGroupService implements DataBinder {

    CurrencyService currencyService
    ExchangeRateGroupCheckService exchangeRateGroupCheckService

    ExchangeRateGroup get(Serializable id) {
        return ExchangeRateGroup.get(id)
    }

    List<ExchangeRateGroup> list(Map args) {

        args['order'] = 'desc'
        args['sort'] = 'startDate'

        def activeParam = args.get('active', true)
        def deletedParam = args.get('deleted', false)

        return ExchangeRateGroup.where {
            deleted == deletedParam
            active == activeParam
        }.list(args);
    }

    List<ExchangeRateGroup> listActive(Map args) {

        args['order'] = 'asc'
        args['sort'] = 'id'

        return ExchangeRateGroup.where{
            deleted == false
            active == true
        }.list(args);
    }

    Long count(Map args) {
        return ExchangeRateGroup.count()
    }

    @Transactional
    ExchangeRateGroup delete(Serializable id) {
        ExchangeRateGroup exchangeRateGroup = ExchangeRateGroup.get(id)

        if(exchangeRateGroup && !exchangeRateGroup.deleted) {
            exchangeRateGroup.deleted = true;
            exchangeRateGroup.dateDeleted = new Date();

            //also delete the exchangeRates
            exchangeRateGroup.exchangeRates*.deleted=true;
            exchangeRateGroup.exchangeRates*.dateDeleted = new Date();

        }

        return exchangeRateGroup.save()
    }

    @Transactional
    ExchangeRateGroupCheck save(ExchangeRateGroupCommand exchangeRateGroupCommand) {
        ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupCheckService.save(exchangeRateGroupCommand)
        exchangeRateGroupCheck
    }

    @Transactional
    ExchangeRateGroup update(Long id, ExchangeRateGroupCommand exchangeRateGroupCommand) {
        ExchangeRateGroup exchangeRateGroup = ExchangeRateGroup.get(id)
        bindData(exchangeRateGroup, exchangeRateGroupCommand)
        return exchangeRateGroup.save()
    }

    ExchangeRateGroup fromTemplate() {
        ExchangeRateGroup exchangeRateGroup = new ExchangeRateGroup()
        //exchangeRateGroup.startDate = new Date().clearTime();

        List<Currency> currencies = currencyService.list([:]);
        Currency reportingCurrency = currencyService.getReportingCurrency()
        currencies.each{Currency currency ->
            if(reportingCurrency == currency)
                return;
            ExchangeRate exchangeRate = new ExchangeRate()
            exchangeRate.fromCurrency = currency;
            exchangeRate.toCurrency = reportingCurrency;
            exchangeRateGroup.addToExchangeRates(exchangeRate);
        }

        return exchangeRateGroup;
    }

}