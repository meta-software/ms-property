package metasoft.property.core

import grails.gorm.services.Service

@Service(PropertyType)
interface PropertyTypeService {

    PropertyType get(Serializable id)

    List<PropertyType> list(Map args)

    Long count()

    void delete(Serializable id)

    PropertyType save(PropertyType propertyType)

}