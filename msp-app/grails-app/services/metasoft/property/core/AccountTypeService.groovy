package metasoft.property.core

import grails.gorm.services.Service

@Service(AccountType)
interface AccountTypeService {

    AccountType get(Serializable id)

    List<AccountType> list(Map args)

    Long count()

    void delete(Serializable id)

    AccountType save(AccountType accountType)

}