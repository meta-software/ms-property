package metasoft.property.core

import grails.gorm.transactions.ReadOnly
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.validation.ValidationException
import grails.web.databinding.DataBinder
import org.springframework.validation.BindException

//@GrailsCompileStatic
class PendingReceiptService implements DataBinder {

    ReceiptBatchService receiptBatchService;
    ReceiptTmpService receiptTmpService
    ReceiptService receiptService
    SpringSecurityService springSecurityService
    CurrencyService currencyService

    PendingReceipt get(Serializable id) {
        return PendingReceipt.get(id);
    }

    @ReadOnly
    List<PendingReceipt> listUnallocated(Map args) {
        String ref = args.get('ref')
        String account = args.get('account')

        return PendingReceipt.where{
            if(ref) { transactionReference ==~ "%$ref%" }
            if(account) { tenant.accountName ==~ "%$account%" }
            allocated == false
        }.list(args);
    }

    @ReadOnly
    Long countPendingReceipts() {
        return PendingReceipt.where{
            allocated == false
        }.count() as Long;
    }

    @Transactional
    def clearAllocations(Serializable id) {
        PendingReceipt pendingReceipt = PendingReceipt.get(id)

        if(!pendingReceipt) {
            Object[] errorArgs = []
            BindException errors = new BindException(pendingReceipt, PendingReceipt.class.name);
            errors.reject('pendingReceipt.exists', errorArgs,"Pending receipt was not found or does not exist")
            def ve = new ValidationException("Pending receipt was not found or does not exist", errors);
            throw ve
        }

        // update the pending receipt
        //pendingReceipt.amountAllocated = 0.0;
        //pendingReceipt.receiptAllocations.clear();

        //delete the allocations
        PendingReceiptAllocation.executeUpdate("delete PendingReceiptAllocation p where p.pendingReceipt.id=:id", [id: id]);
        pendingReceipt.save(flush: true);
    }

    Boolean validate(PendingReceipt pendingReceipt) {
        println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        // check receipt has already been allocated
        if(pendingReceipt.allocated) {
            Object[] errorArgs = []
            BindException errors = new BindException(pendingReceipt, PendingReceipt.class.name);
            errors.reject('pendingReceipt.allocated', errorArgs,"Pending receipt has already been allocated.")
            def ve = new ValidationException("Pending receipt has already been allocated.", errors);
            throw ve
        }

        //check transactionDate is valid
        if(!BillingCycle.isInOpenCycle(pendingReceipt.transactionDate)) {
            BindException errors = new BindException(pendingReceipt, PendingReceipt.class.name);
            Object[] args = [pendingReceipt.transactionDate.format('YYYY-MM-dd')]
            errors.rejectValue("transactionDate","billingCycle.posted.invalid", args, "Invalid transaction date provided the open billing cycles")
            def ve = new ValidationException("An invalid transaction date was provided for the receipt.", errors);
            throw ve
        }

    }

    @Transactional
    def postReceipt(Long receiptId) {
        PendingReceipt pendingReceipt = PendingReceipt.get(receiptId);

        validate(pendingReceipt);

        pendingReceipt.allocated = true;
        pendingReceipt.allocatedBy = springSecurityService.currentUser.username;
        pendingReceipt.save();

        // create the receipt batch
        ReceiptBatch receiptBatch = createReceiptBatchFromPendingReceipt(pendingReceipt)
        receiptBatch.save(flush: true);
        receiptBatchService.postReceiptBatch(receiptBatch);
    }

    ReceiptBatch createReceiptBatchFromPendingReceipt(PendingReceipt pendingReceipt) {

        ReceiptBatch receiptBatch = new ReceiptBatch();
        receiptBatch.receiptBatchType = ReceiptBatch.ReceiptBatchType.AUTO;
        receiptBatch.autoCommitted = true;
        receiptBatch.transactionType = TransactionType.findByTransactionTypeCode("RC");
        receiptBatch.batchNumber = String.format("RN.%05d", pendingReceipt.id);
        receiptBatch.checkStatus = CheckStatus.PENDING
        receiptBatch.makeDate = new Date();
        receiptBatch.maker = springSecurityService.currentUser as SecUser;

        //add the receipt object to the batch
        ReceiptTmp receiptTmp = new ReceiptTmp()
        bindData(receiptTmp, pendingReceipt.properties)
        receiptTmp.narrative = pendingReceipt.narration;

        //add the ReceiptItem objects
        pendingReceipt.receiptAllocations.each { PendingReceiptAllocation receiptAllocation ->
            ReceiptItemTmp receiptItemTmp = new ReceiptItemTmp();
            receiptItemTmp.lease = receiptAllocation.lease;
            receiptItemTmp.allocatedAmount = receiptAllocation.allocatedAmount;
            receiptItemTmp.subAccount = receiptAllocation.subAccount;
            receiptTmp.addToReceiptItemTmps(receiptItemTmp);
            receiptTmp.discard(); //we do not want to persist this object
        }
        receiptTmp.discard(); //we do not want to persist this object

        //apply the applicable exchange rates
        receiptTmp = receiptTmpService.transactionCurrencies(receiptTmp, currencyService.reportingCurrency, currencyService.reportingCurrency);
        Receipt receipt = receiptService.fromReceiptTmp(receiptTmp);
        receiptBatch.addToReceipts(receipt);
        return receiptBatch;
    }

    Long unallocatedReceiptsCount() {
        return PendingReceipt.where{
            allocated == false
        }.count() as Long;
    }
}
