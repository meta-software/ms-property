package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.Lease
import metasoft.property.core.LeaseCheck
import metasoft.property.core.LeaseCheckService

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class LeaseCheckOutboxController {


    LeaseCheckService leaseCheckService

    static allowedMethods = [cancel: "PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<LeaseCheck> leaseCheckList = leaseCheckService.listOutbox(params)
        respond leaseCheckList, model:[leaseCheckCount: leaseCheckService.count()]
    }

    def show(Long id) {
        LeaseCheck leaseCheck = leaseCheckService.get(id)
        Lease lease = leaseCheckService.getEntity(leaseCheck);
        respond (leaseCheck, model: [lease: lease])
    }

    def cancel(Long id) {
        LeaseCheck leaseCheck = leaseCheckService.get(id);

        if (leaseCheck == null) {
            notFound()
            return
        }

        try {
            leaseCheckService.cancel(leaseCheck, request.JSON)
        } catch (ValidationException e) {
            respond leaseCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseCheck.label', default: 'LeaseCheck'), leaseCheck.id])
                redirect leaseCheck
            }
            '*'{ respond leaseCheck, status: ACCEPTED }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseCheck.label', default: 'LeaseCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
