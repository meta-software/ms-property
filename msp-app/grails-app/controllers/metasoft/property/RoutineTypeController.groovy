package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BillingCycle
import metasoft.property.core.BillingCycleService
import metasoft.property.core.RoutineType

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class RoutineTypeController {

    static allowedMethods = []

    def listOptions() {
        Map args = ['sort': ['name': 'asc']];

        List<RoutineType> routineTypes = RoutineType.list(args)
        respond routineTypes
    }

}
