package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.JobSchedule
import metasoft.property.core.JobScheduleService

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class JobScheduleController {

    JobScheduleService jobScheduleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def app() {
        render view: 'app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond jobScheduleService.list(params), model:[jobScheduleCount: jobScheduleService.count()]
    }

    def show(Long id) {
        respond jobScheduleService.get(id)
    }

    def create() {
        respond new JobSchedule(params)
    }

    def save(JobSchedule jobSchedule) {
        if (jobSchedule == null) {
            notFound()
            return
        }

        try {
            jobScheduleService.save(jobSchedule)
        } catch (ValidationException e) {
            respond jobSchedule.errors, view:'create'
            return
        }

        respond jobSchedule, [status: CREATED]
    }

    def edit(Long id) {
        respond jobScheduleService.get(id)
    }

    def update(JobSchedule jobSchedule) {
        if (jobSchedule == null) {
            notFound()
            return
        }

        try {
            jobScheduleService.save(jobSchedule)
        } catch (ValidationException e) {
            respond jobSchedule.errors, view:'edit'
            return
        }

        respond jobSchedule, [status: OK]
    }

    protected void notFound() {
        request.withFormat {
            render status: NOT_FOUND
        }
    }
}
