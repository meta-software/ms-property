package metasoft.property

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import grails.validation.Validateable
import groovy.json.JsonSlurper
import metasoft.property.core.CheckStatus
import metasoft.property.core.SecRole
import metasoft.property.core.SecUser
import metasoft.property.core.SecUserCheck
import metasoft.property.core.SecUserCheckService
import metasoft.property.core.SecUserPreviousPassword
import metasoft.property.core.SecUserPreviousPasswordService
import metasoft.property.core.SecUserSecRole
import metasoft.property.core.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.Errors

import javax.persistence.Transient

import static org.springframework.http.HttpStatus.*

@Secured(value=['ROLE_ADMIN'])
class UserController {

    SpringSecurityService springSecurityService;
    UserService userService;
    SecUserCheckService secUserCheckService;

    static defaultAction = 'index'

    def app() {
        render view: 'vue-app'
    }

    def create() {
        SecUser secUser = new SecUser();
        List<SecRole> userRoles = userService.listUserRoleOptions()
        respond secUser, view: 'create', model: [user: secUser, userRoles: userRoles]
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 50);

        SecUser currentUser = springSecurityService.currentUser as SecUser;

        List<SecUser> userList = userService.list(params);
        Long userCount = userService.count(params);

        List<SecRole> userTypeList = userService.listUserRoleOptions();

        respond userList, model:[userList: userList, userTypeList: userTypeList, userCount: userCount, currentUser: currentUser]
    }

    def update(Long id) {

        SecUser secUser = userService.get(id);

        if(secUser == null) {
            notFound();
            return;
        }

        if(secUser.username == springSecurityService.currentUser?.username) {
            flash.message = ("ERROR: User cannot edit themselves");
            redirect action: "index"
            return;
        }

        Map args = request.JSON ?: params;

        try{
            SecUserCheck secUserCheck =  userService.update(secUser, args)
            redirect action: "index", controller: 'user'
        } catch(ValidationException ve) {
            def userRoles = secUser.authorities;

            if(secUser.isRecordDirty()) {
                // we need to update the user record with the userCheck record so that the updating can be seamless
                Map checkArgs = new JsonSlurper().parseText(secUser.checkEntity.jsonData);

                secUser.setProperties(checkArgs);
                userRoles = secUserCheckService.getAuthorities(secUser.checkEntity);
            }

            respond secUser.errors, view: 'edit', model: [user: secUser, userRoles: userRoles]
        }
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod = "PUT")
    def updatePassword(UserUpdatePasswordCommand userUpdatePasswordCommand) {

        SecUser secUser = userService.get(userUpdatePasswordCommand.id)

        if(secUser == null) {
            notFound();
            return;
        }

        if(!userUpdatePasswordCommand.validate()) {
            respond userUpdatePasswordCommand.errors, view: 'edit', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
            return;
        }

        try {
            userService.updateUserPassword(secUser, userUpdatePasswordCommand.password);
        } catch(ValidationException e) {
            respond secUser.errors, view: 'edit', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
            e.printStackTrace()
            return;
        }
    }

    @Secured(value=['IS_AUTHENTICATED_ANONYMOUSLY'], httpMethod = "POST")
    def updateExpired(UserUpdateExpiredPasswordCommand expiredPasswordCommand) {

        String username = expiredPasswordCommand.username; //session['LAST_LOGIN_USERNAME'];
        SecUser secUser = SecUser.findByUsername(username);

        if(secUser == null || !secUser.passwordExpired ) {
            //respond userUpdatePasswordCommand, view: 'passwordExpired', status: BAD_REQUEST;
            redirect controller: 'login', action: 'auth'
            return;
        }

        if(!expiredPasswordCommand.validate()) {
            respond expiredPasswordCommand.errors, view: 'passwordExpired', model: [username: username], status: UNPROCESSABLE_ENTITY;
            return;
        }

        try {
            userService.updatePassword(secUser, expiredPasswordCommand.newPassword);
            springSecurityService.reauthenticate(username, expiredPasswordCommand.newPassword);
            redirect controller: "dashboard"
        } catch(ValidationException e) {
            //respond secUser.errors, view: 'edit', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
            //authenticate the user here and redirect to the dashboard page
            redirect controller: "dashboard"
        }
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod = "PUT")
    def updatePasswordInit(UserUpdatePasswordCommand userUpdatePasswordCommand) {

        SecUser secUser = springSecurityService.currentUser as SecUser

        if(secUser == null) {
            notFound();
            return;
        }

        if(!userUpdatePasswordCommand.validate()) {
            respond userUpdatePasswordCommand.errors, view: 'edit', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
            return;
        }

        try {
            secUser.password = userUpdatePasswordCommand.password;
            userService.save(secUser)
        } catch(ValidationException e) {
            respond secUser.errors, view: 'edit', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
            return;
        }
    }

    def show(Long id) {

        SecUser user = userService.get(id);

        if(user == null) {
            notFound();
            return;
        }

        respond user;
    }

    def save() {

        Map args = request.JSON ?: params;
        SecUser secUser = new SecUser();
        bindData(secUser, args, [exclude: ['authorities']]);

        if(secUser == null) {
            notFound();
            return;
        }

        try{
            SecUserCheck secUserCheck =  userService.create(secUser, args)
            redirect action: "show", controller: 'user-check', params: [id: secUserCheck.id]
        } catch(ValidationException ve) {
            List<SecRole> userRoles = userService.listUserRoleOptions()
            respond secUser.errors, view: 'create', model: [user: secUser, userRoles: userRoles]
        }
    }

    def edit(Long id) {

        SecUser user = userService.get(id)

        if(user == null) {
            notFound();
            return;
        }

        if(user.username == springSecurityService.currentUser?.username) {
            flash.message = ("ERROR: User cannot update self record");
            redirect action: "index"
            return;
        }

        def userRoles = user.authorities;

        if(user.isRecordDirty()) {
            // we need to update the user record with the userCheck record so that the updating can be seamless
            Map args = new JsonSlurper().parseText(user.checkEntity.jsonData);

            user.setProperties(args);
            userRoles = secUserCheckService.getAuthorities(user.checkEntity);
        }


        respond user, view: 'edit', model: [user: user, userRoles: userRoles]
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(value=['IS_AUTHENTICATED_ANONYMOUSLY'])
    def passwordExpired() {
        String username = session['LAST_LOGIN_USERNAME']

        if(username == null) {
            request.withFormat {
                form multipartForm {
                    redirect action: "auth", method: "GET"
                }
                'html' {
                    redirect action: "auth", method: "GET"
                }
                'json' {render status: UNAUTHORIZED}
                '*' {render status: UNAUTHORIZED}
            }
        }

        [username: username]
    }

}

class UserUpdatePasswordCommand implements Validateable {

    @Transient
    @Autowired
    SpringSecurityService springSecurityService;

    Long id;
    String password;
    String confirmPassword

    static constraints = {
        id nullable: true
        password minSize: 6, validator : { String value, UserUpdatePasswordCommand object, Errors errors ->
            if (!(object.confirmPassword == value)) {
                errors.rejectValue('password', 'user.password.noMatch')
            }
        }
        confirmPassword ()
    }

    def asType (Class type) {
        if(type == SecUser.class) {
            SecUser user = SecUser.get(id);
            user.password = this.password;
            return user;
        }

        return null;
    }

}

class UserUpdateExpiredPasswordCommand implements Validateable {

    @Transient
    @Autowired
    SecUserPreviousPasswordService secUserPreviousPasswordService;

    @Transient
    @Autowired
    SpringSecurityService springSecurityService;

    String username;

    String currentPassword;
    String newPassword;
    String confirmPassword

    static constraints = {
        username nullable: true;
        currentPassword validator: { String value, UserUpdateExpiredPasswordCommand object, Errors errors ->

            SecUser secUser = retrieveUser(object.username);

            // check that the current password is correct
            if(!object.isValidPassword(secUser.password, value)) {
                errors.rejectValue('currentPassword', 'user.password.invalidCurrent')
            }

        }
        newPassword minSize: 6, validator : { String value, UserUpdateExpiredPasswordCommand object, Errors errors ->
            if (!(object.confirmPassword == value)) {
                errors.rejectValue('newPassword', 'user.password.noMatch')
            }
        }
        confirmPassword ()
    }

    def asType (Class type) {
        if(type == SecUser.class) {
            SecUser user = SecUser.findByUsername(username);
            user.password = this.password;
            return user;
        }

        return null;
    }

    static SecUser retrieveUser(String username) {

        SecUser secUser = null;

        SecUser.withNewSession { it ->
            secUser = SecUser.findByUsername(username);
        }

        return secUser;
    }

    boolean isValidPassword(String password, String checkPassword) {
        boolean isValid = springSecurityService.passwordEncoder.isPasswordValid(password, checkPassword, null);
        return isValid;
    }

}

class UserUpdateCommand implements Validateable {

    String firstName
    String lastName
    String fullName
    String emailAddress
    String employeeId

    Boolean enabled
    Boolean accountExpired
    Boolean accountLocked
    Boolean passwordExpired

    Map userType;   //sort of the main user role

    static constraints = {
        fullName nullable: true
        emailAddress nullable: true, email: true, unique: true
        employeeId nullable: true, unique: true
    }

}