package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.SubAccount
import metasoft.property.core.SubAccountService

@Secured(value=['IS_AUTHENTICATED_ANONYMOUSLY'])
class SubAccountController {

    SubAccountService subAccountService;

    static allowedMethods = []

    def findByAccount(String accountNumber) {
        SubAccount subAccount = subAccountService.findByAccount(accountNumber);
        respond subAccount, model:[subAccount: subAccount], view: 'show'
    }

    def index(Integer max) {
        List<SubAccount> subAccountList = subAccountService.list(params)
        respond subAccountList, model: [subAccountList: subAccountList], view: '/subAccount/index'
    }

    def listExpenses(Integer max) {
        List<SubAccount> subAccountList = subAccountService.listExpenses(params)
        respond subAccountList, model: [subAccountList: subAccountList], view: '/subAccount/index'
    }

    def listCosts(Integer max) {
        List<SubAccount> subAccountList = subAccountService.listCosts(params)
        respond subAccountList, model: [subAccountList: subAccountList], view: '/subAccount/index'
    }

    def listOptions() {
        List<SubAccount> subAccountList = subAccountService.listOptions(params)
        respond subAccountList, model: [subAccountList: subAccountList], view: '/subAccount/index'
    }

}
