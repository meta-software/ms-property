package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.ExchangeRateGroup
import metasoft.property.core.ExchangeRateGroupCheck
import metasoft.property.core.ExchangeRateGroupService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ExchangeRateGroupController {

    ExchangeRateGroupService exchangeRateGroupService

    static allowedMethods = [save: "POST", delete: "DELETE"]

    static defaultAction = "app"

    def app() {}

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<ExchangeRateGroup> exchangeRateGroupList = exchangeRateGroupService.list(params);
        respond exchangeRateGroupList, view: 'index', model:[exchangeRateGroupCount: exchangeRateGroupService.count(params)]
    }

    def show(Long id) {
        ExchangeRateGroup exchangeRateGroup = exchangeRateGroupService.get(id)

        if(!exchangeRateGroup) {
            notFound();
            return;
        }

        respond exchangeRateGroup
    }

    def create() {

        ExchangeRateGroup exchangeRateGroup = exchangeRateGroupService.fromTemplate();

        respond exchangeRateGroup
    }

    def save(ExchangeRateGroupCommand exchangeRateGroupCommand) {
        if (exchangeRateGroupCommand == null) {
            notFound()
            return
        }

        try {
            ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupService.save(exchangeRateGroupCommand);

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'exchangeRateGroup.label', default: 'Exchange Rate Group'), exchangeRateGroupCheck.id])
                    redirect exchangeRateGroupCheck
                }
                '*' { respond exchangeRateGroupCheck, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }
    }

    def open(Long id) {

        ExchangeRateGroup exchangeRateGroup = exchangeRateGroupService.get(id)

        if (exchangeRateGroup == null) {
            notFound()
            return
        }

        try {
            exchangeRateGroupService.openCycle(exchangeRateGroup)
        } catch (ValidationException e) {
            respond exchangeRateGroup.errors, view:'show', model: [exchangeRateGroup: exchangeRateGroup]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.opened.message', args: [message(code: 'exchangeRateGroup.label', default: 'ExchangeRateGroup'), exchangeRateGroup.id])
                redirect exchangeRateGroup
            }
            '*'{ respond exchangeRateGroup, [status: OK] }
        }
    }

    def activate(Long id) {

        ExchangeRateGroup exchangeRateGroup = exchangeRateGroupService.get(id)

        if (exchangeRateGroup == null) {
            notFound()
            return
        }

        try {
            exchangeRateGroupService.activateCycle(exchangeRateGroup)
        } catch (ValidationException e) {
            respond exchangeRateGroup.errors, view:'show', model: [exchangeRateGroup: exchangeRateGroup]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.activated.message', args: [message(code: 'exchangeRateGroup.label', default: 'ExchangeRateGroup'), exchangeRateGroup.id])
                redirect exchangeRateGroup
            }
            '*'{ respond exchangeRateGroup, [status: OK] }
        }
    }

    def delete(Long id) {

        ExchangeRateGroup exchangeRateGroup = exchangeRateGroupService.get(id)
        if (exchangeRateGroup == null) {
            notFound()
            return
        }

        try {
            exchangeRateGroup = exchangeRateGroupService.delete(id)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.activated.message', args: [message(code: 'exchangeRateGroup.label', default: 'ExchangeRateGroup'), exchangeRateGroup.id])
                    redirect exchangeRateGroup
                }
                '*'{ respond exchangeRateGroup, [status: OK] }
            }

        } catch (ValidationException e) {
            respond e.errors, view: 'show', model: [exchangeRateGroup: exchangeRateGroup]
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'exchangeRateGroup.label', default: 'ExchangeRateGroup'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}

class ExchangeRateGroupCommand {
    Date startDate
    Date endDate
    Boolean active;

    List<ExchangeRateCommand> exchangeRates;

    static constraints = {
        startDate()
        endDate()
        active()
    }

    class ExchangeRateCommand {
        Map fromCurrency;  //we're converting from this currency to the toCurrency (our reference currency)
        Map toCurrency;    //this should always be the base currency.
        BigDecimal rate;        //the exchange rate value
        Date startDate
        Date endDate

        Boolean deleted = false;
    }
}