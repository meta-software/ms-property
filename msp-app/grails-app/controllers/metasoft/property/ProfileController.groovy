package metasoft.property

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.SecUser
import metasoft.property.core.SecUserPreviousPassword
import metasoft.property.core.SecUserPreviousPasswordService
import metasoft.property.core.UserService
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.metadata.ClassMetadata
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.Errors

import javax.persistence.Transient

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ProfileController {

    SpringSecurityService springSecurityService
    UserService userService;

    def show() {
        render view: 'show', model: [user: currentUser]
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod = "GET")
    def initLogin() {
        SecUser secUser = currentUser;

        respond secUser, view: 'initLogin', model: [user: secUser]
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod = "PUT")
    def updatePassword(UserProfileUpdatePasswordCommand userProfileUpdatePasswordCommand) {
        SecUser secUser = currentUser;

        if(!userProfileUpdatePasswordCommand.validate()) {
            respond userProfileUpdatePasswordCommand.errors, view: 'updateProfilePassword', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
            return;
        }

        try {
            // we're only interested in updating the password.
            userService.updatePassword(secUser, userProfileUpdatePasswordCommand.password);
            render status: ACCEPTED
        } catch(ValidationException e) {
            respond secUser.errors, view: 'edit', model: [user: secUser], status: UNPROCESSABLE_ENTITY;
        }
    }

    private SecUser getCurrentUser() {
        springSecurityService.currentUser as SecUser
    }
}

class UserProfileUpdatePasswordCommand implements Validateable {

    @Transient
    @Autowired
    SecUserPreviousPasswordService secUserPreviousPasswordService;

    @Transient
    @Autowired
    SpringSecurityService springSecurityService;

    @Transient
    static SecUser secUser = null;

    Long id;
    String currentPassword;
    String password
    String confirmPassword

    static constraints = {
        currentPassword validator: { String value, UserProfileUpdatePasswordCommand object, Errors errors ->

            secUser = retrieveUser(object.id);

            // check that the current password is correct
            if(!object.isValidPassword(secUser.password, value)) {
                errors.rejectValue('password', 'user.password.invalidCurrent')
            }

        }
        password minSize: 6, validator : { String value, UserProfileUpdatePasswordCommand object, Errors errors ->

            secUser = retrieveUser(object.id)

            // configure that the new password is not similar to the current password
            if(object.isValidPassword(secUser.password, value)) {
                errors.rejectValue('password', 'user.password.newEqualsCurrent')
            }
            else if (!(object.confirmPassword == value)) {
                errors.rejectValue('password', 'user.password.noMatch')
            }
            else if(object.isPasswordExists(value)) {
                errors.rejectValue('password', 'user.password.noReuse')
            }
        }

        confirmPassword ()
    }

    boolean isValidPassword(String password, String checkPassword) {
        boolean isValid = springSecurityService.passwordEncoder.isPasswordValid(password, checkPassword, null);
        return isValid;
    }

    boolean isPasswordExists(String password) {
        List<SecUserPreviousPassword> passwordList = secUserPreviousPasswordService.retrievePrevious(this.id, 12);

        for(SecUserPreviousPassword previousPassword : passwordList ){
            if(isValidPassword(previousPassword.password, password)) {
                return true;
            }
        }

        return false;
    }

    static SecUser retrieveUser(Long id) {

        SecUser.withNewSession { it ->
            secUser = SecUser.get(id)
        }

        return secUser;
    }
}
