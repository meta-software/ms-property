package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.AccountType
import metasoft.property.core.AccountTypeService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class AccountTypeController {

    AccountTypeService accountTypeService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<AccountType> accountTypeList = accountTypeService.list(params)

        respond accountTypeList, view: 'vue-app'
    }

    def listOptions() {
        List<AccountType> accountTypeList = accountTypeService.list(params)

        respond accountTypeList, view: 'vue-app'
    }

    def create() {
        respond new AccountType(params)
    }

    def save(AccountType accountType) {
        if (accountType == null) {
            notFound()
            return
            accountType.setProperties([:])
        }

        try {
            accountTypeService.save(accountType)
        } catch (ValidationException e) {
            respond accountType.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accountType.label', default: 'AccountType'), accountType.id])
                redirect controller: 'accountType', action: 'index'
            }
            '*' { respond accountType, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond accountTypeService.get(id)
    }

    def update(AccountType accountType) {
        if (accountType == null) {
            notFound()
            return
        }

        try {
            accountTypeService.save(accountType)
        } catch (ValidationException e) {
            respond accountType.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'accountType.label', default: 'AccountType'), accountType.id])
                redirect controller: 'accountType', action: 'index'
            }
            '*'{ respond accountType, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        accountTypeService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'accountType.label', default: 'AccountType'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountType.label', default: 'AccountType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
