package metasoft.property

class UrlMappings {

    static mappings = {

        //api visibility controllers and actions
        //"/api/v1/transactions/refunds" (controller: "transactionApi", action: "refund", method: "POST", namespace: "v1")
        //"/api/v1/transactions/refunds" (controller: "transactionApi", method: "OPTIONS", namespace: "v1")
        //"/api/v1/transactions/reversals" (controller: "transactionApi", action: "reversal", method: "POST", namespace: "v1")
        //"/api/v1/transactions/reversals" (controller: "transactionApi", method: "OPTIONS", namespace: "v1")

        "/api/v1/transactions/receipt/rtgs" (controller: "transactionApi", action: "receiptRtgs", method: "POST", namespace: "v1")
        "/api/v1/transactions/receipt/rtgs" (controller: "transactionApi", method: "OPTIONS", namespace: "v1")

        //API to retrieve new tenant accounts
        "/api/v1/tenant/new-accounts"(controller: 'tenantApi', action: 'newAccounts', namespace: "v1", method: "GET")
        "/api/v1/tenant/create" (controller: "tenantApi", action: "create", method: "POST", namespace: "v1")
        "/api/v1/tenant/create" (controller: "tenantApi", method: "OPTIONS", namespace: "v1")
        //api visibility controllers and actions

        //API to retrieve new rental-units
        "/api/v1/rental-unit/new-units"(controller: 'rentalUnitApi', action: 'newRentalUnits', namespace: "v1", method: "GET")
        //api visibility controllers and actions

        "/transaction/$action?/$id?(.$format)?" {
            controller = 'mainTransaction'
        }
        "/pending-receipt/$receiptId/receipt-allocation" {
            controller = 'receiptAllocation'
            action = 'listByReceipt'
        }
        "/pending-receipt/$id/clear-allocations" {
            controller = 'pendingReceipt'
            action = 'clearAllocations'
        }

        "/rental-unit/property/$propertyId(.$format)?" {
            controller = 'rentalUnit'
            action = 'listByProperty'
        }

        "/transactions/$action?/$id?(.$format)?" {
            controller = 'transactionBatchTmp' //replaced Header with Batch here.
        }

        "/receipt-batch-tmp/$action?/$id?(.$format)?" {
            controller = 'receiptBatchTmp'
        }

        "/receipt-tmp/list-by-batch/$batchId?(.$format)?" {
            controller = 'receiptTmp'
            action = 'listByBatch'
        }

        "/receipt-check/$batchId/receipt-items?(.$format)?" {
            controller = 'receiptCheck'
            action = 'receiptItems'
        }

        "/configs/$action?/$id?(.$format)?" {
            controller = 'configSetting'
        }

        "/transaction-tmp/$action?/$id?(.$format)?" {
            controller = 'transactionTmp'
        }
        "/transaction-tmp/list-by-header/$transactionBatchId?(.$format)?"{
            controller = "transactionTmp"
            action = "listByHeader"
        }

        "/tenant/$accountNumber/rental-units" {
            controller = "tenant"
            action = "rentalUnits"
        }

        "/tenant/$accountNumber/leases" {
            controller = "tenant"
            action = "leases"
        }

        "/tenant/tenant-leases/$tenantId" {
            controller = "tenant"
            action = "tenantLeases"
        }

        "/landlord/fetch-list" {
            controller = "landlord"
            action = "fetchList"
        }

        "/landlord/$accountNumber/properties" {
            controller = "landlord"
            action = "properties"
        }

        "/landlord/$id/property-list" {
            controller = "landlord"
            action = "propertyList"
        }

        "/sub-account/$action?/$id?(.$format)?"{
            controller="subAccount"
            constraints {
                // apply constraints here
            }
        }
        "/sub-account/find-by-account/$accountNumber" {
            controller = "subAccount"
            action = findByAccount
        }

        "/user/update-password"{
            controller = "user"
            action = "updatePassword"
        }
        "/user/password-expired"{
            controller = "user"
            action = "passwordExpired"
        }

        "/$controller/inbox/show/$id?" {
            action = 'show'
        }

        "/$controller/outbox/show/$id?" {
            action = 'showOutbox'
        }

        // url for showing outbox items globally.
        "/$controller/outbox/item/$id?" {
            action = "showOutbox"
        }

        // url for showing outbox items globally.
        "/$controller/list-options" {
            action = "listOptions"
        }

        "/api/v1/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        // @todo: remove

        "/cbz"(controller: "home", action: "cbz")
        "/flex"(controller: "home", action: "flex")

        "/"(controller :"dashboard")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
