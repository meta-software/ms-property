package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.StandingOrder
import metasoft.property.core.StandingOrderService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class StandingOrderController {

    StandingOrderService standingOrderService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond standingOrderService.list(params), model:[standingOrderCount: standingOrderService.count()]
    }

    def show(Long id) {
        respond standingOrderService.get(id)
    }

    @Secured(value=['ROLE_LANDLORD_CREATE'])
    def create() {
        respond new StandingOrder(params)
    }

    @Secured(value=['ROLE_LANDLORD_CREATE'])
    def save(StandingOrder standingOrder) {
        if (standingOrder == null) {
            notFound()
            return
        }

        try {
            standingOrderService.save(standingOrder)
        } catch (ValidationException e) {
            render standingOrder.errors
            return
            respond standingOrder.errors, view:'create', model: [standinOrder: standingOrder]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'standingOrder.label', default: 'StandingOrder'), standingOrder.id])
                redirect controller: "landlord", action: "show", id: standingOrder.account.id
            }
            '*' { respond standingOrder, [status: CREATED] }
        }
    }

    @Secured(value=['ROLE_LANDLORD_UPDATE'])
    def edit(Long id) {
        respond standingOrderService.get(id)
    }

    @Secured(value=['ROLE_LANDLORD_UPDATE'])
    def update(StandingOrder standingOrder) {
        if (standingOrder == null) {
            notFound()
            return
        }

        try {
            standingOrderService.save(standingOrder)
        } catch (ValidationException e) {
            respond standingOrder.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'standingOrder.label', default: 'StandingOrder'), standingOrder.id])
                redirect standingOrder
            }
            '*'{ respond standingOrder, [status: OK] }
        }
    }

    @Secured(value=['ROLE_LANDLORD_DELETE'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        standingOrderService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'standingOrder.label', default: 'StandingOrder'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'standingOrder.label', default: 'StandingOrder'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
