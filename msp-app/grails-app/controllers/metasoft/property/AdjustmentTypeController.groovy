package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.AdjustmentType

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class AdjustmentTypeController {

    static allowedMethods = []

    def listOptions() {
        Map args = [:];
        List<AdjustmentType> adjustmentTypeList = AdjustmentType.list(args)

        respond adjustmentTypeList
    }

}
