package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.LeaseItem
import metasoft.property.core.LeaseItemService
import metasoft.property.core.LeaseReview
import metasoft.property.core.LeaseReviewService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class LeaseReviewController {

    LeaseReviewService leaseReviewService

    static allowedMethods = [save: "POST", update: "PUT", ignore: "PUT", ]

    def app (){
        render view: 'app'
    }

    def dashboard() {
        Map dashboard = leaseReviewService.dashboard();
        respond dashboard, view: 'dashboard'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100)

        List<LeaseReview> leaseReviews = leaseReviewService.pending(params);
        respond leaseReviews, view: 'index'
    }

    def reviewed(Integer max) {
        List<LeaseReview> leaseReviews = leaseReviewService.reviewed(params);
        respond leaseReviews, view: 'index'
    }

    def show(Long id) {
        respond leaseReviewService.get(id);
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def edit(Long id) {
        respond leaseReviewService.get(id);
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def ignore(Long id) {

        LeaseReview leaseReview = leaseItemService.get(id)
        if (leaseReview == null) {
            notFound()
            return
        }

        try {
            leaseReviewService.ignore(leaseReview)
        } catch (ValidationException e) {
            respond leaseReview.errors, view:'ignore'
            return
        }

        request.withFormat {
            respond leaseItem, [status: OK]
        }
    }

    /**
     * Terminate the lease agreement action
     *
     * @param id
     * @return
     */
    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def terminate(Long id) {

        LeaseItem leaseItem = LeaseItem.get(id);

        if (leaseItem == null) {
            notFound();
            return;
        }

        //terminate the lease
        try {
            leaseItem.active = false;
            leaseItemService.save(leaseItem);
        } catch(ValidationException e) {
            leaseItem.errors = e.errors
            respond leaseItem, view:'show'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'lease.terminated.message', args: [message(code: 'leaseItem.label', default: 'LeaseItem'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseItem.label', default: 'LeaseItem'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
