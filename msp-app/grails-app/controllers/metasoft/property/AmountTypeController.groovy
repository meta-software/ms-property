package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.AmountType

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class AmountTypeController {

    static allowedMethods = []

    def listOptions() {
        Map args = [:];
        List<AmountType> amountTypeList = AmountType.list(args)

        respond amountTypeList
    }

}
