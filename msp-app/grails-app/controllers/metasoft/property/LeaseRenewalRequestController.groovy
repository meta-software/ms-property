package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.core.Lease
import metasoft.property.core.LeaseRenewalRequest
import metasoft.property.core.LeaseRenewalRequestService
import metasoft.property.core.LeaseRenewalRequestTmp

import static org.springframework.http.HttpStatus.*

@Slf4j
@Secured(['IS_AUTHENTICATED_FULLY'])
class LeaseRenewalRequestController {

    LeaseRenewalRequestService leaseRenewalRequestService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value=['ROLE_PERM_LEASE_READ'], httpMethod ="GET")
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond leaseRenewalRequestService.list(params), model:[leaseRenewalRequestCount: leaseRenewalRequestService.count()]
    }

    @Secured(value=['ROLE_PERM_LEASE_READ'], httpMethod ="GET")
    def show(Long id) {
        LeaseRenewalRequest leaseRenewalRequest = leaseRenewalRequestService.get(id);

        if(leaseRenewalRequest == null) {
            notFound();
            return;
        }

        Lease lease = leaseRenewalRequest.lease
        respond leaseRenewalRequest, model: [leaseRenewalRequest: leaseRenewalRequest, lease: lease]
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="POST")
    /**
     * Tries to create a routineRequestTmp object and a Check entity pending approval from checkers.
     *
     * @return
     */
    def createRequest(LeaseRenewalRequestCommand leaseRenewalRequestCommand) {

        try {
            LeaseRenewalRequestTmp leaseRenewalRequestTmp = leaseRenewalRequestService.createRequest(leaseRenewalRequestCommand);
            respond leaseRenewalRequestTmp, status: ACCEPTED
        } catch (ValidationException e) {
            log.debug("error while creating leaseRenewalRequest entity: {}", e.message);
            respond(e.errors, status: UNPROCESSABLE_ENTITY);
        }

    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="GET")
    def edit(Long id) {
        respond leaseRenewalRequestService.get(id)
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="PUT")
    def update(LeaseRenewalRequest leaseRenewalRequest) {
        if (leaseRenewalRequest == null) {
            notFound()
            return
        }

        try {
            leaseRenewalRequestService.save(leaseRenewalRequest)
        } catch (ValidationException e) {
            respond leaseRenewalRequest.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseRenewalRequest.label', default: 'LeaseRenewalRequest'), leaseRenewalRequest.id])
                redirect leaseRenewalRequest
            }
            '*'{ respond leaseRenewalRequest, [status: OK] }
        }
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="DELETE")
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        leaseRenewalRequestService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'leaseRenewalRequest.label', default: 'LeaseRenewalRequest'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseRenewalRequest.label', default: 'LeaseRenewalRequest'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class LeaseRenewalRequestCommand {
    Lease lease;
    String actionReason; // the reason why the status must be in this new status
    Date leaseExpireDate; // the new date which the lease expire on

    static constraints = {
        lease validator: {Lease val, obj, errors ->
            if(val != null && val.terminated) {
                errors.rejectValue('lease', 'lease.renewal.terminated', "Renewal of a terminated lease is not allowed")
            }
        }
        actionReason maxSize: 1024
        leaseExpireDate validator: { val, obj, errors ->
            Date today = new Date().clearTime();
            if(val < today) {
                errors.rejectValue('leaseExpireDate', 'default.date.invalid.past', 'Date value should not be in the past')
            }
            if(val < obj.lease.leaseExpiryDate) {
                errors.rejectValue('leaseExpireDate', 'lease.renewal.expireDate.invalid', 'The new expire date should be after the current expire date')
            }
        }
    }

}