package metasoft.property

import grails.core.GrailsApplication
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.SecUser
import metasoft.property.core.SharedSessionService
import metasoft.property.core.UserSessionToken

class ReportController {

    SharedSessionService sharedSessionService;
    SpringSecurityService springSecurityService

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def index() {
        SecUser secUser = springSecurityService.currentUser as SecUser;

        // generate the sharedSessionService
        UserSessionToken userSessionToken = sharedSessionService.create(secUser)

        // redirect to the reports application.
        String reportEndpoint = grailsApplication.config.get('metasoft.config.reportserver.metasoft.loginUrl');

        String authUrl = reportEndpoint+"/"+userSessionToken.token;
        println(authUrl);

        redirect url: authUrl
    }

}
