package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BillingCycle
import metasoft.property.core.MainTransaction
import metasoft.property.core.RoutineRequest
import metasoft.property.core.RoutineRequestService
import metasoft.property.core.RoutineRequestTmp
import metasoft.property.core.RoutineType

import static org.springframework.http.HttpStatus.*

@Secured(['IS_AUTHENTICATED_FULLY'])
class RoutineRequestController {

    RoutineRequestService routineRequestService

    static allowedMethods = [save: "POST", create: "POST", cancel: "PUT"]

    def app() {
        render view: 'vue-app'
    }

    def transactions(Long id) {
        RoutineRequest routineRequest = routineRequestService.get(id);
        List<MainTransaction> mainTransactions = routineRequestService.getRoutineTransactions(routineRequest);

        render (view: '/mainTransaction/index', model: [mainTransactions: mainTransactions])
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<RoutineRequest> routineRequestList = routineRequestService.list(params);
        Long routineRequestCount = routineRequestService.count(params)

        List<BillingCycle> billingCycleList = BillingCycle.findAllByOpen(true, [sort: "name", order: "asc"]);
        List<RoutineType> routineTypeList = RoutineType.list([sort: 'name', order: 'asc'])

        RoutineRequest routineRequest = new RoutineRequest(billingCycle: BillingCycle.getActiveCycle());
        routineRequest.requestStatus = "pending";

        respond routineRequestList, model:[routineRequestCount: routineRequestCount,
                                           billingCycleList: billingCycleList,
                                           routineRequest: routineRequest,
                                           routineTypeList: routineTypeList]
    }

    def show(Long id) {

        RoutineRequest routineRequest = routineRequestService.get(id)
        if(routineRequest == null) {
            notFound();
            return;
        }

        respond routineRequest
    }

    def create() {
        RoutineRequest routineRequest = new RoutineRequest();

        List<BillingCycle> billingCycleList = BillingCycle.findAllByOpen(true, [sort: "name", order: "asc"]);
        List<RoutineType> routineTypeList = RoutineType.list([sort: 'name', order: 'asc'])

        respond routineRequest, view:'create', model:[billingCycleList: billingCycleList,
                                                      routineRequest: routineRequest,
                                                      routineTypeList: routineTypeList]
    }

    def save(RoutineRequest routineRequest) {
        if (routineRequest == null) {
            notFound()
            return
        }

        try {
            routineRequest.requestStatus = 'pending';
            routineRequestService.save(routineRequest)
        } catch (ValidationException e) {

            List<BillingCycle> billingCycleList = BillingCycle.findAllByOpen(true, [sort: "name", order: "asc"]);
            List<RoutineType> routineTypeList = RoutineType.list([sort: 'name', order: 'asc'])

            respond routineRequest.errors, view:'create', model:[billingCycleList: billingCycleList,
                                                                routineRequest: routineRequest,
                                                                routineTypeList: routineTypeList]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'routineRequest.label', default: 'RoutineRequest'), routineRequest.id])
                redirect action: 'index'
            }
            '*' { respond routineRequest, [status: CREATED, view: 'index'] }
        }
    }

    /**
     * Tries to create a routineRequestTmp object and a Check entity pending approval from checkers.
     *
     * @return
     */
    def createRequest() {

        Map args = (Map)request.JSON ?: params;
        RoutineRequestTmp routineRequestTmp = new RoutineRequestTmp()
        bindData(routineRequestTmp, args);

        try {
            routineRequestService.createRequest(routineRequestTmp);
            respond routineRequestTmp, status: ACCEPTED
        } catch (ValidationException e) {
            log.debug("error while creating routineRequestTmp entity: " + e.message);
            respond(e.errors, model: [routineRequestTmp: routineRequestTmp], status: UNPROCESSABLE_ENTITY);
        }

    }

    def cancel(Long id) {
        if (id == null) {
            notFound()
            return
        }

        routineRequestService.cancel(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'routineRequest.label', default: 'RoutineRequest'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'routineRequest.label', default: 'RoutineRequest'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
