package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.AccountType
import metasoft.property.core.AccountTypeService
import metasoft.property.core.BillingInvoiceEmailRequest
import metasoft.property.core.BillingInvoiceEmailRequestService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class BillingInvoiceEmailRequestController {

    BillingInvoiceEmailRequestService billingInvoiceEmailRequestService;

    def index(){
        render view: 'app'
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<BillingInvoiceEmailRequest> billingInvoiceEmailRequestList = billingInvoiceEmailRequestService.list(params)
        respond billingInvoiceEmailRequestList, view: 'vue-app'
    }

    def show(Long id) {
        BillingInvoiceEmailRequest billingInvoiceEmailRequest = billingInvoiceEmailRequestService.get(id);
        respond billingInvoiceEmailRequest
    }
}
