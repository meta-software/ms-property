package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.Account
import metasoft.property.core.AccountTerminateRequest
import metasoft.property.core.AccountTerminateRequestService

import static org.springframework.http.HttpStatus.*

@Secured(['IS_AUTHENTICATED_FULLY'])
class AccountTerminateRequestController {

    AccountTerminateRequestService accountTerminateRequestService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value=['ROLE_PERM_LEASE_REQUEST_READ'], httpMethod="GET")
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond accountTerminateRequestService.list(params), model:[accountTerminateRequestCount: accountTerminateRequestService.count()]
    }

    @Secured(value=['ROLE_PERM_LEASE_REQUEST_READ'], httpMethod="GET")
    def show(Long id) {
        AccountTerminateRequest accountTerminateRequest = accountTerminateRequestService.get(id);

        if(accountTerminateRequest == null) {
            notFound();
            return;
        }

        Account account = accountTerminateRequest.account
        respond accountTerminateRequest, model: [accountTerminateRequest: accountTerminateRequest, account: account]
    }

    @Secured(value=['ROLE_PERM_LEASE_REQUEST_WRITE'], httpMethod ="GET")
    def create() {
        AccountTerminateRequest accountTerminateRequest = new AccountTerminateRequest(params)
        respond accountTerminateRequest
    }

    @Secured(value=['ROLE_PERM_LEASE_REQUEST_WRITE'], httpMethod ="POST")
    def save(AccountTerminateRequest accountTerminateRequest) {
        if (accountTerminateRequest == null) {
            notFound()
            return
        }

        try {
            accountTerminateRequestService.save(accountTerminateRequest)
        } catch (ValidationException e) {
            respond e.errors, view:'create', status: UNPROCESSABLE_ENTITY
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accountTerminateRequest.label', default: 'AccountTerminateRequest'), accountTerminateRequest.id])
                redirect accountTerminateRequest
            }
            '*' { respond accountTerminateRequest, [status: CREATED] }
        }
    }

    @Secured(value=['ROLE_PERM_LEASE_REQUEST_WRITE'], httpMethod ="GET")
    def edit(Long id) {
        respond accountTerminateRequestService.get(id)
    }

    @Secured(value=['ROLE_PERM_LEASE_REQUEST_WRITE'], httpMethod ="DELETE")
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        accountTerminateRequestService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'accountTerminateRequest.label', default: 'AccountTerminateRequest'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    @Secured(['ROLE_PERM_LEASE_REQUEST_CHECK'])
    def makerChecker(AccountTerminateRequest accountTerminateRequest){

        if (accountTerminateRequest == null) {
            notFound();
            return;
        }

        try {
            accountTerminateRequestService.updateMakerChecker(accountTerminateRequest);
        } catch (ValidationException e) {
            respond accountTerminateRequest.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'account.label', default: 'Account'), accountTerminateRequest.id]);
                redirect accountTerminateRequest;
            }
            '*' { respond accountTerminateRequest, status: CREATED, view: "show" }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountTerminateRequest.label', default: 'AccountTerminateRequest'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
