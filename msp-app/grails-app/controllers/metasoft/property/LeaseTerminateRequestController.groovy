package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.core.Lease
import metasoft.property.core.LeaseTerminateRequestTmp
import metasoft.property.core.LeaseTerminateRequest
import metasoft.property.core.LeaseTerminateRequestService
import metasoft.property.core.MetasoftUtil

import static org.springframework.http.HttpStatus.*

@Slf4j
@Secured(['IS_AUTHENTICATED_FULLY'])
class LeaseTerminateRequestController {

    LeaseTerminateRequestService leaseTerminateRequestService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value=['ROLE_PERM_LEASE_READ'], httpMethod="GET")
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond leaseTerminateRequestService.list(params), model:[leaseTerminateRequestCount: leaseTerminateRequestService.count()]
    }

    @Secured(value=['ROLE_PERM_LEASE_READ'], httpMethod="GET")
    def show(Long id) {
        LeaseTerminateRequest leaseTerminateRequest = leaseTerminateRequestService.get(id);

        if(leaseTerminateRequest == null) {
            notFound();
            return;
        }

        Lease lease = leaseTerminateRequest.lease
        respond leaseTerminateRequest, model: [leaseTerminateRequest: leaseTerminateRequest, lease: lease]
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="GET")
    def create() {
        LeaseTerminateRequest leaseTerminateRequest = new LeaseTerminateRequest(params)
        respond leaseTerminateRequest
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="POST")
    def save(LeaseTerminateRequest leaseTerminateRequest) {
        if (leaseTerminateRequest == null) {
            notFound()
            return
        }

        try {
            leaseTerminateRequestService.save(leaseTerminateRequest)
        } catch (ValidationException e) {
            respond e.errors, view:'create', status: UNPROCESSABLE_ENTITY
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'leaseTerminateRequest.label', default: 'LeaseTerminateRequest'), leaseTerminateRequest.id])
                redirect leaseTerminateRequest
            }
            '*' { respond leaseTerminateRequest, [status: CREATED] }
        }
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="GET")
    def edit(Long id) {
        respond leaseTerminateRequestService.get(id)
    }

    @Secured(value=['ROLE_PERM_LEASE_WRITE'], httpMethod ="POST")
    /**
     * Tries to create a routineRequestTmp object and a Check entity pending approval from checkers.
     *
     * @return
     */
    def createRequest(LeaseTerminateRequestCommand terminateRequestCommand) {

        try {
            LeaseTerminateRequestTmp leaseTerminateRequestTmp = leaseTerminateRequestService.createRequest(terminateRequestCommand);
            respond leaseTerminateRequestTmp, status: ACCEPTED
        } catch (ValidationException e) {
            log.debug("error while creating leaseTerminateRequest entity: {}", e.message);
            respond(e.errors, status: UNPROCESSABLE_ENTITY);
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseTerminateRequest.label', default: 'LeaseTerminateRequest'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class LeaseTerminateRequestCommand {

    Lease lease;
    String actionReason; // the reason why the status must be in this new status
    Date actionDate;

    static constraints = {
        lease validator: {Lease val, obj, errors ->
            if(val != null && val.terminated) {
                errors.rejectValue('lease', 'lease.terminate.terminated', "The lease is already terminated")
            }

            if(val?.expired) {
                errors.rejectValue('lease', 'lease.terminate.expired', "The lease is already expired")
            }
        }
        actionReason maxSize: 1024
        actionDate validator: { value, obj, errors ->
            Date today = MetasoftUtil.today()
            if(value < today) {
                Object[] args = (Object[])[value];
                errors.rejectValue('actionDate', 'default.date.invalid.past', args, 'Date value should not be in the past')
            }
        }
    }

}