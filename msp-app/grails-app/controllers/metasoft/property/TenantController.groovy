package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.AccountCategory
import metasoft.property.core.AccountCheckService
import metasoft.property.core.AccountType
import metasoft.property.core.Address
import metasoft.property.core.CheckStatus
import metasoft.property.core.Lease
import metasoft.property.core.LeaseService
import metasoft.property.core.RentalUnit
import metasoft.property.core.Tenant
import metasoft.property.core.TenantService
import metasoft.property.core.Title

import static org.springframework.http.HttpStatus.*

@Secured(['IS_AUTHENTICATED_FULLY'])
class TenantController {

    TenantService tenantService
    LeaseService leaseService

    static allowedMethods = [save: "POST", update: "PUT", terminate: "PUT", delete: "DELETE"]

    @Secured(value=['ROLE_PERM_LANDLORD_READ'], httpMethod='GET')
    def app() {
        render view: 'vue-app';
    }

    def listOptions() {
        params['cat'] = 'tenant';
        List<Tenant> tenantList = tenantService.listOptions(params)
        respond tenantList, view: 'listOptions'
    }

    @Secured(value=['ROLE_PERM_TENANT_READ'], httpMethod='GET')
    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100)
        List<Tenant> tenantList = tenantService.listApproved(params);
        respond tenantList, model:[tenantList: tenantList, tenantCount: tenantService.countApproved(params)]
    }

    /**
     * Retrieve list of RentalUnits leased to the tenant with provided accountNumber
     *
     * @param accountNumber
     */
    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod='GET')
    def rentalUnits(String accountNumber) {
        if(accountNumber == null) {
            accountNumber = params['accountNumber'] as String;
        }

        List<RentalUnit> rentalUnits = tenantService.getRentalUnitsByTenantAccount(accountNumber)
        respond rentalUnits, model: [rentalUnits: rentalUnits], view: 'rentalUnits'
    }

    /**
     * Retrieve list of Leases for the tenant with provided accountNumber
     *
     * @param accountNumber
     */
    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod='GET')
    def leases(Integer max) {

        Tenant tenant = null;
        if(params?.accountId) {
            tenant = Tenant.get(params?.accountId)
        } else {
            tenant = Tenant.findByAccountNumber(params?.accountNumber)
        }

        if(tenant) {
            String accountNumber = tenant.accountNumber;
            List<Lease> leases = leaseService.getAllByTenantAccount(accountNumber)
            respond leases, model: [leases: leases], view: 'leases'
        } else {
            notFound();
        }
    }

    /**
     * Retrieve list of Leases for the tenant with provided accountNumber
     *
     * @param accountNumber
     */
    @Secured(value=['IS_AUTHENTICATED_FULLY'], httpMethod='GET')
    def tenantLeases(Long tenantId) {
        if(tenantId == null) {
            tenantId = params['tenantId'] as Long;
        }

        Tenant tenant = tenantService.get(tenantId);
        List<Lease> leases = leaseService.getAllByTenantAccount(tenant.accountNumber)

        respond leases, model: [leases: leases], view: 'leases'
    }

    @Secured(value=['ROLE_PERM_TENANT_READ'], httpMethod='GET')
    def show(Long id) {
        Tenant tenant = tenantService.get(id);

        if (tenant == null) {
            notFound()
            return
        }

        Long activeLeaseCount = tenantService.activeLeaseCount(tenant.id);

        BigDecimal accountBalance = tenantService.getAccountBalance(tenant);
        BigDecimal depositBalance = tenantService.getAccountDepositBalance(tenant);
        BigDecimal rentalBalance = tenantService.getRentalBalance(tenant);
        BigDecimal opcostBalance = tenantService.getOpcostBalance(tenant);

        tenant.link rel: 'self', href: g.createLink(absolute: true, controller:"tenant", action: 'show', params:[id: tenant.id])

        respond tenant, model:[tenant: tenant,
                               activeLeaseCount: activeLeaseCount,
                               bankAccount: tenant.bankAccount,
                               accountBalance: accountBalance,
                               rentalBalance: rentalBalance,
                               opcostBalance: opcostBalance,
                               depositBalance: depositBalance ], view: 'show'
    }

    @Secured(value=['ROLE_PERM_TENANT_WRITE'], httpMethod='POST')
    def save() {

        Map args = request.JSON ?: params;
        Tenant tenant = new Tenant();
        bindData(tenant, args);

        try {
            tenantService.create(tenant, args)
        } catch (ValidationException e) {
            respond tenant.errors, view:'create', model: [landlord: tenant]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'landlord.label', default: 'Landlord'), tenant.id])
                redirect controller: "landlord", id: tenant.id, action: "show"
            }
            '*' { respond tenant, status: CREATED, view: "show" }
        }
    }

    @Secured(value=['ROLE_PERM_TENANT_WRITE'], httpMethod='GET')
    def edit(Long id) {
        Tenant tenant = tenantService.getForEdit(id)

        respond tenant, model: [tenant: tenant]
    }

    @Secured(value=['ROLE_PERM_TENANT_WRITE'], httpMethod='PUT')
    def update(Long id) {
        Tenant tenant = tenantService.get(id);

        if (tenant == null) {
            notFound()
            return
        }

        Map args = request.JSON ?: params;

        try {
            tenantService.update(tenant, args)
        } catch (ValidationException e) {
            respond tenant.errors, model: [tenant: tenant], view: 'create';
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'tenant.label', default: 'Tennant'), tenant.id])
                redirect action: 'show', id: tenant.id
            }
            '*'{ respond tenant, model: [tenant: tenant], status: OK }
        }
    }

    @Secured(value=['ROLE_PERM_TENANT_WRITE'], httpMethod='DELETE')
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        tenantService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'tenant.label', default: 'Tennant'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    @Secured(value=['ROLE_PERM_TENANT_WRITE'], httpMethod='PUT')
    def terminate(Long id) {
        if (id == null) {
            notFound()
            return
        }

        Tenant tenant = tenantService.terminate(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.terminated.message', args: [message(code: 'tenant.label', default: 'Tenant'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ respond tenant, status: OK, view: 'show' }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tenant.label', default: 'Tennant'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(value=['ROLE_PERM_TENANT_CHECK'], httpMethod='PUT')
    def makerChecker(Tenant tenant){

        if (tenant == null) {
            notFound();
            return;
        }

        try {
            tenantService.updateMakerChecker(tenant);
        } catch (ValidationException e) {
            respond tenant.errors, status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'tenant.label', default: 'Tenant'), tenant.id]);
                redirect tenant;
            }
            '*' { respond tenant, status: CREATED, view: "show" }
        }
    }

    def fetchList() {
        params.max = Math.min(max ?: 10, 100)

        List<Tenant> tenantList = tenantService.list(params);

        respond tenantList, view: '/account/fetchList', model:[accountList: tenantList]
    }
}

class TenantCommand implements Validateable {

    Map title;
    Map accountType; /* Individual, Company */
    Map accountCategory; /* Tenant, Landlord, Suspense, Trust, etc... */
    Map bankAccount;

    Long accountTypeId;

    String bankAccountNumber;
    String accountNumber;
    String accountName;

    Address physicalAddress;
    Address postalAddress;
    Address businessAddress;
    String phoneNumber;
    String faxNumber;
    String emailAddress;
    String vatNumber;

    static constraints = {
        accountNumber nullable: true
        physicalAddress nullable: true
        postalAddress nullable: true
        businessAddress nullable: true
        phoneNumber nullable: true
        faxNumber nullable: true
        title nullable: true
        accountCategory nullable: true
        vatNumber nullable: true
        emailAddress nullable: true
        bankAccountNumber nullable: true
    }

    Object asType(Class type) {
        if(type == Tenant.class) {
            Tenant tenant = new Tenant(this.properties);
            return tenant;
        }
    }
}
