package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.json.JsonSlurper
import metasoft.property.core.PropertyManager
import metasoft.property.core.PropertyManagerCheck
import metasoft.property.core.PropertyManagerCheckService

import static org.springframework.http.HttpStatus.*

@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class PropertyManagerCheckController {

    PropertyManagerCheckService propertyManagerCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond propertyManagerCheckService.list(params), model:[propertyManagerCheckCount: propertyManagerCheckService.count()]
    }

    def show(Long id) {
        PropertyManagerCheck propertyManagerCheck = propertyManagerCheckService.get(id);
        PropertyManager propertyManager = propertyManagerCheck.entity;

        def json = new JsonSlurper().parseText(propertyManagerCheck.jsonData);
        propertyManager.setProperties(json);

        respond ([propertyManagerCheck: propertyManagerCheck, propertyManager: propertyManager])
    }

    def approve(Long id) {

        PropertyManagerCheck propertyManagerCheck = propertyManagerCheckService.get(id);

        if (propertyManagerCheck == null) {
            notFound()
            return
        }

        try {
            propertyManagerCheckService.approve(propertyManagerCheck)
        } catch (ValidationException e) {
            respond propertyManagerCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyManagerCheck.label', default: 'PropertyManagerCheck'), propertyManagerCheck.id])
                redirect propertyManagerCheck
            }
            '*'{ respond propertyManagerCheck, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propertyManagerCheck.label', default: 'PropertyManagerCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
