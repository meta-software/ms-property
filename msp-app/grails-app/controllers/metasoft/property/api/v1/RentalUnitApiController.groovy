package metasoft.property.api.v1

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.RentalUnit
import metasoft.property.core.RentalUnitService
import metasoft.property.core.Tenant

@Secured(value=['ROLE_API_USER'])
class RentalUnitApiController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [newRentalUnits: "GET"]

    static namespace = "v1";

    RentalUnitService rentalUnitService

    def newRentalUnits() {
        List<RentalUnit> rentalUnitList = rentalUnitService.newRentalUnits();
        Long unitsCount = rentalUnitList.size();

        render view: "/rentalUnit/newRentalUnits", model:[rentalUnitList: rentalUnitList, unitsCount: unitsCount]
    }

}
