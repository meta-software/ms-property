package metasoft.property.api.v1

import groovy.util.logging.Slf4j
import metasoft.property.api.TransactionRequest
import metasoft.property.core.MetasoftUtil
import metasoft.property.core.Tenant

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.Lease

@Slf4j
@Secured(value=['IS_AUTHENTICATED_FULLY', "hasRole('ROLE_API_USER')"])
class TransactionApiController {

    static namespace = "v1";

    static allowedMethods = [bankDepositPayment: "POST", receiptRtgs: "POST", refund: "POST", reversal: "POST"]
    static responseFormats = ['json', 'xml']

    TransactionApiService transactionApiService

    def bankDepositPayment() {

        Map requestData = request.JSON ?: params;

        TransactionRequestCommand transactionApiRequestCommand = new TransactionRequestCommand()
        bindData(transactionApiRequestCommand, requestData)

        try {
            Map requestResult = transactionApiService.createBankDepositPayment(transactionApiRequestCommand.properties, requestData);

            if(TransactionRequestStatusCodeEnum.SUCCESS == requestResult['requestStatus']) {
                render view: '/transactionApiRequest/responseSuccess', status: CREATED, model: requestResult
            } else {
                render view: '/transactionApiRequest/responseError', status: UNPROCESSABLE_ENTITY, model: requestResult
            }
        } catch (ValidationException ve) {
            log.error(ve.message);
            respond ve.errors, [status: INTERNAL_SERVER_ERROR]
        } catch(RuntimeException re) {
            log.error("Error while processing request {}", requestData)
            re.printStackTrace();
        }
    }

    def receiptRtgs() {

        Map requestData = (request.JSON ?: params) as Map;
        log.info("request to create RTGS [{}]", requestData);
        Map requestCopy = MetasoftUtil.copyRequest(requestData);

        TransactionApiRequestCommand transactionApiRequestCommand = new TransactionApiRequestCommand();
        bindData(transactionApiRequestCommand, requestData);

        try {
            Map requestResult = transactionApiService.createRtgsPayment(transactionApiRequestCommand, requestCopy);

            if(TransactionRequestStatusCodeEnum.SUCCESS == requestResult['requestStatus']) {
                render view: '/transactionApiRequest/responseSuccess', status: CREATED, model: requestResult;
            } else {
                render view: '/transactionApiRequest/responseError', status: UNPROCESSABLE_ENTITY, model: requestResult;
            }

        } catch (ValidationException ve) {
            respond ve.errors, [status: INTERNAL_SERVER_ERROR];
        } catch(RuntimeException re) {
            render status: INTERNAL_SERVER_ERROR, text: re.message
        }
    }
}

/**
 */
class TransactionRequestCommand implements Validateable {

    String accountNumber;   // the lease account number
    String accountName;     // the tenant account name, supplied to flexcube
    String transactionTypeCode; // the transaction type  [rent, deposit, etc...]
    String transactionReference;    // the flexcube transaction reference
    String accountBr;               // the cbz account account branch receiving
    String accountNumberBr;         // the cbz account number where funds are deposited into.
    String description;             // free text description of transaction
    Date transactionDate;           // the date of transaction
    BigDecimal amount;              // the transaction amount
    String rentalUnitName;
    String rentalUnitCode;

    static constraints = {
        accountBr nullable: false
        transactionTypeCode nullable: true
        accountNumber nullable: false, validator: { value, object, errors ->
            //@todo check if the accountNumber is a valid account number and return as such
            if( !Lease.findByLeaseNumber(value) ) {
                Object[] errorArgs = [value];
                errors.rejectValue('accountNumber', 'transactionApi.accountNumber.notExist', errorArgs, "The specified lease account number '${value}' does not exist")
            }
        }
        transactionReference nullable: false, validator: { value, object, errors ->
            if(TransactionRequest.where{ transactionReference == value }.count() > 0) {
                Object[] errorArgs = [value];
                errors.rejectValue('transactionReference', 'transactionApi.transactionReference.not.unique', errorArgs, "Provided Transaction Reference already exists")
            }
        }
        description nullable: false
        transactionDate nullable: false, validator: { value, object, errors ->
            Date today = new Date().clearTime();

            //@todo: check that date is not in the future
        }
        amount min: 0.0
    }

}

/**
 */
class TransactionApiRequestCommand implements Validateable {

    String accountNumber;   // the flexcube bank account number
    String accountName;     // the flexcube configured account name
    String transactionReference;    // the flexcube transaction reference
    String narration;               // free text description of transaction
    Date transactionDate;           // the date of transaction
    BigDecimal amount;              // the transaction amount
    String currency;
    String trustAccountName;
    String trustAccountNumber;

    static constraints = {
        accountNumber nullable: false, validator: { value, object, errors ->
            //@todo check if the accountNumber is a valid account number and return as such
            if( !Tenant.countByBankAccountNumber(value) ) {
                Object[] errorArgs = [value];
                errors.rejectValue('accountNumber', 'transactionApi.bankAccountNumber.notExist', errorArgs, "No tenant found for provided bank account number [${value}]")
            }
        }
        transactionReference nullable: false
        narration nullable: false
        transactionDate nullable: false, validator: { value, object, errors ->
            Date today = new Date().clearTime();

            if(transactionDate.after(today)) {
                Object[] errorArgs = [value];
                errors.rejectValue('transactionDate', 'transactionApi.date.infuture', errorArgs, "Transaction date cannot be in future")
            }
        }
        amount min: 0.0
    }

}
