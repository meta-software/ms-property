package metasoft.property.api.v1

import com.google.common.collect.ImmutableMap
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import metasoft.property.TenantCommand
import metasoft.property.core.*
import org.apache.commons.lang.SerializationUtils
import org.grails.web.json.JSONObject
import org.grails.web.json.parser.JSONParser

import static org.springframework.http.HttpStatus.CREATED

@Secured(value=['ROLE_API_USER'])
class TenantApiController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [newAccounts: "GET"]

    static namespace = "v1";

    TenantService tenantService

    @Secured(value=['ROLE_API_USER'])
    def newAccounts() {

        List<Lease> leases = tenantService.shareTenantLeases();
        Long recordCount = leases.size();

        render view: "/tenant/newTenantLeases", model:[leases: leases, tenantCount: recordCount]
    }

    def create() {

        HashMap args = request.JSON ?: params;
        Map jsonRequest = new JsonSlurper().parseText(new JsonBuilder(args).toString() );

        def v = jsonRequest == args

        args['accountType'] = ['id': args['accountTypeId']]
        args['bankAccount']['bank'] = [id: args['bankAccount']?.bankId];

        TenantCommandApi tenantCommand = new TenantCommandApi();
        bindData(tenantCommand, args);
        tenantCommand.bankAccountNumber = tenantCommand.fileNumber;

        if(!tenantCommand.validate()) {
            respond tenantCommand.errors, view:'create', model: [tenant: tenantCommand]
            return
        }

        Tenant tenant = tenantCommand as Tenant;

        try {
            tenantService.create(tenant, jsonRequest, [autoCommit: true])
        } catch (ValidationException e) {
            respond e.errors, view:'create', model: [tenant: tenant]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'landlord.label', default: 'Landlord'), tenant.id])
                redirect controller: "tenant", id: tenant.id, action: "show"
            }
            '*' { respond tenant, status: CREATED, view: "show" }
        }

    }

}

class TenantCommandApi extends TenantCommand {

    String fileNumber;
    Long accountTypeId;

    static constraints = {
        importFrom(TenantCommand, exclude: ['bankAccountNumber','accountType'])
        bankAccountNumber nullable: false, validator: { value, command, errors ->

            if(Tenant.countByBankAccountNumber(value) != 0) {
                Object[] args = [value]
                errors.rejectValue('bankAccountNumber', 'tenant.bankAccountNumber.exists', args, "Tenant with provided bank account number '${value}' already exists")
            }
        }
        accountTypeId nullable: false, validator: { value, command, errors ->
            if(AccountType.exists(value)==false) {
                errors.rejectValue('accountTypeId', 'tenant.accountTypeId.notExists', "Invalid account type option selected")
            }
        }
    }

    @Override
    Object asType(Class type) {
        if(type == Tenant.class) {
            this.accountType['id'] = this.accountTypeId;
            return super.asType(type);
        }
    }

}