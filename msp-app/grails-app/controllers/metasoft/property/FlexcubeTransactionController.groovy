package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.api.FlexcubeTransaction
import metasoft.property.api.v1.FlexcubeTransactionService
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(['IS_AUTHENTICATED_FULLY'])
class FlexcubeTransactionController {

    FlexcubeTransactionService flexcubeTransactionService;

    static allowedMethods = [post: "POST"]

    def app() {
        render view: 'app'
    }

    def pending(Integer max) {
        params.max = Math.min(max ?: 15, 100)
        respond flexcubeTransactionService.pending(params), view: 'index';
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100)
        respond flexcubeTransactionService.posted(params);
    }

    def show(Long id) {
        FlexcubeTransaction flexcubeTransaction = flexcubeTransactionService.get(id);

        if(!flexcubeTransaction){
            notFound();
            return;
        }

        respond flexcubeTransaction;
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'flexcubeTransaction.label', default: 'FlexcubeTransaction'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}
