package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.AccountCategory
import metasoft.property.core.AccountType
import metasoft.property.core.Provider
import metasoft.property.core.ProviderService
import metasoft.property.core.Tenant
import metasoft.property.core.Title

import static org.springframework.http.HttpStatus.*

@Secured(['IS_AUTHENTICATED_FULLY'])
class ProviderController {

    ProviderService providerService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", makerChecker: "PUT"]
    
    def fetchList(Integer max) {
        params.max = Math.min(max ?: 10, 100);

        List<Provider> providerList = providerService.fetchList(params);

        respond providerList, view: 'fetchList', model:[providerList: providerList]
    }

    def listOptions() {
        params['cat'] = 'provider';
        List<Provider> providerList = providerService.listOptions(params)
        respond providerList, view: 'listOptions'
    }

    @Secured(value=['ROLE_PERM_LANDLORD_READ'], httpMethod = 'GET')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100);
        params['checkStatus'] = params['checkStatus'] ?: 'all';

        List<Provider> providerList = providerService.listApproved(params);
        respond providerList, model:[providerList: providerList, providerCount: providerService.count()]
    }

    @Secured(value=['ROLE_PERM_LANDLORD_READ'], httpMethod='GET')
    def show(Long id) {
        Provider provider = providerService.get(id)

        def accountStats = providerService.getAccountStats(id)
        def currentStats = providerService.getCurrentAccountStats(id)

        respond provider, model:[ provider: provider
                                  ,accountStats: accountStats
                                  ,currentStats: currentStats
                                  ,bankAccount: provider.bankAccount ], view: 'show'
    }

    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='GET')
    def create() {
        Provider provider = new Provider(params);

        respond provider, model: [provider: provider], view: 'create';
    }

    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='POST')
    def save() {
        
        Map args = request.JSON ?: params;
        Provider provider = new Provider();
        bindData(provider, args);

        try {
            provider = providerService.create(provider, request.JSON ?: params)
        } catch (ValidationException e) {
            respond provider.errors, model: [provider: provider], view: 'create';
            return
        }

        println("is this successful ???");

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'provider.label', default: 'Tennant'), provider.id])
                redirect action: 'show', id: provider.id
            }
            '*' {
                provider.link rel: 'self', href: g.createLink(absolute: true, controller:"provider", action: 'show', params:[id: provider.id])
                respond provider, status: CREATED, view: '/provider/show', model: [provider: provider]
            }
        }
    }

    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='GET')
    def edit(Long id) {
        Provider account = providerService.getForEdit(id)

        respond account, model: [provider: account]
    }

    @Secured(['ROLE_PERM_LANDLORD_WRITE'])
    def update(Long id) {
        Provider provider = providerService.get(id);

        if (provider == null) {
            notFound()
            return
        }

        try {
            providerService.update(provider, request.JSON ?: params)
        } catch (ValidationException e) {
            respond provider.errors, model: [provider: provider], view: 'create';
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'provider.label', default: 'Tennant'), provider.id])
                redirect action: 'show', id: provider.id
            }
            '*'{ respond provider, model: [provider: provider], status: ACCEPTED }
        }
    }
    
    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='DELETE')
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        providerService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'provider.label', default: 'Provider'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'provider.label', default: 'Provider'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(value=['ROLE_PERM_LANDLORD_CHECK'], httpMethod="PUT")
    def makerChecker(Provider provider){

        if (provider == null) {
            notFound();
            return;
        }

        try {
            providerService.updateMakerChecker(provider);
        } catch (ValidationException e) {
            respond provider.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'provider.label', default: 'Provider'), provider.id]);
                redirect provider;
            }
            '*' { respond provider, status: CREATED, view: "show" }
        }
    }

}

class ProviderCommand implements Validateable {

    Title title;
    AccountType accountType; /* Individual, Company */
    AccountCategory accountCategory; /* Provider, Landlord, Provider */

    String accountNumber;
    String accountName;

    String physicalAddress;
    String postalAddress;
    String businessAddress;
    String phoneNumber;
    String faxNumber;
    String emailAddress;
    String bpNumber;
    String vatNumber;

    static constraints = {
        accountNumber nullable: true
        physicalAddress nullable: true
        postalAddress nullable: true
        businessAddress nullable: true
        phoneNumber nullable: true
        faxNumber nullable: true
        emailAddress nullable: true
        title nullable: true
        bpNumber nullable: true
    }

    Object asType(Class type) {
        if(type == Provider.class) {
            Provider provider = new Provider(this.properties);

            return provider;
        }
    }
}
