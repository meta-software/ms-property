package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.PendingReceipt
import metasoft.property.core.PendingReceiptService
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class PendingReceiptController {

    PendingReceiptService pendingReceiptService;

    static allowedMethods = [post: "PUT", update: "PUT", allocate: "POST"]

    def index() {
        render view: '/pendingReceipt/app'
    }

    def clearAllocations(Long id) {
        try{
            pendingReceiptService.clearAllocations(id);
            render status: OK
        } catch(ValidationException e) {
            respond e.errors, status: NOT_ACCEPTABLE
        }
    }

    def unallocated() {
        params.max = params.max ?: 20;
        List<PendingReceipt> pendingReceipts = pendingReceiptService.listUnallocated(params);
        respond pendingReceipts, view: '/pendingReceipt/unallocated'
    }

    def post(Long id) {

        try {
            def postResponse = pendingReceiptService.postReceipt(id);

            render text: "Receipt Batch posted successfully", status: ACCEPTED
        } catch(ValidationException ve) {
            log.error("an error occured while posting the pending-receipt ${id}: ", ve)
            respond ve.errors, status: NOT_ACCEPTABLE
        }

    }

    def show(Long id) {
        PendingReceipt pendingReceipt = pendingReceiptService.get(id)

        if(!pendingReceipt) {
            notFound();
            return;
        }

        respond pendingReceipt, model: [pendingReceipt: pendingReceipt], view: '/pendingReceipt/show'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'pendingReceipt.label', default: 'PendingReceipt'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'  { render status: NOT_FOUND }
            json { render status: NOT_FOUND }
        }
    }
}
