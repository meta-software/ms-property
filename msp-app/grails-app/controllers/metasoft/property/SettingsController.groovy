package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.Settings
import metasoft.property.core.SettingsService

@Secured(value=['ROLE_ADMIN'])
class SettingsController {

    SettingsService settingsService;

    static allowedMethods = [save: "PUT"]

    def list(Integer max) {
        List<Settings> settingsList = settingsService.list(params);
        respond settingsList, model: [settingsList: settingsList]
    }

    def app() {}

    def index() {}
}
