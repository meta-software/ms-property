package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.AmountType
import metasoft.property.core.Country

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class CountryController {

    static allowedMethods = []

    def listOptions() {
        Map args = [:];
        List<Country> countryList = Country.list(args)

        respond countryList
    }

}
