package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BulkInvoice
import metasoft.property.core.BulkInvoiceItem
import metasoft.property.core.BulkInvoiceService
import metasoft.property.core.TransactionType

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class BulkInvoiceController {

    BulkInvoiceService bulkInvoiceService

    static allowedMethods = [save: "POST", approve: "PUT", cancel: "PUT", reject: "PUT", delete: "DELETE"]

    def app() {
        render view: 'app'
    }

    def inbox(Integer max) {
        forward(action: 'index', max: max, params: params)
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkInvoiceService.listInbox(params), model:[bulkInvoiceCount: bulkInvoiceService.count(params)]
    }

    def outbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkInvoiceService.listOutbox(params), model:[bulkInvoiceCount: bulkInvoiceService.count()]
    }

    def trash(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkInvoiceService.listCancelled(params), model:[bulkInvoiceCount: bulkInvoiceService.count()]
    }

    def show(Long id) {
        BulkInvoice bulkInvoice = bulkInvoiceService.get(id)
        Set<BulkInvoiceItem> bulkInvoiceItems = bulkInvoice.bulkInvoiceItems;

        respond (bulkInvoice , [bulkInvoiceItems: bulkInvoiceItems])
    }

    def create() {
        respond new BulkInvoice(params)
    }

    def save() {

        BulkInvoice bulkInvoice = new BulkInvoice()
        bindData(bulkInvoice, request.JSON)

        if (bulkInvoice == null) {
            notFound()
            return
        }

        try {
            bulkInvoiceService.create(bulkInvoice)
        } catch (ValidationException e) {
            respond e.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bulkInvoice.label', default: 'BulkInvoice'), bulkInvoice.id])
                redirect bulkInvoice
            }
            '*' { respond bulkInvoice, [status: CREATED] }
        }
    }

    def simulateAllocations() {
        def postData = request.JSON ?: params;

        BulkInvoice bulkInvoice = new BulkInvoice()
        bindData(bulkInvoice, postData);

        bulkInvoice.transactionType = TransactionType.findByTransactionTypeCode('IV');

        List<BulkInvoiceItem> bulkInvoiceItems = bulkInvoiceService.generateInvoiceItems(bulkInvoice.property
                ,bulkInvoice.amount
                ,bulkInvoice.transactionDate
                ,bulkInvoice.transactionReference
                ,bulkInvoice.billingCycle
                ,bulkInvoice.subAccount);

        bulkInvoice.bulkInvoiceItems = bulkInvoiceItems;

        respond bulkInvoice, view: 'create'
    }

    def edit(Long id) {
        respond bulkInvoiceService.get(id)
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        bulkInvoiceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'bulkInvoice.label', default: 'BulkInvoice'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bulkInvoice.label', default: 'BulkInvoice'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }

    def approve(Long id){

        BulkInvoice bulkInvoice = bulkInvoiceService.get(id);

        if (bulkInvoice == null) {
            notFound();
            return;
        }

        try {
            bulkInvoiceService.approve(bulkInvoice);
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkInvoice.label', default: 'BulkInvoice'), bulkInvoice.id]);
                redirect bulkInvoice;
            }
            '*' { respond bulkInvoice, status: CREATED, view: "show" }
        }
    }
    
    def reject(Long id){

        BulkInvoice bulkInvoice = bulkInvoiceService.get(id);

        if (bulkInvoice == null) {
            notFound();
            return;
        }

        try {
            bulkInvoiceService.reject(bulkInvoice, request.JSON ?: params);
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkInvoice.label', default: 'BulkInvoice'), bulkInvoice.id]);
                redirect bulkInvoice;
            }
            '*' { respond bulkInvoice, status: ACCEPTED, view: "show" }
        }
    }

    def cancel(Long id){

        BulkInvoice bulkInvoice = bulkInvoiceService.get(id);

        if (bulkInvoice == null) {
            notFound();
            return;
        }

        try {
            bulkInvoiceService.cancel(bulkInvoice, request.JSON ?: params);
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkInvoice.label', default: 'BulkInvoice'), bulkInvoice.id]);
                redirect bulkInvoice;
            }
            '*' { respond bulkInvoice, status: ACCEPTED, view: "show" }
        }
    }

}
