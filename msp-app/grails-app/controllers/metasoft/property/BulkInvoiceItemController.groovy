package metasoft.property

import metasoft.property.core.BulkInvoiceItem
import metasoft.property.core.BulkInvoiceItemService
import metasoft.property.core.BulkInvoiceService

class BulkInvoiceItemController {

    BulkInvoiceItemService bulkInvoiceItemService
    BulkInvoiceService bulkInvoiceService;

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long bulkInvoiceId, Integer max) {
        List<BulkInvoiceItem> bulkInvoiceItems = bulkInvoiceItemService.listByBulkInvoice(bulkInvoiceId, params);

        //params.max = Math.min(max ?: 10, 100)
        respond bulkInvoiceItems, model:[bulkInvoiceItemCount: bulkInvoiceItemService.countByBulkInvoice(bulkInvoiceId)]
    }
}
