package metasoft.property

import grails.plugin.springsecurity.SpringSecurityService
import groovy.transform.CompileStatic
import metasoft.property.core.SecUser

@CompileStatic
class MetasoftInterceptor {

    SpringSecurityService springSecurityService;

    MetasoftInterceptor() {
        matchAll()
                .excludes(controller:"login")
                .excludes(controller:"logout")
                .excludes(controller: 'profile', actionName: 'initLogin')
    }

    boolean before() {

        SecUser secUser = null;
        SecUser.withTransaction {
            //@todo: look to find a way to skip having to access database all the time.
            secUser = springSecurityService.currentUser as SecUser;
        }

        if(springSecurityService.isLoggedIn() && secUser.initialLogin) {
            redirect(action: 'init-login', controller: 'profile');
            return false
        }

        return true
    }

    boolean after() {

        // disable content caching as much as possible.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=7776000");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 1L);
        true
    }

    void afterView() {
        // no-op
    }
}
