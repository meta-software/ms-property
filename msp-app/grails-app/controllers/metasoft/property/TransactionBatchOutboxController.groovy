package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.TransactionBatchCheck
import metasoft.property.core.TransactionBatchCheckService
import metasoft.property.core.TransactionBatchTmp

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class TransactionBatchOutboxController {

    TransactionBatchCheckService transactionBatchCheckService

    static allowedMethods = [cancel: "PUT", delete: "DELETE"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<TransactionBatchCheck> transactionBatchCheckList = transactionBatchCheckService.listOutbox(params)
        respond transactionBatchCheckList, model:[transactionBatchCheckCount: transactionBatchCheckService.count()]
    }

    def show(Long id) {
        TransactionBatchCheck transactionBatchCheck = transactionBatchCheckService.get(id)
        TransactionBatchTmp transactionBatchTmp = transactionBatchCheckService.getEntitySource(transactionBatchCheck);
        render (model: [transactionBatchCheck: transactionBatchCheck, transactionBatchTmp: transactionBatchTmp], view: 'show')
    }

    def cancel(Long id) {
        TransactionBatchCheck transactionBatchCheck = transactionBatchCheckService.get(id);

        if (transactionBatchCheck == null) {
            notFound()
            return
        }

        try {
            transactionBatchCheckService.cancel(transactionBatchCheck, (request.JSON ?: params))
        } catch (ValidationException e) {
            respond transactionBatchCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'transactionBatchCheck.label', default: 'TransactionBatchCheck'), transactionBatchCheck.id])
                redirect transactionBatchCheck
            }
            '*'{ respond transactionBatchCheck, status: ACCEPTED }
        }
    }
    
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'transactionBatchCheck.label', default: 'TransactionBatchCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
