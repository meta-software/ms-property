package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.api.ReceiptRequest
import metasoft.property.core.*

import static org.springframework.http.HttpStatus.*

@Slf4j
@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ReceiptAllocationController {

    ReceiptAllocationService receiptAllocationService;

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def listByReceipt(Long receiptId, Integer max) {
        params.max = Math.min(max ?: 15, 50)

        List<PendingReceiptAllocation> receiptRequestAllocations = receiptAllocationService.findAllByReceipt(receiptId, params);
        respond receiptRequestAllocations
    }

    def save(ReceiptAllocationCommand allocationCommand) {

        try {
            PendingReceiptAllocation receiptAllocation =  receiptAllocationService.saveAllocation(allocationCommand.pendingReceiptId, allocationCommand);
            respond receiptAllocation, status: CREATED
        } catch (ValidationException ve) {
            flash.message = "Error while processing allocation"
            respond ve.errors, status: NOT_ACCEPTABLE
        }
    }

    def delete(Long id) {
        try{
            receiptAllocationService.delete(id);
        } catch(RuntimeException e) {
            render status: NOT_ACCEPTABLE
            return;
        }

        render status: NO_CONTENT
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'receiptAllocation.label', default: 'PendingReceiptAllocation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'  { render status: NOT_FOUND }
            json { render status: NOT_FOUND }
        }
    }
}

class ReceiptAllocationCommand {

    Long pendingReceiptId;
    Long leaseId;
    BigDecimal allocatedAmount;
    Long subAccountId;

    static constraints = {
        pendingReceiptId nullable: false, validator: { value, obj, errors ->
            if(!PendingReceipt.exists(value)) {
                errors.rejectValue("pendingReceiptId", "receiptRequestAllocation.pendingReceipt.notexist", "The selected Pending Receipt was not found");
            }
        }
        leaseId nullable: false, validator: { value, obj, errors ->
            if(!Lease.exists(value)) {
                errors.rejectValue("leaseId", "receiptRequestAllocation.lease.notexist", "Invalid Lease selected");
            }
        }
        subAccountId nullable: false, validator: { value, obj, errors ->
            if(!SubAccount.exists(value)) {
                errors.rejectValue("subAccountId", "receiptRequestAllocation.subAccount.notexist", "Invalid sub-account selected");
            }
        }
        allocatedAmount min: 0.0
    }


    Object asType(Class type) {
        if(type == PendingReceiptAllocation.class) {
            PendingReceiptAllocation receiptRequestAllocation = new PendingReceiptAllocation();
            receiptRequestAllocation.lease = Lease.load(this.leaseId);
            receiptRequestAllocation.pendingReceipt = PendingReceipt.load(this.pendingReceiptId);
            receiptRequestAllocation.subAccount = SubAccount.load(this.subAccountId);
            receiptRequestAllocation.allocatedAmount = this.allocatedAmount;
            return receiptRequestAllocation;
        }
        return null;
    }
}
