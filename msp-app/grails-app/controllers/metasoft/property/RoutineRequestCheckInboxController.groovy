package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.RoutineRequestCheck
import metasoft.property.core.RoutineRequestCheckService

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED
import static org.springframework.http.HttpStatus.OK

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class RoutineRequestCheckInboxController {

    RoutineRequestCheckService routineRequestCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<RoutineRequestCheck> routineRequestCheckList = routineRequestCheckService.listInbox(params)

        render view: 'index', model: [routineRequestCheckList: routineRequestCheckList]
    }

    def show(Long id) {
        RoutineRequestCheck routineRequestCheck = routineRequestCheckService.get(id)

        if(!routineRequestCheck) {
            notFound();
            return;
        }

        render model: [routineRequestCheck: routineRequestCheck], view: 'show'
    }

    def reject(Long id) {
        RoutineRequestCheck routineRequestCheck = routineRequestCheckService.get(id);

        if (routineRequestCheck == null) {
            notFound()
            return
        }

        try {
            routineRequestCheckService.reject(routineRequestCheck, request.JSON)
        } catch (ValidationException e) {
            respond routineRequestCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'routineRequestCheck.label', default: 'RoutineRequestCheck'), routineRequestCheck.id])
                redirect routineRequestCheck
            }
            '*'{ respond routineRequestCheck, status: OK }
        }
    }

    def approve(Long id) {
        RoutineRequestCheck routineRequestCheck = routineRequestCheckService.get(id);

        if (routineRequestCheck == null) {
            notFound()
            return
        }

        try {
            routineRequestCheckService.approve(routineRequestCheck)
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'routineRequestCheck.label', default: 'RoutineRequestCheck'), routineRequestCheck.id])
                redirect routineRequestCheck
            }
            '*'{ respond routineRequestCheck, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'routineRequestCheck.label', default: 'RoutineRequestCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
