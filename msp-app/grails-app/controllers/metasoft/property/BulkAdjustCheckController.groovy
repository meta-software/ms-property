package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BulkAdjust
import metasoft.property.core.BulkAdjustCheck
import metasoft.property.core.BulkAdjustCheckService

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class BulkAdjustCheckController {

    BulkAdjustCheckService bulkAdjustCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT", cancel: "PUT"]

    def inbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<BulkAdjustCheck> bulkAdjustCheckList = bulkAdjustCheckService.listInbox(params)
        respond bulkAdjustCheckList, model:[bulkAdjustCheckCount: bulkAdjustCheckService.count(params)]
    }

    def outbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<BulkAdjustCheck> bulkAdjustCheckList = bulkAdjustCheckService.listOutbox(params)
        respond bulkAdjustCheckList, model:[bulkAdjustCheckCount: bulkAdjustCheckService.count()]
    }

    def showOutbox(Long id){
        BulkAdjustCheck bulkAdjustCheck = bulkAdjustCheckService.get(id)
        BulkAdjust bulkAdjust = bulkAdjustCheckService.getEntity(bulkAdjustCheck);
        respond (bulkAdjustCheck, model: [bulkAdjust: bulkAdjust], view: 'showOutbox')
    }

    def show(Long id) {
        BulkAdjustCheck bulkAdjustCheck = bulkAdjustCheckService.get(id)
        BulkAdjust bulkAdjust = bulkAdjustCheckService.getEntity(bulkAdjustCheck);
        respond ([bulkAdjustCheck: bulkAdjustCheck, bulkAdjust: bulkAdjust])
    }

    def approve(Long id) {
        BulkAdjustCheck bulkAdjustCheck = bulkAdjustCheckService.get(id);

        if (bulkAdjustCheck == null) {
            notFound()
            return
        }

        try {
            bulkAdjustCheckService.approve(bulkAdjustCheck)
        } catch (ValidationException e) {
            respond bulkAdjustCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkAdjustCheck.label', default: 'BulkAdjustCheck'), bulkAdjustCheck.id])
                redirect bulkAdjustCheck
            }
            '*'{ respond bulkAdjustCheck, [status: ACCEPTED] }
        }
    }
    
    def reject(Long id) {
        BulkAdjustCheck bulkAdjustCheck = bulkAdjustCheckService.get(id);

        if (bulkAdjustCheck == null) {
            notFound()
            return
        }

        try {
            bulkAdjustCheckService.reject(bulkAdjustCheck, request.JSON)
        } catch (ValidationException e) {
            respond bulkAdjustCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkAdjustCheck.label', default: 'BulkAdjustCheck'), bulkAdjustCheck.id])
                redirect bulkAdjustCheck
            }
            '*'{ respond bulkAdjustCheck, status: ACCEPTED }
        }
    }

    def cancel(Long id) {
        BulkAdjustCheck bulkAdjustCheck = bulkAdjustCheckService.get(id);

        if (bulkAdjustCheck == null) {
            notFound()
            return
        }

        try {
            bulkAdjustCheckService.cancel(bulkAdjustCheck, request.JSON)
        } catch (ValidationException e) {
            respond bulkAdjustCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkAdjustCheck.label', default: 'BulkAdjustCheck'), bulkAdjustCheck.id])
                redirect bulkAdjustCheck
            }
            '*'{ respond bulkAdjustCheck, status: ACCEPTED }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bulkAdjustCheck.label', default: 'BulkAdjustCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
