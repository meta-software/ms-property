package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.InvoiceService

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class InvoiceController {

    InvoiceService invoiceService;

    static allowedMethods = []

    def list() {

        String accountNumber = params?.accountNumber;

        def invoiceList = invoiceService.listInvoicesByAccount(accountNumber)

        //def value = [results: invoiceList]
        //render value as JSON
        respond invoiceList, view: 'index', model: [invoiceList: invoiceList]
    }

}
