package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.Bank
import metasoft.property.core.BankService

import static org.springframework.http.HttpStatus.*

class BankController {

    BankService bankService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def index() {
        render view: 'index'
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bankService.list(params)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def listOptions(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bankService.listOptions(params)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def create() {
        respond new Bank(params)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def save(Bank bank) {
        if (bank == null) {
            notFound()
            return
        }

        try {
            bankService.save(bank)
        } catch (ValidationException e) {
            respond bank.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bank.label', default: 'Bank'), bank.id])
                redirect controller: 'bank', action: 'index'
            }
            'json' { respond bank, [status: CREATED] }
            '*' { respond bank, [status: CREATED] }
        }
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def edit(Long id) {
        respond bankService.get(id)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def update(Bank bank) {
        if (bank == null) {
            notFound()
            return
        }

        try {
            bankService.save(bank)
        } catch (ValidationException e) {
            respond bank.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bank.label', default: 'Bank'), bank.id])
                redirect controller: 'bank', action: 'index'
            }
            'json' { respond bank, [status: ACCEPTED] }
            '*'{ respond bank, [status: ACCEPTED] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Bank'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}
