package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.ReceiptBatchCheckService
import metasoft.property.core.ReceiptBatchCheck
import metasoft.property.core.ReceiptBatchCheckService
import metasoft.property.core.ReceiptBatchTmp

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ReceiptCheckController {

    ReceiptBatchCheckService receiptBatchCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT", cancel: "PUT", delete: "DELETE"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<ReceiptBatchCheck> receiptBatchCheckList = receiptBatchCheckService.listInbox(params)
        respond receiptBatchCheckList, view: "/receiptCheck/index"
    }

    def outbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<ReceiptBatchCheck> receiptBatchCheckList = receiptBatchCheckService.listOutbox (params)
        respond receiptBatchCheckList, model:[receiptBatchCheckCount: receiptBatchCheckService.count()]
    }

    def show(Long id) {
        ReceiptBatchCheck receiptBatchCheck = receiptBatchCheckService.get(id)
        ReceiptBatchTmp receiptBatchTmp = receiptBatchCheckService.getEntitySource(receiptBatchCheck);
        render (model: [receiptBatchCheck: receiptBatchCheck, receiptBatchTmp: receiptBatchTmp], view: 'show')
    }

    def approve(Long id) {
        ReceiptBatchCheck receiptBatchCheck = receiptBatchCheckService.get(id);

        if (receiptBatchCheck == null) {
            notFound()
            return
        }

        try {
            receiptBatchCheckService.approve(receiptBatchCheck)
        } catch (ValidationException e) {
            respond e.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'receiptBatchCheck.label', default: 'ReceiptBatchCheck'), receiptBatchCheck.id])
                redirect receiptBatchCheck
            }
            '*'{ respond receiptBatchCheck, status: ACCEPTED }
        }
    }

    def cancel(Long id) {
        ReceiptBatchCheck receiptBatchCheck = receiptBatchCheckService.get(id);

        if (receiptBatchCheck == null) {
            notFound()
            return
        }

        try {
            receiptBatchCheckService.cancel(receiptBatchCheck, (request.JSON ?: params))
        } catch (ValidationException e) {
            respond receiptBatchCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'receiptBatchCheck.label', default: 'ReceiptBatchCheck'), receiptBatchCheck.id])
                redirect receiptBatchCheck
            }
            '*'{ respond receiptBatchCheck, status: ACCEPTED }
        }
    }
    
    def reject(Long id) {
        ReceiptBatchCheck receiptBatchCheck = receiptBatchCheckService.get(id);

        if (receiptBatchCheck == null) {
            notFound()
            return
        }

        try {
            receiptBatchCheckService.reject(receiptBatchCheck, (request.JSON ?: params) )
        } catch (ValidationException e) {
            respond receiptBatchCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'receiptBatchCheck.label', default: 'ReceiptBatchCheck'), receiptBatchCheck.id])
                redirect receiptBatchCheck
            }
            '*'{ respond receiptBatchCheck, status: ACCEPTED }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'receiptBatchCheck.label', default: 'ReceiptBatchCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
