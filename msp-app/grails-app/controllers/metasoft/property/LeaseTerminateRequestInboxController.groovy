package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.LeaseTerminateRequestCheck
import metasoft.property.core.LeaseTerminateRequestCheckService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class LeaseTerminateRequestInboxController {

    LeaseTerminateRequestCheckService leaseTerminateRequestCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100)
        List<LeaseTerminateRequestCheck> leaseTerminateRequestCheckList = leaseTerminateRequestCheckService.listInbox(params)
        respond leaseTerminateRequestCheckList, view: 'index'
    }

    def show(Long id) {
        LeaseTerminateRequestCheck leaseTerminateRequestCheck = leaseTerminateRequestCheckService.get(id)

        if(!leaseTerminateRequestCheck) {
            notFound();
            return;
        }

        respond leaseTerminateRequestCheck, view: 'show'
    }

    def reject(Long id) {
        LeaseTerminateRequestCheck leaseTerminateRequestCheck = leaseTerminateRequestCheckService.get(id);

        if (leaseTerminateRequestCheck == null) {
            notFound()
            return
        }

        try {
            leaseTerminateRequestCheckService.reject(leaseTerminateRequestCheck, request.JSON)
        } catch (ValidationException e) {
            respond leaseTerminateRequestCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseTerminateRequestCheck.label', default: 'LeaseTerminateRequestCheck'), leaseTerminateRequestCheck.id])
                redirect leaseTerminateRequestCheck
            }
            '*'{ respond leaseTerminateRequestCheck, status: OK }
        }
    }

    def approve(Long id) {
        LeaseTerminateRequestCheck leaseTerminateRequestCheck = leaseTerminateRequestCheckService.get(id);

        if (leaseTerminateRequestCheck == null) {
            notFound()
            return
        }

        try {
            leaseTerminateRequestCheckService.approve(leaseTerminateRequestCheck)
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseTerminateRequestCheck.label', default: 'LeaseTerminateRequestCheck'), leaseTerminateRequestCheck.id])
                redirect leaseTerminateRequestCheck
            }
            '*'{ respond leaseTerminateRequestCheck, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseTerminateRequestCheck.label', default: 'LeaseTerminateRequestCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
