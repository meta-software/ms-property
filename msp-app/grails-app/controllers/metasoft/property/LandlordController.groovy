package metasoft.property


import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.AccountCategory
import metasoft.property.core.AccountType
import metasoft.property.core.ContactPerson
import metasoft.property.core.Landlord
import metasoft.property.core.LandlordService
import metasoft.property.core.LessorPayment
import metasoft.property.core.Property
import metasoft.property.core.StandingOrder
import metasoft.property.core.Title

import static org.springframework.http.HttpStatus.*

@Secured(['IS_AUTHENTICATED_FULLY'])
class LandlordController {

    LandlordService landlordService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", makerChecker: "PUT"]

    @Secured(value=['ROLE_PERM_LANDLORD_READ'], httpMethod='GET')
    def app() {
        render view: 'vue-app';
    }

    /**
     * Retrieve list of Properties leased to the landlord with provided accountNumber
     *
     * @param accountNumber
     */
    @Secured(['IS_AUTHENTICATED_FULLY'])
    def properties(String accountNumber) {
        if(accountNumber == null) {
            accountNumber = params['accountNumber'] as String;
        }

        List<Property> propertyList = landlordService.getPropertiesByLandlordAccount(accountNumber)

        //render propertyList as JSON
        respond propertyList, view: 'propertyListOptions'
    }

    /**
     * Retrieve list of Properties leased to the landlord with provided accountNumber
     *
     * @param accountNumber
     */
    @Secured(['IS_AUTHENTICATED_FULLY'])
    def propertyList(Long id, Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<Property> propertyList = landlordService.propertiesByLandlord(id, params)
        respond propertyList, view: '/property/property'
    }

    def listOptions() {
        params['cat'] = 'landlord';

        List<Landlord> landlordList = landlordService.listOptions(params) as List<Landlord>;
        respond landlordList, view: 'listOptions';
    }

    def fetchList(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<Landlord> landlordList = landlordService.fetchList(params);

        respond landlordList, view: 'fetchList', model:[landlordList: landlordList]
    }

    @Secured(value=['ROLE_PERM_LANDLORD_READ'], httpMethod = 'GET')
    def index(Integer max) {

        params.max = Math.min(max ?: 10, 100);
        params['checkStatus'] = params['checkStatus'] ?: 'all';

        List<Landlord> landlordList = landlordService.listApproved(params);

        respond landlordList, model:[landlordList: landlordList, landlordCount: landlordService.count()]
    }

    @Secured(value=['ROLE_PERM_LANDLORD_READ'], httpMethod='GET')
    def show(Long id) {
        Landlord landlord = landlordService.get(id)

        if(!landlord) {
            notFound();
            return;
        }

        BigDecimal accountBalance = landlordService.getAccountBalance(landlord);
        BigDecimal currentReceipts = landlordService.getCurrentReceipts(landlord.id);

        respond (landlord, model: [
                           bankAccount: landlord.bankAccount
                          ,accountBalance: accountBalance
                          ,currentReceipts: currentReceipts
                          ,buildingsCount: landlordService.buildingsCount(landlord)
//                          ,currentInvoices: currentInvoices
                          ])
    }

    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='GET')
    def create() {
        params['accountCategory'] = AccountCategory.findByCode('landlord')
        Landlord landlord = new Landlord(params);

        respond landlord, model: [landlord: landlord], view: 'create-app';
    }

    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='POST')
    def save() {

        Map args = request.JSON ?: params;
        Landlord landlord = new Landlord();
        bindData(landlord, args);

        try {
            landlordService.create(landlord, args)
        } catch (ValidationException e) {
            respond landlord.errors, view:'create', model: [landlord: landlord]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'landlord.label', default: 'Landlord'), landlord.id])
                redirect controller: "landlord", id: landlord.id, action: "show"
            }
            '*' { respond landlord, status: CREATED, view: "show" }
        }
    }

    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='GET')
    def edit(Long id) {
        Landlord account = landlordService.getForEdit(id)

        respond account, model: [landlord: account]
    }

    @Secured(['ROLE_PERM_LANDLORD_WRITE'])
    def update(Long id) {

        Landlord landlord = Landlord.get(id);

        if (landlord == null) {
            notFound()
            return
        }

        try {
            landlordService.update(landlord, request.JSON as Map);
        } catch (ValidationException e) {
            respond landlord.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'landlord.label', default: 'Landlord'), landlord.id])
                redirect landlord
            }
            '*'{ respond landlord, status: OK, view: "show" }
        }
    }
    @Secured(value=['ROLE_PERM_LANDLORD_WRITE'], httpMethod='DELETE')
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        landlordService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'landlord.label', default: 'Landlord'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'landlord.label', default: 'Landlord'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(value=['ROLE_PERM_LANDLORD_CHECK'], httpMethod="PUT")
    def makerChecker(Landlord landlord){

        if (landlord == null) {
            notFound();
            return;
        }

        try {
            landlordService.updateMakerChecker(landlord);
        } catch (ValidationException e) {
            respond landlord.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'landlord.label', default: 'Landlord'), landlord.id]);
                redirect landlord;
            }
            '*' { respond landlord, status: CREATED, view: "show" }
        }
    }

    def dashboard(String report) {
        respond landlordService.executeDashlet(report, params);
    }

}

class LandlordCommand implements Validateable {

    Title title;
    AccountType accountType; /* Individual, Company */
    AccountCategory accountCategory; /* Tenant, Landlord, Suspense, Trust, etc... */

    String accountNumber;
    String accountName;

    String physicalAddress;
    String postalAddress;
    String businessAddress;
    String phoneNumber;
    String faxNumber;
    String emailAddress;
    String bpNumber;
    String vatNumber;

    static constraints = {
        accountNumber nullable: true
        physicalAddress nullable: true
        postalAddress nullable: true
        businessAddress nullable: true
        phoneNumber nullable: true
        faxNumber nullable: true
        emailAddress nullable: true
        title nullable: true
        bpNumber nullable: true
        standingOrders nullable: true
        lessorPayments nullable: true
    }

    List<StandingOrder> standingOrders;
    List<LessorPayment> lessorPayments;

    ContactPerson contactPerson;

    Object asType(Class type) {
        if(type == Landlord.class) {
            Landlord account = new Landlord(this.properties);

            if(this.contactPerson != null) {
                account.addToContactPersons(this.contactPerson);
            }
            if(this.standingOrders) {
                account.standingOrders = this.standingOrders;
            }

            if(this.lessorPayments) {
                account.lessorPayments = this.lessorPayments;
            }

            return account;
        }
    }
}
