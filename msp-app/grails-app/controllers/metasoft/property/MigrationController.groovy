package metasoft.property


import grails.core.GrailsApplication
import groovy.util.logging.Slf4j
import metasoft.property.core.MigrationService
import org.springframework.beans.factory.annotation.Autowired
import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.LandlordService
import metasoft.property.core.LeaseService
import metasoft.property.core.PropertyService
import metasoft.property.core.ProviderService
import metasoft.property.core.TenantService
import metasoft.property.core.TransactionBatchTmpService
import metasoft.property.core.TransactionTmpService
import metasoft.property.core.UserService
import org.springframework.http.HttpStatus
import grails.plugin.springsecurity.SpringSecurityService

@Slf4j
@Secured(value=['IS_AUTHENTICATED_ANONYMOUSLY'])
class MigrationController {

    @Autowired
    SpringSecurityService springSecurityService

    MigrationService migrationService

    UserService userService
    LeaseService leaseService ;

    GrailsApplication grailsApplication;

    def index() {

    }

    def process() {

        BootStrapFixtures fixtures = new BootStrapFixtures(
                grailsApplication: grailsApplication,
                userService: userService,
                tenantService:   grailsApplication.mainContext.getBean(TenantService.class),
                landlordService: grailsApplication.mainContext.getBean(LandlordService.class),
                propertyService: grailsApplication.mainContext.getBean(PropertyService.class),
                leaseService: grailsApplication.mainContext.getBean(LeaseService.class),
                providerService: grailsApplication.mainContext.getBean(ProviderService.class),
                transactionTmpService: grailsApplication.mainContext.getBean(TransactionTmpService.class),
                transactionBatchTmpService: grailsApplication.mainContext.getBean(TransactionBatchTmpService.class)
        );

        String artifact = params.get('artifact', null);

        switch(artifact) {
            case 'properties':
                fixtures.loadProperties();
                break;
            case 'rental-units':
                fixtures.loadRentalUnits();
                break;
            case 'tenants':
                fixtures.loadTenants()
                break;
            case 'tenant-balances':
                fixtures.loadBalances();
                break;
            case 'leases':
                File leaseItemsErrorsFile = new File(System.getProperty('user.home')+"/leases-items.errors.txt")
                fixtures.loadLeases(leaseItemsErrorsFile);
                break;
            case 'security-deposits':
                //migrationService.loadTenantDeposits();
                migrationService.loadTenantDepositReceipts();
                break;
            case 'ledger-balances':
                fixtures.loadLedgerBalances();
                break;
            case 'expire-leases':
                leaseService.processExpiredLeases()
                break;
            default:
                render status: HttpStatus.OK, text: "FINISHED: Nothing was set to be loaded"
                break;
        }

        //fixtures.loadTenants();
        //fixtures.loadLandlords();
        //fixtures.loadSuppliers();
        //fixtures.loadProperties();
        //*/

        //File leaseItemsErrorsFile = new File(System.getProperty('user.home')+"/leases-items.errors.txt")
        //leaseItemsErrorsFile.write("")
        //fixtures.loadLeases(leaseItemsErrorsFile);
        //fixtures.loadBalances();
        //fixtures.loadLandlordBalances();

        // update the lease details
        //LeaseService leaseService = grailsApplication.mainContext.getBean(LeaseService.class)
        //leaseService.processExpiredLeases();

        render status: HttpStatus.OK, text: "Finished loaded successfully. " + params['artifact']
    }

}
