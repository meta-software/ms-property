package metasoft.property

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.DashboardService
import metasoft.property.core.SecUser

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class DashboardController {

    DashboardService dashboardService;
    SpringSecurityService springSecurityService;

    def index() {
        ///
        SecUser secUser = springSecurityService.currentUser as SecUser;

        String appView = 'index';
        switch(secUser.userType.authority) {
            case 'ROLE_PROP_MANAGER':
                appView = 'property_admin'
                break;
            case 'ROLE_ADMIN':
                appView = 'app_admin'
                break;
            case 'ROLE_PROP_USER':
                appView = 'property_user'
                break;
            case 'ROLE_PROP_EXECUTIVE':
                appView = 'property_executive'
                break;
            case 'ROLE_PROP_DEMO':
                appView = 'app_demo'
                break;
            default:
                appView = 'index';
                break;
        }

        def results = dashboardService.executiveDashboard();

        render view: appView, model: [results: results]
    }

    def checkerInbox() {
        def results = dashboardService.getCheckerInboxState();
        respond results
    }

    def makerOutbox() {
        def results = dashboardService.getMakerOutboxState();
        respond results
    }

    def execute(String dashlet) {
        def results = dashboardService.executeDashlet(dashlet, params);
        respond results
    }
}
