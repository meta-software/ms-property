package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.AmountType
import metasoft.property.core.Currency
import metasoft.property.core.CurrencyService

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class CurrencyController {

    CurrencyService currencyService;

    static allowedMethods = []

    def listOptions() {
        respond currencyService.list(params);
    }

}
