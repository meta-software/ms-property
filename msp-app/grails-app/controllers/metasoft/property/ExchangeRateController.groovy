package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.ExchangeRate
import metasoft.property.core.ExchangeRateService

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ExchangeRateController {

    ExchangeRateService exchangeRateService

    static allowedMethods = [save: "POST", delete: "DELETE"]

    static defaultAction = "app"

    def app() {}

    def convert(String value, String fromCurrency, String date) {
        BigDecimal amount = new BigDecimal(value);

        //@todo: This is a problem in waiting
        Date eventDate = date ? new Date().parse('yyyy-MM-dd', date) : new Date();

        try {
            Map result = exchangeRateService.convertToReporting(amount, fromCurrency, eventDate);
            respond result
        }  catch (ValidationException e) {
            respond e.errors
        }
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<ExchangeRate> exchangeRateList = exchangeRateService.list(params);

        respond exchangeRateList, view: 'index', model:[exchangeRateCount: exchangeRateService.count(params)]
    }

    def listActive() {
        List<ExchangeRate> exchangeRateList = exchangeRateService.listActive(params);
        respond exchangeRateList, view: 'index', model:[exchangeRateCount: exchangeRateService.count(params)]
    }

    def show(Long id) {
        ExchangeRate exchangeRate = exchangeRateService.get(id)

        if(!exchangeRate) {
            notFound();
            return;
        }

        respond exchangeRate
    }

    def create() {
        respond new ExchangeRate(params)
    }

    def save(ExchangeRate exchangeRate) {
        if (exchangeRate == null) {
            notFound()
            return
        }

        try {
            exchangeRateService.save(exchangeRate);

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'exchangeRate.label', default: 'ExchangeRate'), exchangeRate.id])
                    redirect exchangeRate
                }
                '*' { respond exchangeRate, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }
    }

    def edit(Long id) {
        respond exchangeRateService.get(id)
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'exchangeRate.label', default: 'ExchangeRate'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class ExchangeRateConvertCommand {
    String fromCurrency;
    String toCurrency;
    Date eventDate;
}
