package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.RoutineRequestCheck
import metasoft.property.core.RoutineRequestCheckService

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class RoutineRequestCheckOutboxController {

    RoutineRequestCheckService routineRequestCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT", cancel: "PUT", delete: "DELETE"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<RoutineRequestCheck> routineRequestCheckList = routineRequestCheckService.listOutbox(params)

        render view: 'index', model: [routineRequestCheckList: routineRequestCheckList]
    }

    def show(Long id) {
        RoutineRequestCheck routineRequestCheck = routineRequestCheckService.get(id)

        if(!routineRequestCheck) {
            notFound();
            return;
        }

        render model: [routineRequestCheck: routineRequestCheck], view: 'show'
    }

    def cancel(Long id) {
        RoutineRequestCheck routineRequestCheck = routineRequestCheckService.get(id);

        if (routineRequestCheck == null) {
            notFound()
            return
        }

        try {
            routineRequestCheckService.cancel(routineRequestCheck, request.JSON)
        } catch (ValidationException e) {
            respond routineRequestCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'routineRequestCheck.label', default: 'RoutineRequestCheck'), routineRequestCheck.id])
                redirect routineRequestCheck
            }
            '*'{ respond routineRequestCheck, status: ACCEPTED }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'routineRequestCheck.label', default: 'RoutineRequestCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
