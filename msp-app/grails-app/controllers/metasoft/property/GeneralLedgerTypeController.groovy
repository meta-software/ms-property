package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.GeneralLedgerType

@Secured(value=['IS_AUTHENTICATED_ANONYMOUSLY'])
class GeneralLedgerTypeController {

    static allowedMethods = []

    def listOptions() {
        Map args = [sort: 'name', order: 'asc']

        List<GeneralLedgerType> generalLedgerTypeList = GeneralLedgerType.list(args)
        respond generalLedgerTypeList
    }

}
