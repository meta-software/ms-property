package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BankAccount
import metasoft.property.core.BankAccountService

import static org.springframework.http.HttpStatus.*

class BankAccountController {

    BankAccountService bankAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def app() {
        render view: 'vue-app'
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bankAccountService.list(params)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def create() {
        respond new BankAccount(params)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def save(BankAccount bankAccount) {
        if (bankAccount == null) {
            notFound()
            return
        }

        try {
            bankAccountService.save(bankAccount)
        } catch (ValidationException e) {
            respond bankAccount.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankAccount.label', default: 'BankAccount'), bankAccount.id])
                redirect controller: 'bankAccount', action: 'index'
            }
            'json' { respond bankAccount, [status: CREATED] }
            '*' { respond bankAccount, [status: CREATED] }
        }
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def edit(Long id) {
        respond bankAccountService.get(id)
    }

    @Secured(value=['IS_AUTHENTICATED_FULLY'])
    def update(BankAccount bankAccount) {
        if (bankAccount == null) {
            notFound()
            return
        }

        try {
            bankAccountService.save(bankAccount)
        } catch (ValidationException e) {
            respond bankAccount.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bankAccount.label', default: 'BankAccount'), bankAccount.id])
                redirect controller: 'bankAccount', action: 'index'
            }
            'json' { respond bankAccount, [status: ACCEPTED] }
            '*'{ respond bankAccount, [status: ACCEPTED] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankAccount.label', default: 'BankAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}
