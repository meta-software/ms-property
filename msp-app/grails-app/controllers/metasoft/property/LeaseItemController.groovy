package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.LeaseItem
import metasoft.property.core.LeaseItemService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class LeaseItemController {

    LeaseItemService leaseItemService

    static allowedMethods = [save: "POST", update: "PUT", teminate: "PUT"]

    @Secured(['ROLE_PERM_LEASE_READ'])
    def index() {

        List<LeaseItem> leaseItemList = leaseItemService.list(params);

        respond leaseItemList, view: 'index'
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def create() {
        respond new LeaseItem(params)
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def save(LeaseItem leaseItem) {
        if (leaseItem == null) {
            notFound()
            return
        }

        try {
            leaseItemService.save(leaseItem)
        } catch (ValidationException e) {
            respond leaseItem.errors, view:'create', model: [leaseItem: leaseItem]
            render(leaseItem.errors)
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'leaseItem.label', default: 'LeaseItem'), leaseItem.id])
                redirect leaseItem.lease
            }
            '*' { respond leaseItem, [status: CREATED] }
        }
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def edit(Long id) {
        respond leaseItemService.get(id)
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def update(LeaseItem leaseItem) {
        if (leaseItem == null) {
            notFound()
            return
        }

        try {
            leaseItemService.save(leaseItem)
        } catch (ValidationException e) {
            respond leaseItem.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseItem.label', default: 'LeaseItem'), leaseItem.id])
                redirect leaseItem
            }
            '*'{ respond leaseItem, [status: OK] }
        }
    }

    /**
     * Terminate the lease agreement action
     *
     * @param id
     * @return
     */
    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def terminate(Long id) {

        LeaseItem leaseItem = LeaseItem.get(id);

        if (leaseItem == null) {
            notFound();
            return;
        }

        //terminate the lease
        try {
            leaseItem.active = false;
            leaseItemService.save(leaseItem);
        } catch(ValidationException e) {
            leaseItem.errors = e.errors
            respond leaseItem, view:'show'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'lease.terminated.message', args: [message(code: 'leaseItem.label', default: 'LeaseItem'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseItem.label', default: 'LeaseItem'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
