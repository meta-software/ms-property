package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.Property
import metasoft.property.core.PropertyManager
import metasoft.property.core.PropertyManagerService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class PropertyManagerController {

    PropertyManagerService propertyManagerService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<PropertyManager> propertyManagerList = propertyManagerService.list(params)
        Long propertyManagerCount = propertyManagerService.count(params)
        respond propertyManagerList, model:[propertyManagerCount: propertyManagerCount]
    }

    def show(Long id) {

        PropertyManager propertyManager = propertyManagerService.get(id);

        if(propertyManager == null) {
            notFound();
            return;
        }

        Long propertiesCount = Property.where{
            propertyManager == propertyManager
        }.count();

        List<Properties> propertyList = Property.where{
            propertyManager == propertyManager
        }.list()

        respond propertyManagerService.get(id), model: [propertyList: propertyList, propertiesCount: propertiesCount]
    }

    def create() {
        respond new PropertyManager(params)
    }

    def save() {

        PropertyManager propertyManager = new PropertyManager();
        bindData(propertyManager, params);

        try {
            propertyManagerService.create(propertyManager, params);
        } catch (ValidationException e) {
            respond propertyManager.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'propertyManager.label', default: 'PropertyManager'), propertyManager.id])
                redirect controller: 'propertyManager', action: 'index'
            }
            '*' { respond propertyManager, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond propertyManagerService.get(id)
    }

    def update(PropertyManager propertyManager) {
        if (propertyManager == null) {
            notFound()
            return
        }

        try {
            propertyManagerService.update(propertyManager, params)
        } catch (ValidationException e) {
            respond propertyManager.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyManager.label', default: 'PropertyManager'), propertyManager.id])
                redirect controller: 'propertyManager', action: 'index'
            }
            '*'{ respond propertyManager, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        propertyManagerService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'propertyManager.label', default: 'PropertyManager'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propertyManager.label', default: 'PropertyManager'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
