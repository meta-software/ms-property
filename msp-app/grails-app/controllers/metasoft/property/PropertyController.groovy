package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.CheckStatus
import metasoft.property.core.Landlord
import metasoft.property.core.LandlordService
import metasoft.property.core.LeaseService
import metasoft.property.core.Property
import metasoft.property.core.PropertyService
import metasoft.property.core.PropertyTmp
import metasoft.property.core.PropertyType
import metasoft.property.core.PropertyTypeService
import metasoft.property.core.RentalUnitService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class PropertyController {

    PropertyService propertyService
    RentalUnitService rentalUnitService
    LandlordService landlordService
    LeaseService leaseService;
    PropertyTypeService propertyTypeService

    static responseFormats = ['html', 'json']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", makerChecker: "PUT"]

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def rentalUnits(Long id) {
        Property property = propertyService.get(id)

        if (property == null) {
            notFound()
            return
        }

        respond property, view: 'rentalUnits'
    }

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def pending(Integer max) {
        params.max = Math.min(max ?: 15, 50)

        params['mc-status'] = 'pending';

        List<Property> propertyList = propertyService.list(params) ;
        Long propertyCount = propertyService.count();

        respond propertyList, model:[propertyCount: propertyCount], view: 'index' ;
    }

    /**
     * Only retrieves the approved list of artifacts.
     *
     * @param max
     * @return
     */
    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def index(Integer max) {
        params.max = Math.min(max ?: 15, 50)

        params['mc-status'] = CheckStatus.APPROVED;
        List<Property> propertyList = propertyService.list(params);
        Long propertyCount = propertyService.count(params);
        List<PropertyType> propertyTypeList = propertyTypeService.list()

        respond propertyList, model:[propertyCount: propertyCount, propertyTypeList: propertyTypeList]
    }

    /**
     * Only retrieves the approved list of artifacts.
     *
     * @param max
     * @return
     */
    //@Secured(['ROLE_PERM_PROPERTY_READ'])
    def listOptions(Integer max) {
        params.max = Math.min(max ?: 1000, 1500)

        List<Property> propertyList = propertyService.listOptions(params);

        respond propertyList
    }

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def show(Long id) {
        Property property = propertyService.get(id)

        if(property == null) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'property.label', default: 'Property'), id])
            redirect( action: 'index')
            return;
        }

        Map occupancyStats = propertyService.getOccupancyStats(property)
        def rentalUnitList = rentalUnitService.list(['property' :property.id]);

        respond property, model: [occupancyStats: occupancyStats, rentalUnitList: rentalUnitList];
    }

    /**
     *
     * action to create property for the specifid landlord
     *
     * @return
     */
    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def create() {
        redirect controller:'property-tmp', action: 'create'
    }

    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def save() {

        Map args = (Map)request.JSON ?: params;
        Property property = new Property(args);
        //String json = JsonOutput.toJson(args).toString();
        //bindData(property, args);

        try {
            propertyService.create(property, args)
        } catch (ValidationException e) {
            respond property.errors, model: [property: property], status: UNPROCESSABLE_ENTITY
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'property.label', default: 'Property'), property])
                redirect property
            }
            '*' {
            //    render text: 'sample', status: ACCEPTED
                respond property, status: ACCEPTED
            }
        }
    }

    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def edit(Long id) {
        Property property = propertyService.get(id);

        if(!property) {
            notFound();
            return;
        }

        //@todo: check if the property is editeable by this user?

        PropertyTmp propertyTmp = propertyService.prepareForEditing(property);

        redirect (action: 'show', controller: 'property-tmp', id: propertyTmp.id, method: "GET");
    }

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def configure(Long id) {

        Property property = propertyService.get(id);

        if(!property) {
            notFound();
            return;
        }

        try {
            propertyService.configure(id)
            Map responseModel = [message: 'Property area configuration tallies with Rental Units']
            respond responseModel, status: ACCEPTED
        } catch (ValidationException ve) {
            respond ve.errors, status: OK
        }
    }

    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def update(Long id) {

        Property property = propertyService.get(id);
        Map args = (Map)request.JSON ?: params;

        if (property == null) {
            notFound()
            return
        }

        try {
            propertyService.update(property, args)
        } catch (ValidationException e) {
            respond property.errors, model: [property: property]
            return
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'property.label', default: 'Property'), property.id])
                redirect property
            }
            '*' { respond property, status: ACCEPTED, view: "show" }
        }
    }

    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        propertyService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'property.label', default: 'Property'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'property.label', default: 'Property'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
