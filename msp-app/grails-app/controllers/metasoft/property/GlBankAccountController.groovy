package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.GlBankAccount
import metasoft.property.core.GlBankAccountService
import org.springframework.beans.factory.annotation.Autowired

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class GlBankAccountController {

    @Autowired
    GlBankAccountService glBankAccountService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def app() {}
    
    def index() {
        render view: 'index'
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 20, 100)
        respond glBankAccountService.list(params)
    }

    def listOptions(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond glBankAccountService.listOptions(params)
    }
    
    def save(GlBankAccountCommand glBankAccountCommand) {
        if (glBankAccountCommand == null) {
            notFound()
            return
        }

        try {
            GlBankAccount glBankAccount = glBankAccountService.save(glBankAccountCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'bank.label', default: 'GL Bank Account'), glBankAccount.id])
                    redirect controller: 'glBankAccount', action: 'index'
                }
                'json' { respond glBankAccount, [status: CREATED] }
                '*' { respond glBankAccount, [status: CREATED] }
            }

        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    def edit(Long id) {
        respond glBankAccountService.get(id)
    }

    def update(Long id, GlBankAccountCommand glBankAccountCommand) {
        
        if (glBankAccountCommand == null) {
            notFound()
            return
        }

        try {
            GlBankAccount glBankAccount = glBankAccountService.update(id, glBankAccountCommand)
            
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'bank.label', default: 'GL Bank Account'), glBankAccount.id])
                    redirect controller: 'glBankAccount', action: 'index'
                }
                'json' { respond glBankAccount, [status: ACCEPTED] }
                '*'{ respond glBankAccount, [status: ACCEPTED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'edit'
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'GL Bank Account'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class GlBankAccountCommand {
    Long bank;
    String accountName;
    String accountNumber;
    String branch;
    String branchCode;
    String currency
    Long generalLedger;
}