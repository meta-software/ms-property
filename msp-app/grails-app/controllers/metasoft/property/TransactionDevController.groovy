package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.MainTransaction
import metasoft.property.core.MainTransactionService

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class TransactionDevController {

    MainTransactionService mainTransactionService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def app() {
        render view: 'index'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 50, 100)
        def transactionList = mainTransactionService.listDev(params);

        println(transactionList[0])

        render model: [transactionList: transactionList], view: '/transactionDev/index'
    }

    def show(Long id) {
        respond mainTransactionService.get(id)
    }
}
