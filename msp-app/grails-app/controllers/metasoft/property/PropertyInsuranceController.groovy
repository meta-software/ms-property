package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.PropertyInsurance
import metasoft.property.core.PropertyInsuranceService

import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_PERM_PROPERTY_WRITE'])
class PropertyInsuranceController {

    PropertyInsuranceService propertyInsuranceService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond propertyInsuranceService.list(params), model:[propertyInsuranceCount: propertyInsuranceService.count()]
    }

    def create() {
        respond new PropertyInsurance(params)
    }

    def save(PropertyInsurance propertyInsurance) {
        if (propertyInsurance == null) {
            notFound()
            return
        }

        try {
            propertyInsuranceService.save(propertyInsurance)
        } catch (ValidationException e) {
            respond propertyInsurance.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'propertyInsurance.label', default: 'PropertyInsurance'), propertyInsurance.id])
                redirect propertyInsurance.property
            }
            '*' { respond propertyInsurance, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond propertyInsuranceService.get(id)
    }

    def update(PropertyInsurance propertyInsurance) {
        if (propertyInsurance == null) {
            notFound()
            return
        }

        try {
            propertyInsuranceService.save(propertyInsurance)
        } catch (ValidationException e) {
            respond propertyInsurance.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyInsurance.label', default: 'PropertyInsurance'), propertyInsurance.id])
                redirect propertyInsurance
            }
            '*'{ respond propertyInsurance, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propertyInsurance.label', default: 'PropertyInsurance'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
