package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.PropertyCheck
import metasoft.property.core.PropertyCheckService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class PropertyCheckInboxController {

    PropertyCheckService propertyCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT", cancel: "PUT", delete: "DELETE"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<PropertyCheck> propertyCheckList = propertyCheckService.listInbox(params)

        render view: 'index', model: [propertyCheckList: propertyCheckList]
        //respond propertyCheckList, model:[propertyCheckCount: propertyCheckService.count()], view: 'index'
    }

    def show(Long id) {
        PropertyCheck propertyCheck = propertyCheckService.get(id)

        if(!propertyCheck) {
            notFound();
            return;
        }

        //PropertyTmp propertyTmp = propertyCheckService.getEntitySource(propertyCheck);
        //respond propertyCheck,  model: [propertyCheck: propertyCheck], view: 'show'
        render model: [propertyCheck: propertyCheck], view: 'show'
    }

    def reject(Long id) {
        PropertyCheck propertyCheck = propertyCheckService.get(id);

        if (propertyCheck == null) {
            notFound()
            return
        }

        try {
            propertyCheckService.reject(propertyCheck, request.JSON)
        } catch (ValidationException e) {
            respond propertyCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyCheck.label', default: 'PropertyCheck'), propertyCheck.id])
                redirect propertyCheck
            }
            '*'{ respond propertyCheck, status: ACCEPTED }
        }
    }

    def approve(Long id) {
        PropertyCheck propertyCheck = propertyCheckService.get(id);

        if (propertyCheck == null) {
            notFound()
            return
        }

        try {
            propertyCheckService.approve(propertyCheck)
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyCheck.label', default: 'PropertyCheck'), propertyCheck.id])
                redirect propertyCheck
            }
            '*'{ respond propertyCheck, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propertyCheck.label', default: 'PropertyCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
