package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.ReceiptBatch
import metasoft.property.core.ReceiptBatchService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ReceiptBatchController {

    ReceiptBatchService receiptBatchService

    static allowedMethods = [save: "POST", update: "PUT", postBatches: "PUT"]

    def index() {
        List<ReceiptBatch> receiptBatches = receiptBatchService.list(params);
        respond receiptBatches, view: 'index'
    }

    def postBatch(Long id) {

        ReceiptBatch receiptBatch = receiptBatchService.get(id);

        Long start = System.currentTimeMillis();
        def postResponse = receiptBatchService.postReceiptBatch(receiptBatch);
        Long end = System.currentTimeMillis();
        println('$$$$$$ Time Taken: ' + ((end-start)/1000) + ' Seconds');

        render text: "Batch posted successfully", status: ACCEPTED
    }

    def show(Long id) {
        ReceiptBatch receiptBatch = receiptBatchService.get(id)

        if(!receiptBatch) {
            notFound();
            return;
        }

        respond receiptBatch, model: [receiptBatch: receiptBatch], view: '/receiptBatch/show'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'receiptBatch.label', default: 'ReceiptBatch'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'  { render status: NOT_FOUND }
            json { render status: NOT_FOUND }
        }
    }
}
