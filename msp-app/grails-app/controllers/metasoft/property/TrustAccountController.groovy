package metasoft.property

import grails.plugin.springsecurity.annotation.Secured

@Secured(value=['IS_AUTHENTICATED_FULLY', 'ROLE_ACCOUNTS'])
class TrustAccountController {

    def index() {
    }
}
