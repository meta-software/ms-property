package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.RentalUnitTmp
import metasoft.property.core.RentalUnitTmpService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class RentalUnitTmpController {

    RentalUnitTmpService rentalUnitTmpService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 1000);

        List<RentalUnitTmp> rentalUnitTmpList = rentalUnitTmpService.list(params);

        respond rentalUnitTmpList, model:[rentalUnitTmpCount: rentalUnitTmpService.count()]
    }

    def show(Long id) {
        respond rentalUnitTmpService.get(id)
    }

    def create() {
        respond new RentalUnitTmp(params)
    }

    def save(RentalUnitTmp rentalUnitTmp) {
        if (rentalUnitTmp == null) {
            notFound()
            return
        }

        try {
            rentalUnitTmpService.save(rentalUnitTmp)
        } catch (ValidationException e) {
            respond rentalUnitTmp.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'rentalUnitTmp.label', default: 'RentalUnitTmp'), rentalUnitTmp.id])
                redirect rentalUnitTmp
            }
            '*' { respond rentalUnitTmp, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond rentalUnitTmpService.get(id)
    }

    def update(RentalUnitTmp rentalUnitTmp) {
        if (rentalUnitTmp == null) {
            notFound()
            return
        }

        try {
            rentalUnitTmpService.save(rentalUnitTmp)
        } catch (ValidationException e) {
            respond rentalUnitTmp.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'rentalUnitTmp.label', default: 'RentalUnitTmp'), rentalUnitTmp.id])
                redirect rentalUnitTmp
            }
            '*'{ respond rentalUnitTmp, [status: OK] }
        }
    }

    def delete(Long id) {

        RentalUnitTmp rentalUnitTmp = rentalUnitTmpService.get(id)

        if (id == null) {
            notFound()
            return
        }

        try {
            rentalUnitTmpService.delete(rentalUnitTmp)
        } catch(ValidationException e) {
            respond rentalUnitTmp.errors, status: UNPROCESSABLE_ENTITY, model: [rentalUnitTmp: rentalUnitTmp]
            return;
        }

        render status: NO_CONTENT
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'rentalUnitTmp.label', default: 'RentalUnitTmp'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
