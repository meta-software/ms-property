package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.LeaseRenewalRequestCheck
import metasoft.property.core.LeaseRenewalRequestCheckService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class LeaseRenewalRequestInboxController {

    LeaseRenewalRequestCheckService leaseRenewalRequestCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100)
        List<LeaseRenewalRequestCheck> leaseRenewalRequestCheckList = leaseRenewalRequestCheckService.listInbox(params)

        respond leaseRenewalRequestCheckList, view: 'index'
    }

    def show(Long id) {
        LeaseRenewalRequestCheck leaseRenewalRequestCheck = leaseRenewalRequestCheckService.get(id)

        if(!leaseRenewalRequestCheck) {
            notFound();
            return;
        }

        respond leaseRenewalRequestCheck, view: 'show'
    }

    def reject(Long id) {
        LeaseRenewalRequestCheck leaseRenewalRequestCheck = leaseRenewalRequestCheckService.get(id);

        if (leaseRenewalRequestCheck == null) {
            notFound()
            return
        }

        try {
            leaseRenewalRequestCheckService.reject(leaseRenewalRequestCheck, request.JSON)
        } catch (ValidationException e) {
            respond leaseRenewalRequestCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseRenewalRequestCheck.label', default: 'LeaseRenewalRequestCheck'), leaseRenewalRequestCheck.id])
                redirect leaseRenewalRequestCheck
            }
            '*'{ respond leaseRenewalRequestCheck, status: OK }
        }
    }

    def approve(Long id) {
        LeaseRenewalRequestCheck leaseRenewalRequestCheck = leaseRenewalRequestCheckService.get(id);

        if (leaseRenewalRequestCheck == null) {
            notFound()
            return
        }

        try {
            leaseRenewalRequestCheckService.approve(leaseRenewalRequestCheck)
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'leaseRenewalRequestCheck.label', default: 'LeaseRenewalRequestCheck'), leaseRenewalRequestCheck.id])
                redirect leaseRenewalRequestCheck
            }
            '*'{ respond leaseRenewalRequestCheck, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'leaseRenewalRequestCheck.label', default: 'LeaseRenewalRequestCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
