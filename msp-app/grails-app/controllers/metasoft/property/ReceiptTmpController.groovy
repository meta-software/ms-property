package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import groovy.util.logging.Slf4j
import metasoft.property.core.GeneralLedger
import metasoft.property.core.GeneralLedgerService
import metasoft.property.core.ReceiptBatchTmp
import metasoft.property.core.ReceiptBatchTmpService
import metasoft.property.core.ReceiptItemTmp
import metasoft.property.core.ReceiptTmp
import metasoft.property.core.ReceiptTmpService
import metasoft.property.core.Tenant
import metasoft.property.core.Currency
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.Errors

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT

@Slf4j
@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ReceiptTmpController {

    ReceiptTmpService receiptTmpService
    ReceiptBatchTmpService receiptBatchTmpService;

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", post: "POST"]

    def index() {
        render view: 'app'
    }

    def delete(Long id) {
        receiptTmpService.delete(id);
        render status: NO_CONTENT
    }

    def listByBatch(Long batchId) {
        ReceiptBatchTmp receiptBatchTmp = receiptBatchTmpService.get(batchId);

        if(!receiptBatchTmp) {
            notFound();
            return;
        }

        List<ReceiptTmp> receiptTmps = receiptTmpService.listByBatch(batchId, params);
        respond receiptTmps
    }

    def save(ReceiptCreateCommand receiptCreateCommand) {

        try {
            ReceiptTmp receipt =  receiptTmpService.createReceipt(receiptCreateCommand);
            respond receipt
        } catch (ValidationException ve) {
            flash.message = "Error while creating receipt transaction"
            respond ve.errors, status: NOT_ACCEPTABLE
        }
    }

    def show(Long id) {
        ReceiptTmp receiptTmp = receiptTmpService.get(id)

        if(!receiptTmp) {
            notFound();
            return;
        }

        respond receiptTmp, model: [receiptTmp: receiptTmp], view: '/receiptTmp/show'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'receiptTmp.label', default: 'ReceiptTmp'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'  { render status: NOT_FOUND }
            json { render status: NOT_FOUND }
        }
    }
}

class ReceiptCreateCommand {

    @Autowired
    GeneralLedgerService generalLedgerService

    ReceiptBatchTmp receiptBatchTmp;
    String transactionReference;
    String narrative;
    Date transactionDate;
    Tenant tenant;
    BigDecimal receiptAmount;
    GeneralLedger debitAccount;
    List<ReceiptItemTmp> receiptItemTmps = [];
    Currency transactionCurrency

    static constraints = {
        receiptBatchTmp nullable: true
        transactionReference maxSize: 24
        receiptAmount min:0.0
        debitAccount validator: { GeneralLedger value, ReceiptCreateCommand object, Errors errors ->
            if( !object.generalLedgerService.isAccountValidCurrency(value?.accountNumber, object?.transactionCurrency?.id)) {
                Object[] args = [object?.transactionCurrency?.id, value];
                errors.rejectValue("debitAccount","transaction.account.invalidCurrency", args, "Invalid currency {0} for account {1} ")
            }
        }
    }

    Object asType(Class type) {
        if(type == ReceiptTmp.class) {
            ReceiptTmp receiptTmp = new ReceiptTmp(this.properties);
            return receiptTmp;
        }
        return null;
    }
}