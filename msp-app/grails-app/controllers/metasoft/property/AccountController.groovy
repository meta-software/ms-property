package metasoft.property

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.Account
import metasoft.property.core.AccountService

@Secured(['IS_AUTHENTICATED_FULLY'])
class AccountController {

    AccountService accountService

    static allowedMethods = [save: "POST"];

    def list() {
        def accountList = accountService.listAllAccounts(params)

        def value = [results: accountList]
        render value as JSON
        //respond accountList, view: 'index', model: [accountList: accountList]
    }

    def listOptions() {
        List<Account> accountList = accountService.listOptions(params)

        accountList = accountList * 5;

        render model: [accountList: accountList], view: 'listOptions' //, model: [accountList: accountList]
    }

}
