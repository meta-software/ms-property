package metasoft.property

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.TransactionBatch
import metasoft.property.core.TransactionBatchService

import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class TransactionBatchController {

    TransactionBatchService transactionBatchService

    static allowedMethods = []
    
    def app() {
        respond view: 'app'
    }

    def search(Integer max) {
        params.max = Math.min(max ?: 20, 100)
        List<TransactionBatch> transactionBatches = transactionBatchService.searchBatches(params);

        respond transactionBatches, view: '/transactionBatch/search', model: [transactionBatches: transactionBatches]
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 20, 100)
        respond transactionBatchService.list(params)
    }

    def show(Long id) {
        respond transactionBatchService.get(id)
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'transactionBatch.label', default: 'TransactionBatch'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}
