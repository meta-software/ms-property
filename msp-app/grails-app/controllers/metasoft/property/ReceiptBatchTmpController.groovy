package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.*

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ReceiptBatchTmpController {

    ReceiptBatchTmpService receiptBatchTmpService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", postBatch: "PUT"]

    def index() {
        List<AccountType> accountTypeList = AccountType.list();
        render view: 'app', model: [accountTypeList: accountTypeList]
    }

    def postBatch(Long id) {

        ReceiptBatchTmp receiptBatchTmp = receiptBatchTmpService.get(id);

        if(!receiptBatchTmp) {
            notFound();
            return;
        }

        Long start = System.currentTimeMillis();
        def postResponse = receiptBatchTmpService.postReceiptBatchRequest(receiptBatchTmp);
        Long end = System.currentTimeMillis();

        log.debug('Time Taken to post the receipt batch: ' + ((end-start)/1000) + ' Seconds');

        respond receiptBatchTmp, status: OK
    }

    def listPending(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<ReceiptBatchTmp> receiptBatchTmpList = receiptBatchTmpService.listPending(params)

        respond receiptBatchTmpList, model: [receiptBatchTmpCount: 0], view: '/receiptBatchTmp/index'
    }

    def create() {
        try {
            ReceiptBatchTmp receiptBatchTmp = receiptBatchTmpService.createNewBatch()
            respond receiptBatchTmp
        } catch (ValidationException ve) {
            flash.message = "Error while creating transaction batch"
            respond ve.errors, status: NOT_ACCEPTABLE
        }
    }

    def show(Long id) {
        ReceiptBatchTmp receiptBatchTmp = receiptBatchTmpService.get(id)

        if(!receiptBatchTmp) {
            notFound();
            return;
        }

        respond receiptBatchTmp, model: [receiptBatchTmp: receiptBatchTmp], view: '/receiptBatchTmp/show'
    }

    def delete(Long id) {
        receiptBatchTmpService.delete(id);
        render status: NO_CONTENT
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'receiptBatchTmp.label', default: 'ReceiptBatchTmp'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'  { render status: NOT_FOUND }
            json { render status: NOT_FOUND }
        }
    }
}
