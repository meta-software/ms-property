package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.Account
import metasoft.property.core.AccountCheck
import metasoft.property.core.AccountCheckService

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class AccountCheckOutboxController {

    AccountCheckService accountCheckService

    static allowedMethods = [reject: "PUT", cancel: "PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<AccountCheck> accountCheckList = accountCheckService.listOutbox(params)
        respond accountCheckList, model:[accountCheckCount: accountCheckService.count()]
    }

    def show(Long id) {
        AccountCheck accountCheck = accountCheckService.get(id)
        Account account = accountCheckService.getEntity(accountCheck);
        respond ([accountCheck: accountCheck, account: account])
    }

    def cancel(Long id) {
        AccountCheck accountCheck = accountCheckService.get(id);

        if (accountCheck == null) {
            notFound()
            return
        }

        try {
            accountCheckService.cancel(accountCheck, request.JSON)
        } catch (ValidationException e) {
            respond accountCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'accountCheck.label', default: 'AccountCheck'), accountCheck.id])
                redirect accountCheck
            }
            '*'{ respond accountCheck, status: ACCEPTED }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountCheck.label', default: 'AccountCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
