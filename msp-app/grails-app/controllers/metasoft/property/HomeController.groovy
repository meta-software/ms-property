package metasoft.property

import grails.async.Promise
import grails.converters.JSON
import grails.core.GrailsApplication
import grails.plugins.quartz.JobManagerService
import groovy.util.logging.Slf4j
import metasoft.property.core.Lease
import metasoft.property.core.SubAccount
import metasoft.property.core.TenantInvoiceEmailService
import metasoft.property.core.TransactionBatch
import metasoft.property.core.TransactionBatchService
import metasoft.property.core.TransactionBatchTmp
import metasoft.property.core.TransactionTmp
import metasoft.property.core.TransactionType
import org.apache.commons.csv.CSVParser
import org.springframework.beans.factory.annotation.Autowired

import static org.apache.commons.csv.CSVFormat.*

import java.nio.file.Paths

import static grails.async.Promises.task
import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.api.v1.FlexcubeTransactionService
import metasoft.property.core.BillingCycleService
import metasoft.property.core.BillingInvoice
import metasoft.property.core.BillingInvoiceService
import metasoft.property.core.BulkAdjustService
import metasoft.property.core.LandlordService
import metasoft.property.core.LeaseService
import metasoft.property.core.LeaseTerminateRequestService
import metasoft.property.core.PropertyService
import metasoft.property.core.ProviderService
import metasoft.property.core.RoutineRequestService
import metasoft.property.core.TenantService
import metasoft.property.core.TransactionBatchTmpService
import metasoft.property.core.TransactionTmpService
import metasoft.property.core.UserService
import org.springframework.http.HttpStatus
import grails.plugin.springsecurity.SpringSecurityService

@Slf4j
@Secured(value=['IS_AUTHENTICATED_ANONYMOUSLY'])
//@Secured(value=['ROLE_MADD'])
class HomeController {

    @Autowired
    SpringSecurityService springSecurityService

    FlexcubeTransactionService flexcubeTransactionService
    RoutineRequestService routineRequestService
    BillingCycleService billingCycleService;
    UserService userService
    LeaseTerminateRequestService leaseTerminateRequestService
    BulkAdjustService bulkAdjustService;
    BillingInvoiceService billingInvoiceService;
    LeaseService leaseService ;

    TransactionBatchTmpService transactionBatchTmpService;
    TransactionTmpService transactionTmpService;
    TransactionBatchService transactionBatchService;
    TenantInvoiceEmailService tenantInvoiceEmailService
    GrailsApplication grailsApplication;

    JobManagerService jobManagerService

    def index() {

        def passwords = ['APass@562',
                        'ZBeta$947',
                        'APass_232',
                        'Alpha@287',
                        'Alpha@837',
                        'Alpha#111',
                        'Delta!276',
                        'Gamma@188',
                        'Alpha$870',
                        'Gamma!408',
                        'APass#188',
                        'APass$738',
                        'Gamma@507',
                        'Delta$243',
                        'Gamma$188',
                        'APass!617',
                        'Gamma$837',
                        'APass_595',
                        'Alpha_716',
                        'Delta@540'];
        
        def results = [:]
        passwords.each { password ->
            results[password] = springSecurityService.encodePassword(password, password)
        }
 
        render json: results as JSON
    }

    def opcos() {

        leaseTerminateRequestService.processRequests()

        render "finished printing the files"
    }

    def init() {

        BootStrapFixtures fixtures = new BootStrapFixtures(
                grailsApplication: grailsApplication,
                userService: userService,
                tenantService:   grailsApplication.mainContext.getBean(TenantService.class),
                landlordService: grailsApplication.mainContext.getBean(LandlordService.class),
                propertyService: grailsApplication.mainContext.getBean(PropertyService.class),
                leaseService: grailsApplication.mainContext.getBean(LeaseService.class),
                providerService: grailsApplication.mainContext.getBean(ProviderService.class),
                transactionTmpService: grailsApplication.mainContext.getBean(TransactionTmpService.class),
                transactionBatchTmpService: grailsApplication.mainContext.getBean(TransactionBatchTmpService.class)
        );

        switch(params['artifact']) {
            case 'property':
                fixtures.loadProperties();
                break;
            case 'leases':
                File leaseItemsErrorsFile = new File(System.getProperty('user.home')+"/leases-items.errors.txt")
                leaseItemsErrorsFile.write("")
                fixtures.loadLeases(leaseItemsErrorsFile);
                break;
            case 'deposits':
                fixtures.loadTenantDeposits();
                break;
            case 'ledger-balances':
                fixtures.loadLedgerBalances();
                break;
            case 'expire-leases':
                leaseService.processExpiredLeases()
                break;
            case 'routines':
                routineRequestService.processRequests();
                break;
            default:
                render status: HttpStatus.OK, text: "FINISHED: Nothing was set to be loaded"
                break;
        }

        //fixtures.loadTenants();
        //fixtures.loadLandlords();
        //fixtures.loadSuppliers();
        //fixtures.loadProperties();
        //*/

        //File leaseItemsErrorsFile = new File(System.getProperty('user.home')+"/leases-items.errors.txt")
        //leaseItemsErrorsFile.write("")
        //fixtures.loadLeases(leaseItemsErrorsFile);
        //fixtures.loadBalances();
        //fixtures.loadLandlordBalances();

        // update the lease details
        //LeaseService leaseService = grailsApplication.mainContext.getBean(LeaseService.class)
        //leaseService.processExpiredLeases();

        render status: HttpStatus.OK, text: "Finished loaded successfully. " + params['artifact']
    }

    def conf() {

        def value = [:]

        grailsApplication.config.keySet().each { key ->
            if( key.contains("mail") )
                value[key] = grailsApplication.config.getProperty(key);
        }

        render value as JSON;
    }

    /*
    def flex() {
        routineRequestService.executeRoutine(RoutineType.get(3));
        //flexcubeTransactionService.postTransactions();
        //billingCycleService.closePreviousBillingCycle();
        accountBillingStatementService.generateAccountStatements();

        println("passwordExpire @ -> ${grailsApplication.config.get('metasoft.app.password.expirePeriod')}")

        render "It is done, readl"
    }

    def cbz() {
        render """<?xml version="1.0" encoding="UTF-8"?>
<wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" targetNamespace="http://tempuri.org/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://tempuri.org/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/">
<wsdl:types>
<s:schema targetNamespace="http://tempuri.org/" elementFormDefault="qualified">
<s:element name="SubmitTransaction">
<s:complexType>
<s:sequence>
<s:element name="Username" type="s:string" maxOccurs="1" minOccurs="0"/>
<s:element name="Password" type="s:string" maxOccurs="1" minOccurs="0"/>
<s:element name="aSystem" type="s:string" maxOccurs="1" minOccurs="0"/>
<s:element name="TransactionType" type="s:string" maxOccurs="1" minOccurs="0"/>
<s:element name="RequestXml" type="s:string" maxOccurs="1" minOccurs="0"/>
<s:element name="Reference" type="s:string" maxOccurs="1" minOccurs="0"/>
</s:sequence>
</s:complexType>
</s:element>
<s:element name="SubmitTransactionResponse">
<s:complexType>
<s:sequence>
<s:element name="SubmitTransactionResult" type="s:string" maxOccurs="1" minOccurs="0"/>
</s:sequence>
</s:complexType>
</s:element>
</s:schema>
</wsdl:types>
<wsdl:message name="SubmitTransactionSoapIn">
<wsdl:part name="parameters" element="tns:SubmitTransaction"/>
</wsdl:message>
<wsdl:message name="SubmitTransactionSoapOut">
<wsdl:part name="parameters" element="tns:SubmitTransactionResponse"/>
</wsdl:message>
<wsdl:portType name="mobileAppWsSoap">
<wsdl:operation name="SubmitTransaction">
<wsdl:input message="tns:SubmitTransactionSoapIn"/>
<wsdl:output message="tns:SubmitTransactionSoapOut"/>
</wsdl:operation>
</wsdl:portType>
<wsdl:binding name="mobileAppWsSoap" type="tns:mobileAppWsSoap">
<soap:binding transport="http://schemas.xmlsoap.org/soap/http"/>
<wsdl:operation name="SubmitTransaction">
<soap:operation style="document" soapAction="http://tempuri.org/SubmitTransaction"/>
<wsdl:input>
<soap:body use="literal"/>
</wsdl:input>
<wsdl:output>
<soap:body use="literal"/>
</wsdl:output>
</wsdl:operation>
</wsdl:binding>
<wsdl:binding name="mobileAppWsSoap12" type="tns:mobileAppWsSoap">
<soap12:binding transport="http://schemas.xmlsoap.org/soap/http"/>
<wsdl:operation name="SubmitTransaction">
<soap12:operation style="document" soapAction="http://tempuri.org/SubmitTransaction"/>
<wsdl:input>
<soap12:body use="literal"/>
</wsdl:input>
<wsdl:output>
<soap12:body use="literal"/>
</wsdl:output>
</wsdl:operation>
</wsdl:binding>
<wsdl:service name="mobileAppWs">
<wsdl:port name="mobileAppWsSoap" binding="tns:mobileAppWsSoap">
<soap:address location="http://192.168.1.28/mobileapp/mobileAppWs.asmx"/>
</wsdl:port>
<wsdl:port name="mobileAppWsSoap12" binding="tns:mobileAppWsSoap12">
<soap12:address location="http://192.168.1.28/mobileapp/mobileAppWs.asmx"/>
</wsdl:port>
</wsdl:service>
</wsdl:definitions>"""
    }
     //*/
}
