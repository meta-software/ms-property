package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import metasoft.property.core.ConfigSetting
import metasoft.property.core.ConfigSettingService
import metasoft.property.core.ConfigType
import metasoft.property.core.exceptions.RecordNotFoundException
import org.grails.datastore.mapping.validation.ValidationException
import org.springframework.http.HttpStatus

@Secured(['IS_AUTHENTICATED_FULLY'])
class ConfigSettingController {

    ConfigSettingService configSettingService;

    static allowedMethods = [save: "POST", activate: "PUT"]

    def app() {
        render view: 'vue-app';
    }

    def show(Long id) {
        ConfigSetting configSetting = configSettingService.get(id)

        if(!configSetting) {
            render status: HttpStatus.NOT_FOUND
            return;
        }

        respond configSetting
    }

    def activate(Long id) {

        try {
            ConfigSetting configSetting = configSettingService.activate(id)
            respond configSetting, status: HttpStatus.ACCEPTED
        } catch(RecordNotFoundException e) {
            render status: HttpStatus.NOT_FOUND
        }

    }

    def index(Integer max) {
        List<ConfigSetting> settingsList = configSettingService.list(params);
        respond settingsList
    }

    def save(ConfigSettingCommand configSettingCommand) {

        if(!configSettingCommand.validate()) {
            respond configSettingCommand.errors
            return
        }

        try {
            ConfigSetting configSetting = configSettingService.save(configSettingCommand)
            respond configSetting, view: 'show'
        } catch(ValidationException e) {
            respond e.errors
        }
    }

}

class ConfigSettingCommand {
    Long configTypeId;
    String value;
    Date startDate;
    Date endDate;
    boolean enabled = true;
    boolean currentActive = false;

    static constraints = {
        value validator: { val, obj, errors ->
            ConfigType configType = ConfigType.get(obj.configTypeId)

            if(configType) {
                //@todo: confirm that the value type is the correct expected
            }
        }
        configTypeId validator: { val, obj, errors ->
            if(!ConfigType.get(val)) {
                Object[] args = ["Config Type"]
                errors.rejectValue("configTypeId", "default.not.exist", args, "Invalid Config Type selected");
            }
        }
        endDate nullable: true, validator: { val, obj, errors ->
            if(val != null && val.before(obj.startDate) ) {
                errors.reject("config.setting.endDate.beforeStart", "End Date cannot be before Start Date");
            }
        }
    }

    def asType(Class clazz) {
        if(clazz == ConfigSetting) {
            ConfigSetting configSetting = new ConfigSetting(this.properties)
            configSetting.configType = ConfigType.load(this.configTypeId)
            return configSetting;
        }

        return null;
    }
}
