package metasoft.property

import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.Landlord
import metasoft.property.core.LandlordService
import metasoft.property.core.PropertyTmp
import metasoft.property.core.PropertyTmpService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class PropertyTmpController {

    LandlordService landlordService;
    PropertyTmpService propertyTmpService;

    static allowedMethods = [save: "POST", update: "PUT", "post": "PUT", delete: "DELETE", discard: "PUT"]

    def postRequest(Long id) {
        PropertyTmp propertyTmp = propertyTmpService.get(id);

        if(!propertyTmp) {
            notFound();
            return;
        }

        try{
            propertyTmpService.postRequestProperty(propertyTmp)
        } catch (ValidationException e) {
            log.error("error while posting PropertyTmp record ${propertyTmp}: " + e.message);
            respond(propertyTmp.errors, model: [propertyTmp: propertyTmp], status: UNPROCESSABLE_ENTITY);
            return;
        }

        render status: ACCEPTED
    }

    def unitsArea(Long id) {
        PropertyTmp propertyTmp = propertyTmpService.get(id);

        if(!propertyTmp) {
            notFound();
            return;
        }

        def results = [totalUnitsArea: 0.0]

        results['totalUnitsArea'] = propertyTmpService.getTotalUnitsArea(propertyTmp);

        render results as JSON;
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond propertyTmpService.listPending(params), model:[propertyTmpCount: propertyTmpService.count(params)]
    }

    def show(Long id) {
        PropertyTmp propertyTmp = propertyTmpService.get(id);

        if(!propertyTmp) {
            notFound();
            return;
        }

        respond propertyTmp, model: [rentalUnits: propertyTmp.rentalUnits]
    }

    /**
     *
     * action to create property for the specified landlord
     *
     * @return
     */
    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def create() {
        Landlord landlord = landlordService.get(params['landlord.id']);

        PropertyTmp propertyTmp = new PropertyTmp(params)

        if (landlord) {
            propertyTmp.landlord = landlord;
        }
        List<Landlord> landlordList = landlordService.listApproved()

        respond propertyTmp, model:[landlordList: landlordList], view: 'create'
    }

    @Secured(['ROLE_PERM_PROPERTY_WRITE'])
    def save() {

        Map args = (Map)request.JSON ?: params;
        PropertyTmp propertyTmp = new PropertyTmp(args);

        try {
            propertyTmpService.create(propertyTmp);
        } catch (ValidationException e) {
            log.error("error while creating PropertyTmp entity: " + e.message);
            respond(propertyTmp.errors, model: [property: propertyTmp], status: UNPROCESSABLE_ENTITY);
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'propertyTmp.label', default: 'Property'), propertyTmp]);
                redirect propertyTmp;
            }
            '*' {
                //render text: 'sample', status: ACCEPTED
                respond propertyTmp, status: ACCEPTED
            }
        }
    }

    def edit(Long id) {
        respond propertyTmpService.get(id)
    }

    def update(PropertyTmp propertyTmp) {
        if (propertyTmp == null) {
            notFound()
            return
        }

        try {
            propertyTmpService.save(propertyTmp)
        } catch (ValidationException e) {
            respond propertyTmp.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyTmp.label', default: 'PropertyTmp'), propertyTmp.id])
                redirect propertyTmp
            }
            '*'{ respond propertyTmp, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        propertyTmpService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'propertyTmp.label', default: 'PropertyTmp'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    def discard(Long id){
        PropertyTmp propertyTmp = propertyTmpService.get(id)

        if(!propertyTmp) {
            notFound();
            return;
        }
        try {
            propertyTmpService.discardRecord(propertyTmp);
        } catch(ValidationException e) {
            log.error("Operation Failed: Error while discarding the propertyTmp record: ${id} ${e.message}")
            respond propertyTmp.errors, status: UNPROCESSABLE_ENTITY
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'propertyTmp.discarded.message', args: [message(code: 'propertyTmp.label', default: 'PropertyTmp'), id])
                redirect action:"index", method:"GET"
            }
            'json'{ render status: NO_CONTENT }
            '*'{ render status: NO_CONTENT }
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propertyTmp.label', default: 'PropertyTmp'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
