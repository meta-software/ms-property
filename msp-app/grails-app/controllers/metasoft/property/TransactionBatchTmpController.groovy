package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.*
import org.springframework.validation.Errors

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class TransactionBatchTmpController {

    TransactionBatchTmpService transactionBatchTmpService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", post: "POST"]

    def app() {
        render view: 'vue-app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<TransactionBatchTmp> transactionBatchTmpList = transactionBatchTmpService.listPending(params)
        Long transactionBatchTmpCount = transactionBatchTmpService.count();

        respond transactionBatchTmpList, model: [transactionBatchTmpCount: transactionBatchTmpCount], view: '/transactionBatchTmp/index'
    }

    def show(Long id) {
        TransactionBatchTmp transactionBatchTmp = transactionBatchTmpService.get(id)

        //@todo: add validation logic here.
        TransactionTmp transactionTmp = new TransactionTmp(transactionBatchTmp: transactionBatchTmp)

        respond transactionBatchTmp, model: [transactionTmp: transactionTmp], view: '/transactionBatchTmp/show'
    }

    def create() {
        try {
            TransactionBatchTmp transactionBatchTmp = transactionBatchTmpService.createNewBatch()
            redirect transactionBatchTmp
        } catch (ValidationException ve) {
            flash.message = "Error while creating transaction batch"
        }
    }

    def save(TransactionBatchTmp transactionBatchTmp) {

        try {
            transactionBatchTmpService.create(transactionBatchTmp);
        } catch (ValidationException e) {
            respond transactionBatchTmp.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'transactionBatchTmp.label', default: 'TransactionBatchTmp'), transactionBatchTmp.id])
                redirect transactionBatchTmp
            }
            'json' { respond transactionBatchTmp, [status: CREATED] }
            '*' { respond transactionBatchTmp, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond transactionBatchTmpService.get(id)
    }

    /**
     * Posts all the pending transactions for the particular transaction header
     *
     * @param transactionBatchTmp
     * @return
     */
    def postBatch(Long id) {

        def jsonRequest = request.getJSON()

        // retrieve the temporary transaction header object.
        TransactionBatchTmp transactionBatchTmp = transactionBatchTmpService.get(id);

        if (transactionBatchTmp == null) {
            notFound()
            return
        }

        try {
            Long start = System.currentTimeMillis()
            transactionBatchTmpService.postRequest(transactionBatchTmp)
            Long end = System.currentTimeMillis()

            log.debug("time taken to process batch request: " + (end-start)/1000 + " seconds")
        } catch (ValidationException e) {
            flash.message = message(code: 'transactionBatchTmp.posted.invalid', args: [transactionBatchTmp.batchNumber])
            flash.messageType = 'danger'
            log.info e.message
            respond e.errors, view:'edit'
            return
        } catch(TransactionBatchEmptyException e) {
            flash.message = message(code: 'transactionBatchTmp.empty.invalid', args: [transactionBatchTmp.batchNumber])
            flash.messageType = 'danger'
            log.info flash.message
            render status: UNPROCESSABLE_ENTITY, text: e.message
            //redirect(action: 'show', id: transactionBatchTmp.id)
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'transactionBatchTmp.posted.message', args: [transactionBatchTmp.batchNumber])
                redirect controller:"transactions"
            }
            json { respond transactionBatchTmp, [status: OK] }
        }
    }

    def update(TransactionBatchTmp transactionBatchTmp) {
        if (transactionBatchTmp == null) {
            notFound()
            return
        }

        try {
            transactionBatchTmpService.save(transactionBatchTmp)
        } catch (ValidationException e) {
            respond transactionBatchTmp.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'transactionBatchTmp.label', default: 'TransactionBatchTmp'), transactionBatchTmp.id])
                redirect transactionBatchTmp
            }
            json { respond transactionBatchTmp, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        try{
            transactionBatchTmpService.delete(id)
        } catch (ValidationException e) {
            respond e.errors, status: BAD_REQUEST
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'transactionBatchTmp.label', default: 'TransactionBatchTmp'), id])
                redirect action:"index", method:"GET"
            }
            json { render status: NO_CONTENT }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'transactionBatchTmp.label', default: 'TransactionBatchTmp'), params.id])
                redirect action: "index", method: "GET"
            }
            json { render status: NOT_FOUND }
            '*' {render status: NOT_FOUND}
        }
    }
}

class TransactionBatchTmpSaveCommand implements Validateable {
    TransactionType transactionType;

    static constraints = {
        transactionType validator: { TransactionType value, TransactionBatchTmpSaveCommand object, Errors errors ->
            if(!TransactionType.exists(value?.id)) {
                errors.rejectValue("transactionType", "transactionType.notexist", "Invalid selection for Transaction Type")
            }
        }
    }
}

