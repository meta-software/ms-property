package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.LessorPayment
import metasoft.property.core.LessorPaymentService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY', 'ROLE_ACCOUNTS'])
class LessorPaymentController {

    LessorPaymentService lessorPaymentService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond lessorPaymentService.list(params), model:[lessorPaymentCount: lessorPaymentService.count()]
    }

    def show(Long id) {
        respond lessorPaymentService.get(id)
    }

    def create() {
        respond new LessorPayment(params)
    }

    def save(LessorPayment lessorPayment) {
        if (lessorPayment == null) {
            notFound()
            return
        }

        try {
            lessorPaymentService.save(lessorPayment)
        } catch (ValidationException e) {
            render lessorPayment.errors
            return
            respond lessorPayment.errors, view:'create', model: [standinOrder: lessorPayment]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'lessorPayment.label', default: 'LessorPayment'), lessorPayment.id])
                redirect controller: "landlord", action: "show", id: lessorPayment.account.id
            }
            '*' { respond lessorPayment, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond lessorPaymentService.get(id)
    }

    def update(LessorPayment lessorPayment) {
        if (lessorPayment == null) {
            notFound()
            return
        }

        try {
            lessorPaymentService.save(lessorPayment)
        } catch (ValidationException e) {
            respond lessorPayment.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'lessorPayment.label', default: 'LessorPayment'), lessorPayment.id])
                redirect lessorPayment
            }
            '*'{ respond lessorPayment, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        lessorPaymentService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'lessorPayment.label', default: 'LessorPayment'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'lessorPayment.label', default: 'LessorPayment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
