package metasoft.property

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.web.mapping.LinkGenerator
import groovy.json.JsonSlurper
import metasoft.property.core.Notification
import metasoft.property.core.NotificationService
import org.springframework.beans.factory.annotation.Autowired

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class NotificationController {

    @Autowired
    LinkGenerator g
    SpringSecurityService springSecurityService;
    NotificationService notificationService;

    static allowedMethods = []

    def index(Integer max) {
        params.max = Math.min(max ?: 25, 100)
        List<Notification> notificationList = notificationService.listUnreadNotifications(params);
        respond notificationList, model:[notificationCount: notificationService.count()]
    }

    @Transactional
    def show(Long id) {
        Notification notification = notificationService.get(id)

        if(!notification.isRead) {
            notification.isRead = true;
            notification.save();
        }

        def urlParams = new JsonSlurper().parseText(notification.urlParams);
        def redirectUrl = g.link(controller: urlParams['controller'], action: urlParams['action'], id: urlParams['id']) as String;

        redirect(uri: redirectUrl);
    }

    def read(Long id) {
        Notification notification = Notification.get(id);

        if(!notification) {
            render status: NOT_FOUND
        }

        def secUser = springSecurityService.getCurrentUser();
        notificationService.readByUser(notification, secUser)

        respond notification, status: OK, view: 'show';
    }

}
