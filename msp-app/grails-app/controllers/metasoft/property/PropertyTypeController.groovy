package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.PropertyType
import metasoft.property.core.PropertyTypeService

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class PropertyTypeController {

    PropertyTypeService propertyTypeService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond propertyTypeService.list(params)
    }

    def listOptions() {
        respond propertyTypeService.list(params)
    }

    @Secured(value=['ROLE_ADMIN','ROLE_PROP_ADMIN'])
    def create() {
        respond new PropertyType(params)
    }

    @Secured(value=['ROLE_ADMIN','ROLE_PROP_ADMIN'])
    def save(PropertyType propertyType) {
        if (propertyType == null) {
            notFound()
            return
        }

        try {
            propertyTypeService.save(propertyType)
        } catch (ValidationException e) {
            respond propertyType.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'propertyType.label', default: 'PropertyType'), propertyType.id])
                redirect controller: 'propertyType', action: 'index'
            }
            '*' { respond propertyType, [status: CREATED] }
        }
    }

    @Secured(value=['ROLE_ADMIN','ROLE_PROP_ADMIN'])
    def edit(Long id) {
        respond propertyTypeService.get(id)
    }

    @Secured(value=['ROLE_ADMIN','ROLE_PROP_ADMIN'])
    def update(PropertyType propertyType) {
        if (propertyType == null) {
            notFound()
            return
        }

        try {
            propertyTypeService.save(propertyType)
        } catch (ValidationException e) {
            respond propertyType.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'propertyType.label', default: 'PropertyType'), propertyType.id])
                redirect controller: 'propertyType', action: 'index'
            }
            '*'{ respond propertyType, [status: OK] }
        }
    }

    @Secured(value=['ROLE_ADMIN','ROLE_PROP_ADMIN'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        propertyTypeService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'propertyType.label', default: 'PropertyType'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'propertyType.label', default: 'PropertyType'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
