package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.SecRole
import metasoft.property.core.SecUser
import metasoft.property.core.SecUserCheck
import metasoft.property.core.SecUserCheckService
import metasoft.property.core.exceptions.RecordNotFoundException

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['ROLE_ADMIN'])
class UserCheckController {

    SecUserCheckService secUserCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT", cancel: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<SecUserCheck> secUserCheckList = secUserCheckService.listInbox(params)
        respond secUserCheckList, model:[userCheckCount: secUserCheckService.count()]
    }

    def show(Long id) {
        SecUserCheck secUserCheck = secUserCheckService.get(id)
        SecUser secUser = secUserCheckService.getEntity(secUserCheck);

        def authorities = secUserCheckService.getAuthorities(secUserCheck);

        respond ([secUserCheck: secUserCheck, secUser: secUser, secRoles: authorities])
    }

    def approve(Long id) {

        try {
            SecUserCheck secUserCheck = secUserCheckService.approve(id)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'secUserCheck.label', default: 'SecUserCheck'), secUserCheck.id])
                    redirect secUserCheck
                }
                '*'{ respond secUserCheck, [status: ACCEPTED] }
            }

        } catch (ValidationException e) {
            respond e.errors, view:'edit'
            return
        } catch (RecordNotFoundException e) {
            notFound()
            return
        }
    }
    
    def reject(Long id) {
        SecUserCheck secUserCheck = secUserCheckService.get(id);

        if (secUserCheck == null) {
            notFound()
            return
        }

        try {
            secUserCheckService.reject(secUserCheck, request.JSON)
        } catch (ValidationException e) {
            respond secUserCheck.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'secUserCheck.label', default: 'SecUserCheck'), secUserCheck.id])
                redirect secUserCheck
            }
            '*'{ respond secUserCheck, status: ACCEPTED }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'secUserCheck.label', default: 'SecUserCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
