package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import metasoft.property.core.Lease
import metasoft.property.core.LeaseService
import metasoft.property.core.Property
import metasoft.property.core.RentalUnit
import metasoft.property.core.RentalUnitService
import metasoft.property.core.RentalUnitStatus

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class RentalUnitController {

    RentalUnitService rentalUnitService
    LeaseService leaseService;

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", makerChecker: "PUT"]

    def app() {
        render view: "vue-app"
    }

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def index(Integer max) {
        params.max = Math.min(max ?: 20, 100)

        List<RentalUnit> rentalUnitList = rentalUnitService.list(params)
        List<Property> propertyList = Property.list()
        List<RentalUnitStatus> rentalUnitStatuses = RentalUnitStatus.list()
        Long rentalUnitCount = rentalUnitService.count(params)

        respond rentalUnitList, model:[rentalUnitStatuses:rentalUnitStatuses
                                       ,propertyList:propertyList
                                       ,rentalUnitCount: rentalUnitCount], view: '/rentalUnit/index'
    }

    def listOptions() {
        List<RentalUnit> rentalUnitList = rentalUnitService.listOptions(params)
        respond rentalUnitList
    }

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def available(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<RentalUnit> rentalUnitList = rentalUnitService.availableUnits(params);

        respond rentalUnitList, model:[rentalUnitCount: rentalUnitService.count()], view: '/rentalUnit/index'
    }

    @Secured(['ROLE_PERM_PROPERTY_READ'])
    def show(Long id) {

        RentalUnit rentalUnit = rentalUnitService.get(id)

        if(!rentalUnit){
            notFound();
            return;
        }

        def leaseHistoryList = leaseService.leaseHistoryByRentalUnit(id);
        Lease activeLease = rentalUnit.activeLease

        respond rentalUnit, model: [leaseHistoryList: leaseHistoryList, activeLease: activeLease]
    }

    def listByProperty(Long propertyId, Integer max) {

        params.max = Math.min(max ?: 20, 100)

        List<RentalUnit> rentalUnitList = rentalUnitService.listByProperty(propertyId, params)
        Long rentalUnitCount = 0

        respond rentalUnitList, model:[rentalUnitCount: rentalUnitCount], view: '/rentalUnit/index'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'rentalUnit.label', default: 'RentalUnit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
