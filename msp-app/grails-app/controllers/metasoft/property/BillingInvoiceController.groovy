package metasoft.property

import grails.async.Promises
import grails.plugin.springsecurity.annotation.Secured
import groovy.util.logging.Slf4j
import metasoft.property.core.BillingCycle
import metasoft.property.core.BillingInvoice
import metasoft.property.core.BillingInvoiceItem
import metasoft.property.core.BillingInvoiceService
import metasoft.property.core.Lease
import metasoft.property.core.Tenant
import org.springframework.http.HttpHeaders

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

@Slf4j
@Secured(['IS_AUTHENTICATED_FULLY'])
class BillingInvoiceController {

    BillingInvoiceService billingInvoiceService;

    static allowedMethods = [post: "POST"]

    def app() {
        render view: 'app'
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100)
        List<BillingInvoiceReport> billingInvoiceReportList = billingInvoiceService.listInvoices(params);
        respond billingInvoiceReportList, view: '/billingInvoice/listInvoices';
    }

    def showReport(Long id) {
        BillingInvoiceReport billingInvoiceReport = billingInvoiceService.getReport(id);

        if(billingInvoiceReport == null){
            notFound();
            return;
        }

        respond billingInvoiceReport, view: '/billingInvoice/showReport', model: [billingInvoiceReport: billingInvoiceReport];
    }

    def printInvoice(Long id) {
        BillingInvoice billingInvoice = billingInvoiceService.get(id);

        if(billingInvoice == null){
            notFound();
            return;
        }

        File invoiceStream = billingInvoiceService.printInvoice(billingInvoice);

        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=billing-invoice.pdf")
        response.setContentType("application/pdf")
        response.setCharacterEncoding("UTF-8")
        response.outputStream << invoiceStream.getBytes()
        response.outputStream.flush()
        response.outputStream.close()

        Promises.task {
            try {
                invoiceStream.delete();
            } catch(IOException io) {
                log.error("failed to delete temporary billing invoice file [{}] with reason {}", invoiceStream.absolutePath, io.message);
                io.printStackTrace();
            }
        }

    }

    def emailInvoice(Long id) {
        BillingInvoice billingInvoice = billingInvoiceService.get(id);

        if(billingInvoice == null){
            notFound();
            return;
        }

        try {
            billingInvoiceService.emailBillingInvoice(billingInvoice);
        } catch(Exception e) {
            e.printStackTrace();
            render status: BAD_REQUEST, text: e.message
        }

        render status: OK, text: 'Email request sent successfully.'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'billingInvoice.label', default: 'BillingInvoice'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class BillingInvoiceReport {

    Long id;
    Tenant tenant;
    Lease lease;
    String invoiceNumber;
    BillingCycle billingCycle;
    BigDecimal openingBalance;
    BigDecimal invoiceTotal;
    BigDecimal vatTotal;
    BigDecimal discountTotal;

    Date dueDate;
    String invoiceFileName;

    boolean pdfGenerated = false;
    boolean emailSent = false;
    boolean smsSent = false;
    Date emailDate;
    Date smsDate;

    Date dateCreated;

    List<BillingInvoiceItem> billingInvoiceItems;

    static BillingInvoiceReport fromBillingInvoice(BillingInvoice billingInvoice) {
        BillingInvoiceReport billingInvoiceReport = new BillingInvoiceReport();
        billingInvoiceReport.properties.each { prop ->
            if(billingInvoice.hasProperty(prop.key) && prop.key != "class")
                billingInvoiceReport.setProperty(prop.key, billingInvoice.getProperty(prop.key));
        }
        return billingInvoiceReport;
    }
}
