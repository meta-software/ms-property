package metasoft.property

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.ExchangeRateGroup
import metasoft.property.core.ExchangeRateGroupCheck
import metasoft.property.core.ExchangeRateGroupCheckService
import metasoft.property.core.exceptions.RecordNotFoundException

import static org.springframework.http.HttpStatus.ACCEPTED
import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class ExchangeRateGroupCheckController {

    ExchangeRateGroupCheckService exchangeRateGroupCheckService

    static allowedMethods = [approve: "PUT", reject: "PUT", cancel: "PUT"]

    def inbox(Integer max) {
        render view: 'inbox'
    }

    def outbox(Integer max) {
        render view: 'outbox'
    }

    def listInbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<ExchangeRateGroupCheck> exchangeRateGroupCheckList = exchangeRateGroupCheckService.listInbox(params);
        respond exchangeRateGroupCheckList, model:[exchangeRateGroupCheckCount: exchangeRateGroupCheckService.countInbox(params)]
    }

    def listOutbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        List<ExchangeRateGroupCheck> exchangeRateGroupCheckList = exchangeRateGroupCheckService.listOutbox(params)
        respond exchangeRateGroupCheckList, model:[exchangeRateGroupCheckCount: exchangeRateGroupCheckService.countOutbox(params)]
    }

    def showOutbox(Long id){

        /*
        JSON.registerObjectMarshaller(Date) { Date date ->
            return date?.format("yyyy-MM-dd HH:mm:ss")
        }
        println("but we were here?")
        */
        ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupCheckService.get(id)
        respond (exchangeRateGroupCheck, view: 'showOutbox')
    }

    def show(Long id) {
        ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupCheckService.get(id)
        respond (exchangeRateGroupCheck)
    }

    def approve(Long id) {

        try {
            ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupCheckService.approve(id)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'exchangeRateGroupCheck.label', default: 'ExchangeRateGroupCheck'), exchangeRateGroupCheck.id])
                    redirect exchangeRateGroupCheck
                }
                '*'{ respond exchangeRateGroupCheck, [status: ACCEPTED] }
            }
        } catch(RecordNotFoundException e) {
            notFound();
        } catch (ValidationException e) {
            respond e.errors, view:'edit'
        }

    }
    
    def reject(Long id) {
        try {
            ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupCheckService.reject(id, request.JSON);
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'exchangeRateGroupCheck.label', default: 'ExchangeRateGroupCheck'), exchangeRateGroupCheck.id])
                    redirect exchangeRateGroupCheck
                }
                '*'{ respond exchangeRateGroupCheck, status: ACCEPTED }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: BAD_REQUEST
        }

    }

    def cancel(Long id) {

        try {
            ExchangeRateGroupCheck exchangeRateGroupCheck = exchangeRateGroupCheckService.cancel(id, request.JSON)
            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'exchangeRateGroupCheck.label', default: 'ExchangeRateGroupCheck'), exchangeRateGroupCheck.id])
                    redirect exchangeRateGroupCheck
                }
                '*'{ respond exchangeRateGroupCheck, status: ACCEPTED }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'edit'
            return
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'exchangeRateGroupCheck.label', default: 'ExchangeRateGroupCheck'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
