package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BulkAdjust
import metasoft.property.core.BulkAdjustService
import metasoft.property.core.BulkAdjust
import metasoft.property.core.BulkAdjust

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class BulkAdjustController {

    BulkAdjustService bulkAdjustService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def app() {
        render view: 'app'
    }

    def approve(Long id){

        BulkAdjust bulkAdjust = bulkAdjustService.get(id);

        if (bulkAdjust == null) {
            notFound();
            return;
        }

        try {
            bulkAdjustService.approve(bulkAdjust);
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkAdjust.label', default: 'BulkAdjust'), bulkAdjust.id]);
                redirect bulkAdjust;
            }
            '*' { respond bulkAdjust, status: CREATED, view: "show" }
        }
    }

    def reject(Long id){

        BulkAdjust bulkAdjust = bulkAdjustService.get(id);

        if (bulkAdjust == null) {
            notFound();
            return;
        }

        try {
            bulkAdjustService.reject(bulkAdjust, request.JSON ?: params);
        } catch (ValidationException e) {
            respond e.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bulkAdjust.label', default: 'BulkAdjust'), bulkAdjust.id]);
                redirect bulkAdjust;
            }
            '*' { respond bulkAdjust, status: ACCEPTED, view: "show" }
        }
    }

    def inbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkAdjustService.inbox(params), model:[bulkAdjustCount: bulkAdjustService.countInbox()]
    }

    def outbox(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkAdjustService.outbox(params), model:[bulkAdjustCount: bulkAdjustService.countOutbox()]
    }

    def cancelled(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkAdjustService.cancelled(params), model:[bulkAdjustCount: bulkAdjustService.countCancelled()]
    }

    def approved(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkAdjustService.approved(params), model:[bulkAdjustCount: bulkAdjustService.countApproved()]
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond bulkAdjustService.list(params), model:[bulkAdjustCount: bulkAdjustService.count()]
    }

    def show(Long id) {
        respond bulkAdjustService.get(id)
    }

    def create() {
        respond new BulkAdjust(params)
    }

    def save() {

        Map args = request.JSON ?: params as Map;
        BulkAdjust bulkAdjust = new BulkAdjust();
        bindData(bulkAdjust, args);

        if (bulkAdjust == null) {
            notFound()
            return
        }

        try {
            bulkAdjustService.create(bulkAdjust , args)
        } catch (ValidationException e) {
            respond bulkAdjust.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bulkAdjust.label', default: 'BulkAdjust'), bulkAdjust.id])
                redirect bulkAdjust
            }
            '*' { respond bulkAdjust, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond bulkAdjustService.get(id)
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bulkAdjust.label', default: 'BulkAdjust'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
