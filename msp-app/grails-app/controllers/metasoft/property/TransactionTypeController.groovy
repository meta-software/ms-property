package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.AccountType
import metasoft.property.core.TransactionType

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class TransactionTypeController {

    static allowedMethods = []

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<TransactionType> transactionTypes = TransactionType.list(params)

        respond transactionTypes, view: 'index'
    }

    def listOptions() {
        List<TransactionType> transactionTypes = TransactionType.list();
        respond transactionTypes
    }

}
