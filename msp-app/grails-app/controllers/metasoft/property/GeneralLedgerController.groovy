package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.AccountType
import metasoft.property.core.GeneralLedger
import metasoft.property.core.GeneralLedgerService
import metasoft.property.core.SubAccount

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class GeneralLedgerController {

    GeneralLedgerService generalLedgerService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def app() {
        render view: 'app'
    }

    def listOptions() {
        respond generalLedgerService.list(params)
    }

    def banks() {
        respond generalLedgerService.getBanks()
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 150);
        respond generalLedgerService.list(params), model:[generalLedgerCount: generalLedgerService.count()]
    }

    def outbox(Integer max) {
        params.max = Math.min(max ?: 15, 150);
        respond generalLedgerService.outbox(params), model:[generalLedgerCount: generalLedgerService.countOutbox()]
    }

    def create() {
        // default to no subAccount
        params.subAccount = SubAccount.findByAccountNumber(0);

        respond new GeneralLedger(params)
    }

    def save(GeneralLedger generalLedger) {
        if (generalLedger == null) {
            notFound()
            return
        }

        try {
            generalLedgerService.save(generalLedger)
        } catch (ValidationException e) {
            respond generalLedger.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'generalLedger.label', default: 'GeneralLedger'), generalLedger.id])
                redirect action: 'index'
            }
            '*' { respond generalLedger, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond generalLedgerService.get(id)
    }

    def show(Long id) {
        respond generalLedgerService.get(id)
    }

    def update(GeneralLedger generalLedger) {
        if (generalLedger == null) {
            notFound()
            return
        }

        try {
            generalLedgerService.save(generalLedger)
        } catch (ValidationException e) {
            respond generalLedger.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'generalLedger.label', default: 'GeneralLedger'), generalLedger.id])
                redirect action: 'index'
            }
            '*'{ respond generalLedger, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'generalLedger.label', default: 'GeneralLedger'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
