package metasoft.property

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import grails.gorm.transactions.Transactional
import metasoft.property.core.Account
import metasoft.property.core.AccountService
import metasoft.property.core.BillingCycle
import metasoft.property.core.BillingCycleService
import metasoft.property.core.Currency
import metasoft.property.core.GeneralLedger
import metasoft.property.core.GeneralLedgerService
import metasoft.property.core.Lease
import metasoft.property.core.MainTransaction
import metasoft.property.core.Property
import metasoft.property.core.RentalUnit
import metasoft.property.core.SubAccount
import metasoft.property.core.TransactionBatchTmp
import metasoft.property.core.TransactionTmp
import metasoft.property.core.TransactionTmpService
import metasoft.property.core.TransactionType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.Errors

import javax.persistence.Transient

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class TransactionTmpController {

    TransactionTmpService transactionTmpService

    static allowedMethods = [save: "POST", payment: "POST", invoice: "POST", receipt: "POST", refund: "POST", update: "PUT", delete: "DELETE"]

    def listByHeader(Long transactionBatchId) {
        def tmps = transactionTmpService.listByBatch(transactionBatchId);
        //List<TransactionTmp> transactionTmps = transactionTmpService.listByBatch(transactionBatchId);

        render tmps as JSON

        //render mainTransactionTmpList*.creditAccount;
        //respond transactionTmps, view: '/transactionTmp/index', model: [transactionTmps: transactionTmps]
    }

    def create() {
        params['transactionDate'] = new Date()
        TransactionTmp transactionTmp = new TransactionTmp(params)

        respond transactionTmp, view: 'create'
    }

    @Transactional
    def save(TransactionTmp transactionTmp) {

        if (transactionTmp == null) {
            notFound()
            return
        }

        try {
            transactionTmpService.create(transactionTmp)
        } catch (ValidationException e) {
            log.error "error while posting temporary transaction: ${transactionTmp.errors}"
            respond transactionTmp.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]) ;
                redirect transactionTmp.transactionBatchTmp;
            }
            '*' { respond transactionTmp, [status: CREATED] }
        }
    }

    @Transactional
    def refund(RefundCommand refundCommand) {

        if (!refundCommand.validate()) {
            respond refundCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.refund(refundCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    @Transactional
    def cancel(CancelCommand cancelCommand) {

        if (!cancelCommand.validate()) {
            respond cancelCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.cancel(cancelCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    @Transactional
    def payment(PaymentCommand paymentCommand) {

        if (!paymentCommand.validate()) {
            respond paymentCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.payment(paymentCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    @Transactional
    def depositPayment(DepositPaymentCommand depositPaymentCommand) {

        if (!depositPaymentCommand.validate()) {
            respond depositPaymentCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.depositPayment(depositPaymentCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    @Transactional
    def landlordPayment(LandlordPaymentCommand landlordPaymentCommand) {

        if (!landlordPaymentCommand.validate()) {
            respond landlordPaymentCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.landlordPayment(landlordPaymentCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }

        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    @Transactional
    def invoice(InvoiceCommand invoiceCommand) {

        if (!invoiceCommand.validate()) {
            respond invoiceCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.invoice(invoiceCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    @Transactional
    def receipt(ReceiptCommand receiptCommand) {

        if (!receiptCommand.validate()) {
            respond receiptCommand.errors, view: 'create'
            return
        }

        TransactionTmp transactionTmp = null;

        try {
            transactionTmp = transactionTmpService.receipt(receiptCommand.properties)
        } catch (ValidationException e) {
            respond e.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                redirect transactionTmp.transactionBatchTmp;
            }
            '*' { respond transactionTmp, [status: CREATED] }
        }
    }

    @Transactional
    def transfer(TransferCommand transferCommand) {

        if (!transferCommand.validate()) {
            respond transferCommand.errors, view: 'create'
            return
        }

        try {
            TransactionTmp transactionTmp = transactionTmpService.transfer(transferCommand)

            request.withFormat {
                form multipartForm {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                    redirect transactionTmp.transactionBatchTmp;
                }
                '*' { respond transactionTmp, [status: CREATED] }
            }
        } catch (ValidationException e) {
            respond e.errors, view:'create'
        }

    }

    /*
    @todo: no longer required.
    @Transactional
    def balance(BalanceCommand balanceCommand) {

        if (!balanceCommand.validate()) {
            respond balanceCommand.errors, view: 'create'
            return
        }

        TransactionTmp transactionTmp = null;

        try {
            transactionTmp = transactionTmpService.balance(balanceCommand.properties)
        } catch (ValidationException e) {
            respond e.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), transactionTmp.id]);
                redirect transactionTmp.transactionBatchTmp;
            }
            '*' { respond transactionTmp, [status: CREATED] }
        }
    }
    */

    @Transactional
    def delete(Long id) {
        TransactionTmp transactionTmp = transactionTmpService.get(id);

        if(transactionTmp == null) {
            request.withFormat {
                'html' {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), id])
                    redirect controller: 'transactions'
                    return
                }
                'json' {
                    render status: NOT_FOUND
                    return
                }
                '*' {
                    render status: NOT_FOUND
                    return
                }
            }
        }

        try {
            transactionTmpService.delete(id)
        } catch (ValidationException e) {
            request.withFormat {
                'json' {
                    respond e.errors, status: UNPROCESSABLE_ENTITY
                    return
                }
                '*' {
                    respond e.errors, status: UNPROCESSABLE_ENTITY
                    return
                }
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'transactionTmp.label', default: 'TransactionTmp'), id])
                redirect controller: 'transactions', action: 'show', id: transactionHeaderTmp.id
            }
            'json'{ render status: NO_CONTENT }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mainTransactionTmp.label', default: 'MainTransactionTmp'), params.id])
                redirect action: "index", method: "GET"
            }
            'json'{ render status: NOT_FOUND }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class TransactionTmpCommand {

    @Autowired
    BillingCycleService billingCycleService

    @Autowired
    GeneralLedgerService generalLedgerService;

    Date transactionDate;
    String creditAccount;
    String debitAccount;
    String creditSubAccount;
    String debitSubAccount;
    BigDecimal amount;
    String transactionReference;
    String description;
    SubAccount subAccount;
    TransactionBatchTmp transactionBatchTmp;
    String transactionCurrency;

    //Extra fields to add clarity to transaction data
    Lease lease;
    RentalUnit rentalUnit;
    Property property;
    BillingCycle billingCycle;

    static transients = ['billingCycleService']

    static constraints = {
        importFrom(TransactionTmp, exclude: ['transactionReference'])
        amount min: 0.0
        transactionDate validator: { Date value, Object object, Errors errors ->
            Date today = new Date().clearTime();

            if(value && value.after(today)) {
                errors.rejectValue("transactionDate", "transaction.invalid.date.infuture")
                return
            }
        }

    }
}

class DepositPaymentCommand {

    @Autowired
    GeneralLedgerService generalLedgerService

    Date transactionDate;
    String creditAccount;
    String debitAccount;
    String bankAccount;
    BigDecimal amount;
    String transactionReference;
    String description;
    TransactionBatchTmp transactionBatchTmp;
    SubAccount subAccount;
    //Extra fields to add clarity to transaction data
    Lease lease;
    //RentalUnit rentalUnit;
    //Property property;
    BillingCycle billingCycle;
    TransactionType transactionType;
    Currency transactionCurrency;

    static constraints = {
        importFrom(TransactionTmp, exclude: ['transactionReference'])
        amount min: 0.0
        transactionReference nullable: true
        transactionType nullable: true
        lease nullable: false
        transactionDate validator: { Date value, DepositPaymentCommand object, Errors errors ->
            Date today = new Date().clearTime();

            if(value && value.after(today)) {
                errors.rejectValue("transactionDate", "transaction.invalid.date.infuture")
            }
        }
        bankAccount validator: { String value, DepositPaymentCommand object, Errors errors ->
            if( !object.generalLedgerService.isAccountValidCurrency(value, object?.transactionCurrency?.id)) {
                Object[] args = [object?.transactionCurrency?.id, value];
                errors.rejectValue("bankAccount","transaction.account.invalidCurrency", args, "Invalid currency {0} for account {1} ")
            }
        }
    }
}

class LandlordPaymentCommand {

    @Autowired
    GeneralLedgerService generalLedgerService

    Date transactionDate;
    String creditAccount;
    String debitAccount;
    String bankAccount;
    BigDecimal amount;
    String transactionReference;
    String description;
    TransactionBatchTmp transactionBatchTmp;
    SubAccount subAccount;

    //Extra fields to add clarity to transaction data
    RentalUnit rentalUnit;
    Property property;
    BillingCycle billingCycle;
    TransactionType transactionType;
    String transactionCurrency;

    static constraints = {
        importFrom(TransactionTmp, exclude: ['transactionReference'])
        transactionType nullable: true
        transactionReference nullable: true
        property nullable: false
        amount min: 0.0
        transactionDate validator: { Date value, LandlordPaymentCommand object, Errors errors ->
            Date today = new Date().clearTime();

            if(value && value.after(today)) {
                errors.rejectValue("transactionDate", "transaction.invalid.date.infuture")
            }
        }
        bankAccount validator: { String value, LandlordPaymentCommand object, Errors errors ->
            if( !object.generalLedgerService.isAccountValidCurrency(value, object?.transactionCurrency)) {
                Object[] args = [object?.transactionCurrency, value];
                errors.rejectValue("bankAccount","transaction.account.invalidCurrency", args, "Invalid currency {0} for account {1} ")
            }
        }
    }

}

class InvoiceCommand extends TransactionTmpCommand {

    static constraints = {
        importFrom TransactionTmpCommand, exclude: ['transactionReference']
        transactionReference nullable: true
        lease nullable: false
    }

}

class TransferCommand extends TransactionTmpCommand {

    static constraints = {
        importFrom TransactionTmpCommand

        transactionReference nullable: true
        subAccount nullable: true
        debitAccount nullable: false, validator: { String value, TransferCommand object, Errors errors ->
            if(value == object.creditAccount) {
                Object[] args = []
                errors.rejectValue("debitAccount", "transaction.account.similar", args, "Cannot transfer between the same account")
            }

            if(! (GeneralLedger.countByAccountNumber(value) > 0) ) {
                errors.rejectValue("debitAccount", "transaction.account.invalid", "Provided debit account is invalid");
            }
        }
        creditAccount nullable: false, validator: { String value, TransferCommand object, Errors errors ->
            // check validity of the provided ledger account

            if(! (GeneralLedger.countByAccountNumber(value) > 0) ) {
                errors.rejectValue("creditAccount", "transaction.account.invalid", "Provided debit account is invalid");
            }

            // check and confirm the two general ledger are of the same currency
            if( !object.generalLedgerService.isAccountsSameCurrency(value, object.debitAccount) ) {
                errors.rejectValue("creditAccount", "transaction.account.invalidCurrencies", "The selected accounts have different currencies");
            }

            if( !object.generalLedgerService.isAccountValidCurrency(value, object?.transactionCurrency?.id) ){
                Object[] args = [object?.transactionCurrency?.id, value];
                errors.rejectValue("creditAccount","transaction.account.invalidCurrency", args, "Invalid currency {0} for account {1} ")
            }
        }

    }

}

class ReceiptCommand extends TransactionTmpCommand {

    static constraints = {
        importFrom(TransactionTmp, exclude: ['transactionReference'])
        importFrom(TransactionTmpCommand, exclude: ['transactionReference'])
        lease nullable: false
        transactionReference nullable: true
    }

}

class BalanceCommand extends TransactionTmpCommand {

    static constraints = {
        importFrom(TransactionTmp, exclude: ['transactionReference', 'amount'])
        importFrom(TransactionTmpCommand, exclude: ['amount'])
        transactionReference nullable: true
        subAccount nullable: true
        amount nullable: false, min: Double.MAX_VALUE.toBigDecimal().negate()
    }

}

class CancelCommand extends RefundCommand {

    static constraints = {
        importFrom(RefundCommand, exclude: ['transactionReference'])
        transactionReference validator: { String value, RefundCommand object, Errors errors ->
            MainTransaction mainTransaction = MainTransaction.where {
                transactionReference == value               // the reference lookup
                parentTransaction == true                   // we are only interested the main transaction
            }.get();

            if(!mainTransaction){
                Object[] args = [value];
                errors.rejectValue("transactionReference","transaction.reference.notfound", args, "Transaction with reference \"${value}\" was not found")
            }

            object.mainTransaction = mainTransaction;
            MainTransaction cancelTransaction = MainTransaction.findByTransactionReference("${value}-CN");

            if(cancelTransaction){
                Object[] args = [value];
                errors.rejectValue("transactionReference","transaction.reference.cancelled", args, "Transaction with reference \"${value}\" was cancelled already")
            }

            MainTransaction refundTransaction = MainTransaction.findByTransactionReference("${value}-RF");

            if(refundTransaction){
                Object[] args = [value];
                errors.rejectValue("transactionReference","transaction.reference.refunded", args, "Transaction with reference \"${value}\" was refunded already")
            }

        }

    }
}

class PaymentCommand extends TransactionTmpCommand {

    static constraints = {
        importFrom(TransactionTmpCommand, exclude: ['transactionReference', 'transactionDate'])
        transactionReference nullable: true
        creditAccount validator: { String value, PaymentCommand object, Errors errors ->
            // check and confirm if the currency and the debit account currency are the same
            if( !object.generalLedgerService.isAccountValidCurrency(value, object?.transactionCurrency?.id) ){
                Object[] args = [object?.transactionCurrency?.id, value];
                errors.rejectValue("creditAccount","transaction.account.invalidCurrency", args, "Invalid currency {0} for account {1} ")
            }
        }
    }
}

class RefundCommand {
    Date transactionDate;
    String transactionReference;
    String description;
    TransactionBatchTmp transactionBatchTmp

    @Transient
    TransactionTmp transactionTmp;

    @Transient
    MainTransaction mainTransaction;

    @Autowired
    TransactionTmpService transactionTmpService;

    static constraints = {
        transactionTmp nullable: true
        mainTransaction nullable: true
        transactionTmpService nullable: true
        transactionReference validator: { String value, RefundCommand object, Errors errors ->
            MainTransaction mainTransaction = MainTransaction.where{
                transactionReference == value               // the reference lookup
                parentTransaction == true                   // we are only interested the main transaction
                refunded == false                           // not yet refunded transactions
                cancelled == false                          // uncancelled transactions
                transactionType.transactionTypeCode == 'RC' // receipt
            }.get();

            if(!mainTransaction){
                Object[] args = [value];
                errors.rejectValue("transactionReference","transaction.reference.notfound", args, "Transaction with reference \"${value}\" was not found")
            }

            object.mainTransaction = mainTransaction;
            MainTransaction cancelTransaction = MainTransaction.findByTransactionReference("${value}-CN");

            if(cancelTransaction){
                Object[] args = [value];
                errors.rejectValue("transactionReference","transaction.reference.cancelled", args, "Transaction with reference \"${value}\" was cancelled already")
            }

            MainTransaction refundTransaction = MainTransaction.findByTransactionReference("${value}-RF");

            if(refundTransaction){
                Object[] args = [value];
                errors.rejectValue("transactionReference","transaction.reference.refunded", args, "Transaction with reference \"${value}\" was refunded already")
            }

        }
    }
}
