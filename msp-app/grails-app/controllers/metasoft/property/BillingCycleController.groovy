package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.BillingCycle
import metasoft.property.core.BillingCycleService
import metasoft.property.core.MakerChecker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.Errors

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class BillingCycleController {

    BillingCycleService billingCycleService

    static allowedMethods = [save: "POST", update: "PUT", open: "PUT", activate: "PUT"]

    def app() {}

    def index(Integer max) {
        //params.max = Math.min(max ?: 10, 100)

        List<BillingCycle> billingCycleList = billingCycleService.list(params);
        BillingCycle billingCycle = billingCycleService.currentCycle;

        respond billingCycleList, model:[billingCycleCount: billingCycleService.count(params),
                                         billingCycle: billingCycle]
    }

    def listOptions(Integer max) {
        params.max = Math.min(max ?: 10, 100);

        List<BillingCycle> billingCycleList = billingCycleService.listOptions(params);
        respond billingCycleList
    }

    def show(Long id) {
        def billingCycle = billingCycleService.get(id)

        respond billingCycle
    }

    def create() {
        respond new BillingCycle(params)
    }

    def save(BillingCycle billingCycle) {
        if (billingCycle == null) {
            notFound()
            return
        }

        try {
            billingCycleService.save(billingCycle)
        } catch (ValidationException e) {
            respond billingCycle.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'billingCycle.label', default: 'BillingCycle'), billingCycle.id])
                redirect billingCycle
            }
            '*' { respond billingCycle, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond billingCycleService.get(id)
    }

    def update(Long id, BillingCycle billingCycle) {
        if (billingCycle == null) {
            notFound()
            return
        }

        try {
            billingCycleService.update(id, billingCycle)
        } catch (ValidationException e) {
            respond billingCycle.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'billingCycle.label', default: 'BillingCycle'), billingCycle.id])
                redirect billingCycle
            }
            '*'{ respond billingCycle, [status: OK] }
        }
    }

    def open(Long id) {

        BillingCycle billingCycle = billingCycleService.get(id)

        if (billingCycle == null) {
            notFound()
            return
        }

        try {
            billingCycleService.openCycle(billingCycle)
        } catch (ValidationException e) {
            respond billingCycle.errors, view:'show', model: [billingCycle: billingCycle]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.opened.message', args: [message(code: 'billingCycle.label', default: 'BillingCycle'), billingCycle.id])
                redirect billingCycle
            }
            '*'{ respond billingCycle, [status: OK] }
        }
    }

    def activate(Long id) {

        BillingCycle billingCycle = billingCycleService.get(id)

        if (billingCycle == null) {
            notFound()
            return
        }

        try {
            billingCycleService.activateCycle(billingCycle)
        } catch (ValidationException e) {
            respond billingCycle.errors, view:'show', model: [billingCycle: billingCycle]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.activated.message', args: [message(code: 'billingCycle.label', default: 'BillingCycle'), billingCycle.id])
                redirect billingCycle
            }
            '*'{ respond billingCycle, [status: OK] }
        }
    }

    def close(Long id) {

         BillingCycle billingCycle = billingCycleService.get(id)
         if (billingCycle == null) {
            notFound()
            return
         }

         try {
             billingCycleService.closeCycle(billingCycle)
         } catch (ValidationException e) {
             respond billingCycle.errors, view: 'show', model: [billingCycle: billingCycle]
             return
         }

         request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.activated.message', args: [message(code: 'billingCycle.label', default: 'BillingCycle'), billingCycle.id])
                redirect billingCycle
            }
            '*'{ respond billingCycle, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'billingCycle.label', default: 'BillingCycle'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class BillingCycleCommand {

    @Autowired
    BillingCycleService billingCycleService

    String name;
    String shortDescription;
    Boolean active = false;
    Boolean open = false;
    Date startDate;
    Date endDate;

    static constraints = {
        name unique: true
        shortDescription nullable: true
        endDate validator: { Date value, BillingCycleCommand object, Errors errors ->
            if ((value && object.startDate) && value <= object.startDate) {
                Object[] args = (Object[])[value, object.startDate];
                errors.rejectValue("endDate","billingCycle.endDate.afterStart", args, "End Date should be after the Start Date value")
                return false; //the date parameters checks are wrong
            }
        }
        startDate validator: { Date value, BillingCycleCommand object, Errors errors ->
            if(billingCycleService.conflictExists(value, object.endDate)) {
                Object[] args = (Object[])[value, object.startDate];
                errors.reject("billingCycle.conflict", args, "End Date should be after the Start Date value")
                return false; //the date parameters checks are wrong
            }
        }
    }

}