package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import metasoft.property.core.SecPermission
import metasoft.property.core.SecRole
import metasoft.property.core.SecRoleService

import static org.springframework.http.HttpStatus.*

@Secured(value=['ROLE_ADMIN'])
class SecRoleController {

    SecRoleService secRoleService
    static defaultAction = "index"

    static allowedMethods = [update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        List<SecRole> secRoles = secRoleService.list(params);
        List<SecPermission> secPermissions = SecPermission.list([sort: 'name', order: 'asc'])

        respond secRoles, model:[secRoleCount: secRoleService.count(), secRoles: secRoles, secPermissions: secPermissions]
    }

    def list() {

        List<SecRole> secRoles = secRoleService.list(params);
        List<SecPermission> secPermissions = SecPermission.list()

        respond secRoles, model:[secRoles: secRoles, secPermissions: secPermissions]
    }

    def edit(Long id) {
        respond secRoleService.get(id)
    }

    def update(SecRole secRole) {
        if (secRole == null) {
            notFound()
            return
        }

        try {
            secRoleService.save(secRole)
        } catch (ValidationException e) {
            respond secRole.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'secRole.label', default: 'SecRole'), secRole.id])
                redirect secRole
            }
            '*'{ respond secRole, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'secRole.label', default: 'SecRole'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
