package metasoft.property

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import grails.validation.ValidationException
import groovy.json.JsonOutput
import metasoft.property.core.Currency
import metasoft.property.core.Landlord
import metasoft.property.core.Lease
import metasoft.property.core.LeaseRenewalRequestService
import metasoft.property.core.LeaseService
import metasoft.property.core.LeaseStatus
import metasoft.property.core.LeaseStatusRequest
import metasoft.property.core.LeaseTerminateRequest
import metasoft.property.core.LeaseTerminateRequestService
import metasoft.property.core.LeaseType
import metasoft.property.core.Property
import metasoft.property.core.RentalUnit
import metasoft.property.core.RentalUnitService
import metasoft.property.core.Tenant

import static org.springframework.http.HttpStatus.*

@Secured(value=['IS_AUTHENTICATED_FULLY'])
class LeaseController {

    LeaseService leaseService
    RentalUnitService rentalUnitService
    LeaseTerminateRequestService leaseTerminateRequestService

    static allowedMethods = [save: "POST", update: "PUT",
                             delete: "DELETE", makerChecker: 'PUT',
                             requestTermination: 'POST', terminate: 'PUT',
                             verify: 'PUT', updateReview: "PUT"]

    def terminateRequests(Long id) {
        List <LeaseTerminateRequest> leaseTerminateRequestList = leaseTerminateRequestService.listByLease(id, params)

        log.info(""+leaseTerminateRequestList)

        respond leaseTerminateRequestList, view: '/leaseTerminateRequest/index'
    }

    def reviewsDashboard() {
        def results = leaseService.getReviewsDashboard();

        render view: 'reviewsDashboard', model: [results: results]
    }

    def reviews(Integer max) {
        params.max = Math.min(max ?: 15, 100);
        List<Lease> leaseList = leaseService.getReviews(params);
        Long leaseCount = Lease.leaseReviews.countReviews(params);

        respond leaseList, model: [leaseList: leaseList, leaseCount: leaseCount]
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def review(Long id) {
        Lease lease = leaseService.getForEdit(id);

        if(!lease.isEditable()) {
            request.withFormat {
                html {
                    flash.error = "You cannot review/edit an Approved/Terminated lease"
                    redirect(action: 'show', id: id)
                }
                json { respond lease, [status: OK] }
                '*' { respond lease, [status: OK] }
            }
        }
        List<RentalUnit> rentalUnitList = rentalUnitService.list()

        respond lease, model: [lease: lease, rentalUnitList: rentalUnitList]
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def updateReview(Long id) {

        Lease lease = leaseService.get(id);
        Map args = (Map)request.JSON ?: params;
        String json = JsonOutput.toJson(args).toString();

        if (lease == null) {
            notFound()
            return
        }

        if(!lease.isEditable()) {
            flash.error = "You cannot edit an Approved/Terminated lease"
            redirect(action: 'show', id: id)
        }

        try {
            leaseService.updateReview(lease, args)
        } catch (ValidationException e) {
            respond lease.errors, view:'review'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'lease.label', default: 'Lease'), lease.id])
                redirect lease
            }
            '*'{ respond lease, [status: OK] }
        }
    }

    @Secured(['ROLE_PERM_LEASE_READ'])
    def index(Integer max) {
        params.max = Math.min(max ?: 15, 100);
        List<Lease> leaseList = leaseService.list(params);
        Long leaseCount = leaseService.count(params);

        List<Currency> currencyList = Currency.list();
        List<LeaseStatus> leaseStatusList = LeaseStatus.list();

        respond leaseList, model:[leaseCount: leaseCount, leaseStatusList: leaseStatusList, currencyList: currencyList]
    }

    @Secured(['ROLE_PERM_LEASE_READ'])
    def show(Long id) {

        Lease lease = leaseService.get(id);
        Tenant tenant = lease.tenant;
        Property property = lease.property;
        Landlord landlord = property.landlord;
        RentalUnit rentalUnit = lease.rentalUnit;
        Map leaseStats = leaseService.getLeaseStats(id);

        respond lease, view: 'show', model: [leaseStats: leaseStats, rentalUnit:rentalUnit, tenant: tenant, landlord: landlord, property: property]
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def create() {
        Lease lease = leaseService.fromTemplate(params)
        respond (lease,  view: 'create')
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def save() {

        Lease lease = new Lease();
        Map args = (Map)request.JSON ?: params;
        String json = JsonOutput.toJson(args).toString();
        bindData(lease, args);

        try {
            lease = leaseService.create(lease, args, json)
        } catch (ValidationException e) {
            respond e.errors, model: [lease: lease]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'lease.label', default: 'Lease'), lease.id])
                redirect lease
            }
            '*' { respond lease, [status: CREATED] }
        }
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def edit(Long id) {
        Lease lease = leaseService.getForEdit(id);

        if(!lease.isEditable()) {
            request.withFormat {
                html {
                    flash.error = "You cannot edit an Approved/Terminated lease"
                    redirect(action: 'show', id: id)
                }
                json { respond lease, [status: OK] }
                '*' { respond lease, [status: OK] }
            }
        }

        List<RentalUnit> rentalUnitList = rentalUnitService.list()

        respond lease, model: [lease: lease, rentalUnitList: rentalUnitList]
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def update(Long id) {

        Lease lease = leaseService.get(id);
        Map args = (Map)request.JSON ?: params;
        String json = JsonOutput.toJson(args).toString();

        if (lease == null) {
            notFound()
            return
        }

        if(!lease.isEditable()) {
            flash.error = "You cannot edit an Approved/Terminated lease"
            redirect(action: 'show', id: id)
        }

        try {
            leaseService.update(lease, args)
        } catch (ValidationException e) {
            respond lease.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'lease.label', default: 'Lease'), lease.id])
                redirect lease
            }
            '*'{ respond lease, [status: OK] }
        }
    }

    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        leaseService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'lease.label', default: 'Lease'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    /**
     * Request for the lease to be terminated
     */
    @Secured(['ROLE_PERM_LEASE_WRITE'])
    def leaseTerminateRequest(LeaseStatusRequestCommand leaseStatusRequestCommand) {

        if(leaseStatusRequestCommand == null) {
            notFound();
            return;
        }

        Lease lease = leaseStatusRequestCommand.lease;
        //@todo: Check if there is no other request currently pending and block if there is any

        try {
            leaseStatusRequestCommand.leaseStatus = LeaseStatus.findByCode('terminated');
            LeaseStatusRequest leaseStatusRequest = leaseStatusRequestCommand as LeaseStatusRequest;
            leaseService.requestTermination(leaseStatusRequest);
        } catch(ValidationException e){
            lease.errors = e.errors;
            respond lease, view: 'show'
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'lease.requestTermination.message', args: [message(code: 'lease.label', default: 'Lease'), id]);
                redirect action:"index", method:"GET";
            }
            '*'{ respond lease, status: NO_CONTENT }
        }

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'lease.label', default: 'Lease'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(['ROLE_PERM_LEASE_CHECK'])
    def makerChecker(Lease lease){

        if (lease == null) {
            notFound();
            return;
        }

        try {
            leaseService.updateMakerChecker(lease);
        } catch (ValidationException e) {
            respond lease.errors, view:'edit', status: NOT_ACCEPTABLE;
            return;
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'lease.label', default: 'Lease'), lease.id]);
                redirect lease;
            }
            '*' { respond lease, status: CREATED, view: "show" }
        }
    }

}

class LeaseCommand implements Validateable {

    def asType(Class type) {
        if(Lease.class == type) {
            Lease lease = new Lease(this.properties);
            return lease;
        }
    }
}

class LeaseStatusRequestCommand implements Validateable {
    Lease lease;
    LeaseStatus leaseStatus;
    String reason;

    static constraints = {
        lease()
        leaseStatus nullable: true
        reason()
    }

    def asType (Class type) {

        if(LeaseStatusRequest.class == type) {
            LeaseStatusRequest leaseStatusRequest = new LeaseStatusRequest(this.properties);
            return leaseStatusRequest;
        }

        return null ;
    }

}