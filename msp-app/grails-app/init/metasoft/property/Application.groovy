package metasoft.property

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import grails.gorm.transactions.Transactional
import groovy.util.logging.Slf4j
import metasoft.property.core.Settings
import metasoft.property.core.SettingsService
import metasoft.property.core.TenantService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.MessageSource

@Slf4j
class Application extends GrailsAutoConfiguration implements ApplicationRunner {

    @Autowired
    MessageSource messageSource

    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }

    @Override
    void run(ApplicationArguments args) throws Exception {
        log.info('Application::run() method.')
    }

    @Override
    void onStartup(Map<String, Object> event) {
        super.onStartup(event)

        //load external configuration properties file
        this.loadExternalConfiguration();

        //load the database configuration settings, overwriting anything which is specified in the external/internal files
        this.loadDatabaseConfiguration();
    }

    /**
     * loads configuration located externally from conf/ms-property.properties file and merges with the application.yml
     */
    @Transactional
    def loadDatabaseConfiguration() {

        SettingsService settingsService = grailsApplication.mainContext.getBean(SettingsService.class);

        log.info('loading the application configurations settings from the database')
        settingsService.reloadSettings()
        log.info('finished loading the application configurations settings from the database')
    }

    /**
     * loads configuration located externally from conf/ms-property.properties file and merges with the application.yml
     */
    def loadExternalConfiguration() {

        def externalProperties = new Properties();
        File propertiesFile = new File("conf/ms-property.properties");

        if(!propertiesFile.exists()) {
            log.warn("The configuration file conf/ms-property.properties was not found. If this is production setup make sure the file exists")
        } else {
            log.info "loading the external properties file ms-property.properties"
            //load the external properties file
            propertiesFile.withInputStream { inputStream ->
                externalProperties.load(inputStream);
            }

            //propagate all the settings in the properties file to the config object
            externalProperties.each { String key, value ->
                config.put(key, value)
            }
        }

    }

}