package metasoft.property


import grails.core.GrailsApplication
import grails.plugin.springsecurity.SecurityFilterPosition
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.util.Environment
import grails.validation.ValidationException
import metasoft.property.core.AccountCategory
import metasoft.property.core.AccountType
import metasoft.property.core.AmountType
import metasoft.property.core.Bank
import metasoft.property.core.BillingCycle
import metasoft.property.core.AdjustmentType

import metasoft.property.core.ConfigType
import metasoft.property.core.ContactPerson
import metasoft.property.core.Country
import metasoft.property.core.Currency
import metasoft.property.core.ExchangeRate
import metasoft.property.core.GeneralLedger
import metasoft.property.core.GeneralLedgerType
import metasoft.property.core.JobSchedule
import metasoft.property.core.JobType
import metasoft.property.core.Landlord
import metasoft.property.core.LandlordService
import metasoft.property.core.LeaseReviewType
import metasoft.property.core.LeaseStatus
import metasoft.property.core.LeaseType

import metasoft.property.core.PropertyManager
import metasoft.property.core.PropertyType
import metasoft.property.core.RentalUnitStatus
import metasoft.property.core.RentalUnitType
import metasoft.property.core.RoutineType
import metasoft.property.core.SecPermission
import metasoft.property.core.SecRole
import metasoft.property.core.SecUser
import metasoft.property.core.SecUserSecRole
import metasoft.property.core.RecordStatus
import metasoft.property.core.Settings
import metasoft.property.core.SubAccount
import metasoft.property.core.Tenant
import metasoft.property.core.TenantService
import metasoft.property.core.Title
import metasoft.property.core.TransactionType

class BootStrap {

    GrailsApplication grailsApplication;
    TenantService tenantService;
    LandlordService landlordService;

    def init = { servletContext ->

        log.info "loading bootstrap data @server serverURL = " + grailsApplication.config.grails.serverURL

        switch(Environment.current) {
            case Environment.PRODUCTION:
                initProduction();
                break;
            case Environment.TEST:
                initTest();
                break;
            default: // we assume it is in development
                initDevelopment();
                break;
        }

        //
        //JSON.registerObjectMarshaller(Date) { Date date ->
            //we are not interested in the timezone at all for the json output
        //    return date?.format("yyyy-MM-dd'T'HH:mm:ss'Z'")
        //}

        //
        bulkAdjustmentTypes();

        //
        initCountries()

        initialiseSettings();

        //initialise currencies
        initCurrencies()

        //initialise currencies
        initExchangeRates()

        SpringSecurityUtils.clientRegisterFilter('sharedSessionAuthenticationFilter' ,SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + 10);
    }

    def destroy = {
    }

    def initProduction() {
        log.info("bootstrapping data for the #PRODUCTION# environment");

        initDevelopment();
    }

    def initTest() {
        log.info("bootstrapping data for the #TEST# environment");

    }

    def initialiseSettings() {

        if(Settings.count() == 0) {
            List<Settings> settingsList = []
            settingsList << new Settings(category: 'security', name: 'metasoft.app.password.expirePeriod', classType: 'java.lang.Integer', value: 30, description: 'Password Expire Period (Days)');
            settingsList << new Settings(category: 'billing', name: 'metasoft.app.billing.interestRunDate', classType: 'java.lang.Integer', value: 8, description: 'Interest Run (Day of Month)');
            settingsList << new Settings(category: 'billing', name: 'metasoft.app.billing.rentRunDate', classType: 'java.lang.Integer', value: 1, description: 'Rent Run (Day of Month)');
            settingsList << new Settings(category: 'billing', name: 'metasoft.app.billing.opcosRunDate', classType: 'java.lang.Integer', value: 1, description: 'Opcos Run (Day of Month)');
            settingsList << new Settings(category: 'lease', name: 'metasoft.app.lease.reviewPeriod', classType: 'java.lang.Integer', value: 3, description: 'Lease review period (Months)');
            settingsList << new Settings(category: 'system', name: 'metasoft.app.flexcube.sync.enabled', classType: 'java.lang.Boolean', value: 'false', description: 'Flexcube Automatic Sync');

            for (Settings setting : settingsList) {
                if(!setting.save()) {
                    log.error("failed to save the settings record ${setting}")
                }
            }

        }

    }

    def bulkAdjustmentTypes() {
        //initialize the bulkAdjustmentTypes;

        if(AdjustmentType.count() == 0) {
            List<AdjustmentType> bulkAdjustmentTypes = []
            bulkAdjustmentTypes << new AdjustmentType(name: 'Increment', code: 'I')
            bulkAdjustmentTypes << new AdjustmentType(name: 'Decrement', code: 'D')
            bulkAdjustmentTypes.each { bulkAdjustmentType ->
                bulkAdjustmentType.save();
            }
        }
    }

    def initTransactionTypes() {

        if(TransactionType.count() == 0) {
            List<TransactionType> transactionTypes = []
            transactionTypes << new TransactionType(name: 'Receipt', transactionCategory: 'receipt', transactionTypeCode: 'RC');
            transactionTypes << new TransactionType(name: 'Invoice', transactionCategory: 'invoice', transactionTypeCode: 'IV');
            transactionTypes << new TransactionType(name: 'Refund', transactionCategory: 'refund', transactionTypeCode: 'RF');
            transactionTypes << new TransactionType(name: 'Cancel', transactionCategory: 'cancel', transactionTypeCode: 'CN');
            transactionTypes << new TransactionType(name: 'Transfer', transactionCategory: 'transfer', transactionTypeCode: 'IT');
            transactionTypes << new TransactionType(name: 'Payment', transactionCategory: 'payment', transactionTypeCode: 'PM');
            transactionTypes << new TransactionType(name: 'Balance Update', transactionCategory: 'balance', transactionTypeCode: 'BL');
            transactionTypes << new TransactionType(name: 'Deposit Payment', transactionCategory: 'deposit-payment', transactionTypeCode: 'DP');
            transactionTypes << new TransactionType(name: 'Landlord Payment', transactionCategory: 'landlord-payment', transactionTypeCode: 'LP');

            transactionTypes.each { TransactionType transactionType ->
                if(!transactionType.save()) {
                    log.error("Failed to save TransactionType: ${transactionType.errors}");
                }
            }
        }
    }

    def initBanks() {
        if(Bank.count() == 0) {
            List<Bank> banks = []
            banks << new Bank(bankName: 'CBZ BANK', bankCode: "CBZ BANK");

            banks.each { Bank bank ->
                if(!bank.save()) {
                    log.error("Failed to save Bank: ${bank.errors}");
                }
            }
        }
    }

    def loadSecurityRoles() {

        def permissions = [
                'LANDLORD_READ',
                'LANDLORD_WRITE',
                'LANDLORD_CHECK',

                'TENANT_READ',
                'TENANT_WRITE',
                'TENANT_CHECK',

                'PROPERTY_READ',
                'PROPERTY_WRITE',
                'PROPERTY_CHECK',

                'LEASE_READ',
                'LEASE_WRITE',
                'LEASE_CHECK',

                'LEASE_REQUEST_READ',
                'LEASE_REQUEST_WRITE',
                'LEASE_REQUEST_CHECK',

                'USER_READ',
                'USER_WRITE',
                'USER_CHECK',

                'PROP_MANAGER_READ',
                'PROP_MANAGER_WRITE',
                'PROP_MANAGER_CHECK',

                'SETTINGS',
                'ACCOUNTS',
                'REPORTS',

                'ENTITY_CHECK',

                'API_TRANSACTION_POST',
                'API_ACCOUNTS_READ',
                'EXEC_DASHBOARD',

                'TRANSACTION_READ',
                'TRANSACTION_WRITE',
                'TRANSACTION_CHECK'

        ]

        Map<String, SecPermission> permissionsMap = [:];

        permissions.each { permission ->
            String permissionName = permission;
            SecPermission secPermission = new SecPermission(name: permissionName);

            if(!secPermission.save(flush: true)) {
                log.error("failed to save permission: " + secPermission.errors)
            } else {
                permissionsMap[permission] = secPermission;
            }
        }

        def roles = []

        if (SecRole.count() == 0) {

            def secRoles = [
                    [name: 'Api User', authority: 'ROLE_API_USER', rolePermissions: ['API_TRANSACTION_POST', 'API_ACCOUNTS_READ']],
                    [name: 'Super User', authority: 'ROLE_SUPERUSER'],
                    [name: 'Administrator', authority: 'ROLE_ADMIN', rolePermissions: ['USER_READ','USER_WRITE','USER_CHECK', 'SETTINGS']],
                    [name: 'Property User', authority: 'ROLE_PROP_USER', rolePermissions: ["LANDLORD_READ","LANDLORD_WRITE","TENANT_READ","TENANT_WRITE","PROPERTY_READ","PROPERTY_WRITE","LEASE_READ","LEASE_WRITE","PROP_MANAGER_READ","PROP_MANAGER_WRITE","TRANSACTION_READ","TRANSACTION_WRITE"]],
                    [name: 'Property Manager', authority: 'ROLE_PROP_MANAGER', rolePermissions: ["LANDLORD_READ","LANDLORD_WRITE","LANDLORD_CHECK","TENANT_READ","TENANT_WRITE","TENANT_CHECK","PROPERTY_READ","PROPERTY_WRITE","PROPERTY_CHECK","LEASE_READ","LEASE_WRITE","LEASE_CHECK","PROP_MANAGER_READ","PROP_MANAGER_WRITE","PROP_MANAGER_CHECK","LEASE_REQUEST_READ","LEASE_REQUEST_WRITE","LEASE_REQUEST_CHECK","REPORTS","ENTITY_CHECK","TRANSACTION_READ","TRANSACTION_WRITE","TRANSACTION_CREATE"]],
                    [name: 'Property Executive', authority: 'ROLE_PROP_EXECUTIVE', rolePermissions: ["REPORTS"]]
                    ]

            //Save the secRoles
            secRoles.each { secRoleMap ->
                SecRole secRole = new SecRole(secRoleMap);

                if(secRoleMap?.authority == 'ROLE_SUPERUSER' && Environment.PRODUCTION == Environment.current) {
                    log.info "Skipping creation of superuser in ${Environment.current} mode";
                    return; // just skip and do not continue with creation of superuser
                }

                if(!secRole.save()) {
                    log.error("failed to save the SecRole entity : ${secRole.errors}")
                    log.error(secRole.errors)
                } else {
                    secRoleMap['rolePermissions'].each { String permission ->
                        SecPermission secPermission = SecPermission.findByName(permission);

                        if(secPermission == null) {
                            log.error("error permission [$permission] was not found");
                            return; // since we are in a closure
                        }
                        secRole.addToPermissions(secPermission)
                    }

                    if(secRole.name == 'Super User') {
                        SecPermission.list().each { secRole.addToPermissions(it) }
                    }

                }
            }

        } else {
            SecRole.list().each { role ->
                roles << role
            }
        }

        return roles;
    }

    def loadUsers () {

        Map users = [:]

        users['system'] = [ firstName: 'SYS', lastName: 'SYS', username: 'system', password: '.[05AB-3110#49707*93A279@3D3EF.6B686!67BA6', employeeId: 'system']
        users['flexcube'] = [ firstName: 'flexcube', lastName: 'flexcube', username: 'flexcube', password: 'CBZPr0per+y', employeeId: 'N/A',authorities: ['ROLE_API_USER']]
        users['admin'] = [ firstName: 'Admin', lastName: 'Admin', username: 'admin', password: 'Password@01', employeeId: 'admin',authorities: ['ROLE_ADMIN']]
        users['admin2'] = [ firstName: 'Admin2', lastName: 'Admin2', username: 'admin2', password: 'Password@01', employeeId: 'admin2',authorities: ['ROLE_ADMIN']]
        //users['propadmin'] = [ firstName: 'Property', lastName: 'Admin', username: 'padmin', password: 'Password@01', employeeId: 'padmin',authorities: ['ROLE_PROP_MANAGER']]
        //users['propuser'] = [ firstName: 'Property', lastName: 'User', username: 'puser', password: 'Password@01', employeeId: 'puser',authorities: ['ROLE_PROP_USER']]

        if(SecUser.count() == 0){
            users.each { key, userMap ->

                SecUser user = new SecUser(userMap);
                user.userType = SecRole.findByAuthority('ROLE_PROP_USER');

                if(!user.save(flush:true)) {
                    log.error("failed to save user: ${user.errors}")
                } else {
                    //Assign the set role to the user.
                    userMap.authorities.each { authority ->
                        log.info("creating system user : ${user.username} >>> ${authority}")
                        SecRole role = SecRole.findByAuthority(authority)
                        SecUserSecRole.create(user, role)
                    }

                }
            }
        }

    }

    def initLookups() {

        //initialize the bulkAdjustmentTypes;
        List<AdjustmentType> bulkAdjustmentTypes = []
        bulkAdjustmentTypes << new AdjustmentType(name: 'Increment', code: 'I')
        bulkAdjustmentTypes << new AdjustmentType(name: 'Decrement', code: 'D')
        bulkAdjustmentTypes.each { bulkAdjustmentType ->
            bulkAdjustmentType.save();
        }

        //Initialize the leaseStatuses
        List<LeaseType> leaseTypes = []
        leaseTypes << new LeaseType(name: 'Standard Lease');
        leaseTypes.each { leaseType ->
            leaseType.save();
        }

        //Initialize the leaseStatuses
        List<LeaseStatus> leaseStatuses = []
        leaseStatuses << new LeaseStatus(name: "New", code: "new", position: 0)
        leaseStatuses << new LeaseStatus(name: "Active", code: "active", position: 1)
        leaseStatuses << new LeaseStatus(name: 'Expired', code: "expired", position: 2)
        leaseStatuses << new LeaseStatus(name: 'Terminated', code: "terminated", position: 3)
        leaseStatuses.each { leaseStatus ->
            leaseStatus.save();
        }

        //Initialize the leaseStatuses
        List<RecordStatus> recordStatuses = []
        recordStatuses << new RecordStatus(name: "New", code: "new", position: 0)
        recordStatuses << new RecordStatus(name: "Active", code: "active", position: 1)
        recordStatuses << new RecordStatus(name: 'Suspended', code: "suspended", position: 2)
        recordStatuses << new RecordStatus(name: 'Terminated', code: "terminated", position: 3)
        recordStatuses.each { recordStatus ->
            recordStatus.save();
        }

        //Initialise the amountType
        List<AmountType> amountTypes = []
        amountTypes << new AmountType(name: "%", code: "perc")
        amountTypes << new AmountType(name: '$', code: "figure")
        amountTypes.each { amountType ->
            amountType.save();
        }

        // Initialize accountTypes
        List<AccountType> accountTypes = []
        accountTypes << new AccountType(name: "Company", code: "company")
        accountTypes << new AccountType(name: "Individual", code: "individual")
        accountTypes << new AccountType(name: "N.G.O", code: "ngo")
        accountTypes.each { accountType ->
            accountType.save();
        }

        //Initialize accountCategories
        List<AccountCategory> accountCategories = []
        accountCategories << new AccountCategory(name: "Tenant", code: 'tenant', accountPrefix: "001")
        accountCategories << new AccountCategory(name: "Landlord", code: 'landlord', accountPrefix: "002")
        accountCategories << new AccountCategory(name: "Service Provider", code: 'trust', accountPrefix: "003")
//        accountCategories << new AccountCategory(name: "Suspense", code: 'suspense', accountPrefix: "040")
        accountCategories.each { accountCategory ->
            accountCategory.save();
        }

        //Initialize salutation
        List<Title> salutations = []
        salutations << new Title(name: "Mr", code: "mr")
        salutations << new Title(name: "Mrs", code: "mrs")
        salutations << new Title(name: "Dr", code: "dr")
        salutations.each { salutation ->
            salutation.save();
        }

        // Initialize lookup tables
        List<RentalUnitStatus> unitStatuses = []
        unitStatuses << new RentalUnitStatus(name: "Occupied", code: "occupied")
        unitStatuses << new RentalUnitStatus(name: "Vacant", code: "un-occupied")

        unitStatuses.each { unitStatus ->
            unitStatus.save();
        }

        // Initialize the generalLedgerType
        Map<String,GeneralLedgerType> generalLedgerTypeList = [:]
        generalLedgerTypeList['general'] = new GeneralLedgerType(name: "General", startRange: 1, endRange: 599); // 600 general accounts
        generalLedgerTypeList['bank'] = new GeneralLedgerType(name: "Bank", startRange: 600, endRange: 699);     // 100 bank accounts
        generalLedgerTypeList['journal'] = new GeneralLedgerType(name: "Journal", startRange: 99, endRange: 99); // only 1 Journal account

        generalLedgerTypeList.each { String key, GeneralLedgerType generalLedgerType ->
            generalLedgerType.save()
        }

        // Initialize the subAccounts
        Map<String, SubAccount> subAccountList = [:]
        subAccountList['0']  = new SubAccount(accountName: "None", accountNumber: 0, suspenseAccount: 0, hasCommission: false, hasLedger: false, hasVat: false, subAccountType: 'A');
        subAccountList['1']  = new SubAccount(accountName: "Commission Earned", accountNumber: 1, suspenseAccount: 16, hasCommission: false, hasLedger: false, hasVat: false, subAccountType: 'C');
        subAccountList['2']  = new SubAccount(accountName: 'Rent', accountNumber: 2, suspenseAccount: 10, hasCommission: true, hasControl: true, subAccountType: 'E');
        subAccountList['3']  = new SubAccount(accountName: "Operating Costs", accountNumber: 3, suspenseAccount: 11, hasCommission: true, hasControl: true, subAccountType: 'E');
        subAccountList['4']  = new SubAccount(accountName: "Deposit", accountNumber: 4, suspenseAccount: 12, hasCommission: false, hasControl: true, subAccountType: 'E');
        subAccountList['5']  = new SubAccount(accountName: "Repairs", accountNumber: 5, suspenseAccount: 14, hasCommission: false, hasLedger: true, suspense: false, hasControl: true, subAccountType: 'E');
        subAccountList['10'] = new SubAccount(accountName: "Rent Suspense", accountNumber: 10, suspenseAccount: 2, hasCommission: false, suspense: true, subAccountType: 'S');
        subAccountList['11'] = new SubAccount(accountName: "Op Cost Suspense", accountNumber: 11, suspenseAccount: 3, hasCommission: false, suspense: true, subAccountType: 'S');
        subAccountList['12'] = new SubAccount(accountName: "Deposit Suspense", accountNumber: 12, suspenseAccount: 4, hasCommission: false, suspense: true, subAccountType: 'S');
        subAccountList['13'] = new SubAccount(accountName: "Deposit Held Suspense", accountNumber: 13, suspenseAccount: 4, hasCommission: false, suspense: true, subAccountType: 'S');
        subAccountList['14'] = new SubAccount(accountName: "Repairs Suspense", accountNumber: 14, suspenseAccount: 5, hasCommission: false, suspense: true, subAccountType: 'S');
        subAccountList['15'] = new SubAccount(accountName: "Interest", accountNumber: 6, suspenseAccount: 15, hasCommission: false, suspense: false, subAccountType: 'E');
        subAccountList['16'] = new SubAccount(accountName: "Interest Suspense", accountNumber: 15, suspenseAccount: 0, hasCommission: false, suspense: true, subAccountType: 'S');
        subAccountList['17'] = new SubAccount(accountName: "Commission Earned Suspense", accountNumber: 16, suspenseAccount: 1, hasCommission: false, suspense: true, subAccountType: 'S');

        subAccountList.each { String accountNumber, SubAccount subAccount ->
            if(!subAccount.save()) {
                log.error ("failed to save subAccount: ${subAccount.errors}")
            }
        }

        //Initialize the GeneralLedger Accounts
        List<GeneralLedger> generalLedgerList = []
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '3', accountName:'LESSEES TRUST CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '4', accountName:'LESSORS TRUST CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '5', accountName:'TRUST CREDITOR CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '6', accountName:'MISCELLANEOUS TRUST CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '7', accountName:'SELLERS TRUST CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '8', accountName:'TRADE DEBTORS CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '50', accountName:'TRADE CREDITORS CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '55', accountName:'FIXED ASSETS CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '75', accountName:'ACCUMULATED DEPRECIATION', system: true)
//        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '101', accountName:'INTER BRANCH TRANSFER', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '101', accountName:'COMMISSION', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['1'], accountNumber: '102', accountName:'Int Charge - Late Payment', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '105', accountName:'INTEREST CONTROL', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '106', accountName:'TAX ON SALES', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '107', accountName:'DISCOUNT ON SALES', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '121', accountName:'VAT INCOME ACCOUNT', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '122', accountName:'PURCHASES', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '123', accountName:'DISCOUNT ON PURCHASES', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '125', accountName:'TAX ON PURCHASES', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '127', accountName:'OPENING STOCK', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '135', accountName:'CLOSING STOCK', system: true)
//        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '250', accountName:'WORK IN PROGRESS', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '500', accountName:'DEPRECIATION', system: true)
//        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '580', accountName:'SALARIES & EXPENSES', system: true)
//        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '601', accountName:'WAGES & EXPENSES', system: true)
//        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '760', accountName:'STOCK ON HAND', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '761', accountName:'SUSPENDED G.T DEPOSITS', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '762', accountName:'UNCLAIMED GTDS - COMMERCIALS', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '763', accountName:'UNCLAIMED GTD - FLATS', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '764', accountName:'UNCLAIMED GTDS - HOUSES', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '765', accountName:'COMMERCIAL - SUSPENSE A/C', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '767', accountName:'RESIDENTIAL - SUSPENSE A/C', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '769', accountName:'SALES DEPOSITS CONTROL A/C', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '770', accountName:'KF. UNDERS / OVER BANKING A/C', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '775', accountName:'TRADING TAX PAYABLE', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '778', accountName:'REPAIR INVOICE CHG TENANT', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '779', accountName:'K.F.-PROV FOR DEBIT BALANCE', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '780', accountName:'VAT LIABILITY ACCOUNT', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '781', accountName:'VAT RENT ACCOUNT', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '782', accountName:'VAT COMMISSION ACCOUNT', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '851', accountName:'PROVISION FOR PAY', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '998', accountName:'RETAINED EARNINGS', system: true)
        generalLedgerList << new GeneralLedger(generalLedgerType: generalLedgerTypeList['general'], subAccount: subAccountList['0'], accountNumber: '999', accountName:'RECEIPTS & PAYMENTS BATCH', system: true)

        generalLedgerList.each{ GeneralLedger generalLedger ->
            if(!generalLedger.save()) {
                log.error("failed to save general ledger: ${generalLedger.errors}")
            }
        }

        //Initialize the PropertyType items
        List<PropertyType> propertyTypes = []
        propertyTypes << new PropertyType(code: 'residential', name: 'Residential');
        propertyTypes << new PropertyType(code: 'commercial', name: 'Commercial');
        propertyTypes.each { PropertyType propertyType ->
            propertyType.save();
        }

        //Initialise demo billing cycles
        List<BillingCycle> billingCycles = []
        billingCycles << new BillingCycle(name: "201907", startDate: new Date().parse("yyyyMMdd", "20190701"), endDate: new Date().parse("yyyyMMdd", "20190731"))
        billingCycles << new BillingCycle(name: "201908", startDate: new Date().parse("yyyyMMdd", "20190801"), endDate: new Date().parse("yyyyMMdd", "20190831"))
        billingCycles << new BillingCycle(name: "201909", startDate: new Date().parse("yyyyMMdd", "20190901"), endDate: new Date().parse("yyyyMMdd", "20190930"), open: true, active: true)
        billingCycles << new BillingCycle(name: "201910", startDate: new Date().parse("yyyyMMdd", "20191001"), endDate: new Date().parse("yyyyMMdd", "20191031"))
        billingCycles << new BillingCycle(name: "201911", startDate: new Date().parse("yyyyMMdd", "20191101"), endDate: new Date().parse("yyyyMMdd", "20191130"))
        billingCycles << new BillingCycle(name: "201912", startDate: new Date().parse("yyyyMMdd", "20191201"), endDate: new Date().parse("yyyyMMdd", "20191231"))
        billingCycles << new BillingCycle(name: "202001", startDate: new Date().parse("yyyyMMdd", "20200101"), endDate: new Date().parse("yyyyMMdd", "20200131"))
        billingCycles << new BillingCycle(name: "202002", startDate: new Date().parse("yyyyMMdd", "20200201"), endDate: new Date().parse("yyyyMMdd", "20200229"))
        billingCycles << new BillingCycle(name: "202003", startDate: new Date().parse("yyyyMMdd", "20200301"), endDate: new Date().parse("yyyyMMdd", "20200331"))
        billingCycles << new BillingCycle(name: "202004", startDate: new Date().parse("yyyyMMdd", "20200401"), endDate: new Date().parse("yyyyMMdd", "20200430"))
        billingCycles << new BillingCycle(name: "202005", startDate: new Date().parse("yyyyMMdd", "20200501"), endDate: new Date().parse("yyyyMMdd", "20200531"))
        billingCycles.each { billingCycle ->
            if (!billingCycle.save()) {
                log.error(billingCycle.errors)
            }
        }

        loadRentalUnitTypes();

        loadJobTypes();
    }

    def loadRentalUnitTypes = {
        // Initialize accountTypes
        if(RentalUnitType.count() == 0) {
            List<RentalUnitType> rentalUnitTypes = []
            rentalUnitTypes << new RentalUnitType(name: "Standard Unit")
            rentalUnitTypes << new RentalUnitType(name: "Residential")
            rentalUnitTypes << new RentalUnitType(name: "Base Station")
            rentalUnitTypes << new RentalUnitType(name: "Office")
            rentalUnitTypes.each { rentalUnitType ->
                rentalUnitType.save();
            }
        }
    }

    def loadConfigTypes = {
        // Initialize accountTypes
        if(ConfigType.count() == 0) {
            List<ConfigType> configTypes = []
            configTypes << new ConfigType(code: "vat.global.rate", name: "VAT (%)", classType: "java.lang.BigDecimal")
            configTypes << new ConfigType(code: "interest.global.rate", name: "Interest (%)", classType: "java.lang.BigDecimal")
            configTypes.each { configType ->
                configType.save();
            }
        }
    }

    def loadJobTypes = {
        // Initialize accountTypes
        if(JobType.count() == 0) {
            List<JobType> jobTypes = []
            jobTypes << new JobType(name: "Class");

            jobTypes.each { jobType ->
                jobType.save();
            }
        }
    }

    def loadLeaseReviewTypes = {
        // Initialize loadLeaseReviewTypes
        if(LeaseReviewType.count() == 0) {
            List<LeaseReviewType> leaseReviewTypes = []
            leaseReviewTypes << new LeaseReviewType(name: "Lease Review");

            leaseReviewTypes.each { leaseReviewType ->
                leaseReviewType.save();
            }
        }
    }

    def initDevelopment() {

        log.info("bootstrapping data for the #DEVELOPMENT# environment");

        loadConfigTypes();

        loadRentalUnitTypes();

        loadJobTypes();

        loadLeaseReviewTypes();

        bootstrapJobSchedules();

        if(BillingCycle.count() != 0) {
            log.info("application has already been bootstrapped, skipping the whole process")
            return;
        }

        //Init the common lookups
        initLookups();

        //
        initRoutineType();
        initTransactionTypes();
        initBanks();

        //Initialize
        log.info("loading the user-roles and users");
        loadSecurityRoles();
        loadUsers();

        //Initialize the test data
        //initTestData();
    }

    def initRoutineType() {
        List<RoutineType> routineTypeList = []
        routineTypeList << new RoutineType(name: 'Rent Run')
        routineTypeList << new RoutineType(name: 'OpCos Run')
        routineTypeList << new RoutineType(name: 'Interest Run')

        routineTypeList.each{ RoutineType routineType ->
            if(!routineType.save()) {
                log.warn("failed to save the RoutineType '${routineType.name}'")
            }
        }
    }

    def initTestData() {
        //Initialize sample PropertyManagers
        List<PropertyManager> propertyManagers = []
        propertyManagers << new PropertyManager(firstName: 'John', lastName: 'Doe', emailAddress: 'john.doe@email.com', phoneNumber: '263732123456', appointmentDate: new Date().parse('yyyy-MM-dd','2013-05-10'));
        propertyManagers << new PropertyManager(firstName: 'Anna', lastName: 'Other', emailAddress: 'an.othere@email.com', phoneNumber: '263732123456', appointmentDate: new Date().parse('yyyy-MM-dd','2014-11-13'));
        propertyManagers.each { propertyManager ->
            propertyManager.save();
        }

        //Initialize sample tenant
        Tenant tenant = new Tenant()
        tenant.accountName = "A.N Other"
        tenant.accountNumber = "00110000001"    // overridden
        tenant.title = null
        tenant.accountType = AccountType.get(1)
        tenant.phoneNumber = "263732123456";
        tenant.faxNumber = "2637321213456"
        tenant.physicalAddress = '148 Seke Road'
        tenant.businessAddress = '148 Seke Road'
        tenant.emailAddress = "fleboho@gmail.com"
        tenant.bpNumber = "BP098726B"
        tenant.vatNumber = "R5436344B"
        tenant.accountCategory = AccountCategory.findByCode("tenant")

        if( !tenantService.save(tenant) ) {
            log.error("failed to save tenant: ${tenant}")
        }

        //Initialize sample landlord
        Landlord landlord = new Landlord()
        landlord.accountName = "CBZ Holdings"
        landlord.accountNumber = grailsApplication.config.getProperty("metasoft.config.singlelandlord.account") //"00200000001"
        landlord.title = null
        landlord.accountType = AccountType.get(1)
        landlord.phoneNumber = "263732123456";
        landlord.faxNumber = "2637321213456"
        landlord.physicalAddress = '148 Sapphire House'
        landlord.businessAddress = '148 Sapphire House'
        landlord.emailAddress = "sample@cbz.co.zw"
        landlord.bpNumber = "BP098726A"
        landlord.vatNumber = "R5436344C"
        landlord.accountCategory = AccountCategory.findByCode("landlord")

        //crete the landlord contact person
        ContactPerson contactPerson = new ContactPerson()
        contactPerson.emailAddress = 'fleboho@gmail.com'
        contactPerson.phoneNumber = '263732143190'
        contactPerson.firstName = "Farai"
        contactPerson.lastName = "Leboho"
        contactPerson.account = landlord;

        landlord.addToContactPersons(contactPerson);

        try {
            landlordService.save(landlord)
        } catch(ValidationException e) {
            log.error("errors while saving landlord: ${landlord.errors}")
        }
    }

    def bootstrapJobSchedules = {

        JobType jType = JobType.list()[0];

        def jobs = [
                [jobName: 'Bulk Adjustment', jobType: jType, triggerName: 'dailyBulkAdjustCronTrigger', cronExpression: "0 0 2 * * ? *", startDelay: 1500, active: true, jobDescription: 'Bulk Adjustment'],
                [jobName: 'Close Billing Cycle', jobType: jType, triggerName: 'closeBillingCycleCronTrigger', cronExpression: '0 0 1 8 * ? *', startDelay: 1500, active: true, jobDescription: 'Close Billing Cycle'],
                [jobName: 'Expire Passwords', jobType: jType, triggerName: 'expirePasswordsTrigger', cronExpression: "0 0 3 * * ?", startDelay: 1500, active: true, jobDescription: 'Job to expire passwords'],
                [jobName: 'Expire Leases', jobType: jType, triggerName: 'leaseExpireCronTrigger', cronExpression: "0 0 2 * * ? *", startDelay: 300, active: true, jobDescription: 'Process expired leases'],
        ];

        if(JobSchedule.count() == 0) {
            jobs.each{ job ->
                new JobSchedule(job).save();
            }
        }
    }

    def initCountries() {

        if(Country.count() == 0) {
            def countries = [
                    [countryName: 'Zimbabwe', 'countryCode': 'ZWE'],
                    [countryName: 'Zambia', 'countryCode': 'ZAM'],
                    [countryName: 'South Africa', 'countryCode': 'ZAR']
            ]

            countries.forEach{country ->
                new Country(country).save();
            }

        }

    }

    def initCurrencies() {

        if(Currency.count() == 0) {
            def currencies = [
                    [currencyCode: 'ZWL', 'numericCode': '932', currencyName: 'Zimbabwe Dollar', shortDescription: 'ZWL ($)', currencySign: '$'],
                    [currencyCode: 'USD', 'numericCode': '840', currencyName: 'US Dollar', shortDescription: 'USD ($)', currencySign: '$'],
                    [currencyCode: 'ZAR', 'numericCode': '710', currencyName: 'South African Rand', shortDescription: 'ZAR ($)', currencySign: 'R'],
            ]

            currencies.forEach{ Map currencyData ->
                Currency currency = new Currency(currencyData);
                currency.id = currency.currencyCode;
                currency.save();
            }

        }

    }

    def initExchangeRates() {

        if(ExchangeRate.count() == 0) {
            def rates = [
                    [fromCurrency: 'ZWL', 'toCurrency': 'ZWL', rate: 1.00, active: true, startDate: '1900-01-01', endDate: '3000-12-31']
            ]
            rates.forEach{ rate ->
                new ExchangeRate(rate).save();
            }
        }

    }
}
