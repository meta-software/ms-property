<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'receiptBatchCheck.label', default: 'ReceiptBatchCheck')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'receiptBatchTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="receipt-check-list">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Pending Receipts</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="select-all" class="icheck" />
                            </th>
                            <th>Batch Reference</th>
                            <th>Batch Number</th>
                            <th>Transaction Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="receiptBatchCheck in receiptBatchChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                        </th>
                            <td>
                                <router-link class="display-block" :to="{name: 'show', params: {id: receiptBatchCheck.id}}">{{receiptBatchCheck.batchReference}}</router-link>
                            </td>
                            <td>{{receiptBatchCheck.batchReference}}</td>
                            <td>{{receiptBatchCheck.transactionType.name}}</td>
                            <td>{{receiptBatchCheck.dateCreated | formatDate}}</td>
                            <td>{{receiptBatchCheck.maker.fullName}}</td>
                        </tr>
                        <tr v-if="receiptBatchChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="receipt-check-outbox">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Outbox Transaction Batches</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="all" class="icheck" />
                            </th>
                            <th>Batch Reference</th>
                            <th>Batch Number</th>
                            <th>Transaction Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="receiptBatchCheck in receiptBatchChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                        </th>
                            <td>
                                <router-link class="display-block" :to="{name: 'outbox-item', params: {id: receiptBatchCheck.id}}">{{receiptBatchCheck.batchReference}}</router-link>
                            </td>
                            <td>{{receiptBatchCheck.batchReference}}</td>
                            <td>{{receiptBatchCheck.transactionType.name}}</td>
                            <td>{{receiptBatchCheck.dateCreated | formatDate}}</td>
                            <td>{{receiptBatchCheck.maker.fullName}}</td>
                        </tr>
                        <tr v-if="receiptBatchChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="receipt-check-show">
    <section>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="portlet light bg-inverse dblue-hoki margin-top-10">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Batch Transaction
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        ID:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Status:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Action:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Entity:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.entity.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Date Created:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Maker:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Transaction Type:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.transactionType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Batch Reference:
                                    </label>
                                    <div class="col-md-7">{{receiptBatchCheck.batchReference}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <maker-checker :reload="false" :makerid="receiptBatchCheck.maker.id" :item-id="receiptBatchCheck.id" :controller="'receipt-check'" :status="receiptBatchCheck.checkStatus" @approved="onPosted" @rejected="onRejected"  ></maker-checker>

        <div class="row">
            <receipt-items v-if="receiptBatchCheck.entity.id" :receipt-batch-check-id="receiptBatchCheck.entity.id"></receipt-items>
        </div>

    </section>
</template>

<template id="receipt-check-outbox-item">
    <section>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Batch Transaction
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        ID:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Status:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Action:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Entity:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.entity.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Date Created:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Maker:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Transaction Type:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.transactionType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Batch Reference:
                                    </label>
                                    <div class="col-md-8">{{receiptBatchCheck.batchReference}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <maker-checker @rejected="rejected" :item-id="receiptBatchCheck.id" :controller="'receipt-batch-check'" :status="receiptBatchCheck.checkStatus"></maker-checker>

        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-eye"></i> Viewing Batch Transaction : {{receiptBatchCheck.batchReference}}
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <trans-list-comp :receipt-batch="receiptBatchCheck.receiptBatchTmp"></trans-list-comp>
                    </div>
                </div>
            </div>
        </div>
    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Receipts
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Receipts</router-link>
            </div>
        </div>
    </div>
</template>

<template id="receipt-items">
    <section>
        <div class="inbox margin-top-10">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="portlet light bordered bg-inverse">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-3 col-xs-6 col-sm-4">
                                <label>Receipts Count</label>
                                <div>0</div>
                            </div>
                            <div class="col-md-3 col-xs-6 col-sm-4">
                                <label>Batch Total</label>
                                <div>$ 0.00</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-eye"></i> Receipts List
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">
                                <table class="table table-bordered table-condensed table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 18%;">Credit Acc</th>
                                        <th style="width: 18%;">Debit Acc</th>
                                        <th style="width: 10%;">Reference</th>
                                        <th style="width: 10%;">Txn Date</th>
                                        <th style="width: 10%;">Currency</th>
                                        <th>Narrative</th>
                                        <th style="width: 10%;">Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody class="trans-list-tbody">
                                    <tr v-for="(receipt, idx) in receiptItems" :key="idx">
                                        <td>{{receipt.tenant.accountNumber}} : {{receipt.tenant.accountName}}</td>
                                        <td>{{receipt.debitAccount.accountNumber}} : {{receipt.debitAccount.accountName}}</td>
                                        <td>{{receipt.transactionReference}}</td>
                                        <td>{{receipt.transactionDate | formatDate('YYYY-MM-DD')}}</td>
                                        <td>{{receipt.transactionCurrency.id}}</td>
                                        <td :title="receipt.narrative">{{ receipt.narrative | shortify(40) }}</td>
                                        <td>{{receipt.receiptAmount | formatNumber(2)}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/receipt-batch-check/receipt-batch-check.js" asset-defer="true"/>

</body>
</html>