<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyCheck.label', default: 'Pending Property')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'propertyCheck.list.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="propertyCheck.list.label" args="[entityName]" /></title>
</head>

<body>

<g:render template="page_bar" />

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message" />
</g:if>

<div class="row">
    <div class="col-md-12">

        <div class="portlet">
            <div class="portlet-body">
                <div class="table-scrollable" >
                    <g:render template="list" />
                </div>
            </div>
        </div>

        <div class="pagination">
            <mp:pagination total="${propertyCheckCount ?: 0}" />
        </div>

    </div>
</div>

<asset:javascript src="propertyCheck-form.js" asset-defer="true"/>

</body>
</html>