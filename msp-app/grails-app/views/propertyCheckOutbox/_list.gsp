<div class="search-page search-content-4 margin-top-10">
    <loading :active.sync="loader.isLoading"
             :can-cancel="false"
             :is-full-page="loader.fullPage" ></loading>
    <div class="search-bar bordered">
        <form role="form" autocomplete="off" @submit.prevent="search">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" class="form-control" placeholder="Query Request..."  v-model="params.q">
                </div>
                <div class="col-lg-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                    <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <!-- add table container here -->
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Action</th>
                <th>Entity</th>
                <th>Status</th>
                <th>Date Created</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${propertyCheckList}">
                <g:each in="${propertyCheckList}" var="propertyCheck">
                    <tr>
                        <td>
                            <g:link class="display-block" url="/property-check-outbox/show/${propertyCheck.id}" >
                                ${propertyCheck.id}
                            </g:link>
                        </td>
                        <td>${propertyCheck.actionName}</td>
                        <td>
                            <g:link class="display-block" url="/property-check-outbox/show/${propertyCheck.id}" >${propertyCheck.entity}</g:link>
                        </td>
                        <td>${propertyCheck.checkStatus}</td>
                        <td><g:formatDate date="${propertyCheck.makeDate}" /></td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="7">
                        <mp:noRecordsFound />
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
            <!-- add table container here -->
    </div>
</div>


