<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'billingCycle.label', default: 'Billing Cycle')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'billingCycle.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="billingCycle.list.label" args="[entityName]"/></title>
</head>

<body>

<!-- begin: app -->
<div id="app"></div>

<!-- begin: general-ledger-app -->
<template id="bc-app">
    <section>
        <page-bar></page-bar>

        <router-view></router-view>
    </section>
</template>
<!-- begin: general-ledger-app -->

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-calendar-times-o"></i> Billing Cycles
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <button type="button" @click="createCycle" class="btn btn-sm blue-hoki" >
                <i class="fa fa-plus"></i> Create Cycle
            </button>
            <router-link class="btn btn-sm default" to="/">
                <i class="fa fa-list"></i> Billing Cycles
            </router-link>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<!-- begin: general-ledger-approved template -->
<template id="bc-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" v-model="params.query" class="form-control" placeholder="Billing Cycle...." >
                    </div>
                    <div class="col-lg-2 col-md-2">
                        <select v-model="params.open" class="form-control" placeholder="Open?">
                            <option selected="selected">All..</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="submit" @click.prevent="search">Search</button>
                        <button class="btn green" type="button"><i class="fa fa-refresh" @click="reset"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Active ?</th>
                    <th>Open ?</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(billingCycle, key) in billingCycles" :key="key" :class="{'active-cycle': billingCycle.active}">
                    <td>{{billingCycle.name}}</td>
                    <td>{{billingCycle.startDate | formatDate}}</td>
                    <td>{{billingCycle.endDate | formatDate}}</td>
                    <td><span class="label label" :class="[billingCycle.active ? 'label-success' : 'label-danger']" >{{billingCycle.active | formatBoolean}}</span></td>
                    <td><span class="label label" :class="[billingCycle.open ? 'label-success' : 'label-danger']" >{{billingCycle.open | formatBoolean}}</span></td>
                    <td><router-link class="btn btn-xs btn-success" :to="{path: '/show/'+billingCycle.id}"><i class="fa fa-eye"></i> View</router-link></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>
<!-- end: general-ledger-approved template -->

<!-- begin show -->
<template id="bc-show">
    <div class="row">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Billing Cycle Details
                    </div>

                    <div class="actions">
                        <button type="button" class="btn btn-danger btn-sm" @click="$router.go(-1)"><i class="fa fa-arrow-left"></i> Back</button>
                    </div>
                </div>

                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Name:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{billingCycle.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Start Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{billingCycle.startDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        End Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{billingCycle.endDate | formatDate}}</div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Active?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[billingCycle.active ? 'label-success' : 'label-danger']" >{{billingCycle.active | formatBoolean}}</span></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Open?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[billingCycle.open ? 'label-success' : 'label-danger']" >{{billingCycle.open | formatBoolean}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="button" :disabled="billingCycle.active" class="btn btn-sm red-mint" @click="close"><i class="fa fa-close"></i> Close Cycle</button>
                        <button type="button" class="btn btn-sm btn-success" @click="open"><i class="fa fa-folder-open"></i> Open Cycle</button>
                        <button type="button" class="btn btn-sm green-dark" @click="activate"><i class="fa fa-check"></i> Activate Cycle</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</template>
<!-- end show-->

<!-- bc-create -->
<template id="bc-create">
    <div class="portlet light bordered margin-top-10">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-plus"></i> Billing Cycle Form</div>
        </div>

        <div class="portlet-body form">
            <form role="form" @submit.prevent="save">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input name="name" class="form-control" type="text" v-model="billingCycle.name" v-validate="'required'">
                                <span class="has-error help-block-error help-block">{{ errors.first('name') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Description:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input name="shortDescription" class="form-control" type="text" v-model="billingCycle.shortDescription" v-validate="'required'">
                                <span class="has-error help-block-error help-block">{{ errors.first('shortDescription') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Start Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <vdate-picker v-validate="'required'" v-model="billingCycle.startDate" name="startDate" class="form-control"></vdate-picker>
                                <span class="has-error help-block-error help-block">{{ errors.first('startDate') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">End Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <vdate-picker v-validate="'required'" v-model="billingCycle.endDate" name="endDate" class="form-control"></vdate-picker>
                                <span class="has-error help-block-error help-block">{{ errors.first('endDate') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Open:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="checkbox" v-model="billingCycle.open" value="1" name="open"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="button" class="btn btn-sm blue-hoki" @click="save"><i class="fa fa-save"></i> Save</button>
                    <button type="button" class="btn btn-sm default" @click="cancel"><i class="fa fa-times"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
</template>
<!-- bc-create -->

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/billingCycle/billing-cycle-app.js" asset-defer="true"/>

</body>
</html>