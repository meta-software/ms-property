<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4><i class="fa fa-user"></i> ${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>
</div>