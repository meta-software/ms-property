<!DOCTYPE html>
<html>
<head>
	<!-- BEGIN TEMPALTE VARS -->
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
	<!-- END TEMPALTE VARS -->

	<meta name="layout" content="main2"/>
	<title><g:message code="profile.password.update" default="Update Password - Initial Login"/></title>

    <script type="text/javascript">
        var userId = ${user.id};
    </script>

</head>

<body>

<div id="main-app">

	<div class="page-bar">
		<div class="page-bar-header">
			<h4>Update Password Request</h4>
		</div>
	</div>

	<g:if test="${flash.message}">
		<g:render template="/templates/flash_message"/>
	</g:if>

	<g:hasErrors bean="${this.user}">
		<g:render template="/templates/errors" model="['obj': this.user]"/>
	</g:hasErrors>

    <div class="portlet light margin-top-10">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <p class="note note-danger">Please update your password to login to the application.</p>
                </div>
            </div>
        </div>
    </div>

	<g:form action="update-password" controller="profile" class="form-horizontal" ref="updatePasswordForm">
		<div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-refresh"></i> Change Password</div>
            </div>
			<div class="portlet-body form">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                Current Password <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input class="form-control" type="password" name="currentPassword" v-validate="'required'" v-model="updatePasswordForm.currentPassword"/>
                                <span class="has-error help-block help-block-error">{{ errors.first('currentPassword') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="divider"></div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                New Password <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" type="password" v-model="updatePasswordForm.password" name="newPassword" class="form-control" />
                                <span class="has-error help-block help-block-error">{{ errors.first('newPassword') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                Confirm Password <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" type="password" v-model="updatePasswordForm.confirmPassword" name="confirmPassword" class="form-control" />
                                <span class="has-error help-block help-block-error">{{ errors.first('confirmPassword') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <button @click="updatePassword" class="btn btn-default btn-success"><i class="fa fa-save"></i> Change Password</button>
                </div>

			</div>
		</div>
	</g:form>

</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/user/user-init-login.js" asset-defer="true"/>

</body>
</html>
