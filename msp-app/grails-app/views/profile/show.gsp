<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Metafost Property :: [Real Estate management] - Dashboard</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Dashboard :: MetasoftWare" scope="request"/>
    <g:set var="pageHeadTitle" value="${message(code: 'profile.show.label', default: 'User Profile')}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <script type="text/javascript">
        var userId = ${user.id};
    </script>

</head>

<body>

<div id="main-app">
    <g:render template="page_bar"/>

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-8">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-user"></i> Profile
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table">
                        <tbody>
                        <tr><th style="width: 40%">Name: </th><td>${user.fullName}</td></tr>
                        <tr><th>Username: </th><td>${user.username}</td></tr>
                        <tr><th>Email: </th><td>${user.emailAddress}</td></tr>
                        </td></tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="actions">
                <button type="button" @click="openModal=true" class="btn blue-hoki" >
                    <i class="fa fa-rotate-left"></i> Change Password
                </button>
            </div>

        </div>
        <div class="col-md-4">
            &nbsp;
        </div>
    </div>

    <modal v-model="openModal" size="md" ref="updatePasswordModal" id="update-password-form-modal" @hide="passwordModalCallback" >
        <span slot="title"><i class="fa fa-rotate-left"></i> Change Password</span>

        <g:form controller="profile" ref="updatePasswordForm" action="update-password" method="POST" role="form" >
            <g:hiddenField name="id" value="${user.id}" />
            <g:hiddenField name="version" value="${user?.version}" />

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Current Password
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-8">
                            <input type="password" class="form-control" name="currentPassword" v-validate="'required'" v-model="updatePasswordForm.currentPassword" />
                            <span class="has-error help-block help-block-error">{{ errors.first('currentPassword') }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">New Password
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-8">
                            <input type="password" class="form-control" name="password" v-validate="'required'" v-model="updatePasswordForm.password" />
                            <span class="has-error help-block help-block-error">{{ errors.first('password') }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label col-md-4">Confirm Password
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-8">
                            <input type="password" class="form-control" name="confirmPassword" v-validate="'required'" v-model="updatePasswordForm.confirmPassword" />
                            <span class="has-error help-block help-block-error">{{ errors.first('confirmPassword') }}</span>
                        </div>
                    </div>
                </div>
            </div>

        </g:form>

        <div slot="footer">
            <btn @click="openModal=false"><i class="fa fa-times"></i> Cancel</btn>
            <btn @click="updatePassword" class="blue-hoki" ><i class="fa fa-save"></i> Update Password</btn>
        </div>

    </modal>

</div>

<!-- END PAGE BASE CONTENT -->
<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/user/user-profile.js" asset-defer="true"/>

</body>
</html>
