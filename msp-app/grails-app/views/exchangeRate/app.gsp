<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'exchangeRate.label', default: 'Exchange Rate')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'exchangeRate.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="exchangeRate.list.label" args="[entityName]"/></title>
</head>

<body>

<!-- begin: app -->
<div id="app"></div>

<!-- begin: general-ledger-app -->
<template id="er-app">
    <section>
        <page-bar></page-bar>

        <router-view></router-view>
    </section>
</template>
<!-- begin: general-ledger-app -->

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-dollar"></i> Exchange Rates
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <button type="button" @click="createRate" class="btn btn-sm blue-hoki" >
                <i class="fa fa-plus"></i> Create Exchange Rate
            </button>
            <router-link class="btn btn-sm default" to="/">
                <i class="fa fa-list"></i> Exchange Rates
            </router-link>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<!-- begin: general-ledger-approved template -->
<template id="er-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" v-model="params.query" class="form-control" placeholder="Exchange Rate...." >
                    </div>
                    <div class="col-lg-2 col-md-2">
                        <select v-model="params.open" class="form-control" placeholder="Open?">
                            <option selected="selected">All..</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="submit" @click.prevent="search">Search</button>
                        <button class="btn green" type="button"><i class="fa fa-refresh" @click="reset"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>Effective Date</th>
                    <th>From Currency</th>
                    <th>To Currency</th>
                    <th>Exchange Rate</th>
                    <th>Active ?</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(exchangeRate, key) in exchangeRates" :key="key" :class="{'active-rate': exchangeRate.active}">
                    <td>{{exchangeRate.startDate | formatDate}}</td>
                    <td>{{exchangeRate.fromCurrency.shortDescription}}</td>
                    <td>{{exchangeRate.toCurrency.shortDescription}}</td>
                    <td class="number">{{exchangeRate.rate | formatNumber(4)}}</td>
                    <td><span class="mc-status label" :class="[exchangeRate.active ? 'label-success' : 'label-danger']" >{{exchangeRate.active | formatBoolean}}</span></td>
                    <td>
                        <button type="button" class="btn btn-xs btn-outline btn-circle" disabled="disabled"><i class="fa fa-ellipsis-h"></i></button>
                    </td>
                </tr>
                <tr v-if="exchangeRates.length===0">
                    <td colspan="6"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>
<!-- end: general-ledger-approved template -->

<!-- begin show -->
<template id="er-show">
    <div class="row">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Exchange Rate Details
                    </div>

                    <div class="actions">
                        <button type="button" class="btn btn-danger btn-sm" @click="$router.go(-1)"><i class="fa fa-arrow-left"></i> Back</button>
                    </div>
                </div>

                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Name:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{exchangeRate.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Start Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{exchangeRate.startDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        End Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{exchangeRate.endDate | formatDate}}</div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Active?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[exchangeRate.active ? 'label-success' : 'label-danger']" >{{exchangeRate.active | formatBoolean}}</span></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Open?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[exchangeRate.open ? 'label-success' : 'label-danger']" >{{exchangeRate.open | formatBoolean}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="button" class="btn btn-sm red-mint" @click="close"><i class="fa fa-close"></i> Close Rate</button>
                        <button type="button" class="btn btn-sm btn-success" @click="open"><i class="fa fa-folder-open"></i> Open Rate</button>
                        <button type="button" class="btn btn-sm green-dark" @click="activate"><i class="fa fa-check"></i> Activate Rate</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</template>
<!-- end show-->

<!-- er-create -->
<template id="er-create">
    <div class="portlet light bordered margin-top-10">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-plus"></i> Exchange Rate Form</div>
        </div>

        <div class="portlet-body form">
            <form role="form" @submit.prevent="save">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">From Currency:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <currency-select v-validate="'required'" class="form-control" name="fromCurrency" v-model="exchangeRate.fromCurrency" ></currency-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('fromCurrency') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">To Currency:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <currency-select v-validate="'required'" class="form-control" name="toCurrency" v-model="exchangeRate.toCurrency" ></currency-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('toCurrency') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Rate:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input placeholder="0.00" name="rate" class="form-control" type="text" v-model="exchangeRate.rate" v-validate="'required'">
                                <span class="has-error help-block-error help-block">{{ errors.first('rate') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Start Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <datepicker v-validate="'required'" v-model="exchangeRate.startDate" name="startDate" class="form-control"></datepicker>
                                <span class="has-error help-block-error help-block">{{ errors.first('startDate') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">End Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <datepicker v-validate="'required'" v-model="exchangeRate.endDate" name="endDate" class="form-control"></datepicker>
                                <span class="has-error help-block-error help-block">{{ errors.first('endDate') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Open:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="checkbox" v-model="exchangeRate.active" value="1" name="active"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="button" class="btn btn-sm blue-hoki" @click="save"><i class="fa fa-save"></i> Save</button>
                    <button type="button" class="btn btn-sm default" @click="cancel"><i class="fa fa-times"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
</template>
<!-- er-create -->

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/exchangeRate/exchange-rate-app.js" asset-defer="true"/>

</body>
</html>