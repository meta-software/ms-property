<div class="actions entity-actions">

    <g:link action="create" class="btn btn-sm btn-primary" >
        <i class="fa fa-plus"></i>
        <span>Add Supplier</span>
    </g:link>

</div>
