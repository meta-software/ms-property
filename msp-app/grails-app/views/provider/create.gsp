<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'provider.label', default: 'Supplier')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'provider.create.label', default: entityName)}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="provider.create.label" args="[entityName]"/></title>
</head>

<body>

<div class="content" id="provider-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.provider}">
        <g:render template="/templates/errors"/>
    </g:hasErrors>

    <g:form action="save" controller="provider" method="POST" autocomplete="off" class="form-horizontal"
            name="provider-form">
        <g:render template="form" bean="provider"/>
    </g:form>
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/provider/provider-form.js" asset-defer="true"/>

</body>
</html>
