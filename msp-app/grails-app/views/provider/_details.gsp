<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <div class="portlet no-padding">
            <div class="portlet-body no-padding">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_0" data-toggle="tab">Supplier Info</a>
                        </li>
                        <li>
                            <a href="#tab_1" data-toggle="tab">Properties <span
                                    class="badge badge-info">${buildingsCount}</span></a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab">Standing Orders <span
                                    class="badge badge-success">${standingOrdersCount}</span></a>
                        </li>
                        <li>
                            <a href="#tab_3" data-toggle="tab">Lessor Payments <span
                                    class="badge badge-success">${lessorPaymentsCount}</span></a>
                        </li>
                    </ul>

                    <div class="tab-content" style="padding-top: 10px;">
                        <div class="tab-pane active" id="tab_0">
                            <!-- START FORM PORTLET -->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-info "></i>Supplier Info - ${landlord}
                                    </div>

                                    <div class="actions">
                                        <g:link class="btn green btn-outline" controller="landlord" action="edit"
                                                id="${landlord.id}">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </g:link>
                                    </div>

                                </div>

                                <div class="portlet-body">

                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-horizontal-left" role="form">
                                        <div class="form-body">
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Account Name:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">${landlord.accountName}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Account No:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${landlord.accountNumber}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Account Type:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${landlord.accountType}

                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                            </div>
                                            <!--/row-->

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Postal Address:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${landlord.postalAddress}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Business Address:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${landlord.businessAddress}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Created Date:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                <g:formatDate date="${landlord.dateCreated}"
                                                                              format="dd/MMM/yyyy"/>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                            </div>

                                        </div>
                                    </form>
                                    <!-- END FORM-->

                                </div>
                            </div>
                            <!-- END FORM PORTLET -->

                            <!-- START PORTLET -->
                            <div class="portlet box blue-dark">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-book"></i>
                                        Account Details
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <span class="form-control-static">Balance:</span>
                                                </div>

                                                <div class="col-md-7">
                                                    <p class="form-control-static">
                                                        <g:formatNumber number="${0}"
                                                                        type="currency" currencyCode="USD"/></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <span class="form-control-static">Last Activity:</span>
                                                </div>

                                                <div class="col-md-7">
                                                    <p class="form-control-static">
                                                        <g:formatDate number="${landlord.lastUpdated}"
                                                                      format="dd-MMM-yyyy"/></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>
                            </div>
                            <!-- END PORTLET -->

                        </div>

                        <div class="tab-pane" id="tab_1">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-building"></i>Properties</div>

                                    <div class="actions">
                                        <g:link class="btn blue btn-outline" controller="property" action="create"
                                                params="['landlord.id': landlord.id]">
                                            <i class="fa fa-plus"></i>
                                            Add Property
                                        </g:link>
                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <!-- BEGIN PROPERTIES LIST-->
                                    <g:render template="properties"/>
                                    <!-- BEGIN PROPERTIES LIST-->
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-calendar"></i> Standing Orders</div>

                                    <div class="actions">
                                        <!--
                                        <g:link class="btn btn-sm btn-primary" controller="standingOrder"
                                                action="create"
                                                params="['landlord.id': landlord.id]">
                                            <i class="fa fa-plus"></i> Create
                                        </g:link> -->

                                        <td>
                                            <a class="btn blue btn-outline" data-toggle="modal"
                                               href="#_standingOrderFormModal">
                                                <i class="fa fa-plus"></i> Add Standing Order
                                            </a>
                                        </td>

                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <!-- BEGIN STANDING-ORDERS LIST-->
                                    <g:render template="standingOrders"
                                              model="[standingOrders: standingOrders]"/>
                                    <!-- END STANDING-ORDERS LIST-->
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="tab_3">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption"><i class="fa fa-calendar"></i>Lessor Payments</div>

                                    <div class="actions">
                                        <!--
                                        <g:link class="btn btn-sm btn-primary" controller="lessorPayment"
                                                action="create"
                                                params="['landlord.id': landlord.id]">
                                            <i class="fa fa-plus"></i> Create
                                        </g:link> -->

                                        <td>
                                            <a class="btn blue btn-outline" data-toggle="modal"
                                               href="#_lessorPaymentFormModal">
                                                <i class="fa fa-plus"></i> Add Lessor Payment
                                            </a>
                                        </td>

                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <!-- BEGIN STANDING-ORDERS LIST-->
                                    <g:render template="lessorPayments"
                                              model="[lessorPayments: lessorPayments]"/>
                                    <!-- END STANDING-ORDERS LIST-->
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODALS -->
<div class="modal fade" id="_standingOrderFormModal" tabindex="-1" role="basic" aria-hidden="true">
    <g:form method="POST" controller="standingOrder" action="save">
        <g:hiddenField name="account" value="${landlord.id}"/>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create Standing Order</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">
                        <div class="row">
                            <!-- span -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Paycode</label>
                                    <g:select from="${metasoft.property.core.Paycode.list()}"
                                              noSelection="['': 'Paycode...']"
                                              optionKey="id"
                                              name="paycode"
                                              class="form-control select2"
                                              id="_so-paycode"/>
                                </div>
                            </div>
                            <!--span-->
                            <!--span-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Reference</label>
                                    <g:field type="text" name="reference" id="_standingOrder-reference"
                                             class="form-control"
                                             placeholder="reference"/>
                                </div>
                            </div>
                            <!--/span-->
                            <!-- span -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Is Active ?</label>

                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" name="active" id="_standingOrder-active" value="1"
                                                   checked="checked">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- span -->

                        </div>
                        <!--/row-->
                        <div class="row">

                            <!--span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <g:field type="text" name="description" id="_standingOrder-description"
                                             class="form-control"/>
                                </div>
                            </div>
                            <!--/span-->

                            <!--span-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Amount</label>


                                    <div class="input-group _so-amount-cls">
                                        <span class="input-group-addon">
                                            $
                                        </span>
                                        <g:field type="text" name="amount" id="_standingOrder-amount"
                                                 class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <!-- span -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Is Percentage ?</label>

                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" name="percentage" id="_so-percentage" value="1">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- span -->
                        </div>
                        <!--/row-->
                    </div>
                    <!-- END FORM BODY-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green" id="_standingOrder-submit">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </g:form>
<!-- /.modal-dialog -->
</div>
<!-- END MODALS -->

<asset:script>
    $(document).ready(function(){

        var handleCheck = function () {

            $soAmountCls = $("._so-amount-cls");
            $inputGroupAddon = $("._so-amount-cls").find(".input-group-addon");

            console.log("running handleCheck()");
            if($("#_so-percentage").is(':checked')) {
                $inputGroupAddon.html("%");
            } else {
                $inputGroupAddon.html("$");
            }
        }

        $("#_so-percentage").change(function(){
            handleCheck();
        });

        handleCheck();
    })
</asset:script>