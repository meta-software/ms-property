<div class="page-bar-show">
    <div class="page-bar-show-title">
        <div class="page-bar-show-header">
            <h4 class="page-bar-title">
                <i class="fa fa-user"></i>
                Supplier : ${provider.accountName}
            </h4>
            <div class="page-bar-show-subtitle">
                <span>${provider.accountNumber}</span>
                | <span>${provider.phoneNumber}</span>
                | <span>${provider.emailAddress}</span>
                | <mp:makerCheck value="${provider.dirtyStatus}" />
            </div>
        </div>
        <div class="page-toolbar">
            <div class="page-toolbar-context">
                <g:if test="${provider.isChecked()}">
                    <mp:editRecord bean="${provider}" controller="provider" action="edit" id="${provider.id}" class="btn btn-sm blue-hoki" >
                        <i class="fa fa-edit"></i> Edit Record
                    </mp:editRecord>
                </g:if>
            </div>

            <g:link controller="provider" class="btn btn-sm default" ><i class="fa fa-list"></i> Suppliers</g:link>
        </div>
    </div>

    <!--div class="page-bar-show-actions">
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Edit</a>
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Manage Properties</a>
    </div-->
</div>