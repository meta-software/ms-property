<%@ page import="metasoft.property.core.AccountType" %>

<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <g:select optionKey="id"
                              noSelection="['': 'All Types...']"
                              class="form-control select2-no-search"
                              name="accountType"
                              value="${params?.accountType}"
                              from="${AccountType.list()}"/>
                </div>
                <div class="col-lg-2 col-md-2">
                    <input type="text" class="form-control" placeholder="Account Name" name="accountName" value="${params?.accountName}">
                </div>
                <div class="col-lg-2 col-md-2">
                    <input type="text" class="form-control" name="accountNumber" placeholder="Account Number" value="${params?.accountNumber}">
                </div>
                <div class="col-lg-2 col-md-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead class="bg-grey-cararra">
            <tr>
                <th>Name</th>
                <th>Account No</th>
                <th>Account Type</th>
                <th>VAT Number</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${providerList}">
                <g:each in="${providerList}" var="provider">
                    <tr>
                        <td>
                            <g:link class="display-block" action="show" id="${provider.id}">${provider}</g:link>
                        </td>
                        <td>${provider.accountNumber}</td>
                        <td>${provider.accountType}</td>
                        <td>${provider?.vatNumber}</td>
                        <td><mp:makerCheck value="${provider.dirtyStatus}" classes="label-sm" /></td>
                    </tr>
                </g:each>
            </g:if>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${providerCount ?: 0}" />
    </div>
</div>
