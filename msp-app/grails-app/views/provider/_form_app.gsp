<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>
<%@ page import="metasoft.property.core.AccountCategory" %>
<%@ page import="metasoft.property.core.AccountType" %>
<%@ page import="metasoft.property.core.Title" %>

<!-- BEGIN FORM PORTLET-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Supplier Form</div>

                <div class="actions">
                    <button type="button" class="btn btn-sm" @click="onCancel" >
                        <i class="fa fa-times"></i> Cancel
                    </button>
                    <button type="button" class="btn btn-sm green " @click="onSaveSupplier" >
                        <i class="fa fa-check"></i> Save
                    </button>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${provider.errors['accountType'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Account Type
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select from="${AccountType.list()}"
                                          ref="accountType"
                                          optionKey="id"
                                          name="accountTypeSelect"
                                          noSelection="['null': 'Account Type...']"
                                          value="${provider?.accountType?.id}"
                                          class="form-control select2-no-searbanch"/>
                                <input v-validate="'required'" type="hidden" name="accountType" v-model="provider.accountType.id" />
                                <span class="has-error help-block-error help-block">{{ errors.first('accountType') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ${provider.errors['phoneNumber'] ? 'has-error' : ''}">
                            <label class="control-label col-md-4">Phone Number
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field v-validate="'required|max:12|numeric'" class="form-control" type="text" v-model="provider.phoneNumber" name="phoneNumber"
                                         value="${this.provider.phoneNumber}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('phoneNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${provider.errors['accountName'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Account Name
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field v-validate="'required'" class="form-control" v-model="provider.accountName" type="text" name="accountName" value="${provider.accountName}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('accountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${provider.errors['physicalAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Physical Address
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field type="text" v-model="provider.physicalAddress" class="form-control" name="physicalAddress"
                                         value="${provider.physicalAddress}"
                                         v-validate="'required'"
                                         placeholder="Physical Address"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('physicalAddress') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${provider.errors['postalAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Postal Address</label>

                            <div class="col-md-8">
                                <g:field type="text" v-model="provider.postalAddress" class="form-control" name="postalAddress"
                                         value="${provider.postalAddress}"
                                         v-validate="'required'"
                                         placeholder="Postal Address"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('postalAddress') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${provider.errors['businessAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Business Address
                            </label>

                            <div class="col-md-8">
                                <g:field type="text" v-model="provider.businessAddress" class="form-control" name="businessAddress"
                                         value="${provider.businessAddress}"
                                         placeholder="Business Address"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('businessAddress') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${provider.errors['faxNumber'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Cell No</label>

                            <div class="col-md-8">
                                <g:field v-validate="'max:12|numeric'" class="form-control" v-model="provider.faxNumber" type="text" name="cellNumber"
                                         value="${this.provider.faxNumber}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('cellNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${provider.errors['emailAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Email
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="provider.emailAddress" type="text" name="emailAddress"
                                         v-validate="'required|email'"
                                         value="${this.provider.emailAddress}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('emailAddress') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group${provider.errors['vatNumber'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">VAT Number</label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="provider.vatNumber" type="text" name="vatNumber"
                                         value="${this.provider.vatNumber}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('vatNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="form-section">Bank Account</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.name', default: 'Account Name')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="provider.bankAccount.accountName" class="form-control" placeholder="" type="text" name="bankAccountName" />
                                <span class="has-error help-block help-block-error">{{ errors.first('bankAccountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyType', default: 'Bank')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select name="bank"
                                          v-model="provider.bankAccount.bank.id"
                                          v-validate="'required'"
                                          noSelection="['': 'Select Bank...']"
                                          from="${metasoft.property.core.Bank.list()}"
                                          class="form-control"
                                          optionKey="id"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('bank') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyManager', default: 'Account Number')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="provider.bankAccount.accountNumber" class="form-control" placeholder="" type="text" name="accountNumber" />
                                <span class="has-error help-block help-block-error">{{ errors.first('accountNumber') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Branch
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="provider.bankAccount.branch" name="branch" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('branch') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Branch Code
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="provider.bankAccount.branchCode" name="branchCode" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('branchCode') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- END FORM PORTLET-->