<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'provider.label', default: 'Supplier')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'provider.edit.label', default: entityName, args: [provider])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="provider.edit.label" args="[entityName]"/></title>

    <script type="text/javascript">
        let id = ${provider.id};
    </script>

</head>

<body>

<div class="content" id="provider-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.provider}">
        <g:render template="/templates/errors" model="['obj': this.provider]"/>
    </g:hasErrors>

    <g:form id="${this.provider.id}" action="update" method="PUT" role="form" class="form-horizontal">
        <g:hiddenField name="id" value="${this.provider.id}"/>
        <g:hiddenField name="version" value="${this.provider?.version}"/>

        <g:render template="form"/>
    </g:form>
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/provider/provider-edit.js" asset-defer="true"/>

</body>
</html>
