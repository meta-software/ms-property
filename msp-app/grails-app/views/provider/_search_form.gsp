<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.AccountType" %>

<!-- BEGIN Portlet PORTLET-->
<div class="portlet box yellow m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Account Type</label>
                            <g:select optionKey="id"
                                      noSelection="['': 'All Types...']"
                                      class="form-control select2-no-search"
                                      name="accountType"
                                      value="${params?.accountType}"
                                      from="${AccountType.list()}"/>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Account Name</label>
                            <input type="text" class="form-control" name="accountName" value="${params?.accountName}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Account Number</label>
                            <input type="text" class="form-control" name="accountNumber"
                                   value="${params?.accountNumber}">
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-sm blue-dark"><i class="fa fa-search"></i> Search...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->