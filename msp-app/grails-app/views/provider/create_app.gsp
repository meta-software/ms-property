<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'provider.label', default: 'Supplier')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'provider.create.label', default: entityName)}" scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>f

<body>

<div class="content" id="provider-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.provider}">
        <g:render template="/templates/errors"/>
    </g:hasErrors>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Supplier Form</div>
                </div>

                <div class="portlet-body form">
                    <g:form action="save" controller="provider" method="POST" autocomplete="off" class="form-horizontal"
                            name="provider-form">
                        <g:render template="form_app" bean="provider"/>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- paycode jquery form -->
<asset:javascript src="vue-apps/provider/provider-form.js" asset-defer="true"/>
<!-- paycode jquery form -->

</body>
</html>
