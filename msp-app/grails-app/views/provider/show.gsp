<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'provider.label', default: 'Supplier')}"/>
    <g:set var="pageHeadTitle" value="Supplier Info - ${provider}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Supplier - ${this.provider}" args="[entityName]"/></title>

</head>

<body>

<div id="supplier-show-app">
    <g:render template="page_bar_show"/>

    <div class="row margin-top-10">
        <div class="col-md-12 col-lg-12">
            <div class="tabbable-line boxless tabbable-reversed">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_0" data-toggle="tab">Summary</a>
                    </li>
                    <li>
                        <a href="#tab_1" data-toggle="tab">Communication</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <!-- START FORM PORTLET -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="portlet light bordered bg-inverse entity-details">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info"></i> Supplier Details
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Name:</label>
                                                        <div class="form-control-static">${provider.accountName}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account #: </label>
                                                        <div class="form-control-static">${provider.accountNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account Type: </label>
                                                        <div class="form-control-static">${provider.accountType}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email: </label>
                                                        <div class="form-control-static">${provider.emailAddress}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone No: </label>
                                                        <div class="form-control-static">${provider.phoneNumber}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">VAT No: </label>
                                                        <div class="form-control-static">${provider.vatNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="portlet light bordered entity-details">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info"></i> Summary
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Balance: </label>
                                                        <div class="form-control-static "><span class="bold" >$&nbsp;<g:formatNumber number="${accountStats['balance']}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Current Receipts: </label>
                                                        <div class="form-control-static"><span class="bold" >$&nbsp;<g:formatNumber number="${currentStats['receipts']}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- PRINT SUPPLIER ADDRESSES -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered entity-details">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-envelope"></i> Addresses
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <form class="formf-horizontal" role="form">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Physical: </label>

                                                        <mp:address address="${provider.physicalAddress}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Postal: </label>

                                                        <mp:address address="${provider.postalAddress}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Business: </label>

                                                        <mp:address address="${provider.businessAddress}" />
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- PRINT SUPPLIER ADDRESSES -->

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="portlet light entity-details">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bank"></i> Bank Account Details
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                        <!-- BEGIN PROPERTIES LIST-->
                                            <g:if test="${provider.approved && provider.bankAccount}">
                                                <g:render template="/bankAccount/bankAccount" model="[bankAccount: bankAccount]"/>
                                            </g:if>
                                            <g:else>
                                                <p class="well well-sm default"><mp:noRecordsFound /></p>
                                            </g:else>
                                        <!-- BEGIN PROPERTIES LIST-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM PORTLET -->
                    </div>

                    <div class="tab-pane" id="tab_1">
                        <div class="portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i> Communication
                                </div>

                                <div class="actions">
                                    <a class="btn btn-sm blue-hoki " data-toggle="modal" href="#_leaseUnitFormModal">
                                        <i class="fa fa-plus"></i> Add Contact
                                    </a>
                                </div>

                            </div>

                            <div class="portlet-body">
                                <p class="note">Supplier Contact details...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>
