<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'provider.label', default: 'Suppliers')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'provider.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="provider.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="main-app" v-cloak>
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <g:render template="list"/>
        </div>
    </div>
</div>

<template id="provider-form">
    <div class="clearfix inline-block" ref="providerForm">
        <button class="btn btn-sm blue-hoki" size="lg" @click="openModal=true"><i class="fa fa-plus"></i> Add Supplier</button>

        <modal v-model="openModal" size="lg" ref="modal" id="provider-form-modal" @hide="modalCallback" >

            <span slot="title"><i class="fa fa-plus"></i> Supplier Form</span>

            <form method="POST" role="form" class="form-horizontal">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Account Type
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <g:select from="${metasoft.property.core.AccountType.list()}"
                                          optionValue="name"
                                          optionKey="id"
                                          noSelection="['': 'Account Type...']"
                                          class="form-control select2-no-search"
                                          name="accountTypeItem"></g:select>
                                <input type="hidden" name="accountType" v-validate="'required'" v-model="providerForm.accountType" />
                                <span class="has-error help-block help-block-error">{{ errors.first('accountType') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Phone Number
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="phoneNumber" v-validate="'required'" v-model="providerForm.phoneNumber" />
                                <span class="has-error help-block help-block-error">{{ errors.first('phoneNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Account Name
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="accountName" v-validate="'required'" v-model="providerForm.accountName" />
                                <span class="has-error help-block help-block-error">{{ errors.first('accountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Physical Addr
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="physicalAddress" v-validate="'required'" v-model="providerForm.physicalAddress" />
                                <span class="has-error help-block help-block-error">{{ errors.first('physicalAddress') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Postal Addr
                            </label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="postalAddress" v-validate="'required'" v-model="providerForm.postalAddress" />
                                <span class="has-error help-block help-block-error">{{ errors.first('postalAddress') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Business Addr
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="businessAddress" v-validate="'required'" v-model="providerForm.businessAddress" />
                                <span class="has-error help-block help-block-error">{{ errors.first('businessAddress') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email Address</label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="emailAddress" v-validate="'email'" v-model="providerForm.emailAddress" />
                                <span class="has-error help-block help-block-error">{{ errors.first('emailAddress') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">VAT Number</label>

                            <div class="col-md-9">
                                <input type="text" class="form-control" name="vatNumber" v-validate="'required'" v-model="providerForm.vatNumber" />
                                <span class="has-error help-block help-block-error">{{ errors.first('vatNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </form>


            <div slot="footer">
                <btn @click="openModal=false"><i class="fa fa-times"></i> Cancel</btn>
                <btn @click="save" class="blue-hoki" ><i class="fa fa-save"></i> Submit</btn>
            </div>

        </modal>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/provider/provider-index.js" asset-defer="true"/>

</body>
</html>