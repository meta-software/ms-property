<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="provider" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Suppliers
        </g:link>
        <g:link controller="provider" action="create" class="btn blue-hoki btn-sm ">
            <i class="fa fa-plus"></i> Add Supplier
        </g:link>
        <!--provider-form></provider-form-->
    </div>
</div>