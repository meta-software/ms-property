<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'receiptBatchTmp.label', default: 'Receipt')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'receiptBatchTmp.list.label', default: entityName, args: [receiptBatchTmp])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="receipt-batch-list">
    <div ref="receiptBatchListComp">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <page-bar></page-bar>

        <div class="row">
            <div class="col-md-12">
                <div style="display: none" class="search-page search-content-4">
                    <div class="search-bar bordered">
                        <form role="form" autocomplete="off">
                            <div class="row">
                                <div class="col-lg-3">
                                    <input type="text" name="accountName" value="${params.accountName}" class="form-control" placeholder="Account Name...">
                                </div>
                                <div class="col-lg-3">
                                    <g:select optionKey="id"
                                              placeholder="Account Type"
                                              noSelection="['': 'All Types...']"
                                              class="form-control select2-no-search"
                                              name="accountType"
                                              value="${params?.accountType}"
                                              from="${accountTypeList}"/>
                                </div>
                                <div class="col-lg-3 extra-buttons">
                                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="search-table table-responsive">
                        <table datda-toggle="table" class="table table-bordered table-striped table-hover" >
                            <thead class="bg-grey-cararra">
                            <tr>
                                <th>Name</th>
                                <th>Account No</th>
                                <th>Account Type</th>
                                <!--th>Properties</th-->
                                <th>VAT Number</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:if test="${landlordList}">
                                <g:each in="${landlordList}" var="landlord">
                                    <tr>
                                        <td><g:link style="display: block" action="show" id="${landlord.id}">${landlord.title ?: ""} ${landlord.accountName}</g:link></td>
                                        <td>${landlord.accountNumber}</td>
                                        <td>${landlord.accountType}</td>
                                        <td>${landlord.vatNumber}</td>
                                        <!--td>$ {landlord.holdings.size()}</td -->
                                        <td><mp:makerCheck value="${landlord.dirtyStatus}" /></td>
                                    </tr>
                                </g:each>
                            </g:if>
                            <g:else>
                                <tr>
                                    <td colspan="5">
                                        <p class="well well-sm"><g:message code="default.records.notfound" /></p>
                                    </td>
                                </tr>
                            </g:else>
                            </tbody>
                        </table>
                    </div>
                    <div v-if="false" class="search-pagination">
                        <ul class="pagination">
                            <li class="page-active">
                                <a href="javascript:;"> 1 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 3 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 4 </a>
                            </li>
                        </ul>
                    </div>
                </div>




                <div class="portlet light bordered margin-top-10">
                    <div class="portlet-title">
                        <div class="caption red-mint">
                            <i class="fa fa-circle-o-notch"></i>
                            <span class="caption-subject">Receipt batches</span>
                        </div>
                        <div class="actions">
                            <button type="button" class="btn btn-sm red-sunglo" disabled="disabled"><i class="fa fa-trash"></i> Delete Selected</button>
                            <button type="button" class="btn btn-sm blue-hoki" @click="createReceiptBatch"><i class="fa fa-plus"></i> New Batch</button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead class="bg-grey-cararra">
                                <tr>
                                    <th>Batch Nbr</th>
                                    <th>Batch Type</th>
                                    <th>Date Created</th>
                                    <th>Transactions Count</th>
                                    <th>Created By</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="receiptBatch in receiptBatches">
                                    <td>
                                        <router-link :to="{name: 'show', params: {id: receiptBatch.id}}"
                                                     style="display: block">{{receiptBatch.batchReference}}</router-link>
                                    </td>
                                    <td>{{receiptBatch.transactionType.name}}</td>
                                    <td>{{receiptBatch.dateCreated | formatDate('YYYY-MM-DD')}}</td>
                                    <td>{{receiptBatch.transactionsCount}}</td>
                                    <td>{{receiptBatch.createdBy}}</td>
                                    <td>
                                        <div class="actions">
                                            <button @click="removeReceiptBatch(receiptBatch.id)"
                                                    type="button"
                                                    class="btn btn-xs btn-danger">
                                                <i class="fa fa-trash"></i> Delete
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr v-show="receiptBatches.length == 0">
                                    <td colspan="6">No Records Found...</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="receipt-batch-app">
    <div>
        <router-view></router-view>
    </div>
</template>

<template id="receipt-batch-show">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="page-bar">
            <div class="page-bar-header">
                <h4><i class="fa fa-arrow-left" style="cursor: pointer" title="Back" @click="$router.go(-1)"></i> Receipting</h4>
            </div>

            <div class="page-toolbar">
                <button type="button" class="btn btn-sm red-mint" @click="discardReceiptBatch(id)">
                    <i class="fa fa-times"></i> Discard Batch
                </button>
                <button type="button" class="btn btn-sm blue-hoki" @click="postReceiptBatch(id)" :disabled="receiptBatch.batchCount == 0" >
                    <i class="fa fa-check"></i> Post Batch
                </button>
            </div>
        </div>

        <div class="row margin-top-10">
            <div class="col-md-12">
                <receipt-form @receipt-deleted="discardReceipt" @receipt-posted="receiptPosted" :batch-id="receiptBatch.id" :batch-number="receiptBatch.batchNumber"></receipt-form>

                <receipt-batch-summary :receipt-batch="receiptBatch"></receipt-batch-summary>

                <receipt-list @receipt-deleted="receiptDeleted" ref="receiptsList" :batch-id="receiptBatch.id" @transaction-deleted="alert('deleted')" ></receipt-list>
            </div>
        </div>
    </section>
</template>

<template id="receipt-batch-summary">
    <div class="portlet light bordered bg-inverse">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <label>Receipts Count </label>
                    <div>{{receiptBatch.batchCount}}</div>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <label>Batch Total </label>
                    <div>$ {{receiptBatch.batchTotal | formatNumber(2)}}</div>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="receipt-list">
    <div class="portlet box grey-mint">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject">Receipt Transactions</span>
            </div>
        </div>
        <div class="portlet-body no-padding">

            <table class="no-margin table table-bordered table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th style="width: 20%;">Credit Acc</th>
                    <th style="width: 25%;">Debit Acc</th>
                    <th>Reference</th>
                    <th style="width: 10%;">Txn Date</th>
                    <th style="width: 20%;">Narrative</th>
                    <th style="width: 5%;">Currency</th>
                    <th style="width: 10%;">Amount</th>
                    <th><i class="fa fa-trash font-dark"></i></th>
                </tr>
                </thead>
                <tbody class="trans-list-tbody">
                <tr v-for="(receipt, idx) in receipts" :key="idx">
                    <td>{{receipt.tenant.accountNumber}} : {{receipt.tenant.accountName}}</td>
                    <td>{{receipt.debitAccount.accountNumber}} : {{receipt.debitAccount.accountName}}</td>
                    <td>{{receipt.transactionReference}}</td>
                    <td>{{receipt.transactionDate | formatDate('YYYY-MM-DD')}}</td>
                    <td :title="receipt.narrative">{{ receipt.narrative | shortify(40) }}</td>
                    <td>{{receipt.transactionCurrency.id}}</td>
                    <td>{{receipt.receiptAmount | formatNumber(2)}}</td>
                    <th>
                        <button type="button" @click="deleteTransaction($event, receipt)" class="btn btn-xs">
                            <i class="fa fa-trash font-red-mint"></i>
                        </button>
                    </th>
                </tr>
                <tr v-show="receipts.length == 0">
                    <td colspan="8">No Records Found.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="receipt-form">
    <section>
        <g:render template="receipt-form-vue"></g:render>
    </section>
</template>

<template id="receipt-allocation-form">
    <section>
        <g:render template="receipt-allocation-vue" />
    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Receipt Batches
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" to="/"> <i class="fa fa-list"></i> List Receipt Batches</router-link>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/receiptBatchTmp/receipt-batch-tmp-app.js" asset-defer="true"/>

</body>
</html>
