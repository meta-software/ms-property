<!-- the receipt allocation modal part -->
<form autocomplete="off" role="form" @submit.prevent="postAllocation" >
    <div class="portlet light bordered">
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Lease Unit</label>
                        <select v-validate="'required'" v-model="selectedLease" class="form-control " title="Lease Unit..." name="lease" >
                            <option selected="selected" value="">Select Lease...</option>
                            <option v-for="option in leaseOptions.data" :value="option.id">
                                {{ option.text }}
                            </option>
                        </select>
                        <span class="has-error help-block">{{ errors.first('lease') }}</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Sub Account</label>
                        <sub-account-select v-validate="'required'" name="subAccount" v-model="selectedSubAccount" :type="'expenses'">
                        </sub-account-select>
                        <span class="has-error help-block">{{ errors.first('subAccount') }}</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" placeholder="0.0" class="form-control " name="amount" v-validate="'required||min_value:0.01|decimal:2'" v-model.number="receiptAllocation.allocatedAmount" />
                        <span class="has-error help-block">{{ errors.first('amount') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="actions">
                        <button type="button" class="btn btn-sm btn-success" @click="postAllocation"><i class="fa fa-check"></i> Save Allocation</button>
                        <button type="button" class="btn btn-sm btn-danger" @click="cancelAllocation"><i class="fa fa-times"></i> Cancel </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- the receipt allocation modal part -->