<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'accountTerminateRequest.label', default: 'Account Terminate Request')}"/>
    <g:set var="pageHeadTitle" value="Account Terminate Request : ${accountTerminateRequest.account.accountNumber}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Termination Request - ${accountTerminateRequest.account.accountNumber}" args="[entityName]"/></title>

    <script type="text/javascript">
        var accountTerminateRequestId = ${accountTerminateRequest.id};
    </script>

</head>

<body>

<div id="main-app" v-cloak>

    <g:render template="page_bar_show"/>

    <div class="row">
        <div class="col-md-8">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box red-soft margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-list"></i> Account Terminate Request</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body " style="padding: 0px 10px;">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 15%">Account Number</th><td style="width: 35%">${accountTerminateRequest.account.accountNumber}</td>
                                <th style="width: 15%">Account Status</th><td style="width: 35%">${account.accountNumber}</td>
                            </tr>
                            <tr>
                                <th>Action Reason</th><td colspan="3"><p>${accountTerminateRequest.actionReason}</p></td>
                            </tr>
                            <tr>
                                <th>Action Date</th><td>${accountTerminateRequest.actionDate.format('dd MMM, yyyy')}</td>
                                <th>Request Status</th><td><mp:stateStatusLabel status="${accountTerminateRequest.makerChecker.status}" cssClass="mc-status" text="${accountTerminateRequest.makerChecker.status}" /></td>
                            </tr>
                            <tr>
                                <th>Requested By</th><td>${accountTerminateRequest.makerChecker?.maker?.fullName}</td>
                                <th>Processed ?</th><td><mp:boolStatusLabel status="${accountTerminateRequest.processed}" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->

            <div class="actions">
                <button type="button" disabled="disabled" class="btn btn-success"><i class="fa fa-check"></i> Approve</button>
                <button type="button" disabled="disabled" class="btn red-mint"><i class="fa fa-times"></i> Reject</button>
                <g:if test="${!accountTerminateRequest.isChecked()}">
                    <sec:ifAnyGranted roles="ROLE_PERM_ENTITY_CHECKER">
                        <g:if test="${accountTerminateRequest.maker?.id != applicationContext.springSecurityService.currentUser.id}">
                            <maker-checker :item-controller="'account-terminate-request'" :item-id="accountTerminateRequest.id" :status="'${accountTerminateRequest.makerChecker.status}'"></maker-checker>
                        </g:if>
                    </sec:ifAnyGranted>
                </g:if>
            </div>

            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box blue-hoki margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-list"></i> Request Response</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body " style="padding: 0px 10px;">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 15%">Response Date</th><td style="width: 35%">${accountTerminateRequest.checkDate?.format('dd MMM, yyyy') ?: '---'}</td>
                                <th style="width: 15%">Responded By</th><td style="width: 35%">${accountTerminateRequest.checker?.fullName ?: '---'}</td>
                            </tr>
                            <tr>
                                <th>Response</th><td><p>${accountTerminateRequest.makerChecker.comment ?: '---'}</p></td>
                                <th>Response Status</th><td><mp:stateStatusLabel status="${accountTerminateRequest.makerChecker.status}" cssClass="mc-status" text="${accountTerminateRequest.makerChecker.status}" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
        <div class="col-md-4">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box grey-gallery margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-sticky-note"></i> Account Details</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body" style="padding: 0px 10px;">
                        <table class="table">
                            <tbody>
                            <tr><th style="width: 40%">Account Status</th><td>${account.accountNumber}</td></tr>
                            <tr><th>Tenant</th><td>${account.tenant}</td></tr>
                            <tr><th>Account Status</th><td>${mp.stateStatusClass(status: account.accountStatus.code, text: account.accountStatus.name)}</td></tr>
                            <tr><th>Start Date</th><td><g:formatDate date="${account.accountStartDate}" /></td></tr>
                            <tr><th>Expiry Date</th><td><g:formatDate date="${account.accountExpiryDate}" /></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
</div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/accountTerminateRequest/account-terminate-request-show.js" asset-defer="true"/>

</body>
</html>
