<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <router-link :to="{name: 'home'}" class="btn btn-default btn-sm btn-outline">
            <i class="fa fa-list"></i> Transaction Batches
        </router-link>

        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-primary" href="javascript:;" data-toggle="dropdown" data-hover="dropdown"> <i class="fa fa-plus-square"></i> New Batch Transaction</button>
            <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right">
                <li @click="createBatch('payment')">
                    <a href="javascript:;">Payment </a>
                </li>
                <li @click="createBatch('invoice')">
                    <a href="javascript:;">Invoice </a>
                </li>
                <li @click="createBatch('receipt')">
                    <a href="javascript:;">Receipt</a>
                </li>
                <li @click="createBatch('balance')">
                    <a href="javascript:;">Balance</a>
                </li>
                <li @click="createBatch('cancel')">
                    <a href="javascript:;">Cancel</a>
                </li>
                <li @click="createBatch('refund')">
                    <a href="javascript:;">Refund</a>
                </li>
                <li @click="createBatch('transfer')">
                    <a href="javascript:;">Transfer</a>
                </li>
            </ul>
        </div>
    </div>
</div>