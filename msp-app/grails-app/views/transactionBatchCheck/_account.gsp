<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Account Name:
            </label>
            <div class="col-md-8">${account.accountName}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Account Type:
            </label>
            <div class="col-md-8">${account.accountType}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Account Category:
            </label>
            <div class="col-md-8">${account.accountCategory}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                VAT No:
            </label>
            <div class="col-md-8">${account.vatNumber}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                BP Number:
            </label>
            <div class="col-md-8">${account.bpNumber}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Email Address:
            </label>
            <div class="col-md-8">${account.emailAddress}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Physical Addr:
            </label>
            <div class="col-md-8">${account.physicalAddress}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Postal Addr:
            </label>
            <div class="col-md-8">${account.postalAddress}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Business Addr:
            </label>
            <div class="col-md-8">${account.businessAddress}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Phone No:
            </label>
            <div class="col-md-8">${account.phoneNumber}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Fax Number:
            </label>
            <div class="col-md-8">${account.faxNumber}</div>
        </div>
    </div>
</div>

<h4 class="block">Bank Account Details</h4>

<g:set var="bankAccount" value="${account.bankAccount}" />
<g:if test="${bankAccount}">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4">
                    Bank:
                </label>
                <div class="col-md-8">${account.vatNumber}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4">
                    BP Number:
                </label>
                <div class="col-md-8">${account.bpNumber}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-4">
                    Email Address:
                </label>
                <div class="col-md-8">${account.emailAddress}</div>
            </div>
        </div>
    </div>
</g:if>
<g:else>
    <div class="row">
        <div class="col-md-12">
            <p class="note note-info">No Bank Account.</p>
        </div>
    </div>
</g:else>

<!--f:display bean="account" /-->
