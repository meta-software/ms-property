<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-list" class="portlet box grey-mint">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Transactions List</span>
                </div>
                <div class="tools">
                    <a href="" class="reload" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Trans Id</th>
                        <th>Debit Acc</th>
                        <th>Sub Acc</th>
                        <th>Credit Acc</th>
                        <th>Reference</th>
                        <th>Date</th>
                        <th>Trans Details</th>
                        <th>Amount</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody class="trans-list-tbody">
                    <g:each in="${this.transactionHeaderTmp.mainTransactionTmps}" var="transaction">
                        <tr>
                            <td>1</td>
                            <td>${transaction.debitAccount}</td>
                            <td>${transaction.subAccount.accountName}</td>
                            <td>${transaction.creditAccount}</td>
                            <td>${transaction.transactionReference}</td>
                            <td><g:formatDate date="${transaction.transactionDate}" format="dd/MM/yyyy" /></td>
                            <td title="${transaction.description}">${transaction.description?.take(30)+"..."}</td>
                            <td><g:formatNumber number="${transaction.amount}" type="currency" currencyCode="USD" /> </td>
                            <th>
                                <g:form action="delete" controller="transaction-tmp" id="${transaction.id}" method="DELETE">
                                    <button type="submit" class="btn btn-xs"><i class="fa fa-trash font-red-haze"></i></button>
                                </g:form>
                            </th>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>