<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'transactionBatchCheck.label', default: 'TransactionBatchCheck')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'transactionBatchTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/transaction-batch-check/transaction-batch-check.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="trans-batch-check-list">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Pending Transaction Batches</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="all" class="icheck" />
                            </th>
                            <th>Batch Reference</th>
                            <th>Batch Number</th>
                            <th>Transaction Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="transactionBatchCheck in transactionBatchChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                        </th>
                            <td>
                                <router-link class="display-block" :to="{name: 'show', params: {id: transactionBatchCheck.id}}">{{transactionBatchCheck.batchReference}}</router-link>
                            </td>
                            <td>{{transactionBatchCheck.batchReference}}</td>
                            <td>{{transactionBatchCheck.transactionType.name}}</td>
                            <td>{{transactionBatchCheck.dateCreated | formatDate}}</td>
                            <td>{{transactionBatchCheck.maker.fullName}}</td>
                        </tr>
                        <tr v-if="transactionBatchChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="trans-batch-check-outbox">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Outbox Transaction Batches</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="all" class="icheck" />
                            </th>
                            <th>Batch Reference</th>
                            <th>Batch Number</th>
                            <th>Transaction Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="transactionBatchCheck in transactionBatchChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                        </th>
                            <td>
                                <router-link class="display-block" :to="{name: 'outbox-item', params: {id: transactionBatchCheck.id}}">{{transactionBatchCheck.batchReference}}</router-link>
                            </td>
                            <td>{{transactionBatchCheck.batchReference}}</td>
                            <td>{{transactionBatchCheck.transactionType.name}}</td>
                            <td>{{transactionBatchCheck.dateCreated | formatDate}}</td>
                            <td>{{transactionBatchCheck.maker.fullName}}</td>
                        </tr>
                        <tr v-if="transactionBatchChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="trans-batch-check-show">
    <section>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="portlet light bg-inverse dblue-hoki margin-top-10">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Batch Transaction
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        ID:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Status:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Action:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Entity:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.entity.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Date Created:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Maker:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Transaction Type:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.transactionType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Batch Reference:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.batchReference}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <maker-checker :reload="false" :makerid="transactionBatchCheck.maker.id" :item-id="transactionBatchCheck.id" :controller="'transaction-batch-check'" :status="transactionBatchCheck.checkStatus" @approved="onPosted"></maker-checker>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="portlet light bordered margin-top-10">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-eye"></i> Viewing Batch Transaction : {{transactionBatchCheck.batchReference}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <trans-list-comp :transaction-batch="transactionHeader"></trans-list-comp>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="trans-batch-check-outbox-item">
    <section>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Batch Transaction
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        ID:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Status:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Action:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Entity:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.entity.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Date Created:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Maker:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Transaction Type:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.transactionType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Batch Reference:
                                    </label>
                                    <div class="col-md-8">{{transactionBatchCheck.batchReference}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <maker-checker :item-id="transactionBatchCheck.id" :controller="'transaction-batch-check'" :status="transactionBatchCheck.checkStatus"></maker-checker>

        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-eye"></i> Viewing Batch Transaction : {{transactionBatchCheck.batchReference}}
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <trans-list-comp :transaction-batch="transactionBatchCheck.transactionBatchTmp"></trans-list-comp>
                    </div>
                </div>
            </div>
        </div>
    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Transactions
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Transactions</router-link>
            </div>
        </div>
    </div>
</template>

</body>
</html>