<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'leaseReview.label', default: 'Lease Review')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'leaseReview.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="leaseReview.list.label" args="[entityName]"/></title>
</head>

<body>

<!-- begin: app -->
<div id="app"></div>

<!-- begin: lease-review-app -->
<template id="lease-review-app">
    <section>
        <page-bar></page-bar>

        <router-view></router-view>
    </section>
</template>
<!-- begin: lease-review-app -->

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-book"></i> Lease Reviews
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="">
                <router-link class="btn btn-sm blue-hoki" to="/">
                    <i class="fa fa-list"></i> Lease Reviews
                </router-link>
            </div>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<!-- begin: lease-review-approved template -->
<template id="lease-review-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :is-full-page.sync="loader.fullPage"></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" name="q" placeholder="Query..." v-model="params.query" >
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                        <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-hover table-striped table-bordered">
                <thead class="bg-grey-cararra">
                <tr>
                    <th>Lease #</th>
                    <th>Tenant</th>
                    <th>Property</th>
                    <th>Rental Unit</th>
                    <th>Last Review</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(leaseReview, idx) in leaseReviews" :key="idx">
                    <td><router-link class="display-block" :to="{path: '/show/'+leaseReview.id}">{{leaseReview.lease.leaseNumber}}</router-link></td>
                    <td>{{leaseReview.tenant.accountName}}</td>
                    <td>{{leaseReview.lease.property.name}}</td>
                    <td>{{leaseReview.lease.rentalUnit.name}}</td>
                    <td>{{leaseReview.lastReviewDate | formatDate}}</td>
                    <td><span class="label label-default mc-status">{{leaseReview.reviewStatus}}</span></td>
                </tr>
                <tr v-if="leaseReviews.length == 0">
                    <td colspan="6"><mp:noRecordsFound /></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>
<!-- end: lease-review-approved template -->

<!-- begin: lease-review-show -->
<template id="lease-review-show">
    <div class="portlet light bordered bg-inverse margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :is-full-page.sync="loader.fullPage"></loading>
        <div class="portlet-title" v-if="leaseReview.id !== null">
            <div class="caption"><i class="fa fa-anchor"></i> Lease Review : [{{leaseReview.lease.leaseNumber}}]</div>

            <div class="actions">
                <button type="button" class="btn blue-hoki btn-sm" @click="review">
                    <i class="fa fa-book"></i> Review
                </button>
                <button type="button" class="btn btn-warning btn-sm" @click="ignore" >
                    <i class="fa fa-times"></i> Ignore
                </button>
                <button disabled="disabled" type="button" class="btn btn-success btn-sm" @click="processed" >
                    <i class="fa fa-check"></i> Processed
                </button>
            </div>
        </div>

        <div class="portlet-body" v-if="leaseReview.id !== null" >
            <form class="form-horizontal" role="form">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Leaser No: </label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.lease.leaseNumber}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tenant</label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.tenant.accountName}}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Property: </label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.lease.property.name}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Rental Unit: </label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.lease.rentalUnit.name}}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Last Review:
                            </label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.lastReviewDate | formatDate('DD MMM, YYYY') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Created By:
                            </label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.createdBy}}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Expire Date:
                            </label>

                            <div class="col-md-9">
                                <div class="form-control-static">{{leaseReview.lease.leaseExpiryDate | formatDate('DD MMM, YYYY')}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</template>
<!-- end: lease-review-create tamplate -->

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/leaseReview/lease-review-app.js" asset-defer="true"/>

</body>
</html>