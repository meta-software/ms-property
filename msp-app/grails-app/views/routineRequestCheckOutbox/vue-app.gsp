<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'routineRequestCheck.label', default: 'Routine Request')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'routine-requestTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/vue/vue-bootstrap-table.min.js" asset-defer="true"/>
<asset:javascript src="vue-apps/routineRequestCheck/routine-request-check-outbox.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="routine-request-check-list">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Pending Requests</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                            <th>Action</th>
                            <th>Area</th>
                            <th>Date Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(routineRequestCheck, key) in routineRequestChecks" key="key">
                            <td>
                                <router-link class="display-block" :to="{name: 'show', params: {id: routineRequestCheck.id}}">{{routineRequestCheck.transactionSource.name}}</router-link>
                            </td>
                            <td>{{routineRequestCheck.transactionSource.address.city}}</td>
                            <td>{{routineRequestCheck.actionName}}</td>
                            <td>{{routineRequestCheck.transactionSource.area}}</td>
                            <td>{{routineRequestCheck.dateCreated | formatDate }}</td>
                         </tr>
                        <tr v-if="routineRequestChecks.length == 0">
                            <td colspan="7">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="trans-batch-check-outbox">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Outbox Transaction Batches</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="all" class="icheck" />
                            </th>
                            <th>Batch Reference</th>
                            <th>Batch Number</th>
                            <th>Transaction Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="routineRequestCheck in routineRequestChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                        </th>
                            <td>
                                <router-link class="display-block" :to="{name: 'outbox-item', params: {id: routineRequestCheck.id}}">{{routineRequestCheck.batchReference}}</router-link>
                            </td>
                            <td>{{routineRequestCheck.batchReference}}</td>
                            <td>{{routineRequestCheck.transactionType.name}}</td>
                            <td>{{routineRequestCheck.dateCreated | formatDate}}</td>
                            <td>{{routineRequestCheck.maker.fullName}}</td>
                        </tr>
                        <tr v-if="routineRequestChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="routine-request-check-show">
    <section>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        Viewing Pending Task : Routine Request
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    ID:
                                </label>
                                <div class="col-md-7">{{routineRequestCheck.id}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Status:
                                </label>
                                <div class="col-md-7">{{routineRequestCheck.checkStatus}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Action:
                                </label>
                                <div class="col-md-7">{{routineRequestCheck.actionName}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Entity:
                                </label>
                                <div class="col-md-7">{{routineRequestCheck.transactionSource.name}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Date Created:
                                </label>
                                <div class="col-md-7">{{routineRequestCheck.makeDate | formatDate}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Maker:
                                </label>
                                <div class="col-md-7">{{routineRequestCheck.maker.fullName}}</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

        <maker-options :makerid="routineRequestCheck.maker.id" :item-id="routineRequestCheck.id" :controller="'routine-request-check-outbox'" :status="routineRequestCheck.checkStatus"></maker-options>

        <routine-request-tmp :routine-request-tmp="routineRequestCheck.transactionSource" ></routine-request-tmp>
    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Properties
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Properties</router-link>
            </div>
        </div>
    </div>
</template>

<template id="routine-request-tmp">
    <!-- BEGIN PORTLET-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-eye"></i> Viewing Routine Request : {{routine-requestTmp.name}}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Routine Request Name:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.name}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Area:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.area}} M<sup>2</sup></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Routine Request Type:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.routine-requestType.name}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Landlord:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.landlord.accountName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Routine Request Manager:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.routine-requestManager.fullName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Stand No:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.standNumber}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Commission:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.commission}} %</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Charge VAT?:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.chargeVat}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Address:
                                            </label>
                                            <div class="col-md-7">{{routine-requestTmp.address.street}}, {{routine-requestTmp.address.suburb}}, {{routine-requestTmp.address.city}}, {{routine-requestTmp.address.country}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <rental-unit-tmp-list :routine-request="routine-requestTmp"></rental-unit-tmp-list>

        </div>
    </div>
    <!-- END PORTLET-->
</template>

<template id="rental-unit-tmp-list">
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                Rental Units
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <vue-bootstrap-table
                        :columns="columns"
                        :values="values"
                        :show-filter="true"
                        :show-column-picker="false"
                        :sortable="true"
                        :paginated="true"
                >
                </vue-bootstrap-table>
            </div>
        </div>
    </div>
</template>

</body>
</html>