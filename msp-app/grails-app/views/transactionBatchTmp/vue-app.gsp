<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'transactionHeaderTmp.label', default: 'Transaction')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'transactionBatchTmp.list.label', default: entityName, args: [transactionHeaderTmp])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>
<div id="app"></div>

<template id="trans-batch-list">
    <div>
        <g:render template="vue_page_bar"/>

        <div class="row">
            <div class="col-md-12">
                <div class="search-page search-content-4 margin-top-10">
                    <div class="search-bar bordered">
                        <trans-batch-filter @search="search($event)" @reset="reset" ></trans-batch-filter>
                    </div>
                    <div class="search-table table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead class="bg-grey-cararra">
                            <tr>
                                <th>Batch Ref</th>
                                <th>Batch Type</th>
                                <th>Date Created</th>
                                <th>Transactions Count</th>
                                <th>Created By</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="transactionHeader in transactionHeaderList"
                                v-show="transactionHeaderList.length > 0">
                                <td>
                                    <router-link :to="{name: 'editBatch', params: {id: transactionHeader.id}}"
                                                 style="display: block">{{transactionHeader.batchReference}}</router-link>
                                </td>
                                <td>{{transactionHeader.transactionType.name}}</td>
                                <td>{{transactionHeader.dateCreated | formatDate('YYYY-MM-DD')}}</td>
                                <td>{{transactionHeader.transactionsCount}}</td>
                                <td>{{transactionHeader.createdBy}}</td>
                                <td>
                                    <div class="actions">
                                        <button @click="removeTransactionHeader(transactionHeader)"
                                                type="button"
                                                class="btn btn-xs btn-danger">
                                            <i class="fa fa-trash"></i> Delete
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <tr v-show="transactionHeaderList.length == 0">
                                <td colspan="6">
                                    No Records Found...
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</template>

<template id="trans-header-app">
    <router-view></router-view>
</template>

<template id="trans-header-demo">
    <div class="row">
        <div class="col-md-12">
            <h4>Transaction Header Demo</h4>
        </div>
    </div>
</template>

<template id="trans-batch-edit">
    <div>
        <div class="page-bar">
            <div class="page-bar-header">
                <h4><i class="fa fa-arrow-left" style="cursor: pointer" title="Back" @click="$router.go(-1)"></i> {{transactionHeader.transactionType.name}}
                    <g:if test="${pageHeadSubTitle}">
                        <small>${pageHeadSubTitle}</small>
                    </g:if>
                </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light margin-top-10">

                    <div class="portlet-body form">
                        <component @post-transaction="onPostTransaction" :transaction-batch="transactionHeader" v-bind:is="currentFormComponent"></component>

                        <trans-list-comp ref="transactionList"
                                         @delete-transaction="deleteTransaction"
                                         on-click-transaction="selectTransaction"
                                         :transaction-header.sync="transactionHeader"></trans-list-comp>

                        <trans-header-form :transaction-header.sync="transactionHeader"></trans-header-form>
                    </div>

                </div>
            </div>
        </div>

    </div>
</template>

<template id="trans-batch-filter">
    <g:render template="search_form"/>
</template>

<template id="trans-list-comp">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PORTLET-->
            <div id="trans-list" class="portlet box grey-mint">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject uppercase">Transactions</span>
                    </div>

                </div>

                <div class="portlet-body no-padding">
                    <table v-if="true" class="no-margin table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="width: 15%">Credit Acc</th>
                            <th style="width: 10%">Sub Acc</th>
                            <th style="width: 15%">Debit Acc</th>
                            <th>Reference</th>
                            <th style="width: 5%">TransDate</th>
                            <th style="width: 20%">Trans Details</th>
                            <th style="width: 5%">Currency</th>
                            <th style="width: 5%">Amount</th>
                            <th><i class="fa fa-trash font-dark"></i></th>
                        </tr>
                        </thead>
                        <tbody class="trans-list-tbody">
                        <tr v-for="transactionTmp in transactions">
                            <td>{{transactionTmp.creditAccount}}</td>
                            <td>{{transactionTmp.subAccountNumber}} : {{transactionTmp.subAccountName}}</td>
                            <td>{{transactionTmp.debitAccount}}</td>
                            <td>{{transactionTmp.transactionReference}}</td>
                            <td>{{transactionTmp.transactionDate | formatDate('YYYY-MM-DD')}}</td>
                            <td :title="transactionTmp.description">{{ transactionTmp.description | shortify(40) }}</td>
                            <td>{{transactionTmp.transactionCurrency}}</td>
                            <td>{{transactionTmp.amount | formatNumber}}</td>
                            <th>
                                <button type="button" @click="deleteTransaction($event, transactionTmp.id)" class="btn btn-xs">
                                    <i class="fa fa-trash font-red-haze"></i>
                                </button>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                            <td colspan="2" class="bold uppercase">Control Total:</td>
                            <td class="bold">{{transactionHeader.transactionsTotal | formatNumber}}</td>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</template>

<template id="trans-header-form">
    <g:form controller="transactions" action="post" method="POST" class="form-horizontal" role="form">
        <input type="hidden" name="id" :value="transactionHeader.id" />
        <div class="form-actions left">
            <button :disabled="batchNotPostable" name="update" type="button" class="btn green" @click="postHeaderTransaction" ><i class="fa fa-save"></i> Post Batch</button>
            <button type="button" id="cancel-transaction" @click="discardHeaderTransaction" class="btn default"><i class="fa fa-trash"></i> Discard</button>
        </div>
    </g:form>
</template>

<template id="receipting-form-comp">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="receipting-form-vue"/>
    </g:form>
</template>

<template id="payment-form">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="payment-form-vue"/>
    </g:form>
</template>

<template id="deposit-payment-form">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="deposit-payment-form-vue"/>
    </g:form>
</template>

<template id="landlord-payment-form">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="landlord-payment-form-vue"/>
    </g:form>
</template>

<template id="refund-form-comp">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="refund-form-vue"/>
    </g:form>
</template>

<template id="cancel-form-comp">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="cancel-form-vue"/>
    </g:form>
</template>

<template id="balance-form-comp">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="balance-form-vue"/>
    </g:form>
</template>

<template id="bill-form-comp">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="bill-form-vue"/>
    </g:form>
</template>

<template id="bulk-invoice-form">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="bulk-invoice-form-vue"/>
    </g:form>
</template>

<template id="invoice-form">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="invoice-form-vue"/>
    </g:form>
</template>

<template id="transfer-form-comp">
    <g:form controller="transaction-tmp" action="save" method="POST" role="form" autocomplete="off">
        <g:render template="transfer-form-vue"/>
    </g:form>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/transactionBatchTmp/transaction-batch-tmp-app.js" asset-defer="true"/>

</body>
</html>
