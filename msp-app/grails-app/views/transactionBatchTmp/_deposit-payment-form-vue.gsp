<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Deposit Payment - Batch# {{transactionBatch.batchNumber}}</span>
                </div>

                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Bank Account</label>
                            <select2 ref="bankAccountSelect"
                                     :select2="bankAccountOptions"
                                     name="bankAccount"
                                     v-validate="'required'"
                                     v-model="mainTransaction.bankAccount">
                                <option value="">Select One...</option>
                            </select2>
                            <span class="has-error help-block">{{ errors.first('bankAccount') }}</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Tenant</label>
                            <account-select ref="creditAccountSelect"
                                            @input="onSelectCreditAccount"
                                            :select2="creditAccountOptions"
                                            name="creditAccount"
                                            v-validate="'required'"
                                            v-model="mainTransaction.creditAccount">
                                <option value="">Select One...</option>
                            </account-select>
                            <span class="has-error help-block">{{ errors.first('creditAccount') }}</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Supplier</label>
                            <account-select ref="debitAccountSelect"
                                            :select2="debitAccountOptions"
                                            name="debitAccount"
                                            v-validate="'required'"
                                            v-model="mainTransaction.debitAccount">
                                <option value="">Select One...</option>
                            </account-select>
                            <span class="has-error help-block">{{ errors.first('debitAccount') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Lease</label>
                            <lease-select :disabled="!mainTransaction.creditAccount"
                                          :tenant-number="mainTransaction.creditAccount"
                                          v-model="mainTransaction.lease"
                                          v-validate="'required'"
                                          name="lease"
                                          class="form-control">
                                <option disabled="disabled">Select Lease...</option>
                            </lease-select>
                            <span class="has-error help-block">{{ errors.first('lease') }}</span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Sub Account</label>
                            <sub-account-select
                                    name="subAccount"
                                    v-validate="'required'"
                                    placeholder="Sub Account..."
                                    v-model="mainTransaction.subAccount">
                                <option value="">Select One...</option>
                            </sub-account-select>
                            <span class="has-error help-block">{{ errors.first('subAccount') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Currency</label>
                            <currency-select
                                    name="transactionCurrency"
                                    placeholder="Currency"
                                    v-model="mainTransaction.transactionCurrency"
                                    v-validate="'required'"
                                    class="form-control "></currency-select>
                            <span class="has-error help-block">{{ errors.first('transactionCurrency') }}</span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Amount</label>

                            <input type="text"
                                   name="amount"
                                   placeholder="0.00"
                                   :disabled="!mainTransaction.transactionCurrency"
                                   v-model.lazy.number="mainTransaction.amount"
                                   v-validate="'required|decimal:2'"
                                   class="form-control "/>
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <datepicker name="transactionDate"
                                        class="form-control form-control-inline"
                                        autocomplete="off"
                                        data-date-end-date="0d"
                                        v-validate="'required|date_format:YYYY-MM-DD'"
                                        v-model="mainTransaction.transactionDate"
                                        placeholder="YYYY-MM-DD"></datepicker>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text"
                                   name="description"
                                   v-validate="'required'"
                                   v-model="mainTransaction.description"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('description') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="postTransaction"
                                    id="add-transaction-item"
                                    :disabled="transactionBatch.posted"
                                    class="btn blue">
                                <i class="fa fa-plus"></i> Add Transaction Item
                            </button>
                            <button type="button"
                                    id="cancel-mainTransactionTmp-item"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>

                            <exchange-rate-view style="margin-left: 8px;" :event-date="mainTransaction.transactionDate" :value="mainTransaction.amount" :source-currency="mainTransaction.transactionCurrency"></exchange-rate-view>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
