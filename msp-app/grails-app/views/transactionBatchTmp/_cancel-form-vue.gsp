<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Cancel Details - Batch# {{transactionBatch.batchNumber}}</span>
                </div>

                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>

            <div class="portlet-body">


                <div class="row">

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <datepicker
                                   name="transactionDate"
                                   class="form-control form-control-inline date-picker"
                                   autocomplete="off"
                                   data-date-end-date="0d"
                                   v-validate="'required|date_format:YYYY-MM-DD'"
                                   v-model="mainTransaction.transactionDate"
                                   placeholder="yyyy-mm-dd"></datepicker>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Transaction Reference</label>
                            <select2 ref="transactionReference"
                                     :select2="referenceOptions"
                                     class="form-control"
                                     name="transactionReference"
                                     v-validate="'required'"
                                     v-model="mainTransaction.transactionReference">
                                <option value="">Enter Reference...</option>
                            </select2>
                            <span class="has-error help-block">{{ errors.first('transactionReference') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text"
                                   name="description"
                                   v-validate="'required'"
                                   v-model="mainTransaction.description"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('description') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="postTransaction"
                                    id="add-transaction-item"
                                    :disabled="transactionBatch.posted"
                                    class="btn blue">
                                <i class="fa fa-plus"></i> Add Transaction Item
                            </button>
                            <button type="button"
                                    id="cancel-mainTransactionTmp-item"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
