<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Receipt Details - Batch# {{transactionBatch.batchNumber}}</span>
                </div>

                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Tenant Account</label>
                                            <select2 ref="creditAccountSelect"
                                                     @input="onSelectCreditAccount"
                                                     :select2="creditAccountOptions"
                                                     name="creditAccount"
                                                     v-validate="'required'"
                                                     v-model="mainTransaction.creditAccount">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('creditAccount') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Lease</label>
                                            <input type="hidden" :value="mainTransaction.lease.id" />
                                            <select2 ref="rentalUnit"
                                                     @input="onSelectRentalUnit"
                                                     :select2="creditRentalUnitOptions"
                                                     class="form-control"
                                                     name="lease"
                                                     v-validate="'required'"
                                                     v-model="mainTransaction.lease.id">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('lease') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Debit Account</label>
                                            <select2 ref="debitAccount"
                                                     :select2="debitAccountOptions"
                                                     class="form-control"
                                                     name="debitAccount"
                                                     v-model="mainTransaction.debitAccount">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('debitAccount') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Sub Account</label>
                                            <select2 ref="subAccount"
                                                     :select2="subAccountOptions"
                                                     name="subAccount"
                                                     v-validate="'required'"
                                                     v-model="mainTransaction.subAccount">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('subAccount') }}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Amount</label>

                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text"
                                       name="amount"
                                       placeholder="0.00"
                                       v-model="mainTransaction.amount"
                                       v-validate="'required|decimal:2|min:0'"
                                       class="form-control "/>
                            </div>
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <input type="text"
                                   name="transactionDate"
                                   class="form-control form-control-inline input-medium date-picker"
                                   size="16"
                                   autocomplete="off"
                                   data-date-end-date="0d"
                                   v-validate="'required|date_format:YYYY-MM-DD'"
                                   v-model="mainTransaction.transactionDate"
                                   placeholder="yyyy-mm-dd"/>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Reference</label>
                            <input type="text"
                                   name="transactionReference"
                                   v-validate="'required'"
                                   v-model="mainTransaction.transactionReference"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('transactionReference') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text"
                                   name="description"
                                   v-validate="'required'"
                                   v-model="mainTransaction.description"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('description') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="postTransaction"
                                    id="add-transaction-item"
                                    :disabled="transactionBatch.posted"
                                    class="btn blue">
                                <i class="fa fa-plus"></i> Add Transaction Item
                            </button>
                            <button type="button"
                                    id="cancel-mainTransactionTmp-item"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
