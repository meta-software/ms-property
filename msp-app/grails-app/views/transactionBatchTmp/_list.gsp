<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Batch No</th>
        <th>Date Created</th>
        <th>Transactions Count</th>
        <!-- th>Control Amount</th -->
        <th>Created By</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${transactionHeaderTmpList}">
        <g:each in="${transactionHeaderTmpList}" var="transactionHeaderTmp">
            <tr>
                <td><g:link style="display: block" action="show" controller="transactions" id="${transactionHeaderTmp.id}" >
                    ${transactionHeaderTmp.batchNumber}
                </g:link>
                </td>
                <td><g:formatDate date="${transactionHeaderTmp.dateCreated}" format="dd-MMM-yyyy hh:mm a" /></td>
                <td>${transactionHeaderTmp.parentTransactionsCount}</td>
                <!-- td><g:formatNumber number="${128.52}" type="currency" currencyCode="USD" /></td -->
                <td>${'fleboho'}</td>
                <td>
                    <div class="actions">
                        <g:form action="delete" controller="transactions" id="${transactionHeaderTmp.id}" method="DELETE">
                            <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</button>
                        </g:form>
                        <!-- g:link action="edit" id="${transactionHeaderTmp.id}"  class="btn btn-xs btn-danger"  >
                            <i class="fa fa-trash"></i> Delete
                        < /g:link -->
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5">
                No Contents to show for transactionHeaderTmps
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
