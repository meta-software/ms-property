<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Payment Details - Batch# {{transactionBatch.batchNumber}}</span>
                </div>

                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Bank Account</label>
                            <select2 ref="creditAccountSelect"
                                     :select2="creditAccountOptions"
                                     name="creditAccount"
                                     v-validate="'required'"
                                     v-model="mainTransaction.creditAccount">
                                <option value="">Select One...</option>
                            </select2>
                            <span class="has-error help-block">{{ errors.first('creditAccount') }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Destination Account</label>
                            <account-select
                                    :select2="debitAccountOptions"
                                    name="debitAccount"
                                    v-validate="'required'"
                                    v-model="mainTransaction.debitAccount">
                            </account-select>
                            <span class="has-error help-block">{{ errors.first('debitAccount') }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sub Account</label>
                            <sub-account-select
                                    name="subAccount"
                                    v-validate="'required'"
                                    v-model="mainTransaction.subAccount">
                            ></sub-account-select>
                            <span class="has-error help-block">{{ errors.first('subAccount') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <datepicker
                                    name="transactionDate"
                                    class="form-control form-control-inline"
                                    data-date-end-date="0d"
                                    autocomplete="off"
                                    v-validate="'required|date_format:YYYY-MM-DD'"
                                    v-model="mainTransaction.transactionDate"
                                    placeholder="YYYY-MM-DD"></datepicker>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Currency</label>
                            <currency-select
                                    name="transactionCurrency"
                                    placeholder="Currency"
                                    v-model="mainTransaction.transactionCurrency"
                                    v-validate="'required'"
                                    class="form-control "></currency-select>
                            <span class="has-error help-block">{{ errors.first('transactionCurrency') }}</span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Amount</label>

                            <input type="text"
                                   name="amount"
                                   placeholder="0.00"
                                   :disabled="disableAmountField"
                                   v-model.lazy.number="mainTransaction.amount"
                                   v-validate="'required|decimal:2'"
                                   class="form-control "/>
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text"
                                   name="description"
                                   v-validate="'required'"
                                   v-model="mainTransaction.description"
                                   placeholder="Description..."
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('description') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="postTransaction"
                                    id="add-transaction-item"
                                    :disabled="transactionBatch.posted"
                                    class="btn blue">
                                <i class="fa fa-plus"></i> Add Transaction Item
                            </button>
                            <button type="button"
                                    id="cancel-mainTransactionTmp-item"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>

                            <exchange-rate-view style="margin-left: 8px;" :event-date="mainTransaction.transactionDate" :value="mainTransaction.amount" :source-currency="mainTransaction.transactionCurrency"></exchange-rate-view>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
