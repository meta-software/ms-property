<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Bulk Invoice Details</span>
                </div>

                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Property</label>
                                            <input type="hidden" :value="bulkInvoice.property.id" />
                                            <select2 ref="property"
                                                     @input="onSelectProperty"
                                                     :select2="propertyOptions"
                                                     class="form-control"
                                                     name="property"
                                                     v-model="bulkInvoice.property.id">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('property') }}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Amount</label>

                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text"
                                       name="amount"
                                       placeholder="0.00"
                                       v-model="bulkInvoice.amount"
                                       v-validate="'required|decimal:2'"
                                       class="form-control "/>
                            </div>
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <input type="text"
                                   name="transactionDate"
                                   class="form-control form-control-inline input-medium date-picker"
                                   size="16"
                                   autocomplete="off"
                                   v-validate="'required|date_format:YYYY-MM-DD'"
                                   v-model="bulkInvoice.transactionDate"
                                   placeholder="yyyy-mm-dd"/>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Reference</label>
                            <input type="text"
                                   name="transactionReference"
                                   v-model="bulkInvoice.transactionReference"
                                   v-validate="'required'"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('transactionReference') }}</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
