<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Transaction Details - Batch# {{transactionHeader.batchNumber}}</span>
                </div>

                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div id="debit-form" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-arrow-right font-blue-hoki font-dark"></i>
                                    <span class="caption-subject font-blue-hoki bold uppercase">Debit</span>
                                </div>
                            </div>

                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label>GL Account ?</label>

                                            <div class="mt-checkbox-list">

                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" class="form-control"
                                                           name="debitGlAccount"
                                                           value="1"
                                                           v-model="mainTransaction.debitGlAccount"/>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Debit Account</label>
                                            <select2 ref="debitAccountSelect"
                                                     @input="onSelectDebitAccount"
                                                     :select2="debitAccountOptions"
                                                     name="debitAccount"
                                                     v-validate="'required'"
                                                     v-model="mainTransaction.debitAccount">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('debitAccount') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Debit Sub Account</label>
                                            <select2 ref="debitSubAccountEl"
                                                     @input="onSelectDebitSubAccount"
                                                     :select2="debitSubAccountOptions"
                                                     name="debitSubAccount"
                                                     v-validate="'required'"
                                                     :readonly="debitSubAccountReadOnly"
                                                     autocomplete="off"
                                                     v-model="mainTransaction.debitSubAccount">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('debitSubAccount') }}</span>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>

                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div id="credit-form" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bar-chart font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Credit</span>
                                </div>
                            </div>

                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label>GL Account ?</label>

                                            <div class="mt-checkbox-list">

                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox"
                                                           ref="creditSubAccountEl"
                                                           class="form-control"
                                                           name="creditGlAccount"
                                                           value="1"
                                                           v-model="mainTransaction.creditGlAccount" />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Credit Account</label>
                                            <select2 ref="creditAccountSelect"
                                                     :select2="creditAccountOptions"
                                                     @input="onSelectCreditAccount"
                                                     name="creditAccount"
                                                     v-validate="'required'"
                                                     v-model="mainTransaction.creditAccount">
                                                <option value="">Select Account...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('creditAccount') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Credit Sub Acc</label>
                                            <select2
                                                    ref="creditSubAccountEl"
                                                    @input="onSelectCreditSubAccount"
                                                    :select2="creditSubAccountOptions"
                                                    name="creditSubAccount"
                                                    v-validate="'required'"
                                                    :readonly="creditSubAccountReadOnly"
                                                    autocomplete="off"
                                                    v-model="mainTransaction.creditSubAccount">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('creditSubAccount') }}</span>
                                            <input type="text"
                                                   v-show="false"
                                                   @blur="onSelectCreditSubAccount"
                                                   name="creditSubAccount"
                                                   ref="creditSubAccountEl"
                                                   v-validate="'required'"
                                                   v-model="mainTransaction.creditSubAccount"
                                                   class="form-control"
                                                   :readonly="creditSubAccountReadOnly" />
                                            <span v-show="false" class="has-error help-block">{{ errors.first('creditSubAccount') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-5" v-if="false">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input class="form-control"
                                                   type="text"
                                                   :value="creditSubAccount.accountName"
                                                   disabled="disabled"
                                                   id="credit-sub-account-desc"/>

                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Rental Unit</label>
                                            <input type="hidden" :value="mainTransaction.rentalUnit.id" />
                                            <select2 ref="rentalUnit"
                                                     @input="onSelectRentalUnit"
                                                     :select2="creditRentalUnitOptions"
                                                     class="form-control"
                                                     name="rentalUnit"
                                                     v-model="mainTransaction.rentalUnit.id">
                                                <option value="">Select One...</option>
                                            </select2>
                                            <span class="has-error help-block">{{ errors.first('rentalUnit') }}</span>
                                            <!--input type="text"
                                                   class="form-control"
                                                   :value="mainTransaction.rentalUnit.name"
                                                   readonly="true"/-->
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Amount</label>

                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text"
                                       name="amount"
                                       placeholder="0.00"
                                       v-model="mainTransaction.amount"
                                       v-validate="'required|decimal:2'"
                                       class="form-control "/>
                            </div>
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <input type="text"
                                   name="transactionDate"
                                   class="form-control form-control-inline input-medium date-picker"
                                   size="16"
                                   autocomplete="off"
                                   v-validate="'required|date_format:YYYY-MM-DD'"
                                   v-model="mainTransaction.transactionDate"
                                   placeholder="yyyy-mm-dd"/>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Reference</label>
                            <input type="text"
                                   name="transactionReference"
                                   v-model="mainTransaction.transactionReference"
                                   v-validate="'required'"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('transactionReference') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text"
                                   name="description"
                                   v-validate="'required'"
                                   v-model="mainTransaction.description"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('description') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="postTransaction"
                                    id="add-transaction-item"
                                    :disabled="transactionHeader.posted"
                                    class="btn blue">
                                <i class="fa fa-plus"></i> Add Transaction Item
                            </button>
                            <button type="button"
                                    id="cancel-mainTransactionTmp-item"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
