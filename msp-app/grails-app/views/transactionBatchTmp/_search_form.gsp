<form role="form" autocomplete="off" @submit.prevent="search">
    <div class="row">
        <div class="col-lg-2 col-md-2">
            <input type="text" class="form-control" name="batchNumber" placeholder="Batch Number..." v-model="batchNumber">
        </div>
        <div class="col-lg-2 col-md-2">
            <input type="text" class="form-control" name="transactionType" placeholder="Transaction Type..." v-model="transactionType">
        </div>
        <div class="col-lg-2 col-md-2 extra-buttons">
            <button class="btn blue-hoki uppercase bold" type="button" @click="search" >Search</button>
            <button class="btn btn-defualt" type="button" @click="reset" ><i class="fa fa-refresh"></i></button>
        </div>
    </div>
</form>
<!-- END Portlet PORTLET-->