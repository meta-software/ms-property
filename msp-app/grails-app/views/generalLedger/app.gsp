<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'generalLedger.label', default: 'General Ledger')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'generalLedger.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="generalLedger.list.label" args="[entityName]"/></title>
</head>

<body>

<!-- begin: app -->
<div id="app"></div>

<!-- begin: general-ledger-app -->
<template id="general-ledger-app">
    <section>
        <page-bar></page-bar>

        <div class="inbox margin-top-10">
            <div class="row">
                <!-- <div class="col-lg-2 col-md-4 col-xs-12">
                    <div class="inbox-sidebar">
                        <router-link data-title="Create Ledger" class="btn green btn-block" :to="{path: '/create'}">
                            <i class="fa fa-plus"></i> Create Ledger
                        </router-link>
                        <ul class="inbox-nav">
                            <li>
                                <router-link data-title="Inbox" :to="{path: '/approved'}">
                                    Approved
                                </router-link>
                            </li>
                            <li class="actives">
                                <router-link data-title="Outbox" :to="{path: '/outbox'}">
                                    Outbox
                                </router-link>
                            </li>
                        </ul>
                    </div>
                </div> -->
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <router-view></router-view>
                </div>
            </div>
        </div>
    </section>
</template>
<!-- begin: general-ledger-app -->

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4>General Ledgers
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="">
                <router-link class="btn btn-sm blue-hoki" to="/create">
                    <i class="fa fa-plus"></i> Create Ledger
                </router-link>
            </div>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<!-- begin: general-ledger-approved template -->
<template id="general-ledger-approved">
    <div class="table-scrollable-borderless">
        <div id="toobar">
            <h3 class="pull-left">General Ledgers : Approved</h3>
        </div>

        <bootstrap-table
                :columns="table.columns"
                :data="table.data"
                :options="table.options"
        >
        </bootstrap-table>
    </div>
</template>
<!-- end: general-ledger-approved template -->

<!-- begin: general-ledger-outbox template -->
<template id="general-ledger-outbox">
    <div class="inbox-body">
        <div id="toobar">
            <h3 class="pull-left">General Ledgers : Outbox</h3>
        </div>

        <div class="inbox-content">
            <div class="table-scrollable-borderless">
                <bootstrap-table
                        :columns="table.columns"
                        :data="table.data"
                        :options="table.options"
                >
                </bootstrap-table>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</template>
<!-- end: general-ledger-outbox template -->

<!-- begin: general-ledger-approved template -->
<template id="general-ledger-form">
    <form class="form-horizontal" role="form">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Account Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" type="text"
                                 name="accountName"
                                 v-model="generalLedger.accountName"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Account No:
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" type="text" name="accountNumber"
                                 v-model="generalLedger.accountNumber" />
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">GL Type
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <select2 v-model="generalLedger.generalLedgerType.id" name="generalLedgerType"
                                 :select2="generalLedgerTypeOptions">
                            <option disabled value="0">Select one</option>
                        </select2>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Sub Account:
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <select2 v-model="generalLedger.subAccount.id" name="subAccount"
                                 :select2="subAccountOptions">
                            <option disabled value="0">Select one</option>
                        </select2>
                    </div>
                </div>
            </div>
        </div>
    </form>
</template>
<!-- end: general-ledger-approved template -->

<!-- begin: general-ledger-create teamplate -->
<template id="general-ledger-create">
    <div class="row">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="false"></loading>
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> General Ledger</div>

                    <div class="actions">
                        <button @click="cancel" type="button" class="btn default btn-sm">
                            <i class="fa fa-times"></i> Cancel
                        </button>

                        <button type="button" class="btn btn-sm btn-success" @click="save">
                            <i class="fa fa-check"></i> Save Ledger
                        </button>
                    </div>
                </div>

                <div class="portlet-body">
                    <general-ledger-form @save="save" :general-ledger="generalLedger"></general-ledger-form>
                </div>
            </div>
        </div>
    </div>
</template>
<!-- end: general-ledger-create teamplate -->

<!-- begin: general-ledger-show -->
<template id="general-ledger-show">
    <div class="row">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="false"></loading>
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> General Ledger : {{generalLedger.accountName}}</div>

                    <div class="actions">
                        <button @click="editRequest" type="button" class="btn blue-hoki btn-sm">
                            <i class="fa fa-edit"></i> Edit Account
                        </button>
                    </div>
                </div>

                <div class="portlet-body">
                    <form class="form-horizontal" role="form">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Account Name: </label>

                                    <div class="col-md-9">
                                        <div class="form-control-static">{{generalLedger.accountName}}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Account No: </label>

                                    <div class="col-md-9">
                                        <div class="form-control-static">{{generalLedger.accountNumber}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">GL Type: </label>

                                    <div class="col-md-9">
                                        <div class="form-control-static">{{generalLedger.generalLedgerType.name}}</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Sub Account: </label>

                                    <div class="col-md-9">
                                        <div class="form-control-static">{{generalLedger.subAccount.accountName}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Is System ?:
                                    </label>

                                    <div class="col-md-9">
                                        <div class="form-control-static">{{generalLedger.system}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <template v-if="generalLedger.bankAccount !== undefined">
                        <bank-account :bank-account="generalLedger.bankAccount"></bank-account>
                    </template>
                </div>
            </div>
        </div>
    </div>
</template>
<!-- end: general-ledger-create teamplate -->

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/generalLedger/general-ledger-app.js" asset-defer="true"/>

</body>
</html>