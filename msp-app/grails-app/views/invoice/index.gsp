<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Account Type Configurations')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'invoice.list.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="invoice.list.label" args="[entityName]" /></title>
</head>

<body>

<g:render template="page_bar" />

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message" />
</g:if>

<div class="row">
    <div class="col-md-12">

        <div class="portlet">
            <div class="portlet-body">
                <div class="table-scrollable" >
                    <g:render template="list" />
                </div>
            </div>
        </div>

        <div class="pagination">
            <mp:pagination total="${invoiceCount ?: 0}" />
        </div>

    </div>
</div>

<asset:javascript src="invoice-form.js" asset-defer="true"/>

</body>
</html>