<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Invoice #</th>
        <th>Account</th>
        <th>Property</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${invoiceList}">
        <g:each in="${invoiceList}" var="invoice">
            <tr>
                <td><g:link style="display: block" action="edit" id="${invoice.invoiceNumber}" >
                    ${invoice.invoiceNumber}
                </g:link>
                </td>
                <td>${invoice.accountNumber}</td>
                <td>${invoice.property}</td>
                <td>${invoice.description}</td>
                <td>
                    <div class="actions">
                        <g:link action="edit" id="${invoice.id}"  class="btn btn-xs btn-warning">
                            <i class="fa fa-edit"></i>
                        </g:link>
                        <g:link action="delete" id="${invoice.id}"  class="btn btn-xs btn-warning">
                            <i class="fa fa-timest"></i>
                        </g:link>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5">
                <g:message message="default.records.notfound" />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
