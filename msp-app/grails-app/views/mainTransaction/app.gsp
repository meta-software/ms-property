<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'mainTransaction.label', default: 'Transaction')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'mainTransaction.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <router-view></router-view>
    </div>
</template>

<template id="transaction-list">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>

        <div class="search-page search-content-4 margin-top-5">
            <div class="search-bar bordered">
                <transaction-search v-on:search-request="searchTransactions"></transaction-search>
            </div>
            <div class="search-table table-responsive">
                <bootstrap-table :data="mainTransactions" :columns="table.columns" :options="table.options"></bootstrap-table>
            </div>
            <div v-if="false" class="search-pagination">
                <ul class="pagination">
                    <li class="page-active">
                        <a href="javascript:;"> 1 </a>
                    </li>
                    <li>
                        <a href="javascript:;"> 2 </a>
                    </li>
                    <li>
                        <a href="javascript:;"> 3 </a>
                    </li>
                    <li>
                        <a href="javascript:;"> 4 </a>
                    </li>
                </ul>
            </div>
        </div>

    </section>

</template>

<template id="transaction-show">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Transaction Info
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Id:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{mainTransaction.id}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Type:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{mainTransaction.routineType.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Billing Cycle:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{mainTransaction.billingCycle.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Status:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static"><span :class="statusClass(mainTransaction.requestStatus)" class="request-status label label-sm">{{mainTransaction.requestStatus}}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{mainTransaction.runDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Value Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{mainTransaction.valueDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!--Transaction transactions.-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Transaction Transactions
                        </div>
                    </div>

                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <routine-transactions :routine-request-id="id"></routine-transactions>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> <i class="fa fa-search-minus"></i> Browse Transactions
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>
    </div>
</template>

<template id="transaction-search">
    <form role="form" autocomplete="off" @submit.prevent="requestSearch">
        <div class="form-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Sub Account</label>

                        <select2 v-model="query.subacc" name="subacc" :select2="subAccountOptions" class="form-control">
                            <option disabled value="0">Select one</option>
                        </select2>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Txn Reference</label>
                        <input type="text" class="form-control" name="txtef" v-model="query.txref">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Txn Date</label>
                        <datepicker class="form-control" v-model="query.txndate" name="txDate" ></datepicker>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Credit Acc</label>
                        <input type="text" class="form-control" name="cracc" v-model="query.cracc">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions">
            <button type="button" @click="requestSearch" class="btn btn-sm blue-dark"><i class="fa fa-search"></i> Search...</button>
        </div>

    </form>
</template>

<asset:javascript src="application-vue.js" />
<asset:javascript src="vue-apps/mainTransaction/main-transaction-app.js" asset-defer="true"/>

</body>
</html>