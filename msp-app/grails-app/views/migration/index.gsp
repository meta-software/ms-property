<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="Migration"/>
    <g:set var="pageHeadTitle" value="Data Migration" scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
</head>

<body>
<div id="main-app">
    <g:render template="page_bar" />

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <div class="row margin-top-10">
        <div class="col-md-12">
            <g:link action="process" class="btn btn-sm btn-primary"params="[artifact: 'landlords']" >Process Landlords</g:link>
            <g:link action="process" class="btn btn-sm btn-primary"params="[artifact: 'tenants']" >Process Tenants</g:link>
            <g:link action="process" class="btn btn-sm btn-primary"params="[artifact: 'properties']" >Process Properties</g:link>
            <g:link action="process" class="btn btn-sm btn-primary"params="[artifact: 'rental-units']" >Process Rental Units</g:link>
            <g:link action="process" class="btn btn-sm btn-primary"params="[artifact: 'leases']" >Process Leases</g:link>
            <g:link action="process" class="btn btn-sm btn-primary"params="[artifact: 'security-deposits']" >Security Deposits</g:link>
        </div>
    </div>

</div>
</body>
</html>