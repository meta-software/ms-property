<div class="page-bar-show">
    <div class="page-bar-show-title">
        <div class="page-bar-show-header">
            <h4 class="page-bar-title">
                <i class="fa fa-user"></i>
                Landlord : ${landlord.accountName}
            </h4>
            <div class="page-bar-show-subtitle">
                <span>${landlord.accountNumber}</span>
                | <span>${landlord.phoneNumber}</span>
                | <span>${landlord.emailAddress}</span>
                | <mp:makerCheck value="${landlord.dirtyStatus}" />
            </div>
        </div>
        <div class="page-toolbar">
            <div class="page-toolbar-context">
                <g:if test="${landlord.isChecked()}">
                    <mp:editRecord bean="${landlord}" controller="landlord" action="edit" id="${landlord.id}" class="btn btn-sm blue-hoki" >
                        <i class="fa fa-edit"></i> Edit Record
                    </mp:editRecord>
                </g:if>
            </div>

            <g:link controller="landlord" class="btn btn-sm default" ><i class="fa fa-list"></i> Landlords</g:link>
        </div>
    </div>

    <!--div class="page-bar-show-actions">
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Edit</a>
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Manage Properties</a>
    </div-->
</div>