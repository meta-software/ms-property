<%@ page import="metasoft.property.core.AccountType" %>

<!-- BEGIN FORM PORTLET-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Landlord Form</div>

                <div class="actions">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm green dropdown-toggle" @click="onSaveLandlord" data-toggle="dropdown">
                            <i class="fa fa-check"></i> Save
                        </button>
                    </div>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['accountType'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Account Type
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select from="${AccountType.list()}"
                                          ref="accountType"
                                          optionKey="id"
                                          name="accountType"
                                          noSelection="['null': 'Account Type...']"
                                          value="${landlord?.accountType?.id}"
                                          class="form-control select2-no-search input-small"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ${landlord.errors['phoneNumber'] ? 'has-error' : ''}">
                            <label class="control-label col-md-4">Phone Number
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field class="form-control" type="text" v-model="landlord.phoneNumber" name="phoneNumber"
                                         value="${landlord.phoneNumber}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['accountName'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Account Name
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="landlord.accountName" type="text" name="accountName" value="${landlord.accountName}"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['physicalAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Physical Address
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field type="text" v-model="landlord.physicalAddres" class="form-control" name="physicalAddress"
                                         value="${landlord.physicalAddress}"
                                         placeholder=""/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['postalAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Postal Address</label>

                            <div class="col-md-8">
                                <g:field type="text" v-model="landlord.postalAddress" class="form-control" name="postalAddress"
                                         value="${landlord.postalAddress}"
                                         placeholder="Postal Address"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['businessAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Business Address
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field type="text" v-model="landlord.businessAddress" class="form-control" name="businessAddress"
                                         value="${landlord.businessAddress}"
                                         placeholder="Business Street Address"/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['faxNumber'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Fax
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="landlord.faxNumber" type="text" name="faxNumber"
                                         value="${landlord.faxNumber}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['emailAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Email
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="landlord.emailAddress" type="text" name="emailAddress"
                                         value="${landlord.emailAddress}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group${landlord.errors['vatNumber'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">VAT Number</label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="landlord.vatNumber" type="text" name="vatNumber"
                                         value="${landlord.vatNumber}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="form-section">Bank Account</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.name', default: 'Account Name')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="landlord.bankAccount.accountName" class="form-control" placeholder="" type="text" name="accountName" />
                                <span class="has-error help-block help-block-error">{{ errors.first('accountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyType', default: 'Bank')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select name="bank"
                                          v-model="landlord.bankAccount.bank"
                                          v-validate="'required'"
                                          noSelection="['': 'Select Bank...']"
                                          from="${metasoft.property.core.Bank.list()}"
                                          class="form-control"
                                          optionKey="id"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('bank') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyManager', default: 'Account Number')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="landlord.bankAccount.accountNumber" class="form-control" placeholder="" type="text" name="accountNumber" />
                                <span class="has-error help-block help-block-error">{{ errors.first('accountNumber') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Branch
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="landlord.bankAccount.branch" name="branch" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('branch') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Branch Code
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="landlord.bankAccount.branchCode" name="branchCode" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('branchCode') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- END FORM PORTLET-->