<div class="actions entity-actions">

    <g:link action="create" class="btn btn-sm btn-primary" >
        <i class="fa fa-plus"></i>
        <span>Add Landlord</span>
    </g:link>

</div>
