<div class="table-scrollable" style="background-color: #ffffff;">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
        <tr>
            <th>Paycode</th>
            <th>Payee</th>
            <th>Bank Acc No</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${lessorPayments}">
            <g:each in="${lessorPayments}" var="lessorPayment">
                <tr>
                    <td>
                        ${lessorPayment.paycode}
                    </td>
                    <td>${lessorPayment.payee}</td>
                    <td>${lessorPayment.bankAccountNumber}</td>
                    <td><g:formatNumber currencyCode="USD" number="${lessorPayment.amount}" type="currency"/></td>
                    <td><span
                            class="label label-${mp.boolStatusClass(status: lessorPayment.active)}">${lessorPayment.active ? "Active" : "Inactive"}</span>
                    </td>
                    <td>
                        <div class="actions">
                            <g:link title="Edit" action="edit" resource="${lessorPayment}"
                                    class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </g:link>
                        </div>
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="6">
                    No Lessor Payments configured
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>

<div class="pagination">
    <g:paginate total="${propertyCount ?: 0}"/>
</div>

<!-- START MODALS -->
<div class="modal fade" id="_lessorPaymentFormModal" tabindex="-2" role="basic" aria-hidden="true">
    <g:form method="POST" controller="lessorPayment" action="save">
        <g:hiddenField name="account" value="${landlord.id}"/>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create Lessor Payment</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">
                        <div class="row">
                            <!-- span -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Paycode</label>
                                    <g:select from="${metasoft.property.core.Bank.list()}"
                                              noSelection="['': 'Paycode...']"
                                              name="paycode"
                                              class="form-control select2"
                                              id="_lessorPayment-paycode" />
                                </div>
                            </div>
                            <!--span-->
                            <!--span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Bank Acc No</label>
                                    <g:field type="text" name="bankAccountNumber" id="_lessorPayment-bankAccountNumber"
                                             class="form-control"
                                             placeholder="Account No"/>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">

                            <!--span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <g:field type="text" name="description" id="_lessorPayment-description"
                                             class="form-control"/>
                                </div>
                            </div>
                            <!--/span-->

                            <!--span-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            $
                                        </span>
                                        <g:field type="text" name="amount" id="_lessorPayment-amount"
                                                 class="form-control"/>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <!-- span -->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Is Active ?</label>

                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" name="active" id="_lessorPayment-active" value="1"
                                                   checked="checked">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- span -->
                        </div>
                        <!--/row-->
                    </div>
                    <!-- END FORM BODY-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green" id="_lessorPayment-submit">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </g:form>
<!-- /.modal-dialog -->
</div>
<!-- END MODALS -->