<div class="actions entity-actions">
    <g:link action="create" controller="property" params="['landlord.id': landlord.id]" class="btn btn-sm green" >
        <i class="fa"></i>
        <span>Add Property</span>
    </g:link>

</div>
