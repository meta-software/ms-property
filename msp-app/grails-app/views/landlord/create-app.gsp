<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'landlord.label', default: 'Landlord')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'landlord.create.label', default: entityName)}" scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>f

<body>

<div class="content" id="landlord-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.landlord}">
        <g:render template="/templates/errors"/>
    </g:hasErrors>

    <g:form action="save" controller="landlord" method="POST" autocomplete="off" class="form-horizontal"
            name="landlord-form">
        <g:render template="form_app" bean="landlord"/>
    </g:form>

</div>

<!-- paycode jquery form -->
<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/landlord/landlord-form.js" asset-defer="true"/>
<!-- paycode jquery form -->

</body>
</html>
