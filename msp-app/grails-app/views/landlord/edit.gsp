<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'landlord.label', default: 'Landlord')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'landlord.edit.label', default: entityName, args: [landlord])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="landlord.edit.label" args="[entityName]"/></title>

    <script type="text/javascript">
        let id = ${landlord.id};
    </script>

</head>

<body>

<div class="content" id="landlord-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.landlord}">
        <g:render template="/templates/errors" model="['obj': this.landlord]"/>
    </g:hasErrors>

    <g:form id="${this.landlord.id}" action="update" method="PUT" role="form" class="form-horizontal">
        <g:hiddenField name="id" value="${this.landlord.id}"/>
        <g:hiddenField name="version" value="${this.landlord?.version}"/>

        <g:render template="form_app"/>
    </g:form>
</div>

<!-- paycode jquery form -->
<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/landlord/landlord-edit.js" asset-defer="true"/>
<!-- paycode jquery form -->

</body>
</html>
