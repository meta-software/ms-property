<!DOCTYPE html>
<html>
    <head>
        <!-- BEGIN TEMPALTE VARS -->
        <g:set var="entityName" value="${message(code: 'landlord.properties.label', default: 'Landlord Standing Orders')}" />
        <g:set var="pageTitle" value="${message(code: 'default.show.label', default: entityName, args: [entityName])}" scope="request" />
        <!-- END TEMPALTE VARS -->

        <meta name="layout" content="main"/>
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>

    <body>

        <g:render template="standingOrder_actions" />

        <div class="row">
            <div class="col-md-12">

                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <div class="scrollable" style="background-color: #ffffff;">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Commission</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <g:if test="${landlord.standingOrders}">
                        <g:each in="${landlord.standingOrders}" var="standingOrder">
                        <tr>
                            <td>
                                <g:link controller="standingOrder" action="show" resource="${standingOrder}">${standingOrder.name}</g:link>
                            </td>
                            <td>
                                <div class="actions">
                                    <g:link action="show" resource="${standingOrder}" >
                                        <i class="fa fa-list"></i>
                                        <span>View</span>
                                    </g:link>
                                    <g:link action="edit" resource="${standingOrder}" >
                                        <i class="fa fa-edit"></i>
                                        <span>Edit</span>
                                    </g:link>
                                </div>
                            </td>
                        </tr>
                        </g:each>
                        </g:if>
                        <g:else>
                        <tr>
                            <td colspan="6">
                                No Properties Contents to show
                            </td>
                        </tr>
                        </g:else>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <g:paginate total="${standingOrderCount ?: 0}" />
                </div>

            </div>
        </div>

    </body>
</html>