<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.AccountType" %>

<div class="search-page search-content-4">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-3">
                    <input type="text" name="accountName" value="${params.accountName}" class="form-control" placeholder="Account Name...">
                </div>
                <div class="col-lg-3">
                    <g:select optionKey="id"
                              placeholder="Account Type"
                              noSelection="['': 'All Types...']"
                              class="form-control select2-no-search"
                              name="accountType"
                              value="${params?.accountType}"
                              from="${AccountType.list()}"/>
                </div>
                <div class="col-lg-3 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table datda-toggle="table" class="table table-bordered table-striped table-hover" >
            <thead class="bg-grey-cararra">
            <tr>
                <th>Name</th>
                <th>Account No</th>
                <th>Account Type</th>
                <!--th>Properties</th-->
                <th>VAT Number</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${landlordList}">
                <g:each in="${landlordList}" var="landlord">
                    <tr>
                        <td><g:link style="display: block" action="show" id="${landlord.id}">${landlord.title ?: ""} ${landlord.accountName}</g:link></td>
                        <td>${landlord.accountNumber}</td>
                        <td>${landlord.accountType}</td>
                        <td>${landlord.vatNumber}</td>
                        <!--td>$ {landlord.holdings.size()}</td -->
                        <td><mp:makerCheck value="${landlord.dirtyStatus}" /></td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="5">
                        <p class="well well-sm"><g:message code="default.records.notfound" /></p>
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
    <div v-if="false" class="search-pagination">
        <ul class="pagination">
            <li class="page-active">
                <a href="javascript:;"> 1 </a>
            </li>
            <li>
                <a href="javascript:;"> 2 </a>
            </li>
            <li>
                <a href="javascript:;"> 3 </a>
            </li>
            <li>
                <a href="javascript:;"> 4 </a>
            </li>
        </ul>
    </div>
</div>
