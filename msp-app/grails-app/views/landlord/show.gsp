<%@ page import="metasoft.property.core.PropertyManager; metasoft.property.core.PropertyType" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'landlord.label', default: 'Landlord')}"/>
    <g:set var="pageHeadTitle" value="Landlord Info - ${landlord}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Landlord - ${this.landlord}" args="[entityName]"/></title>

    <script type="text/javascript">
        let landlordId = ${landlord.id};
        let accountNumber = '${landlord.accountNumber}';
    </script>

</head>

<body>

<div id="landlord-show-app">
    <g:render template="page_bar_show"/>

    <div class="row margin-top-10">
        <div class="col-md-12 col-lg-12">
            <div class="tabbable-line boxless tabbable-reversed">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_0" data-toggle="tab">Summary</a>
                    </li>
                    <li>
                        <a href="#tab_1" data-toggle="tab">Properties</a>
                    </li>
                    <!--li>
                        <a href="#tab_2" data-toggle="tab">Standing Orders</a>
                    </li>
                    <li>
                        <a href="#tab_3" data-toggle="tab">Landlord Payments</a>
                    </li>
                    <li>
                        <a href="#tab_4" data-toggle="tab">Contacts</a>
                    </li>
                    < !-- -->
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <!-- START FORM PORTLET -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="portlet light bordered bg-inverse entity-details">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info"></i> Landlord Details
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Name:</label>
                                                        <div class="form-control-static">${landlord.accountName}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account #: </label>
                                                        <div class="form-control-static">${landlord.accountNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account Type: </label>
                                                        <div class="form-control-static">${landlord.accountType}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email: </label>
                                                        <div class="form-control-static">${landlord.emailAddress}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone No: </label>
                                                        <div class="form-control-static">${landlord.phoneNumber}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">VAT No: </label>
                                                        <div class="form-control-static">${landlord.vatNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="portlet light bordered entity-details">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info"></i> Summary
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Balance: </label>
                                                        <div class="form-control-static "><span class="bold" >$&nbsp;<g:formatNumber number="${accountBalance}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Properties: </label>
                                                        <div class="form-control-static"><span class="bold" ><g:formatNumber number="${buildingsCount}" format="###,##0" /></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Current Receipts: </label>
                                                        <div class="form-control-static"><span class="bold" >$&nbsp;<g:formatNumber number="${currentReceipts}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email: </label>
                                                        <div class="form-control-static">${landlord.emailAddress}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- PRINT LANDLORD ADDRESSES -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered entity-details">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-envelope"></i> Addresses
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <form class="formf-horizontal" role="form">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Physical: </label>

                                                        <mp:address address="${landlord.physicalAddress}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Postal: </label>

                                                        <mp:address address="${landlord.postalAddress}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Business: </label>

                                                        <mp:address address="${landlord.businessAddress}" />
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- PRINT LANDLORD ADDRESSES -->

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="portlet light bordered entity-details">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bank"></i> Bank Account Details
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                        <!-- BEGIN PROPERTIES LIST-->
                                            <g:if test="${landlord.approved && landlord.bankAccount}">
                                                <g:render template="/bankAccount/bankAccount" model="[bankAccount: bankAccount]"/>
                                            </g:if>
                                            <g:else>
                                                <p class="well well-sm default"><mp:noRecordsFound /></p>
                                            </g:else>
                                        <!-- BEGIN PROPERTIES LIST-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM PORTLET -->
                    </div>

                    <div class="tab-pane" id="tab_1">
                        <!-- BEGIN PROPERTIES LIST-->
                        <property-list :landlord-id="landlord.id" />
                        <!-- END PROPERTIES LIST-->
                    </div>

                    <div class="tab-pane" id="tab_2">
                        <div class="portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i> Standing Orders
                                </div>

                                <div class="actions">
                                    <a class="btn btn-sm blue-hoki " data-toggle="modal" href="#_leaseUnitFormModal">
                                        <i class="fa fa-plus"></i> Standing Orders
                                    </a>
                                </div>

                            </div>

                            <div class="portlet-body">
                                <p class="note">Standing orders list...</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/landlord/landlord-show.js" asset-defer="true"/>

<template id="property-list">
    <div class="portlet">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-building"></i> Properties</div>
        </div>
        <div class="portlet-body form">
            <!------------------------------- -->
            <div class="search-page search-content-4 margin-top-10">
                <div class="search-bar bordered">
                    <form role="form" autocomplete="off" @submit.prevent="search">
                        <div class="row">
                            <div class="col-lg-8 col-md-8">
                                <input type="text" class="form-control" placeholder="Query..." name="query" v-model="queryParams.query" />
                            </div>
                            <div class="col-lg-2 col-md-2 extra-buttons">
                                <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                                <button class="btn green" type="button" @click="refresh"><i class="fa fa-refresh"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="search-table table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                            <th>Property Type</th>
                            <th>Area</th>
                            <th>Commission</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="property, idx in properties" :key="idx">
                            <td><a :href="href('/property/show/'+property.id)">{{property.name}}</a></td>
                            <td>{{property.address.city}}</td>
                            <td>{{property.propertyType.name}}</td>
                            <td>{{property.area | formatNumber(2)}} M<sup>2</sup></td>
                            <td>{{property.commission | formatNumber(2)}} %</td>
                            <td>{{property.status}}</td>
                            <td><button class="btn btn-xs btn-circle" type="button"><i class="fa fa-ellipsis-h"></i></button></td>
                        </tr>
                        <tr v-if="properties.length == 0">
                            <td colspan="7">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--------------------------------->
        </div>
    </div>
</template>

<template id="current-receipts">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" ref="item">
        <a class="dashboard-stat dashboard-stat blue-hoki" href="#">
            <div class="visual">
                <i class="fa fa-money"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="0">$ {{totalReceipts | formatNumber(2)}}</span>
                </div>
                <div class="desc"> Current Receipts </div>
            </div>
        </a>
    </div>
</template>

</body>

</html>
