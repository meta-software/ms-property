<div class="table-scrollable" style="background-color: #ffffff;">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
        <tr>
            <th>Paycode</th>
            <th>Amount</th>
            <th>Reference</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${standingOrders}">
            <g:each in="${standingOrders}" var="standingOrder">
                <tr>
                    <td>
                        ${standingOrder.bank}
                    </td>
                    <td><g:formatNumber currencyCode="USD" number="${standingOrder.amount}" type="currency"/></td>
                    <td>${standingOrder.reference}</td>
                    <td><span class="label label-${mp.boolStatusClass(status: standingOrder.active)}">${standingOrder.active ? "Active" : "Inactive"}
                    </td>
                    <td>
                        <div class="actions">
                            <g:link title="Edit" action="edit" resource="${standingOrder}"
                                    class="btn btn-xs btn-warning">
                                <i class="fa fa-edit"></i>
                            </g:link>
                        </div>
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="5">
                    No Standing Orders configured
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>

<div class="pagination">
    <g:paginate total="${propertyCount ?: 0}"/>
</div>
