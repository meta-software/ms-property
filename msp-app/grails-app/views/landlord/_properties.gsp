<div class="table-scrollable-borderless">
    <table class="table table-striped table-bordered table-hover table-condensed table-advanced">
        <thead>
        <tr>
            <th>Name</th>
            <th>City</th>
            <th>Country</th>
            <th>Area</th>
            <th>Property Type</th>
            <!--th>Rental Units</th-->
            <th>Commission</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${holdings}">
            <g:each in="${holdings}" var="property">
                <tr>
                    <td>
                        <g:link style="display: block;" controller="property" action="show" resource="${property}">${property.name}</g:link>
                    </td>
                    <td>${property.address?.city}</td>
                    <td>${property.address?.country}</td>
                    <td style="white-space: nowrap"><g:formatNumber number="${property.area}" format="###,###" /> m<sup>2</sup></td>
                    <td>${property.propertyType}</td>
                    <!--td>< g:formatNumber number="$ {property.rentalUnitsCount()}" format="###,###" /></td-->
                    <td><g:formatNumber number="${property.commission}" format="##.00" /> %</td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="7">
                    <g:message message="default.records.notfound" />
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
