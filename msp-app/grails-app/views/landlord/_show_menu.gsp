<div class="actions entity-actions">
    <g:link action="properties" resource="${landlord}" class="btn btn-sm green" >
        <i class="fa"></i>
        <span>Properties</span>
    </g:link>

    <g:link action="standingOrders" resource="${landlord}" class="btn btn-sm green" >
        <i class="fa"></i>
        <span>Standing Orders</span>
    </g:link>

</div>
