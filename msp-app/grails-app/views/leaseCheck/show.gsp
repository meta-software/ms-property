<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'leaseCheck.label', default: 'Viewing : Lease')}"/>
    <g:set var="pageHeadTitle" value="Viewing Task : Lease - ${leaseCheck.entity.id}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let checkId = ${leaseCheck.id};
    </script>

</head>

<body>

<!-- BEGIN PAGE BASE CONTENT -->
<div id="app">
    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> View Pending Task : Lease
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="details" />
                </div>

            </div>
        </div>
    </div>

    <maker-checker :makerid="${leaseCheck.maker.id}" :item-id="checkId" :controller="'lease-check'" :status="'${leaseCheck.checkStatus}'"></maker-checker>

    <g:render template="lease" bean="${lease}" var="lease" />

</div>
<!-- END PAGE BASE CONTENT -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/leaseCheck/lease-check-show.js" asset-defer="true" />

</body>
</html>