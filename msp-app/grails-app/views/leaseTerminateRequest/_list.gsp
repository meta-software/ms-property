<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Request #</th>
        <th>Action Reason</th>
        <th>Lease No</th>
        <th>Tenant</th>
        <th>Status</th>
        <th>Action Date</th>
        <th>Requested By</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${leaseTerminateRequestList}">
        <g:each in="${leaseTerminateRequestList}" var="leaseTerminateRequest">
            <tr>
                <td>
                <g:link style="display: block" action="show" id="${leaseTerminateRequest.id}" >
                    ${leaseTerminateRequest.id}
                </g:link>
                </td>
                <td style="width: 20%;">${leaseTerminateRequest.actionReason}</td>
                <td>
                    <g:link style="display: block" controller="lease" action="show" id="${leaseTerminateRequest.lease.id}" >
                        ${leaseTerminateRequest.lease.leaseNumber}
                    </g:link>
                </td>
                <td>${leaseTerminateRequest.lease.tenant}</td>
                <td><mp:stateStatusLabel cssClass="mc-status" status="${leaseTerminateRequest.makerChecker.status}" text="${leaseTerminateRequest.makerChecker.status}" /></td>
                <td>${leaseTerminateRequest.actionDate.format('dd MMM, yyyy')}</td>
                <td>${leaseTerminateRequest.makerChecker?.maker?.shortName}</td>
                <td>
                    <div class="actions">
                        <button type="button" disabled="disabled" class="btn btn-xs btn-danger"  >
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="6">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
