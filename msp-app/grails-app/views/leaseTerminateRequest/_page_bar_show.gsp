<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4><i class="fa fa-sticky-note"></i> ${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if> - <mp:makerChecker value="${leaseTerminateRequest.makerChecker}" />
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar pull-left" >
         <g:if test="${!leaseTerminateRequest.isChecked()}">
             <sec:ifAnyGranted roles="ROLE_PERM_LEASE_REQUEST_CHECK">
                 <g:if test="${leaseTerminateRequest.maker?.id != applicationContext.springSecurityService.currentUser.id}">
                     <maker-checker :item-controller="'lease-terminate-request'" :item-id="leaseTerminateRequest.id" :status="'${leaseTerminateRequest.makerChecker.status}'"></maker-checker>
                 </g:if>
             </sec:ifAnyGranted>
        </g:if>
    </div>

    <div class="page-toolbar" >
        <g:link controller="leaseTerminateRequest" type="button" class="btn btn-sm blue-hoki">
            <i class="fa fa-edit"></i> Requests
        </g:link>
    </div>

</div>