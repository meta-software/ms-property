<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'leaseTerminateRequest.label', default: 'Lease Terminate Request')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'leaseTerminateRequest.list.label', default: "Lease Termiante Request", args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="leaseTerminateRequest.list.label" args="[entityName]" /></title>
</head>

<body>

<div id="main-app">
    <g:render template="page_bar" />

    <div class="row">
        <div class="col-md-12">
            <g:render template="search_form"/>
        </div>
    </div>


    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message" />
    </g:if>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <g:render template="list" />
                    </div>
                </div>
            </div>

            <div class="pagination">
                <mp:pagination total="${leaseTerminateRequestCount ?: 0}" />
            </div>

        </div>
    </div>
</div>

</body>
</html>