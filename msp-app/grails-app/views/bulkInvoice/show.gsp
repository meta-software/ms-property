<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'bulkInvoice.label', default: 'Bulk Invoice')}"/>
    <g:set var="pageHeadTitle" value="Bulk Invoice Info - ${bulkInvoice.transactionReference}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Bulk Invoice - ${this.bulkInvoice.transactionReference}" args="[entityName]"/></title>

    <script type="text/javascript">
        let bulkInvoiceId = ${bulkInvoice.id};
    </script>

</head>

<body>
<div id="bulkInvoiceShow">
    <g:render template="page_bar_show"/>

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Bulk Invoice Info
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-condensed">
                        <tbody>
                        <tr><th style="width: 40%">Reference: </th><td>${bulkInvoice.transactionReference}</td></tr>
                        <tr><th>Transaction Date: </th><td>${bulkInvoice.transactionDate.format('yyyy-MM-dd')}</td></tr>
                        <tr><th>Property: </th><td>${bulkInvoice.property.name}</td></tr>
                        <tr><th>Amount: </th><td>${bulkInvoice.amount}</td></tr>
                        <tr><th>Transaction Type: </th><td>${bulkInvoice.transactionType}</td></tr>
                        <tr><th>Billing Cycle: </th><td>${bulkInvoice.billingCycle}</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <!-- BEGIN BULK-INVOICE-ITEMS -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Bulk Invoice Items
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="scrollable" id="inner-condtent-div">
                        <g:render template="bulkInvoiceItems" model="[bulkInvoiceItemList: bulkInvoiceItems]" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   END BULK-INVOICE-ITEMS -->
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/bulkInvoice/bulkInvoice-show.js" asset-defer="true"/>
</body>
</html>
