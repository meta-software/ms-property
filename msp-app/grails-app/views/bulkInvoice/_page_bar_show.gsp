<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>
                <i class="fa fa-user"></i>
                ${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if> -
                <mp:makerCheck value="${bulkInvoice.checkStatus}" />
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar pull-left">
        <g:if test="${bulkInvoice.isChecked()}">
        </g:if>
        <g:else>
            <sec:ifAnyGranted roles="ROLE_PERM_LANDLORD_CHECK">
                <g:if test="${bulkInvoice.maker?.id != applicationContext.springSecurityService.currentUser.id}">
                <maker-checker :item-controller="'bulkInvoice'" :item-id="bulkInvoice.id" :status="'${bulkInvoice.checkStatus}'"></maker-checker>
                </g:if>
            </sec:ifAnyGranted>
        </g:else>
    </div>
</div>