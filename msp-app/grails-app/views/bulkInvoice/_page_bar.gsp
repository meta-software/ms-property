<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="bulkInvoice" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Bulk Transactions
        </g:link>

        <div class="btn-group">
            <button type="button" class="btn btn-primary btn-sm dropdown-toggle"><i class="fa fa-plus-square"></i> Create Bulk Transaction</button>
            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>
                    <g:link controller="bulkInvoice" action="create">
                        Create Bulk Invoice
                    </g:link>
                </li>
            </ul>
        </div>
    </div>
</div>