<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Transaction Type</th>
        <th>Transaction Reference</th>
        <th>Billing Cycle</th>
        <th>Transaction Date</th>
        <th>Amount</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${bulkInvoiceList}">
        <g:each in="${bulkInvoiceList}" var="bulkInvoice">
            <tr>
                <td>${bulkInvoice.transactionType}</td>
                <td><g:link class="display-block" controller="bulkInvoice" action="show" id="${bulkInvoice.id}">${bulkInvoice.transactionReference}</g:link></td>
                <td>${bulkInvoice.billingCycle}</td>
                <td><g:formatDate date="${bulkInvoice.transactionDate}" format="dd MMM, yyyy" /></td>
                <td><g:formatNumber number="${bulkInvoice.amount}" format="##,###,###.00" /></td>
                <td>
                    <div class="actions">
                        <g:link action="edit" id="${bulkInvoice.id}" class="btn btn-xs btn-warning"  >
                            <i class="fa fa-edit"></i> Edit Invoice
                        </g:link>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="6">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
