<table data-toggle="table" data-pagination="true" data-search="true" data-height="280px" class="table table-hover table-striped table-bordered">
    <thead>
    <tr>
        <th>Tenant</th>
        <th>Lease Number</th>
        <th>Rental Unit</th>
        <th>Area</th>
        <th>Amount</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${!bulkInvoiceItemList}">
    <tr>
        <td colspan="6"><mp:noRecordsFound /></td>
    </tr>
    </g:if>
    <g:each in="${bulkInvoiceItemList}" var="bulkInvoiceItem">
    <tr>
        <td>${bulkInvoiceItem.lease.tenant.accountName}</td>
        <td>${bulkInvoiceItem.lease.leaseNumber}</td>
        <td>${bulkInvoiceItem.rentalUnit.name}</td>
        <td>${bulkInvoiceItem.rentalUnit.area}</td>
        <td>${bulkInvoiceItem.amount}</td>
        <td>
            <button disabled="disabled" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
        </td>
    </tr>
    </g:each>
    </tbody>
</table>
