<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'bulkInvoice.label', default: 'Bulk Invoice')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'default.create.label', args: [entityName], default: 'Bulk Invoice')}" scope="request" />

    <g:set var="entityName" value="${message(code: 'default.create.label', args: [entityName],  default: 'Bulk Invoice')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
    <!-- END TEMPALTE VARS -->
</head>

<body>

<g:render template="page_bar_create" />

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message" />
</g:if>

<div class="row">
    <div class="col-md-12">

        <div class="portlet margin-top-10">
            <div class="portlet-body form">

                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <g:hasErrors bean="${this.bulkInvoice}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${this.bulkInvoice}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>

                <g:form name="app" class="form-horizontal" resource="${this.bulkInvoice}" method="POST">
                    <g:render template="form" />
                </g:form>

            </div>
        </div>

    </div>
</div>

<template id="bulkInvoiceList">
    <div>
        <table class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th>Tenant</th>
                <th>Lease Number</th>
                <th>Rental Unit</th>
                <th>Area</th>
                <th>Amount</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                <tr v-if="bulkInvoiceItems.length == 0" >
                    <td colspan="6"><mp:noRecordsFound /></td>
                </tr>
                <tr v-for="(bulkInvoiceItem, index) in bulkInvoiceItems" :key="index">
                    <td>{{bulkInvoiceItem.lease.tenant.accountName}}</td>
                    <td>{{bulkInvoiceItem.lease.leaseNumber}}</td>
                    <td>{{bulkInvoiceItem.rentalUnit.name}}</td>
                    <td>{{bulkInvoiceItem.rentalUnit.area}}</td>
                    <td>{{bulkInvoiceItem.amount}}</td>
                    <td>
                        <button disabled="disabled" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/bulkInvoice/bulkInvoice-create.js" asset-defer="true"/>

</body>
</html>
