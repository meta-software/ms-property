<%@ page import="metasoft.property.core.Property; metasoft.property.core.SubAccount" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group${this.bulkInvoice.errors['transactionReference'] ? ' has-error' : ''}">
            <label class="control-label col-md-4">Transaction Reference
                <span class="required">*</span>
            </label>

            <div class="col-md-8">
                <input type="text" v-model="bulkInvoice.transactionReference" class="form-control" type="text" name="transactionReference"
                         value="${this.bulkInvoice.transactionReference}"/>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group${this.bulkInvoice.errors['transactionType'] ? ' has-error' : ''}">
            <label class="control-label col-md-4">Transaction Type
                <span class="required">*</span>
            </label>

            <div class="col-md-8">
                <g:select from="${SubAccount.list()}"
                          optionKey="id"
                          v-model="bulkInvoice.subAccount.id"
                          name="subAccount"
                          noSelection="['null': 'Sub Account...']"
                          value="${bulkInvoice?.subAccount?.id}"
                          class="form-control select2 input-xsmall"/>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group${this.bulkInvoice.errors['transactionDate'] ? ' has-error' : ''}">
            <label class="control-label col-md-4">Transaction Date
                <span class="required">*</span>
            </label>

            <div class="col-md-8">
                <g:field v-model="bulkInvoice.transactionDate" class="form-control" type="text" name="transactionDate"
                         value="${this.bulkInvoice.transactionDate?.format('YYYY-MM-dd')}"/>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group${this.bulkInvoice.errors['property'] ? ' has-error' : ''}">
            <label class="control-label col-md-4">Property
                <span class="required">*</span>
            </label>

            <div class="col-md-8">
                <g:select from="${metasoft.property.core.Property.list()}"
                          optionKey="id"
                          name="property"
                          v-model="bulkInvoice.property.id"
                          noSelection="['null': 'Property...']"
                          value="${bulkInvoice?.property?.id}"
                          class="form-control select2"/>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group${this.bulkInvoice.errors['billingCycle'] ? ' has-error' : ''}">
            <label class="control-label col-md-4">Billing Cycle
                <span class="required">*</span>
            </label>

            <div class="col-md-8">
                <g:select from="${metasoft.property.core.BillingCycle.findAllByOpen(true)}"
                          optionKey="id"
                          v-model="bulkInvoice.billingCycle.id"
                          name="billingCycle"
                          noSelection="['null': 'Billing Cycle...']"
                          value="${bulkInvoice?.billingCycle?.id}"
                          class="form-control select2"/>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group${this.bulkInvoice.errors['amount'] ? ' has-error' : ''}">
            <label class="control-label col-md-4">Amount
                <span class="required">*</span>
            </label>

            <div class="col-md-8">
                <g:field class="form-control" type="text" name="amount" v-model="bulkInvoice.amount"
                         value="${this.bulkInvoice.amount}"/>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="row">
    <div class="col-md-12">
        <div class="">
            <button type="button" class="btn default btn-sm"><i class="fa fa-times"></i> Cancel</button>

            <button @click="save" type="button" class="btn btn-primary btn-sm" ><i class="fa fa-save"></i> Create</button>

            <button @click="simulate" type="button" class="btn blue-hoki btn-sm">Simulate</button>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h4>Invoice Allocations <span class="badge badge-danger">{{bulkInvoiceItems.length}}</span></h4>

        <bulk-invoice-list :bulk-invoice-items="bulkInvoiceItems"></bulk-invoice-list>

    </div>
</div>