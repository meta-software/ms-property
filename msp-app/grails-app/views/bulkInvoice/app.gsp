<%@ page import="metasoft.property.core.SubAccount" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'bulkInvoice.label', default: 'Bulk Invoice')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'bulkInvoice.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="bulkInvoice.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="app">
    <page-bar></page-bar>

    <div class="row">
        <!--<div class="col-lg-2 col-md-4 col-xs-12">
                <div class="inbox-sidebar">
                    <router-link data-title="Create Bulk Invoice" class="btn green btn-block" :to="{path: '/create'}">
                        <i class="fa fa-plus"></i> Create Bulk Invoice
                    </router-link>
                    <ul class="inbox-nav">
                        <li>
                            <router-link data-title="Inbox" :to="{path: '/inbox'}">
                                Inbox
                            </router-link>
                        </li>
                        <li class="actives">
                            <router-link data-title="Outbox" :to="{path: '/outbox'}">
                                Outbox
                            </router-link>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <router-link data-title="Cancelled" class="sbold uppercase" :to="{path: '/trash'}">
                                Trash
                            </router-link>
                        </li>
                    </ul>
                </div>
            </div> -->
        <div class="col-lg-12 col-md-12 col-xs-12">
            <router-view></router-view>
        </div>
    </div>
</div>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4>Bulk Invoices
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="">
                <router-link class="btn btn-sm default" :to="{path: '/'}">
                    <i class="fa fa-list"></i> List Bulk Invoices
                </router-link>
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/create'}">
                    <i class="fa fa-plus"></i> Create Bulk Invoice
                </router-link>
            </div>
        </div>
    </div>
</template>

<template id="bulk-invoice-app">
    <div class="">
        <h4>Bulk Invoice App</h4>
    </div>
</template>

<template id="bulk-invoice-show">
    <div class="portlet light">
        <div class="portlet-title">
            <div class="caption">Bulk Invoice - REF://{{bulkInvoice.transactionReference}}</div>

            <div class="form-actions pull-right">
                <maker-checker :makerid="bulkInvoice.maker.id" :item-id="bulkInvoice.id" :status="bulkInvoice.checkStatus" :controller="'bulk-invoice'" ></maker-checker>
            </div>
        </div>

        <div class="portlet-body form">
            <form class="metasoft-form horizontal-form">
                <div class="portlet bordered light bg-inverse entity-details">
                    <div class="portlet-title">
                        <div class="caption"><i class="fa fa-info"></i> Details</div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Reference:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.transactionReference}}</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Status:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Txn Date:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.transactionDate | formatDate}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Property:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.property.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Currency:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.transactionCurrency.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Amount:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.amount | formatNumber}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4">
                                        Billing Cycle:
                                    </label>
                                    <div class="col-md-8">{{bulkInvoice.billingCycle.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-3">
                                        Narartive:
                                    </label>
                                    <div class="col-md-9">{{bulkInvoice.narrative}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <bulk-invoice-items :bulk-invoice-items="bulkInvoice.bulkInvoiceItems"></bulk-invoice-items>
        </div>
    </div>

    <div class="inbox-body">
        <div class="inbox-header border-bottom" style="border-bottom: 1px solid #eee;">
            <h3 class="pull-left">Bulk Invoice - REF://{{bulkInvoice.transactionReference}}</h3>

            <div class="form-actions pull-right">
                <maker-checker :makerid="bulkInvoice.maker.id" :item-id="bulkInvoice.id" :status="bulkInvoice.checkStatus" :controller="'bulk-invoice'" ></maker-checker>
            </div>
        </div>

        <div class="inbox-content">

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">Invoice Items</div>

                    <div class="actions">
                        <button type="button" class="btn blue btn-outline btn-xs"><i class="fa fa-refresh"></i> </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <bulk-invoice-items :bulk-invoice-items="bulkInvoice.bulkInvoiceItems"></bulk-invoice-items>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="bulk-invoice-outbox-show">
    <div class="inbox-body">
        <div class="inbox-header border-bottom" style="border-bottom: 1px solid #eee;">
            <h3 class="pull-left">Bulk Invoice - REF://{{bulkInvoice.transactionReference}}</h3>

            <div class="form-actions pull-right">
                <maker-options :item-id="bulkInvoice.id" :status="bulkInvoice.checkStatus" :controller="'bulk-invoice'" ></maker-options>
            </div>
        </div>

        <div class="inbox-content">
            <div class="portlet light bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-info"></i> Details</div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Reference:
                                </label>
                                <div class="col-md-8">{{bulkInvoice.transactionReference}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Status:
                                </label>
                                <div class="col-md-8">{{bulkInvoice.checkStatus}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Txn Date:
                                </label>
                                <div class="col-md-8">{{bulkInvoice.transactionDate | formatDate}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Property:
                                </label>
                                <div class="col-md-8">{{bulkInvoice.property.name}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Amount:
                                </label>
                                <div class="col-md-8">$ {{bulkInvoice.amount | formatNumber}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Billing Cycle:
                                </label>
                                <div class="col-md-8">{{bulkInvoice.billingCycle.name}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">Invoice Items</div>

                    <div class="actions">
                        <button type="button" class="btn blue btn-outline btn-xs"><i class="fa fa-refresh"></i> </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <bulk-invoice-items :bulk-invoice-items="bulkInvoice.bulkInvoiceItems"></bulk-invoice-items>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="bulk-invoice-create">
    <section>
        <form class="metasoft-form">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><a @click="$router.push('/')"><i class="fa fa-arrow-left" style="font-size: 1.10em;"></i>
                    </a> Create Bulk Invoice</div>

                    <div class="actions">
                        <button @click="onCancelBulkInvoice" type="button" class="btn default btn-sm">
                            <i class="fa fa-times"></i> Cancel
                        </button>

                        <button type="button" class="btn btn-sm btn-success" @click="onSaveBulkInvoice">
                            <i class="fa fa-check"></i> Save Invoice
                        </button>
                    </div>

                </div>

                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Sub Account
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkInvoice.subAccount.id" name="subAccount"
                                         :select2="subAccountOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Property
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkInvoice.property.id" name="property"
                                         :select2="propertyOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Billing Cycle
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkInvoice.billingCycle.id" name="billingCycle"
                                         :select2="billingCycleOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Currency
                                    <span class="required">*</span>
                                </label>
                                <currency-select ref="transactionCurrency" v-model="bulkInvoice.transactionCurrency" class="form-control"
                                       name="transactionCurrency"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Amount
                                    <span class="required">*</span>
                                </label>
                                <input type="text" ref="amount" v-model="bulkInvoice.amount" class="form-control"
                                       name="amount"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Transaction Date
                                    <span class="required">*</span>
                                </label>
                                <input type="text" ref="transactionDate"
                                       data-date-end-date="0d"
                                       v-model="bulkInvoice.transactionDate"
                                       class="form-control" name="transactionDate"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Narrative</label>
                                <input type="text" ref="narrative" v-model="bulkInvoice.narrative" class="form-control"
                                       name="narrative"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">&nbsp;</label>
                                <exchange-rate-view class="form-control" disabled="disabled" style="margin-left: 8px;" :event-date="bulkInvoice.transactionDate" :value="bulkInvoice.amount" :source-currency="bulkInvoice.transactionCurrency"></exchange-rate-view>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="">
                                <button @click="onCancelBulkInvoice" type="button" class="btn default btn-sm"><i
                                        class="fa fa-times"></i> Cancel</button>

                                <button @click="onSimulateBulkInvoice" type="button" class="btn blue-hoki btn-sm">
                                    <i class="fa fa-cogs"></i> Preview Allocations
                                </button>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>

        <bulk-invoice-items :bulk-invoice-items="bulkInvoiceItems"></bulk-invoice-items>
    </section>
</template>

<template id="bulk-invoice-items">
    <div class="panel bordered panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Invoice Items</h3>
        </div>
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>Tenant</th>
                <th>Lease Number</th>
                <th>Rental Unit</th>
                <th>Area</th>
                <th>Amount</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="bulkInvoiceItem in bulkInvoiceItems">
                <td>{{bulkInvoiceItem.lease.tenant.accountName}}</td>
                <td>{{bulkInvoiceItem.lease.leaseNumber}}</td>
                <td>{{bulkInvoiceItem.rentalUnit.name}}</td>
                <td>{{bulkInvoiceItem.rentalUnit.area}}</td>
                <td>{{bulkInvoiceItem.amount | formatNumber}}</td>
                <td><span>...</span></td>
            </tr>
            <tr v-if="bulkInvoiceItems.length == 0">
                <td colspan="7">No Records found...</td>
            </tr>
            </tbody>
        </table>

    </div>
</template>

<template id="bulk-invoice-inbox">

    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" placeholder="Query Invoice..."  v-model="params.q">
                    </div>
                    <div class="col-lg-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                        <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <!-- add table container here -->
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="all" class="icheckbox_flat"/></th>
                    <th>Txn Reference</th>
                    <th>Txn Date</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Property</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(bulkInvoice, index) in bulkInvoices" :key="index" @click="$router.push({name: 'inboxItem', params: {id: bulkInvoice.id}})">
                    <td><input type="checkbox" class="icheck" /></td>
                    <td>{{bulkInvoice.transactionReference}}</td>
                    <td>{{bulkInvoice.transactionDate | formatDate}}</td>
                    <td>{{bulkInvoice.transactionCurrency.id}}</td>
                    <td>{{bulkInvoice.amount | formatNumber(2)}}</td>
                    <td>{{bulkInvoice.property.name}}</td>
                    <td>{{bulkInvoice.dateCreated | formatDate}}</td>
                    <td>{{bulkInvoice.maker.username}}</td>
                </tr>
                <tr v-if="bulkInvoices.length == 0">
                    <td colspan="7"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
            <!-- add table container here -->
        </div>
    </div>

</template>

<template id="bulk-invoice-outbox">
    <div class="inbox-body">
        <div class="inbox-header">
            <h3 class="pull-left">Bulk Invoices : Outbox</h3>
        </div>

        <div class="inbox-content">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="all" class="icheckbox_flat"/></th>
                    <th>Txn Reference</th>
                    <th>Txn Date</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Property</th>
                    <th>Date Created</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(bulkInvoice, index) in bulkInvoices" :key="index">
                    <td><input type="checkbox" class="icheck" /></td>
                    <td>{{bulkInvoice.transactionReference}}</td>
                    <td>{{bulkInvoice.transactionDate | formatDate}}</td>
                    <td>{{bulkInvoice.transactionCurrency.id}}</td>
                    <td>{{bulkInvoice.amount | formatNumber(2)|}</td>
                    <td>{{bulkInvoice.property.name}}</td>
                    <td>{{bulkInvoice.dateCreated | formatDate}}</td>
                </tr>
                <tr v-if="bulkInvoices.length == 0">
                    <td colspan="7"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="bulk-invoice-cancelled">
    <div class="inbox-body">
        <div class="inbox-header">
            <h3 class="pull-left">Bulk Invoices : <span class="sbold">TRASH</span></h3>
        </div>

        <div class="inbox-content">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="all" class="icheckbox_flat"/></th>
                    <th>Txn Reference</th>
                    <th>Txn Date</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Property</th>
                    <th>Date Created</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(bulkInvoice, index) in bulkInvoices" :key="index">
                    <td><input type="checkbox" class="icheck" /></td>
                    <td>{{bulkInvoice.transactionReference}}</td>
                    <td>{{bulkInvoice.transactionDate | formatDate}}</td>
                    <td>{{bulkInvoice.transactionCurrency.id}}</td>
                    <td>{{bulkInvoice.amount  | formatNumber(2)|}</td>
                    <td>{{bulkInvoice.property.name}}</td>
                    <td>{{bulkInvoice.dateCreated | formatDate}}</td>
                </tr>
                <tr v-if="bulkInvoices.length == 0">
                    <td colspan="7"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/bulkInvoice/bulk-invoice-app.js" asset-defer="true"/>

</body>
</html>