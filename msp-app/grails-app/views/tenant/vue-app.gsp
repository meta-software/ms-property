<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tenant')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'tenant.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="tenant.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="app"></div>

<template id="tenant-app">
    <div>

        <g:render template="page_bar"/>

        <router-view></router-view>
    </div>
</template>

<template id="tenant-placeholder">
    <h4>Tenant PlaceHolder Component</h4>
</template>

<template id="tenant-edit-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Edit Tenant - {{tenant.accountNumber}}</div>
                </div>

                <div class="portlet-body">
                    <g:form action="save" controller="tenant" method="POST" class="form-horizontal"
                            name="tenant-form">
                        <p class="note note-info">The Tenant Edit Form...</p>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

</template>

<template id="tenant-show-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet no-padding">
                <div class="portlet-body no-padding">

                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_0" data-toggle="tab">Tenant Info</a>
                            </li>
                            <li>
                                <a href="#tab_1" data-toggle="tab">Properties <span
                                        class="badge badge-info">{{tenant.propertyCount}}</span></a>
                            </li>
                            <li>
                                <a href="#tab_2" data-toggle="tab">Standing Orders <span
                                        class="badge badge-success">{standingOrdersCount}</span></a>
                            </li>
                            <li>
                                <a href="#tab_3" data-toggle="tab">Lessor Payments <span
                                        class="badge badge-success">{lessorPaymentsCount}</span></a>
                            </li>
                        </ul>

                        <div class="tab-content" style="padding-top: 10px;">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info "></i>Tenant Info - {{tenant.accountName}}
                                        </div>

                                        <div class="actions">
                                            <router-link :to="{name: 'edit', params:{id: tenant.id}}"
                                                         class="btn btn-xs btn-warning">
                                                <i class="fa fa-edit"></i> Edit
                                            </router-link>
                                        </div>
                                    </div>

                                    <!-- portlet-body -->
                                    <div class="portlet-body">

                                        <form class="form-horizontal form-horizontal-left" role="form">
                                            <div class="form-body">
                                                <!--/row-->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5 bold">Account Name:</label>

                                                            <div class="col-md-7">
                                                                <p class="form-control-static">{{tenant.accountName}}</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5 bold">Account No:</label>

                                                            <div class="col-md-7">
                                                                <p class="form-control-static">{{tenant.accountNumber}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                    <!--/span-->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-5 bold">Account Type:</label>
                                                            <div class="col-md-7">
                                                                <p class="form-control-static">{{tenant.accountType.name}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->

                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <!-- portlet-body -->

                                </div>
                            </div>

                            <div class="tab-pane" id="tab_1">Properties</div>

                            <div class="tab-pane" id="tab_2">Standing Orders</div>

                            <div class="tab-pane" id="tab_3">Lessor Payments</div>
                        </div>
                    </div>
                </div>
            </div>

            <h4>{{tenant.accountName}} - {{tenant.accountNumber}}</h4>
        </div>
    </div>
</template>

<template id="tenant-list-comp">

    <div class="row">
        <div class="col-md-12">

            <div class="portlet">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Account No</th>
                                <th>Account Type</th>
                                <th>VAT Number</th>
                                <th>Properties</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-if="tenantList.length > 0" v-for="tenant in tenantList">
                                <td>
                                    <router-link :to="{name: 'show', params:{id: tenant.id}}" class="display-block">
                                        {{tenant.id}}
                                    </router-link>
                                </td>
                                <td>{{tenant.accountNumber}}</td>
                                <td>{{tenant.accountType.name}}</td>
                                <td>{{tenant.vatNumber}}</td>
                                <td>{{tenant.propertyCount}}</td>
                                <td>
                                    <div class="actions">
                                        <router-link :to="{name: 'edit', params:{id: tenant.id}}"
                                                     class="btn btn-xs btn-warning">
                                            <i class="fa fa-edit"></i> Edit
                                        </router-link>
                                    </div>
                                </td>
                            </tr>
                            <tr v-if="tenantList.length == 0">
                                <td colspan="5">No Records Found...</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="pagination">
            </div>

        </div>
    </div>

</template>

<template id="acc-type-form-comp">
    <g:form action="save" method="POST" role="form" class="form-horizontal">
        <g:hiddenField name="id" value="" v-model="accountType.id"/>
        <!-- BEGIN FORM PORTLET-->
        <div class="portlet light">

            <div class="portlet-body form">

                <div class="tab-pane active" id="tab0">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text"
                                             name="name"
                                             v-model="accountType.name"
                                             value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Code
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text" name="code"
                                             v-model="accountType.code"
                                             value=""/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Description
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text" name="description"
                                             v-model="accountType.description"
                                             value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Is Active?
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" v-model="accountType.active" value="1"
                                                   name="active"/>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions right">
                    <button @click="$router.push({name: 'home'})" type="button" class="btn default">Cancel</button>

                    <button @click="onSubmit" type="button" class="btn green" name="create"
                            v-show="accountType.id == null">
                        ${message(code: 'default.button.create.label', default: 'Submit')}
                    </button>
                    <button @click="onUpdate" type="button" class="btn green" name="create"
                            v-show="accountType.id != null">
                        ${message(code: 'default.button.update.label', default: 'Update')}
                    </button>
                </div>

            </div>
        </div>
        <!-- END FORM PORTLET-->
    </g:form>
</template>

<template id="acc-type-edit-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Account Type</div>
                </div>

                <div class="portlet-body">
                    <acc-type-form-comp v-on:save="save" v-bind:account-type="accountType"></acc-type-form-comp>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="acc-type-create-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Account Type</div>
                </div>

                <div class="portlet-body">
                    <acc-type-form-comp v-on:save="save" v-bind:account-type="accountType"></acc-type-form-comp>
                </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js"/>
<asset:javascript src="vue-apps/tenant/tenant-app.js" asset-defer="true"/>

</body>
</html>