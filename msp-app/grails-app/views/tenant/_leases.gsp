<div class="table-scrollable">
<table class="table table-striped table-bordered table-hover table-condensed table-advance">
    <thead>
    <tr>
        <th>Lease Number</th>
        <th>Property</th>
        <th>Rental Unit</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>Expiry Date</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${leaseList}">
        <g:each in="${leaseList}" var="lease">
            <tr>
                <td>
                    <g:link style="display: block" action="show" controller="lease" id="${lease.id}" >
                        ${lease.leaseNumber}
                    </g:link>
                </td>
                <td>${lease.property}</td>
                <td>${lease.rentalUnit}</td>
                <td><mp:stateStatusClass status="${lease.leaseStatus.code}" text="${lease.leaseStatus.name}" /></td>
                <td><g:formatDate date="${lease.leaseStartDate}" format="dd-MMM-yyyy" /></td>
                <td><g:formatDate date="${lease.leaseExpiryDate}" format="dd-MMM-yyyy" /></td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="7">
                <g:message message="default.records.notfound" />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
</div>