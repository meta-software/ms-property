<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <a disabled="disabled" controller="tenant" action="create" class="btn blue-hoki btn-sm">
            <i class="fa fa-plus"></i> Add Tenant
        </a>
        <g:link controller="tenant" action="index" class="btn default btn-sm">
            <i class="fa fa-list"></i> Tenants
        </g:link>
    </div>

</div>