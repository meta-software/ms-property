<div class="page-bar-show">

    <div class="page-bar-show-title">
        <div class="page-bar-show-header">
            <h4 class="page-bar-title">
                <i class="fa fa-user"></i>
                Tenant : ${tenant.accountName}
            </h4>
            <div class="page-bar-show-subtitle">
                <span>${tenant.accountNumber}</span>
                | <span>${tenant.phoneNumber}</span>
                | <span>${tenant.emailAddress}</span>
                | <mp:makerCheck value="${tenant.dirtyStatus}" />
            </div>
        </div>
        <div class="page-toolbar">
            <div class="page-toolbar-context">
                <g:if test="${tenant.isChecked()}">
                    <mp:editRecord bean="${tenant}" controller="tenant" action="edit" id="${tenant.id}" class="btn btn-sm blue-hoki" >
                        <i class="fa fa-edit"></i> Edit Record
                    </mp:editRecord>
                </g:if>
            </div>

            <button type="button" disabled="disabled" class="btn blue-hoki btn-sm">
                <i class="fa fa-plus"></i> Add Tenant
            </button>
            <g:link controller="tenant" action="index" class="btn btn-sm default">
                <i class="fa fa-list"></i> Tenants
            </g:link>
        </div>
    </div>

    <!--div class="page-bar-show-actions">
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Edit</a>
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Manage Properties</a>
    </div-->
</div>

<!--div class="page-bar">
<g:if test="${pageHeadTitle}">
    <div class="page-bar-header">
        <h4>${pageHeadTitle}
    <g:if test="${pageHeadSubTitle}">
        <small>${pageHeadSubTitle}</small>
    </g:if>
    </h4>
</div>
</g:if>

<div class="page-toolbar">
<g:link controller="tenant" action="index" class="btn btn-default btn-sm">
    <i class="fa fa-list"></i> Tenants
</g:link>
<button type="button" disabled="disabled" class="btn blue-hoki btn-sm">
    <i class="fa fa-plus"></i> Add Tenant
</button>
<!--tenant-form></tenant-form-- >
</div>

</div-->