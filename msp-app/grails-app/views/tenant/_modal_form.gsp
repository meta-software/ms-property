<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>
<%@ page import="metasoft.property.core.AccountCategory" %>
<%@ page import="metasoft.property.core.AccountType" %>
<%@ page import="metasoft.property.core.Title" %>

<div class="modal fade" id="_tenantFormModal" tabindex="-1" role="basic" aria-hidden="true">
    <g:form name="_tenantForm" controller="tenant" action="save" method="POST" class="horizontal-form">
        <!-- g:hiddenField name="accountCategory" value="$ {tenant.accountCategory.id}"/-->
        class="content scaffold-create form-horizontal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create New Tenant</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Account Type
                                        <span class="required">*</span>
                                    </label>
                                    <g:select from="${AccountType.list()}"
                                              optionKey="id"
                                              name="accountType"
                                              noSelection="['null': 'Account Type...']"
                                              class="form-control  select2-no-search input-xsmall"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Account Name
                                        <span class="required">*</span>
                                    </label>
                                    <g:field class="form-control" type="text" name="accountName"
                                             placeholder="Account Name"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Physical Address
                                        <span class="required">*</span>
                                    </label>
                                    <g:field type="text" class="form-control" name="physicalAddress"
                                             placeholder="Physical Address"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Postal Address
                                    </label>
                                    <g:field type="text" class="form-control" name="postalAddress"
                                             placeholder="Postal Address"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Business Address
                                        <span class="required">*</span>
                                    </label>
                                    <g:field type="text" class="form-control" name="businessAddress"
                                             placeholder="Business Street Address"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Phone Number
                                        <span class="required">*</span>
                                    </label>
                                    <g:field class="form-control" type="text" name="phoneNumber"
                                             placeholder="Phone Number"/>
                                </div>

                                <div class="form-group">
                                    <label class=" control-label">Fax
                                        <span class="required">*</span>
                                    </label>

                                    <g:field class="form-control" type="text" name="faxNumber"
                                             placeholder="Fax Number"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Email
                                        <span class="required">*</span>
                                    </label>
                                    <g:field class="form-control" type="text" name="emailAddress"
                                             placeholder="email address"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">VAT Number
                                    </label>
                                    <g:field class="form-control" type="text" name="vatNumber"
                                             placeholder="VAT Number"/>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn green" id="_tenant-submit">Save changes</button>
                    </div>

                </div>
            </div>
        </div>
    </g:form>
</div>

<!-- paycode jquery form -->
<asset:javascript src="tenant-form.js" asset-defer="true"/>
<!-- paycode jquery form -->
