<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tennant')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'tenant.edit.label', default: entityName, args: [tenant])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="tenant.edit.label" args="[entityName]"/></title>

    <script type="text/javascript">
        let id = ${tenant.id};
    </script>

</head>

<body>

<div class="content" id="tenant-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.tenant}">
        <g:render template="/templates/errors" model="['obj': this.tenant]"/>
    </g:hasErrors>

    <g:form id="${this.tenant.id}" action="update" method="PUT" role="form" class="form-horizontal">
        <g:hiddenField name="id" value="${this.tenant.id}"/>
        <g:hiddenField name="version" value="${this.tenant?.version}"/>

        <g:render template="form"/>
    </g:form>
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/tenant/tenant-edit.js" asset-defer="true"/>

</body>
</html>
