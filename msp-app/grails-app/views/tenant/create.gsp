<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tenant')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'tenant.create.label', default: entityName)}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="tenant.create.label" args="[entityName]"/></title>
</head>

<body>

<div class="content" id="tenant-form-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.tenant}">
        <g:render template="/templates/errors" model="['obj': this.tenant]"/>
    </g:hasErrors>

    <g:form action="save" method="POST" class="form-horizontal" name="frm-tenant">
        <g:render template="form" bean="tenant"/>
    </g:form>

</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/tenant/tenant-form.js" asset-defer="true"/>
</body>
</html>
