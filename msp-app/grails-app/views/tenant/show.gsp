<%@ page import="metasoft.property.core.Property" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tenant')}"/>
    <g:set var="pageHeadTitle" value="Tenant Info - ${tenant}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Tenant - ${this.tenant}" args="[entityName]"/></title>

    <script type="text/javascript">
        let tenantId = ${tenant.id};
    </script>

</head>

<body>

<div id="main-app" v-cloak>

    <g:render template="page_bar_show"/>

    <div class="row margin-top-10">
        <div class="col-md-12 col-lg-12">
            <div class="tabbable-line boxless tabbable-reversed">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_0" data-toggle="tab">Summary</a>
                    </li>
                    <li>
                        <a href="#tab_1" data-toggle="tab">Leases</a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab" @click="retrieveTransactions" >Transactions</a>
                    </li>
                    <!--li>
                        <a href="#tab_3" data-toggle="tab">Contacts</a>
                    </li>
                    < !-- -->
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_0">
                        <!-- START FORM PORTLET -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="portlet light bordered bg-inverse entity-details">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info"></i> Tenant Details
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Name:</label>
                                                        <div class="form-control-static">${tenant.accountName}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account #: </label>
                                                        <div class="form-control-static">${tenant.accountNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Account Type: </label>
                                                        <div class="form-control-static">${tenant.accountType}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Email: </label>
                                                        <div class="form-control-static">${tenant.emailAddress}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Phone No: </label>
                                                        <div class="form-control-static">${tenant.phoneNumber}</div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">VAT No: </label>
                                                        <div class="form-control-static">${tenant.vatNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Tenant Ref: </label>
                                                        <div class="form-control-static">${tenant.bankAccountNumber}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="portlet light bordered entity-details">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-info"></i> Summary
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Balance: </label>
                                                        <div class="form-control-static "><span class="label label-success bold" >$&nbsp;<g:formatNumber number="${accountBalance}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Deposit Balance: </label>
                                                        <div class="form-control-static "><span class="label label-default bold" >$&nbsp;<g:formatNumber number="${depositBalance}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Rent Balance: </label>
                                                        <div class="form-control-static "><span class="label label-default bold" >$&nbsp;<g:formatNumber number="${rentalBalance}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">OpCost Balance: </label>
                                                        <div class="form-control-static "><span class="label label-default bold" >$&nbsp;<g:formatNumber number="${opcostBalance}" format="###,##0.00" /></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Active Leases: </label>
                                                        <div class="form-control-static"><span class="bold" ><g:formatNumber number="${activeLeaseCount}" format="###,##0" /></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- PRINT TENANT ADDRESSES -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered entity-details">

                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-envelope"></i> Addresses
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <form class="formf-horizontal" role="form">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Physical: </label>

                                                        <mp:address address="${tenant.physicalAddress}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Postal: </label>

                                                        <mp:address address="${tenant.postalAddress}" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Business: </label>

                                                        <mp:address address="${tenant.businessAddress}" />
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- PRINT TENANT ADDRESSES -->

                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="portlet light bordered entity-details">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-bank"></i> Bank Account Details
                                        </div>
                                    </div>

                                    <div class="portlet-body form">
                                        <form class="horizontal-form" role="form">
                                        <!-- BEGIN PROPERTIES LIST-->
                                            <g:if test="${tenant.approved && tenant.bankAccount}">
                                                <g:render template="/bankAccount/bankAccount" model="[bankAccount: bankAccount]"/>
                                            </g:if>
                                            <g:else>
                                                <mp:noRecordsFound />
                                            </g:else>
                                        <!-- BEGIN PROPERTIES LIST-->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM PORTLET -->
                    </div>

                    <div class="tab-pane" id="tab_1">
                        <div class="portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-building-o"></i>Leases
                                </div>
                            </div>

                            <div class="portlet-body">
                                <!-- BEGIN LEASES LIST-->
                                <g:render template="leases" model="['leaseList': tenant.leases]" />
                                <!-- BEGIN LEASES LIST-->
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="tab_2">
                        <account-transactions ref="transactionsComponent" :account-id="tenant.id"></account-transactions>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE BASE CONTENT -->
</div>

<!-- START VUE COMPONENT MODALS -->
<template id="account-transactions">
    <div class="portlet">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-list"></i> Transactions
            </div>
        </div>

        <div class="portlet-body" ref="transactionsBody">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th>Txn Date</th>
                    <th>Txn Type</th>
                    <th>Reference</th>
                    <th>Narration</th>
                    <th>Lease #</th>
                    <th>Rental Unit</th>
                    <th>Txn Amount</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="transaction in transactions">
                    <td>{{transaction.transactionDate | formatDate('DD-MMM-YYYY')}}</td>
                    <td>{{transaction.transactionType.name}}</td>
                    <td>{{transaction.transactionReference}}</td>
                    <td>{{transaction.description | shortify(80)}}</td>
                    <td>{{transaction.lease.leaseNumber}}</td>
                    <td>{{transaction.rentalUnit.unitReference}}</td>
                    <td>{{transaction.totalAmount | formatNumber(2) }}</td>
                </tr>
                </tbody>
            </table>
            <p v-if="transactions.length == 0" class="well well-sm"><g:message code="default.records.notfound" /></p>
        </div>
    </div>
</template>

<template id="tenant-terminate">
    <div class="pull-left" ref="tenantTerminate">
        <button class="btn btn-sm btn-danger" @click="openModalForm()"><i class="fa fa-times"></i> Request Termination</button>

        <modal v-model="openModal" ref="modal" id="tenant-terminate-form-" @hide="callback">

            <span slot="title"><i class="fa fa-check"></i> Tenant Termination Request</span>

            <span slot="default">
                <form method="POST" autocomplete="off">
                    <div class="form-group">
                        <label>Termination Date:</label>
                        <input v-model="tenantStatusRequest.actionDate" name="actionDate" type="text" class="form-control input-small" placeholder="DD/MM/YYYY" />
                    </div>

                    <div class="form-group">
                        <label>Termination Reason:</label>
                        <textarea v-model="tenantStatusRequest.reason" name="reason" class="form-control" ></textarea>
                    </div>
                </form>
            </span>

        </modal>
    </div>
</template>
<!-- END VUE COMPONENT MODALS -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/tenant/tenant-show.js" asset-defer="true" />

</body>
</html>
