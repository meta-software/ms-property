<%@ page import="metasoft.property.core.AccountType" %>

<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-2 col-md-2">
                    <g:select optionKey="id"
                              noSelection="['': 'All Types...']"
                              class="form-control select2-no-search"
                              name="accountType"
                              value="${params?.accountType}"
                              from="${AccountType.list()}"/>
                </div>
                <div class="col-lg-2 col-md-2">
                    <input type="text" class="form-control" placeholder="Account Name" name="accountName" value="${params?.accountName}">
                </div>
                <div class="col-lg-2 col-md-2">
                    <input type="text" class="form-control" name="accountNumber" placeholder="Account Number" value="${params?.accountNumber}">
                </div>
                <div class="col-lg-2 col-md-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-hover table-bordered table-condensed table-striped table-advanced " >
            <thead class="bg-grey-cararra">
            <tr>
                <th style="width: 30%">Name</th>
                <th style="width: 16%">Account No</th>
                <th style="width: 14%">Account Type</th>
                <th style="width: 14%">Bank Reference</th>
                <th style="width: 14%">VAT Number</th>
                <th style="width: 12%">Status</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${tenantList}">
                <g:each in="${tenantList}" var="tenant">
                    <tr>
                        <td>
                            <g:link class="display-block" action="show" id="${tenant.id}">${tenant}</g:link>
                        </td>
                        <td>${tenant.accountNumber}</td>
                        <td>${tenant.accountType}</td>
                        <td>${tenant.bankAccountNumber}</td>
                        <td>${tenant.vatNumber}</td>
                        <td><mp:makerCheck value="${tenant.dirtyStatus}" /></td>
                    </tr>
                </g:each>
            </g:if>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${tenantCount ?: 0}" params="${params}"  />
    </div>
</div>
