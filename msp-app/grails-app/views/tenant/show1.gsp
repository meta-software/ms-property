<%@ page import="metasoft.property.core.Property" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tenant')}"/>
    <g:set var="pageHeadTitle" value="Tenant Info - ${tenant}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Tenant - ${this.tenant}" args="[entityName]"/></title>

    <script type="text/javascript">
        let tenantId = ${tenant.id};
    </script>

</head>

<body>

<div id="main-app" v-cloak>

    <g:render template="page_bar_show"/>

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-4">
            <div class="portlet light bordered bg-inverse margin-top-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Tenant Details
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-condensed">
                        <tbody>
                        <tr><th style="width: 40%">Name: </th><td>${tenant}</td></tr>
                        <tr><th>Account No: </th><td>${tenant.accountNumber}</td></tr>
                        <tr><th>Account Type: </th><td>${tenant.accountType.name}</td></tr>
                        <tr><th>Bank Reference: </th><td>${tenant.bankAccountNumber}</td></tr>
                        <tr><th>Email: </th><td>${tenant.emailAddress}</td></tr>
                        <tr><th>Phone No: </th><td>${tenant.phoneNumber}</td></tr>
                        <tr><th>VAT No: </th><td>${tenant.vatNumber}</td></tr>
                        <tr><th>CheckStatus: </th><td><mp:makerCheck value="${tenant.dirtyStatus}" /></td></tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="col-md-8">
            <div class="portlet box grey-mint">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#tab-details1" data-toggle="tab">
                                <i class="fa fa-info"></i> Account Details
                            </a>
                        </li>
                        <li>
                            <a href="#tab-details2" data-toggle="tab">
                                <i class="fa fa-bank"></i> Bank Account
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-details1">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat blue" href="#">
                                        <div class="visual">
                                            <i class="fa fa-bar-chart-o"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="1">${activeLeaseCount}</span>
                                            </div>
                                            <div class="desc"> Active Leases </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat green" href="#">
                                        <div class="visual">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="1">$<g:formatNumber number="${accountBalance}" format="###,##0.00" /></span>
                                            </div>
                                            <div class="desc"> Balance </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <a class="dashboard-stat purple" href="#">
                                        <div class="visual">
                                            <i class="fa fa-angle-right"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span data-counter="counterup" data-value="1">$<g:formatNumber number="${depositBalance}" format="###,##0.00" /></span>
                                            </div>
                                            <div class="desc"> Deposit Balance </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-details2">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-building"></i>Bank Account
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <!-- BEGIN PROPERTIES LIST-->
                                    <g:if test="${tenant.approved && tenant.bankAccount}">
                                        <g:render template="/bankAccount/bankAccount" model="[bankAccount: bankAccount]"/>
                                    </g:if>
                                    <g:else>
                                        <mp:noRecordsFound />
                                    </g:else>
                                    <!-- BEGIN PROPERTIES LIST-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab">
                                Leases
                            </a>
                        </li>
                        <li @click="retrieveInvoices">
                            <a href="#tab2" data-toggle="tab">
                                Invoices
                            </a>
                        </li>
                        <li @click="retrieveReceipts">
                            <a href="#tab3" data-toggle="tab">
                                Payments
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="portlet-body">
                    <div class="tab-content">

                        <div class="tab-pane active" id="tab1">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-building-o"></i>Leases
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <!-- BEGIN LEASES LIST-->
                                    <g:render template="leases" model="['leaseList': tenant.leases]" />
                                    <!-- BEGIN LEASES LIST-->
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            <account-invoices ref="invoicesComponent" :account-id="tenant.id"></account-invoices>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <account-receipts ref="receiptsComponent" :account-id="tenant.id"></account-receipts>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE BASE CONTENT -->

    <g:if test="${tenant.isChecked()}">
        <!--g:render template="/makerChecker/makerChecker" model="[makerChecker: tenant.makerChecker]" /-->
    </g:if>

</div>

<!-- START VUE COMPONENT MODALS -->
<template id="account-invoices">
    <div class="portlet">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> Invoices
            </div>
        </div>

        <div class="portlet-body" ref="invoicesBody">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th>Txn Date</th>
                    <th>Reference</th>
                    <th>Narration</th>
                    <th>Lease #</th>
                    <th>Rental Unit</th>
                    <th>Txn Amount</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="transaction in transactions">
                    <td>{{transaction.transactionDate | formatDate('DD-MMM-YYYY')}}</td>
                    <td>{{transaction.transactionReference}}</td>
                    <td>{{transaction.description | shortify(80)}}</td>
                    <td>{{transaction.lease.leaseNumber}}</td>
                    <td>{{transaction.rentalUnit.unitReference}}</td>
                    <td>{{transaction.totalAmount | formatNumber(2) }}</td>
                </tr>
                </tbody>
            </table>
            <p v-if="transactions.length == 0" class="well well-sm"><g:message code="default.records.notfound" /></p>
        </div>
    </div>
</template>

<template id="account-receipts">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-cogs"></i> Receipts
            </div>
        </div>

        <div class="portlet-body" ref="invoicesBody">
            <table class="table table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th>Txn Date</th>
                    <th>Reference</th>
                    <th>Narration</th>
                    <th>Lease #</th>
                    <th>Rental Unit</th>
                    <th>Txn Amount</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="transaction in transactions">
                    <td>{{transaction.transactionDate | formatDate('DD-MMM-YYYY')}}</td>
                    <td>{{transaction.transactionReference}}</td>
                    <td>{{transaction.description | shortify(80)}}</td>
                    <td>{{transaction.lease.leaseNumber}}</td>
                    <td>{{transaction.rentalUnit.unitReference}}</td>
                    <td>{{transaction.credit | formatNumber | numberFormat }}</td>
                </tr>
                </tbody>
            </table>
            <p v-if="transactions.length == 0" class="well well-sm"><g:message code="default.records.notfound" /></p>
        </div>
    </div>
</template>

<template id="tenant-terminate">
    <div class="pull-left" ref="tenantTerminate">
        <button class="btn btn-sm btn-danger" @click="openModalForm()"><i class="fa fa-times"></i> Request Termination</button>

        <modal v-model="openModal" ref="modal" id="tenant-terminate-form-" @hide="callback">

            <span slot="title"><i class="fa fa-check"></i> Tenant Termination Request</span>

            <span slot="default">
                <form method="POST" autocomplete="off">
                    <div class="form-group">
                        <label>Termination Date:</label>
                        <input v-model="tenantStatusRequest.actionDate" name="actionDate" type="text" class="form-control input-small" placeholder="DD/MM/YYYY" />
                    </div>

                    <div class="form-group">
                        <label>Termination Reason:</label>
                        <textarea v-model="tenantStatusRequest.reason" name="reason" class="form-control" ></textarea>
                    </div>
                </form>
            </span>

        </modal>
    </div>
</template>
<!-- END VUE COMPONENT MODALS -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/tenant/tenant-show.js" asset-defer="true" />

</body>
</html>
