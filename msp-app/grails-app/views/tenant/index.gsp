<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tenants')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'tenant.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="tenant.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="main-app" v-cloak>
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
                <g:render template="list"/>
        </div>
    </div>
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/tenant/tenant-index.js" asset-defer="true"/>

</body>
</html>