<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tennant')}"/>
    <g:set var="pageHeadTitle" value="Tennant Info - ${tenant}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Tennant - ${this.tenant}" args="[entityName]"/></title>

    <script type="text/javascript">
        let tenantId = ${tenant.id};
    </script>
</head>

<body>

<div id="main-app" v-cloak>

</div>

<template id="lease-list-comp">
    <div class="table-scrollable" ref="leaseList" >


        <table class="table table-striped table-bordered table-hover table-condensed table-advance">
            <loading :active.sync="isLoading"
                      :can-cancel="false"
                      :opacity="0.4"
                      :container="$refs.leaseList"
                      :is-full-page="fullPage"></loading>
            <thead>
            <tr>
                <th>Lease Number {{tenantId}}</th>
                <th>Property</th>
                <th>Rental Unit</th>
                <th>Terminated</th>
                <th>Expired</th>
                <th>Start Date</th>
                <th>Expiry Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="lease in leases">
                <td><a class="display-block" :href="'/lease/show/'+lease.id">{{lease.leaseNumber}}</a></td>
                <td>{{lease.property.name}}</td>
                <td>{{lease.rentalUnit.name}}</td>
                <td>{{lease.terminated}}</td>
                <td>{{lease.expired}}</td>
                <td>{{lease.leaseStartDate}}</td>
                <td>{{lease.leaseExpiryDate}}</td>
                <td>&nbsp;</td>
            </tr>
            </tbody>

        </table>

    </div>
</template>


<asset:javascript src="vue-apps/tenant/tenant-show.js" asset-defer="true"/>

</body>
</html>
