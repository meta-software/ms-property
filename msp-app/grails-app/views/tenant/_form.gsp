<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>
<%@ page import="metasoft.property.core.AccountCategory" %>
<%@ page import="metasoft.property.core.AccountType" %>
<%@ page import="metasoft.property.core.Title" %>
<%@ page import="metasoft.property.core.Country" %>

<!-- BEGIN FORM PORTLET-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Tenant Form</div>

                <div class="actions">
                    <button type="button" class="btn btn-sm" @click="onCancel" >
                        <i class="fa fa-times"></i> Cancel
                    </button>
                    <button type="button" class="btn btn-sm green " @click="onSaveTenant" >
                        <i class="fa fa-check"></i> Save
                    </button>
                </div>
            </div>

            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['accountType'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Account Type
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select from="${AccountType.list()}"
                                          ref="accountType"
                                          optionKey="id"
                                          name="accountTypeSelect"
                                          noSelection="['null': 'Account Type...']"
                                          value="${tenant?.accountType?.id}"
                                          class="form-control select2-no-search1"/>
                                <input v-validate="'required'" type="hidden" name="accountType" v-model="tenant.accountType.id" />
                                <span class="has-error help-block-error help-block">{{ errors.first('accountType') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ${tenant.errors['phoneNumber'] ? 'has-error' : ''}">
                            <label class="control-label col-md-4">Phone Number
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field v-validate="'required|max:12|numeric'" class="form-control" type="text" v-model="tenant.phoneNumber" name="phoneNumber"
                                         value="${this.tenant.phoneNumber}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('phoneNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['accountName'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Account Name
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field v-validate="'required'" class="form-control" v-model="tenant.accountName" type="text" name="accountName" value="${tenant.accountName}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('accountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['faxNumber'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Cell No</label>

                            <div class="col-md-8">
                                <g:field v-validate="'max:12|numeric'" class="form-control" v-model="tenant.faxNumber" type="text" name="cellNumber"
                                         value="${this.tenant.faxNumber}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('cellNumber') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['emailAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Email
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="tenant.emailAddress" type="text" name="emailAddress"
                                         v-validate="'required|email'"
                                         value="${this.tenant.emailAddress}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('emailAddress') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['vatNumber'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">VAT Number</label>

                            <div class="col-md-8">
                                <g:field class="form-control" v-model="tenant.vatNumber" type="text" name="vatNumber"
                                         value="${this.tenant.vatNumber}"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('vatNumber') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['postalAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Postal Address</label>

                            <div class="col-md-8">
                                <div class="address-control">
                                    <g:field type="text" v-model="tenant.postalAddress.street" class="form-control" name="postalAddressStreet"
                                             value="${tenant.postalAddress?.street}"
                                             v-validate="'required'"
                                             placeholder="Street Address"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('postalAddressStreet') }}</span>
                                    <g:field type="text" v-model="tenant.postalAddress.suburb" class="form-control" name="postalAddressSuburb"
                                             value="${tenant.postalAddress?.suburb}"
                                             v-validate="'required'"
                                             placeholder="Suburb"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('postalAddressSuburb') }}</span>
                                    <g:field type="text" v-model="tenant.postalAddress.city" class="form-control" name="postalAddressCity"
                                             value="${tenant.postalAddress?.city}"
                                             v-validate="'required'"
                                             placeholder="City"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('postalAddressCity') }}</span>

                                    <g:select from="${Country.list()}"
                                              ref="postalAddressCountry"
                                              name="country"
                                              optionKey="id"
                                              noSelection="['null': 'Country ...']"
                                              value="${tenant.postalAddress?.country?.id}"
                                              class="form-control select2"/>
                                    <input v-validate="'required'" type="hidden" name="postalAddressCountry" v-model="tenant.postalAddress.country" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('postalAddressCountry') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['businessAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Business Address
                            </label>

                            <div class="col-md-8">
                                <div class="address-control">
                                    <g:field type="text" v-model="tenant.businessAddress.street" class="form-control" name="businessAddressStreet"
                                             value="${tenant.businessAddress?.street}"
                                             v-validate="'required'"
                                             placeholder="Street Address"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('businessAddressStreet') }}</span>
                                    <g:field type="text" v-model="tenant.businessAddress.suburb" class="form-control" name="businessAddressSuburb"
                                             value="${tenant.businessAddress?.suburb}"
                                             v-validate="'required'"
                                             placeholder="Suburb"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('businessAddressSuburb') }}</span>
                                    <g:field type="text" v-model="tenant.businessAddress.city" class="form-control" name="businessAddressCity"
                                             value="${tenant.businessAddress?.city}"
                                             v-validate="'required'"
                                    placeholder="City"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('businessAddressCity') }}</span>
                                    <g:select from="${Country.list()}"
                                              ref="businessAddressCountry"
                                              name="country"
                                              optionKey="id"
                                              noSelection="['null': 'Country ...']"
                                              value="${tenant.businessAddress?.country?.id}"
                                              class="form-control select2"/>
                                    <input v-validate="'required'" type="hidden" name="businessAddressCountry" v-model="tenant.businessAddress.country" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('businessAddressCountry') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group${tenant.errors['physicalAddress'] ? ' has-error' : ''}">
                            <label class="control-label col-md-4">Physical Address
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <div class="address-control">
                                    <g:field type="text" v-model="tenant.physicalAddress.street" class="form-control" name="physicalAddressStreet"
                                             value="${tenant.physicalAddress?.street}"
                                             v-validate="'required'"
                                             placeholder="Street Address"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('physicalAddressStreet') }}</span>
                                    <g:field type="text" v-model="tenant.physicalAddress.suburb" class="form-control" name="physicalAddressSuburb"
                                             value="${tenant.physicalAddress?.suburb}"
                                             v-validate="'required'"
                                             placeholder="Suburb"/>
                                    <g:field type="text" v-model="tenant.physicalAddress.city" class="form-control" name="physicalAddressCity"
                                             value="${tenant.physicalAddress?.city}"
                                             v-validate="'required'"
                                             placeholder="City"/>
                                    <g:select from="${Country.list()}"
                                              ref="physicalAddressCountry"
                                              name="country"
                                              optionKey="id"
                                              noSelection="['null': 'Country ...']"
                                              value="${tenant.physicalAddress?.country?.id}"
                                              class="form-control select2"/>
                                    <input v-validate="'required'" type="hidden" name="physicalAddressCountry" v-model="tenant.physicalAddress.country" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('physicalAddressCountry') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="form-section">Bank Account</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.name', default: 'Account Name')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="tenant.bankAccount.accountName" class="form-control" placeholder="" type="text" name="bankAccountName" />
                                <span class="has-error help-block help-block-error">{{ errors.first('bankAccountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyType', default: 'Bank')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select name="bank"
                                          v-model="tenant.bankAccount.bank.id"
                                          v-validate="'required'"
                                          noSelection="['': 'Select Bank...']"
                                          from="${metasoft.property.core.Bank.list()}"
                                          class="form-control"
                                          optionKey="id"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('bank') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyManager', default: 'Account Number')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="tenant.bankAccount.accountNumber" class="form-control" placeholder="" type="text" name="accountNumber" />
                                <span class="has-error help-block help-block-error">{{ errors.first('accountNumber') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Branch
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="tenant.bankAccount.branch" name="branch" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('branch') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Branch Code
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="tenant.bankAccount.branchCode" name="branchCode" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('branchCode') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<!-- END FORM PORTLET-->