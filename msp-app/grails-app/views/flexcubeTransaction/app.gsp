<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'flexcubeTransaction.label', default: 'Flexcube Transaction')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'flexcubeTransaction.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <router-view></router-view>
    </div>
</template>

<template id="transaction-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="search-bar bordered">
            <transaction-search v-on:search-request="searchTransactions"></transaction-search>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Txn Reference</th>
                    <th>Value Date</th>
                    <th>Txn Details</th>
                    <th>Credit Acc</th>
                    <th>Debit Acc</th>
                    <th>Receiving Bank</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Posted ?</th>
                    <th>Date Posted</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(transaction, key) in transactions" key="key">
                    <td>{{transaction.transactionReference}}</td>
                    <td>{{transaction.transactionDate | formatDate}}</td>
                    <td :title="transaction.transactionDetails">{{transaction.transactionDetails | shortify(64)}}</td>
                    <td>{{transaction.creditAccountBranch +'-'+transaction.accountNumberCr}}</td>
                    <td>{{transaction.debitAccountBranch +'-'+transaction.accountNumberDb}}</td>
                    <td>{{transaction.receivingBank}}</td>
                    <td>{{transaction.currency}}</td>
                    <td>{{transaction.transactionAmount | formatNumber}}</td>
                    <td><span class="label label-sm" :class="{'label-succes': transaction.posted, 'label-danger': !transaction.posted}">{{transaction.posted | formatBoolean}}</span></td>
                    <td>{{transaction.datePosted | formatDate}}</td>
                </tr>
                <tr v-if="transactions.length == 0">
                    <td colspan="10">No Records found...</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="search-pagination">
            <mp:pagination total="${tenantCount ?: 0}" />
        </div>
    </div>
</template>

<template id="transaction-show">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Transaction Info
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Id:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{flexcubeTransaction.id}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Type:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{flexcubeTransaction.routineType.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Billing Cycle:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{flexcubeTransaction.billingCycle.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Status:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static"><span :class="statusClass(flexcubeTransaction.requestStatus)" class="request-status label label-sm">{{flexcubeTransaction.requestStatus}}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{flexcubeTransaction.runDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Value Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{flexcubeTransaction.valueDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!--Transaction transactions.-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Transaction Transactions
                        </div>
                    </div>

                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <routine-transactions :routine-request-id="id"></routine-transactions>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-houzz"></i> Flexcube Transactions
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>
    </div>
</template>

<template id="transaction-search">
    <form role="form" autocomplete="off" @submit.prevent="requestSearch">
        <div class="form-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Sub Account</label>

                        <select2 v-model="query.subacc" name="subacc" :select2="subAccountOptions" class="form-control">
                            <option disabled value="0">Select one</option>
                        </select2>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Txn Reference</label>
                        <input type="text" class="form-control" name="txtef" v-model="query.txref">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Txn Date</label>
                        <input type="text" class="form-control" name="txDate" v-model="query.txndate">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Credit Acc</label>
                        <input type="text" class="form-control" name="cracc" v-model="query.cracc">
                    </div>
                </div>

            </div>
        </div>

        <div class="form-actions">
            <button type="button" @click="requestSearch" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
        </div>

    </form>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/flexcubeTransaction/flexcube-transaction-app.js" asset-defer="true"/>

</body>
</html>