<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Metafost Property :: [Real Estate management] - Welcome</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Welcome :: MetasoftWare" scope="request"/>
    <g:set var="pageSubTitle" value="version 1.0-SNAPSHOT" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<div id="main-app">
    <div class="row">
        <div class="col-md-12">
            <div class="note note-info">
                <p>Introduction to the Metasoft Properties :: real estate property management application</p>
            </div>

            <div>
                <h1>{{name}}</h1>
                <sub-account-select v-model="name" class="form-control">
                    <option disabled="disabled">Select One...</option>
                </sub-account-select>
                <button class="btn" type="button" @click="sample" >Click Here..</button>
            </div>

        </div>
    </div>
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/home/home-index.js" asset-defer="true"/>

</body>
</html>
