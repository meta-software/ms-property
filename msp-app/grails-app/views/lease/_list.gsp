<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" name="leaseNumber" class="form-control" placeholder="Lease Number..." value="${params?.leaseNumber}">
                </div>
                <div class="col-lg-3 col-md-3">
                    <input type="text" class="form-control" placeholder="Tenant..." name="tenant" value="${params?.tenant}">
                </div>
                <div class="col-md-2 col-lg-2">
                    <g:select noSelection="['': 'Select Currency...']" optionKey="id" name="currency" value="${params?.currency}" class="form-control select2-no-search" from="${currencyList}" />
                </div>
                <div class="col-md-2 col-lg-2">
                    <g:select noSelection="['': 'Select Status...']" optionKey="code" name="leaseStatus" value="${params?.leaseStatus}" class="form-control select2-no-search" from="${leaseStatusList}" />
                </div>
                <div class="col-lg-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-bordered table-striped table-hover table-condensed">
            <thead class="bg-grey-cararra">
            <tr>
                <th nowrap="nowrap" style="width: 8%">Lease No</th>
                <th style="width: 5%">Currency</th>
                <th style="width: 20%">Tenant</th>
                <th style="width: 20%">Property</th>
                <th nowrap="nowrap" style="width: 10%">Rental Unit</th>
                <th style="width: 5%">Status</th>
                <th nowrap="nowrap">Start Date</th>
                <th nowrap="nowrap">Expiry Date</th>
                <th>Check</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${leaseList}">
                <g:each in="${leaseList}" var="lease">
                    <tr class="mc-${lease?.checkStatus ?: metasoft.property.core.CheckStatus.PENDING}">
                        <td><g:link style="display: block" action="show" controller="lease" id="${lease.id}" >
                            ${lease.leaseNumber}
                        </g:link>
                        </td>
                        <td>${lease.leaseCurrency}</td>
                        <td>${lease.tenant}</td>
                        <td>${lease.property}</td>
                        <td>${lease.rentalUnit}</td>
                        <td>${mp.stateStatusClass(status: lease.leaseStatus.code, text: lease.leaseStatus.name)}</td>
                        <td nowrap="nowrap"><g:formatDate date="${lease.leaseStartDate}" format="dd MMM, yyyy" /></td>
                        <td nowrap="nowrap"><g:formatDate date="${lease.leaseExpiryDate}" format="dd MMM, yyyy" /></td>
                        <td><mp:makerCheck value="${lease.dirtyStatus}" /></td>
                    </tr>
                </g:each>
            </g:if>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${leaseCount ?: 0}" params="${params}" />
    </div>
</div>
