<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>
    
    <div class="page-toolbar">
        <g:link controller="lease" action="create" type="button" class="btn blue-hoki btn-sm">
            <i class="fa fa-plus"></i> Add Lease
        </g:link>

        <g:link controller="lease" action="index" class="btn default btn-sm">
            <i class="fa fa-list"></i> Leases
        </g:link>
    <!--lease-form></lease-form-->
    </div>
</div>