<%@ page import="metasoft.property.core.SubAccount" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'lease.label', default: 'Lease')}"/>
    <g:set var="pageHeadTitle" value="Lease Details: ${lease.leaseNumber}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Lease - ${lease.leaseNumber}" args="[entityName]"/></title>

    <script type="text/javascript">
        const leaseId = "${lease.id}";
    </script>

</head>

<body>

<div id="main-app" v-cloak >

    <g:render template="page_bar_show"/>

    <div class="margin-top-10">
        <div class="row">
            <div class="col-md-6">
                <div class="portlet light bordered bg-inverse entity-details">
                    <div class="portlet-body form">
                        <form class="horizontal-form" role="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Lease #: </label>
                                        <div class="form-control-static">${lease.leaseNumber}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Tenant: </label>
                                        <div class="form-control-static">${lease.tenant.accountName}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Lease Type: </label>
                                        <div class="form-control-static">${lease.leaseType}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Property: </label>
                                        <div class="form-control-static">${lease.property}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Rental Unit: </label>
                                        <div class="form-control-static">${lease.rentalUnit}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Landlord: </label>
                                        <div class="form-control-static">${landlord.accountName}</div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet light bordered entity-details">
                    <div class="portlet-body form">
                        <form class="horizontal-form" role="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Lease Currency: </label>
                                        <div class="form-control-static">${lease.leaseCurrency}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Invoicing Currency: </label>
                                        <div class="form-control-static">${lease.invoicingCurrency}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Start Date: </label>
                                        <div class="form-control-static"><g:formatDate date="${lease.leaseStartDate}" /></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Expiry Date: </label>
                                        <div class="form-control-static"><g:formatDate date="${lease.leaseExpiryDate}" /></div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Lease Status: </label>
                                        <div class="form-control-static">${mp.stateStatusClass(status: lease.leaseStatus.code, text: lease.leaseStatus.name)}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Balance: </label>
                                        <div class="form-control-static"><g:formatNumber number="${leaseStats['totalBalance']}" format="###,##0.00" /></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <g:render template="lease_items" model="[leaseItems: lease.leaseItems]" />
        </div>
        <div class="col-md-4">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light bordered bg-inversed">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-list"></i> Lease Details</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body " style="padding: 0px 10px;">
                        <table class="table table-condensed">
                            <tbody>
                            <tr><th>Last Review Date</th><td><g:formatDate date="${lease.lastReviewDate}" /></td></tr>
                            <tr><th>Next Review Date</th><td><g:formatDate date="${lease.nextReviewDate}" /></td></tr>
                            <tr><th>Expired Date</th><td><g:formatDate date="${lease.expiredDate}" /></td></tr>
                            <tr><th>Termination Date</th><td><g:formatDate date="${lease.dateTerminated}" /></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>

    <lease-requests :lease-id="leaseId"></lease-requests>

    <lease-event-history class="margin-top-10"></lease-event-history>
</div>

<template id="lease-terminate">
    <div class="pull-left" ref="leaseTerminate">
        <button class="btn btn-sm btn-danger" @click="openModalForm()"><i class="fa fa-times"></i> Request Termination</button>

        <modal v-model="openModal" ref="modal" id="lease-terminate-form-" @hide="callback">

            <span slot="title"><i class="fa fa-check"></i> Terminate Request</span>

            <span slot="default">
                <form method="POST" autocomplete="off">
                    <div class="form-group">
                        <label>Termination Date:</label>
                        <input v-model="leaseStatusRequest.actionDate" name="actionDate" type="text" class="form-control input-small" placeholder="YYYY-MM-DD" />
                    </div>

                    <div class="form-group">
                        <label>Termination Reason:</label>
                        <textarea v-model="leaseStatusRequest.actionReason" name="actionReason" class="form-control" ></textarea>
                    </div>
                </form>
            </span>

        </modal>
    </div>
</template>

<template id="lease-renewal">
    <div class="pull-left" ref="leaseRenewal">
        <button class="btn btn-sm green-seagreen" @click="openModalForm()"><i class="fa fa-check"></i> Request Renewal</button>

        <modal v-model="openModal" ref="modal" id="lease-renewal-form-" @hide="callback">

            <span slot="title"><i class="fa fa-check"></i> Lease Renewal Request</span>

            <span slot="default">
                <form method="POST" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Renewal Date:</label>
                                <input v-model="leaseStatusRequest.actionDate" name="actionDate" type="text" class="form-control input-small" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lease Expire Date:</label>
                                <input v-model="leaseStatusRequest.leaseExpireDate" name="leaseExpireDate" type="text" class="form-control input-small" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Renewal Reason:</label>
                                <textarea v-model="leaseStatusRequest.actionReason" name="actionReason" class="form-control" ></textarea>
                            </div>
                        </div>
                    </div>

                </form>
            </span>

        </modal>
    </div>
</template>

<template id="lease-item-list">
    <table class="table table-striped table-bordered table-hover table-condensed" ref="leaseItemList">
        <thead>
        <tr>
            <th>Type</th>
            <th>Status</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="leaseItem in leaseItems" :class="{'pending': leaseItem.checkStatus=='PENDING','invalid': leaseItem.checkerStatus=='REJECTED'}">
            <td>{{leaseItem.subAccount.accountName}}</td>
            <td>
                <span v-if="!leaseItem.active" class="label mc-status label-success">Active</span>
                <span v-else class="label mc-status label-danger">InActive</span>
            </td>
            <td>{{leaseItem.startDate | formatDate('DD MMM, YYYY') }}</td>
            <td>{{leaseItem.endDate | formatDate('DD MMM, YYYY') }}</td>
            <td>{{'$'}} {{leaseItem.amount | formatNumber(2)}}</td>
        </tr>
        <tr v-if="leaseItems.length == 0">
            <td colspan="5"><mp:noRecordsFound/></td>
        </tr>
        </tbody>
    </table>
</template>

<template id="lease-event-history">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-history"></i> Event History</div>
                </div>
                <div class="portlet-body">
                    <mp:noRecordsFound />
                </div>
            </div>
        </div>
    </div>
</template>

<template id="lease-requests">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-info"></i> Lease Requests</div>
                </div>
                <div class="portlet-body">
                    <table class="table table-bordered table-hover table-advance">
                        <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Action</th>
                            <th>Status</th>
                            <th>Date Created</th>
                            <th>Action Date</th>
                            <th>Processed</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="leaseRequest in leaseRequests">
                            <td>{{leaseRequest.id}}</td>
                            <td>Terminate</td>
                            <td><bs-label type="info" class="mc-status">{{leaseRequest.checkStatus}}</bs-label></td>
                            <td>{{leaseRequest.dateCreated | formatDate('DD MMM, YYYY')}}</td>
                            <td>{{leaseRequest.actionDate | formatDate('DD MMM, YYYY')}}</td>
                            <td><bool-label :bool-value="leaseRequest.processed" /></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/lease/lease-show.js" asset-defer="true"/>

</body>
</html>
