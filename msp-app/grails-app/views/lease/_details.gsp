<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <div class="portlet no-padding">
            <div class="portlet-body no-padding">

                <div id="show-lease" class="content scaffold-show" role="main">
                    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
                    <g:if test="${flash.message}">
                        <div class="message" role="status">${flash.message}</div>
                    </g:if>
                    <f:display bean="lease" />
                    <g:form resource="${this.lease}" method="DELETE">
                        <fieldset class="buttons">
                            <g:link class="edit" action="edit" resource="${this.lease}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                            <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                        </fieldset>
                    </g:form>
                </div>

            </div>
        </div>

    </div>
</div>
<!-- END PAGE BASE CONTENT -->
