<%@ page import="metasoft.property.core.SubAccount" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'lease.label', default: 'Lease')}"/>
    <g:set var="pageHeadTitle" value="Lease Details: ${lease.leaseNumber}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Lease - ${lease.leaseNumber}" args="[entityName]"/></title>

    <script type="text/javascript">
        const leaseId = "${lease.id}";
    </script>

</head>

<body>

<div id="main-app" v-cloak >

    <g:render template="page_bar_show"/>

    <g:if test="${flash.message || flash.error}">
        <g:render template="/templates/flash_message" />
    </g:if>

    <div class="row">
        <div class="col-md-4">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Lease Info
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body" style="padding: 0px 10px;">
                        <table class="table table-condensed">
                            <tbody>
                            <tr><th style="width: 40%">Lease No</th><td>${lease.leaseNumber}</td></tr>
                            <tr><th>Tenant</th><td><g:link class="display-block" controller="tenant" action="show" id="${lease.tenant.id}">${lease.tenant}</g:link> </td></tr>
                            <tr><th>Lease Type</th><td>${lease.leaseType}</td></tr>
                            <tr><th>Property</th><td><g:link class="display-block" controller="property" action="show" id="${lease.property?.id}">${lease.property}</g:link> </td></tr>
                            <tr><th>Rental Unit</th><td><g:link class="display-block" controller="rental-unit" action="show" id="${lease.rentalUnit.id}">${lease.rentalUnit}</g:link> </td></tr>
                            <tr><th>Created By: </th><td>${lease.maker?.fullName}</td></tr>
                            <tr><th>CheckStatus: </th><td><mp:makerCheck value="${lease.checkStatus}" /></td></tr>
                            <g:if test="${lease.isChecked()}">
                                <tr><th>Checked By: </th><td>${lease.checker?.fullName}</td></tr>
                                <tr><th>Checked Date: </th><td><g:formatDate date="${lease.checkDate}" format="dd-MM-yyyy" /></td></tr>
                            </g:if>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->

            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box grey-gallery margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-list"></i> Lease Details</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body " style="padding: 0px 10px;">
                        <table class="table table-condensed">
                            <tbody>
                            <tr><th>Last Review Date</th><td><g:formatDate date="${lease.lastReviewDate}" /></td></tr>
                            <tr><th>Next Review Date</th><td><g:formatDate date="${lease.nextReviewDate}" /></td></tr>
                            <tr><th>Termination Date</th><td><g:formatDate date="${lease.dateTerminated}" /></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>

        <div class="col-md-8">
            <div class="portlet box grey-mint">
                <div class="portlet-title">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#tab1" data-toggle="tab"> Lease Items </a>
                        </li>
                    </ul>
                    <div class="caption pull-right">
                        Lease Items
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" div="tab1">
                            <g:render template="lease_items" model="[leaseItems: lease.leaseItems]" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<template id="lease-terminate">
    <div class="pull-left" ref="leaseTerminate">
        <button class="btn btn-sm btn-danger" @click="openModalForm()"><i class="fa fa-times"></i> Request Termination</button>

        <modal v-model="openModal" ref="modal" id="lease-terminate-form-" @hide="callback">

            <span slot="title"><i class="fa fa-check"></i> Lease Termination Request</span>

            <span slot="default">
                <form method="POST" autocomplete="off">
                    <div class="form-group">
                        <label>Termination Date:</label>
                        <input v-model="leaseStatusRequest.actionDate" name="actionDate" type="text" class="form-control input-small" placeholder="YYYY-MM-DD" />
                    </div>

                    <div class="form-group">
                        <label>Termination Reason:</label>
                        <textarea v-model="leaseStatusRequest.actionReason" name="actionReason" class="form-control" ></textarea>
                    </div>
                </form>
            </span>

        </modal>
    </div>
</template>

<template id="lease-renewal">
    <div class="pull-left" ref="leaseRenewal">
        <button class="btn btn-sm green-seagreen" @click="openModalForm()"><i class="fa fa-check"></i> Request Renewal</button>

        <modal v-model="openModal" ref="modal" id="lease-renewal-form-" @hide="callback">

            <span slot="title"><i class="fa fa-check"></i> Lease Renewal Request</span>

            <span slot="default">
                <form method="POST" autocomplete="off">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Renewal Date:</label>
                                <input v-model="leaseStatusRequest.actionDate" name="actionDate" type="text" class="form-control input-small" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lease Expire Date:</label>
                                <input v-model="leaseStatusRequest.leaseExpireDate" name="leaseExpireDate" type="text" class="form-control input-small" placeholder="YYYY-MM-DD" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Renewal Reason:</label>
                                <textarea v-model="leaseStatusRequest.actionReason" name="actionReason" class="form-control" ></textarea>
                            </div>
                        </div>
                    </div>

                </form>
            </span>

        </modal>
    </div>
</template>

<template id="lease-item-list">
    <table class="table table-striped table-bordered table-hover table-condensed" ref="leaseItemList">
        <thead>
        <tr>
            <th>#</th>
            <th>Type</th>
            <th>Active</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="leaseItem in leaseItems" :class="{'pending': leaseItem.checkStatus=='PENDING','invalid': leaseItem.checkerStatus=='REJECTED'}">
            <td>{{leaseItem.id}}</td>
            <td>{{leaseItem.subAccount.accountName}}</td>
            <td>
                <span v-show="!leaseItem.active" class="label label-sm label-success">Active</span>
                <span v-show="leaseItem.active" class="label label-sm label-danger">InActive</span>
            </td>
            <td>{{leaseItem.startDate | formatDate('DD-MMM-YYYY') }}</td>
            <td>{{leaseItem.endDate | formatDate('DD-MMM-YYYY') }}</td>
            <td>{{'$ '+leaseItem.amount}}</td>
        </tr>
        </tbody>
    </table>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/lease/lease-show.js" asset-defer="true"/>

</body>
</html>
