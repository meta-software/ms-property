<%@ page import="metasoft.property.core.Property" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'lease.label', default: 'Lease')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'lease.list.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="lease.list.label" args="[entityName]" /></title>
</head>

<body>

<div id="main-app" v-cloak>
    <g:render template="page_bar" />

    <div class="row">
        <div class="col-md-12">
            <g:render template="list" />
        </div>
    </div>
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/lease/lease-index.js" asset-defer="true"/>

</body>
</html>