<div class="portlet bordered light">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-cogs"></i> Lease Items
        </div>
    </div>

    <div class="portlet-body">
        <div class="table-scrollable-borderless">
            <lease-item-list :lease="lease" ref="leaseItemList" ></lease-item-list>
            <table v-if="false" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Value</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <g:if test="${leaseItems}">
                    <g:each in="${leaseItems}" var="leaseItem">
                    <tr class="">
                        <td><i class="fa fa-edit"></i></td>
                        <td>${leaseItem.subAccount}</td>
                        <td>
                            <sec:ifAnyGranted roles="ROLE_PERM_ENTITY_CHECKER">
                                <g:if test="${leaseItem.maker?.id != applicationContext.springSecurityService.currentUser.id}">
                                    <maker-checker :item-controller="'lease-item'" :item-id="${leaseItem.id}" :status="'${leaseItem.makerChecker.status}'"></maker-checker>
                                </g:if>
                                <g:else>
                                    <button type="button" disabled="disabled" class="btn btn-xs btn-default">${leaseItem.makerChecker.status}</button>
                                </g:else>
                            </sec:ifAnyGranted>
                        </td>
                        <td><g:formatDate date="${leaseItem.startDate}" format="dd-MMM-yyyy" /></td>
                        <td><g:formatDate date="${leaseItem.endDate}" format="dd-MMM-yyyy" /></td>
                        <td><g:formatNumber number="${leaseItem.amount}" type="currency" /></td>
                        <td>
                            <div>

                            </div>
                        </td>
                    </tr>
                    </g:each>
                </g:if>
                <g:else>
                <tr>
                    <td colspan="7">
                        <mp:noRecordsFound />
                    </td>
                </tr>
                </g:else>
                </tbody>
            </table>
        </div>
    </div>
</div>
