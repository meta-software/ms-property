<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'lease.label', default: 'Lease')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'default.review.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.review.label" args="[entityName]"/></title>

    <script type="text/javascript">
        let leaseId = ${lease.id};
    </script>

</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.lease}">
    <g:render template="/templates/errors" model="['obj': this.lease]"/>
</g:hasErrors>

<g:form action="save" controller="lease" method="POST" autocomplete="false" class="form-horizontal" name="lease-form">
    <g:render template="review_form_app" bean="lease"/>
</g:form>

<template id="lease-item-form">
    <div>
        <table class="table table-hover table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th><input aria-label="check-all" type="checkbox" disabled="disabled" /></th>
                <th>Sub Account</th>
                <th>Amount</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Save</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <select name="subAccount" class="form-control input-sm" v-model="leaseItem.subAccount" v-validate="'required'">
                        <option v-for="option in subAccountOptions" :value="option.id">
                            {{ option.text }}
                        </option>
                    </select>
                    <span class="has-error help-block-error help-block">{{ errors.first('subAccount') }}</span>
                </td>
                <td>
                    <input name="amount" v-validate="'required|decimal'" v-model.number="leaseItem.amount" type="text" class="form-control input-sm" />
                    <span class="has-error help-block-error help-block">{{ errors.first('amount') }}</span>
                </td>
                <td>
                    <vdate-picker name="startDate" v-validate="'required'" v-model="leaseItem.startDate" class="form-control input-sm"></vdate-picker>
                    <span class="has-error help-block-error help-block">{{ errors.first('startDate') }}</span>
                </td>
                <td>
                    <vdate-picker name="endDate" v-validate="'required'" v-model="leaseItem.endDate" class="form-control input-sm"></vdate-picker>
                    <span class="has-error help-block-error help-block">{{ errors.first('endDate') }}</span>
                </td>
                <td><button @click="addLeaseItem" type="button" class="btn btn-primary btn-xs"><i class="fa fa-save" ></i></button></td>
            </tr>
            <tr v-for="(leaseItem, key) in leaseItems" :key="key">
                <td>&nbsp;</td>
                <td>{{leaseItem.subAccount.accountName}}</td>
                <td>{{leaseItem.amount | formatNumber}}</td>
                <td>{{leaseItem.startDate | formatDate('DD-MMM-YYYY')}}</td>
                <td>{{leaseItem.endDate | formatDate('DD-MMM-YYYY')}}</td>
                <td><button @click="removeLeaseItem(key)" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash" ></i></button></td>
            </tr>
            </tbody>
        </table>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="/vue-apps/lease/lease-review-form.js" asset-defer="true"/>

</body>
</html>

