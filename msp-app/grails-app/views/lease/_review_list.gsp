<table data-toggle="table" class="table table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Lease Number</th>
        <th>Tenant</th>
        <th>Property</th>
        <th>Rental Unit</th>
        <th>Last Review</th>
        <th>Start Date</th>
        <th>Expiry Date</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${leaseList}">
        <g:each in="${leaseList}" var="lease">
            <tr class="mc-${lease?.checkStatus ?: metasoft.property.core.CheckStatus.PENDING}">
                <td>
                    <g:link style="display: block" action="review" controller="lease" id="${lease.id}">
                        ${lease.leaseNumber}
                    </g:link>
                </td>
                <td>${lease.tenant}</td>
                <td>${lease.property}</td>
                <td>${lease.rentalUnit}</td>
                <td><g:formatDate date="${lease.lastReviewDate}" format="dd MMM, yyyy"/></td>
                <td><g:formatDate date="${lease.leaseStartDate}" format="dd MMM, yyyy"/></td>
                <td><g:formatDate date="${lease.leaseExpiryDate}" format="dd MMM, yyyy"/></td>
                <td><mp:makerCheck value="${lease.dirtyStatus}"/></td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="9">
                <g:message code="default.records.notfound"/>
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
