<%@ page import="metasoft.property.core.Property" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'lease.review.label', default: 'Lease Review Prompts')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'lease.list.review.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="lease.list.label" args="[entityName]" /></title>
</head>

<body>

<div id="main-app" v-cloak>
    <g:render template="page_bar_reviews" />

    <div class="row">
        <div class="col-md-12">
            <g:render template="search_form"/>
        </div>
    </div>

    <g:if test="${flash.message || flash.error}">
        <g:render template="/templates/flash_message" />
    </g:if>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet">
                <div class="portlet-body">
                    <div class="table-responsive">
                        <g:render template="review_list" />
                    </div>
                </div>
            </div>

            <div class="pagination">
                <mp:pagination total="${leaseCount ?: 0}" />
            </div>

        </div>
    </div>
</div>

<template id="lease-form">
    <div class="clearfix inline-block" ref="leaseForm">
        <button class="btn btn-sm blue-hoki" size="lg" @click="openModal=true"><i class="fa fa-plus"></i> Add Lease</button>

        <modal v-model="openModal" size="lg" ref="modal" id="lease-form-modal" @hide="modalCallback" >

            <span slot="title"><i class="fa fa-plus"></i> Create Lease</span>

            <form method="POST" role="form" class="form-horizontal">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tenant
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <g:select from="${metasoft.property.core.Tenant.approvedList()}"
                                          optionValue="accountName"
                                          optionKey="id"
                                          noSelection="['': 'Tenant Select...']"
                                          class="form-control select2"
                                          name="tenantItem"></g:select>
                                <input type="hidden" name="tenant" v-validate="'required'" v-model="leaseForm.tenant" />
                                <span class="has-error help-block help-block-error">{{ errors.first('tenant') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Property
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <g:select from="${metasoft.property.core.Property.approvedList()}"
                                          optionValue="name"
                                          optionKey="id"
                                          noSelection="['': 'Property Select...']"
                                          class="form-control select2"
                                          name="propertyItem"></g:select>
                                <input type="hidden" name="property" v-validate="'required'" v-model="leaseForm.property" />
                                <span class="has-error help-block help-block-error">{{ errors.first('property') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="rentalUnitItem" class="control-label col-md-3">Rental Unit
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <select class="form-control select2"
                                        name="rentalUnitItem"
                                        id="rentalUnitItem"></select>
                                <input type="hidden" v-model="leaseForm.rentalUnit" name="rentalUnit" v-validate="'required'">
                                <span class="has-error help-block help-block-error">{{ errors.first('rentalUnit') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Start Date
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <dropdown>
                                    <input name="leaseStartDate" placeholder="yyyy-MM-dd" ref="leaseStartDate" v-validate="'required|date_format:YYYY-MM-DD'" placeholder="dd-MM-yyy" class="form-control dropdown-toggle" type="text" v-model="leaseForm.leaseStartDate">
                                    <template slot="dropdown">
                                        <li>
                                            <date-picker format="yyyy-MM-dd" v-model="leaseForm.leaseStartDate" />
                                        </li>
                                    </template>
                                </dropdown>
                                <span class="has-error help-block help-block-error">{{ errors.first('leaseStartDate') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">End Date
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <dropdown>
                                    <input placeholder="yyyy-MM-dd" v-validate="'required|date_format:YYYY-MM-DD|after:leaseStartDate'" name="leaseExpiryDate" class="form-control dropdown-toggle" type="text" v-model="leaseForm.leaseExpiryDate">
                                    <template slot="dropdown">
                                        <li>
                                            <date-picker format="yyyy-MM-dd" v-model="leaseForm.leaseExpiryDate"/>
                                        </li>
                                    </template>
                                </dropdown>
                                <span class="has-error help-block help-block-error">{{ errors.first('leaseExpiryDate') }}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </form>


            <div slot="footer">
                <btn @click="openModal=false"><i class="fa fa-times"></i> Cancel</btn>
                <btn @click="save" class="blue-hoki" ><i class="fa fa-save"></i> Submit</btn>
            </div>

        </modal>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/lease/lease-index.js" asset-defer="true"/>

</body>
</html>