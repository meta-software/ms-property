<%@ page import="metasoft.property.core.LeaseType; metasoft.property.core.RentalUnit; metasoft.property.core.Tenant; metasoft.property.core.Lease" %>
<%@ page import="metasoft.property.core.LeaseItem" %>
<%@ page import="metasoft.property.core.Property" %>
<%@ page import="metasoft.property.core.LeaseStatus" %>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered bg-inverse margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Lease Form - #${lease.leaseNumber}</div>

                <div class="actions">
                    <button type="button" class="btn btn-sm default " @click="onCancel" >
                        <i class="fa fa-times"></i> Cancel Review
                    </button>
                    <button type="button" class="btn btn-sm green " @click="onSaveLease" >
                        <i class="fa fa-check"></i> Save Review
                    </button>
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM PORTLET-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group${lease.errors['tenant'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">Tenant
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" value="${lease.tenant.accountName}" disabled="disabled" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['rentalUnit'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3" for="propertySelect">Property
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" value="${lease.property.name}" disabled="disabled" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Rental Unit
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" value="${lease.rentalUnit.unitReference + " : " + lease.rentalUnit.name}" disabled="disabled" class="form-control" />
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['Start Date'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">Start Date
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" value="${lease.leaseStartDate.format('yyyy-MM-dd')}" disabled="disabled" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['End Date'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">End Date
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" value="${lease.leaseExpiryDate.format('yyyy-MM-dd')}" disabled="disabled" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['leaseType'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">Lease Type
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="text" value="${lease.leaseType.name}" disabled="disabled" class="form-control" />
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END FORM PORTLET-->
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-mint margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list-alt"></i> Lease Items</div>

            </div>

            <div class="portlet-body">
                <div class="tab-content">
                    <!-- BEGIN THE FORM LEASE ITEMS PART -->
                    <div class="row">
                        <div class="col-md-12">
                            <lease-item-form @remove-item="onRemoveLeaseItem" @add-item="onAddLeaseItem" :lease-items="lease.leaseItems"></lease-item-form>
                        </div>
                    </div>
                    <!-- END THE FORM LEASE ITEMS PART -->
                </div>
            </div>
        </div>
    </div>
</div>
