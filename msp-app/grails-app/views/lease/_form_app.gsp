<%@ page import="metasoft.property.core.LeaseType; metasoft.property.core.RentalUnit; metasoft.property.core.Tenant; metasoft.property.core.Lease" %>
<%@ page import="metasoft.property.core.LeaseItem" %>
<%@ page import="metasoft.property.core.Property" %>
<%@ page import="metasoft.property.core.LeaseStatus" %>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered bg-inverse margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Lease Form</div>

                <div class="actions">
                    <button type="button" class="btn btn-sm default " @click="onCancel" >
                        <i class="fa fa-times"></i> Cancel
                    </button>
                    <button type="button" class="btn btn-sm green " @click="onSaveLease" >
                        <i class="fa fa-check"></i> Save
                    </button>
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM PORTLET-->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group${lease.errors['tenant'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">Tenant
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <account-select
                                    :select2="tenantOptions"
                                    name="tenant"
                                    v-model="lease.tenant.id"
                                    v-validate="'required'"
                                    class="form-control"></account-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('tenant') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['rentalUnit'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3" for="propertySelect">Property
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <property-select
                                        :select2="propertyOptions"
                                        name="property"
                                        v-model="lease.property.id"
                                        v-validate="'required'"
                                        class="form-control"></property-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('property') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Rental Unit
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <rental-unit-select :disabled="!lease.property.id"
                                              :property-id="lease.property.id"
                                              v-model="lease.rentalUnit.id"
                                              v-validate="'required'"
                                              name="rentalUnit"
                                              class="form-control">
                                    <option disabled="disabled">Select Lease...</option>
                                </rental-unit-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('rentalUnit') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['Start Date'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">Start Date
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <datepicker placeholder="YYYY-MM-DD" v-validate="'required'" v-model="lease.leaseStartDate" name="leaseStartDate" class="form-control"></datepicker>
                                <span class="has-error help-block-error help-block">{{ errors.first('leaseStartDate') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['End Date'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">End Date
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <datepicker placeholder="YYYY-MM-DD" v-validate="'required'" v-model="lease.leaseExpiryDate" name="leaseExpiryDate" class="form-control"></datepicker>
                                <span class="has-error help-block-error help-block">{{ errors.first('leaseExpiryDate') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group${lease.errors['leaseType'] ? ' has-error' : ''}">
                            <label class="control-label col-md-3">Lease Type
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <g:select name="leaseTypeSelect"
                                          v-model="lease.leaseType.id"
                                          ref="leaseTypeSelect"
                                          optionKey="id"
                                          noSelection="['': 'Lease Type...']"
                                          from="${LeaseType.list()}"
                                          value="${lease.leaseType?.id}"
                                          class="form-control"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('leaseType') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Lease Currency
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <currency-select name="leaseCurrency" v-validate="'required'" v-model="lease.leaseCurrency.id" class="form-control" ></currency-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('leaseCurrency') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Invoicing Currency
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <currency-select name="invoicingCurrency" v-validate="'required'" v-model="lease.invoicingCurrency.id" class="form-control" ></currency-select>
                                <span class="has-error help-block-error help-block">{{ errors.first('invoicingCurrency') }}</span>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END FORM PORTLET-->
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-mint margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-list-alt"></i> Lease Items</div>

            </div>

            <div class="portlet-body">
                <div class="tab-content">
                    <!-- BEGIN THE FORM LEASE ITEMS PART -->
                    <div class="row">
                        <div class="col-md-12">
                            <lease-item-form @remove-item="onRemoveLeaseItem" @add-item="onAddLeaseItem" :lease-items="lease.leaseItems"></lease-item-form>
                        </div>
                    </div>
                    <!-- END THE FORM LEASE ITEMS PART -->
                </div>
            </div>
        </div>
    </div>
</div>
