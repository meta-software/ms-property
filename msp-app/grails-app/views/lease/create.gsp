<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'lease.label', default: 'Lease')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'default.create.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.lease}">
    <g:render template="/templates/errors" model="['obj': this.lease]"/>
</g:hasErrors>

<div id="lease-form-app">
    <g:form action="save" controller="lease" method="POST" autocomplete="off" class="form-horizontal" name="lease-form">
        <g:render template="form_app" bean="lease"/>
    </g:form>
</div>

<template id="lease-item-form">
    <div>
        <table class="table table-hover table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th><input aria-label="check-all" type="checkbox" disabled="disabled" /></th>
                <th>Sub Account</th>
                <th>Amount</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Save</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <sub-account-select name="subAccount" class="form-control input-sm" v-model="leaseItem.subAccount" v-validate="'required'">
                    </sub-account-select>
                    <span class="has-error help-block-error help-block">{{ errors.first('subAccount') }}</span>
                </td>
                <td>
                    <input name="amount" v-validate="'required|decimal'" v-model="leaseItem.amount" type="text" class="form-control input-sm" />
                    <span class="has-error help-block-error help-block">{{ errors.first('amount') }}</span>
                </td>
                <td>
                    <vdate-picker name="startDate" v-validate="'required'" v-model="leaseItem.startDate" class="form-control input-sm"></vdate-picker>
                    <span class="has-error help-block-error help-block">{{ errors.first('startDate') }}</span>
                </td>
                <td>
                    <vdate-picker name="endDate" v-validate="'required'" v-model="leaseItem.endDate" class="form-control input-sm"></vdate-picker>
                    <span class="has-error help-block-error help-block">{{ errors.first('endDate') }}</span>
                </td>
                <td><button @click="addLeaseItem" type="button" class="btn btn-primary btn-xs"><i class="fa fa-save" ></i></button></td>
            </tr>
            <tr v-for="(leaseItem, key) in leaseItems" key="key">
                <td><input aria-label="check-all" type="checkbox" disabled="disabled" /></td>
                <td>{{leaseItem.subAccount.accountName}}</td>
                <td>{{leaseItem.amount}}</td>
                <td>{{leaseItem.startDate}}</td>
                <td>{{leaseItem.endDate}}</td>
                <td><button @click="removeLeaseItem(key)" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash" ></i></button></td>
            </tr>
            </tbody>
        </table>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="/vue-apps/lease/lease-form.js" asset-defer="true"/>

</body>
</html>

