<%@ page import="metasoft.property.core.LeaseStatus" %>
<!-- BEGIN Portlet PORTLET-->
<div class="portlet box yellow m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Lease Number</label>
                            <input type="text" class="form-control" name="leaseNumber" value="${params?.leaseNumber}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Tenant</label>
                            <input type="text" class="form-control" name="tenant" value="${params?.tenant}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <g:select noSelection="['': 'Select Status....']" optionKey="code" name="leaseStatus" value="${params?.leaseStatus}" class="form-control select2" from="${LeaseStatus.list()}" />
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
                <button type="reset" class="btn btn-danger"><i class="fa fa-refresh"></i> Reset...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->