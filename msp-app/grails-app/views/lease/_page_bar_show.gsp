<div class="page-bar-show">
    <div class="page-bar-show-title">
        <div class="page-bar-show-header">
            <h4 class="page-bar-title">
                Lease : ${tenant.accountName}
                <small class="grey-cararra">[${lease.leaseNumber}]</small>
            </h4>
            <div class="page-bar-show-subtitle">
                <span>${property.name}</span> - <span>${rentalUnit.name}</span>
                | <mp:makerCheck value="${lease.dirtyStatus}" />
            </div>
        </div>
        <div class="page-toolbar">

            <div class="page-toolbar-context">
                <g:if test="${lease.isChecked()}">
                    <g:if test="${lease.isApproved() && lease.isActive()}">
                        <div class="pull-left">
                            <lease-terminate class="pull-left" style="margin-left: 10px" />
                        </div>

                        <div class="pull-left">
                            <lease-renewal class="pull-left" style="margin-left: 10px" />
                        </div>

                        <g:if test="${lease.expired == false}">
                            <g:link action="edit" controller="lease" id="${lease.id}" type="button" class="btn btn-sm blue-hoki pull-right">
                                <i class="fa fa-edit"></i> Edit Lease
                            </g:link>
                        </g:if>
                    </g:if>

                    <g:else>
                        <button disabled="disabled" type="button" class="btn red-mint btn-sm" style="margin-left: 10px" >
                            <i class="fa fa-times"></i> Terminate
                        </button>
                        <g:link action="edit" controller="lease" id="${lease.id}" type="button" class="btn btn-sm blue-hoki pull-right">
                            <i class="fa fa-edit"></i> Edit Lease
                        </g:link>
                    </g:else>

                </g:if>
            </div>

            <g:link controller="lease" class="btn btn-sm default" style="margin-right: 10px" ><i class="fa fa-list"></i> Leases</g:link>
        </div>
    </div>

    <!--div class="page-bar-show-actions">
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Edit</a>
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Manage Properties</a>
    </div-->
</div>
