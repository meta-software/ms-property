<!DOCTYPE html>
<html>
<head> 
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="{message(code: 'leaseTerminateRequestCheck.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle"
           value="{message(code: 'leaseTerminateRequestTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/vue/vue-bootstrap-table.min.js" asset-defer="true"/>
<asset:javascript src="vue-apps/leaseTerminateRequest/lease-terminate-request-inbox.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="lease-terminate-request-inbox-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" placeholder="Query Lease..."  v-model="params.q">
                    </div>
                    <div class="col-lg-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                        <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-bordered table-striped table-hover table-condensed">
                <thead class="bg-grey-cararra">
                <tr>
                    <th nowrap="nowrap" style="width: 8%">Request #</th>
                    <th style="width: 22.5%">Lease No</th>
                    <th style="width: 22.5%">Action Reason</th>
                    <th nowrap="nowrap">Tenant</th>
                    <th nowrap="nowrap">Terminate Date</th>
                    <th nowrap="nowrap">Requested By</th>
                </tr>
                </thead>
                <tbody>
                    <tr v-for="(leaseTerminateRequest, key) in leaseTerminateRequestInboxes" id="key">
                        <td>{{leaseTerminateRequest.id}}</td>
                        <td><router-link :to="{path: '/show/'+leaseTerminateRequest.id}">{{leaseTerminateRequest.lease.leaseNumber}}</router-link></td>
                        <td>{{leaseTerminateRequest.entity.actionReason}}</td>
                        <td>{{leaseTerminateRequest.lease.tenant}}</td>
                        <td>{{leaseTerminateRequest.entity.actionDate | formatDate('DD, MMM YYYY')}}</td>
                        <td>{{leaseTerminateRequest.maker.username}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Lease Terminate Requests
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" to="/"> <i class="fa fa-list"></i> List Requests</router-link>
            </div>
        </div>
    </div>
</template>

<template id="lease-terminate-request-inbox-show">
    <section >
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div v-if="leaseTerminateRequestInbox" class="row">
            <div class="col-md-8">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box red-mint margin-top-10">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-chain-broken"></i> Lease Terminate Request</div>
                    </div>

                    <div class="portlet-body">
                        <div class="portlet-body " style="padding: 0px 10px;">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width: 15%">Lease Number</th><td style="width: 35%">{{leaseTerminateRequestInbox.lease.leaseNumber}}</td>
                                    <th style="width: 15%">Lease Status</th><td style="width: 35%">{{leaseTerminateRequestInbox.lease.leaseStatus}}</td>
                                </tr>
                                <tr>
                                    <th>Action Reason</th><td colspan="3"><p>{{leaseTerminateRequestInbox.entity.actionReason}}</p></td>
                                </tr>
                                <tr>
                                    <th>Terminate Date</th><td>{{leaseTerminateRequestInbox.entity.actionDate | formatDate('DD MMM, YYYY')}}</td>
                                    <th>Request Status</th><td>{{leaseTerminateRequestInbox.checkStatus}}</td>
                                </tr>
                                <tr>
                                    <th>Requested By</th><td>{{leaseTerminateRequestInbox.maker.fullName}}</td>
                                    <th>Processed ?</th><td>{{leaseTerminateRequestInbox.entity.processed}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->

                <maker-checker @approved="fetchLeaseTerminateRequest" @rejected="fetchLeaseTerminateRequest" :reload="false" :makerid="leaseTerminateRequestInbox.maker.id" controller="lease-terminate-request-inbox" :item-id="leaseTerminateRequestInbox.id" :status="leaseTerminateRequestInbox.checkStatus"></maker-checker>

                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box blue-hoki margin-top-10">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-list"></i> Request Response</div>
                    </div>

                    <div class="portlet-body">
                        <div class="portlet-body " style="padding: 0px 10px;">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width: 15%">Response Date</th><td style="width: 35%">{{leaseTerminateRequestInbox.checkDate | formatDate('DD MMM, YYYY') }}</td>
                                    <th style="width: 15%">Responded By</th><td style="width: 35%">{{leaseTerminateRequestInbox.checker.fullName}}</td>
                                </tr>
                                <tr>
                                    <th>Response</th><td><p>{{leaseTerminateRequestInbox.checkComment}}</p></td>
                                    <th>Response Status</th><td>{{leaseTerminateRequestInbox.checkStatus}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->

            </div>

            <div class="col-md-4">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box grey-gallery margin-top-10">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-sticky-note"></i> Lease Details</div>
                    </div>

                    <div class="portlet-body">
                        <div class="portlet-body" style="padding: 0px 10px;">
                            <table class="table">
                                <tbody>
                                <tr><th style="width: 40%">Lease Status</th><td>{{lease.leaseNumber}}</td></tr>
                                <tr><th>Tenant</th><td>{{lease.tenant}}</td></tr>
                                <tr><th>Property</th><td>{{lease.property}}</td></tr>
                                <tr><th>Rental Unit</th><td>{{lease.rentalUnit}}</td></tr>
                                <tr><th>Lease Status</th><td>{{lease.leaseStatus }}</td></tr>
                                <tr><th>Start Date</th><td>{{lease.leaseStartDate | formatDate('DD MMM, YYYY') }}</td></tr>
                                <tr><th>Expiry Date</th><td>{{lease.leaseExpiryDate | formatDate('DD MMM, YYYY') }}</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>
        </div>

        <div v-show="leaseTerminateRequestInbox != null"></div>
    </section>
</template>

</body>
</html>