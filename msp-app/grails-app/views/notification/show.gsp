<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'tenant.label', default: 'Tennant')}"/>
    <g:set var="pageHeadTitle" value="Tennant Info - ${tenant}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Tennant - ${this.tenant}" args="[entityName]"/></title>
</head>

<body>

<g:render template="page_bar"/>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <div class="portlet box no-padding">
            <div class="portlet-body no-padding">
                <div class="tabbable-line boxless tabbable-reversed">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_0" data-toggle="tab">Tennant Info</a>
                        </li>
                        <li>
                            <a href="#tab_1" data-toggle="tab">Leases <span class="badge badge-success">${0}</span></a>
                        </li>
                        <li>
                            <a href="#tab_2" data-toggle="tab">Lease Units</a>
                        </li>
                    </ul>

                    <div class="tab-content" style="padding-top: 5px;">
                        <div class="tab-pane active" id="tab_0">
                            <!-- START FORM PORTLET -->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-info"></i>Tennant Info - ${tenant}</div>

                                    <div class="actions">
                                        <g:link class="btn blue btn-outline" action="edit" id="${tenant.id}">
                                            <i class="fa fa-edit"></i>
                                            Edit
                                        </g:link>
                                    </div>

                                </div>

                                <div class="portlet-body " style="padding: 0px 10px;">

                                    <!-- BEGIN FORM-->
                                    <form class="form-horizontal form-horizontal-left" role="form">
                                        <div class="form-body">
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Name:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">${tenant}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Account No:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${tenant?.accountNumber}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Tennant Type:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${tenant.accountType}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Phone:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">${tenant.phoneNumber}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Email:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${tenant.emailAddress}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-5 bold">Fax:</label>

                                                        <div class="col-md-7">
                                                            <p class="form-control-static">
                                                                ${tenant.faxNumber}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                        </div>
                                    </form>
                                    <!-- END FORM-->

                                </div>
                            </div>
                            <!-- END FORM PORTLET -->

                            <!-- START FORM PORTLET -->
                            <div class="portlet box blue-hoki">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-book"></i> Account Details - ${tenant?.accountNumber}
                                    </div>
                                </div>

                                <div class="portlet-body">
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-5 bold">Balance:</label>

                                                <div class="col-md-7">
                                                    <p class="form-control-static">
                                                        <g:formatNumber number="${tenant?.balance}"
                                                                        type="currency" currencyCode="USD"/>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label col-md-5 bold">Last Updated:</label>

                                                <div class="col-md-7">
                                                    <p class="form-control-static">
                                                        <g:formatDate date="${tenant?.lastUpdated}"
                                                                      format="dd/MMM/yyyy"/>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- END FORM PORTLET -->

                        </div>

                        <div class="tab-pane" id="tab_1">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-building-o"></i>Leases
                                    </div>

                                    <div class="actions">
                                        <!--
                                        <g:link class="btn blue btn-outline" controller="lease" action="create"
                                                params="['tenant.id': tenant.id]">
                                            <i class="fa fa-plus"></i>
                                            New Lease
                                        </g:link> -->
                                        <a class="btn blue btn-outline" data-toggle="modal" href="#_leaseFormModal">
                                            <i class="fa fa-plus"></i> Add Lease
                                        </a>
                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <!-- BEGIN PROPERTIES LIST-->
                                    <div class="note note-info">Placeholder for the list of leases.</div>
                                    <!-- BEGIN PROPERTIES LIST-->
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_2">
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i> Lease Unit
                                    </div>

                                    <div class="actions">
                                        <a class="btn btn-sm blue btn-outline" data-toggle="modal"
                                           href="#_leaseUnitFormModal">
                                            <i class="fa fa-plus"></i> Add Lease Unit
                                        </a>
                                    </div>

                                </div>

                                <div class="portlet-body">
                                    <p class="note">Tenant lease information here.</p>
                                    <!-- BEGIN STANDING-ORDERS LIST-- >
                                        <! g:render templ ate="standingOrders" />
                                        <!-- END STANDING-ORDERS LIST-->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START PREPARE VIEW MODALS -->
<div class="modal fade draggable-modal" id="_leaseFormModal" tabindex="-1" role="basic" aria-hidden="true">
    <g:form method="POST" controller="rentalUnit" action="save">
        <g:hiddenField name="tenant" value="${tenant.id}"/>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create Lease</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">
                        <p class="note note-info">Form placeholder...</p>
                    </div>
                    <!-- END FORM BODY-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Save Changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </g:form>
<!-- /.modal-dialog -->
</div>

<div class="modal fade draggable-modal" id="_leaseUnitFormModal" tabindex="-1" role="basic" aria-hidden="true">
    <g:form method="POST" controller="rentalUnit" action="save">
        <g:hiddenField name="tenant" value="${tenant.id}"/>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create Lease Unit</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">
                        <p class="note note-info">Lease Unit Form placeholder...</p>
                    </div>
                    <!-- END FORM BODY-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Save Changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </g:form>
<!-- /.modal-dialog -->
</div>
<!-- END PREPARE VIEW MODALS -->

</body>
</html>
