<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'notification.label', default: 'Notifications')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'notification.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="notification.list.label" args="[entityName]" default="Notifications"/></title>
</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<div class="row">
    <div class="col-md-12">
        <g:render template="search_form"/>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="table-scrollable">
            <g:render template="list"/>
        </div>

    </div>
</div>

</body>
</html>