<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Reference</th>
        <th>Entity</th>
        <th>Short Description</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${notificationList}">
        <g:each in="${notificationList}" var="notification">
            <tr>
                <td>${notification.message}</td>
                <td>${notification.entity}</td>
                <td><g:link controller="notification" action="show" id="${notification.id}">${notification.entity}</g:link></td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="3">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
