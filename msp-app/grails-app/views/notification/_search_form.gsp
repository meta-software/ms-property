<!-- BEGIN Portlet PORTLET-->
<div class="portlet box yellow m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Notification</label>
                            <input type="text" class="form-control" value="${params?.query}" />
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->