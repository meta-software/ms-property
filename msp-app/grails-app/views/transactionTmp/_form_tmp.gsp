<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Transaction Details - Batch# ${transactionHeaderTmp.id}</span>
                </div>


                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                </div>
                <!--div class="tools">
                    <a href="" class="reload" data-original-title="" title=""></a>
                </div-->
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div id="debit-form" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-blue-hoki bold uppercase"><i class="fa fa-arrow-circle-right"></i> Debit</span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label>GL Account ?</label>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <g:checkBox class="form-control" name="debitGlAccount" value="1"
                                                                checked=""/>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Debit Account</label>
                                            <select name="debitAccount"
                                                    id="debitAccount"
                                                    v-model="{{mainTransaction.debitAccount}}"
                                                    class="form-control select2-ajax">
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Debit Sub Account</label>
                                            <g:select
                                                    optionKey="id"
                                                    noSelection="['': 'Select Sub Account...']"
                                                    name="debitSubAccount"
                                                    optionValue="displayName"
                                                    v-model="{{mainTransaction.debitSubAccount}}"
                                                    from="${metasoft.property.core.SubAccount.list()}"
                                                    class="form-control select2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>

                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div id="credit-form" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="caption-subject font-green-dark bold uppercase"><i class="fa fa-arrow-circle-left"></i> Credit</span>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group">
                                            <label>GL Account ?</label>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <g:checkBox class="form-control" name="creditGlAccount" value="1"
                                                                checked=""/>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Credit Account</label>
                                            <select name="creditAccount"
                                                    id="creditAccount"
                                                    v-model="{{mainTransaction.creditAccount}}"
                                                    class="form-control select2-ajax">
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Credit Sub Account</label>
                                            <g:select
                                                    optionKey="id"
                                                    noSelection="['': 'Select Sub Account...']"
                                                    name="creditSubAccount"
                                                    optionValue="displayName"
                                                    v-model="{{mainTransaction.creditSubAccount}}"
                                                    from="${metasoft.property.core.SubAccount.list()}"
                                                    class="form-control select2"/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Amount</label>

                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <g:textField type="text"
                                       name="amount"
                                       placeholder="0.00"
                                       value="${mainTransactionTmp.amount}"
                                       v-model="mainTransaction.amount"
                                       v-validate="'required|decimal:2'"
                                       class="form-control"/>
                            </div>
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <g:textField type="text"
                                   name="transactionDate"
                                   class="form-control form-control-inline input-medium date-picker"
                                   size="16"
                                   autocomplete="off"
                                   v-validate="'required|date_format:DD/MM/YYYY'"
                                   v-model="mainTransaction.transactionDate"
                                   value="${mainTransactionTmp.transactionDate?.format('yy/MM/yyyy')}"
                                   placeholder="dd/MM/yyyy"/>
                            <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Reference</label>
                            <g:textField type="text"
                                   name="transactionReference"
                                   v-model="mainTransaction.transactionReference"
                                   v-validate="'required'"
                                   value="${mainTransactionTmp.transactionReference}"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('transactionReference') }}</span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>
                            <g:textField type="text"
                                   name="description"
                                   v-validate="'required'"
                                   v-model="mainTransaction.description"
                                   value="${mainTransactionTmp.description}"
                                   class="form-control"/>
                            <span class="has-error help-block">{{ errors.first('description') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="postTransaction"
                                    id="add-transaction-item"
                                    :disabled="transactionHeader.posted"
                                    class="btn blue">
                                <i class="fa fa-plus-square"></i> Add Transaction Item
                            </button>
                            <button type="button"
                                    @click="cancelTransaction"
                                    id="cancel-mainTransactionTmp-item"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

