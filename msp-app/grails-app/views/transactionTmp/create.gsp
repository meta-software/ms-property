<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'transactionHeaderTmp.label', default: 'Transaction')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'transactionHeaderTmp.create.label', default: entityName, args: [transactionHeaderTmp])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>
<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.mainTransactionTmp}">
    <g:render template="/templates/errors" model="['obj': this.mainTransactionTmp]"/>
</g:hasErrors>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light margin-top-10">

            <div class="portlet-body form">
                <g:form controller="transaction-tmp" autocomplete="off" action="save" method="POST" role="form">
                    <g:render template="form" />
                </g:form>

                <g:render template="transaction_list"/>

                <g:form action="post" controller="transactions" method="POST" role="form">
                    <g:hiddenField name="id" value="${mainTransactionTmp.transactionHeaderTmp.id}" />
                    <div class="form-actions left">
                        <g:if test="${transactionHeaderTmp.posted}">
                        <button name="update" type="button" disabled="disabled" class="btn green" ><i class="fa fa-save"></i> Post Transactions</button>
                        </g:if>
                        <g:else>
                        <button name="update" class="btn green" ><i class="fa fa-save"></i> Post Transactions</button>
                        </g:else>

                        <button type="button" id="cancel-transaction" class="btn default"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </g:form>
            </div>

        </div>
    </div>
</div>

</body>
</html>
