<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="generalLedger" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Transactions
        </g:link>
        <g:link controller="generalLedger" action="create" class="btn green btn-sm btn-outline ">
            <i class="fa fa-plus"></i> Add Transaction
        </g:link> 
    </div>
</div>