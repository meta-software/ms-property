<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Transaction Details - Batch# {{transactionHeader.batchNumber}}</span>
                </div>

                <div class="tools">
                    <a href="" class="reload" data-original-title="" title=""></a>
                </div>
            </div>

            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div id="debit-form" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bar-chart font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Debit</span>
                                </div>
                            </div>

                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label>GL Account ?</label>

                                            <div class="mt-checkbox-list">

                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <g:checkBox class="form-control" name="debitGlAccount" value="1"
                                                                checked="{{mainTransaction.debitGlAccount}}"/>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Debit Account</label>
                                            <select name="debitAccount"
                                                    id="debitAccount"
                                                    v-model="{{mainTransaction.debitAccount}}"
                                                    class="form-control select2-ajax">
                                            </select>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Debit Sub Account</label>
                                            <g:textField name="debitSubAccount"
                                                         v-model="{{mainTransaction.debitSubAccount}}"
                                                         class="form-control"/>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input class="form-control"
                                                   type="text"
                                                   disabled="disabled"
                                                   id="debit-sub-account-desc"/>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>

            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
