<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'glBankAccount.label', default: 'GL Bank Account')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'glBankAccount.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="glBankAccount.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="app"></div>

<template id="gl-bank-account-app">
    <div>
        <!--g:render template="page_bar"/-->
        <page-bar></page-bar>

        <router-view></router-view>
    </div>
</template>

<template id="gl-bank-account-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" v-model="params.query" class="form-control" placeholder="Account Name...." >
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="submit" click.prevent="search">Search</button>
                        <button class="btn green" type="button"><i class="fa fa-refresh" click="reset"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-bordered table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th>Account Name</th>
                    <th>Account #</th>
                    <th>Bank</th>
                    <th>Currency</th>
                    <th>Branch</th>
                    <th>GL Account</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="glBankAccount in glBankAccounts" :key="glBankAccount.id">
                    <td>{{glBankAccount.accountName}}</td>
                    <td>{{glBankAccount.accountNumber}}</td>
                    <td>{{glBankAccount.bank.bankName}}</td>
                    <td>{{glBankAccount.currency?.id}}</td>
                    <td>{{glBankAccount.branch}}</td>
                    <td>{{glBankAccount.generalLedger.accountNumber}} : {{glBankAccount.generalLedger.accountName}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4>GL Bank Accounts</h4>
        </div>

        <div class="page-toolbar">
            <div class="">
                <router-link class="btn btn-sm default" to="/">
                    <i class="fa fa-list"></i> List GL Banks
                </router-link>
                <router-link class="btn btn-sm blue-hoki" to="/create">
                    <i class="fa fa-plus"></i> Create GL Bank
                </router-link>
            </div>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<template id="gl-bank-account-create">
    <form action="save" method="POST" role="form" class="form-horizontal">
        <!-- BEGIN FORM PORTLET-->
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <h4 class="caption">GL Bank Account Form</h4>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Account Name
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input v-validate="'required'" class="form-control" type="text" name="accountName" v-model="glBankAccount.accountName" >
                                <span class="has-error help-block">{{ errors.first('accountName') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Account Number
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input v-validate="'required'" class="form-control" type="text" name="accountNumber" v-model="glBankAccount.accountNumber" >
                                <span class="has-error help-block">{{ errors.first('accountNumber') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Bank
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <select2 v-validate="'required'" v-model="glBankAccount.bank" name="bank" :select2="bankOptions" class="form-control">
                                    <option disabled value="0">Select one</option>
                                </select2>
                                <span class="has-error help-block">{{ errors.first('bank') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Currency
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <currency-select v-validate="'required'" ref="currency" v-model="glBankAccount.currency" class="form-control"
                                                 name="currency"></currency-select>
                                <span class="has-error help-block">{{ errors.first('currency') }}</span>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Branch
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input v-validate="'required'" class="form-control" type="text" name="branch"
                                       v-model="glBankAccount.branch"
                                       value=""/>
                                <span class="has-error help-block">{{ errors.first('branch') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Branch Code
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input v-validate="'required'" class="form-control" type="text" name="branchCode"
                                       v-model="glBankAccount.branchCode"
                                       value=""/>
                                <span class="has-error help-block">{{ errors.first('branchCode') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">General Ledger
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <gl-account-select v-validate="'required'" :account-type="'bank'" v-model="glBankAccount.generalLedger" class="form-control"
                                                   name="generalLedger"></gl-account-select>
                                <span class="has-error help-block">{{ errors.first('generalLedger') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="button"
                                    @click="onSubmit"
                                    id="add-gl-bank-account"
                                    :disabled="!valid"
                                    class="btn blue">
                                <i class="fa fa-plus"></i> Add Bank Account
                            </button>
                            <button type="button"
                                    @click="onCancel"
                                    id="cancel-gl-bank-account"
                                    class="btn default">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- END FORM PORTLET-->
    </form>
</template>

<template id="gl-bank-account-edit">
    <div>GL Bank Account::Edit</div>
</template>

<template id="acc-type-create-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Account Type</div>
                </div>

                <div class="portlet-body">
                    <acc-type-form-comp v-on:save="save" v-bind:account-type="glBankAccount"></acc-type-form-comp>
                </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/glBankAccount/gl-bank-account.js" asset-defer="true"/>

</body>
</html>