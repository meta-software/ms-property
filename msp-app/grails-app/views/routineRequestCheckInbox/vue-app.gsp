<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'routineRequestCheck.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'routineRequestTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/vue/vue-bootstrap-table.min.js" asset-defer="true"/>
<asset:javascript src="vue-apps/routineRequest/routine-request-check-inbox.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="routine-request-check-list">

    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" placeholder="Query Request..."  v-model="params.q">
                    </div>
                    <div class="col-lg-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                        <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>Request #</th>
                    <th>Action</th>
                    <th>Type</th>
                    <th>Cycle</th>
                    <th>Run Date</th>
                    <th>Value Date</th>
                    <th>Created By</th>
                    <th>Date Created</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(routineRequestCheck, key) in routineRequestChecks" key="key">
                    <td>
                        <router-link class="display-block" :to="{name: 'show', params: {id: routineRequestCheck.id}}">{{routineRequestCheck.id}}</router-link>
                    </td>
                    <td>{{routineRequestCheck.actionName}}</td>
                    <td>{{routineRequestCheck.entity.routineType.name}}</td>
                    <td>{{routineRequestCheck.entity.billingCycle.name}}</td>
                    <td>{{routineRequestCheck.entity.runDate | formatDate }}</td>
                    <td>{{routineRequestCheck.entity.valueDate | formatDate }}</td>
                    <td>{{routineRequestCheck.maker.fullName }}</td>
                    <td>{{routineRequestCheck.entity.dateCreated | formatDate('YYYY-MM-DD HH:mm') }}</td>
                </tr>
                <tr v-if="routineRequestChecks.length == 0">
                    <td colspan="8">No Records found...</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Routine Requests
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" to="/"> <i class="fa fa-list"></i> List Requests</router-link>
            </div>
        </div>
    </div>
</template>

<template id="routine-request-check-show">
    <section>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Routine Request
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        ID:
                                    </label>
                                    <div class="col-md-7">{{routineRequestCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Status:
                                    </label>
                                    <div class="col-md-7">{{routineRequestCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Action:
                                    </label>
                                    <div class="col-md-7">{{routineRequestCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Entity:
                                    </label>
                                    <div class="col-md-7">{{routineRequestCheck.entity.routineType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Date Created:
                                    </label>
                                    <div class="col-md-7">{{routineRequestCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Maker:
                                    </label>
                                    <div class="col-md-7">{{routineRequestCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <maker-checker :makerid="routineRequestCheck.maker.id" :item-id="routineRequestCheck.id" :controller="'routine-request-check-inbox'" :status="routineRequestCheck.checkStatus"></maker-checker>

        <div class="row margin-top-10">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Outbox Routine Request : #{{routineRequestCheck.id}}
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Id:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{routineRequestCheck.entity.id}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Type:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{routineRequestCheck.entity.routineType.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Billing Cycle:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{routineRequestCheck.entity.billingCycle.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Status:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{routineRequestCheck.checkStatus}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{routineRequestCheck.entity.runDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Value Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{routineRequestCheck.entity.valueDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</template>


</body>
</html>