<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'billingInvoice.label', default: 'Billing Invoice')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'billingInvoice.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <asset:stylesheet src="pages/css/invoice.css"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <router-view></router-view>
    </div>
</template>

<template id="billing-invoice-list">

    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>

        <transaction-search v-on:search-request="searchInvoices"></transaction-search>

        <div class="search-table table-responsive">
            <table class="table table-striped table-hover table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Invoice Number</th>
                    <th>Billing Cycle</th>
                    <th>Tenant</th>
                    <th>Lease</th>
                    <th>Email Sent</th>
                    <th style="text-align: right; padding-right: 5px;">Invoice Total</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(billingInvoice, key) in billingInvoices" :key="key" @click="$router.push('/'+billingInvoice.id)">
                    <td>{{billingInvoice.invoiceNumber}}</td>
                    <td>{{billingInvoice.billingCycle.name}}</td>
                    <td>{{billingInvoice.tenant.accountName}}</td>
                    <td>{{billingInvoice.lease.leaseNumber + ' : ' + billingInvoice.rentalUnit.name }}</td>
                    <td>{{billingInvoice.emailSent}}</td>
                    <td style="text-align: right; padding-right: 5px;">{{ billingInvoice.invoiceTotal | formatNumber}}</td>
                </tr>
                <tr v-if="billingInvoices.length == 0">
                    <td colspan="7">No Records found...</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>

</template>

<template id="billing-invoice-show">
    <section class="margin-top-10" >
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <!-- invoice -->
        <div class="invoice" v-if="billingInvoice != null">
            <div class="row invoice-logo">
                <div class="col-xs-6 invoice-logo-space">
                    <img src="/assets/pages/img/invoices-logo.jpg" class="img-responsive" alt="" />
                </div>
                <div class="col-xs-6">
                    <p> #{{billingInvoice.invoiceNumber}} / {{billingInvoice.invoiceDate | formatDate('DD MMM YYYY') }}</p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-xs-4">
                    <h4>{{billingInvoice.property.name}}</h4>
                    <h4>Tenant:</h4>
                    <ul class="list-unstyled">
                        <li> {{billingInvoice.tenant.accountName}} </li>
                        <li style="padding-left: 5px; padding-top: 5px;"><address-show :address="billingInvoice.tenant.businessAddress" ></address-show> </li>
                    </ul>
                </div>
                <div class="col-xs-4 col-xs-offset-4 invoice-payment">
                    <h4>Payment Details:</h4>
                    <table class="table table-condensed display-none">
                        <tbody>
                            <tr><th>V.A.T Reg #</th><td>{{billingInvoice.tenant.vatNumber}} </td></tr>
                            <tr><th>Account Name:</th><td> {{billingInvoice.tenant.accountName}} </td></tr>
                            <tr><th>Tenant Number:</th><td>{{billingInvoice.tenant.accountNumber}}</td></tr>
                            <tr><th>Lease Number:</th><td>{{billingInvoice.lease.leaseNumber}}</td></tr>
                        </tbody>
                    </table>
                    <ul class="list-unstyled">
                        <li><strong>V.A.T Reg #:</strong>  {{billingInvoice.tenant.vatNumber}} </li>
                        <li><strong>Account Name:</strong> {{billingInvoice.tenant.accountName}} </li>
                        <li><strong>Tenant Number:</strong> {{billingInvoice.tenant.accountNumber}}</li>
                        <li><strong>Lease Number:</strong> {{billingInvoice.lease.leaseNumber}}</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th> Reference# </th>
                            <th> Narrative </th>
                            <th class="hidden-xs"> Vat </th>
                            <th> Amount </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(billingInvoiceItem, key) in billingInvoice.billingInvoiceItems" :key="key">
                            <td> {{billingInvoiceItem.transactionReference}} </td>
                            <td class="hidden-xs"> {{billingInvoiceItem.narrative}} </td>
                            <td class="hidden-xs"> {{billingInvoiceItem.vatAmount | formatNumber}} </td>
                            <td class="hidden-xs"> {{billingInvoiceItem.invoiceAmount | formatNumber}} </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-offset-7 col-xs-5 invoice-block amounts">
                    <div class="well">
                        <div class="row">
                            <div class="col-md-8"><strong>Sub Total Amount:</strong></div><div class="col-md-2">{{billingInvoice.invoiceSubTotal | formatNumber}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-8"><strong>Discount:</strong></div><div class="col-md-2">{{billingInvoice.discountTotal | formatNumber}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-8"><strong>VAT Total:</strong></div><div class="col-md-2">{{billingInvoice.vatTotal | formatNumber}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-8"><strong>Invoice Total:</strong></div><div class="col-md-2">{{invoiceTotal | formatNumber}}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-actions">
                        <a disabled="disabled" class="btn btn-lg btn-default grey hidden-print margin-bottom-5">
                            <i class="fa fa-comment"></i> SMS Notify
                        </a>

                        <a class="btn btn-lg blue-hoki margin-bottom-5" @click="emailInvoice">
                            <i class="fa fa-envelope"></i> Email Invoice
                        </a>

                        <button type="button" @click="printInvoice" class="btn btn-lg blue hidden-print margin-bottom-5">
                            <i class="fa fa-print"></i> Print Invoice
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- invoice -->

    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-files-o"></i> Billing Invoices
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <router-link class="btn btn-sm default" to="/">
                <i class="fa fa-list"></i> Billing Invoices
            </router-link>
        </div>
    </div>
</template>

<template id="transaction-search">
    <div class="search-bar bordered">
        <form autocomplete="off" @submit.prevent="requestSearch">
            <div class="row">
                <div class="col-md-2 col-lg-2">
                    <div class="form-group">
                        <select2 v-model="query.cycle" name="cycle" :select2="billingCycleOptions" class="form-control">
                            <option disabled value="0">Select one</option>
                        </select2>
                    </div>
                </div>

                <div class="col-md-2 col-lg-2">
                    <div class="form-group">
                        <input type="text" placeholder="Tenant..." class="form-control" name="tenant" v-model="query.tenant">
                    </div>
                </div>

                <div class="col-md-2 col-lg-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/billingInvoice/billing-invoice-app.js" asset-defer="true"/>

</body>
</html>