<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'transactionHeaderTmp.label', default: 'Pending Transactions')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'transactionHeaderTmp.list.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="transactionHeaderTmp.list.label" args="[entityName]" /></title>
</head>

<body>

<g:render template="page_bar" />

<div class="row">
    <div class="col-md-12">
        <g:render template="search_form"/>
    </div>
</div>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message" />
</g:if>

<div class="row">
    <div class="col-md-12">

        <div class="portlet">
            <div class="portlet-body">
                <div class="table-scrollable">
                    <g:render template="list" />
                </div>
            </div>
        </div>

        <div class="pagination">
            <mp:pagination total="${transactionHeaderTmpCount ?: 0}" />
        </div>

    </div>
</div>

<asset:javascript src="transactionHeaderTmp-form.js" asset-defer="true"/>

</body>
</html>