<form role="form" autocomplete="off">
    <div class="row">
        <div class="col-lg-2 col-md-2">
            <input type="text" class="form-control" name="batchNumber" placeholder="Batch Number..." value="${params?.batchNumber}">
        </div>
        <div class="col-lg-2 col-md-2">
            <input type="text" class="form-control" name="creator" placeholder="Created By..." value="${params?.creator}">
        </div>
        <div class="col-lg-2 col-md-2 extra-buttons">
            <button class="btn grey-steel uppercase bold" type="submit">Search</button>
            <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
        </div>
    </div>
</form>
<!-- END Portlet PORTLET-->