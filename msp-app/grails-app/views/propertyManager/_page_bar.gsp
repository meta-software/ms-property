<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="property-manager" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Property Managers
        </g:link>
        <!-- <a class="btn blue btn-sm btn-outline" data-toggle="modal"
           href="#_paycodeFormModal">
            <i class="fa fa-plus"></i> Add Property Manager
        </a> -->

        <g:link controller="property-manager" action="create" class="btn btn-sm blue-hoki">
            <i class="fa fa-plus"></i> Add Property Manager
        </g:link>
    </div>
</div>