<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>Full Name</th>
        <th>Email</th>
        <th>Active ?</th>
        <th>Phone Number</th>
        <th>Appointment Date</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${propertyManagerList}">
        <g:each in="${propertyManagerList}" var="propertyManager">
            <tr>
                <td><g:link style="display: block" action="show" id="${propertyManager.id}" >
                    ${propertyManager.fullName}
                </g:link>
                </td>
                <td>${propertyManager.emailAddress}</td>
                <td><mp:boolStatusLabel status="${propertyManager.active}" /></td>
                <td>${propertyManager.phoneNumber}</td>
                <td><g:formatDate date="${propertyManager.appointmentDate}" /></td>
                <td>
                    <div class="actions">
                        <g:link action="show" id="${propertyManager.id}"  class="btn btn-xs btn-primary" >
                            <i class="fa fa-eye"></i>
                        </g:link>
                        <g:link action="edit" id="${propertyManager.id}"  class="btn btn-xs btn-warning"  >
                            <i class="fa fa-edit"></i>
                        </g:link>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
