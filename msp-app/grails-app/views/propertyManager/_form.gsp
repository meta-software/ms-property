<!-- BEGIN FORM PORTLET-->
<div class="portlet light">

    <div class="portlet-body form">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${propertyManager.errors['firstName'] ? ' has-error' : ''}">
                    <label class="control-label col-md-3">First Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" type="text" name="firstName"
                                 value="${propertyManager.firstName}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group${propertyManager.errors['lastName'] ? ' has-error' : ''}">
                    <label class="control-label col-md-3">Last Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" type="text" name="lastName" value="${propertyManager?.lastName}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${propertyManager.errors['emailAddress'] ? ' has-error' : ''}">
                    <label class="control-label col-md-3">Email Address
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" type="text" name="emailAddress"
                                 value="${propertyManager.emailAddress}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group${propertyManager.errors['active'] ? ' has-error' : ''}">
                    <label class="control-label col-md-3">Is Active?
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <g:checkBox class="form-control icheck" name="active" checked="${propertyManager.active}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${propertyManager.errors['phoneNumber'] ? ' has-error' : ''}">
                    <label class="control-label col-md-3">Phone Number </label>

                    <div class="col-md-9">
                        <span></span>

                        <g:field class="form-control" type="text" name="phoneNumber"
                                 value="${propertyManager.phoneNumber}"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group${propertyManager.errors['appointmentDate'] ? ' has-error' : ''}">
                    <label class="control-label col-md-3">Appointment Date </label>

                    <div class="col-md-9">
                        <span></span>

                        <g:datePicker name="appointmentDate"
                                      precision="month"
                                      relativeYears="[-10..1]"
                                      value="${propertyManager.appointmentDate}"
                                      class="form-control select2" />

                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${propertyManager.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>

    </div>
</div>

<!-- END FORM PORTLET-->