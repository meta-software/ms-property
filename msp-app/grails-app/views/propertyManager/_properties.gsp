<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover table-advance table-condensed">
        <thead>
        <tr>
            <th>Name</th>
            <th>City</th>
            <th>Country</th>
            <th>Property Type</th>
            <th>Area</th>
            <th>Commission</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${propertyList}">
            <g:each in="${propertyList}" var="property">
                <tr>
                    <td>
                        <g:link controller="property" action="show" resource="${property}"
                                style="display: block;">${property}</g:link>
                    </td>
                    <td>${property.address.city}</td>
                    <td>${property.address.city}</td>
                    <td>${property.propertyType}</td>
                    <td><g:formatNumber number="${property.area}" format="###,###.0" /> m<sup>2</sup></td>
                    <td>${property.commission} %</td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="6">
                    <g:message message="default.records.notfound" />
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>

<div class="pagination">
    <g:paginate total="${propertyCount ?: 0}"/>
</div>