<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyManager.label', default: 'Property Manager')}"/>
    <g:set var="pageHeadTitle" value="Property Manager Info - ${propertyManager.fullName}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>
</head>

<body>

<g:render template="page_bar_show"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-4">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info"></i> Property Manager Details
                </div>
            </div>

            <div class="portlet-body">
                <table class="table">
                    <tbody>
                    <tr><th style="width: 40%">Name: </th><td>${propertyManager.fullName}</td></tr>
                    <tr><th>Appointment Date: </th><td><g:formatDate date="${propertyManager.appointmentDate}" format="dd-MMM-yyyy" /></td></tr>
                    <tr><th>Property Type: </th><td>${propertyManager.emailAddress}</td></tr>
                    <tr><th>Phone Number: </th><td>${propertyManager.phoneNumber}</td></tr>
                    <tr><th>Active: </th><td><span class="label label-${mp.boolStatusClass(status: propertyManager.active)} label-sm">${propertyManager.active ? 'Yes' : 'No'}</span>
                    </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet box blue-hoki">
            <div class="portlet-title">
                <ul class="nav nav-tabs pull-left">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab">
                            Properties <span class="badge badge-info">${propertiesCount}</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="portlet">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-building"></i>Properties
                                </div>
                            </div>

                            <div class="portlet-body">
                                <!-- BEGIN PROPERTIES LIST--!>
                                <g:render template="properties" />
                                <!-- BEGIN PROPERTIES LIST-->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

</body>
</html>