<!-- START MODALS -->
<div class="modal fade" id="_paycodeFormModal" tabindex="-1" role="basic" aria-hidden="true">
    <g:form method="POST" controller="paycode" action="save" name="_paycodeForm">
        <g:hiddenField name="id" value="${paycode?.id}"/>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create New Paycode</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">
                        <div class="row">
                            <!-- span -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Name</label>
                                    <g:field type="text" id="_paycode-name" class="form-control"
                                           name="name"
                                           placeholder="Paycode name" />
                                </div>
                            </div>
                            <!--span-->
                            <!--span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Code</label>
                                    <g:field type="text" name="code" id="_paycode-reference"
                                             class="form-control"
                                             placeholder="reference-code"/>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">

                            <!--span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <g:field type="text" name="description" id="_paycode-description"
                                             class="form-control"
                                             placeholder="" />
                                </div>
                            </div>
                            <!--/span-->

                            <!--span-->
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Is Active ?</label>

                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" name="active" id="_paycode-active" value="1"
                                                   checked="checked">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                    </div>
                    <!-- END FORM BODY-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green" id="_paycode-submit">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </g:form>
<!-- /.modal-dialog -->
</div>
<!-- END MODALS -->