<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyManager.label', default: 'Property Manager')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'propertyManager.create.label', default: entityName, args: [propertyManager])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>
<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.propertyManager}">
    <g:render template="/templates/errors" model="['obj': this.propertyManager]"/>
</g:hasErrors>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Property Manager</div>
            </div>

            <div class="portlet-body">
                <g:form action="save" method="POST" role="form" class="form-horizontal">
                    <g:render template="form"/>
                </g:form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
