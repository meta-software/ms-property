<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${accountTypeList}">
        <g:each in="${accountTypeList}" var="accountType">
            <tr>
                <td><g:link style="display: block" action="edit" id="${accountType.id}" >
                    ${accountType.name}
                </g:link>
                </td>
                <td>${accountType.code}</td>
                <td>
                    <span class="label label-sm label-${mp.boolStatusClass(status: accountType.active)}">${accountType.active ? 'Active' : 'Inactive'}</span>
                </td>
                <td>${accountType.description}</td>
                <td>
                    <div class="actions">
                        <g:link action="edit" id="${accountType.id}"  class="btn btn-xs btn-warning"  >
                            <i class="fa fa-edit"></i> Edit
                        </g:link>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5">
                No Contents to show for Account Types
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
