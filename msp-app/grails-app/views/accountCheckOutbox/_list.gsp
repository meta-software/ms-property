<div class="search-page search-content-4 margin-top-10">
    <loading :active.sync="loader.isLoading"
             :can-cancel="false"
             :is-full-page="loader.fullPage" ></loading>
    <div class="search-bar bordered">
        <form role="form" autocomplete="off" @submit.prevent="search">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" class="form-control" placeholder="Query Request..."  v-model="params.q">
                </div>
                <div class="col-lg-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                    <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date Created</th>
                <th>Status</th>
                <th>Type</th>
                <th>Entity</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${accountCheckList}">
                <g:each in="${accountCheckList}" var="accountCheck">
                    <tr>
                        <td><g:link class="display-block" controller="account-check-outbox" action="show" id="${accountCheck.id}" >
                            ${accountCheck.id}
                        </g:link>
                        </td>
                        <td><g:formatDate date="${accountCheck.makeDate}" /></td>
                        <td><mp:makerCheck value="${accountCheck.checkStatus}">${accountCheck.checkStatus}</mp:makerCheck></td>
                        <td>${accountCheck.accountCategory}</td>
                        <td>
                            <g:link class="display-block" action="show" id="${accountCheck.id}" >${accountCheck.entity}</g:link>
                        </td>
                        <td>${accountCheck.actionName}</td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="7">
                        <mp:noRecordsFound />
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
</div>