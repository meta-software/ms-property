<!-- BEGIN Portlet PORTLET-->
<div class="portlet box grey-mint m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Manager Name</label>
                            <input type="text" class="form-control" name="name" value="${params?.name}">
                        </div>
                    </div>

                </div>

            </div>

            <div class="actions">
                <button type="submit" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->