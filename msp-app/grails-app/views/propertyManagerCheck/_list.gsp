<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Date Created</th>
        <th>Status</th>
        <th>User</th>
        <th>Entity</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${propertyManagerCheckList}">
        <g:each in="${propertyManagerCheckList}" var="propertyManagerCheck">
            <tr>
                <td><g:link class="display-block" action="show" id="${propertyManagerCheck.id}" >
                    ${propertyManagerCheck.id}
                </g:link>
                </td>
                <td><g:formatDate date="${propertyManagerCheck.makeDate}" /></td>
                <td>${propertyManagerCheck.checkStatus}</td>
                <td>${propertyManagerCheck.maker.username}</td>
                <td><g:link class="display-block" action="show" id="${propertyManagerCheck.id}" >
                    ${propertyManagerCheck.entity}
                </g:link></td>
                <td>${propertyManagerCheck.actionName}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
