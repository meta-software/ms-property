<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyManagerCheck.label', default: 'Viewing : Property Manager')}"/>
    <g:set var="pageHeadTitle" value="Viewing Task : Property Manager - ${propertyManagerCheck.entity.id}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let checkId = ${propertyManagerCheck.id};
    </script>

</head>

<body>

<!-- BEGIN PAGE BASE CONTENT -->
<div id="app">
    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> View Pending Task : Property Manager
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="propertyManagerCheck" />
                </div>

            </div>
        </div>
    </div>

    <maker-checker :item-id="checkId" :status="'${propertyManagerCheck.checkStatus}'"></maker-checker>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box grey-mint">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Viewing Task : Property Manager
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="propertyManager" />
                </div>

            </div>
        </div>
    </div>

</div>
<!-- END PAGE BASE CONTENT -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/propertyManagerCheck/property-manager-check-show.js" asset-defer="true" />

</body>
</html>