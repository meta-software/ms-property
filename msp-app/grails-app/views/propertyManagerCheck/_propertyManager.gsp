<table class="table">
    <tbody>
    <tr><th style="width: 40%">Name: </th><td>${propertyManager.fullName}</td></tr>
    <tr><th>Appointment Date: </th><td><g:formatDate date="${propertyManager.appointmentDate}" format="MMM yyyy" /></td></tr>
    <tr><th>Property Type: </th><td>${propertyManager.emailAddress}</td></tr>
    <tr><th>Phone Number: </th><td>${propertyManager.phoneNumber}</td></tr>
    <tr><th>Active: </th><td><span class="label label-${mp.boolStatusClass(status: propertyManager.active)} label-sm">${propertyManager.active ? 'Yes' : 'No'}</span>
    </td></tr>
    </tbody>
</table>
