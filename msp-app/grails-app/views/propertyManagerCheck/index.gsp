<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyManagerCheck.label', default: 'Property Managers : Pending Tasks')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'propertyManagerCheck.list.label', default: 'Property Managers : Pending Tasks', args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="propertyManager.list.label" args="[entityName]" /></title>
</head>

<body>

<div id="app">
    <g:render template="page_bar" />

    <div class="row">
        <div class="col-md-12">
            <g:render template="search_form"/>
        </div>
    </div>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message" />
    </g:if>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <g:render template="list" />
                    </div>
                </div>
            </div>

            <div class="pagination">
                <mp:pagination total="${propertyManagerCheckCount ?: 0}" />
            </div>

        </div>
    </div>

    </body>
</html>