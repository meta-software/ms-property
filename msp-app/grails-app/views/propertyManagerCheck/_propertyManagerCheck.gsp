<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                ID:
            </label>
            <div class="col-md-8">${propertyManagerCheck.id}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Status:
            </label>
            <div class="col-md-8">${propertyManagerCheck.checkStatus}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Action:
            </label>
            <div class="col-md-8">${propertyManagerCheck.actionName}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Entity:
            </label>
            <div class="col-md-8">${propertyManagerCheck.entity}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Date Created:
            </label>
            <div class="col-md-8"><g:formatDate date="${propertyManagerCheck.makeDate}" format="dd MMM, yyyy" /></div>
        </div>
    </div>
</div>

