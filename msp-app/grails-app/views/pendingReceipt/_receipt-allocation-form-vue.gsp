<!-- BEGIN PORTLET-->
<div id="trans-details" class="portlet box blue-hoki">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-dark hide"></i>
            <span class="caption-subject uppercase">Allocation Form</span>
        </div>

        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Debit Account</label>
                    <select2 ref="debitAccount"
                             :select2="debitAccountOptions"
                             @input="debitAccountSelected"
                             class="form-control"
                             name="debitAccount"
                             v-validate="'required'"
                             v-model="selectedDebitAccount">
                        <option value="">Select One...</option>
                    </select2>
                    <span class="has-error help-block">{{ errors.first('debitAccount') }}</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Amount</label>

                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text"
                               name="allocationAmount"
                               placeholder="0.00"
                               v-model.number="receiptRequest.allocationAmount"
                               v-validate="'required|decimal:2|min_value:0.01'"
                               class="form-control "/>
                    </div>
                    <span class="has-error help-block">{{ errors.first('receiptAmount') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-actions">
                    <button type="button"
                            id="add-transaction-item"
                            class="btn blue">
                        <i class="fa fa-check-square"></i> Capture Receipt
                    </button>
                    <button click="resetForm"
                            type="button"
                            id="cancel-mainTransactionTmp-item"
                            class="btn red-mint">
                        <i class="fa fa-refresh"></i> Clear
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PORTLET-->

<!-- the receipt allocation modal part -->
<modal v-model="openAllocations" :append-to-body="true" title="Receipt Allocations" size="lg" ref="modal" id="allocation-modal" :dismiss-btn="false" :keyboard="false" :backdrop="false">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-dark hide"></i>
            <span class="caption-subject uppercase">Allocation Form</span>
        </div>
    </div>

    <div class="portlet light bordered bg-inverse">
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <label>Txn Count:</label>
                    <span class="form-control-static">{{receiptAllocations.length}}</span>
                </div>
                <div class="col-md-3 col-xs-6">
                    <label>Allocated Total:</label>
                    <span class="form-control-static">$ {{allocatedTotal | formatNumber(2)}}</span>
                </div>
                <div class="col-md-3 col-xs-6">
                    <label>Receipt Amount:</label>
                    <span class="form-control-static">$ {{receiptRequest.receiptAmount | formatNumber(2)}}</span>
                </div>
                <div class="col-md-3 col-xs-6">
                    <label>Txn Ref:</label>
                    <span class="form-control-static "><span class="label label-sm label-default">{{receiptRequest.transactionReference}}</span></span>
                </div>
            </div>
        </div>
    </div>

    <div slot="footer">
        <btn type="button" class="grey" click="cancelAllocations"><i class="fa fa-times"></i> Cancel</btn>
        <btn type="success" :disabled="isAllocated == false" click="addAllocation"><i class="fa fa-save"></i> Add Allocation</btn>
    </div>
</modal>
<!-- the receipt allocation modal part -->