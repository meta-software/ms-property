<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'pendingReceipt.label', default: 'Pending Receipt')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'pendingReceipt.list.label', default: entityName, args: [pendingReceipts])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="pending-receipt-filter">
    <form role="form" autocomplete="off" @submit.prevent="search">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <input type="text" class="form-control" name="ref" placeholder="Receipt Reference..." v-model="params.ref" @keyup.enter="search">
            </div>
            <div class="col-lg-3 col-md-3">
                <input type="text" class="form-control" name="account" placeholder="Account Name..." v-model="params.account" @keyup.enter="search">
            </div>
            <div class="col-lg-2 col-md-2 extra-buttons">
                <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
            </div>
        </div>
    </form>
</template>

<template id="pending-receipt-list">
    <div ref="pendingReceiptListComp">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <page-bar></page-bar>

        <div class="search-page search-content-4 margin-top-10">
            <div class="search-bar bordered">
                <pending-receipt-filter @search="_onSearch($event)" @reset="reset"></pending-receipt-filter>
            </div>
            <div class="search-table table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead class="bg-grey-cararra">
                    <tr>
                        <th style="width: 23%;">Tenant</th>
                        <th style="width: 12%;" class="text-nowrap">Txn Reference</th>
                        <th style="width: 10%;" class="text-nowrap">Txn Date</th>
                        <th>Debit Account</th>
                        <th style="width: 15%;" class="text-nowrap">Amount</th>
                        <th style="width: 10%;" class="text-nowrap">Date Created</th>
                        <th style="width: 5%;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="pendingReceipt in pendingReceipts">
                        <td>{{pendingReceipt.tenant.accountName}}</td>
                        <td>{{pendingReceipt.transactionReference}}</td>
                        <td class="text-nowrap">{{pendingReceipt.transactionDate | formatDate('DD, MMM YYYY')}}</td>
                        <td>{{pendingReceipt.debitAccount.accountNumber}}</td>
                        <td>{{pendingReceipt.transactionCurrency.id}}&nbsp;{{pendingReceipt.receiptAmount | formatNumber(2) }}</td>
                        <td class="text-nowrap">{{pendingReceipt.dateCreated | formatDate('DD, MMM YYYY')}}</td>
                        <td>
                            <div class="actions">
                                <button @click="showPendingReceipt(pendingReceipt.id)"
                                        type="button"
                                        class="btn btn-xs btn-success">
                                    <i class="fa fa-trash"></i> Allocate
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr v-show="pendingReceipts.length == 0">
                        <td colspan="7"><mp:noRecordsFound /></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</template>

<template id="pending-receipt-app">
    <div>
        <router-view></router-view>
    </div>
</template>

<template id="pending-receipt-show">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="page-bar">
            <div class="page-bar-header">
                <h4><button @click="$router.go(-1)" class="btn btn-sm default" type="button" ><i class="fa fa-arrow-left" style="cursor: pointer" title="Back"></i> Back</button> Allocate Receipt - ref://{{pendingReceipt.transactionReference}}</h4>
            </div>

            <div class="page-toolbar">
                <button type="button" class="btn btn-sm default" @click="discardReceiptBatch(id)">
                    <i class="fa fa-times"></i> Cancel
                </button>
                <button :disabled="false == isAmountFullyAllocated" type="button" class="btn btn-sm green" @click="postReceiptAllocations">
                    <i class="fa fa-check"></i> Post Receipt Allocations
                </button>
            </div>
        </div>

        <div class="row margin-top-10">
            <div class="col-md-12">
                <receipt-header v-if="pendingReceipt.id" :pending-receipt="pendingReceipt" ></receipt-header>

                <receipt-allocation-form v-if="pendingReceipt.id" @allocation-added="allocationItemAdded" :pending-receipt="pendingReceipt"></receipt-allocation-form>

                <receipt-allocation-list @clear-allocations="clearAllocations" ref="receiptAllocationList" v-if="pendingReceipt.id" @item-deleted="receiptItemDeleted" ref="itemsList" :receipt-id="pendingReceipt.id" :pending-receipt="pendingReceipt" ></receipt-allocation-list>
            </div>
        </div>
    </section>
</template>

<template id="receipt-header">

    <div class="portlet light bordered bg-inverse">
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <label class="control-label">Tenant:</label> <span class="form-control-static">{{pendingReceipt.tenant.accountNumber}}:{{pendingReceipt.tenant.accountName}}</span>
                </div>

                <div class="col-md-4 col-xs-6">
                    <label>Receipt Amount:</label> <span class="form-control-static">{{pendingReceipt.transactionCurrency.id}} {{pendingReceipt.receiptAmount | formatNumber(2)}}</span>
                </div>

                <div class="col-md-4 col-xs-6">
                    <label>Allocated Amount:</label> <span class="form-control-static">{{pendingReceipt.transactionCurrency.id}} {{pendingReceipt.amountAllocated | formatNumber(2)}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <label>Txn Date:</label> <span class="form-control-static">{{pendingReceipt.transactionDate | formatDate('YYYY-MMM-DD')}}</span>
                </div>
                <div class="col-md-4 col-xs-6">
                    <label>Txn Ref:</label> <span class="form-control-static "><span class="label label-sm bold label-default">{{pendingReceipt.transactionReference}}</span></span>
                </div>
                <div class="col-md-4 col-xs-6">
                    <label>Narration:</label> <span class="form-control-static">{{pendingReceipt.narration}}</span>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="receipt-allocation-list">
    <section>
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject">Receipt Allocations</span>
            </div>

            <div class="actions">
                <button :disabled="receiptAllocations.length == 0" type="button" class="btn btn-sm default" @click="clearAllocations">
                    <i class="fa fa-trash"></i> Clear Allocations
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-bordered table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th style="width: 20%;">Lease</th>
                    <th style="width: 25%;">Rental Unit</th>
                    <th>Sub Account</th>
                    <th style="width: 10%;">Amount</th>
                    <th><i class="fa fa-trash font-dark"></i></th>
                </tr>
                </thead>
                <tbody class="trans-list-tbody">
                <tr v-for="(receiptAllocation, idx) in receiptAllocations" :key="idx">
                    <td>{{receiptAllocation.lease.leaseNumber}}</td>
                    <td>{{receiptAllocation.rentalUnit.name}}</td>
                    <td>{{receiptAllocation.subAccount.accountNumber }} : {{receiptAllocation.subAccount.accountName }}</td>
                    <td>{{receiptAllocation.allocatedAmount | formatNumber(2)}}</td>
                    <td>
                        <button type="button" @click="deleteAllocation($event, receiptAllocation)" class="btn btn-xs">
                            <i class="fa fa-trash font-red-mint"></i>
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    </section>

</template>

<template id="receipt-allocation-form">
    <!-- the receipt allocation form part -->
    <form autocomplete="off" role="form" @submit.prevent="postAllocation">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
    <div class="portlet light bordered">
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Lease Unit</label>
                            <select  :disabled="isAmountFullyAllocated"  v-validate="'required'" v-model="selectedLease" class="form-control " title="Lease Unit..." name="lease" >
                                <option selected="selected" value="">Select Lease...</option>
                                <option v-for="option in leaseOptions.data" :value="option.id">
                                    {{ option.text }}
                                </option>
                            </select>
                            <span class="has-error help-block">{{ errors.first('lease') }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sub Account</label>
                            <select :disabled="isAmountFullyAllocated"  v-validate="'required'"  name="subAccount" class="form-control selectpicker" title="Sub Account..." v-model="selectedSubAccount">
                                <option selected="selected" value="">Sub Account...</option>
                                <option v-for="option in subAccountOptions.data" :value="option.id">
                                    {{ option.text }}
                                </option>
                            </select>
                            <span class="has-error help-block">{{ errors.first('subAccount') }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Amount</label>
                            <input  :disabled="isAmountFullyAllocated"  type="text" placeholder="0.0" class="form-control " name="amount" v-validate="'required||min_value:0.01|decimal:2'" v-model.number="receiptAllocation.allocatedAmount" />
                            <span class="has-error help-block">{{ errors.first('amount') }}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="actions">
                            <button type="button" class="btn btn-sm btn-success" @click="postAllocation" :disabled="isAmountFullyAllocated"><i class="fa fa-check"></i> Save Allocation</button>
                            <button :disabled="isAmountFullyAllocated"  type="button" class="btn btn-sm btn-danger" @click="cancelAllocation"><i class="fa fa-times"></i> Cancel </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- the receipt allocation form part -->
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Unallocated Receipts
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">

                <router-link class="btn btn-sm blue-hoki" to="/"> <i class="fa fa-list"></i> Unallocated Receipts</router-link>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/pendingReceipt/pending-receipt-app.js" asset-defer="true"/>

</body>
</html>
