<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'settings.label', default: 'Settings')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'settings.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/configSetting/config-setting-app.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="config-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" v-model="params.query" class="form-control" placeholder="Configuration...." >
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <g:select from="${metasoft.property.core.ConfigType.list()}"
                                  optionKey="id"
                                  name="configType"
                                  v-model="params.configType"
                                  noSelection="['null': 'Config Type...']"
                                  class="form-control"/>
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="submit" @click.prevent="search">Search</button>
                        <button class="btn green" type="button"><i class="fa fa-refresh" @click="reset"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>Config Type</th>
                    <th>Value</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Current Active ?</th>
                    <th>Enabled ?</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(configSetting, key) in configSettings" :key="key" :class="{'active-cycle': configSetting.active}">
                    <td>{{configSetting.configType.name}}</td>
                    <td>{{configSetting.value}}</td>
                    <td>{{configSetting.startDate | formatDate}}</td>
                    <td>{{configSetting.endDate | formatDate}}</td>
                    <td><span class="label label" :class="[configSetting.currentActive ? 'label-success' : 'label-danger']" >{{configSetting.currentActive | formatBoolean}}</span></td>
                    <td><span class="label label" :class="[configSetting.enabled ? 'label-success' : 'label-danger']" >{{configSetting.enabled | formatBoolean}}</span></td>
                    <td><router-link :to="{path: '/show/'+configSetting.id}" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> View</router-link></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-cog"></i> Configurations
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <button type="button" @click="create" class="btn btn-sm blue-hoki" > <i class="fa fa-plus"></i> New Configuration</button>
            <router-link class="btn btn-sm default" :to="{path: '/'}"> <i class="fa fa-list"></i> List Configurations</router-link>
        </div>
    </div>
</template>

<template id="conf-create">
    <div class="portlet light bordered margin-top-10">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-cogs"></i> Configuration Form</div>
        </div>

        <div class="portlet-body form">
            <form role="form" @submit.prevent="save">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Config Type:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <g:select from="${metasoft.property.core.ConfigType.list()}"
                                          optionKey="id"
                                          name="configType"
                                          v-model="configSetting.configTypeId"
                                          noSelection="['null': 'Config Type...']"
                                          class="form-control select2"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('configType') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Value:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input name="value" class="form-control" type="text" v-model="configSetting.value" v-validate="'required'">
                                <span class="has-error help-block-error help-block">{{ errors.first('value') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Start Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <vdate-picker v-validate="'required'" v-model="configSetting.startDate" name="startDate" class="form-control"></vdate-picker>
                                <span class="has-error help-block-error help-block">{{ errors.first('startDate') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">End Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <vdate-picker v-validate="'required'" v-model="configSetting.endDate" name="endDate" class="form-control"></vdate-picker>
                                <span class="has-error help-block-error help-block">{{ errors.first('endDate') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Enabled:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-9">
                                <input type="checkbox" v-model="configSetting.enabled" value="1" name="enabled"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="button" class="btn btn-sm blue-hoki" @click="save"><i class="fa fa-save"></i> Save Configuration</button>
                    <button type="button" class="btn btn-sm default" @click="cancel"><i class="fa fa-times"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
</template>

<!-- begin show -->
<template id="conf-show">
    <div class="row">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog"></i> Configuration - {{configSetting.configType.name}}
                    </div>

                    <div class="actions">
                        <button type="button" class="btn btn-danger btn-sm" @click="$router.go(-1)"><i class="fa fa-arrow-left"></i> Back</button>
                    </div>
                </div>

                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Config Type:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{configSetting.configType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Start Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{configSetting.startDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        End Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{configSetting.endDate | formatDate}}</div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Active?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[configSetting.currentActive ? 'label-success' : 'label-danger']" >{{configSetting.currentActive | formatBoolean}}</span></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Open?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[configSetting.enabled ? 'label-success' : 'label-danger']" >{{configSetting.enabled | formatBoolean}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="button" class="btn btn-sm green-dark" @click="activate"><i class="fa fa-play"></i> Activate Configuration</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</template>
<!-- end show-->

</body>
</html>