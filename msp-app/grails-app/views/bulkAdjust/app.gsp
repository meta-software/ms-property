
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'bulkAdjust.label', default: 'Bulk Adjust')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'bulkAdjust.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="bulkAdjust.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="app">
    <page-bar></page-bar>

    <div class="inbox margin-top-10">
        <div class="row">
            <div class="col-lg-2 col-md-4 col-xs-12">
                <div class="inbox-sidebar">
                    <router-link data-title="Create Bulk Adjust" class="btn green btn-block" :to="{path: '/create'}">
                        <i class="fa fa-plus"></i> Create Bulk Adjust
                    </router-link>
                    <ul class="inbox-nav">
                        <li>
                            <router-link data-title="Inbox" :to="{path: '/inbox'}">
                                Inbox
                            </router-link>
                        </li>
                        <li>
                            <router-link data-title="Inbox" :to="{path: '/approved'}">
                                Approved
                            </router-link>
                        </li>
                        <li class="actives">
                            <router-link data-title="Outbox" :to="{path: '/outbox'}">
                                Outbox
                            </router-link>
                        </li>
                        <!--li class="divider"></li>
                        <li>
                            <router-link data-title="Cancelled" class="sbold uppercase" :to="{path: '/trash'}">
                                Trash
                            </router-link>
                        </li-->
                    </ul>
                </div>
            </div>
            <div class="col-lg-10 col-md-8 col-xs-12">
                <router-view></router-view>
            </div>
        </div>
    </div>
</div>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4>Bulk Adjusts
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/create'}">
                    <i class="fa fa-plus"></i> Create Bulk Adjust
                </router-link>
            </div>
        </div>
    </div>
</template>

<template id="bulk-adjust-app">
    <div class="">
        <h4>Bulk Adjust App</h4>
    </div>
</template>

<template id="bulk-adjust-show">
    <div class="inbox-body">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="inbox-header border-bottom" style="border-bottom: 1px solid #eee;">
            <h3 class="pull-left">Bulk Adjust - REF://{{bulkAdjust.subAccount.accountName}}</h3>

            <div class="form-actions pull-right">
                <maker-checker :makerid="bulkAdjust.maker.id" :item-id="bulkAdjust.id" :status="bulkAdjust.checkStatus" :controller="'bulk-adjust'" ></maker-checker>
            </div>
        </div>

        <div class="inbox-content">
            <div class="portlet light bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-info"></i> Details</div>
                </div>
                <div class="portlet-body">
                    <div class="row">

                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Adjustment Type:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.adjustmentType.name}}</div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Status:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.checkStatus}}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Amount Type:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.amountType.name}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Value:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.amount | formatNumber(2)}}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Currency:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.transactionCurrency.id}}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Sub Account:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.subAccount.accountName}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Action Date:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.actionDate | formatDate('DD MMM YYYY')}}</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Property Type:
                                </label>
                                <div class="col-md-7">{{bulkAdjust.propertyType.name}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</template>

<template id="bulk-adjust-outbox-show">
    <div class="inbox-body">
        <div class="inbox-header border-bottom" style="border-bottom: 1px solid #eee;">
            <h3 class="pull-left">Bulk Adjust - REF://{{bulkAdjust.transactionReference}}</h3>

            <div class="form-actions pull-right">
                <maker-options :item-id="bulkAdjust.id" :status="bulkAdjust.checkStatus" :controller="'bulk-adjust'" ></maker-options>
            </div>
        </div>

        <div class="inbox-content">
            <div class="portlet light bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-info"></i> Details</div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Reference:
                                </label>
                                <div class="col-md-8">{{bulkAdjust.transactionReference}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Status:
                                </label>
                                <div class="col-md-8">{{bulkAdjust.checkStatus}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Txn Date:
                                </label>
                                <div class="col-md-8">{{bulkAdjust.transactionDate | formatDate}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Property:
                                </label>
                                <div class="col-md-8">{{bulkAdjust.property.name}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Value:
                                </label>
                                <div class="col-md-8">$ {{bulkAdjust.amount | formatNumber}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4">
                                    Billing Cycle:
                                </label>
                                <div class="col-md-8">{{bulkAdjust.billingCycle.name}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">Adjust Items</div>

                    <div class="actions">
                        <button type="button" class="btn blue btn-outline btn-xs"><i class="fa fa-refresh"></i> </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <bulk-adjust-items :bulk-adjust-items="bulkAdjust.bulkAdjustmentItems"></bulk-adjust-items>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="bulk-adjust-create">
    <section>
        <form class="horizontal-form metasoft-form">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><a @click="$router.push('/')"><i class="fa fa-arrow-left" style="font-size: 1.10em;"></i>
                    </a> Create Bulk Adjust</div>

                    <div class="actions">
                        <button @click="onCancelBulkAdjust" type="button" class="btn default btn-sm">
                            <i class="fa fa-times"></i> Cancel
                        </button>

                        <button type="button" class="btn btn-sm btn-success" @click="onSaveBulkAdjust">
                            <i class="fa fa-check"></i> Save Adjust
                        </button>
                    </div>

                </div>

                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Adjustment Type
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkAdjust.adjustmentType.id" name="adjustmentType"
                                         :select2="adjustmentTypeOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label uppercase">Sub Account
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkAdjust.subAccount.id" name="subAccount" :select2="subAccountOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Amount Type
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkAdjust.amountType.id" name="amountType"
                                         :select2="amountTypeOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Currency
                                    <span class="required">*</span>
                                </label>
                                <currency-select ref="transactionCurrency" v-model="bulkAdjust.transactionCurrency" class="form-control"
                                                name="transactionCurrency"></currency-select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Value
                                    <span class="required">*</span>
                                </label>
                                <input type="text" ref="amount" v-model="bulkAdjust.amount" class="form-control"
                                       name="amount"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Action Date:
                                    <span class="required">*</span>
                                </label>
                                <datepicker placeholder="YYYY-MM-DD" v-validate="'required'" v-model="bulkAdjust.actionDate" class="form-control"></datepicker>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Property Type
                                    <span class="required">*</span>
                                </label>
                                <select2 v-model="bulkAdjust.propertyType.id" name="propertyType"
                                         :select2="propertyTypeOptions">
                                    <option disabled value="0">Select one</option>
                                </select2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2">Reason
                                    <span class="required">*</span>
                                </label>
                                <textarea class="form-control" rows="4" type="text" v-model="bulkAdjust.reason" name="reason" ></textarea>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </form>

    </section>
</template>

<template id="bulk-adjust-items">
    <table class="table table-striped table-hover table-bordered">
        <thead>
        <tr>
            <th>Tenant</th>
            <th>Lease Number</th>
            <th>Rental Unit</th>
            <th>Area</th>
            <th>Amount</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="bulkAdjustItem in bulkAdjustmentItems">
            <td>{{bulkAdjustItem.lease.tenant.accountName}}</td>
            <td>{{bulkAdjustItem.lease.leaseNumber}}</td>
            <td>{{bulkAdjustItem.rentalUnit.name}}</td>
            <td>{{bulkAdjustItem.rentalUnit.area}}</td>
            <td>{{bulkAdjustItem.amount | formatNumber}}</td>
            <td><span>...</span></td>
        </tr>
        <tr v-if="bulkAdjustmentItems.length == 0">
            <td colspan="6">No Records found...</td>
        </tr>
        </tbody>
    </table>
</template>

<template id="bulk-adjust-inbox">
    <div class="inbox-body">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="inbox-header">
            <h3 class="pull-left">Bulk Adjusts : Inbox</h3>
        </div>

        <div class="inbox-content">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="all-items" class="icheckbox_flat"/></th>
                    <th>Sub Account</th>
                    <th>Adjust Type</th>
                    <th>Amount Type</th>
                    <th>Processed ?</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Action Date</th>
                    <th>Created By</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(bulkAdjust, index) in bulkAdjusts" :key="index" @click="$router.push({name: 'inboxItem', params: {id: bulkAdjust.id}})">
                    <td><input type="checkbox" class="icheck" /></td>
                    <td>{{bulkAdjust.subAccount.accountName}}</td>
                    <td>{{bulkAdjust.adjustmentType.name}}</td>
                    <td>{{bulkAdjust.amountType.name}}</td>
                    <td>{{bulkAdjust.processed}}</td>
                    <td>{{bulkAdjust.transactionCurrency.id}}</td>
                    <td>{{bulkAdjust.amount | formatNumber }}</td>
                    <td>{{bulkAdjust.actionDate | formatDate('YYYY-MMM-DD')}}</td>
                    <td>{{bulkAdjust.maker.username}}</td>
                </tr>
                <tr v-if="bulkAdjusts.length == 0">
                    <td colspan="8"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="bulk-adjust-approved">
    <div class="inbox-body">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="inbox-header">
            <h3 class="pull-left">Bulk Adjusts : Approved</h3>
        </div>

        <div class="inbox-content">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="all-approved-items" class="icheckbox_flat"/></th>
                    <th>Sub Account</th>
                    <th>Adjust Type</th>
                    <th>Amount Type</th>
                    <th>Processed ?</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Action Date</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(bulkAdjust, index) in bulkAdjusts" :key="index" @click="$router.push({name: 'approvedItem', params: {id: bulkAdjust.id}})">
                    <td><input type="checkbox" class="icheck" /></td>
                    <td>{{bulkAdjust.subAccount.accountName}}</td>
                    <td>{{bulkAdjust.adjustmentType.name}}</td>
                    <td>{{bulkAdjust.amountType.name}}</td>
                    <td>{{bulkAdjust.processed}}</td>
                    <td>{{bulkAdjust.transactionCurrency.id}}</td>
                    <td>{{bulkAdjust.amount | formatNumber }}</td>
                    <td>{{bulkAdjust.actionDate | formatDate('YYYY-MMM-DD')}}</td>
                </tr>
                <tr v-if="bulkAdjusts.length == 0">
                    <td colspan="7"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="bulk-adjust-outbox">
    <div class="inbox-body">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="inbox-header">
            <h3 class="pull-left">Bulk Adjusts : Outbox</h3>
        </div>

        <div class="inbox-content">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    <th><input type="checkbox" id="all-outbox-items" class="icheckbox_flat"/></th>
                    <th>Sub Account</th>
                    <th>Adjust Type</th>
                    <th>Amount Type</th>
                    <th>Processed ?</th>
                    <th>Currency</th>
                    <th>Amount</th>
                    <th>Action Date</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(bulkAdjust, index) in bulkAdjusts" :key="index" @click="$router.push({name: 'approvedItem', params: {id: bulkAdjust.id}})">
                    <td><input type="checkbox" class="icheck" /></td>
                    <td>{{bulkAdjust.subAccount.accountName}}</td>
                    <td>{{bulkAdjust.adjustmentType.name}}</td>
                    <td>{{bulkAdjust.amountType.name}}</td>
                    <td>{{bulkAdjust.processed}}</td>
                    <td>{{bulkAdjust.transactionCurrency.id}}</td>
                    <td>{{bulkAdjust.amount | formatNumber }}</td>
                    <td>{{bulkAdjust.actionDate | formatDate('YYYY-MMM-DD')}}</td>
                </tr>
                <tr v-if="bulkAdjusts.length == 0">
                    <td colspan="7"><mp:noRecordsFound/></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/bulkAdjust/bulk-adjust-app.js" asset-defer="true"/>

</body>
</html>