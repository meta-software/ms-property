<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'leaseRenewalRequest.label', default: 'Lease Renewal Request')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'leaseRenewalRequest.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/plugins/vue/bootstrap-table/bootstrap-table-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/leaseRenewalRequest/lease-renewal-request-app.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <div class="inbox margin-top-10">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-xs-12">
                    <div class="inbox-sidebar">
                        <router-link data-title="Create Request" class="btn green btn-block" :to="{name: 'create'}">
                            <i class="fa fa-plus"></i> Create Request
                        </router-link>
                        <ul class="inbox-nav">
                            <li>
                                <router-link data-title="Approved" to="/">
                                    Approved
                                </router-link>
                            </li>
                            <li class="actives">
                                <router-link data-title="Outbox" to="/outbox">
                                    Outbox
                                </router-link>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-10 col-md-8 col-xs-12">
                    <router-view></router-view>
                </div>
            </div>
        </div>

    </div>
</template>

<template id="lease-renewal-request-outbox">
    <div class="inbox margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
    <div class="row">
        <div class="col-md-12" ref="outboxList">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Outbox : Lease Renewal Requests</h3>
                </div>
                <div class="inbox-content">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Action</th>
                                <th>Type</th>
                                <th>Cycle</th>
                                <th>Run Date</th>
                                <th>Value Date</th>
                                <th>Date Created</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(leaseRenewalRequestCheck, key) in leaseRenewalRequestChecks" key="key">
                                <td>
                                    <router-link class="display-block" :to="{name: 'outboxShow', params: {id: leaseRenewalRequestCheck.id}}">{{leaseRenewalRequestCheck.id}}</router-link>
                                </td>
                                <td>{{leaseRenewalRequestCheck.actionName}}</td>
                                <td>{{leaseRenewalRequestCheck.entity.leaseRenewalType.name}}</td>
                                <td>{{leaseRenewalRequestCheck.entity.billingCycle.name}}</td>
                                <td>{{leaseRenewalRequestCheck.entity.runDate | formatDate }}</td>
                                <td>{{leaseRenewalRequestCheck.entity.valueDate | formatDate }}</td>
                                <td>{{leaseRenewalRequestCheck.entity.dateCreated | formatDate('YYYY-MM-DD HH:mm') }}</td>
                            </tr>
                            <tr v-if="leaseRenewalRequestChecks.length == 0">
                                <td colspan="7">No Records found...</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="lease-renewal-request-list">

    <div class="inbox">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div id="toolbar-list">
            <h3> LeaseRenewal Requests</h3>
        </div>
        <div class="row">
        <div class="col-md-12">

            <div class="inbox-body">
                <div class="inbox-content">
                    <bootstrap-table :columns="table.columns" :data="table.data" :options="table.options"></bootstrap-table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="lease-renewal-outbox-show">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki bg-inverse">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : LeaseRenewal Request
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        ID:
                                    </label>
                                    <div class="col-md-7">{{leaseRenewalRequestCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Status:
                                    </label>
                                    <div class="col-md-7">{{leaseRenewalRequestCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Action:
                                    </label>
                                    <div class="col-md-7">{{leaseRenewalRequestCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Entity:
                                    </label>
                                    <div class="col-md-7">{{leaseRenewalRequestCheck.entity.leaseRenewalType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Date Created:
                                    </label>
                                    <div class="col-md-7">{{leaseRenewalRequestCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Maker:
                                    </label>
                                    <div class="col-md-7">{{leaseRenewalRequestCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <maker-options :makerid="leaseRenewalRequestCheck.maker.id" :item-id="leaseRenewalRequestCheck.id" :controller="'lease-renewal-request-check-outbox'" :status="leaseRenewalRequestCheck.checkStatus"></maker-options>
        
        <div class="row margin-top-10">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Outbox LeaseRenewal Request : #{{leaseRenewalRequestCheck.id}}
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Id:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequestCheck.entity.id}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Type:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequestCheck.entity.leaseRenewalType.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Billing Cycle:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequestCheck.entity.billingCycle.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Status:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static"><span :class="statusClass(leaseRenewalRequestCheck.transactionSource.requestStatus)" class="request-status label label-sm">{{leaseRenewalRequestCheck.transactionSource.requestStatus}}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Date:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequestCheck.entity.runDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Value Date:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequestCheck.entity.valueDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </section>
</template>

<template id="lease-renewal-request-show">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet  light bordered bg-inverse">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> LeaseRenewal Request Info
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Id:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequest.id}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Type:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequest.leaseRenewalType.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Billing Cycle:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequest.billingCycle.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Status:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static"><span :class="statusClass(leaseRenewalRequest.requestStatus)" class="request-status label label-sm">{{leaseRenewalRequest.requestStatus}}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequest.runDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Value Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{leaseRenewalRequest.valueDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!--LeaseRenewal Request transactions.-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Lease Renewal Request Transactions
                        </div>
                    </div>

                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <lease-renewal-transactions :lease-renewal-request-id="id"></lease-renewal-transactions>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="lease-renewal-transactions">
    <div class="table-scrollable-borderless">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div id="toolbar">
            <h3> Transactions</h3>
        </div>
        <bootstrap-table :columns="table.columns" :data="table.data" :options="table.options"></bootstrap-table>
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> <i class="fa fa-cogs"></i> Lease Renewal Requests
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Requests</router-link>
            </div>
        </div>
    </div>
</template>

<template id="lease-renewal-request-create">
    <section>
        <form role="form">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><a @click="$router.go(-1)"><i class="fa fa-arrow-left" style="font-size: 1.10em;"> </i>
                    </a> Create Request</div>

                    <div class="actions">
                        <button @click="onCancelCreate" type="button" class="btn default btn-sm">
                            <i class="fa fa-times"></i> Cancel
                        </button>

                        <button type="button" class="btn btn-sm btn-success" @click="onSaveRequest">
                            <i class="fa fa-check"></i> Save Request
                        </button>
                    </div>

                </div>

                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Lease Renewal Type
                                    <span class="required">*</span>
                                </label>

                                <div>
                                    <select2 v-model="leaseRenewalRequest.leaseRenewalType.id" name="leaseRenewalType"
                                             :select2="leaseRenewalTypeOptions">
                                        <option disabled value="0">Select one</option>
                                    </select2>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Billing Cycle
                                    <span class="required">*</span>
                                </label>

                                <div>
                                    <select2 v-model="leaseRenewalRequest.billingCycle.id" name="billingCycle"
                                             :select2="billingCycleOptions">
                                        <option disabled value="0">Select one</option>
                                    </select2>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                            <dropdown class="form-group">
                                <label class="control-label">Run Date: <span class="required">*</span>
                                </label>

                                <div class="input-group">
                                    <input class="form-control" type="text" v-model="leaseRenewalRequest.runDate" />
                                    <div class="input-group-btn">
                                        <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                                    </div>
                                </div>
                                <template slot="dropdown">
                                    <li>
                                        <date-picker v-model="leaseRenewalRequest.runDate"/>
                                    </li>
                                </template>
                            </dropdown>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                            <dropdown class="form-group">
                                <label class="control-label">Value Date: <span class="required">*</span> </label>

                                <div class="input-group">
                                    <input class="form-control" type="text" v-model="leaseRenewalRequest.valueDate" />
                                    <div class="input-group-btn">
                                        <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                                    </div>
                                </div>
                                <template slot="dropdown">
                                    <li>
                                        <date-picker v-model="leaseRenewalRequest.valueDate"/>
                                    </li>
                                </template>
                            </dropdown>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </section>
</template>

</body>
</html>