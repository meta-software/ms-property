<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Request #</th>
        <th>Action Reason</th>
        <th>Lease No</th>
        <th>Tenant</th>
        <th>Status</th>
        <th>Action Date</th>
        <th>Requested By</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${leaseRenewalRequestList}">
        <g:each in="${leaseRenewalRequestList}" var="leaseRenewalRequest">
            <tr>
                <td>
                <g:link style="display: block" action="show" id="${leaseRenewalRequest.id}" >
                    ${leaseRenewalRequest.id}
                </g:link>
                </td>
                <td style="width: 20%;">${leaseRenewalRequest.actionReason}</td>
                <td>
                    <g:link style="display: block" controller="lease" action="show" id="${leaseRenewalRequest.lease.id}" >
                        ${leaseRenewalRequest.lease.leaseNumber}
                    </g:link>
                </td>
                <td>${leaseRenewalRequest.lease.tenant}</td>
                <td><mp:stateStatusLabel cssClass="mc-status" status="${leaseRenewalRequest.makerChecker.status}" text="${leaseRenewalRequest.makerChecker.status}" /></td>
                <td>${leaseRenewalRequest.actionDate.format('dd MMM, yyyy')}</td>
                <td>${leaseRenewalRequest.makerChecker?.maker?.shortName}</td>
                <td>
                    <div class="actions">
                        <button type="button" disabled="disabled" class="btn btn-xs btn-danger"  >
                            <i class="fa fa-trash"></i>
                        </button>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="6">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
