<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'leaseRenewalRequest.label', default: 'Lease')}"/>
    <g:set var="pageHeadTitle" value="Lease Renewal Request : ${leaseRenewalRequest.lease.leaseNumber}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message value="View Renewal Request - ${leaseRenewalRequest.lease.leaseNumber}" args="[entityName]"/></title>

    <script type="text/javascript">
        var leaseRenewalRequestId = ${leaseRenewalRequest.id};
    </script>

</head>

<body>

<div id="main-app" v-cloak>

    <g:render template="page_bar_show"/>

    <div class="row">
        <div class="col-md-8">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box green-haze margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-list"></i> Lease Renewal Request</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body " style="padding: 0px 10px;">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 15%">Lease Number</th><td style="width: 35%">${leaseRenewalRequest.lease.leaseNumber}</td>
                                <th style="width: 15%">Lease Status</th><td style="width: 35%">${lease.leaseNumber}</td>
                            </tr>
                            <tr>
                                <th>Action Reason</th><td colspan="3"><p>${leaseRenewalRequest.actionReason}</p></td>
                            </tr>
                            <tr>
                                <th>Action Date</th><td>${leaseRenewalRequest.actionDate.format('dd MMM, yyyy')}</td>
                                <th>Request Status</th><td><mp:stateStatusLabel status="${leaseRenewalRequest.makerChecker.status}" cssClass="mc-status" text="${leaseRenewalRequest.makerChecker.status}" /></td>
                            </tr>
                            <tr>
                                <th>Requested By</th><td>${leaseRenewalRequest.makerChecker?.maker?.fullName}</td>
                                <th>Processed ?</th><td><mp:boolStatusLabel status="${leaseRenewalRequest.processed}" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->

            <maker-checker :makerid="${leaseRenewalRequest.maker.id}" :controller="'lease-renewal-request'" :item-id="leaseRenewalRequest.id" :status="'${leaseRenewalRequest.makerChecker.status}'"></maker-checker>

            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box blue-hoki margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-list"></i> Request Response</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body " style="padding: 0px 10px;">
                        <table class="table">
                            <tbody>
                            <tr>
                                <th style="width: 15%">Response Date</th><td style="width: 35%">${leaseRenewalRequest.checkDate?.format('dd MMM, yyyy') ?: '---'}</td>
                                <th style="width: 15%">Responded By</th><td style="width: 35%">${leaseRenewalRequest.checker?.fullName ?: '---'}</td>
                            </tr>
                            <tr>
                                <th>Response</th><td><p>${leaseRenewalRequest.makerChecker.comment ?: '---'}</p></td>
                                <th>Response Status</th><td><mp:stateStatusLabel status="${leaseRenewalRequest.makerChecker.status}" cssClass="mc-status" text="${leaseRenewalRequest.makerChecker.status}" /></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
        <div class="col-md-4">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box grey-gallery margin-top-10">
                <div class="portlet-title">
                    <div class="caption"> <i class="fa fa-sticky-note"></i> Lease Details</div>
                </div>

                <div class="portlet-body">
                    <div class="portlet-body" style="padding: 0px 10px;">
                        <table class="table">
                            <tbody>
                            <tr><th style="width: 40%">Lease Status</th><td>${lease.leaseNumber}</td></tr>
                            <tr><th>Tenant</th><td>${lease.tenant}</td></tr>
                            <tr><th>Property</th><td>${lease.property.name}</td></tr>
                            <tr><th>Rental Unit</th><td>${lease.rentalUnit}</td></tr>
                            <tr><th>Lease Status</th><td>${mp.stateStatusClass(status: lease.leaseStatus.code, text: lease.leaseStatus.name)}</td></tr>
                            <tr><th>Start Date</th><td><g:formatDate date="${lease.leaseStartDate}" /></td></tr>
                            <tr><th>Expiry Date</th><td><g:formatDate date="${lease.leaseExpiryDate}" /></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
</div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/leaseRenewalRequest/lease-renewal-request-show.js" asset-defer="true"/>

</body>
</html>
