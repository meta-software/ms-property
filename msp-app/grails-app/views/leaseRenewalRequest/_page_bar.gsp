<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4><i class="fa fa-refresh"></i> ${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="leaseRenewalRequest" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Requests
        </g:link>
        <button type="button" disabled="disabled" class="btn blue-hoki btn-sm">
            <i class="fa fa-plus"></i> Add Request
        </button>
    </div>
</div>