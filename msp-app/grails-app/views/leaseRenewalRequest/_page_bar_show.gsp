<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4><i class="fa fa-sticky-note"></i> ${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if> - <mp:makerChecker value="${leaseRenewalRequest.makerChecker}" />
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar pull-left" >
         <g:if test="${!leaseRenewalRequest.isChecked()}">
             <sec:ifAnyGranted roles="ROLE_PERM_LEASE_REQUEST_CHECK">
                 <g:if test="${leaseRenewalRequest.maker?.id != applicationContext.springSecurityService.currentUser.id}">
                     <maker-checker :item-controller="'lease-renewal-request'" :item-id="leaseRenewalRequest.id" :status="'${leaseRenewalRequest.makerChecker.status}'"></maker-checker>
                 </g:if>
             </sec:ifAnyGranted>
        </g:if>
    </div>

    <div class="page-toolbar" >
        <g:link controller="leaseRenewalRequest" type="button" class="btn btn-sm blue-hoki">
            <i class="fa fa-edit"></i> Requests
        </g:link>
    </div>

</div>