<div class="row">
    <div class="col-md-4">
        <div class="portlet box grey-mint">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info"></i> Viewing Task : Lease
                </div>
            </div>

            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4">
                                Number:
                            </label>
                            <div class="col-md-8">${lease.leaseNumber}</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4">
                                Lease Type:
                            </label>
                            <div class="col-md-8">${lease.leaseType}</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4">
                                Tenant:
                            </label>
                            <div class="col-md-8">${lease.tenant}</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4">
                                Rental Unit:
                            </label>
                            <div class="col-md-8">${lease.rentalUnit}</div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4">
                                Start Date:
                            </label>
                            <div class="col-md-8"><g:formatDate date="${lease.leaseStartDate}" format="dd MMM, yyyy" /></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-4">
                                Expiry Date:
                            </label>
                            <div class="col-md-8"><g:formatDate date="${lease.leaseExpiryDate}" format="dd MMM, yyyy" /></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-8">
        <div class="portlet box grey-gallery">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info"></i> Lease Items
                </div>
            </div>

            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <table class="table table-bordered table-condensed table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>ChargeCode</th>
                                    <th>Amount</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${leaseCheck.entity.leaseItems}" var="leaseItem" >
                                <tr>
                                    <td>${leaseItem.subAccount} - ${leaseCheck.entity.id}</td>
                                    <td>${leaseItem.amount}</td>
                                    <td><g:formatDate date="${leaseItem.startDate}" format="dd MMM, yyyy" /></td>
                                    <td><g:formatDate date="${leaseItem.endDate}" format="dd MMM, yyyy" /></td>
                                </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
