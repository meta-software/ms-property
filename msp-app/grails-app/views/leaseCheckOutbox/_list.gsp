<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Action</th>
        <th>Lease#</th>
        <th>Tenant</th>
        <th>RentalUnit</th>
        <th>Status</th>
        <th>Date Created</th>
        <th>User</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${leaseCheckList}">
        <g:each in="${leaseCheckList}" var="leaseCheck">
            <tr>
                <td>
                    <g:link class="display-block" action="show" id="${leaseCheck.id}" >
                        ${leaseCheck.id}
                    </g:link>
                </td>
                <td>${leaseCheck.actionName}</td>
                <td>${leaseCheck.entity?.leaseNumber}</td>
                <td>
                    <g:link class="display-block" action="show" id="${leaseCheck.id}" >${leaseCheck.entity.tenant}</g:link>
                </td>
                <td>
                    ${leaseCheck.entity.rentalUnit}
                </td>
                <td>${leaseCheck.checkStatus}</td>
                <td><g:formatDate date="${leaseCheck.makeDate}" /></td>
                <td>${leaseCheck.maker.username}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="7">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
