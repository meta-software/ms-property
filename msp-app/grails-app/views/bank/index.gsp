<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'bank.label', default: 'Banks')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'bank.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="bank.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="app"></div>

<template id="bank-app">
    <section>
        <g:render template="page_bar"/>

        <router-view></router-view>
    </section>
</template>

<template id="bank-list">
    <div class="search-page search-content-4 margin-top-10">
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-2 col-md-2">
                        <input type="text" v-model="params.bankName" class="form-control" placeholder="Bank Name" name="bankName" >
                    </div>
                    <div class="col-lg-2 col-md-2">
                        <input type="text" v-model="params.bankCode" class="form-control" name="bankCode" placeholder="Bank Code" >
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="button" @click.prevent="search">Search</button>
                        <button class="btn green" type="button"><i class="fa fa-refresh" @click="refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-striped table-bordered table-hover table-advance">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="bankList.length > 0" v-for="bank in bankList">
                    <td>{{bank.bankName}}</td>
                    <td>{{bank.bankCode}}</td>
                    <td>
                        <span class="label mc-status" :class="{'label-info':bank.active, 'label-danger':!bank.active}">
                            {{bank.active ? 'Active' : 'InActive'}}
                        </span>
                    </td>
                    <td>
                        <div class="actions">
                            <router-link :to="{name: 'edit', params:{id: bank.id}}"
                                         class="btn btn-xs btn-primary">
                                <i class="fa fa-edit"></i> Edit
                            </router-link>
                        </div>
                    </td>
                </tr>
                <tr v-if="bankList.length == 0">
                    <td colspan="4">No Records Found...</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="search-pagination">
            <mp:pagination total="${tenantCount ?: 0}" />
        </div>
    </div>
</template>

<template id="bank-edit">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Account Type</div>
                </div>

                <div class="portlet-body">
                    <bank-form v-on:save="save" v-bind:bank="bank"></bank-form>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="bank-form">
    <g:form action="save" method="POST" role="form" class="form-horizontal">
        <g:hiddenField name="id" value="" v-model="bank.id"/>
        <!-- BEGIN FORM PORTLET-->
        <div class="portlet light">

            <div class="portlet-body form">

                <div class="tab-pane active" id="tab0">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Bank Name
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text"
                                             name="name"
                                             v-model="bank.bankName"
                                             value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Bank Code
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text" name="code"
                                             v-model="bank.bankCode"
                                             value=""/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Is Active?
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" v-model="bank.active" value="1"
                                                   name="active"/>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions right">
                    <button @click="$router.push({name: 'home'})" type="button" class="btn default">Cancel</button>

                    <button @click="onSubmit" type="button" class="btn green" name="create" v-show="bank.id == null">
                        ${message(code: 'default.button.create.label', default: 'Submit')}
                    </button>
                    <button @click="onUpdate" type="button" class="btn green" name="create"
                            v-show="bank.id != null">
                        ${message(code: 'default.button.update.label', default: 'Update')}
                    </button>
                </div>

            </div>
        </div>
        <!-- END FORM PORTLET-->
    </g:form>
</template>

<template id="bank-create">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Bank Form</div>
                </div>

                <div class="portlet-body">
                    <bank-form v-on:save="save" v-bind:bank="bank"></bank-form>
                </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js"/>
<asset:javascript src="vue-apps/bank/bank-app.js" asset-defer="true"/>

</body>
</html>