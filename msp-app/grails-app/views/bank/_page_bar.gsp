<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <router-link :to="{name: 'home'}" class="btn btn-default btn-sm btn-outline">
            <i class="fa fa-list"></i> Banks
        </router-link>
        <router-link :to="{name: 'create'}" class="btn btn-primary btn-sm">
            <i class="fa fa-plus-square"></i> Add Bank
        </router-link>
    </div>
</div>