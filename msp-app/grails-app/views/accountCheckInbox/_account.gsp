<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Account Name:
            </label>
            <div class="col-md-7">${account.accountName}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Account Type:
            </label>
            <div class="col-md-7">${account.accountType}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Account Category:
            </label>
            <div class="col-md-7">${account.accountCategory}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                VAT No:
            </label>
            <div class="col-md-7">${account.vatNumber}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                BP Number:
            </label>
            <div class="col-md-7">${account.bpNumber}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Email Address:
            </label>
            <div class="col-md-7">${account.emailAddress}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Physical Addr:
            </label>
            <div class="col-md-7"><mp:address address="${account.physicalAddress}" /></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Postal Addr:
            </label>
            <div class="col-md-7"><mp:address address="${account.postalAddress}" /></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Business Addr:
            </label>
            <div class="col-md-7"><mp:address address="${account.businessAddress}" /></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Phone No:
            </label>
            <div class="col-md-7">${account.phoneNumber}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-5">
                Fax Number:
            </label>
            <div class="col-md-7">${account.faxNumber}</div>
        </div>
    </div>
</div>

<h4 class="block"><i class="fa fa-bank"></i> Bank Account</h4>

<g:set var="bankAccount" value="${account.bankAccount}" />
<g:if test="${bankAccount}">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-5">
                    Account Name:
                </label>
                <div class="col-md-7">${bankAccount.accountName}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-5">
                    Bank:
                </label>
                <div class="col-md-7">${bankAccount.bank}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-5">
                    Account No:
                </label>
                <div class="col-md-7">${bankAccount.accountNumber}</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-5">
                    Branch:
                </label>
                <div class="col-md-7">${bankAccount.branch}</div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-md-5">
                    Branch Code:
                </label>
                <div class="col-md-7">${bankAccount.branchCode}</div>
            </div>
        </div>
    </div>
</g:if>
<g:else>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <p class="note note-info">No Bank Account.</p>
        </div>
    </div>
</g:else>

<!--f:display bean="account" /-->
