<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="account-check-inbox" action="index" class="btn blue-hoki btn-sm ">
            <i class="fa fa-list"></i> Pending Requests
        </g:link>
    </div>
</div>