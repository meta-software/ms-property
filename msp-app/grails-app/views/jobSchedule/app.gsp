<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'jobSchedule.label', default: 'Job Schedule')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'jobSchedule.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <asset:stylesheet src="global/css/search.css" defer="true" />

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <router-view></router-view>
    </div>
</template>

<template id="job-schedule-list">
    <section class="margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="row">
            <div class="col-md-12">
                <div class="search-page">
                    <div class="search-bar bordered">
                        <div class="row">
                            <div class="col-lg-3">
                                <input type="text" v-model="params.jobName" class="form-control" placeholder="Job Name..." @keyup.enter.prevent="search" />
                            </div>
                            <div class="col-lg-3 extra-buttons">
                                <button class="btn grey-steel uppercase bold" type="button" @click="search" >Search</button>
                                <button class="btn green" type="button" @click="reset" ><i class="fa fa-refresh"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="search-table table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><span>Job Name</span></th>
                                <th><span>Job Type</span></th>
                                <th><span>Cron Expression</span></th>
                                <th><span>Status</span></th>
                                <th><span>Last Run Time</span></th>
                                <th><span>Next Run Time</span></th>
                                <th><span>Last Run Status</span></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(jobSchedule, idx) in jobSchedules" >
                                <td><router-link :to="{path: '/show/'+jobSchedule.id}">{{jobSchedule.jobName}}</router-link></td>
                                <td>{{jobSchedule.jobType.name}}</td>
                                <td>{{jobSchedule.cronExpression}}</td>
                                <td><span class="label label-sm mc-status" :class="{'label-success': jobSchedule.active, 'label-danger': !jobSchedule.active}">{{jobSchedule.active | formatBoolean(['Active', 'Disabled'])}}</span></td>
                                <td>{{jobSchedule.lastRunTime | formatDate('YYYY-MM-DD HH:MM')}}</td>
                                <td>{{jobSchedule.nextRunTime | formatDate('YYYY-MM-DD HH:MM')}}</td>
                                <td>{{jobSchedule.lastRunStatus}}</td>
                            </tr>
                            <tr v-if="jobSchedules.length === 0">
                                <td colspan="8">No Records found...</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div v-if="false" class="search-pagination">
                        <ul class="pagination">
                            <li class="page-active">
                                <a href="javascript:;"> 1 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 3 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 4 </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </section>

</template>

<template id="job-schedule-show">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered entity-details bg-inverse">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-clock"></i> Job Schedule Info : {{jobSchedule.jobName}}
                        </div>

                        <div class="actions">
                            <button type="button" class="btn btn-sm green"><i class="fa fa-play"></i> Execute</button>
                            <button type="button" class="btn btn-sm btn-danger"><i class="fa fa-stop"></i> Disable</button>
                            <button type="button" class="btn btn-sm blue-hoki"><i class="fa fa-edit"></i> Edit</button>
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="horizontal-form" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Job Id:</label>
                                            <div class="form-control-static">{{jobSchedule.id}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Job Type:</label>
                                            <div class="form-control-static">{{jobSchedule.jobType.name}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Job Name:</label>
                                            <div class="form-control-static">{{jobSchedule.jobName}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Status:</label>
                                            <div class="form-control-static"><span :class="{'label-success':jobSchedule.active, 'label-danger':!jobSchedule.active}" class="request-status label label-sm mc-status">{{jobSchedule.active | formatBoolean(['Active', 'Disabled' ]) }}</span></div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Last Run Date</label>
                                            <div class="form-control-static">{{jobSchedule.lastRunDate | formatDate}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Next Run Date</label>
                                            <div class="form-control-static">{{jobSchedule.nextRunDate | formatDate}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Cron Expression:</label>
                                            <div class="form-control-static" style="font-style: italic">{{jobSchedule.cronExpression}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Job Run List
                        </div>
                    </div>

                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-clock-o"></i> Job Schedules
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="">
                <router-link class="btn btn-sm blue-hoki" to="/">
                    <i class="fa fa-list-alt"></i> Job Schedules
                </router-link>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/jobSchedule/job-schedule-app.js" asset-defer="true"/>

</body>
</html>