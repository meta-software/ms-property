<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" placeholder="Reference..." class="form-control" name="txref" value="${params?.txref}" />
                </div>

                <div class="col-lg-2 col-md-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-scrollable">
        <table class="table table-striped table-bordered table-hover table-advance">
            <thead>
            <tr>
                <th>Txn Type</th>
                <th>Credit Acc</th>
                <th>Debit Acc</th>
                <th>Sub Acc</th>
                <th>Ref</th>
                <th>Txn Date</th>
                <th>Txn Details</th>

                <th>Txn Cur</th>
                <th>Credit</th>
                <th>Debit</th>

                <th>Operating Cur</th>
                <th>Operating Cr</th>
                <th>Operating Db</th>

                <th>Reporting Cur</th>
                <th>Reporting Cr</th>
                <th>Reporting Db</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${transactionList}" var="transaction">
                <tr>
                    <td>${transaction.transactionType}</td>
                    <td>${transaction.creditAccount}</td>
                    <td>${transaction.debitAccount}</td>
                    <td>${transaction.subAccountNumberCr}:${transaction.subAccountNameCr}</td>
                    <td>${transaction.transactionReference}</td>
                    <td><g:formatDate date="${transaction.transactionDate}" format="yyyy-MM-dd" /></td>
                    <td>${transaction.description}</td>

                    <td>${transaction.transactionCurrency}</td>
                    <td><g:formatNumber number="${transaction.credit}" format="###,##0.00" /></td>
                    <td><g:formatNumber number="${transaction.debit}" format="###,##0.00" /></td>

                    <td>${transaction.operatingCurrency}</td>
                    <td><g:formatNumber number="${transaction.creditOperatingAmount}" format="###,##0.00" /></td>
                    <td><g:formatNumber number="${transaction.debitOperatingAmount}" format="###,##0.00" /></td>

                    <td>${transaction.reportingCurrency}</td>
                    <td><g:formatNumber number="${transaction.creditReportingAmount}" format="###,##0.00" /></td>
                    <td><g:formatNumber number="${transaction.debitReportingAmount}" format="###,##0.00" /></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>