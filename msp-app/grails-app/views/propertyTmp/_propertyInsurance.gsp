<!-- START FORM PORTLET -->
<div class="portlet box default">
    <div class="portlet-body">
        <!-- BEGIN FORM-->
        <form class="form-horizontal form-horizontal-left" role="form">
            <div class="form-body">
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 bold">Insurer Name:</label>
                            <div class="col-md-8">
                                <span class="form-control-static"> ${propertyInsurance.insurerName} </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 bold">Insurance No:</label>
                            <div class="col-md-8">
                                <span class="form-control-static">${propertyInsurance.insuranceNumber}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 bold">Renewal Date:</label>
                            <div class="col-md-8">
                                <span class="form-control-static"> <g:formatDate date="${propertyInsurance.renewalDate}" /></span>                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 bold">Sum Assured:</label>
                            <div class="col-md-8">
                                <span class="form-control-static"> <g:formatNumber type="currency" number="${propertyInsurance.sumAssured}" currencyCode="USD" /></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4 bold">Status:</label>
                            <div class="col-md-8">
                                <span class="form-control-static">
                                    <span class="label label-sm label-${mp.boolStatusClass(status: propertyInsurance.active)}">${propertyInsurance.active ? "Active" : "InActive"}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->

                <div class="actions">
                    <!-- g:link class="btn blue btn-outline sbold" controller="rentalUnit" action="create" params="['property.id': property.id]">
                                            <i class="fa fa-plus"></i>
                                            Create
                                        < /g:link  -->

                    <a class="btn btn-sm red-haze" >
                        <i class="fa fa-plus"></i> Terminate
                    </a>
                </div>

            </div>
        </form>
        <!-- END FORM-->

    </div>
</div>
<!-- END FORM PORTLET -->
