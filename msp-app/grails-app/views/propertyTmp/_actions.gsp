<div class="actions entity-actions">
    <g:link controller="property" action="create" class="btn btn-sm btn-primary" >
        <i class="fa fa-plus"></i>
        <span>Create Property</span>
    </g:link>
    <g:link controller="property" class="btn btn-sm btn-default" >
        <i class="fa fa-list"></i>
        <span>List Property</span>
    </g:link>
</div>
