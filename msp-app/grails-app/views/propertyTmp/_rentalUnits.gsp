<div class="table-responsive" id="landlord_rentalUnits" style="background-color: #ffffff;">
    <table id="tbl-ru-001" data-heights="300" data-toggle="table" data-classes="table table-condensed table-striped table-hover ">
        <thead>
        <tr>
            <th><input type="checkbox" name="all" id="all" class="icheckbox_flat-orange"/></th>
            <th>Reference</th>
            <th>Name</th>
            <th>Area</th>
            <th>Occupation</th>
            <th>Occupied By</th>
        </tr>
        </thead>
        <tbody>
        <g:if test="${rentalUnitList}">
            <g:each in="${rentalUnitList}" var="rentalUnit">
                <tr>
                    <td>
                        <g:if test="${rentalUnit.isChecked()}">
                            <input type="checkbox" disabled="disabled" class="icheckbox_flat-orange"/>
                        </g:if>
                        <g:else>
                            <input type="checkbox" name="ru-${rentalUnit.id}" id="ru-${rentalUnit.id}" class="icheckbox_flat-orange"/>
                        </g:else>
                    </td>
                    <td>${rentalUnit.unitReference}</td>
                    <td>
                        <g:link controller="rentalUnit" action="show" class="display-block" resource="${rentalUnit}">${rentalUnit.name}</g:link>
                        <!-- ${rentalUnit.name} -->
                    </td>
                    <td><g:formatNumber number="${rentalUnit.area}" format="###,###" /> M<sup>2</sup></td>
                    <td>
                        <span class="label label-${mp.boolStatusClass(status: rentalUnit.isOccupied)} label-sm"> ${rentalUnit.isOccupied ? 'Occupied' : 'Vacant'} </span>
                    </td>
                    <td>
                        <span class="">${rentalUnit.activeLease?.tenant?.accountName ?: 'N/A'}</span>
                    </td>
                </tr>
            </g:each>
        </g:if>
        <g:else>
            <tr>
                <td colspan="6">
                    <g:message message="default.records.notfound" />
                </td>
            </tr>
        </g:else>
        </tbody>
    </table>
</div>
