<div class="portlet box blue">
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form class="form-horizontal" role="form">
            <div class="form-body">
                <h4 class="form-section">Property Info - ${property.name}</h4>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> ${property.name} </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Landlord:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <g:link controller="property" action="show" resource="${property.landlord}">${property.landlord}</g:link>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Commission:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <g:formatNumber number="${property.commission}" format="##.00" /> %
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Date Created:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> <g:formatDate date="${new Date()}" format="dd/MMM/yyyy" /> </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Property Manager:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> ${property.propertyManager} </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Property Type:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> ${property.propertyManager} </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Charge VAT:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> <g:formatBoolean boolean="${property.chargeVat == true}" true="Yes" false="No" /> ${property.chargeVat} </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Status:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                 <span class="label label-primary label-lg">${property.propertyStatus} </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Area:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> 
                                    <g:formatNumber number="${property.area}" format="###,###.0" /> sqm
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Opening Date:</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> <g:formatDate date="${new Date()}" format="dd/MMM/yyyy" /> </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>