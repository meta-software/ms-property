<%@ page import="metasoft.property.core.Landlord" %>

<!DOCTYPE html>
<html>
    <head>
        <!-- BEGIN TEMPLATE VARS -->
        <g:set var="entityName" value="${message(code: 'propertyTmp.label', default: 'Pending Property')}" />
        <g:set var="pageHeadTitle" value="${message(code: 'propertyTmp.list.label', default: entityName, args: [entityName])}" scope="request" />
        <!-- END TEMPLATE VARS -->

        <meta name="layout" content="main"/>
        <title><g:message code="default.list.label" args="[entityName]" /></title>

    </head>

    <body>
    <div id="main-app" v-cloak>
        <g:render template="page_bar" />

        <div class="row">
            <div class="col-md-12">
                <g:render template="search_form" />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <div class="table-scrollable-borderless">
                    <table data-toggle="table" class="table-striped table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>City</th>
                            <th>Action</th>
                            <th>Landlord</th>
                            <th>Property Type</th>
                            <th>Area</th>
                            <th>Commission</th>
                            <th>Date Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:if test="${propertyTmpList}">
                            <g:each in="${propertyTmpList}" var="propertyTmp">
                                <tr>
                                    <td>
                                        <g:link controller="property-tmp" action="show" resource="${propertyTmp}" style="display: block;">${propertyTmp}</g:link>
                                    </td>
                                    <td>${propertyTmp?.address?.city}</td>
                                    <td>${propertyTmp.userAction}</td>
                                    <td>
                                        ${propertyTmp.landlord}
                                    </td>
                                    <td>${propertyTmp.propertyType}</td>
                                    <td><g:formatNumber number="${propertyTmp.area}" format="###,###.0" /> m<sup>2</sup></td>
                                    <td>${propertyTmp.commission} %</td>
                                    <td><g:formatDate date="${propertyTmp.dateCreated}" format="yyyy MMM, dd" /></td>
                                </tr>
                            </g:each>
                        </g:if>

                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <mp:pagination total="${propertyCount ?: 0}" />
                </div>

            </div>
        </div>

    </div>

    <template id="property-form">
        <div class="clearfix inline-block" ref="propertyForm">
            <button class="btn btn-sm blue-hoki" @click="openModal=true"><i class="fa fa-plus"></i> Add Property</button>

            <modal v-model="openModal" size="lg" ref="modal" id="property-form-modal" @hide="modalCallback" >

                <span slot="title"><i class="fa fa-plus"></i> Create Property</span>

                <g:form method="POST" controller="property" action="save" role="form" class="form-horizontal">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">${message(code: 'label.propertyTmp.landlord', default: 'Landlord')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <g:select from="${landlordList}"
                                              optionValue="accountName"
                                              optionKey="id"
                                              noSelection="['': 'Landlord Select...']"
                                              class="form-control select2"
                                              name="landlordItem"></g:select>
                                    <input type="hidden" name="landlord" v-validate="'required'" v-model="propertyForm.landlord"/>
                                    <span class="has-error help-block help-block-error">{{ errors.first('landlord') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">${message(code: 'label.propertyTmp.name', default: 'Name')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <input v-validate="'required'" v-model="propertyForm.name" class="form-control" placeholder="" type="text" name="name" />
                                    <span class="has-error help-block help-block-error">{{ errors.first('name') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">${message(code: 'label.propertyTmp.propertyType', default: 'Property Type')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <g:select name="propertyType"
                                              v-model="propertyForm.propertyType"
                                              v-validate="'required'"
                                              noSelection="['': 'Select Type...']"
                                              from="${metasoft.property.core.PropertyType.list()}"
                                              class="form-control"
                                              optionKey="id"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('propertyType') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">${message(code: 'label.propertyTmp.propertyManager', default: 'Property Manager')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <g:select name="propertyManager"
                                              v-model="propertyForm.propertyManager"
                                              v-validate="'required'"
                                              noSelection="['': 'Select Type...']"
                                              from="${metasoft.property.core.PropertyManager.list()}"
                                              class="form-control"
                                              optionKey="id"/>
                                    <span class="has-error help-block-error help-block">{{ errors.first('propertyManager') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">Stand No
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <input type="text" v-validate="'required'" v-model="propertyForm.standNumber" name="standNumber" class="form-control" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('standNumber') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    ${message(code: 'label.propertyTmp.address.street', default: 'Address Street')}
                                <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <input type="text" v-validate="'required'" v-model="propertyForm.address.street" name="street" class="form-control" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('street') }}</span>
                                </div>
                                </div>
                            </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    ${message(code: 'label.propertyTmp.address.street', default: 'Address Suburb')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <input type="text" v-validate="'required'" v-model="propertyForm.address.suburb" name="suburb" class="form-control" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('suburb') }}</span>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    ${message(code: 'label.propertyTmp.address.street', default: 'Address City')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <input type="text" v-validate="'required'" v-model="propertyForm.address.city" name="city" class="form-control" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('city') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    ${message(code: 'label.propertyTmp.address.country', default: 'Address Country')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <input type="text" v-validate="'required'" v-model="propertyForm.address.country" name="country" class="form-control" />
                                    <span class="has-error help-block-error help-block">{{ errors.first('country') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">${message(code: 'label.propertyTmp.commission', default: 'Commission')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <div class="input-inline input-medium">
                                        <div class="input-group">
                                            <g:field class="form-control" placeholder="" type="text"
                                                     v-model="propertyForm.commission"
                                                     name="commission" v-validate="'required|decimal'"/>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                        <span class="has-error help-block-error help-block">{{ errors.first('commission') }}</span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">${message(code: 'label.propertyTmp.area', default: 'Area')}
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-8">
                                    <div class="input-inline input-medium">
                                        <div class="input-group">
                                            <g:field class="form-control" placeholder="" type="text"
                                                     name="area" v-validate="'required|decimal'" v-model="propertyForm.area" />
                                            <span class="input-group-addon">
                                                M<sup>2</sup>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-4">
                                    VAT Chargeable
                                </label>

                                <div class="col-md-8">
                                    <g:checkBox class="form-control icheck"
                                                name="active"
                                                id="chargeVat"
                                                v-model="propertyForm.chargeVat" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-2">
                                    ${message(code: 'label.propertyTmp.description', default: 'Description')}
                                </label>

                                <div class="col-md-10">
                                    <g:textArea class="form-control" placeholder="" name="description" v-validate="'required'"
                                                v-model="propertyForm.description"
                                                rows="2"/>
                                </div>
                            </div>
                        </div>
                    </div>


                </g:form>

                <div slot="footer">
                    <btn @click="openModal=false"><i class="fa fa-times"></i> Cancel</btn>
                    <btn @click="saveProperty" class="blue-hoki" ><i class="fa fa-save"></i> Submit</btn>
                </div>

            </modal>
        </div>
    </template>

    <asset:script>
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
    </asset:script>

    <asset:javascript src="application-vue.js" asset-defer="true"/>
    <asset:javascript src="vue-apps/propertyTmp/propertyTmp-index.js" asset-defer="true"/>

    </body>
</html>