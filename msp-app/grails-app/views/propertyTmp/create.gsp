<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyTmp.label', default: 'Pending Property')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'default.create.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.propertyTmp}">
    <g:render template="/templates/errors" model="['obj': this.propertyTmp]"/>
</g:hasErrors>

<g:form action="save" controller="property-tmp" method="POST" class="form-horizontal" name="property-form">
    <g:render template="form_app" bean="propertyTmp"/>
</g:form>

<template id="rental-unit-form">
    <div>
        <table class="table table-hover table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th style="width: 30%;">Reference</th>
                <th style="width: 30%;">Name</th>
                <th style="width: 30%;">Area (m<sup>2</sup>)</th>
                <th>Save</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(rentalUnit, key) in rentalUnits" key="key">
                <td>{{rentalUnit.unitReference}}</td>
                <td>{{rentalUnit.name}}</td>
                <td>{{rentalUnit.area}} M<sup>2</sup></td>
                <td><button @click="removeUnit(key)" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash" ></i></button></td>
            </tr>
            <tr>
                <td style="width: 20%;">
                    <input v-validate="'required'" v-model="rentalUnit.unitReference" type="text" name="unitReference" class="form-control input-sm has-error" />
                    <span class="has-error help-block-error help-block">{{ errors.first('unitReference') }}</span>
                </td>
                <td>
                    <input v-validate="'required'" v-model="rentalUnit.name" type="text" name="name" class="form-control input-sm" />
                    <span class="has-error help-block-error help-block">{{ errors.first('name') }}</span>
                </td>
                <td style="width: 15%;">
                    <input v-validate="'required|decimal:2'" v-model="rentalUnit.area" type="text" name="area" class="form-control input-sm" />
                    <span class="has-error help-block-error help-block">{{ errors.first('area') }}</span>
                </td>
                <td style="width: 5%;"><button @click="addUnit" type="button" class="btn btn-primary btn-xs"><i class="fa fa-save" ></i></button></td>
            </tr>
            </tbody>
        </table>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="/vue-apps/propertyTmp/propertyTmp-form.js" asset-defer="true"/>

</body>
</html>

