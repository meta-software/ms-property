<%@ page import="metasoft.property.core.Landlord" %>
<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>

<!-- BEGIN FORM PORTLET-->
<div class="portlet white">
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-6">
                <g:if test="${propertyTmp.landlord?.id == null}">
                    <div class="form-group">
                        <label class="control-label col-md-3">${message(code: 'label.propertyTmp.name', default: 'Landlord Account')}
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:select name="landlord"
                                      noSelection="['': 'Select Landlord...']"
                                      from="${Landlord.list()}"
                                      value="${property?.landlord?.id}"
                                      class="form-control select2"
                                      optionKey="id"/>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <g:hiddenField name="landlord" value="${propertyTmp.landlord.id}" />
                    <div class="form-group">
                        <label class="control-label col-md-3">${message(code: 'label.propertyTmp.name', default: 'Landlord Account')}
                        </label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" disabled="disabled"
                                   value="${propertyTmp.landlord} -  ${propertyTmp.landlord.accountNumber}"/>
                        </div>
                    </div>
                </g:else>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.propertyTmp.name', default: 'Name')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" placeholder="" type="text" name="name"
                                 value="${this.propertyTmp.name}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.propertyTmp.propertyType', default: 'Property Type')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:select name="propertyType"
                                  noSelection="['': 'Select Type...']"
                                  from="${PropertyType.list()}"
                                  value="${property?.propertyType?.id}"
                                  class="form-control select2-no-search"
                                  optionKey="id"/>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.propertyTmp.propertyManager', default: 'Property Manager')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:select name="propertyManager"
                                  noSelection="['': 'Select Manager...']"
                                  from="${PropertyManager.list()}"
                                  value="${property?.propertyManager?.id}"
                                  class="form-control select2"
                                  optionKey="id"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Stand No
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field type="text" name="standNumber" class="form-control"
                                 value="${propertyTmp.standNumber}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.propertyTmp.address', default: 'Address')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textArea class="form-control" placeholder="" name="address"
                                    value="${this.propertyTmp.address}"
                                    rows="2"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.propertyTmp.description', default: 'Description')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textArea class="form-control" placeholder="" name="description"
                                    value="${this.propertyTmp.description}" rows="2"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'propertyTmp.label.city', default: 'City')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" placeholder="" type="text" name="city"
                                 value="${this.propertyTmp.city}"/>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.propertyTmp.commission', default: 'Commission')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-inline input-medium">
                            <div class="input-group">
                                <g:field class="form-control" placeholder="" type="text"
                                         name="commission"
                                         value="${this.propertyTmp.commission}"/>
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.propertyTmp.area', default: 'Area')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-inline input-large">
                            <div class="input-group">
                                <g:field class="form-control" placeholder="" type="text"
                                         name="area"
                                         value="${this.propertyTmp.area}"/>
                                <span class="input-group-addon">
                                    M<sup>2</sup>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        VAT Chargeable
                    </label>

                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <g:checkBox class="form-control icheck"
                                        name="active"
                                        id="chargeVat"
                                        checked="${propertyTmp.chargeVat}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${propertyTmp.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>
    </div>
</div>
<!-- END FORM PORTLET-->
