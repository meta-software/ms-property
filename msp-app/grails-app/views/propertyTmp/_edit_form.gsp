<%@ page import="metasoft.property.core.Landlord" %>
<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>

<!-- BEGIN FORM PORTLET-->

<div class="portlet white">
    <div class="portlet-body form">

        <div class="row">
            <div class="col-md-6">
                <g:if test="${property.landlord?.id == null}">
                    <div class="form-group">
                        <label class="control-label col-md-3">${message(code: 'label.property.name', default: 'Landlord Account')}
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:select name="landlord"
                                      noSelection="['': 'Select Landlord...']"
                                      from="${Landlord.list()}"
                                      value="${property?.landlord?.id}"
                                      class="form-control select2"
                                      optionKey="id"/>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <g:hiddenField name="landlord" value="${property.landlord.id}" />
                    <div class="form-group">
                        <label class="control-label col-md-3">${message(code: 'label.property.name', default: 'Landlord Account')}
                        </label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" disabled="disabled"
                                   value="${property.landlord} -  ${property.landlord.accountNumber}"/>
                        </div>
                    </div>
                </g:else>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.name', default: 'Name')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" placeholder="" type="text" name="name"
                                 value="${this.property.name}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.propertyType', default: 'Property Type')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:select name="propertyType"
                                  noSelection="['': 'Select Type...']"
                                  from="${PropertyType.list()}"
                                  value="${property?.propertyType?.id}"
                                  class="form-control select2-no-search"
                                  v-validate="'required'"
                                  optionKey="id"/>
                        <span class="has-error help-block-error help-block">{{ errors.first('propertyType') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.propertyManager', default: 'Property Manager')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:select name="propertyManager"
                                  noSelection="['': 'Select Manager...']"
                                  from="${PropertyManager.list()}"
                                  value="${property?.propertyManager?.id}"
                                  class="form-control select2"
                                  v-validate="'required'"
                                  optionKey="id"/>
                        <span class="has-error help-block-error help-block">{{ errors.first('propertyManager') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Stand No
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field type="text" name="standNumber" class="form-control"
                                 v-validate="'required'"
                                 value="${property.standNumber}"/>
                        <span class="has-error help-block-error help-block">{{ errors.first('standNumber') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.address.street', default: 'Address Street')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textField name="address.street" v-validate="'required'" class="form-control" value="${property.address?.street}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('address.street') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.address.street', default: 'Address Suburb')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textField name="address.suburb" v-validate="'required'" class="form-control" value="${property.address?.suburb}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('address.suburb') }}</span>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.address.street', default: 'Address City')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textField name="address.city" v-validate="'required'" class="form-control" value="${property.address?.city}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('address.city') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.address.country', default: 'Address Country')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textField name="address.country" v-validate="'required'" class="form-control" value="${property.address?.country}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('address.country') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.commission', default: 'Commission')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-inline input-medium">
                            <div class="input-group">
                                <g:field class="form-control" placeholder="" type="text"
                                         name="commission"
                                         v-validate="'required'"
                                         value="${this.property.commission}"/>
                                <span class="input-group-addon">%</span>
                            </div>
                            <span class="has-error help-block-error help-block">{{ errors.first('commission') }}</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.area', default: 'Area')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-inline input-large">
                            <div class="input-group">
                                <g:field class="form-control" placeholder="" type="text"
                                         name="area"
                                         v-validate="'required'"
                                         value="${this.property.area}"/>
                                <span class="input-group-addon">
                                    M<sup>2</sup>
                                </span>
                            </div>
                        </div>
                        <span class="has-error help-block-error help-block">{{ errors.first('area') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        VAT Chargeable
                    </label>

                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <g:checkBox class="form-control icheck"
                                        name="active"
                                        id="chargeVat"
                                        checked="${property.chargeVat}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.description', default: 'Description')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textArea class="form-control" placeholder="" name="description"
                                    value="${this.property.description}" rows="2"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${property.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>

    </div>
</div>
<!-- END FORM PORTLET-->
