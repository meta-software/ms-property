<%@ page import="metasoft.property.core.Landlord" %>
<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Property Form</div>

                <div class="actions">
                    <button type="button" class="btn btn-sm" @click="onCancel" >
                        <i class="fa fa-times"></i> Cancel
                    </button>
                    <button type="button" class="btn btn-sm green dropdown-toggle" @click="onSaveProperty">
                        <i class="fa fa-check"></i> Save
                    </button>
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM PORTLET-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.landlord', default: 'Landlord')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <select2 :select2="landlordOptions" name="landlord" v-validate="'required'" v-model="property.landlord.id">
                                    <option>Select Landlord...</option>
                                </select2>
                                <span class="has-error help-block help-block-error">{{ errors.first('landlord') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyType', default: 'Property Type')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select name="propertyType"
                                          v-model="property.propertyType.id"
                                          v-validate="'required'"
                                          noSelection="['': 'Select Type...']"
                                          from="${metasoft.property.core.PropertyType.list()}"
                                          class="form-control"
                                          optionKey="id"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('propertyType') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.name', default: 'Name')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input v-validate="'required'" v-model="property.name" class="form-control" placeholder="" type="text" name="name" />
                                <span class="has-error help-block help-block-error">{{ errors.first('name') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.propertyManager', default: 'Property Manager')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <g:select name="propertyManager"
                                          v-model="property.propertyManager.id"
                                          v-validate="'required'"
                                          noSelection="['': 'Select Type...']"
                                          from="${metasoft.property.core.PropertyManager.list()}"
                                          class="form-control"
                                          optionKey="id"/>
                                <span class="has-error help-block-error help-block">{{ errors.first('propertyManager') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">Stand No
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="property.standNumber" name="standNumber" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('standNumber') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.area', default: 'Area')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <div class="input-inline input-medium">
                                    <div class="input-group">
                                        <g:field class="form-control" placeholder="" type="text"
                                                 name="area" v-validate="'required|decimal'" v-model="property.area" />
                                        <span class="input-group-addon">
                                            M<sup>2</sup>
                                        </span>
                                    </div>
                                    <span class="has-error help-block-error help-block">{{ errors.first('area') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                ${message(code: 'label.property.address.street', default: 'Address Street')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="property.address.street" name="street" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('street') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                ${message(code: 'label.property.address.street', default: 'Address Suburb')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="property.address.suburb" name="suburb" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('suburb') }}</span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                ${message(code: 'label.property.address.street', default: 'Address City')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="property.address.city" name="city" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('city') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                ${message(code: 'label.property.address.country', default: 'Address Country')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="text" v-validate="'required'" v-model="property.address.country" name="country" class="form-control" />
                                <span class="has-error help-block-error help-block">{{ errors.first('country') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">${message(code: 'label.property.commission', default: 'Commission')}
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <div class="input-inline input-medium">
                                    <div class="input-group">
                                        <g:field class="form-control" placeholder="" type="text"
                                                 v-model="property.commission"
                                                 name="commission" v-validate="'required|decimal'"/>
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <span class="has-error help-block-error help-block">{{ errors.first('commission') }}</span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                VAT Chargeable
                            </label>

                            <div class="col-md-8">
                                <g:checkBox class="form-controls icheck"
                                            name="active"
                                            id="chargeVat"
                                            v-model="property.chargeVat" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-2">
                                ${message(code: 'label.property.description', default: 'Description')}
                            </label>

                            <div class="col-md-10">
                                <g:textArea class="form-control" placeholder="" name="description"
                                            v-model="property.description"
                                            rows="2"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM PORTLET-->
            </div>
        </div>
    </div>
</div>
