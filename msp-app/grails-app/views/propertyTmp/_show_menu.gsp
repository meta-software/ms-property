<div class="actions entity-actions">
    <g:link action="rentalUnits" resource="${property}" class="btn btn-sm green" >
        <i class="fa"></i>
        <span>Rental Units</span>
    </g:link>

    <g:link action="rentalUnitGroups" resource="${property}" class="btn btn-sm green" >
        <i class="fa"></i>
        <span>Rental Unit Groups</span>
    </g:link>

    <g:link action="insurance" resource="${property}" class="btn btn-sm green" >
        <i class="fa"></i>
        <span>Insurance</span>
    </g:link>

</div>
