<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyTmp.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle" value="Property Info - ${propertyTmp.name}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let propertyId = ${propertyTmp.id};
    </script>

    <style type="text/css">
        .deleted {
            text-decoration: line-through;
            color: red;
        }
    </style>
</head>

<body>

<div id="main-app" v-cloak>
    <g:render template="page_bar_show"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-5">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Property Details
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-condensed">
                        <tbody>
                        <tr>
                            <th width="12.5%" >Name: </th><td width="37.5%" >${propertyTmp.name}</td>
                            <th width="12.5%" >Landlord: </th><td width="37.5%" ><g:link class="display-block" controller="landlord" action="show" id="${propertyTmp.landlord.id}">${propertyTmp.landlord}</g:link></td>
                        </tr>
                        <tr>
                            <th>Area: </th><td><g:formatNumber number="${propertyTmp.area}" format="##,###.#" /> M<sup>2</sup></td>
                            <th>Property Type: </th><td>${propertyTmp.propertyType.name}</td>
                        <tr>
                            <th rowspan="2">Address: </th><td rowspan="2"><mp:address address="${propertyTmp.address}" /></td>
                            <th>Manager: </th><td><g:link class="display-block" controller="property-manager" action="show" id="${propertyTmp.propertyManager.id}">${propertyTmp.propertyManager}</g:link></td>
                        </tr>
                        <tr>
                            <th>Commission: </th><td><g:formatNumber number="${propertyTmp.commission}" type="number" maxFractionDigits="2" roundingMode="HALF_DOWN" maxIntegerDigits="3"  /> %</td>
                            <th>&nbsp;</th><td>&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="col-md-12" style="margin-bottom: 8px;">
            <g:form controller="property-tmp" action="post" method="POST" class="form-horizontal" role="form">
                <input type="hidden" name="id" :value="propertyId" />
                <div class="form-actions left">
                    <button name="update" type="button" class="btn green" @click="postProperty" ><i class="fa fa-save"></i> Post Property</button>
                    <button type="button" id="cancel-transaction" @click="discardProperty" class="btn default"><i class="fa fa-trash"></i> Discard</button>
                </div>
            </g:form>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Rental Units
                    </div>

                    <div class="actions">
                        <a class="btn btn-lg btn-default">
                            <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                </div>

                <div class="portlet-body">

                    <div class="row">
                        <div class="col-md-12" style="padding-bottom: 5px; padding-left: 20px;">
                            Total Units Area: <b>{{totalUnitsArea}} M<sup>2</sup></b>
                        </div>
                    </div>
                    <!-- BEGIN PROPERTIES LIST -->
                    <rental-unit-form @removeunit="onRemoveRentalUnit" @input="onAddRentalUnit" :rental-units="property.rentalUnits"></rental-unit-form>
                    <!-- BEGIN PROPERTIES LIST-->
                </div>
            </div>

        </div>
    </div>

</div>

<template id="rental-unit-form">
    <div>
        <table class="table table-hover table-bordered table-striped table-condensed">
            <thead>
            <tr>
                <th style="width: 30%;">Reference</th>
                <th style="width: 30%;">Name</th>
                <th style="width: 30%;">Area (m<sup>2</sup>)</th>
                <th>Save</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <input v-validate="'required'" v-model="rentalUnit.unitReference" type="text" name="unitReference" class="form-control input-sm has-error" />
                    <span class="has-error help-block-error help-block">{{ errors.first('unitReference') }}</span>
                </td>
                <td>
                    <input v-validate="'required'" v-model="rentalUnit.name" type="text" name="name" class="form-control input-sm" />
                    <span class="has-error help-block-error help-block">{{ errors.first('name') }}</span>
                </td>
                <td>
                    <input v-validate="'required|decimal:2'" v-model="rentalUnit.area" type="text" name="area" class="form-control input-sm" />
                    <span class="has-error help-block-error help-block">{{ errors.first('area') }}</span>
                </td>
                <td><button @click="addUnit" type="button" class="btn btn-primary btn-xs"><i class="fa fa-save" ></i></button></td>
            </tr>
            <tr v-for="(rentalUnit, key) in rentalUnits" key="key" :class="{'deleted' : rentalUnit.deleted}" >
                <td>{{rentalUnit.unitReference}}</td>
                <td>{{rentalUnit.name}}</td>
                <td>{{rentalUnit.area}} M<sup>2</sup></td>
                <td><button :disabled="rentalUnit.deleted" @click="removeRentalUnit(rentalUnit)" type="button" class="btn btn-danger btn-xs"><i class="fa fa-trash" ></i></button></td>
            </tr>
            </tbody>
        </table>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/propertyTmp/propertyTmp-show.js" asset-defer="true" />

</body>
</html>
