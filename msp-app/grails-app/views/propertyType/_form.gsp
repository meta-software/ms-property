<!-- BEGIN FORM PORTLET-->
<div class="portlet light">

    <div class="portlet-body form">

        <div class="tab-pane active" id="tab0">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group${propertyType.errors['name'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Property Type Name
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="name" value="${propertyType?.name}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group${propertyType.errors['code'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Code
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="code" value="${propertyType?.code}"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group${propertyType.errors['description'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Description
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="description" value="${propertyType?.description}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group${propertyType.errors['active'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Is Active?
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <div class="mt-checkbox-inline">
                                <g:checkBox class="form-control icheck" name="active" checked="${propertyType.active}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${propertyType.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>

    </div>
</div>

<!-- END FORM PORTLET-->