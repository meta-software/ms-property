<!-- START MODALS -->
<div class="modal fade" id="_propertyTypeFormModal" tabindex="-1" role="basic" aria-hidden="true">
    <g:form method="POST" controller="propertyType" action="save" name="_propertyTypeForm">
        <g:hiddenField name="id" value="${propertyType?.id}"/>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header default">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create Property Type</h4>
                </div>

                <div class="modal-body">
                    <!-- BEGIN FORM BODY-->
                    <div class="form-body">
                        <div class="row">
                            <!-- span -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Type Name</label>
                                    <g:field type="text" id="_propertyType-name" class="form-control"
                                             name="name"
                                             value="${propertyType?.name}"
                                             placeholder="Property Type Name" />
                                </div>
                            </div>
                            <!--span-->
                            <!--span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Code</label>
                                    <g:field type="text" name="code" id="_propertyType-reference"
                                             class="form-control"
                                            value="${propertyType?.code}"
                                             placeholder="reference-code"/>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                    </div>
                    <!-- END FORM BODY-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green" id="_propertyType-submit">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </g:form>
<!-- /.modal-dialog -->
</div>
<!-- END MODALS -->