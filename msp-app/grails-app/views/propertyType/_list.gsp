<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
        <th>Description</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${propertyTypeList}">
        <g:each in="${propertyTypeList}" var="propertyType">
            <tr>
                <td><g:link style="display: block" action="edit" id="${propertyType.id}" >
                    ${propertyType.name}
                </g:link>
                </td>
                <td>${propertyType.code}</td>
                <td>
                    <span class="label label-sm label-${mp.boolStatusClass(status: propertyType.active)}">${propertyType.active ? 'Active' : 'Inactive'}</span>
                </td>
                <td>${propertyType.description}</td>
                <td>
                    <div class="actions">
                        <g:link action="edit" id="${propertyType.id}"  class="btn btn-xs btn-warning"  >
                            <i class="fa fa-edit"></i> Edit
                        </g:link>
                    </div>
                </td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="5">
                No Contents to show for propertyTypes
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
