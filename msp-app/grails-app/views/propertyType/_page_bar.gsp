<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="propertyType" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Property Types
        </g:link>
        <!-- <a class="btn blue btn-sm btn-outline" data-toggle="modal"
           href="#_propertyTypeFormModal">
            <i class="fa fa-plus"></i> Add Property Type
        </a> -->
        <g:link controller="propertyType" action="create" class="btn green btn-sm btn-outline ">
            <i class="fa fa-plus"></i> Add Property Type
        </g:link> 
    </div>
</div>