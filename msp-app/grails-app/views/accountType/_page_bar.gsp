<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <router-link :to="{name: 'home'}" class="btn btn-default btn-sm btn-outline">
            <i class="fa fa-list"></i> Account Types
        </router-link>
        <!-- g:link controller="accountType" action="index" class="btn btn-default btn-sm btn-outline ">
            <i class="fa fa-list"></i> Account Types
        < /g:link>
        <!-- <a class="btn blue btn-sm btn-outline" data-toggle="modal"
           href="#_accountTypeFormModal">
            <i class="fa fa-plus"></i> Add Property Type
        </a> -->
        <router-link :to="{name: 'create'}" class="btn green btn-sm btn-outline">
            <i class="fa fa-plus"></i> Add Account Type
        </router-link>
        <!--g:link controller="accountType" action="create" class="btn green btn-sm btn-outline ">
            <i class="fa fa-plus"></i> Add Account Type
        < /g:link -->
    </div>
</div>