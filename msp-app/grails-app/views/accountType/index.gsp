<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'accountType.label', default: 'Account Type Configurations')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'accountType.list.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="accountType.list.label" args="[entityName]" /></title>
</head>

<body>

<g:render template="page_bar" />

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message" />
</g:if>

<div class="row">
    <div class="col-md-12">

        <div class="portlet">
            <div class="portlet-body">
                <div class="table-scrollable" >
                    <g:render template="list" />
                </div>
            </div>
        </div>

        <div class="pagination">
            <mp:pagination total="${accountTypeCount ?: 0}" />
        </div>

    </div>
</div>

<asset:javascript src="accountType-form.js" asset-defer="true"/>
<asset:javascript src="accountType-form.js" asset-defer="true"/>

</body>
</html>