<!doctype html>
<html>
<html lang="en">
    <meta name="layout" content="login"/>
    <link href="assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
    <title><g:message code="default.login.label" default="MetaSoft::MSProperty - Login"/></title>
</head>

<body>

<!-- BEGIN LOGIN -->
<div class="content" id="login-app">

<!-- BEGIN LOGIN FORM -->
    <g:form class="login-form" method="POST" controller="login" action="authenticate" autocomplete="off">
        <div class="form-title">
            <span class="form-title">Welcome.</span>
            <span class="form-subtitle">Please Login.</span>
        </div>

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>Enter username and password.</span>
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control placeholder-no-fix" type="text" v-validate="'required'"
                   placeholder="Username" name="username" v-model="loginForm.username" value="${params?.username}" />
        </div>

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control placeholder-no-fix" type="password" v-validate="'required'"
                   placeholder="Password" name="password" v-model="loginForm.password" value="" />
        </div>

        <div class="form-actions">
            <button type="submit" class="btn red-flamingo btn-block uppercase">Login</button>
        </div>

        <g:if test="${flash.message}">
            <div class="alert alert-danger display-hide" style="display: block;">
                <button class="close" data-close="alert"></button>
                <span>${flash.message}</span>
            </div>
        </g:if>

    </g:form>
<!-- END LOGIN FORM -->

</div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/login/login.js" asset-defer="true"/>

</body>

</html>
