<!doctype html>
<html>
<head>

    <meta name="layout" content="login"/>
    <title><g:message code="default.login.label" default="Nhakr::Nhakr - Login"/></title>

</head>

<body>

<!-- BEGIN LOGIN -->
<div class="content" id="login-app">

    <!-- BEGIN LOGO -->
    <div class="logo">
        <div class="logo-container">
            <asset:image src="global/img/nhakr-app-icon.png" class="login-logo" alt="Nhakr" />
            <div class="logo-text-large">Nhakr</div>
        </div>
    </div>
    <!-- END LOGO -->

<!-- BEGIN LOGIN FORM -->
    <g:form class="login-form" method="POST" controller="login" action="authenticate" submit.prevent="authenticate">

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>Enter username and password.</span>
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control placeholder-no-fix" type="text" v-validate="'required'"
                   placeholder="Username..." name="username" v-model="username" value="${params?.username}" />
        </div>

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control placeholder-no-fix" type="password" v-validate="'required'"
                   placeholder="Password..." name="password" v-model="password" value="${params?.password}" />
        </div>

        <div class="form-actions">
            <button type="submit" class="btn white btn-block uppercase">Login</button>
        </div>

        <g:if test="${flash.message}">
            <div class="alert alert-danger display-hide" style="display: block;">
                <button class="close" data-close="alert"></button>
                <span>${flash.message}</span>
            </div>
        </g:if>

    </g:form>
<!-- END LOGIN FORM -->

</div>

</body>

</html>
