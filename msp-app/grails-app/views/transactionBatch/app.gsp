<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'transactionBatch.label', default: 'Transaction')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'transactionBatch.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <router-view></router-view>
    </div>
</template>

<template id="batch-list">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <batch-search v-on:search-request="searchBatches"></batch-search>

        <div class="inbox margin-top-10">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <bootstrap-table :data="transactionBatches" @on-click-batch="alert('hello')" :columns="table.columns" :options="table.options"></bootstrap-table>                </div>
            </div>
        </div>
    </section>
</template>

<template id="batch-show">
    <section>
        <h1>Hello there.</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Transaction Batch Details
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Id:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{transactionBatch.id}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Type:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{transactionBatch.transactionType.name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Billing Cycle:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{transactionBatch}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Status:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">Here Me Now !!!</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Run Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{transactionBatch.createdDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4">Value Date</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static">{{transactionBatch.createdDate | formatDate}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!--Transaction transactions.-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Transaction Transactions
                        </div>
                    </div>

                    <div class="portlet-body">
                        <!-- BEGIN FORM-- >
                        <routine-transactions :routine-request-id="id"></routine-transactions>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> <i class="fa fa-book"></i> Browse Transaction Batches
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>
    </div>
</template>

<template id="batch-search">
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet box yellow m-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-filter"></i> Search
                    </div>
                </div>

                <div class="portlet-body">
                    <form role="form" autocomplete="off" @submit.prevent="requestSearch">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Transaction Type</label>

                                        <select2 v-model="query.trantype" name="trantype" :select2="transactionTypeOptions">
                                            <option disabled value="0">Select one</option>
                                        </select2>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Batch Number</label>
                                        <input type="text" class="form-control" name="txtef" v-model="query.txref">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Date Created</label>
                                        <vdate-picker v-model="query.txndate" name="txDate" ></vdate-picker>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Credit Acc</label>
                                        <input type="text" class="form-control" name="cracc" v-model="query.cracc">
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="form-actions">
                            <button type="button" @click="requestSearch" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
                        </div>

                    </form>

                </div>
            </div>
            <!-- END Portlet PORTLET-->
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/transactionBatch/transaction-batch-app.js" asset-defer="true"/>

</body>
</html>