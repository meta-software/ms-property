<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="{message(code: 'leaseRenewalRequestCheck.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle"
           value="{message(code: 'leaseRenewalRequestTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/vue/vue-bootstrap-table.min.js" asset-defer="true"/>
<asset:javascript src="vue-apps/leaseRenewalRequest/lease-renewal-request-inbox.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="lease-renewal-request-inbox-list">
    <div class="search-page search-content-4 margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" placeholder="Query Lease..."  v-model="params.q">
                    </div>
                    <div class="col-lg-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                        <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-bordered table-striped table-hover table-condensed">
                <thead class="bg-grey-cararra">
                <tr>
                    <th nowrap="nowrap" style="width: 8%">Request #</th>
                    <th nowrap="nowrap">Lease No</th>
                    <th style="width: 22.5%">Action Reason</th>
                    <th nowrap="nowrap">Tenant</th>
                    <th nowrap="nowrap">Terminate Date</th>
                    <th nowrap="nowrap">Requested By</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(leaseRenewalRequest, key) in leaseRenewalRequestInboxes" id="key">
                    <td>{{leaseRenewalRequest.id}}</td>
                    <td><router-link :to="{path: '/show/'+leaseRenewalRequest.id}">{{leaseRenewalRequest.lease.leaseNumber}}</router-link></td>
                    <td>{{leaseRenewalRequest.entity.actionReason}}</td>
                    <td>{{leaseRenewalRequest.lease.tenant}}</td>
                    <td>{{leaseRenewalRequest.entity.actionDate | formatDate('DD, MMM YYYY')}}</td>
                    <td>{{leaseRenewalRequest.maker.username}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Lease Renewal Requests
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" to="/"> <i class="fa fa-list"></i> List Requests</router-link>
            </div>
        </div>
    </div>
</template>

<template id="lease-renewal-request-inbox-show">
    <section >
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div v-if="leaseRenewalRequestInbox" class="row">
            <div class="col-md-8">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box green-soft margin-top-10">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-chain"></i> Lease Renewal Request</div>
                    </div>

                    <div class="portlet-body">
                        <div class="portlet-body " style="padding: 0px 10px;">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width: 15%">Lease Number</th><td style="width: 35%">{{leaseRenewalRequestInbox.lease.leaseNumber}}</td>
                                    <th style="width: 15%">Lease Status</th><td style="width: 35%">{{leaseRenewalRequestInbox.lease.leaseStatus}}</td>
                                </tr>
                                <tr>
                                    <th>Action Reason</th><td colspan="3"><p>{{leaseRenewalRequestInbox.entity.actionReason}}</p></td>
                                </tr>
                                <tr>
                                    <th>Expire Date</th><td>{{leaseRenewalRequestInbox.entity.leaseExpireDate | formatDate('DD MMM, YYYY')}}</td>
                                    <th>Request Status</th><td>{{leaseRenewalRequestInbox.checkStatus}}</td>
                                </tr>
                                <tr>
                                    <th>Requested By</th><td>{{leaseRenewalRequestInbox.maker.fullName}}</td>
                                    <th>Processed ?</th><td>{{leaseRenewalRequestInbox.entity.processed}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->

                <maker-checker @approved="fetchLeaseRenewalRequest" @rejected="fetchLeaseRenewalRequest" :reload="false" :makerid="leaseRenewalRequestInbox.maker.id" controller="lease-renewal-request-inbox" :item-id="leaseRenewalRequestInbox.id" :status="leaseRenewalRequestInbox.checkStatus"></maker-checker>

                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box blue-hoki margin-top-10">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-list"></i> Request Response</div>
                    </div>

                    <div class="portlet-body">
                        <div class="portlet-body " style="padding: 0px 10px;">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th style="width: 15%">Response Date</th><td style="width: 35%">{{leaseRenewalRequestInbox.checkDate | formatDate('DD MMM, YYYY') }}</td>
                                    <th style="width: 15%">Responded By</th><td style="width: 35%">{{leaseRenewalRequestInbox.checker.fullName}}</td>
                                </tr>
                                <tr>
                                    <th>Response</th><td><p>{{leaseRenewalRequestInbox.checkComment}}</p></td>
                                    <th>Response Status</th><td>{{leaseRenewalRequestInbox.checkStatus}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->

            </div>

            <div class="col-md-4">
                <!-- BEGIN Portlet PORTLET-->
                <div class="portlet box grey-gallery margin-top-10">
                    <div class="portlet-title">
                        <div class="caption"> <i class="fa fa-sticky-note"></i> Lease Details</div>
                    </div>

                    <div class="portlet-body">
                        <div class="portlet-body" style="padding: 0px 10px;">
                            <table class="table">
                                <tbody>
                                <tr><th style="width: 40%">Lease Status</th><td>{{lease.leaseNumber}}</td></tr>
                                <tr><th>Tenant</th><td>{{lease.tenant}}</td></tr>
                                <tr><th>Property</th><td>{{lease.property}}</td></tr>
                                <tr><th>Rental Unit</th><td>{{lease.rentalUnit}}</td></tr>
                                <tr><th>Lease Status</th><td>{{lease.leaseStatus }}</td></tr>
                                <tr><th>Start Date</th><td>{{lease.leaseStartDate | formatDate('DD MMM, YYYY') }}</td></tr>
                                <tr><th>Expiry Date</th><td>{{lease.leaseExpiryDate | formatDate('DD MMM, YYYY') }}</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>
        </div>

        <div v-show="leaseRenewalRequestInbox == null">
            Loading...
        </div>
    </section>
</template>

</body>
</html>