<%@ page import="metasoft.property.core.PropertyManager" %>
<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.AccountType" %>

<!-- BEGIN Portlet PORTLET-->
<div class="portlet box yellow m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Property Name</label>
                            <input type="text" class="form-control" name="name" value="${params?.name}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Property Type</label>
                            <g:select optionKey="id"
                                      noSelection="['': 'All...']"
                                      class="form-control select2-no-search"
                                      name="propertyType"
                                      value="${params?.propertyType}"
                                      from="${PropertyType.list()}"/>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Property Manager</label>
                            <g:select optionKey="id"
                                      noSelection="['': 'All...']"
                                      class="form-control select2"
                                      placeholder="Property Manager"
                                      name="propertyManager"
                                      value="${params?.propertyManager}"
                                      from="${PropertyManager.list()}"/>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">City</label>
                            <input type="text" disabled="disabled" class="form-control disabled" name="city" value="">
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-actions">
                <button type="submit" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->