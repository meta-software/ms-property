<div class="actions entity-actions">
    <g:link action="create" controller="rentalUnit" params="['property.id': property.id]" class="btn btn-sm green" >
        <i class="fa fa-plus"></i>
        <span>Add Rental Unit</span>
    </g:link>

</div>
