<%@ page import="metasoft.property.core.Landlord" %>
<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.PropertyManager" %>

<!-- BEGIN FORM PORTLET-->
<div class="portlet white">
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-6">
                <g:if test="${property.landlord?.id == null}">
                    <div class="form-group">
                        <label class="control-label col-md-3">${message(code: 'label.property.name', default: 'Landlord Account')}
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:select name="landlord"
                                      noSelection="['': 'Select Landlord...']"
                                      from="${Landlord.list()}"
                                      value="${property?.landlord?.id}"
                                      class="form-control select2"
                                      optionKey="id"/>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <g:hiddenField name="landlord" value="${property.landlord.id}" />
                    <div class="form-group">
                        <label class="control-label col-md-3">${message(code: 'label.property.name', default: 'Landlord Account')}
                        </label>

                        <div class="col-md-9">
                            <input type="text" class="form-control" disabled="disabled"
                                   value="${property.landlord} -  ${property.landlord.accountNumber}"/>
                        </div>
                    </div>
                </g:else>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.name', default: 'Name')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" placeholder="" type="text" name="name"
                                 value="${this.property.name}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.propertyType', default: 'Property Type')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:select name="propertyType"
                                  noSelection="['': 'Select Type...']"
                                  from="${PropertyType.list()}"
                                  value="${property?.propertyType?.id}"
                                  class="form-control select2-no-search"
                                  optionKey="id"/>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.propertyManager', default: 'Property Manager')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:select name="propertyManager"
                                  noSelection="['': 'Select Manager...']"
                                  from="${PropertyManager.list()}"
                                  value="${property?.propertyManager?.id}"
                                  class="form-control select2"
                                  optionKey="id"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">Stand No
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field type="text" name="standNumber" class="form-control"
                                 value="${property.standNumber}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.address', default: 'Address')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textArea class="form-control" placeholder="" name="address"
                                    value="${this.property.address}"
                                    rows="2"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        ${message(code: 'label.property.description', default: 'Description')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:textArea class="form-control" placeholder="" name="description"
                                    value="${this.property.description}" rows="2"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.city', default: 'City')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <g:field class="form-control" placeholder="" type="text" name="city"
                                 value="${this.property.city}"/>

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.commission', default: 'Commission')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-inline input-medium">
                            <div class="input-group">
                                <g:field class="form-control" placeholder="" type="text"
                                         name="commission"
                                         value="${this.property.commission}"/>
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">${message(code: 'label.property.area', default: 'Area')}
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-inline input-large">
                            <div class="input-group">
                                <g:field class="form-control" placeholder="" type="text"
                                         name="area"
                                         value="${this.property.area}"/>
                                <span class="input-group-addon">
                                    M<sup>2</sup>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-3">
                        VAT Chargeable
                    </label>

                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <g:checkBox class="form-control icheck"
                                        name="active"
                                        id="chargeVat"
                                        checked="${property.chargeVat}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${property.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>
    </div>
</div>
<!-- END FORM PORTLET-->
