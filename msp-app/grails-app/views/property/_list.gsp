<%@ page import="metasoft.property.core.PropertyType" %>

<div class="search-page search-content-4">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
        <div class="row">
            <div class="col-lg-3">
                <input type="text" name="name" value="${params?.name}" class="form-control" placeholder="Property Name...">
            </div>
            <div class="col-lg-2">
                <g:select optionKey="id"
                          placeholder="Property Type"
                          noSelection="['': 'All...']"
                          class="form-control select2-no-search"
                          name="propertyType"
                          value="${params?.propertyType}"
                          from="${propertyTypeList}"/>
            </div>
            <div class="col-lg-2 extra-buttons">
                <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                <a></a>
                <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
            </div>
        </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-hover table-striped table-bordered">
            <thead class="bg-grey-steel">
            <tr>
                <th>Name</th>
                <th style="width: 10%;">City</th>
                <th style="width: 20%;">Landlord</th>
                <th style="width: 10%; white-space: nowrap;">Property Type</th>
                <th style="width: 8%;">Area</th>
                <th style="width: 5%;">Commission</th>
                <th style="width: 5%;">VAT?</th>
                <th style="width: 5%; text-align: center;">Status</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${propertyList}">
                <g:each in="${propertyList}" var="property">
                    <tr class="mc-${property.checkStatus}">
                        <td>
                            <g:link controller="property" action="show" resource="${property}" style="display: block;">${property}</g:link>
                        </td>
                        <td>${property?.address?.city}</td>
                        <td>
                            <g:link controller="landlord" action="show" id="${property.landlord.id}" style="display: block;" >${property.landlord}</g:link>
                        </td>
                        <td>${property.propertyType}</td>
                        <td><g:formatNumber number="${property.area}" format="###,###.0" /> m<sup>2</sup></td>
                        <td>${property.commission} %</td>
                        <td><mp:boolStatusLabel status="${property.chargeVat}" /></td>
                        <td>
                            <mp:makerCheck value="${property.dirtyStatus}" />
                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="8">
                        <mp:noRecordsFound />
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${propertyCount ?: 0}" params="${params}" />
    </div>
</div>
