<!DOCTYPE html>
<html>
    <head>
        <!-- BEGIN TEMPALTE VARS -->
        <g:set var="entityName" value="${message(code: 'property.properties.label', default: 'Property Rental Units')}" />
        <g:set var="pageTitle" value="${message(code: 'default.show.label', default: entityName, args: [entityName])}" scope="request" />
        <!-- END TEMPALTE VARS -->

        <meta name="layout" content="main"/>
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>

    <body>

        <g:render template="rental_unit_actions" />

        <div class="row">
            <div class="col-md-12">

                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <div class="scrollable" style="background-color: #ffffff;">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Area</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <g:if test="${property.rentalUnits}">
                        <g:each in="${property.rentalUnits}" var="rentalUnit">
                        <tr>
                            <td>
                                <g:link controller="rentalUnit" action="show" resource="${rentalUnit}">${rentalUnit.name}</g:link>
                            </td>
                            <td><g:formatNumber number="${rentalUnit.area}" format="###,###" /> sqm</td>
                            <td>
                                <div action="rentalUnit" class="actions">
                                    <g:link action="edit" resource="${rentalUnit}" >
                                        <i class="fa fa-edit"></i>
                                        <span>Edit</span>
                                    </g:link>
                                </div>
                            </td>
                        </tr>
                        </g:each>
                        </g:if>
                        <g:else>
                        <tr>
                            <td colspan="3">
                                No Rental Units to show
                            </td>
                        </tr>
                        </g:else>
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    <g:paginate total="${rentalUnitsListCount ?: 0}" />
                </div>

            </div>
        </div>

    </body>
</html>