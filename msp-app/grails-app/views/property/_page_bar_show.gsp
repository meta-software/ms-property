<div class="page-bar-show">
    <div class="page-bar-show-title">
        <div class="page-bar-show-header">
            <h4 class="page-bar-title">
                <i class="fa fa-user"></i>
                Property : ${property.name}
            </h4>
            <div class="page-bar-show-subtitle">
                <mp:makerCheck value="${property.dirtyStatus}" />
            </div>
        </div>
        <div class="page-toolbar">
            <div class="page-toolbar-context">
                <g:if test="${property.isChecked()}">
                    <mp:editRecord bean="${property}" controller="property" action="edit" id="${property.id}" class="btn btn-sm blue-hoki" >
                        <i class="fa fa-edit"></i> Edit Record
                    </mp:editRecord>
                </g:if>
            </div>

            <g:link controller="property" class="btn btn-sm default" ><i class="fa fa-list"></i> Properties</g:link>
        </div>
    </div>

    <!--div class="page-bar-show-actions">
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Edit</a>
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Manage Properties</a>
    </div-->
</div>
