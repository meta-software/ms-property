<template id="rental-unit-list">
    <div class="portlet">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-building-o"></i> Rental Units</div>

            <div class="tools">
                <a href="" class="fullscreen" data-original-title="" title=""> </a>
            </div>
        </div>
        <div class="portlet-body form">
            <!------------------------------- -->
            <div class="search-page search-content-4 margin-top-10">
                <div class="search-bar bordered">
                    <form role="form" autocomplete="off" @submit.prevent="search">

                        <div class="row">
                            <div class="col-md-3 col-lg-3">
                                <input type="text" name="query" class="form-control" v-model="queryParams.query" placeholder="Rental Unit...">
                            </div>
                            <div class="col-md-3 col-lg-3">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" v-model="queryParams.isVacant" true-value="yes" false-value="no" name="isVacant" value="yes" />
                                            Vacant ?
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2 extra-buttons">
                                <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                                <button class="btn green" type="button" @click="refresh"><i class="fa fa-refresh"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="search-table table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 20%;">Reference</th>
                            <th style="width: 25%;">Name</th>
                            <th style="width:  8%;">Area</th>
                            <th style="width: 10%;">Occupation</th>
                            <th style="width: 35%;">Occupied By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="rentalUnit, idx in rentalUnits" :key="idx">
                            <td><input type="checkbox" disabled="disabled" class="icheckbox_flat-orange"/></td>
                            <td><a class="display-block" :href="href('/rental-unit/show/'+rentalUnit.id)">{{rentalUnit.unitReference}}</a></td>
                            <td>{{rentalUnit.name}}</td>
                            <td>{{rentalUnit.area | formatNumber(2)}} M<sup>2</sup></td>
                            <td><bool-label :bool-value="rentalUnit.isOccupied" :show-icon="true" :bool-text="{trueValue: 'Occupied', falseValue: 'Vacant'}"></bool-label></td>
                            <td>{{rentalUnit?.occupant.name == null ? "N/A" : rentalUnit?.occupant.name }}</td>
                        </tr>
                        <tr v-if="rentalUnits.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--------------------------------->
        </div>
    </div>
</template>
