<!DOCTYPE html>
<html>
    <head>
        <!-- BEGIN TEMPALTE VARS -->
        <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}" />
        <g:set var="pageHeadTitle" value="${message(code: 'property.list.label', default: entityName, args: [entityName])}" scope="request" />
        <!-- END TEMPALTE VARS -->

        <meta name="layout" content="main"/>
        <title><g:message code="default.list.label" args="[entityName]" /></title>

    </head>

    <body>
    <div id="main-app" >
        <g:render template="page_bar" />

        <div class="row margin-top-10">
            <div class="col-md-12">

                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <g:render template="list" />

            </div>
        </div>

    </div>

    <asset:javascript src="application-vue.js" asset-defer="true"/>
    <asset:javascript src="vue-apps/property/property-index.js" asset-defer="true"/>

    </body>
</html>