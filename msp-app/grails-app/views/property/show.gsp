<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle" value="Property Info - ${property.name}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let propertyId = ${property.id};
    </script>

</head>

<body>

<div id="main-app" v-cloak>
    <g:render template="page_bar_show"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="entity-details">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="portlet light bg-inverse">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Property Details
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form class="horizontal-form" role="form" style="min-height: 215px">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Name: </label>
                                        <div class="form-control-static">${property.name}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Landlord: </label>
                                        <div class="form-control-static"><g:link class="display-block" controller="landlord" action="show" id="${property.landlord.id}">${property.landlord}</g:link></div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Area: </label>
                                        <div class="form-control-static"><g:formatNumber number="${property.area}" format="##,###.0#" /> M<sup>2</sup></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Property Type: </label>
                                        <div class="form-control-static">${property.propertyType.name}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Manager: </label>
                                        <div class="form-control-static">${property.propertyManager}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Commission: </label>
                                        <div class="form-control-static"><g:formatNumber number="${property.commission}" type="number" maxFractionDigits="2" roundingMode="HALF_DOWN" maxIntegerDigits="3"  /> %</div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Address: </label>
                                        <div class="form-control-static"><mp:address address="${property.address}" /></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Check Status: </label>
                                        <div class="form-control-static"><mp:makerCheck value="${property.dirtyStatus}" /></div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                        </form>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-line-chart"></i> Property Summary
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form class="horizontal-form" role="form" style="height: 215px;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Rental Units: </label>
                                        <div class="form-control-static">${occupancyStats['totalUnits']}</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Occupied Area: </label>
                                        <div class="form-control-static"><g:formatNumber number="${occupancyStats['occupiedArea']}" format="##,###.0#" /> M<sup>2</sup></div>
                                    </div>
                                </div>

                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Occupancy: </label>
                                        <div class="form-control-static"><g:formatNumber number="${occupancyStats['occupancyRatePerc']}" format="##,##0.0#" /> %</div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Balances: </label>
                                    <div class="form-control-static">$ <g:formatNumber number="0.00" format="##,##0.0#" /></div>
                                </div>
                            </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Vacant Units: </label>
                                        <div class="form-control-static"><g:formatNumber number="${occupancyStats['vacantUnits']}" format="##,##0" /></div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <rental-unit-list :property-id="${property.id}"></rental-unit-list>
        </div>
    </div>

</div>

<g:render template="rentalUnits" model="['rentalUnitList': rentalUnitList ]"/>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/property/property-show.js" asset-defer="true" />

</body>
</html>
