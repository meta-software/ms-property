<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'property.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'default.create.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>

</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.property}">
    <g:render template="/templates/errors" model="['obj': this.property]"/>
</g:hasErrors>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Property Form</div>
            </div>

            <div class="portlet-body">
                <g:form action="save" method="POST" class="content scaffold-create form-horizontal" name="property-form">
                    <g:render template="form" bean="property"/>
                </g:form>
            </div>
        </div>
    </div>
</div>

<asset:javascript src="property-form-wizard.js" asset-defer="true"/>

</body>
</html>

