<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="property" action="create" class="btn blue-hoki btn-sm">
            <i class="fa fa-plus"></i> Add Property
        </g:link>
        <g:link controller="property" action="index" class="btn default btn-sm">
            <i class="fa fa-list"></i> Properties
        </g:link>
        <!--property-form></property-form-->
    </div>
</div>