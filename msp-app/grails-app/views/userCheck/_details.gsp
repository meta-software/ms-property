<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                ID:
            </label>
            <div class="col-md-8">${secUserCheck.id}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Status:
            </label>
            <div class="col-md-8"><mp:makerCheck value="${secUserCheck.checkStatus}" ></mp:makerCheck></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Action:
            </label>
            <div class="col-md-8">${secUserCheck.actionName}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Entity:
            </label>
            <div class="col-md-8">${secUserCheck.username}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Date Created:
            </label>
            <div class="col-md-8"><g:formatDate date="${secUserCheck.makeDate}" format="dd MMM, yyyy" /></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-4">
                Maker:
            </label>
            <div class="col-md-8">${secUserCheck.maker.fullName}</div>
        </div>
    </div>
</div>

