<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Date Created</th>
        <th>Status</th>
        <th>Type</th>
        <th>Entity</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${accountCheckList}">
        <g:each in="${accountCheckList}" var="accountCheck">
            <tr>
                <td><g:link class="display-block" url="/account-check/outbox/item/${accountCheck.id}" >
                    ${accountCheck.id}
                </g:link>
                </td>
                <td><g:formatDate date="${accountCheck.makeDate}" /></td>
                <td><mp:makerCheck value="${accountCheck.checkStatus}">${accountCheck.checkStatus}</mp:makerCheck></td>
                <td>${accountCheck.accountCategory}</td>
                <td>
                    <g:link class="display-block" action="show" id="${accountCheck.id}" >${accountCheck.entity}</g:link>
                </td>
                <td>${accountCheck.actionName}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="7">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
