<!-- BEGIN PAGE BASE CONTENT -->
<div class="portlet-body form">
    <!-- BEGIN FORM-->
    <form class="horizontal-form" role="form">
        <div class="form-body">
            <div class="row">
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Username:</label>
                        <div class="form-control-static">${secUser.username}</div>
                    </div>
                </div>
                <!--/span-->
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Full Name:</label>
                        <div class="form-control-static">${secUser.fullName}</div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Email Address:</label>
                        <div class="form-control-static">${secUser.emailAddress}</div>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">User Type:</label>
                        <div class="form-control-static">${secUser.userType.name}</div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Employee Id:</label>
                        <div class="form-control-static">${secUser.employeeId}</div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Status:</label>
                        <div class="form-control-static"><mp:makerCheck value="${secUserCheck.checkStatus}" ></mp:makerCheck></div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <!--/row-->
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Enabled?:</label>
                        <div class="form-control-static"><mp:boolStatusLabel status="${secUser.enabled}" /></div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Expired ?:</label>
                        <div class="form-control-static"><mp:boolStatusLabel status="${secUser.accountExpired}" /></div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Locked ?:</label>
                        <div class="form-control-static"><mp:boolStatusLabel status="${secUser.accountLocked}" /></div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

            <h4 class="caption">User Roles</h4>
            <div class="row">
                <g:each in="${secRoles}" var="role" >
                    <div class="col-md-4">
                        <span>
                            <i class="fa fa-check"></i>
                            ${role.name}
                        </span>
                    </div>
                </g:each>
                <g:if test="${secRoles?.isEmpty()}">
                    <div class="col-md-12">
                        <mp:noRecordsFound/>
                    </div>
                </g:if>
            </div>
        </div>
    </form>
    <!-- END FORM-->
</div>
<!-- END PAGE BASE CONTENT -->
