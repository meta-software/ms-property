<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'secUserCheck.label', default: 'Viewing : User')}"/>
    <g:set var="pageHeadTitle" value="Viewing Task : User - ${secUserCheck.username}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let checkId = ${secUserCheck.id};
    </script>

</head>

<body>

<!-- BEGIN PAGE BASE CONTENT -->
<div id="app">

    <g:render template="page_bar_show"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bg-inverse entity-details">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> View Pending Task : User
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="details" />
                </div>

            </div>
        </div>
    </div>

    <maker-checker makerid="${secUserCheck.maker.id}" :item-id="${secUserCheck.id}" controller="user-check" status="${secUserCheck.checkStatus}"></maker-checker>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="portlet light bordered entity-details">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Viewing Task : User
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="secUser" bean="${secUser}" var="secUser" />
                </div>

            </div>
        </div>
    </div>

</div>
<!-- END PAGE BASE CONTENT -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/secUserCheck/sec-user-check-show.js" asset-defer="true" />

</body>
</html>