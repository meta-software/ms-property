<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" placeholder="User Query..." class="form-control" name="q" value="${params?.q}" />
                </div>
                <div class="col-lg-2 col-md-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Username</th>
                <th>Date Created</th>
                <th>Status</th>
                <th>Action Name</th>
                <th>Created By</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${secUserCheckList}">
                <g:each in="${secUserCheckList}" var="secUserCheck">
                    <tr>
                        <td>
                            <g:link class="display-block" action="show" id="${secUserCheck.id}" >${secUserCheck.username}</g:link>
                        </td>
                        <td><g:formatDate date="${secUserCheck.makeDate}" /></td>
                        <td><mp:makerCheck value="${secUserCheck.checkStatus}">${secUserCheck.checkStatus}</mp:makerCheck></td>
                        <td>${secUserCheck.actionName}</td>
                        <td>${secUserCheck.maker.username}</td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="5">
                        <mp:noRecordsFound />
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${userCheckCount ?: 0}" />
    </div>
</div>
