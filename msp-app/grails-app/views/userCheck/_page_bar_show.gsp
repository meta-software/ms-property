<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar actions" style="left-margin: 8px;">
        <g:link controller="user-check" type="button" class="btn btn-sm btn-default btn-outline">
            <i class="fa fa-list"></i> List Users
        </g:link>
    </div>
</div>