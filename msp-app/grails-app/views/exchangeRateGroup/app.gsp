<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'exchangeRateGroup.label', default: 'Exchange Rate Group')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'exchangeRateGroup.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="exchangeRateGroup.list.label" args="[entityName]"/></title>
</head>

<body>

<!-- begin: app -->
<div id="app"></div>

<!-- begin: general-ledgerg-app -->
<template id="erg-app">
    <section>
        <page-bar></page-bar>

        <router-view></router-view>
    </section>
</template>
<!-- begin: general-ledgerg-app -->

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-dollar"></i> Exchange Rates
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <router-link class="btn btn-sm default" to="/">
                <i class="fa fa-list"></i> Exchange Rates
            </router-link>
            <button type="button" @click="createRateGroup" class="btn btn-sm blue-hoki" >
                <i class="fa fa-plus"></i> Create Exchange Rates
            </button>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<!-- begin: general-ledgerg-approved template -->
<template id="erg-list">
    <div class="row">
        <div class="col-md-7 col-lg-7">
            <div class="search-page search-content-4 margin-top-10">
                <loading :active.sync="loader.isLoading"
                         :is-full-page="loader.fullPage"></loading>
                <div class="search-bar bordered">
                    <form role="form" autocomplete="off" @submit.prevent="search">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <input type="text" v-model="params.query" class="form-control" placeholder="Exchange Rate Group...." >
                            </div>
                            <div class="col-lg-3 col-md-3 col-lg-offset-3 extra-buttons">
                                <button class="btn grey-steel uppercase bold" type="submit" @click.prevent="search">Search</button>
                                <button class="btn green" type="button"><i class="fa fa-refresh" @click="reset"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="search-container">
                    <table class="table table-bordered table-condensed table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="width: 10%;" class="text-nowrap">Group ID#</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Active</th>
                            <th style="width: 10%;" class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(exchangeRateGroup, key) in exchangeRateGroups" :key="key" :class="{'active-rate': exchangeRateGroup.active}">
                            <td><a href="#" @click.stop="selectExchangeRateGroupId(exchangeRateGroup)" class="display-block">{{exchangeRateGroup.id}}</a></td>
                            <td class="text-nowrap">{{exchangeRateGroup.startDate | formatDate}}</td>
                            <td class="text-nowrap">{{exchangeRateGroup.endDate | formatDate}}</td>
                            <td><bool-label :bool-value="exchangeRateGroup.active"></bool-label></td>
                            <td>
                                <button type="button" class="btn btn-xs btn-danger" @click="deleteExchangeRateGroup(exchangeRateGroup.id)"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                        <tr v-if="exchangeRateGroups.length===0">
                            <td colspan="4"><mp:noRecordsFound/></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-lg-5">
            <exchange-rate-preview class="margin-top-10" :group-id="selectedExchangeRateGroupId" ></exchange-rate-preview>
        </div>
    </div>

</template>
<!-- end: general-ledger-approved template -->

<!-- begin show -->
<template id="erg-show">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-md-3">Start Date:</label>
                <div class="col-md-9">
                    <input disabled="disabled" :value="exchangeRateGroup.startDate | formatDate('DD MMM YYYY')" class="form-control" />
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-md-3">End Date:</label>

                <div class="col-md-9">
                    <input disabled="disabled" :value="exchangeRateGroup.endDate | formatDate('DD MMM YYYY')"  class="form-control" />
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label col-md-3">Active:</label>

                <div class="col-md-9">
                    <input type="checkbox" v-model="exchangeRateGroup.active" value="1" />
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Exchange Rate Details
                    </div>

                    <div class="actions">
                        <button type="button" class="btn btn-danger btn-sm" @click="$router.go(-1)"><i class="fa fa-arrow-left"></i> Back</button>
                    </div>
                </div>

                <div class="portlet-body form">
                    <div class="form-body">
                        <div class="row">

                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Name:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{exchangeRateGroup.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Start Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{exchangeRateGroup.startDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        End Date:
                                    </label>
                                    <div class="col-md-8 col-xs-8">{{exchangeRateGroup.endDate | formatDate}}</div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Active?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[exchangeRateGroup.active ? 'label-success' : 'label-danger']" >{{exchangeRateGroup.active | formatBoolean}}</span></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-4 col-xs-4">
                                        Is Open?:
                                    </label>
                                    <div class="col-md-8 col-xs-8"><span class="label label" :class="[exchangeRateGroup.open ? 'label-success' : 'label-danger']" >{{exchangeRateGroup.open | formatBoolean}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="button" class="btn btn-sm red-mint" @click="close"><i class="fa fa-close"></i> Close Rate</button>
                        <button type="button" class="btn btn-sm btn-success" @click="open"><i class="fa fa-folderg-open"></i> Open Rate</button>
                        <button type="button" class="btn btn-sm green-dark" @click="activate"><i class="fa fa-check"></i> Activate Rate</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
</template>
<!-- end show-->

<!-- erg-create -->
<template id="erg-create">
    <div class="portlet light bordered margin-top-10 md-shadow-z-3">
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-plus"></i> Exchange Rates Form</div>
        </div>

        <div class="portlet-body form">
            <form role="form" @submit.prevent="save" action="javascript:;">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Start Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <datepicker placeholder="YYYY-MM-DD" v-validate="'required'" v-model="exchangeRateGroup.startDate" name="startDate" class="form-control"></datepicker>
                                <span class="has-error help-block-error help-block">{{ errors.first('startDate') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">End Date:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <datepicker placeholder="YYYY-MM-DD" v-validate="'required'" v-model="exchangeRateGroup.endDate" name="endDate" class="form-control"></datepicker>
                                <span class="has-error help-block-error help-block">{{ errors.first('endDate') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Active ?:
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input style="width: 1.85rem; height: 1.85rem;" type="checkbox" v-model="exchangeRateGroup.active" value="1" name="active"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-list"></i> Exchange Rates</div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-bordered table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 40%;">From</th>
                                        <th style="width: 40%;">To</th>
                                        <th style="width: 20%;">Rate</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <template v-for="(exchangeRate, index) in exchangeRateGroup.exchangeRates">
                                        <er-form name="exRate" v-model="exchangeRateGroup.exchangeRates[index]"></er-form>
                                    </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <button type="button" class="btn btn-sm blue-hoki" @click="save"><i class="fa fa-save"></i> Save</button>
                    <button type="button" class="btn btn-sm default" @click="cancel"><i class="fa fa-times"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
</template>
<!-- erg-create -->

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/exchangeRateGroup/exchange-rate-group-app.js" asset-defer="true"/>

</body>
</html>