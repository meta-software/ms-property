<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Metafost Property :: [Real Estate management] - Welcome</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Welcome :: MetasoftWare" scope="request"/>
    <g:set var="pageSubTitle" value="version 1.0-SNAPSHOT" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<div class="row">
    <div class="col-md-12">
        <div class="note note-info">
            <p>Introduction to the Metasoft Properties :: Real Estate Property Management Application</p>
        </div>

    </div>
</div>

</body>
</html>
