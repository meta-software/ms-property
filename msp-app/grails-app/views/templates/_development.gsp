<g:if test="${grails.util.Environment.current == grails.util.Environment.DEVELOPMENT}">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed table-striped">
                <tbody>
                <tr>
                    <td>Something here.</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</g:if>
