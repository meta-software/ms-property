<div class="page-bar">
    <g:if test="${pageHeadTitle}">
    <div class="page-bar-header">
        <h4>${pageHeadTitle}
            <g:if test="${pageHeadSubTitle}">
                <small>${pageHeadSubTitle}</small>
            </g:if>
        </h4>
    </div>
    </g:if>

    <div class="page-toolbar">
        <div class="actions">
            <g:link controller="landlord" action="index" class="btn btn-default btn-sm btn-outline ">
                <i class="fa fa-list"></i> Landlords
            </g:link>
            <g:link controller="landlord" action="create" class="btn green btn-sm btn-outline ">
                <i class="fa fa-plus"></i> Add Landlord
            </g:link>
        </div>
    </div>
</div>