<div class="row">
    <div class="col-md-12">
        <g:if test="${flash.error}">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                ${flash.error}
            </div>
        </g:if>
        <g:if test="${flash.warning}">
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                ${flash.warning}
            </div>
        </g:if>
        <g:else>
            <div class="alert alert-${flash.messageType ?: "info"} alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                ${flash.message}
            </div>
        </g:else>
    </div>
</div>
