<div class="row">
    <div class="col-md-12">
        <div class="alert alert-error alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>

            <ul class="errors" role="alert">
                <g:eachError bean="${obj}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </div>
    </div>
</div>
