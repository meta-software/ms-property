<div class="top-menu">
    <ul class="nav navbar-nav pull-right">
        <li class="separator hide"></li>
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
        <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
        <!--
        <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true">
                <i class="icon-bell"></i>
                <span class="badge badge-success">2</span>
            </a>
            <ul class="dropdown-menu">
                <li class="external">
                    <h3><span class="bold">2</span> Notifications</h3>
                </li>
                <li>
                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                        <li>
                            <a href="javascript:;">
                                <span class="details">
                                    <span class="label label-sm label-icon label-success">
                                        <i class="fa fa-plus"></i>
                                    </span> New user registered.</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;">
                                <span class="details">
                                    <span class="label label-sm label-icon label-danger">
                                        <i class="fa fa-book"></i>
                                    </span>Leases about to Expire</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        -->
        <!-- END NOTIFICATION DROPDOWN -->
        <!-- BEGIN USER LOGIN DROPDOWN -->
        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
        <li class="dropdown dropdown-user dropdown-dark">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
               data-close-others="true">
                <span class="username username-hide-on-mobile"><sec:ifLoggedIn><sec:loggedInUserInfo field="fullName" /></sec:ifLoggedIn></span>
                <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                <asset:image alt="${sec.loggedInUserInfo(field: 'username')}" class="img-rounded" src="layouts/layout/img/avatar.png"/>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <g:link controller="profile" action="show">
                        <i class="icon-user"></i> My Profile
                    </g:link>
                </li>
                <li class="divider"></li>
                <li>
                    <g:link controller="logout">
                        <i class="icon-key"></i> Log Out
                    </g:link>
                </li>
            </ul>
        </li>
        <!-- END USER LOGIN DROPDOWN -->
    </ul>
</div>