<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start ${mp.activeController(controllerName: ["home","dashboard"])}">
                <g:link controller="dashboard" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Home</span>
                    <mp:navItemSelected controllerName="home"/>
                </g:link>
            </li>

            <li class="heading">
                <h3 class="uppercase">General</h3>
            </li>

            <sec:ifAnyGranted roles="ROLE_PROP_MANAGER">
                <li class="nav-item ${mp.activeController(controllerName: ['routine-request-check-inbox', 'bulk-adjust-inbox', 'account-check-inbox', 'lease-check-inbox', 'property-check-inbox', 'transaction-batch-check', 'receipt-check', 'lease-renewal-request-inbox','lease-terminate-request-inbox', 'lease-renewal'])} ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-inbox"></i>
                        <span class="title">Checker Inbox </span>
                        <mp:navItemSelected
                                controllerName="['routine-request-check-inbox','transaction-batch-check', 'receipt-check', 'bulk-adjust-inbox', 'account-check-inbox', 'property-check-inbox', 'lease-check-inbox', 'lease-renewal-request-inbox','lease-terminate-request', 'lease-renewal']"/>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item ${mp.activeController(controllerName: "transaction-batch-check")}">
                            <g:link controller="transaction-batch-check" action="app" class="nav-link ">
                                <i class="fa fa-users"></i>
                                <span class="title">Transaction Batch</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "receipt-check")}">
                            <g:link controller="receipt-check" action="app" class="nav-link ">
                                <i class="fa fa-inbox"></i>
                                <span class="title">Receipt Batch</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "account-check-inbox")}">
                            <g:link controller="account-check-inbox" class="nav-link ">
                                <i class="fa fa-users"></i>
                                <span class="title">Account</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "property-check-inbox")}">
                            <g:link controller="property-check-inbox" action="app" class="nav-link ">
                                <i class="fa fa-building-o"></i>
                                <span class="title">Property</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "lease-check-inbox")}">
                            <g:link controller="lease-check-inbox" class="nav-link ">
                                <i class="fa fa-sticky-note-o"></i>
                                <span class="title">Lease</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "bulk-adjust-inbox")}">
                            <g:link controller="bulk-adjust-inbox" class="nav-link ">
                                <i class="fa fa-adjust"></i>
                                <span class="title">Bulk Adjust</span>
                            </g:link>
                        </li>

                        <li class="nav-item ${mp.activeController(controllerName: ['lease-terminate-request-inbox'])}">
                            <g:link controller="leaseTerminateRequestInbox" action="app" class="nav-link">
                                <span class="title">Lease Terminations</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: ['lease-renewal-request-inbox'])}">
                            <g:link controller="leaseRenewalRequestInbox" action="app" class="nav-link">
                                <span class="title">Lease Renewals</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: ['routine-request-check-inbox'])}">
                            <g:link controller="routine-request-check-inbox" action="app" class="nav-link">
                                <span class="title">Routine Requests</span>
                            </g:link>
                        </li>
                    </ul>

                </li>
            </sec:ifAnyGranted>

            <sec:ifAnyGranted roles="ROLE_PROP_MANAGER,ROLE_PROP_USER">
                <li class="nav-item ${mp.activeController(controllerName: ['transaction-batch-outbox', 'property-check-outbox', 'bulk-adjust-outbox','account-check-outbox', 'lease-check-outbox'])} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-envelope-o"></i>
                    <span class="title">Maker Outbox</span>
                    <mp:navItemSelected
                            controllerName="['bulk-adjust-outbox','account-check-outbox', 'property-check-outbox', 'lease-check-outbox']"/>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ${mp.activeController(controllerName: "transaction-batch-outbox")}">
                        <g:link controller="transaction-batch-outbox" action="app" class="nav-link ">
                            <i class="fa fa-users"></i>
                            <span class="title">Transaction Batch</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: "account-check-outbox")}">
                        <g:link controller="account-check-outbox" class="nav-link ">
                            <i class="fa fa-users"></i>
                            <span class="title">Account</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: "property-check-outbox")}">
                        <g:link controller="property-check-outbox" action="app" class="nav-link ">
                            <i class="fa fa-building-o"></i>
                            <span class="title">Property</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: "lease-check-outbox")}">
                        <g:link controller="lease-check-outbox" class="nav-link ">
                            <i class="fa fa-sticky-note-o"></i>
                            <span class="title">Lease</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: "bulk-adjust-outbox")}">
                        <g:link controller="bulk-adjust-outbox" class="nav-link ">
                            <i class="fa fa-adjust"></i>
                            <span class="title">Bulk Adjust</span>
                        </g:link>
                    </li>
                </ul>

            </li>
            </sec:ifAnyGranted>

            <sec:ifAnyGranted roles="ROLE_PROP_MANAGER,ROLE_PROP_USER">
                <li class="nav-item ${mp.activeController(controllerName: ['lease-review', 'property-tmp', 'bank', 'rental-unit', 'lease', 'provider', 'landlord', 'property', 'tenant', 'trust-account','general-ledger','property-manager','property-type', 'property-type','account-type','gl-bank-account'])} ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-bulb"></i>
                        <span class="title">Master Data</span>
                        <span class="arrow ${mp.activeController(controllerName: ['lease-review', 'property-tmp', 'bank', 'rental-unit', 'lease', 'provider', 'landlord', 'property', 'tenant', 'trust-account','general-ledger','property-manager','property-type', 'property-type','account-type','gl-bank-account'])}"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item ${mp.activeController(controllerName: "landlord")}">
                            <g:link controller="landlord" class="nav-link ">
                                <i class="fa fa-user-times"></i>
                                <span class="title">Landlord</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: ['property'])}">
                            <g:link controller="property" class="nav-link ">
                                <i class="fa fa-building-o"></i>
                                <span class="title">Property</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: ['property-tmp'])}">
                            <g:link controller="property-tmp" class="nav-link ">
                                <i class="fa fa-building"></i>
                                <span class="title">Pending Properties</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: ['rental-unit'])}">
                            <g:link controller="rental-unit" class="nav-link ">
                                <i class="fa fa-building"></i>
                                <span class="title">Rental Units</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "tenant")}">
                            <g:link controller="tenant" class="nav-link ">
                                <i class="fa fa-user"></i>
                                <span class="title">Tenant</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: ['lease'])} ">
                            <g:link controller="lease" class="nav-link ">
                                <i class="fa fa-sticky-note"></i>
                                <span class="title">Lease</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "lease-review")}">
                            <g:link controller="lease-review" action="app" class="nav-link ">
                                <i class="fa fa-book"></i>
                                <span class="title">Lease Review</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "provider")}">
                            <g:link controller="provider" class="nav-link ">
                                <i class="fa fa-user"></i>
                                <span class="title">Supplier</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "general-ledger")}">
                            <g:link controller="generalLedger" class="nav-link" action="app">
                                <span class="title">General Ledger</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "gl-bank-account")}">
                            <g:link controller="glBankAccount" class="nav-link" action="app">
                                <span class="title">GL Bank Accounts</span>
                            </g:link>
                        </li>
                        <!--
                        <li class="nav-item ${mp.activeController(controllerName: "account-type")}">
                            <g:link controller="account-type" class="nav-link ">
                                <span class="title">Account Type</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "property-type")}">
                            <g:link controller="property-type" class="nav-link ">
                                <span class="title">Property Type</span>
                            </g:link>
                        </li> -->
                        <li class="nav-item ${mp.activeController(controllerName: "bank")}">
                            <g:link controller="bank" class="nav-link ">
                                <i class="fa fa-bank"></i>
                                <span class="title">Bank</span>
                            </g:link>
                        </li>
                        <li class="nav-item ${mp.activeController(controllerName: "property-manager")}">
                            <g:link controller="property-manager" class="nav-link ">
                                <span class="title">Property Manager</span>
                            </g:link>
                        </li>
                    </ul>
                </li>
            </sec:ifAnyGranted>

            <sec:ifAnyGranted roles="ROLE_PROP_MANAGER,ROLE_PROP_USER">
            <!-- start accounts -->
            <li class="nav-item ${mp.activeController(controllerName: ['pending-receipt', 'receipt-batch-tmp', 'main-transaction', 'billing-invoice', 'flexcube-transaction', 'bulk-invoice', 'bulk-adjust', 'account', 'transaction-batch-tmp', 'invoice'])} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-money"></i>
                    <span class="title">Accounting</span>
                    <span class="arrow ${mp.activeController(controllerName: ['pending-receipt', 'receipt-batch-tmp', 'main-transaction', 'flexcube-transaction', 'bulk-invoice', 'bulk-adjust', 'account', 'transaction-batch-tmp', 'invoice'])}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ${mp.activeController(controllerName: ['transaction-batch-tmp'])} ">
                        <g:link controller="transactions" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-credit-card"></i> Transactions</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['receipt-batch-tmp'])} ">
                        <g:link controller="receipt-batch-tmp" class="nav-link ">
                            <span class="title"><i class="fa fa-credit-card"></i> Receipting</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['pending-receipt'])} ">
                        <g:link controller="pending-receipt" class="nav-link ">
                            <span class="title"><i class="fa fa-credit-card"></i> Unallocated Receipts</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['bulk-invoice'])} ">
                        <g:link controller="bulk-invoice" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-credit-card"></i> Bulk Invoices</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName:['bulk-adjust'])} ">
                        <g:link controller="bulk-adjust" action="app" class="nav-link ">
                            <i class="fa fa-gavel"></i>
                            <span class="title">Bulk Adjust</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['main-transaction'])} ">
                        <g:link controller="transaction" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-search"></i> Browse Transactions</span>
                        </g:link>
                    </li>
                    <!--<li class="nav-item ${mp.activeController(controllerName: ['transaction-batch'])} ">
                        <g:link controller="transaction-batch" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-search"></i> Batch Transactions</span>
                        </g:link>
                    </li> -->
                    <li class="nav-item ${mp.activeController(controllerName: ['flexcube-transaction'])} ">
                        <g:link controller="flexcube-transaction" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-houzz"></i> FCC Transactions</span>
                        </g:link>
                    </li>
                    <!--li class="nav-item ${mp.activeController(controllerName: ['flexcube-request'])} ">
                        <g:link controller="flexcube-request" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-arrow-right"></i> FCC Requests</span>
                        </g:link>
                    </li-->
                    <li class="nav-item ${mp.activeController(controllerName: ['billing-invoice'])} ">
                        <g:link controller="billing-invoice" action="app" class="nav-link ">
                            <span class="title"><i class="fa fa-files-o"></i> Billing Invoices</span>
                        </g:link>
                    </li>
                    <!--li class="nav-item ${mp.activeController(controllerName: ['transaction-dev'])} ">
                        <g:link controller="transaction-dev" action="index" class="nav-link ">
                            <span class="title"><i class="fa fa-plug"></i> Transactions - Dev</span>
                        </g:link>
                    </li-->
                </ul>
            </li>
            <!-- end accounts -->
            </sec:ifAnyGranted>

            <!-- START PROCESSING -->
            <li class="nav-item ${mp.activeController(controllerName: ['exchange-rate-group', 'routine-request','job-schedule', 'billing-cycle', 'account-terminate-request'])}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-wrench"></i>
                    <span class="title">Management</span>
                    <span class="arrow <mp:navItemSelected controllerName="['exchange-rate-group', 'routine-request','job-schedule', 'billing-cycle', 'account-terminate-request']"/>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ${mp.activeController(controllerName: ['routine-request'])}">
                        <g:link controller="routineRequest" action="app" class="nav-link">
                            <span class="title"><i class="fa fa-cogs"></i> Routine Requests</span>
                        </g:link>
                    </li>
                    <!--li class="nav-item ${mp.activeController(controllerName: ['job-schedule'])}">
                        <g:link controller="job-schedule" action="app" class="nav-link">
                            <i class="icon-clock"></i>
                            <span class="title">Job Schedules</span>
                        </g:link>
                    </li-->
                    <li class="nav-item ${mp.activeController(controllerName: ['billing-cycle'])} ">
                        <g:link controller="billing-cycle" action="app" class="nav-link ">
                            <i class="fa fa-calendar-times-o"></i>
                            <span class="title">Billing Cycles</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['exchange-rate-group'])} ">
                        <g:link controller="exchange-rate-group" action="app" class="nav-link ">
                            <i class="fa fa-dollar"></i>
                            <span class="title">Exchange Rates</span>
                        </g:link>
                    </li>
                    <!--li class="nav-item ${mp.activeController(controllerName: ['account-terminate-request'])}">
                        <g:link controller="accountTerminateRequest" class="nav-link">
                            <span class="title">Account Terminations</span>
                        </g:link>
                    </li> -->
                </ul>
            </li>
            <!-- END PROCESSING -->

            <sec:ifAnyGranted roles="ROLE_ADMIN">
            <!-- START CONFIGURATION GROUP -->
            <li class="heading">
                <h3 class="uppercase">Configurations</h3>
            </li>
            <li class="nav-item ${mp.activeController(controllerName: ['user', 'user-check', 'sec-role', 'settings', 'config-setting'])} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class=" icon-settings"></i>
                    <span class="title">Administration</span>

                    <span class="arrow <mp:navItemSelected controllerName="['user', 'user-check', 'sec-role', 'settings', 'config-setting']"/>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item ${mp.activeController(controllerName: ['user'])} ">
                        <g:link controller="user" action="index" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">Users </span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['user-check'])} ">
                        <g:link controller="user-check" action="index" class="nav-link ">
                            <i class="fa fa-user-plus"></i>
                            <span class="title">Users Inbox</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['sec-role'])} ">
                        <g:link controller="secRole" action="index" class="nav-link ">
                            <i class="fa fa-users"></i>
                            <span class="title">User Roles </span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['settings'])}">
                        <g:link controller="settings" class="nav-link ">
                            <i class="icon-settings"></i>
                            <span class="title">Settings</span>
                        </g:link>
                    </li>
                    <li class="nav-item ${mp.activeController(controllerName: ['config-setting'])}">
                        <g:link controller="configs" action="app" class="nav-link ">
                            <i class="icon-drawer"></i>
                            <span class="title">Configurations</span>
                        </g:link>
                    </li>
                </ul>
            </li>
            </sec:ifAnyGranted>

            <li class="heading">
                <h3 class="uppercase">Reports</h3>
            </li>
            <li class="nav-item ${mp.activeController(controllerName:['report'])} ">
                <g:link controller="report" class="nav-link ">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Reports</span>
                </g:link>
            </a>
            </li>
            <!-- END CONFIGURATIONS GROUP -->

        </ul>
    </div>
</div>