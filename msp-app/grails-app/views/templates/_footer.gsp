<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> &copy; 2018 - 2021 <g:meta name="info.app.copyrightYear"/> Powered by Nhakr
        &nbsp;- Ver: <g:meta name="info.app.version"/> Product All Rights Reserved
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
