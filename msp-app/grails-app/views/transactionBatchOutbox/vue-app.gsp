<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'transactionBatchCheck.label', default: 'TransactionBatchCheck')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'transactionBatchTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="trans-batch-outbox-list">

    <div class="search-page search-content-4 margin-top-10">

        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control" placeholder="Query Request..."  v-model="params.q">
                    </div>
                    <div class="col-lg-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                        <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th>
                        <input type="checkbox" id="all-items" class="icheck" />
                    </th>
                    <th>Batch Reference</th>
                    <th>Batch Number</th>
                    <th>Transaction Type</th>
                    <th>Date Created</th>
                    <th>Created By</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="transactionBatchCheck in transactionBatchChecks">
                    <td>
                        <div class="icheck icheck-default">
                            <input type="checkbox" id="someCheckboxId" />
                        </div>
                </th>
                    <td>
                        <router-link class="display-block" :to="{name: 'show', params: {id: transactionBatchCheck.id}}">{{transactionBatchCheck.batchReference}}</router-link>
                    </td>
                    <td>{{transactionBatchCheck.batchReference}}</td>
                    <td>{{transactionBatchCheck.transactionType.name}}</td>
                    <td>{{transactionBatchCheck.dateCreated | formatDate}}</td>
                    <td>{{transactionBatchCheck.maker.fullName}}</td>
                </tr>
                <tr v-if="transactionBatchChecks.length == 0">
                    <td colspan="6">No Records found...</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</template>

<template id="trans-batch-outbox-show">
    <section>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <div class="portlet light bg-inverse dblue-hoki margin-top-10">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Batch Transaction
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        ID:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.id}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Status:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.checkStatus}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Action:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.actionName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Entity:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.entity.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Date Created:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.makeDate | formatDate}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Maker:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.maker.fullName}}</div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Transaction Type:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.transactionType.name}}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-5">
                                        Batch Reference:
                                    </label>
                                    <div class="col-md-7">{{transactionBatchCheck.batchReference}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <maker-options @cancelled="onCancelled" :reload="false" :makerid="transactionBatchCheck.maker.id" :item-id="transactionBatchCheck.id" :controller="'transaction-batch-check'" :status="transactionBatchCheck.checkStatus"></maker-options>

        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12">
                <div class="portlet light bordered margin-top-10">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-eye"></i> Viewing Batch Transaction : {{transactionBatchCheck.batchReference}}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <trans-list-comp :transaction-batch="transactionHeader"></trans-list-comp>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Transactions
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Transactions</router-link>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/transaction-batch-check/transaction-batch-outbox.js" asset-defer="true"/>

</body>
</html>