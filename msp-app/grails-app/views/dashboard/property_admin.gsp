<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>MS Property :: [Real Estate management] - Dashboard</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Dashboard :: MS Property" scope="request"/>
    <g:set var="pageHeadTitle" value="Property Dashboard" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<g:render template="page_bar"/>

<div class="dashboard-container" id="dashboard-app">

    <!-- BEGIN DASHBOARD MENU 1-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <outstanding-balances></outstanding-balances>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <occupancy-lite></occupancy-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <lease-reviews></lease-reviews>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <commission-lite desc="Current Commission"></commission-lite>
        </div>

    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD MENU 1-->

    <!-- BEGIN DASHBOARD STATS 2-->
    <div class="row">

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <checker-inbox-widget></checker-inbox-widget>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <leases-dashlet></leases-dashlet>

            <billing-cycles></billing-cycles>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <!-- maker-outbox-widget></maker-outbox-widget -->
            <exchange-rate-preview mode="dashlet" ></exchange-rate-preview>
        </div>

    </div>
    <!-- END DASHBOARD STATS 2-->
    
    <!-- begin analytics 1 -->
    <!-- begin analytics 1 -->
</div>

<template id="occupancy-lite">
    <a class="dashboard-stat dashboard-stat-v2 green" href="#">
        <div class="visual">
            <i class="fa fa-user"></i>
        </div>
        <div class="details">
            <div class="number">
                <span data-counter="counterup" :data-value="occupiedRate">{{occupiedRate}}</span> %
            </div>
            <div class="desc"> Occupancy Rate </div>
        </div>
    </a>
</template>

<template id="outstanding-balances">
    <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
        <div class="visual">
            <i class="fa fa-money"></i>
        </div>
        <div class="details">
            <div class="number">
                $ <span data-counter="counterup" data-value="0">{{dashboard.outstandingBalances | formatNumber(0)}}</span>
            </div>
            <div class="desc"> Outstanding Balances </div>
        </div>
    </a>
</template>

<template id="lease-reviews">
    <g:link controller="lease-review" action="app" class="dashboard-stat dashboard-stat-v2 blue" title="Pending Lease Reviews">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="visual">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="details">
            <div class="number">
                <span data-counter="counterup" :data-value="dashboard.reviewCount">{{dashboard.reviewCount}}</span>
            </div>
            <div class="desc"> Lease Reviews </div>
        </div>
    </g:link>
</template>

<template id="checker-inbox-widget">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-inbox"></i> Checker Inbox</h3>
        </div>
        <!-- List group -->
        <ul class="list-group">
            <li class="list-group-item" v-for="item in widgetItems" >
                <a :href="config.API_URL + item.url" class="display-block" >{{item.name}}
                    <span v-if="item.count == 0" class="badge badge-default">{{item.count}}</span>
                    <span v-if="item.count > 0" class="badge badge-info">{{item.count}}</span>
                </a>
            </li>
        </ul>
    </div>
</template>

<template id="maker-outbox-widget">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-envelope"></i> Maker Outbox</h3>
        </div>
        <!-- List group -->
        <ul class="list-group">
            <list class="list-group-item" v-for="item in widgetItems" >
                <a :href="config.API_URL + item.url" class="display-block" >{{item.name}}
                    <span v-if="item.count == 0" class="badge badge-default">{{item.count}}</span>
                    <span v-if="item.count > 0" class="badge badge-info">{{item.count}}</span>
                </a>
            </list>
        </ul>
    </div>
</template>

<template id="leases-dashlet">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-asterisk"></i> Expiring Leases</h3>
        </div>
        <!-- List group -->
        <ul class="list-group">
            <li class="list-group-item" >
                Leases expiring in 30 Days  <span class="label pull-right label-danger">{{widgetData['30']}}</span>
            </li>
            <li class="list-group-item" >
                Leases expiring in 31 to 90 Days  <span class="label pull-right  label-warning">{{widgetData['90']}}</span>
            </li>
            <li class="list-group-item" >
                Leases expiring in 91 to 120 Days  <span class="label pull-right  label-success">{{widgetData['120']}}</span>
            </li>
            <li class="list-group-item" >
                Leases expiring in 121 to 180 Days  <span class="label pull-right  label-primary">{{widgetData['180']}}</span>
            </li>
        </ul>
    </div>
</template>

<template id="rental-listings-dashlet">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">Rental Listings</div>
        </div>

        <div class="portlet-body" ref="rentalListings" >
            <ul class="list-group">

            </ul>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashboard-property-admin.js" asset-defer="true" />

</body>
</html>
