<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4><i class="fa fa-dashboard"></i> ${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <!--div class="page-toolbar">
        <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>&nbsp;
            <span class="thin uppercase hidden-xs"></span>&nbsp;
            <i class="fa fa-angle-down"></i>
        </div>
    </div-->
</div>