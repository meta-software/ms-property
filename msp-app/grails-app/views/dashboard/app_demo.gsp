<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>MS Property :: [Real Estate management] - Dashboard</title>

    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="pageTitle" value="Dashboard :: MS Property" scope="request"/>
    <g:set var="pageHeadTitle" value="Property Dashboard" scope="request"/>
    <!-- END TEMPLATE VARS -->

</head>

<body>

<g:render template="page_bar"/>

<div class="dashboard-container" id="dashboard-app">

    <!-- BEGIN DASHBOARD MENU 1-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <outstanding-balances></outstanding-balances>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <occupancy-lite></occupancy-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <lease-reviews></lease-reviews>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <commission-lite desc="Current Commission"></commission-lite>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD MENU 1-->

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ds-invoice-lite desc="Current Invoices"></ds-invoice-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ds-receipt-lite desc="Current Receipts"></ds-receipt-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ds-commission-widget desc="Current Commission"></ds-commission-widget>
        </div>
    </div>

    <!-- BEGIN DASHBOARD STATS 2-->
    <div class="row">

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <checker-inbox-widget></checker-inbox-widget>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <maker-outbox-widget></maker-outbox-widget>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <leases-dashlet></leases-dashlet>
        </div>

    </div>
    <!-- END DASHBOARD STATS 2-->

    <!-- DASHBOARD MENU CHARTS -->
    <div class="row">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <ds-revenue-chart></ds-revenue-chart>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <ds-revenue-pie></ds-revenue-pie>
        </div>
    </div>
    <!-- DASHBOARD MENU CHARTS -->

    <!-- BEGIN DASHBOARD MENU 1-->
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <occupancy-dashlet></occupancy-dashlet>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <occupancy-dashlet></occupancy-dashlet>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD MENU 1-->

    <!-- begin analytics 1 -->
    <!-- begin analytics 1 -->
</div>

<template id="occupancy-lite">
    <a class="dashboard-stat dashboard-stat-v2 green" href="#">
        <div class="visual">
            <i class="fa fa-user"></i>
        </div>
        <div class="details">
            <div class="number">
                <span data-counter="counterup" :data-value="occupiedRate">{{occupiedRate}}</span> %
            </div>
            <div class="desc"> Occupancy Rate </div>
        </div>
    </a>
</template>

<template id="outstanding-balances">
    <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
        <div class="visual">
            <i class="fa fa-money"></i>
        </div>
        <div class="details">
            <div class="number">
                $ <span data-counter="counterup" data-value="0">{{dashboard.outstandingBalances | formatNumber(0)}}</span>
            </div>
            <div class="desc"> Outstanding Balances </div>
        </div>
    </a>
</template>

<template id="lease-reviews">
    <g:link controller="lease-review" action="app" class="dashboard-stat dashboard-stat-v2 blue" title="Pending Lease Reviews">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="visual">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="details">
            <div class="number">
                <span data-counter="counterup" :data-value="dashboard.reviewCount">{{dashboard.reviewCount}}</span>
            </div>
            <div class="desc"> Lease Reviews </div>
        </div>
    </g:link>
</template>

<template id="checker-inbox-widget">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-inbox"></i> Checker Inbox
            </div>

            <div class="actions">
                <a class="btn btn-circle btn-icon-only btn-default" @click="refresh">
                    <i class="icon-refresh"></i>
                </a>
            </div>
        </div>

        <div class="portlet-body" ref="checkerInbox" >
            <div class="dashlet-body" style="height: 280px;">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr v-for="item in widgetItems">
                            <td>{{item.pos}}</td>
                            <td><a :href="config.API_URL + item.url" class="display-block" >{{item.name}}</a></td>
                            <td>
                                <span v-if="item.count == 0" class="badge badge-default">{{item.count}}</span>
                                <span v-if="item.count > 0" class="badge badge-info">{{item.count}}</span>
                            </td>
                        </tr>
                        <tr v-if="widgetItems.length == 0">
                            <td colspan="3">No Records found.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="maker-outbox-widget">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-envelope"></i> Maker Outbox
            </div>

            <div class="actions">
                <a class="btn btn-circle btn-icon-only btn-default" @click="refresh">
                    <i class="icon-refresh"></i>
                </a>
            </div>
        </div>

        <div class="portlet-body" ref="makerOutbox" >
            <div class="dashlet-body" style="height: 280px;">
                <div class="table-responsive">
                    <ul class="list-group" v-if="false">
                        <li v-for="item in widgetItems" class="list-group-item" >
                            {{item.name}}
                            <span class="badge badge-info">{{item.count}}</span>
                        </li>
                    </ul>
                    <table class="table table-striped table-hover" >
                        <tbody>
                        <tr v-for="item in widgetItems">
                            <td><a :href="config.API_URL + item.url" class="display-block" >{{item.name}}</a></td>
                            <td>
                                <span v-if="item.count == 0" class="badge badge-default">{{item.count}}</span>
                                <span v-if="item.count > 0" class="badge badge-info">{{item.count}}</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="leases-dashlet">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">Expiring Leases</div>

            <div class="actions">
                <button class="btn btn-circle btn-icon-only btn-default" >
                    <i class="icon-check"></i>
                </button>
            </div>
        </div>

        <div class="portlet-body">
            <div class="dashlet-body" style="height: 280px;">
                <ul class="list-group">
                    <li class="list-group-item" >
                        Leases expiring in 30 Days  <span class="label pull-right label-danger">{{widgetData['30']}}</span>
                    </li>
                    <li class="list-group-item" >
                        Leases expiring in 31 to 90 Days  <span class="label pull-right  label-warning">{{widgetData['90']}}</span>
                    </li>
                    <li class="list-group-item" >
                        Leases expiring in 91 to 120 Days  <span class="label pull-right  label-success">{{widgetData['120']}}</span>
                    </li>
                    <li class="list-group-item" >
                        Leases expiring in 121 to 180 Days  <span class="label pull-right  label-primary">{{widgetData['180']}}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</template>

<template id="rental-listings-dashlet">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">Rental Listings</div>
        </div>

        <div class="portlet-body" ref="rentalListings" >
            <ul class="list-group">

            </ul>
        </div>
    </div>
</template>

<template id="occupancy-dashlet">
    <div class="portlet light portlet-fit bordered">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-cursor font-dark hide"></i>
                <span class="caption-subject font-dark bold uppercase">Occupancy Summary</span>
            </div>
            <div class="actions">
                <button type="button" @click="refresh" href="javascript:;" class="btn btn-sm btn-circle red-mint">
                    <i class="fa fa-repeat"></i> Reload
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <easy-pie-chart
                            :percent="dashboard.notOccupiedRate"
                            :color="'red'"
                            :legend="'Vacant'"
                            :chart-class="'icon-close'"
                    >
                    </easy-pie-chart>
                </div>
                <div class="margin-bottom-10 visible-sm"> </div>
                <div class="col-md-6">
                    <easy-pie-chart
                            :percent="dashboard.occupiedRate"
                            :color="'green'"
                            :legend="'Occupied'"
                            :chart-class="'icon-arrow-right'"
                    >
                    </easy-pie-chart>
                </div>
            </div>
            <div class="margin-bottom-10 visible-sm"> </div>
        </div>
    </div>
</div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/plugins/jquery-easypiechart/vue.easypiechart.js" asset-defer="true" />
<asset:javascript src="global/plugins/flot/jquery.flot.min.js" asset-defer="true" />
<asset:javascript src="global/plugins/flot/jquery.flot.resize.min.js" asset-defer="true" />
<asset:javascript src="global/plugins/flot/jquery.flot.categories.min.js" asset-defer="true" />

<asset:javascript src="vue-apps/dashboard/dashboard-property-admin.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-invoices-lite.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-receipts-lite.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-commission-widget.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-revenue-chart.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-revenue-pie.js" asset-defer="true" />

</body>
</html>
