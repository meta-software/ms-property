<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>MS Property :: [Real Estate management] - Dashboard</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Dashboard :: MS Property" scope="request"/>
    <g:set var="pageHeadTitle" value="Property Dashboard" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<g:render template="page_bar"/>

<div class="dashboard-container" id="dashboard-app">

    <!-- BEGIN DASHBOARD MENU 1-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <pending-receipts-lite></pending-receipts-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <g:link controller="property" action="create" class="dashboard-stat dashboard-stat-v2 blue" title="Create a new property">
                <div class="visual">
                    <i class="fa fa-building"></i>
                </div>
                <div class="details">
                    <div class="number" style="text-align: center;">
                        <span><i class="fa fa-plus"></i></span>
                    </div>
                    <div class="desc"> Create Property </div>
                </div>
            </g:link>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <g:link controller="tenant" action="create" class="dashboard-stat dashboard-stat-v2 red" title="Create a new tenant">
                <div class="visual">
                    <i class="fa fa-user"></i>
                </div>
                <div class="details">
                    <div class="number" style="text-align: center;">
                        <span><i class="fa fa-plus"></i></span>
                    </div>
                    <div class="desc"> Create Tenant </div>
                </div>
            </g:link>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <g:link controller="lease" action="create" class="dashboard-stat dashboard-stat-v2 green" title="Create a new lease">
                <div class="visual">
                    <i class="fa fa-sticky-note"></i>
                </div>
                <div class="details">
                    <div class="number" style="text-align: center;">
                        <span><i class="fa fa-plus"></i></span>
                    </div>
                    <div class="desc"> Create Lease </div>
                </div>
            </g:link>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD MENU 1-->

    <!-- BEGIN DASHBOARD STATS 2-->
    <div class="row">

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <maker-outbox-widget></maker-outbox-widget>
        </div>

    </div>
    <!-- END DASHBOARD STATS 2-->

</div>

<template id="checker-inbox-widget">
    <section>
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-inbox"></i> Checker Inbox
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" @click="refresh">
                        <i class="icon-refresh"></i>
                    </a>
                </div>
            </div>

            <div class="portlet-body" ref="checkerInbox" >
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <tbody>
                        <tr v-for="item in widgetItems">
                            <td>{{item.pos}}</td>
                            <td><a :href="item.url" class="display-block" >{{item.name}}</a></td>
                            <td>
                                <span v-if="item.count == 0" class="badge badge-default">{{item.count}}</span>
                                <span v-if="item.count > 0" class="badge badge-info">{{item.count}}</span>
                            </td>
                        </tr>
                        <tr v-if="widgetItems.length == 0">
                            <td colspan="3">No Records found.</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</template>

<template id="maker-outbox-widget">
    <section>
        <div class="portlet light borderedd">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-envelope"></i> Maker Outbox
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default" @click="refresh">
                        <i class="icon-refresh"></i>
                    </a>
                </div>
            </div>

            <div class="portlet-body" ref="makerOutbox" >
                <div class="table-responsive">
                    <ul class="list-group" v-if="false">
                        <li v-for="item in widgetItems" class="list-group-item" >
                            {{item.name}}
                            <span class="badge badge-info">{{item.count}}</span>
                        </li>
                    </ul>
                    <table class="table table-striped table-hover" >
                        <tbody>
                        <tr v-for="item in widgetItems">
                            <td><a :href="item.url" class="display-block" >{{item.name}}</a></td>
                            <td>
                                <span v-if="item.count == 0" class="badge badge-default">{{item.count}}</span>
                                <span v-if="item.count > 0" class="badge badge-info">{{item.count}}</span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashboard-index.js" asset-defer="true" />

</body>
</html>
