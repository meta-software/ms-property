<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>MS Property :: [Real Estate management] - Dashboard</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Dashboard :: MS Property" scope="request"/>
    <g:set var="pageHeadTitle" value="Administrator Dashboard" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<g:render template="page_bar"/>

<div class="dashboard-container" id="dashboard-app">

    <!-- BEGIN DASHBOARD MENU 1-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <g:link controller="user" action="create" class="dashboard-stat dashboard-stat-v2 blue" title="Create a new user">
                <div class="visual">
                    <i class="fa fa-user-plus"></i>
                </div>
                <div class="details">
                    <div class="number" style="text-align: center;">
                        <span><i class="fa fa-plus"></i></span>
                    </div>
                    <div class="desc"> Create User </div>
                </div>
            </g:link>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <ds-pending-users></ds-pending-users>
        </div>

</div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD MENU 1-->

</div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashboard-admin-app.js" asset-defer="true" />

<template id="ds-pending-users">
    <g:link controller="userCheck" action="index" class="dashboard-stat dashboard-stat-v2 red-mint" title="Pending users">
        <loading :active.sync="loader.isLoading"
                 :is-full-page="loader.fullPage"></loading>
        <div class="visual">
            <i class="fa fa-user"></i>
        </div>
        <div class="details">
            <div class="number">
                <span data-counter="counterup" :data-value="dashboard.pendingUsers">{{dashboard.pendingUsers}}</span>
            </div>
            <div class="desc"> Pending Users </div>
        </div>
    </g:link>
</template>


</body>
</html>
