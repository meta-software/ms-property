<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="Dashboard :: Nhakr" scope="request"/>
    <g:set var="pageHeadTitle" value="Executive Dashboard" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<g:render template="page_bar"/>

<div class="dashboard-container" id="dashboard-app">

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ds-invoice-lite desc="Current Invoices"></ds-invoice-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ds-receipt-lite desc="Current Receipts"></ds-receipt-lite>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ds-commission-widget desc="Current Commission"></ds-commission-widget>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">

        </div>
    </div>

    <!-- BEGIN DASHBOARD MENU 1-->
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <occupancy-dashlet></occupancy-dashlet>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        </div>
    </div>

    <div class="clearfix"></div>
    <!-- END DASHBOARD MENU 1-->

    <!-- DASHBOARD MENU CHARTS -->
    <!--div class="row">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <ds-revenue-chart></ds-revenue-chart>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <ds-revenue-pie></ds-revenue-pie>
        </div>
    </div-->
    <!-- DASHBOARD MENU CHARTS -->

</div>

<template id="occupancy-dashlet">
    <div class="portlet light bordered">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-cursor font-dark hide"></i>
                <span class="caption-subject font-dark bold uppercase">Occupancy Summary</span>
            </div>
            <div class="actions">
                <button type="button" @click="refresh" href="javascript:;" class="btn btn-sm btn-circle red-mint">
                    <i class="fa fa-repeat"></i> Reload
                </button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <easy-pie-chart
                            :percent="dashboard.notOccupiedRate"
                            :color="'red'"
                            :legend="'Vacant'"
                            :chart-class="'icon-close'"
                    >
                    </easy-pie-chart>
                </div>
                <div class="margin-bottom-10 visible-sm"> </div>
                <div class="col-md-6">
                    <easy-pie-chart
                            :percent="dashboard.occupiedRate"
                            :color="'green'"
                            :legend="'Occupied'"
                            :chart-class="'icon-arrow-right'"
                    >
                    </easy-pie-chart>
                    </div>
                </div>
                <div class="margin-bottom-10 visible-sm"> </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="global/plugins/jquery-easypiechart/jquery.easypiechart.js" asset-defer="true" />
<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/plugins/jquery-easypiechart/vue.easypiechart.js" asset-defer="true" />
<asset:javascript src="global/plugins/flot/jquery.flot.min.js" asset-defer="true" />
<asset:javascript src="global/plugins/flot/jquery.flot.resize.min.js" asset-defer="true" />
<asset:javascript src="global/plugins/flot/jquery.flot.categories.min.js" asset-defer="true" />

<asset:javascript src="vue-apps/dashboard/dashboard-property-executive.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-invoices-lite.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-receipts-lite.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-commission-widget.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-revenue-chart.js" asset-defer="true" />
<asset:javascript src="vue-apps/dashboard/dashlets/dashlet-revenue-pie.js" asset-defer="true" />

</body>
</html>
