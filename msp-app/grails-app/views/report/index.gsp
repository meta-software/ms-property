<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Metafost Property :: [Real Estate management] - Reports</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageTitle" value="MetasoftWare :: Reporting" scope="request"/>
    <g:set var="pageHeadTitle" value="${message(code: 'report.default.label', default: 'All Reports')}" scope="request"/>
    <!-- END TEMPALTE VARS -->

</head>

<body>

<g:render template="page_bar"/>

<div>

    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="table-scrollable">
                <table class="table table-bordered table-condensed table-stripped table-hover table-advance">
                    <thead>
                    <tr>
                        <th>Report Name</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Category</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><g:link class="display-block" controller="report" action="process" params="[r:'lessee-statement']">Lessee Statement</g:link></td>
                        <td>Lessee Statement report</td>
                        <td>Report</td>
                        <td>Statements</td>
                    </tr>
                    <tr>
                        <td><g:link class="display-block" controller="report" action="process" params="[r:'property-md-rpt']">Property Master Report</g:link></td>
                        <td>Property Master Data Report</td>
                        <td>Master Data Report</td>
                        <td>Statements</td>
                    </tr>
                    <tr>
                        <td><g:link class="display-block" controller="report" action="process" params="[r:'tenant-md-rpt']">Tenant Master Report</g:link></td>
                        <td>Tenant Master Data report</td>
                        <td>Master Data Report</td>
                        <td>Statements</td>
                    </tr>
                    <tr>
                        <td><g:link class="display-block" controller="report" action="process" params="[r:'landlord-md-rpt']">Landlord Master Report</g:link></td>
                        <td>Landlord Data report</td>
                        <td>Master Data Report</td>
                        <td>Statements</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD STATS 1-->

</div>

</body>
</html>
