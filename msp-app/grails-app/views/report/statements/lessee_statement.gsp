<%@ page import="metasoft.property.core.BillingCycle" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Metafost Property :: Report</title>

    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="pageHeadTitle" value="${message(code: 'report.default.label', default: 'Lessee Statement')}" scope="request"/>
    <!-- END TEMPALTE VARS -->
</head>

<body>

<!--g:render template="page_bar"/-->

<div id="report-app">

    <!-- BEGIN report parameters -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="portlet light bordered m-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-line-chart"></i> Sample Report
                    </div>
                    <div class="tools">
                        <a data-toggle="collapse" class="collapse"></a>
                    </div>
                </div>

                <div class="portlet-body" style="padding: 5px 6px;">
                    <form target="_reportIframe" ref="reportParamsForm" role="form" autocomplete="off">
                        <input type="hidden" ref="reportUrl" value="${pentaho.reportUrl(src: ':public:Steel Wheels:Reports:Buyer Report (sparkline report).prpt')}" />
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Select a Line</label>
                                        <select name="line" class="select2-no-search">
                                            <option value="" selected="selected">Select a Line...</option>
                                            <option value="Classic Cars">Classic Cars</option>
                                            <option value="Motorcycles">Motorcycles</option>
                                            <option value="Planes">Planes</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <select name="output-target" class="form-control select2-no-search input-sm">
                                    <!-- <option value="table/html;page-mode=page">HTML (Paginated)</option> -->
                                    <option value="table/html;page-mode=stream">HTML (Single Page)</option>
                                    <option value="pageable/pdf">PDF</option>
                                    <option value="table/excel;page-mode=flow">Excel</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <button type="submit" @click="executeReport" class="btn blue-hoki"><i class="fa fa-"></i> View Report</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END report parameters -->

    <div class="row" >
        <div ref="_reportIframeContainer" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <pentaho:report src=":public:CBZ:reports:Lessee Statement.prpt" />
        </div>
    </div>
</div>

<asset:javascript src="reportviewer.js" asset-defer="true"/>
<!-- END The report Filter -->

</body>
</html>
