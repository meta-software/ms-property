<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'settings.label', default: 'Settings')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'settings.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="settings-app">
    <section>
        <page-bar></page-bar>

        <router-view></router-view>
    </section>
</template>

<template id="settings-list">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <div class="portlet-body form">
                    <!------------------------------- -->
                    <div class="search-page search-content-4 margin-top-10">
                        <div class="search-bar bordered">
                            <form role="form" autocomplete="off">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8">
                                        <input type="text" class="form-control" placeholder="Query..." name="query" v-model="queryParams.query"  @keyup="search" />
                                    </div>
                                    <div class="col-lg-2 col-md-2 extra-buttons">
                                        <button class="btn grey-steel uppercase bold" type="button" @click.stop="search">Search</button>
                                        <button class="btn green" type="button" @click.stop="refresh"><i class="fa fa-refresh"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="search-table table-responsive">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="setting in settings">
                                    <td>{{setting.category}}</td>
                                    <td>{{setting.name}}</td>
                                    <td>{{setting.description}}</td>
                                    <td><input type="text" :value="setting.value" disabled="disabled" class="form-control input-sm input-small" /></td>
                                </tr>
                                <tr v-if="settings.length == 0">
                                    <td colspan="4">No Records found...</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!------------------------------- -->
                </div>
            </div>
        </div>
    </div>
</template>

<template id="settings-page">
    <div class="row settings-content">
        <div class="col-md-3">
            <ul class="ver-inline-menu tabbable margin-bottom-10">
                <li class="active">
                    <a data-toggle="tab" href="#tab_1-1" aria-expanded="true">
                        <i class="fa fa-cog"></i> General </a>
                        <span class="after"> </span>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#tab_2-2" aria-expanded="false">
                        <i class="fa fa-envelope"></i> Email Config
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#tab_3-3" aria-expanded="false">
                        <i class="fa fa-money"></i> Finance
                    </a>
                </li>
                <li class="">
                    <a data-toggle="tab" href="#tab_4-4" aria-expanded="false">
                        <i class="fa fa-eye"></i> Billing
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div id="tab_1-1" class="tab-pane active">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">General Settings</div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" role="form" action="#">
                                <div class="form-group">
                                    <label class="col-md-5">Default Currency:</label>
                                    <div class="col-md-7"><input class="form-control" /></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-5">TimeZone:</label>
                                    <div class="col-md-7"><input class="form-control" /></div>
                                </div>

                                <div class="margiv-top-10">
                                    <a href="javascript:;" class="btn green"> Save Changes </a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="tab_2-2" class="tab-pane">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">Email Configuration</div>
                        </div>
                        <div class="portlet-body">
                            <form class="form-horizontal" action="#" role="form">
                                <div class="form-group">
                                    <label class="col-md-5">Server Host:</label>
                                    <div class="col-md-7"><input class="form-control" /></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-5">Port: </label>
                                    <div class="col-md-7"><input class="form-control" /></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-5">Username: </label>
                                    <div class="col-md-7"><input class="form-control" /></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-5">Password: </label>
                                    <div class="col-md-7"><input class="form-control" /></div>
                                </div>

                                <div class="margin-top-10">
                                    <a href="javascript:;" class="btn green"> Submit </a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="tab_3-3" class="tab-pane">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">Email Configuration</div>
                        </div>
                        <div class="portlet-body">
                            <form action="#">
                                <div class="form-group">
                                    <label class="control-label">Current Password</label>
                                    <input type="password" class="form-control"> </div>
                                <div class="form-group">
                                    <label class="control-label">New Password</label>
                                    <input type="password" class="form-control"> </div>
                                <div class="form-group">
                                    <label class="control-label">Re-type New Password</label>
                                    <input type="password" class="form-control"> </div>
                                <div class="margin-top-10">
                                    <a href="javascript:;" class="btn green"> Change Password </a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="tab_4-4" class="tab-pane">
                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption">Email Configuration</div>
                        </div>
                        <div class="portlet-body">
                            <form action="#">
                                <table class="table table-bordered table-striped">
                                    <tbody><tr>
                                        <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                        <td>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios1" value="option1"> Yes
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios1" value="option2" checked=""> No
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                        <td>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" value="option1"> Yes
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios21" value="option2" checked=""> No
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                        <td>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios31" value="option1"> Yes
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios31" value="option2" checked=""> No
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                        <td>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios41" value="option1"> Yes
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="optionsRadios41" value="option2" checked=""> No
                                                    <span></span>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody></table>
                                <!--end profile-settings-->
                                <div class="margin-top-10">
                                    <a href="javascript:;" class="btn green"> Save Changes </a>
                                    <a href="javascript:;" class="btn default"> Cancel </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end col-md-9-->
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> <i class="fa fa-cogs"></i> Settings
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/settings/settings-app.js" asset-defer="true" />

</body>
</html>