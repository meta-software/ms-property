<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'routineRequest.label', default: 'Routine Request')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'routineRequest.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="global/plugins/vue/bootstrap-table/bootstrap-table-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/routineRequest/routine-request-app.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <div class="inbox margin-top-10">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-xs-12">
                    <div class="inbox-sidebar">
                        <router-link data-title="Create Request" class="btn green btn-block" :to="{name: 'create'}">
                            <i class="fa fa-plus"></i> Create Request
                        </router-link>
                        <ul class="inbox-nav">
                            <li>
                                <router-link data-title="Approved" to="/">
                                    Approved
                                </router-link>
                            </li>
                            <li class="actives">
                                <router-link data-title="Outbox" to="/outbox">
                                    Outbox
                                </router-link>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-10 col-md-8 col-xs-12">
                    <router-view></router-view>
                </div>
            </div>
        </div>

    </div>
</template>

<template id="routine-request-outbox">
    <div class="inbox margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
    <div class="row">
        <div class="col-md-12" ref="outboxList">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Outbox : Routine Requests</h3>
                </div>
                <div class="inbox-content">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Action</th>
                                <th>Type</th>
                                <th>Cycle</th>
                                <th>Run Date</th>
                                <th>Value Date</th>
                                <th>Date Created</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(routineRequestCheck, key) in routineRequestChecks" :key="key">
                                <td>
                                    <router-link class="display-block" :to="{name: 'outboxShow', params: {id: routineRequestCheck.id}}">{{routineRequestCheck.id}}</router-link>
                                </td>
                                <td>{{routineRequestCheck.actionName}}</td>
                                <td>{{routineRequestCheck.entity.routineType.name}}</td>
                                <td>{{routineRequestCheck.entity.billingCycle.name}}</td>
                                <td>{{routineRequestCheck.entity.runDate | formatDate }}</td>
                                <td>{{routineRequestCheck.entity.valueDate | formatDate }}</td>
                                <td>{{routineRequestCheck.entity.dateCreated | formatDate('YYYY-MM-DD HH:mm') }}</td>
                            </tr>
                            <tr v-if="routineRequestChecks.length == 0">
                                <td colspan="7">No Records found...</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="routine-request-list">

    <div class="inbox">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div id="toolbar-list">
            <h3> Routine Requests</h3>
        </div>
        <div class="row">
        <div class="col-md-12">

            <div class="inbox-body">
                <div class="inbox-content">
                    <bootstrap-table :columns="table.columns" :data="table.data" :options="table.options"></bootstrap-table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="routine-outbox-show">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light entity-details bg-inverse">
                    <div class="portlet-title">
                        <div class="caption">
                            Viewing Pending Task : Routine Request
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form class="horizontal-form" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Id:</label>
                                            <div class="form-control-static">{{routineRequestCheck.id}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Status:</label>
                                            <div class="form-control-static">{{routineRequestCheck.checkStatus}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4  col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Action:</label>
                                            <div class="form-control-static">{{routineRequestCheck.actionName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label class="control-label">Value Date: </label>
                                            <div class="form-control-static">{{routineRequestCheck.entity.valueDate | formatDate}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label class="control-label">Entity: </label>
                                            <div class="form-control-static">{{routineRequestCheck.entity.routineType.name}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label class="control-label">Date Created:</label>
                                            <div class="form-control-static">{{routineRequestCheck.makeDate | formatDate}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label class="control-label">Maker:</label>
                                            <div class="form-control-static">{{routineRequestCheck.maker.fullName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4 ">
                                        <div class="form-group">
                                            <label class="control-label">Run Date:</label>
                                            <div class="form-control-static">{{routineRequestCheck.entity.runDate | formatDate}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <maker-options :makerid="routineRequestCheck.maker.id" :item-id="routineRequestCheck.id" :controller="'routine-request-check-outbox'" :status="routineRequestCheck.checkStatus"></maker-options>

    </section>
</template>

<template id="routine-request-show">
    <section>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered bg-inverse entity-details">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Routine Request Info
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="horizontal-form" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Run Id:</label>
                                            <div class="form-control-static">{{routineRequest.id}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Run Type:</label>
                                            <div class="form-control-static">{{routineRequest.routineType.name}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Billing Cycle:</label>
                                            <div class="form-control-static">{{routineRequest.billingCycle.name}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Status:</label>
                                            <div class="form-control-static"><span :class="statusClass(routineRequest.requestStatus)" class="request-status label label-sm">{{routineRequest.requestStatus}}</span></div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Run Date:</label>
                                            <div class="form-control-static">{{routineRequest.runDate | formatDate}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label">Value Date:</label>
                                            <div class="form-control-static">{{routineRequest.valueDate | formatDate}}</div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!--Routine Request transactions.-->
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Routine Request Transactions
                        </div>
                    </div>

                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <routine-transactions :routine-request-id="id"></routine-transactions>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </section>
</template>

<template id="routine-transactions">
    <div class="table-scrollable-borderless">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div id="toolbar">
            <h3> Transactions</h3>
        </div>
        <bootstrap-table :columns="table.columns" :data="table.data" :options="table.options"></bootstrap-table>
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> <i class="fa fa-cogs"></i> Routine Requests
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Requests</router-link>
            </div>
        </div>
    </div>
</template>

<template id="routine-request-create">
    <section>
        <form role="form">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><a @click="$router.go(-1)"><i class="fa fa-arrow-left" style="font-size: 1.10em;"> </i>
                    </a> Create Routine Request</div>

                    <div class="actions">
                        <button @click="onCancelCreate" type="button" class="btn default btn-sm">
                            <i class="fa fa-times"></i> Cancel
                        </button>

                        <button type="button" class="btn btn-sm btn-success" @click="onSaveRequest">
                            <i class="fa fa-check"></i> Save Request
                        </button>
                    </div>

                </div>

                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Routine Type
                                    <span class="required">*</span>
                                </label>

                                <div>
                                    <select2 v-model="routineRequest.routineType.id" name="routineType"
                                             :select2="routineTypeOptions">
                                        <option disabled value="0">Select one</option>
                                    </select2>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Billing Cycle
                                    <span class="required">*</span>
                                </label>

                                <div>
                                    <select2 v-model="routineRequest.billingCycle.id" name="billingCycle"
                                             :select2="billingCycleOptions">
                                        <option disabled value="0">Select one</option>
                                    </select2>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                            <dropdown class="form-group">
                                <label class="control-label">Run Date: <span class="required">*</span>
                                </label>

                                <div class="input-group">
                                    <input class="form-control" type="text" v-model="routineRequest.runDate" />
                                    <div class="input-group-btn">
                                        <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                                    </div>
                                </div>
                                <template slot="dropdown">
                                    <li>
                                        <date-picker v-model="routineRequest.runDate"/>
                                    </li>
                                </template>
                            </dropdown>
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                            <dropdown class="form-group">
                                <label class="control-label">Value Date: <span class="required">*</span> </label>

                                <div class="input-group">
                                    <input class="form-control" type="text" v-model="routineRequest.valueDate" />
                                    <div class="input-group-btn">
                                        <btn class="dropdown-toggle"><i class="glyphicon glyphicon-calendar"></i></btn>
                                    </div>
                                </div>
                                <template slot="dropdown">
                                    <li>
                                        <date-picker v-model="routineRequest.valueDate"/>
                                    </li>
                                </template>
                            </dropdown>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </section>
</template>

</body>
</html>