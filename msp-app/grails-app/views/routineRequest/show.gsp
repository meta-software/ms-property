<%@ page import="metasoft.property.core.PropertyManager; metasoft.property.core.PropertyType" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'routineRequest.label', default: 'Routine Request')}"/>
    <g:set var="pageHeadTitle" value="Routine Request Info #${routineRequest.id}" scope="request"/>

    <meta name="layout" content="main"/>
    <title><g:message value="View Routine Request - ${this.routineRequest}" args="[entityName]"/></title>

    <script type="text/javascript">
        let routineRequestId = ${routineRequest.id};
    </script>

</head>

<body>

<div id="routine-request-show-app">

    <g:render template="page_bar_show"/>

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Routine Request Info
                    </div>
                </div>

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Run Id:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">${routineRequest}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Run Type:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">${routineRequest.routineType}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Billing Cycle:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">${routineRequest.billingCycle}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Status:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"><mp:requestStatusLabel status="${routineRequest.requestStatus}" text="${routineRequest.requestStatus}" cssClass="display-block title-case label label-sm" /></p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Run Date</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"><g:formatDate date="${routineRequest.runDate}"/></p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Value Date</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"><g:formatDate date="${routineRequest.valueDate}"/></p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>

            <!--Routine Request transactions.-->
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Routine Request Transactions
                    </div>
                </div>

                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Txn Type</th>
                            <th>Property</th>
                            <th>Rental Unit</th>
                            <th>Credit Acc</th>
                            <th>Reference</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${routineRequest.transactionBatch?.transactions}" var="transaction">
                        <g:if test="${transaction.parentTransaction}">
                            <tr>
                                <td>${transaction.transactionType}</td>
                                <td>${transaction.property}</td>
                                <td>${transaction.rentalUnit.unitReference}</td>
                                <td>${transaction.accountNumberCr}</td>
                                <td>${transaction.transactionReference}</td>
                                <td style="text-align: right"><g:formatNumber number="${transaction.debit}" format="###,###.00" /></td>
                            </tr>
                        </g:if>
                    </g:each>
                    <g:if test="${!routineRequest.transactionBatch?.transactions}">
                        <tr>
                            <td colspan="6"><mp:noRecordsFound/></td>
                        </tr>
                    </g:if>
                    </tbody>
                    </table>
                    <!-- END FORM-->
                </div>
            </div>

        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/routineRequest/routineRequest-show.js" asset-defer="true"/>

</body>

</html>
