<!-- BEGIN PORTLET-->
<div id="trans-details" class="portlet box blue-hoki">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-bar-chart font-dark hide"></i>
            <span class="caption-subject uppercase">Receipt Details - Batch# { {receiptBatch.batchNumber}}</span>
        </div>

        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Tenant Account</label>
                    <select2 ref="tenantSelect"
                             :select2="tenantOptions"
                             @input="tenantSelected"
                             name="tenant"
                             v-validate="'required'"
                             v-model="selectedTenant">
                        <option value="">Select One...</option>
                    </select2>
                    <span class="has-error help-block">{{ errors.first('tenant') }}</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Debit Account</label>
                    <select2 ref="debitAccount"
                             :select2="debitAccountOptions"
                             @input="debitAccountSelected"
                             class="form-control"
                             name="debitAccount"
                             v-validate="'required'"
                             v-model="selectedDebitAccount">
                        <option value="">Select One...</option>
                    </select2>
                    <span class="has-error help-block">{{ errors.first('debitAccount') }}</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Amount</label>

                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text"
                               name="receiptAmount"
                               placeholder="0.00"
                               v-model.number="receipt.receiptAmount"
                               v-validate="'required|decimal:2|min_value:0.01'"
                               class="form-control "/>
                    </div>
                    <span class="has-error help-block">{{ errors.first('receiptAmount') }}</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Transaction Date</label>
                    <vdate-picker name="transactionDate" v-validate="'required|date_format:YYYY-MM-DD'" v-model="receipt.transactionDate" class="form-control"></vdate-picker>
                    <span class="has-error help-block">{{ errors.first('transactionDate') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Narrative</label>
                    <input type="text"
                           placeholder="Narrative..."
                           name="narrative"
                           v-validate="'required'"
                           v-model="receipt.narrative"
                           class="form-control"/>
                    <span class="has-error help-block">{{ errors.first('narrative') }}</span>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Reference</label>
                    <input type="text"
                           placeholder="Txn Reference"
                           name="transactionReference"
                           v-validate="'required'"
                           v-model="receipt.transactionReference"
                           class="form-control"/>
                    <span class="has-error help-block">{{ errors.first('transactionReference') }}</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-actions">
                    <button type="button"
                            @click="allocateReceipt"
                            id="add-transaction-item"
                            class="btn blue">
                        <i class="fa fa-check-square"></i> Capture Receipt
                    </button>
                    <button @click="resetForm"
                            type="button"
                            id="cancel-mainTransactionTmp-item"
                            class="btn red-mint">
                        <i class="fa fa-refresh"></i> Clear
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PORTLET-->

<!-- the receipt allocation modal part -->
<modal v-model="openAllocations" :append-to-body="true" title="Receipt Allocations" size="lg" ref="modal" id="allocation-modal" :dismiss-btn="false" :keyboard="false" :backdrop="false">
    <receipt-allocation-form @allocation-added="addAllocation" v-if="receipt.tenant != null" :tenant="receipt.tenant" :receipt="receipt"></receipt-allocation-form>

    <div class="portlet light bordered bg-inverse">
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-3 col-xs-6">
                    <label>Txn Count:</label>
                    <span class="form-control-static">{{receiptAllocations.length}}</span>
                </div>
                <div class="col-md-3 col-xs-6">
                    <label>Allocated Total:</label>
                    <span class="form-control-static">$ {{allocatedTotal | formatNumber(2)}}</span>
                </div>
                <div class="col-md-3 col-xs-6">
                    <label>Receipt Amount:</label>
                    <span class="form-control-static">$ {{receipt.receiptAmount | formatNumber(2)}}</span>
                </div>
                <div class="col-md-3 col-xs-6">
                    <label>Txn Ref:</label>
                    <span class="form-control-static "><span class="label label-sm label-default bold">{{receipt.transactionReference}}</span></span>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
        <tr>
            <th>Sub Account</th>
            <th>Amount</th>
            <th>Lease Unit</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="(receiptAllocation, idx) in receiptAllocations" :key="idx">
            <td>{{receiptAllocation.subAccount.text}}</td>
            <td>{{receiptAllocation.allocatedAmount | formatNumber(2)}}</td>
            <td>{{receiptAllocation.lease.text}}</td>+
            <td>
                <button type="button" class="btn btn-xs red-mint" @click="removeAllocation(receiptAllocation, idx)"><i class="fa fa-trash"></i> Remove</button>
            </td>
        </tr>
        </tbody>
    </table>

    <div slot="footer">
        <btn type="danger" @click="discardReceipt"><i class="fa fa-times"></i> Discard Receipt</btn>
        <btn type="success" :disabled="isAllocated == false" @click="postReceipt"><i class="fa fa-save"></i> Post Receipt</btn>
    </div>
</modal>
<!-- the receipt allocation modal part -->