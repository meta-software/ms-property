<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'receiptBatchTmp.label', default: 'Receipt')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'receiptBatchTmp.list.label', default: entityName, args: [receiptBatchTmp])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="receipt-batch-list">
    <div ref="receiptBatchListComp">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <page-bar></page-bar>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered margin-top-10">
                    <div class="portlet-title">
                        <div class="caption red-mint">
                            <i class="fa fa-circle-o-notch"></i>
                            <span class="caption-subject">Receipt batches</span>
                        </div>
                        <div class="actions">
                            <button type="button" class="btn btn-sm red-sunglo" disabled="disabled"><i class="fa fa-trash"></i> Delete Selected</button>
                            <button type="button" class="btn btn-sm blue-hoki" @click="createReceiptBatch"><i class="fa fa-plus"></i> New Batch</button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead class="bg-grey-cararra">
                                <tr>
                                    <th>Batch Nbr</th>
                                    <th>Batch Type</th>
                                    <th>Date Created</th>
                                    <th>Transactions Count</th>
                                    <th>Created By</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="receiptBatch in receiptBatches">
                                    <td>
                                        <router-link :to="{name: 'show', params: {id: receiptBatch.id}}"
                                                     style="display: block">{{receiptBatch.batchReference}}</router-link>
                                    </td>
                                    <td>{{receiptBatch.transactionType.name}}</td>
                                    <td>{{receiptBatch.dateCreated | formatDate('YYYY-MM-DD')}}</td>
                                    <td>{{receiptBatch.transactionsCount}}</td>
                                    <td>{{receiptBatch.createdBy}}</td>
                                    <td>
                                        <div class="actions">
                                            <button @click="removeReceiptBatch(receiptBatch.id)"
                                                    type="button"
                                                    class="btn btn-xs btn-danger">
                                                <i class="fa fa-trash"></i> Delete
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr v-show="receiptBatches.length == 0">
                                    <td colspan="6">
                                        No Records Found...
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="receipt-batch-app">
    <div>
        <router-view></router-view>
    </div>
</template>

<template id="receipt-batch-show">
    <section>
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="page-bar">
            <div class="page-bar-header">
                <h4><i class="fa fa-arrow-left" style="cursor: pointer" title="Back" @click="$router.go(-1)"></i> Receipting</h4>
            </div>

            <div class="page-toolbar">
                <button type="button" class="btn btn-sm red-mint" @click="discardReceiptBatch(id)">
                    <i class="fa fa-times"></i> Discard Batch
                </button>
                <button type="button" class="btn btn-sm green" @click="postReceiptBatch(id)">
                    <i class="fa fa-check"></i> Post Batch
                </button>
            </div>
        </div>

        <div class="row margin-top-10">
            <div class="col-md-12">
                <receipt-form @receipt-deleted="discardReceipt" @receipt-posted="receiptPosted" :batch-id="receiptBatch.id"></receipt-form>

                <receipt-batch-summary></receipt-batch-summary>

                <receipt-list ref="receiptsList" :batch-id="receiptBatch.id"></receipt-list>
            </div>
        </div>
    </section>
</template>

<template id="receipt-batch-summary">
    <div class="portlet light bordered bg-inverse">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <label>Receipts Count </label>
                    <div>0</div>
                </div>
                <div class="col-md-3 col-xs-6 col-sm-4">
                    <label>Batch Total </label>
                    <div>$ 0.00</div>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="receipt-list">
    <div class="portlet box grey-mint">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject">Receipt Transactions</span>
            </div>
        </div>
        <div class="portlet-body">

            <table class="table table-bordered table-condensed table-hover table-striped">
                <thead>
                <tr>
                    <th style="width: 20%;">Credit Acc</th>
                    <th style="width: 20%;">Debit Acc</th>
                    <th>Reference</th>
                    <th>Txn Date</th>
                    <th style="width: 25%;">Narrative</th>
                    <th>Amount</th>
                    <th><i class="fa fa-trash font-dark"></i></th>
                </tr>
                </thead>
                <tbody class="trans-list-tbody">
                <tr v-for="(receipt, idx) in receipts" :key="idx">
                    <td>{{receipt.tenant.accountNumber}} : {{receipt.tenant.accountName}}</td>
                    <td>{{receipt.debitAccount.accountNumber}} : {{receipt.debitAccount.accountName}}</td>
                    <td>{{receipt.transactionReference}}</td>
                    <td>{{receipt.transactionDate | formatDate('YYYY-MM-DD')}}</td>
                    <td :title="receipt.narrative">{{ receipt.narrative | shortify(40) }}</td>
                    <td>{{receipt.receiptAmount | formatNumber(2)}}</td>
                    <th>
                        <button type="button" @click="deleteTransaction($event, receipt)" class="btn btn-xs">
                            <i class="fa fa-trash font-red-mint"></i>
                        </button>
                    </th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>

<template id="receipt-form">
    <section>
        <g:render template="receipt-form-vue"></g:render>
    </section>
</template>

<template id="receipt-allocation-form">
    <section>
        <g:render template="receipt-allocation-vue" />
    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Receipt Batches
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">

                <router-link class="btn btn-sm blue-hoki" to="/"> <i class="fa fa-list"></i> List Receipt Batches</router-link>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/receiptBatchTmp/receipt-batch-tmp-app.js" asset-defer="true"/>

</body>
</html>
