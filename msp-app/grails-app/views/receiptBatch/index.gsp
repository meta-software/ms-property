<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'receiptBatch.label', default: 'Suppliers')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'receiptBatch.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="receiptBatch.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="main-app">

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover table-condensed">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Batch Number</th>
                    <th>Date Created</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${receiptBatchList}" var="receiptBatch">
                    <tr>
                        <td>${receiptBatch.id}</td>
                        <td>${receiptBatch.batchNumber}</td>
                        <td>${receiptBatch.dateCreated.format('dd MMM, yyyy')}</td>
                        <td>
                            <g:link class="display-block" controller="receiptBatch" action="post-batch" id="${receiptBatch.id}" >${receiptBatch.batchNumber}</g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>

</div>

</body>
</html>