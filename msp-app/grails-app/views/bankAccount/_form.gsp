<!-- BEGIN FORM PORTLET-->
<div class="portlet light">

    <div class="portlet-body form">

        <div class="tab-pane active" id="tab0">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group${bank.errors['bankName'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Bank Name
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="bankName" value="${bank?.bankName}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group${bank.errors['bankCode'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Bank Code
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="bankCode" value="${bank?.bankCode}"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group${bank.errors['active'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Is Active?
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <div class="mt-checkbox-inline">
                                <g:checkBox class="form-control icheck" name="active" checked="${bank.active}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${bank.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>

    </div>
</div>

<!-- END FORM PORTLET-->