<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'accountType.label', default: 'Account Type Configurations')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'accountType.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="accountType.list.label" args="[entityName]"/></title>
</head>

<body>

<div id="app"></div>

<template id="acc-type-app">
    <div>

        <g:render template="page_bar"/>

        <g:if test="${flash.message}">
            <g:render template="/templates/flash_message"/>
        </g:if>

        <router-view></router-view>
    </div>
</template>

<template id="acc-type-list-comp">

    <div class="row">
        <div class="col-md-12">

            <div class="portlet">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-bordered table-hover table-advance">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                                <th>Status</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-if="accountTypeList.length > 0" v-for="accountType in accountTypeList">
                                <td>{{accountType.name}}</td>
                                <td>{{accountType.code}}</td>
                                <td>
                                    <span class="label label-sm"
                                          v-bind:class="{'label-info':accountType.active, 'label-danger':!accountType.active}">
                                        {{accountType.active ? 'Active' : 'InActive'}}
                                    </span>
                                </td>
                                <td>{{accountType.description}}</td>
                                <td>
                                    <div class="actions">
                                        <router-link :to="{name: 'edit', params:{id: accountType.id}}"
                                                     class="btn btn-xs btn-warning">
                                            <i class="fa fa-edit"></i> Edit
                                        </router-link>
                                    </div>
                                </td>
                            </tr>
                            <tr v-if="accountTypeList.length == 0">
                                <td colspan="5">No Records Found...</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="pagination">
            </div>

        </div>
    </div>

</template>

<template id="acc-type-form-comp">
    <g:form action="save" method="POST" role="form" class="form-horizontal">
        <g:hiddenField name="id" value="" v-model="accountType.id"/>
        <!-- BEGIN FORM PORTLET-->
        <div class="portlet light">

            <div class="portlet-body form">

                <div class="tab-pane active" id="tab0">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text"
                                             name="name"
                                             v-model="accountType.name"
                                             value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Code
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text" name="code"
                                             v-model="accountType.code"
                                             value=""/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Description
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <g:field class="form-control" type="text" name="description"
                                             v-model="accountType.description"
                                             value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label col-md-3">Is Active?
                                    <span class="required">*</span>
                                </label>

                                <div class="col-md-9">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" v-model="accountType.active" value="1"
                                                   name="active"/>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions right">
                    <button @click="$router.push({name: 'home'})" type="button" class="btn default">Cancel</button>

                    <button @click="onSubmit" type="button" class="btn green" name="create" v-show="accountType.id == null">
                        ${message(code: 'default.button.create.label', default: 'Submit')}
                    </button>
                    <button @click="onUpdate" type="button" class="btn green" name="create"
                            v-show="accountType.id != null">
                        ${message(code: 'default.button.update.label', default: 'Update')}
                    </button>
                </div>

            </div>
        </div>
        <!-- END FORM PORTLET-->
    </g:form>
</template>

<template id="acc-type-edit-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Account Type</div>
                </div>

                <div class="portlet-body">
                    <acc-type-form-comp v-on:save="save" v-bind:account-type="accountType"></acc-type-form-comp>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="acc-type-create-comp">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-anchor"></i> Account Type</div>
                </div>

                <div class="portlet-body">
                    <acc-type-form-comp v-on:save="save" v-bind:account-type="accountType"></acc-type-form-comp>
                </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js"/>
<asset:javascript src="vue-apps/accountType/account-type-app.js" asset-defer="true"/>

</body>
</html>