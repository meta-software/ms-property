<table class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        <th>Account Name</th>
        <th>Bank</th>
        <th>Account Number</th>
        <th>Branch</th>
        <th>Branch Code</th>
        <th>Active?</th>
        <!--th>Actions</th -->
    </tr>
    </thead>
    <tbody>
    <g:if test="${bankAccountList}">
        <g:each in="${bankAccountList}" var="bankAccount" >
            <tr>
                <td><g:link style="display: block" controller="bankAccount" action="edit" id="${bankAccount.id}" >
                    ${bankAccount.accountName}
                </g:link>
                </td>
                <td>${bankAccount.bank}</td>
                <td>${bankAccount.accountNumber}</td>
                <td>${bankAccount.branch}</td>
                <td>${bankAccount.branchCode}</td>
                <td>
                    <span class="label label-sm label-${mp.boolStatusClass(status: bankAccount.active)}">${bankAccount.active ? 'Active' : 'Inactive'}</span>
                </td>
                <!-- td>
                    <div class="actions">
                        <g:link action="edit" id="${bankAccount.id}"  class="btn btn-xs btn-warning"  >
                            <i class="fa fa-edit"></i> Edit
                        </g:link>
                    </div>
                </td -->
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="6">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
