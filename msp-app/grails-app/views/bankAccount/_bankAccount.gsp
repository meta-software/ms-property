<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Bank</label>
            <div class="form-control-static">${bankAccount.bank}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Account Name</label>
            <div class="form-control-static">${bankAccount.accountName}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Account Number</label>
            <div class="form-control-static">${bankAccount.accountNumber}</div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Branch</label>
            <div class="form-control-static">${bankAccount.branch}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Branch Code</label>
            <div class="form-control-static">${bankAccount.branchCode}</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Active ?</label>
            <div class="form-control-static"><span class="mc-status label label-${mp.boolStatusClass(status: bankAccount.active)}">${bankAccount.active ? 'Active' : 'Inactive'}</span></div>
        </div>
    </div>
</div>
