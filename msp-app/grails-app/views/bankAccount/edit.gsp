<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'bank.label', default: 'Bank')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'bank.edit.label', default: entityName, args: [bank])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>

</head>

<body>
<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.bank}">
    <g:render template="/templates/errors" model="['obj': this.bank]"/>
</g:hasErrors>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-anchor"></i> Bank</div>
            </div>

            <div class="portlet-body">
                <g:form resource="${this.bank}" action="update" method="PUT" role="form" class="form-horizontal">
                    <g:hiddenField name="id" value="${this.bank.id}"/>
                    <g:hiddenField name="version" value="${this.bank?.version}"/>

                    <g:render template="form"/>
                </g:form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
