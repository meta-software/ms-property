<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-details" class="portlet box blue-hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Transaction Details</span>
                </div>
                <div class="tools">
                    <a href="" class="reload" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div id="debit-form" class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bar-chart font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Debit</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label>GL Account </label>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox"> GL Account ?
                                                <g:checkBox name="debitGlAccount" value="1" checked="${params['debitGlAccount']}" />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Debit Account</label>
                                            <g:textField name="debitAccount" value="${mainTransactionTmp?.debitAccount}" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Debit Sub Account</label>
                                            <g:textField name="debitSubAccount" value="${mainTransactionTmp?.debitSubAccount}" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                    <div class="col-md-6">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet light bordered" id="credit-form">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bar-chart font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Credit</span>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-3">

                                        <div class="form-group">
                                            <label>GL Account </label>
                                            <div class="mt-checkbox-list">
                                                <label class="mt-checkbox"> GL Account ?
                                                <g:checkBox name="creditGlAccount" value="1" checked="${params['creditGlAccount']}" />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Credit Account</label>
                                            <g:textField name="creditAccount" value="${mainTransactionTmp?.creditAccount}" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Credit Sub Account</label>
                                            <g:textField name="creditSubAccount" value="${mainTransactionTmp?.creditSubAccount}" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Amount</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <g:textField name="amount"
                                             value="${mainTransactionTmp.amount}"
                                             placeholder="0.00"
                                             class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Transaction Date</label>
                            <g:textField name="transactionDate"
                                         class="form-control"
                                         value="${formatDate(format:'yyyy-05-05',date:mainTransactionTmp?.transactionDate)}"
                                         placeholder="MM/dd/yyyy" />
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Reference</label>

                            <g:textField name="transactionReference"
                                         value="${mainTransactionTmp?.transactionReference}"
                                         class="form-control" />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Description</label>

                            <g:textField name="description"
                                         value="${mainTransactionTmp.description}"
                                         class="form-control" />
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-actions">
                            <button type="submit" id="add-transaction-item" class="btn blue"><i class="fa fa-plus"></i> Add Transaction Item</button>
                            <button type="button" id="cancel-mainTransactionTmp-item" class="btn default"><i class="fa fa-times"></i> Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
