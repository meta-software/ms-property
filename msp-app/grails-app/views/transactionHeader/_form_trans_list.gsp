<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div id="trans-list" class="portlet box grey-mint">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject uppercase">Transactions List</span>
                </div>
                <div class="tools">
                    <a href="" class="reload" data-original-title="" title=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                    <tr>
                        <th>Trans Id</th>
                        <th>Debit Acc</th>
                        <th>Credit Acc</th>
                        <th>Reference</th>
                        <th>Date</th>
                        <th>Trans Details</th>
                        <th>Amount</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody class="trans-list-tbody">
                    <tr>
                        <td>1</td>
                        <td>Telecel Zimbabwe</td>
                        <td>Barclays Bank</td>
                        <td>00525415</td>
                        <td>01/May/2018</td>
                        <td>Rent payment...</td>
                        <td>$ 125.58</td>
                        <th><a href="#"><i class="fa fa-trash font-red-haze"></i></a></th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>