<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'mainTransactionTmp.label', default: 'Transaction')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'mainTransactionTmp.create.label', default: entityName, args: [mainTransactionTmp])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>
<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.mainTransactionTmp}">
    <g:render template="/templates/errors" model="['obj': this.mainTransactionTmp]"/>
</g:hasErrors>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light margin-top-10">

            <div class="portlet-body form">
                <g:form action="save" method="POST" role="form">
                    <g:render template="form_details"/>
                </g:form>

                <g:form action="commit" method="POST" role="form">
                    <g:render template="form_trans_list"/>

                    <div class="form-actions left">
                        <button name="update" class="btn green" ><i class="fa fa-save"></i> Save Transaction</button>
                        <button type="button" id="cancel-transaction" class="btn default"><i class="fa fa-times"></i> Cancel</button>
                    </div>
                </g:form>
            </div>

        </div>
    </div>
</div>

</body>
</html>
