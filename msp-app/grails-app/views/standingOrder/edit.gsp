<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'standingOrder.label', default: 'Standing Order')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'standingOrder.edit.label', default: entityName, args: [standingOrder])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>

</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<g:hasErrors bean="${this.standingOrder}">
    <g:render template="/templates/errors" model="['obj': this.standingOrder]"/>
</g:hasErrors>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered margin-top-10">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-money"></i> Standing Order Details</div>
            </div>

            <div class="portlet-body">
                <g:form resource="${this.standingOrder}" action="update" method="PUT" role="form" class="form-horizontal">
                    <g:hiddenField name="id" value="${this.standingOrder.id}"/>
                    <g:hiddenField name="version" value="${this.standingOrder?.version}"/>

                    <g:render template="form"/>

                </g:form>
            </div>
        </div>
    </div>
</div>

</body>
</html>
