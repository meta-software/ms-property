<!-- BEGIN FORM PORTLET-->
<div class="portlet light">

    <div class="portlet-body form">

        <div class="tab-pane active" id="tab0">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group${standingOrder.errors['paycode'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3">Paycode
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:select name="paycode"
                                      class="form-control select2"
                                      optionKey="id"
                                      noSelection="['null': 'Paycode...']"
                                      value="${standingOrder.paycode?.id}"
                                      from="${metasoft.property.core.Bank.list()}"
                            />
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group${standingOrder.errors['reference'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3"> Reference
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="reference" value="${standingOrder?.reference}"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group${standingOrder.errors['amount'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3"> Amount
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="amount"
                                     value="${standingOrder.amount}"/>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group${standingOrder.errors['description'] ? ' has-error' : ''}">
                        <label class="control-label col-md-3"> Description
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <g:field class="form-control" type="text" name="description"
                                     value="${standingOrder.description}"/>

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-actions right">
            <button type="button" class="btn default">Cancel</button>

            <g:if test="${standingOrder.id == null}">
                <g:submitButton name="create" class="btn green"
                                value="${message(code: 'default.button.create.label', default: 'Submit')}"/>
            </g:if>
            <g:else>
                <g:submitButton name="update" class="btn green"
                                value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            </g:else>
        </div>

    </div>
</div>
<!-- END FORM PORTLET-->