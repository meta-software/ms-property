<%@ page import="metasoft.property.core.SecPermission" %>
<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'secRole.label', default: 'User Roles')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'secRole.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="secRole.list.label" default="User Roles" args="[entityName]"/></title>
</head>

<body>

<div id="main-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <form class="form-horizontal">
    <div class="row">
        <div class="col-md-3 margin-top-30">
            <h4>Available Roles </h4>
            <select @change="onSelectRole" v-model="selectedRoleIdx" class="form-control" size="30">
                <option v-for="(option, idx) in userRoles" :value="idx">{{option.name}}</option>
            </select>
        </div>
        <div class="col-md-3 margin-top-30">
            <h4 class="bold">Applied Permissions</h4>
            <select class="form-control" size="30" readonly="readonly">
                <option v-for="(option, idx) in selectedRole.permissions" :value="idx">{{option.name}}</option>
            </select>
        </div>
        <div class="col-md-3 margin-top-30">
            <h4 class="bold">Available Permissions</h4>
            <g:select disabled="disabled" class="form-control" name="secPermission" from="${secPermissions}" multiple="true" size="30" />
        </div>
    </div>
    </form>

    <asset:javascript src="application-vue.js" asset-defer="true"/>
    <asset:javascript src="vue-apps/user-role/user-role-index.js" asset-defer="true"/>
</div>
</body>
</html>