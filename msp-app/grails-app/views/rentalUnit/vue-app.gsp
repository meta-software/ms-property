<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'rentalUnit.label', default: 'Rental Unit')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'rentalUnit.list.label', default: entityName, args: [entityName])}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="rentalUnit.list.label" args="[entityName]"/></title>
</head>

<body>

<!-- begin: app -->
<div id="app"></div>

<!-- begin: rental-unit-app -->
<template id="rental-unit-app">
    <section>
        <page-bar></page-bar>

        <router-view></router-view>
    </section>
</template>
<!-- begin: rental-unit-app -->

<!-- begin: page-par template -->
<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4><i class="fa fa-calendar-times-o"></i> Rental Units
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>
    </div>
</template>
<!-- end: page-bar template -->

<!-- begin: rental-unit-list template -->
<template id="rental-unit-list">
    <div class="search-page search-content-4 margin-top-10">
        <div class="search-bar bordered">
            <form role="form" autocomplete="off" @submit.prevent="search">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <input type="text" v-model="params.query" class="form-control" placeholder="Rental Unit...." >
                    </div>
                    <div class="col-lg-2 col-md-2 extra-buttons">
                        <button class="btn grey-steel uppercase bold" type="submit" @click.prevent="search">Search</button>
                        <button class="btn green" type="button"><i class="fa fa-refresh" @click="reset"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <div class="search-table table-responsive">
            <table class="table table-condensed table-bordered table-striped table-hover">
                <thead class="bg-grey-cararra">
                <tr>
                    <th style="width: 10%;">Name</th>
                    <th style="width: 10%;">Reference</th>
                    <th style="width: 35%;">Property</th>
                    <th style="width: 10%;">Area</th>
                    <th style="width: 15%;">Occupation</th>
                    <th style="width: 20%;">Occupied By</th>
                </tr>
                </thead>
                <tbody>
                    <tr v-for="(rentalUnit, idx) in rentalUnits" key="idx">
                    <td>{{rentalUnit.name}}</td>
                    <td>{{rentalUnit.unitReference}}</td>
                    <td>{{rentalUnit.property.name}}</td>
                    <td>{{rentalUnit.area | formatNumber(2)}} M<sup>2</sup></td>
                    <td>{{rentalUnit.isOccupied}}</td>
                    <td><span class="">{{rentalUnit.currentTenant}}</span></td>
                </tr>
                <tr v-if="rentalUnits.length == 0">
                    <td colspan="6">
                        <g:message message="default.records.notfound" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</template>
<!-- end: rental-unit-list template -->

<!-- begin show -->
<template id="rental-unit-show">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Rental Unit Details
                    </div>

                    <div class="actions">
                        <button type="button" class="btn btn-danger btn-sm" @click="$router.go(-1)"><i class="fa fa-arrow-left"></i> Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
<!-- end show-->

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/rentalUnit/rental-unit-app.js" asset-defer="true"/>

</body>
</html>