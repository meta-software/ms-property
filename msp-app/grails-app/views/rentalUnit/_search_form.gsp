<%@ page import="metasoft.property.core.PropertyManager" %>
<%@ page import="metasoft.property.core.PropertyType" %>
<%@ page import="metasoft.property.core.AccountType" %>

<!-- BEGIN Portlet PORTLET-->
<div class="portlet box yellow m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off" method="GET">
            <div class="form-body">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Search Query</label>
                            <input type="text" class="form-control" name="q" value="${params?.q}">
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-actions">
                <button type="submit" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->