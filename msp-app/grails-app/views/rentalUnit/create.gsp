<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'property.label', default: 'Rental Unit')}" />
    <g:set var="pageTitle" value="${message(code: 'default.create.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

    <!--g:render template="index_menu" /-->

    <div id="create-rentalUnit" class="content scaffold-create" role="main">

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

        <g:hasErrors bean="${this.rentalUnit}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.rentalUnit}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
        </g:hasErrors>

        <g:render template="form" bean="rentalUnit" />

    </div>

    </body>
</html>
