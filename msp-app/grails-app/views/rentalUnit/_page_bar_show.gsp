<div class="page-bar-show">
    <div class="page-bar-show-title">
        <div class="page-bar-show-header">
            <h4 class="page-bar-title">
                Rental Unit : ${rentalUnit.name}
                <small class="grey-cararra">[${rentalUnit.unitReference}]</small>
            </h4>
            <div class="page-bar-show-subtitle">
                <span>${rentalUnit.property.name}</span>
                | <mp:makerCheck value="${rentalUnit.dirtyStatus}" />
            </div>
        </div>
        <div class="page-toolbar">
            <g:link controller="rentalUnit" class="btn btn-sm default" ><i class="fa fa-list"></i> Rental Units</g:link>
        </div>
    </div>

    <!--div class="page-bar-show-actions">
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Edit</a>
        <a href="#" class="btn-link link" ><i class="fa fa-edit"></i> Manage Properties</a>
    </div-->
</div>