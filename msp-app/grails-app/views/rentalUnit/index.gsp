<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'rentalUnit.label', default: 'RentalUnit')}" />
    <g:set var="pageHeadTitle" value="${message(code: 'rentalUnit.list.label', default: entityName, args: [entityName])}" scope="request" />
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

</head>

<body>
<div id="main-app">
    <g:render template="page_bar" />

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <g:render template="list" />
        </div>
    </div>

</div>

</body>
</html>