<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'rentalUnit.label', default: 'Rental Unit')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'default.edit.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>

</head>

<body>

<div id="main-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered margin-top-10">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-building-o"></i> Rental Unit Form</div>
                </div>

                <div class="portlet-body form">
                    <g:form resource="${this.rentalUnit}"
                            method="PUT"
                            class="form-horizontal" name="rentalUnit-form">
                        <g:hiddenField name="id" value="${this.rentalUnit.id}"/>
                        <g:hiddenField name="version" value="${this.rentalUnit?.version}"/>

                        <g:render template="form"/>
                    </g:form>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>
