<div class="search-page search-content-4">
    <loading :active.sync="loader.isLoading"
             :can-cancel="false"
             :is-full-page="loader.fullPage" ></loading>
    <div class="search-bar bordered">
        <form role="form" autocomplete="off" @submit.prevent="search">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" class="form-control" placeholder="Query Lease..."  v-model="params.q">
                </div>
                <div class="col-lg-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="button" @click="search">Search</button>
                    <button class="btn green" type="button" @click="reset"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <!-- add table container here -->
        <table data-toggled="table" data-search="false" class="table table-bordered table-striped table-hover table-condensed table-advanced">
            <thead>
            <tr>
                <th>Lease Number</th>
                <th>Tenant</th>
                <th>Status</th>
                <th>Start Date</th>
                <th>Expiry Date</th>

            </tr>
            </thead>
            <tbody>
            <g:if test="${leaseList}">
                <g:each in="${leaseList}" var="lease">
                    <tr>
                        <td><g:link style="display: block" action="show" controller="lease" id="${lease.id}" >
                            ${lease.leaseNumber}
                        </g:link>
                        </td>
                        <td>${lease.tenant}</td>
                        <td>
                            <span class="label label-sm mc-status label-${mp.boolStatusClass(status: (lease.leaseStatus.code =='active'))}">${lease.leaseStatus.name}</span>
                        </td>
                        <td><g:formatDate date="${lease.leaseStartDate}" format="dd-MMM-yyyy" /></td>
                        <td><g:formatDate date="${lease.leaseExpiryDate}" format="dd-MMM-yyyy" /></td>

                    </tr>
                </g:each>
            </g:if>
            </tbody>
        </table>
        <!-- add table container here -->
    </div>
</div>
