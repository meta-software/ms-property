<!-- BEGIN FORM PORTLET-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group${rentalUnit.errors['name'] ? ' has-error' : ''}">
            <label class="control-label col-md-3">${message(code: 'label.rentalUnit.name', default: 'Unit Name')}
            </label>

            <div class="col-md-9">
                <g:field class="form-control" type="text" name="name" value="${this.rentalUnit.name}" />
                <g:if test="${rentalUnit.errors['area']}">
                    <span class="has-error help-block help-block-error">
                        <g:fieldError bean="${rentalUnit}" field="name" />
                    </span>
                </g:if>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group${rentalUnit.errors['area'] ? ' has-error' : ''}">
            <label class="control-label col-md-3">${message(code: 'label.rentalUnit.area', default: 'Area')}
            </label>

            <div class="col-md-9">
                <div class="input-group input-icon input-medium">
                    <g:field class="form-control " type="text" name="area" value="${this.rentalUnit.area}" />
                    <span class="input-group-addon">M<sup>2</sup></span>
                </div>
                <g:if test="${rentalUnit.errors['area']}">
                    <span class="has-error help-block help-block-error">
                        <g:fieldError bean="${rentalUnit}" field="area" />
                    </span>
                </g:if>
            </div>
        </div>
    </div>

</div>

<!-- BEGIN FORM PORTLET-->
<div class="row">
    <div class="col-md-6">
        <div class="form-group${rentalUnit.errors['unitReference'] ? ' has-error' : ''}">
            <label class="control-label col-md-3">
                ${message(code: 'label.rentalUnit.unitReference', default: 'Unit Reference')}
            </label>

            <div class="col-md-9">
                <g:field class="form-control" type="text" name="unitReference" value="${this.rentalUnit.unitReference}" />
                <g:if test="${rentalUnit.errors['unitReference']}">
                    <span class="has-error help-block help-block-error">
                        <g:fieldError bean="${rentalUnit}" field="unitReference" />
                    </span>
                </g:if>
            </div>
        </div>
    </div>
</div>

<div class="form-actions right">
    <g:link controller="rentalUnit" action="show" id="${rentalUnit.id}" class="btn default">Cancel</g:link>
    <g:submitButton name="create" class="btn green" value="${message(code: 'default.button.update.label', default: 'Update')}" />
</div>
<!-- END FORM PORTLET-->
