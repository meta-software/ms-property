<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-3">
                    <g:select optionKey="id"
                              placeholder="Property"
                              noSelection="['': 'Property...']"
                              class="form-control select2"
                              name="property"
                              value="${params?.property}"
                              from="${propertyList}"/>
                </div>
                <div class="col-lg-3">
                    <g:select optionKey="id"
                              placeholder="Status"
                              noSelection="['': 'Status...']"
                              class="form-control select2"
                              name="status"
                              value="${params?.status}"
                              from="${rentalUnitStatuses}"/>
                </div>
                <div class="col-lg-3">
                    <input type="text" name="q" value="${params.q}" class="form-control" placeholder="Search Query...">
                </div>
                <div class="col-lg-3 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-condensed table-bordered table-striped table-hover">
            <thead class="bg-grey-cararra">
            <tr>
                <th style="width: 10%;">Name</th>
                <th style="width: 10%;">Reference</th>
                <th style="width: 35%;">Property</th>
                <th style="width: 10%;">Area</th>
                <th style="width: 15%;">Occupation</th>
                <th style="width: 20%;">Occupied By</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${rentalUnitList}">
                <g:each in="${rentalUnitList}" var="rentalUnit">
                    <tr>
                        <td>
                            <g:link controller="rentalUnit" action="show" class="display-block" resource="${rentalUnit}">${rentalUnit.name}</g:link>
                        </td>
                        <td>${rentalUnit.unitReference}</td>
                        <td>
                            <g:link controller="property" action="show" class="display-block" resource="${rentalUnit.property}">${rentalUnit.property}</g:link>
                        </td>
                        <td><g:formatNumber number="${rentalUnit.area}" format="###,###" /> M<sup>2</sup></td>
                        <td>
                            <span class="label label-${mp.boolStatusClass(status: rentalUnit.isOccupied)} mc-status"> ${rentalUnit.isOccupied ? 'Occupied' : 'Vacant'} </span>
                        </td>
                        <td>
                            <span class="">${ rentalUnit.getActiveLease()?.tenant?.accountName ?: '(none)'}</span>
                        </td>
                    </tr>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="6">
                        <g:message message="default.records.notfound" />
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${rentalUnitCount ?: 0}" params="${params}" />
    </div>
</div>
