<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'rentalUnit.label', default: 'Rental Unit')}"/>
    <g:set var="pageHeadTitle" value="Rental Unit Info - ${rentalUnit.name}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <g:set var="lease" value="${rentalUnit.activeLease}" scope="request"/>

    <script type="text/javascript">
        let rentalUnitId = ${rentalUnit.id};
    </script>

</head>

<body>

<div id="main-app" >

    <g:render template="page_bar_show"/>

    <div style="display: none" class="entity-details">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="portlet light bg-inverse">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-info"></i> Rental Unit Details
                        </div>
                    </div>

                    <div class="portlet-body">
                        <form class="form-horizontal" role="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name: </label>

                                        <div class="col-md-8">
                                            <div class="form-control-static">${rentalUnit.name}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-5">Reference: </label>

                                        <div class="col-md-7">
                                            <div class="form-control-static">${rentalUnit.unitReference}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Property: </label>

                                        <div class="col-md-8">
                                            <div class="form-control-static">${rentalUnit.property}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-5">Landlord: </label>

                                        <div class="col-md-7">
                                            <div class="form-control-static">${rentalUnit.property.landlord}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- row -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Manager: </label>

                                        <div class="col-md-8">
                                            <div class="form-control-static">${rentalUnit.property.propertyManager}</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- row -->

                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">
        <div class="col-md-8">
            <div class="portlet light bg-inverse entity-details">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Rental Unit Details
                    </div>
                </div>

                <div class="portlet-body">
                    <table class="table table-condensed">
                        <tbody>
                        <tr><th style="width: 20%">Name: </th><td style="width: 30%">${rentalUnit.name}</td><th style="width: 20%">Reference: </th><td>${rentalUnit.unitReference}</td></tr>
                        <tr><th>Property: </th><td><g:link class="display-block" controller="property" action="show" id="${rentalUnit.property.id}">${rentalUnit.property}</g:link></td>
                            <th>Landlord: </th><td><g:link class="display-block" controller="landlord" action="show" id="${rentalUnit.landlord.id}">${rentalUnit.landlord}</g:link></td></tr>
                        <tr><th>Area: </th><td><g:formatNumber number="${rentalUnit.area}" format="###,##.#" /> M<sup>2</sup></td>
                        <th>Property Manager: </th><td><g:link class="display-block" controller="property-manager" action="show" id="${rentalUnit.property.propertyManager.id}">${rentalUnit.property.propertyManager}</g:link></td></tr>
                        <tr><th>Address: </th><td><mp:address address="${rentalUnit.property.address}" /></td>
                        <th>CheckStatus: </th><td><mp:makerCheck value="${rentalUnit.checkStatus}" /></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="portlet light bordered entity-details-list">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-sticky-note"></i> Current Lease
                    </div>
                </div>

                <div class="portlet-body">
                    <g:if test="${lease}">
                        <table class="table table-condensed">
                            <tbody>
                            <tr><th style="width: 40%">Lease No</th><td>${lease.leaseNumber}</td></tr>
                            <tr><th>Tenant</th><td><g:link controller="tenant" action="show" id="${lease.tenant.id}">${lease.tenant}</g:link> </td></tr>
                            <tr><th>Lease Type</th><td>${lease?.leaseType ?: '---'}</td></tr>
                            <tr><th style="width: 40%">Status</th><td><span class="label label-sm label-${mp.boolStatusClass(status: !lease.terminated)}  mc-status">${ lease.terminated ? 'Inactive' : 'Active' }</span></td></tr>
                            <tr><th>Start Date</th><td><g:formatDate date="${lease.leaseStartDate}" /></td></tr>
                            <tr><th>Expiry Date</th><td><g:formatDate date="${lease.leaseExpiryDate}" /></td></tr>
                            </tbody>
                        </table>
                    </g:if>
                    <g:else>
                        <p class="note note-warning"><g:message code="default.records.notfound" /> </p>
                    </g:else>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->

    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i> Lease History
                    </div>
                </div>

                <div class="portlet-body">
                    <!-- BEGIN PROPERTIES LIST-->
                    <g:render template="leaseHistory" model="[leaseList: leaseHistoryList]" />
                    <!-- BEGIN PROPERTIES LIST-->
                </div>
            </div>
        </div>
    </div>

</div>


<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/rentalUnit/rentalUnit-show.js" asset-defer="true" />

</body>
</html>
