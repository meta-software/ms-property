<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'accountCheck.label', default: 'Viewing : Account')}"/>
    <g:set var="pageHeadTitle" value="Viewing Task : Account - ${accountCheck.entity.id}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let itemId = ${accountCheck.id};
    </script>

</head>

<body>

<!-- BEGIN PAGE BASE CONTENT -->
<div id="app">
    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> View Pending Task : Account
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="details" />
                </div>

            </div>
        </div>
    </div>

    <maker-options :item-id="itemId" :status="'${accountCheck.checkStatus}'" :controller="'account-check'" ></maker-options>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box grey-mint">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> Viewing Outbox Task : Account
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="bulkAdjust" bean="${account}" var="account" />
                </div>

            </div>
        </div>
    </div>

</div>
<!-- END PAGE BASE CONTENT -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/accountCheck/account-check-show-outbox.js" asset-defer="true" />

</body>
</html>