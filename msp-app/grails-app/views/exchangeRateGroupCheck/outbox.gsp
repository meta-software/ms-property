<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'exchangeRateGroup.label', default: 'Exchange Rate Group')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'exchangeRateGroup.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="exg-check-outbox-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<!-- exchangerategroup-check-list -->
<template id="exg-check-list">
    <div class="row">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="col-md-12">
            <div class="search-page search-content-4 margin-top-10">
                <div class="search-bar bordered">
                    <form role="form" autocomplete="off">
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <input type="text" class="form-control" name="exchangeRateGroupCheck" placeholder="Exchange Rate Group" value="${params?.accountNumber}">
                            </div>
                            <div class="col-lg-2 col-md-2 extra-buttons">
                                <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                                <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="search-table table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="select-all" class="icheck" />
                            </th>
                            <th>Req ID</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Action</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="exchangeRateGroup in exchangeRateGroupChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                            </td>
                            <td><router-link class="display-block" :to="{name: 'show', params: {id: exchangeRateGroup.id}}">{{exchangeRateGroup.id}}</router-link></td>
                            <td>{{exchangeRateGroup.entity.startDate | formatDate('YYYY-MM-DD')}}</td>
                            <td>{{exchangeRateGroup.entity.endDate | formatDate('YYYY-MM-DD')}}</td>
                            <td>{{exchangeRateGroup.actionName}}</td>
                            <td>{{exchangeRateGroup.dateCreated | formatDate('YYYY-MM-DD')}}</td>
                            <td>{{exchangeRateGroup.makerChecker.maker.fullName}}</td>
                        </tr>
                        <tr v-if="exchangeRateGroupChecks.length == 0">
                            <td colspan="7">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Outbox : Exchange Rate Groups
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Groups</router-link>
            </div>
        </div>
    </div>
</template>

<template id="exg-check-show">
    <div class="portlet light bordered margin-top-10">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="portlet-title">
            <div class="caption"><i class="fa fa-plus"></i> Exchange Rates Check</div>
        </div>

        <div class="portlet-body form">

            <maker-options :controller="'exchange-rate-group-check'" :item-id="exchangeRateGroupCheck.id" :status="exchangeRateGroupCheck.checkStatus"></maker-options>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-3">Start Date:
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <datepicker placeholder="YYYY-MM-DD" v-model="exchangeRateGroup.startDate" class="form-control"></datepicker>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-3">End Date:
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <datepicker placeholder="YYYY-MM-DD" v-model="exchangeRateGroup.endDate" class="form-control"></datepicker>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label col-md-3">Active:
                            <span class="required">*</span>
                        </label>

                        <div class="col-md-9">
                            <input type="checkbox" v-model="exchangeRateGroup.active" name="active"/>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="portlet bordered light">
                        <div class="portlet-title">
                            <div class="caption"><i class="fa fa-list"></i> Exchange Rates</div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Rate</th>
                                </tr>
                                </thead>
                                <tbody>
                                <template v-for="(exchangeRate, index) in exchangeRateGroup.exchangeRates">
                                    <tr>
                                        <td>{{exchangeRate.fromCurrency.id }}</td>
                                        <td>{{exchangeRate.toCurrency.id }}</td>
                                        <td>{{exchangeRate.rate }}</td>
                                    </tr>
                                </template>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn btn-sm blue-hoki" ><i class="fa fa-save"></i> Save</button>
                <button type="button" class="btn btn-sm default" ><i class="fa fa-times"></i> Cancel</button>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/exchangeRateGroupCheck/exchange-rate-group-check-outbox.js" asset-defer="true"/>

</body>
</html>