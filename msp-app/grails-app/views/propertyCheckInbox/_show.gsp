<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyCheck.label', default: 'Viewing : Property')}"/>
    <g:set var="pageHeadTitle" value="Viewing Task : Property - ${propertyCheck.entity.id}" scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.show.label" args="[pageHeadTitle]"/></title>

    <script type="text/javascript">
        let checkId = ${propertyCheck.id};
    </script>

</head>

<body>

<!-- BEGIN PAGE BASE CONTENT -->
<div id="app">
    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-info"></i> View Pending Task : Property
                    </div>
                </div>

                <div class="portlet-body">
                    <g:render template="details" />
                </div>

            </div>
        </div>
    </div>

    <maker-checker :makerid="${propertyCheck.maker.id}" :item-id="checkId" :controller="'property-check-inbox'" :status="'${propertyCheck.checkStatus}'"></maker-checker>

    <g:render template="property" bean="${property}" var="property" />

</div>
<!-- END PAGE BASE CONTENT -->

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/propertyCheck/property-check-show.js" asset-defer="true" />

</body>
</html>