<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th>ID</th>
        <th>Action</th>
        <th>Entity</th>
        <th>Status</th>
        <th>Date Created</th>
        <th>User</th>
    </tr>
    </thead>
    <tbody>
    <g:if test="${propertyCheckList}">
        <g:each in="${propertyCheckList}" var="propertyCheck">
            <tr>
                <td>
                <g:link class="display-block" action="show" id="${propertyCheck.id}" >
                    ${propertyCheck.id}
                </g:link>
                </td>
                <td>${propertyCheck.actionName}</td>
                <td>
                    <g:link class="display-block" action="show" id="${propertyCheck.id}" >${propertyCheck.entity}</g:link>
                </td>
                <td>${propertyCheck.checkStatus}</td>
                <td><g:formatDate date="${propertyCheck.makeDate}" /></td>
                <td>${propertyCheck.maker.username}</td>
            </tr>
        </g:each>
    </g:if>
    <g:else>
        <tr>
            <td colspan="7">
                <mp:noRecordsFound />
            </td>
        </tr>
    </g:else>
    </tbody>
</table>
