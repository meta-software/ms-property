<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPLATE VARS -->
    <g:set var="entityName" value="${message(code: 'propertyCheck.label', default: 'Property')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'propertyTmp.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPLATE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/propertyCheckInbox/property-check-inbox.js" asset-defer="true"/>

<template id="main-app">
    <div>
        <page-bar></page-bar>
        <router-view></router-view>
    </div>
</template>

<template id="property-check-list">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Pending Properties</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 8%;">Request#</th>
                            <th>Name</th>
                            <th style="width: 20%;">City</th>
                            <th style="width: 10%;">Action</th>
                            <th style="width: 10%;">Area</th>
                            <th style="width: 15%;">Date Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(propertyCheck, key) in propertyChecks" key="key">
                            <td>{{propertyCheck.id}}</td>
                            <td>
                                <router-link class="display-block" :to="{name: 'show', params: {id: propertyCheck.id}}">{{propertyCheck.transactionSource.name}}</router-link>
                            </td>
                            <td>{{propertyCheck.transactionSource.address.city}}</td>
                            <td>{{propertyCheck.actionName}}</td>
                            <td>{{propertyCheck.transactionSource.area}}</td>
                            <td>{{propertyCheck.dateCreated | formatDate }}</td>
                         </tr>
                        <tr v-if="propertyChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="trans-batch-check-outbox">
    <div class="inbox margin-top-10">
    <div class="row">
        <div class="col-md-12">
            <div class="inbox-body">
                <div class="inbox-header">
                    <h3 class="pull-left">Outbox Transaction Batches</h3>
                </div>
                <div class="inbox-content">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" id="all" class="icheck" />
                            </th>
                            <th>Batch Reference</th>
                            <th>Batch Number</th>
                            <th>Transaction Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="propertyCheck in propertyChecks">
                            <td>
                                <div class="icheck icheck-default">
                                    <input type="checkbox" id="someCheckboxId" />
                                </div>
                        </th>
                            <td>
                                <router-link class="display-block" :to="{name: 'outbox-item', params: {id: propertyCheck.id}}">{{propertyCheck.batchReference}}</router-link>
                            </td>
                            <td>{{propertyCheck.batchReference}}</td>
                            <td>{{propertyCheck.transactionType.name}}</td>
                            <td>{{propertyCheck.dateCreated | formatDate}}</td>
                            <td>{{propertyCheck.maker.fullName}}</td>
                        </tr>
                        <tr v-if="propertyChecks.length == 0">
                            <td colspan="6">No Records found...</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

</template>

<template id="property-check-show">
    <section>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue-hoki">
                <div class="portlet-title">
                    <div class="caption">
                        Viewing Pending Task : Property
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    ID:
                                </label>
                                <div class="col-md-7">{{propertyCheck.id}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Status:
                                </label>
                                <div class="col-md-7">{{propertyCheck.checkStatus}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Action:
                                </label>
                                <div class="col-md-7">{{propertyCheck.actionName}}</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Entity:
                                </label>
                                <div class="col-md-7">{{propertyCheck.transactionSource.name}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Date Created:
                                </label>
                                <div class="col-md-7">{{propertyCheck.makeDate | formatDate}}</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5">
                                    Maker:
                                </label>
                                <div class="col-md-7">{{propertyCheck.maker.fullName}}</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

        <maker-checker :makerid="propertyCheck.maker.id" :item-id="propertyCheck.id" :controller="'property-check-inbox'" :status="propertyCheck.checkStatus"></maker-checker>

        <property-tmp v-if="propertyCheck.transactionSource.propertyType" :property-tmp="propertyCheck.transactionSource" ></property-tmp>
    </section>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> Inbox : Properties
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <div class="btn-group">
                <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Properties</router-link>
            </div>
        </div>
    </div>
</template>

<template id="property-tmp">
    <!-- BEGIN PORTLET-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered bg-inverse margin-top-10">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-eye"></i> Viewing Property : {{propertyTmp.name}}
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Property Name:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.name}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Area:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.area}} M<sup>2</sup></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Property Type:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.propertyType.name}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Landlord:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.landlord.accountName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Property Manager:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.propertyManager.fullName}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Stand No:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.standNumber}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Commission:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.commission}} %</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Charge VAT?:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.chargeVat}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="col-md-5">
                                                Address:
                                            </label>
                                            <div class="col-md-7">{{propertyTmp.address.street}}, {{propertyTmp.address.suburb}}, {{propertyTmp.address.city}}, {{propertyTmp.address.country}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <rental-units :property="propertyTmp"></rental-units>

        </div>
    </div>
    <!-- END PORTLET-->
</template>

<template id="rental-units">
    <div class="portlet box blue-hoki">
        <div class="portlet-title">
            <div class="caption">
                Rental Units
            </div>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <bootstrap-table
                            :columns="table.columns"
                            :data="table.data"
                            :options="table.options">
                    </bootstrap-table>
                </div>
            </div>
        </div>
    </div>
</template>

</body>
</html>