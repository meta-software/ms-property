<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-mint">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info"></i> Viewing Task : Property
                </div>
            </div>

            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Name:
                            </label>
                            <div class="col-md-8">${property.name}</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Property Type:
                            </label>
                            <div class="col-md-8">${property.propertyType}</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Stand No:
                            </label>
                            <div class="col-md-8">${property.standNumber}</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Area:
                            </label>
                            <div class="col-md-8"><g:formatNumber number="${property.area}" format="###,##0.#" /> M<sup>2</sup></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Landlord:
                            </label>
                            <div class="col-md-8">${property.landlord}</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Manager:
                            </label>
                            <div class="col-md-8">${property.propertyManager}</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Charge VAT:
                            </label>
                            <div class="col-md-8"><mp:boolStatusLabel status="${property.chargeVat}" /> </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Commission:
                            </label>
                            <div class="col-md-8"><g:formatNumber number="${property.commission}" format="##0.##" /> %</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-4">
                                Address:
                            </label>
                            <div class="col-md-8"><mp:address address="${property.address}" /></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-2">
                                Description:
                            </label>
                            <div class="col-md-10">${property.description}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey-mint">
            <div class="portlet-title">
                <ul class="nav nav-tabs pull-left">
                    <li class="active">
                        <a href="#tab0" data-toggle="tab"> Rental Units </a>
                    </li>
                    <li>
                        <a href="#tab1" data-toggle="tab"> Insurance </a>
                    </li>
                </ul>
                <div class="caption pull-right">
                    Details
                </div>
            </div>

            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab0">
                        <g:set var="rentalUnits" value="${property.rentalUnits}" />
                        <div class="scrollable" id="landlord_rentalUnits" style="background-color: #ffffff;">
                            <table data-toggle="table" data-height="300" class="table-condensed table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="5px"><input type="checkbox" name="all" id="all" class="icheckbox_flat-orange"/></th>
                                    <th>Reference</th>
                                    <th>Name</th>
                                    <th>Area</th>

                                </tr>
                                </thead>
                                <tbody>
                                <g:if test="${rentalUnits}">
                                    <g:each in="${rentalUnits}" var="rentalUnit">
                                        <tr>
                                            <td>
                                                <g:if test="${rentalUnit.isChecked()}">
                                                    <input type="checkbox" disabled="disabled" class="icheckbox_flat-orange"/>
                                                </g:if>
                                                <g:else>
                                                    <input type="checkbox" disabled="disabled" name="ru-${rentalUnit.id}" id="ru-${rentalUnit.id}" class="icheckbox_flat-orange"/>
                                                </g:else>
                                            </td>
                                            <td>${rentalUnit.unitReference}</td>
                                            <td>${rentalUnit.name}</td>
                                            <td><g:formatNumber number="${rentalUnit.area}" format="###,###" /> M<sup>2</sup></td>
                                        </tr>
                                    </g:each>
                                </g:if>
                                <g:else>
                                    <tr>
                                        <td colspan="4">
                                            <g:message message="default.records.notfound" />
                                        </td>
                                    </tr>
                                </g:else>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab1">
                        <g:if test="${property.propertyInsurance}">
                            <p class="note note-info">Insurance over here.</p>
                        </g:if>
                        <g:else>
                            <p class="note note-info">No Insurance Set</p>
                        </g:else>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
