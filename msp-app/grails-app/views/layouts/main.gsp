<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:link rel="shortcut icon" href="favicon.ico"/>

    <g:layoutHead/>
    <!--<g:layoutTitle default="Nhakr Property Management System :: Nhakr"/>-->
    <title>
        <g:message code="default.title" default="Nhakr Property Management System :: Nhakr" />
    </title>

    <asset:stylesheet src="application.css"/>

    <g:set var="specificAssetExists" value="${false}"/>
    <g:set var="specificAsset" value="${params.controller}_${params?.action ?: 'index'}.js"/>
    <asset:assetPathExists src="${specificAsset}">
        <g:set var="specificAssetExists" value="${true}"/>
    </asset:assetPathExists>

</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logos page-content-white page-footer-fixed page-sidebar-fixed">
<div class="se-pre-con"></div>
<div id="page-wrapper">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <g:link controller="dashboard">
                    <asset:image src="global/img/nhakr-app-icon.png" alt="NHAKA" style="height: 35px" class="logo-default"/>
                    <div class="logo-text">
                        NHAKR
                    </div>
                </g:link>

                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <g:render template="/templates/top_menu"/>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <g:render template="/templates/left_sidebar"/>
        <!-- END SIDEBAR -->

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">

                <g:layoutBody/>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    <g:render template="/templates/footer"/>
    <!-- END FOOTER -->

</div>

<!--[if lt IE 9]>
<asset:javascript src="lt_ie9.js" />
<![endif]-->

<g:if test="${specificAssetExists}">
    <asset:javascript src="${specificAsset}"/>
</g:if>
<g:else>
    <asset:javascript src="application.js"/>
</g:else>

<asset:deferredScripts />

<script type="text/javascript">
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("fast");
    });
</script>

</body>

</html>