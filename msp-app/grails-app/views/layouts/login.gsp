<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Nhakr::Nhakr - Login"/>

    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <asset:link rel="shortcut icon" href="images/favicon.ico"/>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="pages/css/login-2.css" />

    <g:layoutHead/>
</head>
<!-- END HEAD -->

<body class=" login">

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <g:layoutBody />
    <!-- END LOGIN FORM -->
</div>
<div class="copyright hide"> 2019 © MetasoftWare:Nhakr </div>
<!-- END LOGIN -->

<!--[if lt IE 9]>
<asset:javascript src="lt_ie9.js"/>
<![endif]-->

<asset:javascript src="global/plugins/jquery/jquery.js"/>
<asset:deferredScripts/>

</body>

</html>