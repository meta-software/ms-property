<%@ page import="metasoft.property.core.SecUser" %>
<%@ page import="metasoft.property.core.SecRole" %>

<!-- BEGIN Portlet PORTLET-->
<div class="portlet box yellow m-top-10">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-filter"></i> Filter Users
        </div>
    </div>

    <div class="portlet-body">
        <form role="form" autocomplete="off">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Search Query</label>
                            <input type="text" class="form-control" name="q" value="${params?.q}" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">User Type</label>
                            <g:select
                                    class="form-control select2"
                                    noSelection="['': 'Select User Type...']"
                                    name="usertype"
                                    from="${metasoft.property.core.SecRole.list()}"
                                    value="${params['usertype']}"
                                    optionKey="id"
                                    optionValue="name"
                            />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Show Pending</label>
                            <g:select
                                    class="form-control select2"
                                    name="pending"
                                    from="${[[id:0, name: 'No'], [id:1, name: 'Yes']]}"
                                    value="${params['pending']}"
                                    optionKey="id"
                                    optionValue="name"
                            />
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-actions">
                <button type="submit" class="btn blue-dark"><i class="fa fa-search"></i> Search...</button>
            </div>

        </form>

    </div>
</div>
<!-- END Portlet PORTLET-->