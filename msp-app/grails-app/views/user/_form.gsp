<%@ page import="metasoft.property.core.SecRole" %>

<!-- BEGIN USER DETAILS PORTLET -->
<div class="portlet light bg-inverse margin-top-5">
    <div class="portlet-title">
        <div class="caption">User Details</div>
    </div>

    <div class="portlet-body">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group ${user.errors['username'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Username
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-8">
                        <input v-validate="'required'" v-model="userForm.username" type="text" placeholder="Username" name="username" class="form-control" value="${user.username}"/>
                        <span class="has-error help-block-error help-block">{{ errors.first('username') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group${user.errors['password'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Password
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-8">
                        <input v-validate="'required'" v-model="userForm.password" class="form-control" placeholder="Password" type="password" name="password" value="${params?.password}"/>
                        <span class="has-error help-block-error help-block">{{ errors.first('password') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group${user.errors['firstName'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">First Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-8">
                        <input v-validate="'required'" v-model="userForm.firstName" placeholder="First Name" class="form-control" type="text" name="firstName" value="${user.firstName}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('password') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group ${user.errors['lastName'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Last Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-8">
                        <input v-validate="'required'" v-model="userForm.lastName" class="form-control" placeholder="Last Name" type="text" name="lastName" value="${user.lastName}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('lastName') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group${user.errors['emailAddress'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Email Address</label>

                    <div class="col-md-8">
                        <input v-validate="'required|email'" v-model="userForm.emailAddress" class="form-control" placeholder="Email Address" type="text" name="emailAddress" value="${params?.emailAddress}" />
                        <span class="has-error help-block-error help-block">{{ errors.first('emailAddress') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group${user.errors['employeeId'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Employee ID</label>

                    <div class="col-md-8">
                        <input v-validate="'required'" v-model="userForm.employeeId" type="text" placeholder="Employee ID" name="employeeId" class="form-control" value="${user.employeeId}"/>
                        <span class="has-error help-block-error help-block">{{ errors.first('employeeId') }}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group${user.errors['userType'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        User Type
                    </label>

                    <div class="col-md-8">
                        <g:select
                                v-validate="'required'"
                                v-model="userForm.userType"
                                class="form-control"
                                from="${userRoles}"
                                value="${user.userType?.id}"
                                optionValue="name"
                                optionKey="id"
                                name="userType"
                                placeholder="User Type"
                        ></g:select>
                        <span class="has-error help-block-error help-block">{{ errors.first('userType') }}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group${user.errors['passwordExpired'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Password Expired?
                    </label>

                    <div class="col-md-8">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input v-model="userForm.passwordExpired" disabled="disabled" type="checkbox" name="enabled" value="1"
                                        checked="${user.passwordExpired}"/><span></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5">
                <div class="form-group${user.errors['enabled'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Enabled?
                    </label>

                    <div class="col-md-8">
                        <div class="mt-checkbox mt-checkbox-outline">
                            <input  v-model="userForm.enabled"
                                    disabled="disabled"
                                    type="checkbox" name="enabled"
                                    value="1" checked="${user.enabled}" /><span></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group${user.errors['accountExpired'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Account Expired?
                    </label>

                    <div class="col-md-8">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input v-model="userForm.accountExpired"
                                   disabled="disabled" type="checkbox"
                                   name="enabled" value="1"
                                   checked="${user.accountExpired}"/><span></span>
                        </label>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END USER DETAILS PORTLET -->

<!-- BEGIN USER DETAILS PORTLET -->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">User Roles</div>
    </div>

    <div class="portlet-body">
        <div class="row" v-show="errors.has('authorities')">
            <div class="col-md-12">
                <span class="has-error help-block-error help-block">{{ errors.first('authorities') }}</span>
            </div>
        </div>
        <div class="row">
            <g:each in="${userRoles}" var="role">
                <g:if test="${ !(role.authority in ['ROLE_PROP_ADMIN', 'ROLE_API_USER', 'ROLE_PROP_EXEC', 'ROLE_SUPERUSER', 'ROLE_SUPERUSER'])}">
                    <div class="col-md-3">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input v-validate="'required'" v-model="userForm.authorities" type="checkbox" value="${role.id}" checked="" name='authorities' /><span></span> ${role.name}
                        </label>
                    </div>
                </g:if>
            </g:each>

        </div>
    </div>

</div>
<!-- END USER DETAILS PORTLET -->

<!-- BEGIN FORM PORTLET-->
<div class="form-actions right">
    <g:link controller="user" action="index" class="btn default">Cancel</g:link>

    <g:if test="${user.id == null}">
        <g:submitButton name="create" class="btn green"
                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
    </g:if>
    <g:else>
        <g:submitButton name="update" class="btn green"
                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
    </g:else>
</div>
<!-- END FORM PORTLET-->