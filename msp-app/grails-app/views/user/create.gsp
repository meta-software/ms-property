<!DOCTYPE html>
<html>
<head>
	<!-- BEGIN TEMPALTE VARS -->
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
	<g:set var="pageHeadTitle" value="${message(code: 'user.create.label', default: entityName, args: [user])}"
		   scope="request"/>
	<!-- END TEMPALTE VARS -->

	<meta name="layout" content="main"/>
	<title><g:message code="default.create.label" args="[entityName]"/></title>

</head>

<body>

<div id="main-app">
	<g:render template="page_bar"/>

	<section class="uiv" v-show="!formValid && formSubmitted">
		<alert dismissible @dismissed="formSubmitted=false" type="danger" >
			<b>Invalid Form!</b> The form is invalid, check form for validation results.
		</alert>
	</section>

	<div class="row">
		<div class="col-md-12">

            <div class="portlet-body form">
                <g:form action="javascript:;" method="POST" class="form-horizontal" name="create-user" @submit.prevent="onSaveUser">
                    <g:render template="form" />
                </g:form>
            </div>

		</div>
	</div>

</div>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/user/user-create.js" asset-defer="true"/>

</body>
</html>
