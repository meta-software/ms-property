<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'user.edit.label', default: entityName, args: [user])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>

    <script type="text/javascript">
        var userId = ${user.id};
    </script>

</head>

<body>

<div id="main-app">
    <g:render template="page_bar"/>

    <g:if test="${flash.message}">
        <g:render template="/templates/flash_message"/>
    </g:if>

    <g:hasErrors bean="${this.user}">
        <g:render template="/templates/errors" model="['obj': this.user]"/>
    </g:hasErrors>

    <div class="row">
        <div class="col-md-12">

            <div class="portlet-body form">
                <g:form action="update" id="${this.user.id}" method="PUT" class="form-horizontal" name="edit-user">
                    <g:hiddenField name="id" value="${this.user.id}"/>
                    <g:hiddenField name="version" value="${this.user?.version}"/>
                    <g:render template="edit_form" />
                </g:form>
            </div>

        </div>
    </div>

</div>

<template id="change-password-form">
    <section>
        <button type="button" @click="openModal=true" class="btn btn-sm blue-hoki" >
            <i class="fa fa-rotate-left"></i> Change Password
        </button>
        <modal v-model="openModal" size="md" ref="updatePasswordModal" id="update-password-form-modal" @hide="passwordModalCallback" >
            <span slot="title"><i class="fa fa-rotate-left"></i> Change Password</span>

            <form @submit.prevent="updatePassword" ref="updatePasswordForm" action="javascript:;" method="POST" >
                <input type="hidden" name="id" value="${user.id}" />
                <input type="hidden" name="version" value="${user?.version}" />

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">New Password
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="password" class="form-control" name="password" v-validate="'required'" v-model="updatePasswordForm.password" ref="password" />
                                <span class="has-error help-block help-block-error">{{ errors.first('password') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label col-md-4">Confirm Password
                                <span class="required">*</span>
                            </label>

                            <div class="col-md-8">
                                <input type="password" class="form-control" name="confirmPassword" v-validate="'required|confirmed:password'" v-model="updatePasswordForm.confirmPassword" :class="{'is-danger': errors.has('confirmPassword')}" />
                                <span class="has-error help-block help-block-error">{{ errors.first('confirmPassword') }}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </form>

            <div slot="footer">
                <btn type="button" @click="openModal=false"><i class="fa fa-times"></i> Cancel</btn>
                <btn type="button" @click="updatePassword" :disabled="!formValid" class="blue-hoki" ><i class="fa fa-save"></i> Update Password</btn>
            </div>

        </modal>
    </section>
</template>

<asset:javascript src="application-vue.js" asset-defer="true"/>
<asset:javascript src="vue-apps/user/user-edit.js" asset-defer="true"/>

</body>
</html>