<div class="page-bar">
    <g:if test="${pageHeadTitle}">
        <div class="page-bar-header">
            <h4>${pageHeadTitle}
                <g:if test="${pageHeadSubTitle}">
                    <small>${pageHeadSubTitle}</small>
                </g:if>
            </h4>
        </div>
    </g:if>

    <div class="page-toolbar">
        <g:link controller="user" action="index" class="btn btn-default btn-sm">
            <i class="fa fa-list"></i> List Users
        </g:link>
        <g:link controller="user" action="create" class="btn blue-hoki btn-sm">
            <i class="fa fa-plus"></i> Add User
        </g:link>
    </div>
</div>