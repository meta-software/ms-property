<%@ page import="metasoft.property.core.CheckStatus" %>

<div class="search-page search-content-4 margin-top-10">
    <div class="search-bar bordered">
        <form role="form" autocomplete="off">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <input type="text" placeholder="User Query..." class="form-control" name="q" value="${params?.q}" />
                </div>
                <div class="col-lg-2 col-md-2">
                    <g:select
                            class="form-control select2-no-search"
                            noSelection="['': 'User Type...']"
                            name="usertype"
                            from="${userTypeList}"
                            value="${params['usertype']}"
                            optionKey="id"
                            optionValue="name"
                    />
                </div>

                <div class="col-lg-2 col-md-2 extra-buttons">
                    <button class="btn grey-steel uppercase bold" type="submit">Search</button>
                    <button class="btn green" type="button"><i class="fa fa-refresh"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="search-table table-responsive">
        <table class="table table-bordered table-striped table-hover" >
            <thead class="bg-grey-cararra">
            <tr>
                <th style="width: 20%;">Name</th>
                <th style="width: 15%;">Username</th>
                <th style="width: 15%; text-wrap: none;">User Type</th>
                <th style="width: 15%;">Email Address</th>
                <th style="width: 10%;">Enabled ?</th>
                <th style="width: 10%;">Expired ?</th>
                <th style="width: 10%;">Locked ?</th>
                <th style="width: 5%;">Status</th>
            </tr>
            </thead>
            <tbody>
            <g:if test="${userList}">
                <g:each in="${userList}" var="user">
                    <g:if test="${ !(user.username in [currentUser.username, 'flexcube']) }">
                        <tr>
                            <td>
                                <g:link style="display: block" action="edit" id="${user.id}">${user.fullName}</g:link>
                            </td>
                            <td>
                                <g:link style="display: block" action="edit" id="${user.id}">${user.username}</g:link>
                            </td>
                            <td>${user.userType?.name ?: "---"}</td>
                            <td>${user.emailAddress}</td>
                            <td><mp:boolStatusLabel status="${user.enabled}" /></td>
                            <td><mp:boolStatusLabel status="${user.accountExpired}" /></td>
                            <td><mp:boolStatusLabel status="${user.accountLocked}" /></td>
                            <td><mp:makerCheck value="${user.dirtyStatus}" /></td>
                        </tr>
                    </g:if>
                    <g:else>
                        <tr>
                            <td>${user.fullName}</td>
                            <td>${user.username}</td>
                            <td>${user.userType?.name ?: "---"}</td>
                            <td>${user.emailAddress}</td>
                            <td><mp:boolStatusLabel status="${user.enabled}" /></td>
                            <td><mp:boolStatusLabel status="${user.accountExpired}" /></td>
                            <td><mp:boolStatusLabel status="${user.accountLocked}" /></td>
                            <td><mp:makerCheck value="${CheckStatus.APPROVED}" /></td>
                        </tr>
                    </g:else>
                </g:each>
            </g:if>
            <g:else>
                <tr>
                    <td colspan="8">
                        <mp:noRecordsFound />
                    </td>
                </tr>
            </g:else>
            </tbody>
        </table>
    </div>
    <div class="search-pagination">
        <mp:pagination total="${userCount ?: 0}" />
    </div>
</div>