<!doctype html>
<html>
<head>

    <meta name="layout" content="login"/>
    <title><g:message code="default.login.label" default="MetaSoft::MSProperty - Password Expired"/></title>

    <script type="text/javascript">
        const username = '${username}';
    </script>

</head>

<body>

<div id="app"></div>

<!-- BEGIN LOGIN -->
<template id="password-expired">
    <div class="content">
        <loading :active.sync="loader.loading"
                 :is-full-page="loader.fullPage" ></loading>

        <g:if test="${flash.message}">
            <div class="alert alert-danger display-hide" style="display: block;">
                <button class="close" data-close="alert"></button>
                <span>${flash.message}</span>
            </div>
        </g:if>

    <!-- BEGIN LOGIN FORM -->
        <g:form class="login-form" method="POST" controller="user" action="update-expired" @submit.prevent="updatePassword" autocomplete="off">
            <input type="hidden" style="display: none" name="username" value="${username}" />
            <div class="form-title">
                <span class="form-title">Password Expired.</span>
                <span class="form-subtitle">Please Update.</span>
            </div>

            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <span>Enter username and password.</span>
            </div>

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <input class="form-control placeholder-no-fix" type="text" disabled="disabled" v-model="form.username"/>
            </div>

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Current Password</label>
                <input class="form-control placeholder-no-fix" type="password" v-validate="'required'"
                       autocomplete="off"
                       placeholder="Current Password" name="currentPassword" v-model="form.currentPassword" />
                <span class="has-error help-block">{{ errors.first('currentPassword') }}</span>
            </div>

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">New Password</label>
                <input class="form-control placeholder-no-fix" type="password" v-validate="'required|min:8'"
                       autocomplete="off" ref="newPassword"
                       placeholder="New Password" name="newPassword" v-model="form.newPassword" />
                <span class="has-error help-block">{{ errors.first('newPassword') }}</span>
            </div>

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Confirm New Password</label>
                <input class="form-control placeholder-no-fix" type="password" v-validate="'required|confirmed:newPassword'"
                       autocomplete="off" data-vv-as="Confirm Password"
                       placeholder="Confirm Password" name="confirmPassword" v-model="form.confirmPassword" />
                <span class="has-error help-block">{{ errors.first('confirmPassword') }}</span>
            </div>

            <div class="form-actions">
                <button type="button" @click="cancel" :disabled="loader.loading == true" class="btn red-flamingo uppercase">Cancel</button>
                <button type="button" @click="updatePassword" :disabled="!passwordsMatch || loader.loading" class="btn green uppercase pull-right">Update Password</button>
            </div>

        </g:form>
    <!-- END LOGIN FORM -->

    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/user/password-expired.js" asset-defer="true"/>

</body>

</html>
