<%@ page import="metasoft.property.core.SecRole" %>

<!-- BEGIN USER DETAILS PORTLET -->
<div class="portlet light bordered portlet-fit margin-top-5 bg-inverse">
    <div class="portlet-title">
        <div class="caption"><i class="fa fa-user"></i> User Details</div>


    </div>

    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label col-md-4">Username
                    </label>

                    <div class="col-md-8">
                        <input type="text" readonly="true" class="form-control" value="${user.username}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="col-md-offset-4 col-md-8">
                    <change-password-form></change-password-form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${user.errors['firstName'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">First Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-8">
                        <g:field class="form-control" type="text" name="firstName" value="${user.firstName}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group${user.errors['lastName'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Last Name
                        <span class="required">*</span>
                    </label>

                    <div class="col-md-8">
                        <g:field class="form-control" type="text" name="lastName" value="${user.lastName}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${user.errors['emailAddress'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Email Address</label>

                    <div class="col-md-8">
                        <g:field class="form-control" type="text" name="emailAddress" value="${user?.emailAddress}"/>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group${user.errors['employeeId'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">Employeed ID</label>

                    <div class="col-md-8">
                        <input type="text" name="employeeId" class="form-control" value="${user.employeeId}"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${user.errors['enabled'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Enabled?
                    </label>

                    <div class="col-md-8">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <g:checkBox name="enabled" value="1" class="blue"
                                    checked="${user.enabled}"/><span></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group${user.errors['accountExpired'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Account Expired?
                    </label>

                    <div class="col-md-8">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <g:checkBox name="accountExpired" value="1"
                                        checked="${user.accountExpired}"/><span></span>
                        </label>

                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${user.errors['passwordExpired'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Password Expired?
                    </label>

                    <div class="col-md-8">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <g:checkBox name="passwordExpired" value="1"
                                        checked="${user.passwordExpired}"/><span></span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group${user.errors['accountLocked'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        Account Locked?
                    </label>

                    <div class="col-md-8">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <g:checkBox name="accountLocked" value="1" class="blue"
                                        checked="${user.accountLocked}"/><span></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group${user.errors['userType'] ? ' has-error' : ''}">
                    <label class="control-label col-md-4">
                        User Type
                    </label>

                    <div class="col-md-8">
                        <g:select
                            class="form-control"
                            from="${metasoft.property.core.SecRole.listRoles()}"
                            value="${user.userType.id}"
                            optionValue="name"
                            optionKey="id"
                            name="userType"
                        ></g:select>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- END USER DETAILS PORTLET -->

<!-- BEGIN USER DETAILS PORTLET -->
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">User Roles</div>
    </div>

    <div class="portlet-body">
        <div class="row">
            <g:each in="${SecRole.list()}" var="role">
                <g:if test="${ !(role.authority in ['ROLE_PROP_ADMIN', 'ROLE_API_USER', 'ROLE_PROP_EXEC', 'ROLE_SUPERUSER', 'ROLE_SUPERUSER']) }">
                    <div class="col-md-3">
                            <g:if test="${this.userRoles.find{it.id == role.id}}">
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <g:checkBox value="${role.id}" checked="true" name='authorities' /><span></span>
                                    ${role.name}
                                </label>
                            </g:if>
                            <g:else>
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <g:checkBox value="${role.id}" checked="" name='authorities' /><span></span>
                                    ${role.name}
                                </label>
                            </g:else>
                        </div>
                </g:if>
            </g:each>
        </div>
    </div>

</div>
<!-- END USER DETAILS PORTLET -->

<!-- BEGIN FORM PORTLET-->
<div class="form-actions left">
    <g:if test="${user.id == null}">
        <g:submitButton name="create" class="btn green"
                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
    </g:if>
    <g:else>
        <g:submitButton name="update" class="btn green"
                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
    </g:else>

    <g:link controller="user" action="index" class="btn default">Cancel</g:link>
</div>
<!-- END FORM PORTLET-->