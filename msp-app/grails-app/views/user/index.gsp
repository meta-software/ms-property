<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'user.label', default: 'Users')}"/>
    <g:set var="pageHeadTitle" value="${message(code: 'user.list.label', default: entityName, args: [entityName])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>
    <title><g:message code="user.list.label" args="[entityName]"/></title>
</head>

<body>

<g:render template="page_bar"/>

<g:if test="${flash.message}">
    <g:render template="/templates/flash_message"/>
</g:if>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <g:render template="list" />
    </div>
</div>

</body>
</html>