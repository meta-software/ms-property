<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN TEMPALTE VARS -->
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <g:set var="pageHeadTitle"
           value="${message(code: 'user.list.label', default: entityName, args: [null])}"
           scope="request"/>
    <!-- END TEMPALTE VARS -->

    <meta name="layout" content="main"/>

    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<div id="app"></div>

<template id="main-app">
    <div>
        <page-bar></page-bar>

        <div class="inbox margin-top-10">
            <div class="row">
                <div class="col-lg-2 col-md-4 col-xs-12">
                    <div class="inbox-sidebar">

                        <router-link data-title="Create User" class="btn green btn-block" :to="{name: 'create'}">
                            <i class="fa fa-plus"></i> Create User
                        </router-link>

                        <ul class="inbox-nav">
                            <li>
                                <router-link data-title="Approved" to="/" >
                                    Approved
                                </router-link>
                            </li>
                            <li>
                                <router-link data-title="Approved" to="/pending" >
                                    Pending <span class="badge badge-success">5</span>
                                </router-link>
                            </li>
                            <li>
                                <router-link data-title="Outbox" to="/outbox" >
                                    Outbox <span class="badge badge-warning">3</span>
                                </router-link>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-10 col-md-8 col-xs-12">
                    <router-view></router-view>
                </div>
            </div>
        </div>

    </div>
</template>

<!--the user approved list component-->
<template id="user-approved">
    <div class="portlet light bordered" style="min-height: 400px">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div id="user-list-toolbar">
            <h3>Users : Approved</h3>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable-borderless">
                        <user-list :users="users" v-on:on-click="onRowClick"></user-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<!--the user approved list component-->
<template id="user-pending">
    <div class="portlet  fill-height light bordered" style="min-height: 400px">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div id="user-pending-toolbar">
            <h3>Users : Pending</h3>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable-borderless">
                        <user-check-list :user-checks="userChecks" v-on:onClick="alert('hello')"></user-check-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<!--user list-->
<template id="user-list">
    <bootstrap-table :columns="columns" :data="data" :options="options"></bootstrap-table>
</template>

<!--user checkList-->
<template id="user-check-list">
    <bootstrap-table :columns="columns" :data="data" :options="options"></bootstrap-table>
</template>

<template id="page-bar">
    <div class="page-bar">
        <div class="page-bar-header">
            <h4> <i class="fa fa-user"></i> Users
                <small v-if="subTitle">{{subTitle}}</small>
            </h4>
        </div>

        <div class="page-toolbar">
            <router-link class="btn btn-sm blue-hoki" :to="{path: '/'}"> <i class="fa fa-list"></i> List Users</router-link>
            <router-link class="btn btn-sm green" :to="{path: '/create'}"> <i class="fa fa-plus"></i> Create User</router-link>
        </div>
    </div>
</template>

<!--the user approved list component-->
<template id="user-show">
    <div class="portlet light bordered" style="min-height: 400px">
        <loading :active.sync="loader.isLoading"
                 :can-cancel="false"
                 :is-full-page="loader.fullPage" ></loading>
        <div class="portlet-title">
            <div class="caption">User Details : {{user.fullName}}</div>

            <div class="actions">
                <button class="btn btn-sm"><i class="fa fa-refresh"></i> Change Password</button>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable-borderless">

                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<asset:javascript src="application-vue.js" asset-defer="true" />
<asset:javascript src="vue-apps/secUser/sec-user-app.js" asset-defer="true"/>

</body>
</html>