/**
 * Created by lebohof on 12/24/2021.
 */
//= require_self

const config = {
    API_URL: 'http://192.168.1.214:8081/msp-app' // PRODUCTION IP [letting-server]
};

//Vue global configurations
Vue.config.productionTip = true;
Vue.config.devtools = false;

// disable javascript logging, when in production
console.log   = () => {};
console.error = () => {};
console.warn  = () => {};