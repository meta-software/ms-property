/**
 * Created by lebohof on 7/28/2018.
 */
// global javascript application configuration
//Vue library
//= require ${grails.util.Environment.currentEnvironment.toString() == 'DEVELOPMENT' ? 'global/plugins/vue/vue.js' : 'global/plugins/vue/vue.min.js'}

//Utilised vue plugins.
//= require global/plugins/vue/vue-router/vue-router
//= require global/plugins/axios/axios
//= require global/plugins/vue/uiv/uiv
//= require global/plugins/vue/vue-loading-overlay@3
//= require global/plugins/vue/vue-cookies
//= require global/plugins/vue/vue-resource/vue-resource

// Nice to have basic plugins
//= require global/plugins/iziToast/iziToast

//= require ${grails.util.Environment.currentEnvironment.toString() == 'DEVELOPMENT' ? 'appconfig_dev.js' : 'appconfig_prod.js'}

//= require_self

//iziToast global configurations
iziToast.settings({
    timeout: 5000,
    position: 'topRight',
    theme: 'light'
});

Vue.prototype.$axios = axios;
//axios global defaults and configurations
axios.defaults.baseURL = config.API_URL;
axios.defaults.headers['Content-Type'] = 'application/json';
axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers['nopage'] = 'true';

axios.interceptors.response.use((resp) => {
        return Promise.resolve(resp);
    }, (error) => {

    // Do something with response error
    if (error.response.status === 401) {
        console.log('unauthorized, logging out ...');

        iziToast.error({
            message: 'Your session has been timed out.',
            title: 'Logout',
            position: 'topCenter',
            overlay: true,
            close: false,
            drag: false,
            zindex: 999990,
            timeout: 0});

        //@todo: best thing would be to open a login dialog box and try to login from there, but redirecting is good for now.
        window.location.assign(config.API_URL+"/login/auth");

        return;
    }

    // Do something with response error
    if (error.response.status === 500) {
        iziToast.error({
            message: "An Internal Server Error occurred.\nPlease contact System Administrator for assistance.",
            title: "Internal Server Error.<br \>",
            position: 'topCenter',
            timeout: 10000});
    }

    if (error.response.status === 422) {
        if (error.response.data.errors) {
            error.response.data.errors.forEach((error) => {
                iziToast.error({message: error.message, timeout: 10000});
            })
        }
    }

    return Promise.reject(error);
});

/**
 *
 */
Vue.component('select2', {
    props: ['select2', 'options', 'value', 'ajax'],
    template: '<select class="form-control"><slot></slot></select>',
    methods: {
        reset() {
            $(this.$el)
                .val(null)
                .trigger('change');
        }
    },
    mounted () {

        $(this.$el)
            .select2()
            .val(this.value)
            .on('change',  (event) => this.$emit('input', event.target.value) )
            .trigger('change');
    },
    watch: {
        value(value) {
            // update value
            $(this.$el)
                .val(value)
                .trigger('change.select2')
        },
        options(options) {
            // update options
            $(this.$el)
                .empty()
                .select2({data: options})
                .val(null).trigger('change');
        },
        ajax(ajax) {
            // update ajax
            $(this.$el)
                .empty()
                .select2({ajax: ajax})
                .val(null)
                .trigger('change');
        },
        select2(select2) {
            //update the whole method parameter
            $(this.$el)
                .empty()
                .select2(select2)
                .val(null)
                .trigger('change');
        }
    },
    destroyed() {
        $(this.$el)
            .off()
            .select2('destroy')
    }
});

Vue.component('datepicker', {
    template: "<input type='text' :value='value' />",
    props: {
        value: {
            required: true
        },
        configs: {
            required: false
        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value);
        }
    },
    mounted() {
        $(this.$el).datepicker({
            autoclose: true,
            orientation: 'bottom',
            format: 'yyyy-mm-dd'
        }).on('change', (evt) => this.updateValue(evt) )
        .trigger('change');
    },
    watch: {
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change');
        }
    }
});

Vue.component('amount-input', {
    template: `<input type='text' :value="value" @keydown="onKeydown" />`,
    props: ['value'],
    methods: {
       onKeydown(event) {
           console.log(event)
           alert(event.target.value)
       }
    }
});

Vue.component('vdate-picker', {
    template: `<input type="text" :name="name" v-datepicker :value="value" @input="update($event.target.value)" />`,
    directives: {
        datepicker: {
            inserted (el, binding, vNode) {
                $(el).datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd'
                }).on('changeDate', function (e) {
                    vNode.context.$emit('input', e.format(0))
                }).trigger('change');
            }
        }
    },
    props: ['value', 'name'],
    methods: {
        update: function (v) {
            this.$emit('input', v)
        },
        val: function () {
            return this.value;
        }
    }
});

/**
 * prepare some global vue filters.
 */
Vue.filter('formatDate', function (value, format) {
    if (!format) {
        format = "YYYY-MM-DD";
    }

    if (value) {
        return moment(String(value)).format(format)
    }
});

Vue.filter('shortify', function (value, length) {
    if (value && value.length > length) {
        return value.substring(0, length) + "...";
    }

    return value;
});

Vue.filter('formatNumber', function (number, dec = 2) {

    if(number == null) return null;

    number = number.toFixed(dec) + '';
    let x = number.split('.');
    let x1 = x[0];
    let x2 = x.length > 1 ? '.' + x[1] : '';
    let rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
});

Vue.filter('formatBoolean', function(value, format = ['Yes', 'No']) {
    return value ? format[0] : format[1];
});

Vue.use(VueLoading);
Vue.component('loading', VueLoading)

Vue.use(VueRouter);

Vue.use(uiv);

//global functions
function shortify(value, length) {
    if (value && value.length > length) {
            return value.substring(0, length) + "...";
    }
    return value;
}

function formatCheckStatus(value, format) {
    return `<span class="request-status display-block label label-sm ${statusClass(value)}">${value}</span>`
}

const formatBoolean = function (value, format = ['Yes', 'No']) {
    return value ? `<span class="label label-sm label-success">${format[0]}</span>` : `<span class="label label-sm label-danger">${format[1]}</span>`;
};

const formatNumber = function(value, format = '0,0.00') {
    return numeral(value).format(format);
};

const numberFormat = function(value, format = '0,0.00') {
    return numeral(value).format(format);
};

// configure a Vue.mixin as a global variable usage
Vue.mixin({
    data: function() {
        return {
            config: {
                API_URL: config.API_URL,
                APP_NAME: "Nhakr"
            }
        };
    },
    methods: {
        href(param) {
            return config.API_URL+param
        }
    }
});

// configure the Vue.http
Vue.http.options.root = config.API_URL;
Vue.http.headers.common['Content-Type'] = 'application/json';
Vue.http.headers.common['X-Requested-With'] = 'XMLHttpRequest';
Vue.http.headers.common['nopage'] = 'true';

const overlayLoader = {
    fullPageLoader: function() {
        return Vue.$loading.show({
            canCancel: false,
            isFullPage: true,
            color: '#000000',
            loader: 'spinner'
        });
    },
    customerLoader: function(params) {
        return Vue.$loading.show(params);
    }
};

// disable javascript logging, when in production
//console.log   = () => {};
//console.error = () => {};
//console.warn  = () => {};