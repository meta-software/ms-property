/**
 * Created by lebohof on 01/15/2019.
 */
//= require vue-apps/dashboard/dashlets/dashlet-pending-receipts-lite

Vue.component('maker-outbox-widget', {
    template: '#maker-outbox-widget',
    data: function() {
        return {
            widgetItems: []
        }
    },
    methods: {
        loadWidget: function() {

            const url = config.API_URL + "/dashboard/maker-outbox";
            this.loader = this.$loading.show({
                container: this.$refs.makerOutbox,
                canCancel: false,
                placement: 'center'
            });

            this.$axios.get(url).then((response) => {
                if(response.status === 200) {
                    this.widgetItems = response.data;
                }
            }).finally(() => {
                this.loader.hide();
            });

        },
        refresh: function() {
            this.loadWidget();
        }
    },
    created: function() {
        // initialize by loading the widget
        this.loadWidget()

        //prepare the interval for the widget
        setInterval(() => {
            this.refresh();
        }, 60000);
    },
    mounted: function() {
    }
});

Vue.component('lease-reviews', {
    template: '#lease-reviews',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                reviewCount: 0
            },
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;
            this.$axios.get(config.API_URL+'/api/v1/lease-review/dashboard').then((resp) => {
                this.dashboard = resp.data
            }).finally((err) => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.refresh();
    }
});

jQuery(document).ready(function() {
    new Vue({
        el: '#dashboard-app'
    });
});
