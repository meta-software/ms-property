/**
 * Created by lebohof on 01/15/2019.
 */

Vue.component('RentalListingsDashlet', {
    template: '#rental-listings-dashlet',
    data: function() {
        return {
            widgetData: [],
            loading: false
        }
    },
    methods: {
        loadWidget: function() {
            const url = config.API_URL + "/dashboard/execute?dashlet=rental-listings";
            this.loading = true;

            this.$axios.get(url).then((response) => {
                if(response.status === 200) {
                    this.widgetData = response.data;
                }
            }).finally( ()=> this.loading = false );

        },
        refresh: function() {
            this.loadWidget();
        }
    },
    created: function() {
        // initialize by loading the widget
        this.loadWidget();
    }
});

Vue.component('OccupancyDashlet', {
    template: '#occupancy-dashlet',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                occupancy: 0
            },
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;
            this.$axios.get(config.API_URL+'/dashboard/execute?dashlet=occupancy').then((resp) => {
                this.dashboard = resp.data;

                //calculate the occupancy rates;
                if(this.dashboard.totalUnits > 0) {
                    this.dashboard.occupiedRate = formatNumber(100*this.dashboard.occupied/this.dashboard.totalUnits, 0);
                    this.dashboard.notOccupiedRate = formatNumber(100*this.dashboard.notOccupied/this.dashboard.totalUnits, 0);
                } else {
                    this.dashboard.occupiedRate    = 0;
                    this.dashboard.notOccupiedRate = 0;
                }
            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    created() {
        this.refresh();
    }
});

jQuery(document).ready(function() {
    new Vue({
        el: '#dashboard-app'
    });
});
