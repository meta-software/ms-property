/**
 * Created by lebohof on 03/07/2020.
 */

Vue.component('ds-pending-users', {
    template: '#ds-pending-users',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                pendingUsers: 0
            },
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;
            this.$axios.get(config.API_URL+'/dashboard/execute?dashlet=pending-users').then((resp) => {
                this.dashboard = resp.data
            }).finally((err) => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.refresh();
    }
});


jQuery(document).ready(function() {
    new Vue({
        el: '#dashboard-app'
    });
});
