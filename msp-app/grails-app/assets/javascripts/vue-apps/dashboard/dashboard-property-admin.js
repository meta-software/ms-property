/**
 * Created by lebohof on 01/15/2019.
 */
//= require vue-apps/dashboard/dashlets/dashlet-occupancy-lite
//= require vue-apps/dashboard/dashlets/dashlet-commission-lite
//= require vue-apps/exchangeRate/exchange-rate-preview
//= require vue-apps/components/billing-cycles

Vue.component('checker-inbox-widget', {
    template: '#checker-inbox-widget',
    data: function() {
        return {
            widgetItems: []
        }
    },
    methods: {
        loadWidget: function() {
            const url = config.API_URL + "/dashboard/checker-inbox";
            this.loader = this.$loading.show({
                container: this.$refs.checkerInbox,
                canCancel: false,
                placement: 'center'
            });

            this.$axios.get(url).then((response) => {

                if(response.status === 200) {
                    this.widgetItems = response.data;
                }

                this.loader.hide();
            }).catch((error) => {
                console.error(error);
                this.loader.hide();
            })

        },
        refresh: function() {
            this.loadWidget();
        }
    },
    created: function() {
        // initialize by loading the widget
        this.loadWidget()

        //prepare the interval for the widget
        setInterval(() => {
            this.refresh();
        }, 120000);
    },
    mounted: function() {
    }
});

Vue.component('maker-outbox-widget', {
    template: '#maker-outbox-widget',
    data: function() {
        return {
            widgetItems: []
        }
    },
    methods: {
        loadWidget: function() {

            const url = config.API_URL + "/dashboard/maker-outbox";
            this.loader = this.$loading.show({
                container: this.$refs.makerOutbox,
                canCancel: false,
                placement: 'center'
            });

            this.$axios.get(url).then((response) => {

                if(response.status === 200) {
                    this.widgetItems = response.data;
                }

                this.loader.hide();
            }).catch((error) => {
                console.error(error);
                this.loader.hide();
            });

        },
        refresh: function() {
            this.loadWidget();
        }
    },
    created: function() {
        // initialize by loading the widget
        this.loadWidget()

        //prepare the interval for the widget
        setInterval(() => {
            this.refresh();
        }, 120000);
    },
    mounted: function() {
    }
});

Vue.component('leases-dashlet', {
    template: '#leases-dashlet',
    data: function() {
        return {
            widgetData: [],
            loading: false
        }
    },
    methods: {
        loadWidget: function() {
            const url = config.API_URL + "/dashboard/execute?dashlet=expiring-leases";
            this.loading = true;

            this.$axios.get(url).then((resp) => {
                if(resp.status === 200) {
                    this.widgetData = resp.data;
                }
            }).finally( ()=> {
                this.loading = false;
            });
        },
        refresh: function() {
            this.loadWidget();
        }
    },
    created: function() {
        // initialize by loading the widget
        this.loadWidget();
    }
});

Vue.component('lease-reviews', {
    template: '#lease-reviews',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                reviewCount: 0
            },
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;
            this.$axios.get(config.API_URL+'/api/v1/lease-review/dashboard').then((resp) => {
                this.dashboard = resp.data
            }).finally((err) => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.refresh();
    }
});

Vue.component('outstanding-balances', {
    template: '#outstanding-balances',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                outstandingBalances: 0
            },
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;
            this.$axios.get(config.API_URL+'/dashboard/execute?dashlet=outstanding-balances').then((resp) => {

                if(resp.data.outstandingBalances) {
                    this.dashboard = resp.data
                }

            }).finally((err) => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.refresh();
    }
});

jQuery(function() {
    new Vue({
        el: '#dashboard-app'
    });
});
