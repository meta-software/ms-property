/**
 *
 */

var templateString = `
    <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
        <div class="visual">
            <i class="fa fa-play"></i>
        </div>
        <div class="details">
            <div class="number">
               $ <span data-counter="counterup" :data-value="commissionAmount">{{commissionAmount}}</span>
            </div>
            <div class="desc"> {{desc}}</div>
        </div>
    </a>`;

Vue.component('commission-lite', {
    template: templateString,
    props: {
        desc: {
            default: 'Commission'
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                commissionAmount: 0.0
            }
        }
    },
    computed: {
        commissionAmount(){
            return this.dashboard.commissionAmount === null ? 0 : this.dashboard.commissionAmount
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;

            let queryParams = {
                dashlet: 'commission',
                period: 'current'
            }

            this.$axios.get(config.API_URL+'/dashboard/execute', {params: queryParams}).then((resp) => {
                this.dashboard = resp.data;
            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    created() {
        this.refresh();
    }
});
