/**
 *
 */

var templateString = `
    <a class="dashboard-stat dashboard-stat-v2 red-mint" href="/pending-receipt">
        <div class="visual">
            <i class="fa fa-money"></i>
        </div>
        <div class="details">
            <div class="number">
               <span data-counter="counterup" :data-value="pendingReceiptsCount">{{pendingReceiptsCount}}</span>
            </div>
            <div class="desc"> {{desc}}</div>
        </div>
    </a>`;

Vue.component('pending-receipts-lite', {
    template: templateString,
    props: {
        desc: {
            default: 'Pending Receipts'
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                pendingReceiptsCount: 0.0
            }
        }
    },
    computed: {
        pendingReceiptsCount(){
            return this.dashboard.pendingReceiptsCount === null ? 0 : this.dashboard.pendingReceiptsCount
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;

            let queryParams = {
                dashlet: 'pending-receipt'
            };

            this.$axios.get(config.API_URL+'/dashboard/execute', {params: queryParams}).then((resp) => {

                if(resp.data.pendingReceiptsCount != undefined) {
                    this.dashboard = resp.data;
                }

            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    created() {
        this.refresh();
    }
});
