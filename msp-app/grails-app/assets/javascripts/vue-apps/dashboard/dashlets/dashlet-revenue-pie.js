/**
 *
 */

//= require global/plugins/flot/jquery.flot.pie.min.js

var template = `
<!-- BEGIN PORTLET-->
<div class="portlet light bordered bg-inverse" ref="dsRevPie">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-blue-dark bold uppercase">Balances</span>
            <span class="caption-helper">balances distribution ...</span>
        </div>
        <div class="actions">
            <button type="button" @click="refresh" href="javascript:;" class="btn btn-sm btn-circle blue-dark">
                <i class="fa fa-repeat"></i> Reload
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div ref="chartArea" style="height: 300px"></div>
    </div>
</div>
<!-- END PORTLET-->
`;

Vue.component('ds-revenue-pie', {
    template: template,
    props: {
        desc: {
            default: 'Revenue'
        },
        subTitle: {
            defualt: "Monthly Stats..."
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {

            }
        }
    },
    methods: {
        refresh: function() {
            const loader = this.$loading.show({
                container: this.$refs.dsRevPie,
                canCancel: false,
            });

            setTimeout(() => {
                loader.hide();
            }, 2000)
        },
        initChart() {

            var data = [];
            var series = Math.floor(Math.random() * 5) + 1;
            series = series < 5 ? 5 : series;

            for (var i = 0; i < series; i++) {
                data[i] = {
                    label: "Series" + (i + 1),
                    data: Math.floor(Math.random() * 100) + 1
                };
            }

            console.log(data);
            $.plot($(this.$refs.chartArea), data, {
                series: {
                    pie: {
                        show: true
                    }
                },
                legend: {
                    show: false
                }
            });

        }
    },
    created() {
        this.refresh();
    },
    mounted() {
        this.initChart();
    }
});
