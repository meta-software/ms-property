/**
 *
 */
Vue.component('occupancy-lite', {
    template: '#occupancy-lite',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                occupancy: 0
            },
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;
            this.$axios.get(config.API_URL+'/dashboard/execute?dashlet=occupancy').then((resp) => {
                this.dashboard = resp.data;

                //calculate the occupancy rates;
                if(this.dashboard.totalUnits > 0) {
                    this.dashboard.occupiedRate = formatNumber(100*this.dashboard.occupied/this.dashboard.totalUnits, 0);
                    this.dashboard.notOccupiedRate = formatNumber(100*this.dashboard.notOccupied/this.dashboard.totalUnits, 0);
                } else {
                    this.dashboard.occupiedRate    = 0;
                    this.dashboard.notOccupiedRate = 0;
                }
            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    computed: {
        occupiedRate() {
            return this.dashboard.occupiedRate;
        }
    },
    created() {
        this.refresh();
    }
});
