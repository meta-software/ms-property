/**
 *
 */

var templateString = `
<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
    <h4 class="widget-thumb-heading">{{desc}}</h4>
    <div class="widget-thumb-wrap">
        <i class="widget-thumb-icon bg-red icon-bulb"></i>
        <div class="widget-thumb-body">
            <span class="widget-thumb-subtitle">$</span>
            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="1,293">{{invoicesAmount | formatNumber(0) }}</span>
        </div>
    </div>
</div>
`;

Vue.component('ds-invoice-lite', {
    template: templateString,
    props: {
        desc: {
            default: 'Invoices'
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                invoicesAmount: 0.0
            }
        }
    },
    computed: {
        invoicesAmount(){
            return this.dashboard.invoicesAmount === null ? 0 : this.dashboard.invoicesAmount
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;

            let queryParams = {
                dashlet: 'ds-invoice',
                period: 'current'
            };

            this.$axios.get(config.API_URL+'/dashboard/execute', {params: queryParams}).then((resp) => {
                this.dashboard = resp.data;
            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    created() {
        this.refresh();
    }
});
