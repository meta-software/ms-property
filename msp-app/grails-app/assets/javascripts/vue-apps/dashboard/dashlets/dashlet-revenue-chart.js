/**
 *
 */

var templateString = `
<!-- BEGIN PORTLET-->
<div class="portlet light bordered" ref="dsRevChart">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-red-sunglo hide"></i>
            <span class="caption-subject font-dark bold uppercase">Revenue</span>
            <span class="caption-helper">monthly stats...</span>
        </div>
        <div class="actions">
            <button type="button" @click="refresh" href="javascript:;" class="btn btn-sm btn-circle green-dark">
                <i class="fa fa-repeat"></i> Reload
            </button>
        </div>
    </div>
    <div class="portlet-body">
        <div id="site_activities_loading">
            <img src="../assets/global/img/loading.gif" alt="loading" /> </div>
        <div id="site_activities_content" class="display-none">
            <div id="site_activities" style="height: 228px;"> </div>
        </div>
        <div style="margin: 20px 0 10px 30px">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-6 text-stat">
                    <span class="label label-sm label-success"> Receipts: </span>
                    <h3>$13,234</h3>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 text-stat">
                    <span class="label label-sm label-info"> Invoices: </span>
                    <h3>$134,900</h3>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6 text-stat">
                    <span class="label label-sm label-danger"> Commission: </span>
                    <h3>$1,134</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PORTLET-->
`;

Vue.component('ds-revenue-chart', {
    template: templateString,
    props: {
        desc: {
            default: 'Revenut'
        },
        subTitle: {
            defualt: "Monthly Stats..."
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
            }
        }
    },
    methods: {
        refresh: function() {
            const loader = this.$loading.show({
                container: this.$refs.dsRevChart,
                canCancel: false,
            });

            setTimeout(() => {
                loader.hide();
            }, 2000)
        },
        initChart() {
            if ($('#site_activities').size() != 0) {
                //site activities
                var previousPoint2 = null;
                $('#site_activities_loading').hide();
                $('#site_activities_content').show();

                var data1 = [
                    ['DEC', 300],
                    ['JAN', 600],
                    ['FEB', 1100],
                    ['MAR', 1200],
                    ['APR', 860],
                    ['MAY', 1200],
                    ['JUN', 1450],
                    ['JUL', 1800],
                    ['AUG', 1200],
                    ['SEP', 600]
                ];


                var plot_statistics = $.plot($("#site_activities"),

                    [{
                        data: data1,
                        lines: {
                            fill: 0.2,
                            lineWidth: 0,
                        },
                        color: ['#BAD9F5']
                    }, {
                        data: data1,
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#9ACAE6",
                            lineWidth: 2
                        },
                        color: '#9ACAE6',
                        shadowSize: 1
                    }, {
                        data: data1,
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3
                        },
                        color: '#9ACAE6',
                        shadowSize: 0
                    }],

                    {

                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 18,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                $("#site_activities").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + 'M$');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function() {
                    $("#tooltip").remove();
                });
            }

        }
    },
    created() {
        this.refresh();
    },
    mounted() {
        this.initChart();
    }
});
