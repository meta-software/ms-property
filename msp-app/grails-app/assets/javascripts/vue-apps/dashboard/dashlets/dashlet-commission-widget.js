/**
 *
 */

var templateString = `
<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
    <h4 class="widget-thumb-heading">{{desc}}</h4>
    <div class="widget-thumb-wrap">
        <i class="widget-thumb-icon bg-purple icon-control-play"></i>
        <div class="widget-thumb-body">
            <span class="widget-thumb-subtitle">$</span>
            <span class="widget-thumb-body-stat" data-counter="counterup" :data-value="commissionAmount">{{commissionAmount | formatNumber(0) }}</span>
        </div>
    </div>
</div>
`;

Vue.component('ds-commission-widget', {
    template: templateString,
    props: {
        desc: {
            default: 'Commission'
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                commissionAmount: 0.0
            }
        }
    },
    computed: {
        commissionAmount(){
            return this.dashboard.commissionAmount === null ? 0 : this.dashboard.commissionAmount
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;

            let queryParams = {
                dashlet: 'commission',
                period: 'current'
            }

            this.$axios.get(config.API_URL+'/dashboard/execute', {params: queryParams}).then((resp) => {
                this.dashboard = resp.data;
            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    created() {
        this.refresh();
    }
});
