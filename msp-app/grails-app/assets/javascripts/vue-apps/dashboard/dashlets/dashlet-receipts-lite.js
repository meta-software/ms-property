/**
 *
 */

var templateString = `
<div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
    <h4 class="widget-thumb-heading">{{desc}}</h4>
    <div class="widget-thumb-wrap">
        <i class="widget-thumb-icon bg-green icon-drawer"></i>
        <div class="widget-thumb-body">
            <span class="widget-thumb-subtitle">$</span>
            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="1,293">{{receiptsAmount | formatNumber(0) }}</span>
        </div>
    </div>
</div>
`;

Vue.component('ds-receipt-lite', {
    template: templateString,
    props: {
        desc: {
            default: 'receipts'
        }
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            dashboard: {
                receiptsAmount: 0.0
            }
        }
    },
    computed: {
        receiptsAmount(){
            return this.dashboard.receiptsAmount === null ? 0 : this.dashboard.receiptsAmount
        }
    },
    methods: {
        refresh: function() {
            this.loader.isLoading = true;

            let queryParams = {
                dashlet: 'ds-receipt',
                period: 'current'
            };

            this.$axios.get(config.API_URL+'/dashboard/execute', {params: queryParams}).then((resp) => {
                this.dashboard = resp.data;
            }).finally((err) => {
                this.loader.isLoading = false;
            });
        },
    },
    created() {
        this.refresh();
    }
});
