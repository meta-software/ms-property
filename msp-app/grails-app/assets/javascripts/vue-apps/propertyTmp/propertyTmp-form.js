/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            landlord: 'Landlord',
            name: 'Name',
            propertyType: 'Property Type',
            propertyManager: 'Property Manager',
            standNumber: 'Stand Number',
            area: 'Area',
            city: 'City',
            street: 'Street',
            country: 'Country',
            suburb: 'Suburb',
            commission: 'Commission',
            unitReference: 'Unit Reference'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

//Validator.localize(dictionary);
Vue.component('rental-unit-form', {
    props: {
        rentalUnits: Array
    },
    template: "#rental-unit-form",
    data: () => {
        return {
            rentalUnit: {
                name: String,
                unitReference: String,
                area: null
            }
        }
    },
    methods: {
        clearFormData: function() {
            this.rentalUnit = {
                name: "",
                unitReference: "",
                area: null
            }
        },
        removeUnit(index){
            this.$emit('remove-unit', index);
        },
        addUnit : function (){
            //@todo: perform a bit of validation

            this.$validator.validate().then((result) => {
               if(result) {
                   let rentalUnit = Object.assign({}, this.rentalUnit);

                   rentalUnit.area = Number(rentalUnit.area);

                   this.clearFormData();

                   //@emit: the event and the object which has just been added.
                   this.$emit('input', rentalUnit);
               } else {
               }
            });
        }
    },
    created: function() {
        this.clearFormData();
    }
})
;

/**
 * The main application component
 */
let vm = new Vue({
    data: {
        property: {},
        totalUnitsArea: 0,
        landlordOptions: {
            placeholder: "Landlord..."
        }
    },
    methods: {
        onCancel: function() {
            this.$confirm({
                title: 'Cancel ?',
                content: 'Are you sure you want to cancel?'
            }).then(() =>{
                window.location.assign(config.API_URL + "/property-tmp");
            }).catch(() => {})
        },
        onSaveProperty: function() {
            console.log(this.property);
            const url = config.API_URL + '/property-tmp/save';

            this.$validator.validate().then((result) => {
                if(result) {
                    // show the loader
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });

                    axios.post(url, this.property).then((response) => {
                        let property = response.data;

                        this.loader.hide();

                        this.$alert({
                            content: "Property created successfully.",
                            title: 'Success'
                        }, () => {

                            this.loader = this.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            }); //*/
                            window.location.assign(property['_links']['self']['href']);
                        })

                    }).catch((error) => {
                        console.log("Error occured while trying to save landlord entity", error);
                        this.loader.hide();

                        this.$alert({
                            content: 'Failed to save the landlord entity',
                            title: 'Error !'
                        })
                    });

                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });

        },
        onAddRentalUnit: function (rentalUnit) {
            console.log(rentalUnit);

            this.totalUnitsArea += rentalUnit.area;

            this.property.rentalUnits.push(rentalUnit)
        },
        onRemoveRentalUnit : function (index) {
            let rentalUnit = this.property.rentalUnits[index];
            this.property.rentalUnits.splice(index, 1);
            this.totalUnitsArea -= rentalUnit.area;
        },
        initForm: function () {

            this.property = {
                name: "",
                landlord: {},
                address: {
                    city: "",
                    country: "",
                    suburb: "",
                    street: ""
                },
                description: "",
                area: "",
                commission: "",
                chargeVat: false,
                isFullyConfigured: false,
                propertyType: {},
                propertyManager: {},
                propertyStatus: {},
                mandateDate: "",
                lastInspectionDate: "",
                terminatedDate: "",
                terminated: false,
                propertyCode: "",
                notes: "",
                standNumber: "",
                dateCreated: null,
                lastUpdated: null,

                rentalUnits: [],
                propertyInsurances: []
            }

        }
    },
    computed: {
        totalUnitsArea: function() {
            return this.property.rentalUnits.reduce((a, {area}) => a + area, 0.0);
        }
    },
    created: function() {
        //initialise the form
        this.initForm();

        this.$axios.get(config.API_URL + "/property-tmp/create").then( (response) => {
            console.log(response.data)
        }).catch( (error) => {
            console.log("error occured: ", error);
        });
    },

    mounted: function() {
        this.landlordOptions = {
            placeholder: 'Landlord...',
            ajax: {
                url: config.API_URL + '/landlord/list-options',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'

                    let options = data.map((option) => {
                        return {
                            id: option.id,
                            text: `${option.accountNumber} : ${option.accountName}`
                        };
                    });

                    return {
                        results: options
                    };
                },
                data: function (params) {
                    return {
                        q: params.term
                    };
                }
            }
        };
    }
});

jQuery(document).ready(() => {
    vm.$mount('#property-form');
});