/**
 * Created by lebohof on 8/22/2018.
 * /
// = require application-vue */
//= require global/plugins/vue/vee-validate
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            landlord: 'Landlord',
            name: 'Name',
            propertyType: 'Property Type',
            propertyManager: 'Property Manager',
            standNumber: 'Stand Number',
            area: 'Area',
            city: 'City',
            street: 'Street',
            country: 'Country',
            suburb: 'Suburb',
            commission: 'Commission',
            unitReference: 'Unit Reference'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

//Validator.localize(dictionary);
Vue.component('rental-unit-form', {
    template: "#rental-unit-form",
    data: () => {
        return {
            rentalUnits: Array,
            rentalUnit: {
                name: String,
                unitReference: String,
                area: null,
                deleted: false,
                rentalUnit: null,
                property: {
                    id: propertyId,
                }
            }
        }
    },
    methods: {
        clearFormData: function() {
            this.rentalUnit = {
                rentalUnit: null,
                name: "",
                unitReference: "",
                deleted: false,
                area: null,
                property: {
                    id: propertyId,
                }
            }
        },
        loadRentalUnits: function() {
            const url = config.API_URL + "/rental-unit-tmp/index/?propertyId=" + propertyId;

            this.$axios.get(url).then((response ) => {
                this.rentalUnits = response.data;
            }).catch((error) => {
                console.log("error occured while fetching rental units");
                iziToast.error({position: 'topCenter', message: `Error: Failed to fetch the rental units for the property entity`});
            })
        },
        removeRentalUnit(rentalUnit){

            const url = config.API_URL + "/rental-unit-tmp/delete/"+rentalUnit.id;

            this.$axios.delete(url).then((response) => {
                iziToast.success({position: 'topRight', message: `Rental Unit deleted successfully...`});
                this.loadRentalUnits();

                this.$emit('removeunit', rentalUnit);
            }).catch((error) => {
                console.log("error while deleting rental unit : " + rentalUnit.id)
            });

        },
        addUnit : function (){
            //@todo: perform a bit of validation

            const vm = this;
            const url = config.API_URL + "/rental-unit-tmp/save";

            this.$validator.validate().then((result) => {
                if(result) {

                    vm.$axios.post(url, this.rentalUnit).then((response) => {
                        const rentalUnit = response.data;
                        vm.rentalUnits.unshift(rentalUnit);
                        this.clearFormData();

                        //@emit: the event and the object which has just been added.
                        this.$emit('input', rentalUnit);

                        iziToast.success({position: 'topRight', message: `Rental Unit was added successfully...`});

                    }).catch((error) => {

                    });

                } else {
                }
            });
        }
    },
    created: function() {
        this.clearFormData();

        this.loadRentalUnits();
    },
    mounted: function() {
        console.log('in method mounted')
    }
})
;

const propertyTmpApp = new Vue({
    el: '#main-app',
    data: {
        propertyId: propertyId,
        confDisabled: false,
        property: {
            id: propertyId
        },
        totalUnitsArea: 0.0
    },
    mounted: function(){

    },
    methods: {
        postProperty: function() {
            const url = config.API_URL + "/property-tmp/post-request/"+propertyId;
            this.$axios.put(url).then((response) => {
                iziToast.success({position: 'topRight', message: `Property Request was posted succesfully...`});

                window.location.assign(config.API_URL + "/property-tmp");
            }).catch((error) => {

            })
        },
        discardProperty: function() {
            this.$confirm({
                title: 'Confirm',
                content: 'Are you sure you want to discard your changes ?'
            }).then(() => {
                const url = config.API_URL +"/property-tmp/discard/"+this.property.id;
                    this.$axios.put(url).then( (resp) => {
                            this.$notify({
                                type: 'success',
                                content: 'Discard completed.'
                            });
                            window.location.assign(config.API_URL+"/property-tmp")
                    }).catch( (err) => {
                        this.$notify({
                            type: 'danger',
                            content:'Operation Failed: Error occured while executing.'
                        });
                    })
                });
        },
        onRemoveRentalUnit: function() {
            this.getUnitsArea();
        },
        onAddRentalUnit: function() {
            this.getUnitsArea();
        },
        configureProperty: function() {
            const vm = this;
            this.confDisabled = true;
            this.$axios.get(config.API_URL + "/property/configure/"+this.propertyId).then((response) => {
                vm.$notify({
                    duration: 6000,
                    title: 'Success',
                    type: 'success',
                    content: response.data.message
                });
            }).catch((error) => {
                error.data.errors.forEach((error) => {
                    vm.$notify({
                        duration: 6000,
                        type: 'danger',
                        content: error.message
                    })
                })
            }).then(() => {
                vm.confDisabled = false;
            })
        },
        terminateProperty: function() {
            const vm = this;

            vm.$confirm({
                title: "Terminate Property",
                size: 'md',
                content: "Are you Sure you want to proceed with termination?"
            }).then(function(){
                vm.$alert("continue with termination");
            }).catch(function(){
                vm.$notify({
                    type: 'primary',
                    content: 'You cancelled the property termination request.'
                })
            });

            this.property.terminated = true;

            this.$axios.put(config.API_URL + "/property/terminate/"+this.propertyId, this.property).then(function(response){
                location.reload(true);
            }).catch(function(error){
                console.log(error.data);
            });
        },
        approveProperty: function() {
            const vm = this;

            vm.$confirm({
                title: "Approve Property",
                size: 'md',
                content: "Are you Sure you want to proceed?"
            }).then(function(){
                alert("continue with approval");
            }).catch(function(){
                vm.$notify({
                    type: 'danger',
                    content: 'You cancelled the approval'
                })
            });
        },
        getUnitsArea: function() {
            const url = config.API_URL + "/property-tmp/units-area/"+propertyId;

            this.$axios.get(url).then((response) => {
                this.totalUnitsArea = response.data.totalUnitsArea;
            }).catch((error) => {
                iziToast.error({position: 'topCenter', message: `Error: Failed to fetch the rental units configured area for the property`});
            });
        }
    },
    created: function() {
        this.getUnitsArea();
    }
});
