
/**
 * Created by lebohof on 8/22/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/lodash.min

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

const propertyFormComp = Vue.component("property-form", {
    template: "#property-form",
    data: function () {
        return {
            loader: null,
            openModal: false,
            propertyForm: {
                landlord: null,
                name: null,
                area: 0,
                commission: null,
                description: null,
                address: {
                    suburb: null,
                    street: null,
                    city: null,
                    country: null
                }
            },
            landlordOptions: [],
            options: []
        }
    },
    methods: {
        _onSearchLandlord: function(search, loading) {
            loading(true);
            this.search(loading, search, this);
        },
        search: _.debounce( (loading, search, vm) => {

                const url = config.API_URL + `/landlord/fetch-list?checkStatus=approved&q=${escape(search)}`

                vm.$axios.get(url).then(function(response){
                    vm.options = response.data;
                    loading(false);
                }).catch(function(error){
                    //
                    loading(false);
                })
            }, 350),
        selectCallback: function(event) {

        },
        saveProperty: function() {

            const vm = this;

            vm.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
            });

            this.$validator.validate().then(function (result) {

                if(result){
                    vm.$axios.post(config.API_URL + '/property/save', vm.propertyForm).then(function (response) {
                        const property = response.data;

                        console.log(property)

                        vm.loader.hide();
                        vm.$alert({
                            content: "Property "+property.name+" created successfully.",
                            title: 'Success'
                        }, function () {
                            vm.openModal = false; //close the modal

                            vm.loader = vm.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(property['_links']['self']['href']);
                        })
                    }).catch(function (error) {
                        vm.loader.hide();
                        vm.$notify({
                            type: 'danger',
                            title: 'Error!!!',
                            content: 'failed to save the property record'
                        });
                    })
                } else {
                    vm.loader.hide();
                }

            })
        },
        modalCallback: function (msg) {

        },
    },
    created: function() {

    },
    mounted: function() {
        const vm = this;

        $("#landlordItem").change(function(){
            vm.propertyForm.landlord = $(this).val();
        });

        this.landlordOptions = {
            placeholder: 'Debit Account...',
            ajax: {
                url: config.API_URL + '/account/list',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,

                data: function (params) {
                    const glAccount = 0;

                    var query = {
                        q: params.term,
                        glAccount: glAccount,
                        ac: 'tenant'
                    };

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                }
            }
        };
    }
});

const propertyMainApp = new Vue({
    el: '#main-app',
    data: {
        propertyForm: false
    },
    methods: {

    },
    mounted: function() {},
    created: function() {
    }
});
