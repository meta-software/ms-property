/**
 * Created by lebohof on 12/25/2019
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker
//= require vue-apps/maker-options
//= require vue-apps/components/currency-select

//Setup the plugins.
Vue.use(VeeValidate, { events: 'change' });

const API_URL = config.API_URL + "/bulk-adjust";
const API_CHECK_URL = config.API_URL + "/bulk-adjust-check";

//The page-bar component
Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

/**
 * Lists the bulkAdjusts
 */
const BulkAdjustInbox = Vue.component('bulk-adjust-inbox', {
    template: '#bulk-adjust-inbox',
    data: function() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           bulkAdjusts: []
       }
    },
    created: function() {
        this.loader.isLoading = true;
        bulkAdjustService.inbox().then((response) => {
            this.bulkAdjusts = response.data;
        }).catch((error) => {
            iziToast.error({message: 'Internal error occured while retriving adjust items', timeout: 10000});
        }).finally(() => {
            this.loader.isLoading = false;
        });
    }
});

/**
 * Lists the bulkAdjusts
 */
const BulkAdjustApproved = Vue.component('bulk-adjust-approved', {
    template: '#bulk-adjust-approved',
    data: function() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           bulkAdjusts: []
       }
    },
    created: function() {
        this.loader.isLoading = true;
        bulkAdjustService.approved().then((response) => {
            this.bulkAdjusts = response.data;
        }).catch((error) => {
            iziToast.error({message: 'Internal error occured while retriving adjust items', timeout: 10000});
        }).finally(() => {
            this.loader.isLoading = false;
        });
    }
});

const bulkAdjustService = {
    inbox() {
        const url = API_URL + "/inbox"
        return axios.get(url);
    },
    outbox() {
        const url = API_URL + "/outbox"
        return axios.get(url);
    },
    cancelled() {
        const url = API_URL + "/cancelled"
        return axios.get(url);
    },
    approved() {
        const url = API_URL + "/approved"
        return axios.get(url);
    },
    get(id) {
        const url = API_URL + '/show/' + id;
        return axios.get(url);
    },
    save(bulkAdjust) {
        const url = API_URL + '/save';
        return axios.post(url, bulkAdjust);
    },
    cancel(id) {
        const url = API_URL + '/cancel/' + id;
        return axios.put(url);
    }
};

/**
 * Lists the bulkAdjusts outbox (the ones created by this particular user)
 */
const BulkAdjustOutbox = Vue.component('bulk-adjust-outbox', {
    template: '#bulk-adjust-outbox',
    data: function() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           bulkAdjusts: []
       }
    },
    created: function() {

        bulkAdjustService.outbox().then((response) => {
            this.bulkAdjusts = response.data;
        }).catch((error) => {
            iziToast.error({message: 'Internal error occured while retriving adjust items', timeout: 10000});
        });
    }
});

/**
 * Lists the bulkAdjusts outbox (the ones created by this particular user)
 */
const BulkAdjustCancelled = Vue.component('bulk-adjust-cancelled', {
    template: '#bulk-adjust-cancelled',
    data: function() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           bulkAdjusts: []
       }
    },
    created: function() {
        bulkAdjustService.cancelled().then((response) => {
            this.bulkAdjusts = response.data;
        }).catch((error) => {
            iziToast.error({message: 'Internal error occured while retriving adjust items', timeout: 10000});
        });
    }
});

const BulkAdjustShow = Vue.component('bulk-adjust-show', {
    template: '#bulk-adjust-show',
    data: function() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           bulkAdjust: {
               maker: {},
               amountType: {},
               adjustmentType: {},
               checkStatus: null,
               property: {},
               billingCycle: {},
               subAccount: {},
               bulkAdjustmentItems: [],
               amount: 0
           }
       }
    },
    computed: {
        id: function() {
            return this.$route.params['id'];
        }
    },
    methods: {
        get: function() {
            this.loader.isLoading = true;

            bulkAdjustService.get(this.id).then((response) => {
                this.bulkAdjust = response.data;
            }).catch((error) => {
                iziToast.error({message: 'Internal Error occured while retrieving bulk-adjust record.', timeout: 10000});
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    created: function() {
        this.get(this.id);
    }
});

const BulkAdjustOutboxShow = Vue.component('bulk-adjust-outbox-show', {
    template: '#bulk-adjust-outbox-show',
    data: function() {
       return {
           bulkAdjust: {
               checkStatus: null,
               property: {},
               billingCycle: {},
               bulkAdjustmentItems : [],
               amount: 0
           }

       }
    },
    computed: {
        id: function() {
            return this.$route.param['id'];
        }
    },
    methods: {
        get() {

            bulkAdjustService.get(this.id).then((response) => {
                this.bulkAdjust = response.data;
            }).catch((error) => {
                iziToast.error({message: 'Internal Error occured while retrieving bulk-adjust record.', timeout: 10000});
            });
        }
    },
    created: function() {
        this.get();
    }
});

/**
 * List the simulated bulkAdjust(Items)
 */
const BulkAdjustmentItems = Vue.component("bulk-adjust-items", {
    template: '#bulk-adjust-items',
    props: {
        bulkAdjustmentItems: Array
    },
    data: function() {
        return {
        }
    },
    methods: {}
});

const BulkAdjustCreate = Vue.component('bulk-adjust-create', {
    template: '#bulk-adjust-create',
    data: function() {
       return {
           subAccountOptions: {
               placeholder: 'Sub Account...'
           },
           adjustmentTypeOptions: {
               placeholder: 'Adjustment Type...'
           },
           amountTypeOptions: {
               placeholder: 'Amount Type...'
           },
           propertyTypeOptions: {
               placeholder: 'Amount Type...'
           },
           bulkAdjust: {
               subAccount: {},
               amountType: {},
               adjustmentType: {},
               propertyType: {},
               actionDate: null,
               amount: null
           },
           bulkAdjustmentItems: []
       }
    },
    methods: {
        onSaveBulkAdjust: function() {

            this.loader = this.$loading.show({
                canCancel: true,
                onCancel: this.onCancel,
            });

            bulkAdjustService.save(this.bulkAdjust).then((response) => {
                this.loader.hide();
                iziToast.success({message: 'Bulk Adjust request generated successfully.', timeout: 3000});

                this.$router.push("/");
            }).catch((errors) => {
                this.loader.hide();
                iziToast.error({message: 'Errors occured while saving bulk-adjust.', timeout: 7500});
            })
        },
        onCancelBulkAdjust: function() {
            this.$router.replace("/");
        }
    },
    mounted: function() {
        const vm = this;

        // prepare the other form components
        this.$axios.get(config.API_URL+"/sub-account/list-options").then((resp) => {
            let subAccounts = resp.data.map((type) => {
                return {
                    id: type.id,
                    text: `${type.accountNumber} : ${type.accountName}`
                }
            });

            this.subAccountOptions = {
                placeholder: 'Sub Account...',
                data: subAccounts
            };
        });

        // prepare the other form components
        this.$axios.get(config.API_URL+"/adjustment-type/list-options").then((resp) => {
            let adjustmentTypes = resp.data.map((type) => {
                return {
                    id: type.id,
                    text: type.name
                }
            });

            this.adjustmentTypeOptions = {
                placeholder: 'Adjustment Type...',
                data: adjustmentTypes
            };
        });

        // prepare the other form components
        this.$axios.get(config.API_URL+"/amount-type/list-options").then((resp) => {
            let amountTypes = resp.data.map((type) => {
                return {
                    id: type.id,
                    text: type.name
                }
            });

            this.amountTypeOptions = {
                placeholder: 'Amount Type...',
                data: amountTypes
            };
        });

        this.$axios.get(config.API_URL+"/property-type/list-options").then((resp) => {
            let propertyTypes = resp.data.map((type) => {
                return {
                    id: type.id,
                    text: type.name
                }
            });

            this.propertyTypeOptions = {
                placeholder: 'Property Type...',
                data: propertyTypes
            };
        });

        //prepare jquery items
        $(this.$refs.actionDate).datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true}).on(
            "changeDate", function() {
                vm.bulkAdjust.transactionDate = $(this).val()
            }
        );
    }
});

// Prepare the application routes here.
// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: BulkAdjustInbox},
    {path: "/inbox", name: 'homeInbox', component: BulkAdjustInbox},
    {path: "/inbox/:id", name: 'inboxItem', component: BulkAdjustShow},
    {path: "/approved", name: 'approved', component: BulkAdjustApproved},
    {path: "/approved/:id", name: 'approvedItem', component: BulkAdjustShow},
    {path: "/outbox", name: 'homeOutbox', component: BulkAdjustOutbox},
    {path: "/outbox/:id", name: 'showOutboxItem', component: BulkAdjustOutboxShow},
    {path: "/trash", name: 'trash', component: BulkAdjustCancelled},
    {path: "/trash/:id", name: 'showTrashItem', component: BulkAdjustCancelled},
    {path: "/create", name: 'create', component: BulkAdjustCreate}
];

// the application router.
const router = new VueRouter({
    routes: routes
});
// prepare the application routes here


jQuery(document).ready( () => {

    //This is the main app
    const vm = new Vue({
        el: "#app",
        router: router,
        created: function () {
            console.log('created the main bulk-adjust Vue app successfully.')
        }
    });

});