/**
 * Created by lebohof on 7/28/2018.
 */
//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require global/plugins/vue/autocomplete/autocomplete.min
//= require vue-apps/components/account-select
//= require vue-apps/components/sub-account-select
//= require vue-apps/components/lease-select
//= require vue-apps/components/currency-select
//= require vue-apps/components/gl-account-select
//= require vue-apps/components/property-select
//= require vue-apps/components/exchange-rate

function disableAmountField () {
    return !(this.mainTransaction.transactionCurrency && this.mainTransaction.transactionDate)
};

function totalFormatter(data) {

    let total = data.map(function (row) {
         return row.amount
    }).reduce(function (sum, i) {
        return sum + i
    }, 0);

    return numberFormat(total);
}

function transCountFormatter(data) {
    var field = this.field;

    return data.map(function (row) {
        return 1
    }).reduce(function (sum, i) {
        return sum + i
    }, 0);
}

Vue.use(Vuex);

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            landlord: 'Landlord',
            tenant: 'Tenant',
            subAccount: 'Sub Account',
            transactionReference: 'Reference',
            transactionDate: 'Transaction Date',
            amount: 'Amount',
            supplier: 'Supplier',
            lease: 'Lease',
            bankAccount: 'Bank Account',
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

//store mutations
const UPDATE_GL_BANKACCOUNTS = "UPDATE_GL_BANK_ACCOUNTS"

// prepare the global store
const store = new Vuex.Store({
    state: {
        transactionTypes : [],
        transactionBatch: {},
        glBankAccounts: []
    },
    actions: {
        getGlBankAccounts({commit, state}) {
            if(state.glBankAccounts && state.glBankAccounts.length === 0) {
                axios.get(config.API_URL +'/general-ledger/banks').then( async (response) => {
                    commit(UPDATE_GL_BANKACCOUNTS, await response.data);
                })
            }
        }
    },
    mutations: {
        transactionTypes(state, types) {
            state.transactionTypes = types;
        },
        transactionBatch(state, transactionBatch) {
            state.transactionBatch = transactionBatch;
        },
        [UPDATE_GL_BANKACCOUNTS](state, payload) {
            state.glBankAccounts = payload;
        }
    },
    getters: {
        transactionType: (state) => (category) => {
            // filter the transactionType
            return state.transactionTypes.find( obj => obj.transactionCategory === category)
        },
        transactionBatch: (state) => {
            return state.transactionBatch;
        },
        glBankAccounts: (state) => {
            return state.glBankAccounts;
        }
    }
});

const TRANS_API = config.API_URL+"/transaction-tmp";
const API_URL = config.API_URL+"/transactions";
const batchTmpService = {
    pending(queryParams) {
        return axios.get(API_URL, {params: queryParams});
    },
    postBatch(id) {
        return axios.put(API_URL+'/post-batch/'+id, {});
    },
    createBatch(postData) {
        return axios.post(API_URL+'/save', postData)
    },
    deleteBatch(id) {
        return axios.delete(API_URL+"/delete/"+id);
    },
    deleteTransaction(id) {
        return axios.delete(TRANS_API+"/delete/"+id);
    },
    get(id) {
        return axios.get(API_URL+"/show/"+id);
    },
    listBatchTransactions(headerId) {
        return axios.get(TRANS_API+"/list-by-header/"+headerId);
    },
    postTransaction(url, payload) {
        return axios.post(url, payload);
    }
};

Vue.component('trans-batch-filter', {
    template: '#trans-batch-filter',
    data() {
        return {
            transactionType: '',
            batchNumber: ''
        }
    },
    methods: {
        search() {
            this.$emit('search', this.searchParams)
        },
        reset() {
            this.transactionType = ''
            this.batchNumber = ''
            this.search();
        }
    },
    computed: {
        searchParams() {
            return {
                transactionType: this.transactionType,
                batchNumber: this.batchNumber
            }
        }
    }
});

const TransHeaderListComp = Vue.component('trans-batch-list', {
    template: '#trans-batch-list',
    data: function () {
        return {
            transactionHeaderList: []
        }
    },
    methods: {
        createBatch: function(transactionCategory) {
            const url = config.API_URL + "/transactions/save";
            let transactionType = this.$store.getters.transactionType(transactionCategory);
            const postData = {
                transactionType: transactionType
            };

            const loader = overlayLoader.fullPageLoader()
            batchTmpService.createBatch({transactionType: transactionType}).then( (resp) => {
                let transaction = resp.data;
                // we want to push the router to editing this new transaction
                this.$router.push({name: 'editBatch', params: {id: transaction.id}});
            }).catch( (error) => {
                iziToast.error({position: 'topCenter', message: "Failed to create the batch transaction"})
            }).finally(() => loader.hide() );
        },

        reset() {
            this.fetchTransactions()
        },

        search(searchParams) {
            this.fetchTransactions(searchParams);
        },

        fetchTransactions: function (searchParams) {
            const loader = overlayLoader.fullPageLoader();
            batchTmpService.pending(searchParams).then((resp) => {
                this.transactionHeaderList = resp.data;
            }).catch((err) => {
                iziToast.error({position: 'topCenter', message: "Error while fetching the Batch Transactions"});
            }).finally(() => loader.hide() );
        },

        removeTransactionHeader: function(transactionHeader) {
            this.$confirm({
                title: 'Confirm Delete?',
                content: "Are you sure you want to Delete the Batch? The action is irreversible.",
                backdrop: false
            }).then( (msg) => {
                const loader = overlayLoader.fullPageLoader()
                batchTmpService.deleteBatch(transactionHeader.id).then((resp) => {
                    //now we update the transactionHeader list.
                    iziToast.success({position: 'topCenter', message: `Transaction Batch deleted successfully !!`});
                    this.fetchTransactions();
                }).catch( (err) => {
                    console.error(err)
                    iziToast.error({position: 'topCenter', message: `Error when trying to delete transaction batch !!!`});
                }).finally(() => loader.hide() );
            });
        }
    },
    created: function () {
        this.fetchTransactions();
    }
});

Vue.component('trans-list-comp', {
    template: "#trans-list-comp",
    components: {
        BootstrapTable: BootstrapTable
    },
    data: function () {
        return {
            transactions: [],
            options: {
                onClickRow: this.handleClickRow ,
                onClickCell: this.handleClickCell ,
                search: false,
                showSearchClearButton: true,
                searchOnEnterKey: true,
                showButtonIcons: true,
                showSearchButton: true,
                showFooter: true,
                height: '450',
                footer: true,
                classes: "table table-striped table-hover table-condensed"
            },
            columns: [{
                title: "Credit Acc",
                field: "creditAccount",
                formatter: (e) => shortify(e, 64)
            },{
                title: "Sub Acc",
                field: "subAccountName"
            },{
                title: "Debit Acc",
                field: "debitAccount",
                formatter: (e) => shortify(e, 64)
            },{
                title: "Txn Ref",
                field: "transactionReference",
                footerFormatter: () => "Trans Count: "
            },{
                title: "Txn Date",
                field: "transactionDate",
                formatter: (e) => moment(e).format("YYYY-MMM-DD"),
                footerFormatter: transCountFormatter
            },{
                title: "Txn Details",
                field: "description",
                formatter: (e) => shortify(e, 64),
                footerFormatter: () => "Control Total: "
            },{
                title: "Amount ($)",
                field: "amount",
                formatter: (value) => formatNumber(value),
                footerFormatter: totalFormatter
            },{
                title: '<span class="display-block"><i class="fa fa-trash-o"></i></span>',
                field: "action",
                clickToSelect: false,
                formatter: (e, v, t) => {
                    return '<a type="button" class="btn btn-xs"><i class="fa fa-times font-red-haze"></i></a>';
                },
            }]
        }
    },
    computed: {
        transactionsTotal() {
            return this.$store.getters.transactionBatch.transactionsTotal;
        },
        transactionHeader() {
            return this.$store.getters.transactionBatch;
        }
    },
    methods: {
        handleClickRow(row, $el, field) {
            if(field === 'action')
                return;

            this.$emit("on-click-transaction", row.id);
        },
        handleClickCell(field, value, row, $el) {
            if(field === 'action') {
                this.$emit("delete-transaction", row.id);
            }
        },
        loadTransactions() {

            const loader = overlayLoader.fullPageLoader()
            batchTmpService.listBatchTransactions(this.$route.params.id).then((resp) => {
                this.transactions = resp.data;

                this.transactions = this.transactions.map((txn) => {
                    txn.creditAccount = txn.creditAccount + " : " + txn.creditAccountName;
                    txn.debitAccount = txn.debitAccount + " : " + txn.debitAccountName;

                    return txn;
                })
            }).finally(() => loader.hide() );
        },
        deleteTransaction(evt, txnId) {
            const loader = overlayLoader.fullPageLoader()

            batchTmpService.deleteTransaction(txnId).then((resp) => {
                this.loadTransactions();
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created() {
        this.loadTransactions();
    }
});

const TransBatchEdit = Vue.component('trans-batch-edit', {
    template: '#trans-batch-edit',
    data: function () {
        return {
            transactionHeader: {
                transactionType: {}
            },
            currentFormComponent: ''
        }
    },
    methods: {
        loadComponent: function(componentName) {
            this.currentFormComponent = componentName;
        },
        onPostTransaction: function(event) {
            iziToast.success({message: "Transaction posted successfully", position: "topCenter"})

            this.$refs.transactionList.loadTransactions();
            this.refreshHeader();
        },
        deleteTransaction: function(txnId) {

            if(this.transactionHeader) {
                const loader = overlayLoader.fullPageLoader()
                batchTmpService.deleteTransaction(txnId).then( (resp) => {
                    //now we refresh the List component array
                    this.$emit('transaction-deleted', txnId);
                    iziToast.success({message: "Transaction deleted successfuly.", position: "topCenter"});

                    this.refreshHeader();
                    this.$refs.transactionList.loadTransactions();
                }).catch( (err) => {
                    console.log(err);
                    iziToast.error({position: "topCenter", message: "Failed to delete the transaction.c"})
                }).finally(() => loader.hide());
            }
        },
        initComponent: function() {
            this.getTransactionHeader();
        },
        refreshHeader() {
            const loader = overlayLoader.fullPageLoader()
            return batchTmpService.get(this.id).then((resp) => {
                this.$store.commit('transactionBatch', resp.data);
                this.transactionHeader = resp.data;
            }).catch((err) => {
                if(err.status === 404) {
                    iziToast.warning({message: "The Transaction Batch Request does not exist anymore"})
                }
                this.$router.push({path: "/"});
            }).finally(() => loader.hide() )
        },
        getTransactionHeader: function() {
            const loader = overlayLoader.fullPageLoader()
            this.refreshHeader().then(() => {
                // decide on which component to use for the selected batch
                switch(this.transactionHeader.transactionType.transactionCategory) {
                    case 'payment':
                        this.loadComponent('payment-form');
                        break;
                    case 'receipt':
                        this.loadComponent('receipting-form-comp');
                        break;
                    case 'invoice':
                        this.loadComponent('invoice-form');
                        break;
                    case 'balance':
                        this.loadComponent('balance-form-comp');
                        break;
                    case 'refund':
                        this.loadComponent('refund-form-comp');
                        break;
                    case 'cancel':
                        this.loadComponent('cancel-form-comp');
                        break;
                    case 'transfer':
                        this.loadComponent('transfer-form-comp');
                        break;
                    case 'deposit-payment':
                        this.loadComponent('deposit-payment-form');
                        break;
                    case 'landlord-payment':
                        this.loadComponent('landlord-payment-form');
                        break;
                    default:
                        iziToast.error({overlay: true, timeout: 0, position: "center", message: "Unexpected error occured. Please notify Administrator."})
                        break;
                }
            }).finally(() => loader.hide());

        }
    },
    computed: {
        id() { return this.$route.params.id; }
    },
    created () {
        this.initComponent()
    }
});

const TransHeaderDemo = Vue.component('trans-header-demo', {
    template: "#trans-header-demo",
    data: function () {
        return {
            title: "Transaction Header Demo Component"
        }
    }
});

Vue.component('trans-header-form', {
    template: "#trans-header-form",
    data: function () {
        return {}
    },
    props: ['transactionHeader'],
    methods: {
        postHeaderTransaction: function(event) {
            const vm = this;
            const loader = overlayLoader.fullPageLoader()

            batchTmpService.postBatch(this.id).then((resp) => {
                iziToast.success({message: "Transactions posted successfully.", position: "topCenter"});
                vm.$router.push("/");
            }).catch(function(error){
            }).finally(() => loader.hide() );
        },
        discardHeaderTransaction: function(event) {
            this.$confirm({
                title: 'Confirm Discard?',
                content: "Are you sure you want to Discard the Batch? The action will delete the batch and this action is not reversible.",
                backdrop: false
            }).then( (msg) => {
                const loader = overlayLoader.fullPageLoader()
                batchTmpService.deleteBatch(this.id).then((resp) => {
                    //now we update the transactionHeader list.
                    iziToast.success({position: 'topCenter', message: `Transaction Batch deleted successfully !!`});
                    this.$router.push("/");
                }).catch( (err) => {
                    iziToast.error({position: 'topCenter', message: `Error when trying to delete transaction batch !!!`});
                }).finally(() => loader.hide() );
            });
        }
    },
    computed: {
        id() {
            return this.$route.params.id;
        },
        batchNotPostable() {
            return this.transactionHeader.transactionsCount === 0
        }
    }
});

// payment form
Vue.component("payment-form", {
    template: "#payment-form",
    data: function () {
        return {
            mainTransaction: {},
            creditAccountOptions: {},
            debitAccountOptions: {
                placeholder: 'Debit Account...',
                allowClear: true
            }
        }
    },
    methods: {
        initForm: function() {
            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('payment'),
                rentalUnit: {
                    id: null,
                    name: null,
                    property: {
                        id: null,
                        name: ""
                    }
                },
                entryNumber: null,
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                creditAccount: "",
                debitAccount: "",
                creditSubAccount: "",
                debitSubAccount: "",
                amount: "",
                description: "",
                subAccount: "",
                transactionBatchTmp: this.transactionBatch
            };

            // debit account options
            this.debitAccountOptions = {
                placeholder: 'Landlord...',
                ajax: {
                    url: config.API_URL + '/landlord/list-options',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        'accepts': 'application/json'
                    },
                    delay: 300,
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        let options = data.map((option) => {
                            return {
                                id: option.accountNumber,
                                text: `${option.accountNumber} : ${option.accountName}`
                            };
                        });

                        return {
                            results: options
                        };
                    },
                    data: function (params) {
                        return {
                            q: params.term
                        };
                    }
                }
            };

        },
        async postTransaction (event) {
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'center'
            });

            try {
                const result = await this.$validator.validate();
                if(result) {
                    const resp = await axios.post(`${config.API_URL}/transaction-tmp/payment`, this.mainTransaction);
                    this.$emit('post-transaction', event);
                    this.initForm()
                }
            } finally{
                this.loader.hide();
            }
        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    computed: {
        disableAmountField
    },
    created: function() {
        this.initForm();
    },
    mounted: function () {

        // credit account options
        this.$axios.get(config.API_URL +'/general-ledger/banks').then((response) => {
            let options = response.data.map((option) => {
                return {id: option.accountNumber, text: option.accountNumber + " : " + option.accountName}
            });

            this.creditAccountOptions = {
                placeholder: 'Debit Account...',
                allowClear: true,
                data: options
            };
        });
    }
});

// deposit payment form
Vue.component("deposit-payment-form", {
    template: "#deposit-payment-form",
    data: function () {
        return {
            mainTransaction: {},
            creditAccountOptions: {},
            debitAccountOptions: {},
            bankAccountOptions: {}
        }
    },
    methods: {
        initForm: function() {
            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('deposit-payment'),
                lease: {
                    rentalUnit: {},
                    property: {}
                },
                entryNumber: null,
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                creditAccount: "",
                debitAccount: "",
                bankAccount: "",
                creditSubAccount: "",
                debitSubAccount: "",
                amount: "",
                description: "",
                subAccount: "",
                transactionBatchTmp: this.transactionBatch
            };
        },
        postTransaction: function (event) {
            const vm = this;

            this.$validator.validate().then(function (result) {
                if(result) {
                    vm.loader = vm.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                        placement: 'center'
                    });

                    vm.$axios.post(config.API_URL + "/transaction-tmp/deposit-payment", vm.mainTransaction).then(function(response) {
                        //also update the transactions list by updating the
                        vm.$emit('post-transaction', event);
                        vm.initForm();
                    }).finally(() => {
                        vm.loader.hide();
                    });
                }
            });

        },
        onSelectCreditAccount: function (value) {
            this.mainTransaction.lease = null;
        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    created: function() {
        this.initForm();
    },
    mounted: function () {
        const vm = this;

        // prepare the chargeCode options
        this.debitAccountOptions = {
            placeholder: 'Supplier Account...',
            allowClear: true,
            ajax: {
                url: config.API_URL + '/account/list',
                delay: 300,

                data: function (params) {
                    return {
                        q: params.term,
                        ac: 'provider'
                    };
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    let options = data['results'].map((option) => {
                        return {
                            id: option.accountNumber,
                            text: option.text
                        };
                    });

                    return {
                        results: options
                    };
                }
            }

        };

        // prepare the debit account select options
        this.creditAccountOptions = {
            placeholder: 'Tenant Account...',
            allowClear: true,
            ajax: {
                url: config.API_URL + '/account/list',
                delay: 300,

                data: function (params) {
                    return {
                        q: params.term,
                        ac: 'tenant'
                    };
                },
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'
                    let options = data['results'].map((option) => {
                        return {
                            id: option.accountNumber,
                            text: option.text
                        };
                    });

                    return {
                        results: options
                    };
                }
            }
        };

        // bank account options
        this.$axios.get(config.API_URL +'/general-ledger/banks').then((response) => {
            let options = response.data.map((option) => {
                return {id: option.accountNumber, text: option.accountNumber + " : " + option.accountName}
            });

            this.bankAccountOptions = {
                placeholder: 'Bank Account...',
                allowClear: true,
                data: options
            };
        });
    }
});

// landlord payment form
const LandlordPaymentFormComp = Vue.component("landlord-payment-form", {
    template: "#landlord-payment-form",
    data: function () {
        return {
            mainTransaction: {},
            creditAccountOptions: {},
            debitAccountOptions: {},
            bankAccountOptions: {},
            propertyOptions: {
                placeholder: 'Property...',
                allowClear: true
            },
            rentalUnitOptions: {
                placeholder: 'Lease...',
                allowClear: true,
                data: []
            },
            subAccountOptions: {
                placeholder: 'Sub Account...',
                allowClear: true
            }
        }
    },
    methods: {
        initForm: function() {

            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('landlord-payment'),
                property: {},
                entryNumber: null,
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                creditAccount: "",
                debitAccount: "",
                creditSubAccount: "",
                debitSubAccount: "",
                bankAccount: "",
                amount: "",
                description: "",
                subAccount: "",
                transactionBatchTmp: this.transactionBatch
            };

        },
        async postTransaction (event) {

            const loader = overlayLoader.fullPageLoader();
            try {
                const result = await this.$validator.validate();
                if(result) {
                    const resp = await axios.post(config.API_URL + "/transaction-tmp/landlord-payment", this.mainTransaction);
                    this.$emit('post-transaction', event);
                    this.initForm();
                }
            } catch (errors) {
                console.log('the errors: ', errors);
                if(errors.response.data['message']) {
                    iziToast.error({
                        title: 'Transaction Error!',
                        position: 'topCenter',
                        timeout: 10000,
                        message: errors.response.data['message']
                    });
                }
            } finally {
                loader.hide()
            }
        },
        onSelectDebitAccount: function (value) {

            if(this.mainTransaction.debitAccount){
                const debitAccount = this.mainTransaction.debitAccount;
                const propertyUrl = config.API_URL + "/landlord/"+debitAccount+"/properties";

                this.propertyOptions = {
                    placeholder: 'Properties...',
                    ajax: {
                        url: propertyUrl,
                        dataType: 'json',
                        contentType: "application/json; charset=utf-8",
                        headers: {
                            'accepts': 'application/json'
                        },
                        delay: 350,
                        processResults: function (data) {
                            // Transforms the top-level key of the response object from 'items' to 'results'
                            let options = data.map((option) => {
                                return {
                                    id: option.id,
                                    text: option.name
                                };
                            });

                            return {
                                results: options
                            };
                        },
                        data: function (params) {
                            return {
                                q: params.term
                            };
                        }
                    }
                };
            }
        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    created: function() {
        this.initForm();
    },
    mounted: function () {
        const vm = this;

        // prepare the chargeCode options
        this.creditAccountOptions = {
            placeholder: 'Credit Account...',
            ajax: {
                url: config.API_URL + '/provider/list-options',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'

                    let options = data.map((option) => {
                        return {
                            id: option.accountNumber,
                            text: `${option.accountNumber} : ${option.accountName}`
                        };
                    });

                    return {
                        results: options
                    };
                },
                data: function (params) {
                    return {
                        q: params.term
                    };
                }
            }
        };

        // prepre the debit account select options
        this.debitAccountOptions = {
            placeholder: 'Debit Account...',
            ajax: {
                url: config.API_URL + '/landlord/list-options',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'

                    let options = data.map((option) => {
                        return {
                            id: option.accountNumber,
                            text: `${option.accountNumber} : ${option.accountName}`
                        };
                    });

                    return {
                        results: options
                    };
                },
                data: function (params) {
                    return {
                        q: params.term
                    };
                }
            }
        };

        // bank account options
        this.$axios.get(config.API_URL +'/general-ledger/banks').then((response) => {
            let options = response.data.map((option) => {
                return {id: option.accountNumber, text: option.accountNumber + " : " + option.accountName}
            });

            this.bankAccountOptions = {
                placeholder: 'Bankt Account...',
                allowClear: true,
                data: options
            };

        });

        //sub-account options
        // prepare the chargeCode options
        this.$axios.get(config.API_URL + '/sub-account/list-options').then((response) => {
            let subAccounts = response.data.map((subAccount) => {
                return {id: subAccount.id, text: subAccount.accountName};
            });

            this.subAccountOptions = {
                placeholder: 'Sub Account...',
                data: subAccounts
            };
        });

        //rentalUnit options
        this.rentalUnitOptions =  {
            placeholder: 'Lease...',
            allowClear: true,
            data: []
        };

        //property options setup
        this.propertyOptions = {
            placeholder: 'Property...',
            allowClear: true
        };

        $(".date-picker").datepicker({format: 'yyyy-mm-dd', autoclose: true, orientation: 'bottom left'}).on(
            "changeDate", function() { vm.mainTransaction.transactionDate = $(this).val() }
        );
    }
});

// refund form
const RefundFormComp = Vue.component("refund-form-comp", {
    template: "#refund-form-comp",
    data: function () {
        return {
            mainTransaction: {},
            referenceOptions: {}
        }
    },
    methods: {
        initForm: function() {

            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('refund'),
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                transactionReference: "",
                description: "",
                transactionBatchTmp: this.transactionBatch
            };
        },
        async postTransaction (event) {
            const loader = overlayLoader.fullPageLoader();
            try {
                const result = await this.$validator.validate();
                if(result) {
                    const resp = await this.$axios.post(config.API_URL + "/transaction-tmp/refund", this.mainTransaction);
                    this.$emit('post-transaction', event);
                    this.initForm();
                    console.log('transaction posted successfully: ', response.data);
                }
            } finally {
                loader.hide()
            }
        },
        prepareAutocomplete : function() {
            const apiurl = config.API_URL + '/transaction/payments'
            const params = ''

            new Autocomplete('#autocomplete', {

                // search for payment transactions (refund)
                search: input => {
                    const url = `${apiurl}?ref=${encodeURI(input)}`

                    return new Promise(resolve => {
                        if (input.length < 3) {
                            return resolve([])
                        }

                        fetch(url)
                            .then(response => response.json())
                            .then(data => {
                                resolve(data)
                            })
                    })
                },

                getResultValue: result => result.transactionReference,

                // Open the selected article in
                // a new window
                onSubmit: result => {
                    this.mainTransaction.transactionReference = result.transactionReference
                }
            })
        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    created: function() {
        this.initForm();
    },
    mounted: function () {
        const formatTransaction = function (transaction) {
            if (!transaction.id) {
                return transaction.transactionReference;
            }

            var $transaction = $(
                '<span> ref=' + transaction.text + ', crAcc= ' + transaction.accountNumberCr +', amt='+transaction.amount+'</span>'
            );

            return $transaction;
        };

        const vm = this;
        // credit account options
        this.creditAccountOptions = {
            placeholder: 'Bank Account...',
            ajax: {
                url: config.API_URL + '/account/list',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,

                data: function (params) {
                    const glAccount = 1  // vm.mainTransaction.creditGlAccount ? 1 : 0;
                    return {
                        q: params.term,
                        glAccount: glAccount
                    };
                }
            }
        };

        // debit account options
        this.referenceOptions = {
            placeholder: 'Txt Reference...',
            templateResult: formatTransaction,
            ajax: {
                url: config.API_URL + '/transaction/receipts',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                data: function (params) {
                    return {
                        ref: params.term
                    };
                },
                processResults: function (data) {
                    let results = {results: []};

                    for(let i = 0; i < data.length; i++  ) {
                        let obj = data[i];
                        let option = {id: obj.transactionReference,
                            text: obj.transactionReference,
                            accountNumberCr: obj.accountNumberCr,
                            amount: Math.abs(obj.debit-obj.credit)
                        };
                        results['results'].push(option);
                    }

                    return results;
                }
            }
        };
    }
});

// cancel form
const CancelFormComp = Vue.component("cancel-form-comp", {
    template: "#cancel-form-comp",
    data: function () {
        return {
            mainTransaction: {},
            referenceOptions: {}
        }
    },
    methods: {
        initForm: function() {

            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('cancel'),
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                amount: "",
                transactionReference: "",
                description: "",
                transactionBatchTmp: this.transactionBatch
            };
        },
        async postTransaction(event) {

            const loader = overlayLoader.fullPageLoader();

            try {
                const result = await this.$validator.validate();
                if(result) {
                    const resp = await axios.post(config.API_URL + "/transaction-tmp/cancel", this.mainTransaction)
                    this.$emit('post-transaction', event);
                    this.initForm();
                    console.log('transaction posted successfully: ', response.data);
                }
            } finally {
                loader.hide()
            }
        },
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    created: function() {
        this.initForm();
    },
    mounted: function () {
        const vm = this;

        const formatTransaction = function (transaction) {
            if (!transaction.id) {
                return transaction.transactionReference;
            }

            return $(
                '<span>ref=' + transaction.text + ', crAcc= ' + transaction.accountNumberCr + ', amt=' + transaction.amount + '</span>'
            );
        };

        // debit account options
        this.referenceOptions = {
            placeholder: 'Txt Reference...',
            templateResult: formatTransaction,
            ajax: {
                url: config.API_URL + '/transaction/cancellation-transactions',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                data: function (params) {
                    return {
                        ref: params.term
                    };
                },
                processResults: function (data) {
                    let results = {results: []};

                    for(let i = 0; i < data.length; i++  ) {
                        let obj = data[i];
                        let option = {id: obj.transactionReference,
                            text: obj.transactionReference,
                            accountNumberCr: obj.accountNumberCr,
                            amount: (obj.debit - obj.credit)
                        };
                        results['results'].push(option);
                    }

                    return results;
                }
            }
        };
    }
});

// receipt form (taking money from tenant)
const ReceiptingFormComp = Vue.component("receipting-form-comp", {
    template: "#receipting-form-comp",
    data: function () {
        return {
            mainTransaction: {},
            creditAccountOptions: {},
            creditRentalUnitOptions: {},
            debitAccountOptions: {placeholder: "Nothing..."},
            subAccountOptions: {}
        }
    },
    methods: {
        initForm: function() {
            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('receipt'),
                lease: {
                    id: null
                },
                subAccount: null,
                entryNumber: null,
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                creditAccount: "",
                debitAccount: null,
                creditSubAccount: "",
                debitSubAccount: "",
                amount: "",
                transactionReference: "",
                description: "",
                transactionBatchTmp: {
                    id: this.transactionBatch.id
                }
            };
        },
        onSelectRentalUnit: function(evt){},
        postTransaction: function (event) {
            const vm = this;
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'center'
            });

            this.$validator.validate().then(function (result) {
                if(result) {
                    vm.$axios.post(config.API_URL + "/transaction-tmp/receipt", vm.mainTransaction).then(function(response) {
                        vm.loader.hide();
                        //also update the transactions list by updating the
                        vm.$emit('post-transaction', event);
                        vm.initForm();
                        console.log('transaction posted successfully: ', response.data);
                        //iziToast.success({message: "Transaction posted sucessfully", position: "topCenter"});
                    }).catch( function(errors) {
                        //also update the jquery select2 object states.
                        console.error('errors during posting of transaction:  ', errors.response.data['message']);
                        vm.loader.hide();

                        if(errors.response.data['message']) {
                            vm.$notify({
                                title: 'Transaction Error!',
                                type: 'danger',
                                content: errors.response.data['message'],
                                duration: 10000
                            })
                        }
                    });
                } else {
                    vm.loader.hide();
                }
            });

            console.log("posting the transaction: \n", this.mainTransaction)
        },
        onSelectCreditAccount: function (value) {
            if(this.mainTransaction.creditAccount){
                const creditAccount = this.mainTransaction.creditAccount;
                this.$axios.get(config.API_URL + "/tenant/"+creditAccount+"/leases").then( (resp) => {
                    if(resp.data.length === 1) {
                        this.mainTransaction.lease = resp.data[0];
                        console.log('the transaction: ', this.mainTransaction);
                    }
                }).catch( (err) => {
                    console.error('error: ', err);
                });
            }

        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    created: function() {
        this.initForm();

        this.$axios.get(config.API_URL +'/general-ledger/banks').then((response) => {
            let options = response.data.map((option) => {
                return {id: option.accountNumber, text: option.accountNumber + " : " + option.accountName}
            });

            this.debitAccountOptions = {
                placeholder: 'Debit Account...',
                allowClear: true,
                minimumResultsForSearch: Infinity,
                data: options
            };
        });
    },
    mounted: function () {
        const vm = this;

        // prepare the chargeCode options
        this.$axios.get(config.API_URL + '/sub-account/list-options').then((response) => {
            let subAccounts = response.data.map((subAccount) => {
                return {id: subAccount.id, text: `${subAccount.accountNumber} : ${subAccount.accountName}`};
            });

            vm.subAccountOptions = {
                placeholder: 'Sub Account...',
                data: subAccounts
            };
        });

        //
        this.creditAccountOptions = {
            placeholder: 'Tenant Account...',
            ajax: {
                url: config.API_URL + '/account/list',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,

                data: function (params) {
                    const glAccount = vm.mainTransaction.creditGlAccount ? 1 : 0;
                    return {
                        q: params.term,
                        glAccount: glAccount,
                        ac: 'tenant'
                    };
                }
            }
        };

        //creditSubAccountOptions setup
        this.$axios.get(config.API_URL+"/sub-account/list-options/").then( (resp) => {
            let data = $.map(resp.data, function(data) {
                return {
                    id: data.accountNumber,
                    text: `${data.accountNumber} : ${data.accountName}`
                };
            });

            data.unshift({id: '', text: 'Select Account...'});
            vm.creditSubAccountOptions = {
                data: data
            }
        });

        //We also need to update the lease selection options.
        this.creditRentalUnitOptions = {
            placeholder: 'Lease...',
            minimumResultsForSearch: -1,
            ajax: {
                url: config.API_URL + '/tenant/leases',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                data: function (params) {
                    const creditAccount = vm.mainTransaction.creditAccount;
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        q: params.term,
                        accountNumber: creditAccount,
                    };
                },
                processResults: function (data) {
                    let results = {results: []};
                    for(let i = 0; i < data.length; i++  ) {
                        let obj = data[i];
                        let rentalUnit = obj.rentalUnit;
                        let textDisplay = `${obj.leaseNumber} - ${rentalUnit.unitReference} : ${rentalUnit.name}`
                        let option = {id: obj.id, text: textDisplay};
                        results['results'].push(option);
                    }

                    return results;
                }
            }
        };

        //@todo: use the vuejs date picker, it is good enough.
        $(".date-picker").datepicker({format: 'yyyy-mm-dd', maxDate: '+0d', autoclose: true, orientation: 'bottom left'}).on(
            "changeDate", function() { vm.mainTransaction.transactionDate = $(this).val() }
        );
    }
});

// receipt form (taking money from tenant)
const InvoiceFormComp = Vue.component("invoice-form", {
    template: "#invoice-form",
    data: function () {
        return {
            mainTransaction: {},
            creditAccountOptions: {
                placeholder: 'Tenant Account...',
                allowClear: true,
                ajax: {
                    url: config.API_URL + '/account/list',
                    delay: 300,

                    data: function (params) {
                        return {
                            q: params.term,
                            ac: 'tenant'
                        };
                    },
                    processResults: function (data) {
                        // Transforms the top-level key of the response object from 'items' to 'results'
                        let options = data['results'].map((option) => {
                            return {
                                id: option.accountNumber,
                                text: option.text
                            };
                        });

                        return {
                            results: options
                        };
                    }
                }

            }
        }
    },
    methods: {
        initForm: function() {
            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('invoice'),
                lease: {
                    rentalUnit: {},
                    property: {},
                    id: null
                },
                subAccount: null,
                entryNumber: null,
                transactionDate: null, // moment().format('YYYY-MM-DD', new Date()),
                creditAccount: "",
                debitAccount: "",
                creditSubAccount: "",
                debitSubAccount: "",
                amount: "",
                description: "",
                transactionBatchTmp: this.transactionBatch
            };
        },
        postTransaction: function (event) {
            const vm = this;

            this.$validator.validate().then( (result) => {

                if(result) {
                    vm.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                        placement: 'center'
                    });

                    this.mainTransaction.debitAccount = this.mainTransaction.creditAccount;

                    vm.$axios.post(config.API_URL + "/transaction-tmp/invoice", vm.mainTransaction).then(function(response) {
                        vm.loader.hide();
                        //also update the transactions list by updating the
                        vm.$emit('post-transaction', event);
                        vm.initForm();
                    }).catch( function(errors) {
                        //also update the jquery select2 object states.
                        console.error('errors during posting of transaction:  ', errors.response.data['message']);
                        vm.loader.hide();

                        if(errors.response.data['message']) {
                            iziToast.error({
                                title: "Transaction Error!",
                                position: "center",
                                message:  errors.response.data['message'],
                                timeout: 0,
                                overlay: true
                            });
                        }
                    }).finally(() => {
                        vm.loader.hide();
                    });
                }
            });

            console.log("posting the transaction: \n", this.mainTransaction)
        },
        onSelectCreditAccount: function (value) {
            this.mainTransaction.lease.id = null;
        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        }
    },
    computed: {
        disableAmountField
    },
    created: function() {
        this.initForm();
    }
});

// deposit invoice form (taking money from tenant)
const InterbankTransfer = Vue.component("transfer-form-comp", {
    template: "#transfer-form-comp",
    data: function () {
        return {
            mainTransaction: {},
            creditAccountOptions: {},
            debitAccountOptions: {}
        }
    },
    methods: {
        initForm: function() {
            this.mainTransaction = {
                id: null,
                transactionType: this.$store.getters.transactionType('transfer'),
                entryNumber: null,
                transactionDate: null,
                creditAccount: "",
                debitAccount: "",
                creditSubAccount: "",
                debitSubAccount: "",
                amount: "",
                description: "",
                transactionBatchTmp: this.transactionBatch
            };
        },
        postTransaction: function (event) {
            const vm = this;
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'center'
            });

            this.$validator.validate().then( (result) => {
                if(result) {
                    vm.$axios.post(config.API_URL + "/transaction-tmp/transfer", vm.mainTransaction).then(function(response) {
                        vm.loader.hide();
                        //also update the transactions list by updating the
                        vm.$emit('post-transaction', event);
                        vm.initForm();
                    }).catch( function(errors) {
                        //also update the jquery select2 object states.
                        console.error('errors during posting of transaction:  ', errors.response.data['message']);
                        vm.loader.hide();

                        if(errors.response.data['message']) {
                            iziToast.error({
                                title: "Transaction Error!",
                                position: "center",
                                message:  errors.response.data['message'],
                                timeout: 0,
                                overlay: true
                            });
                        }
                    });
                } else {
                    vm.loader.hide();
                }
            });

            console.log("posting the transaction: \n", this.mainTransaction)
        }
    },
    props: ['transactionBatch'],
    watch: {
        transactionBatch: function(transactionBatch) {
            this.mainTransaction.transactionBatchTmp = transactionBatch;
        },
        transactionCurrency(newValue, oldValue) {
            if(newValue !== oldValue) {
                const filteredAccounts = this.$store.getters.glBankAccounts.filter((value) => value.bankAccount.currencyId === newValue)

                let options = filteredAccounts.map((option) => {
                    let obj = {id: option.id, text: option.accountNumber + " : " + option.accountName};
                    return {id: JSON.stringify(obj), text: obj.text}
                });

                this.debitAccountOptions = {
                    placeholder: 'Debit Account...',
                    allowClear: true,
                    data: options
                };

                this.creditAccountOptions = {
                    placeholder: 'Debit Account...',
                    allowClear: true,
                    data: options
                };

                if(options.length === 1) {
                    this.$nextTick(() => {
                        this.selectedDebitAccount = options[0].id;
                    })
                }
            }
        }
    },
    computed: {
        disableAmountField: disableAmountField,
        transactionCurrency() {
            return this.mainTransaction.transactionCurrency;
        }
    },
    created: function() {
        this.initForm();
        this.$store.dispatch('getGlBankAccounts')
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: TransHeaderListComp},
    {path: "/edit/:id", name: 'editBatch', component: TransBatchEdit},
    {path: "/create", name: "createTransHeader", component: TransHeaderDemo},
    {path: "/transaction/create", name: 'createTrans', component: TransHeaderDemo},
    {path: "/transaction/delete/:id", name: 'deleteTrans', component: TransHeaderDemo}
];

// the application router.
const router = new VueRouter({
    routes: routes
});

/**
 * The main application component
 */
Vue.component("trans-header-app", {
    template: '#trans-header-app',
    methods: {
        createNewTransaction: function() {
            const url = config.API_URL + "/transactions/create"
            const vm = this;

            this.$axios.get(url).then(function(response){

                var transaction = response.data;

                // we want to push the router to editing this new transaction
                vm.$router.push({name: 'editBatch', params: {id: transaction.id}});

            }).catch(function(error){
                console.error(error)
            })
        }
    }
});

jQuery(document).ready(function() {

    const vm = new Vue({
        el: '#app',
        router: router,
        store: store,
        template: "<trans-header-app></trans-header-app>",
        created: function() {
            // retrieve the transactionTypes
            this.$axios.get(config.API_URL + "/transaction-type").then((response) => {
                this.$store.commit('transactionTypes', response.data);
            });
        }
    });

});
