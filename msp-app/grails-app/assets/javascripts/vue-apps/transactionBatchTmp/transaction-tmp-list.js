/**
 * Created by lebohof on 7/28/2018.
 */
//= require global/plugins/vue/bootstrap-table/bootstrap-table-vue

const TRANS_API = config.API_URL+"/transaction-tmp";
const transTmpService = {
    listBatchTransactions(headerId) {
        return axios.get(TRANS_API+"/list-by-header/"+headerId);
    }
};

const template = `<!-- BEGIN PORTLET-->
<section>
    <div v-if="false" id="trans-list-comp-toolbar">
            <button type="button" class="btn btn btn-danger"><i class="fa fa-trash"></i> Delete Selected</button>
    </div>
    <bootstrap-table :data="transactions" :columns="columns" :options="options"></bootstrap-table>
</section>    
    <!-- END PORTLET-->`

Vue.component('trans-list-comp', {
    template: template,
    components: {
        BootstrapTable: BootstrapTable
    },
    watch: {
        transactionBatch(transactionBatch) {
            this.fetchTransactions();
        }
    },
    props: {
        transactionBatch: {
            required: true
        },
        showDelete: {
            type: Boolean,
            default: false
        }
    },
    data: function () {
        return {
            transactions: [],
            options: {
                onClickRow: this.handleClickRow ,
                onClickCell: this.handleClickCell ,
                search: true,
                toolbar: "#trans-list-comp-toolbar",
                showSearchClearButton: true,
                searchOnEnterKey: true,
                showButtonIcons: true,
                showSearchButton: true,
                showFooter: true,
                height: '450',
                footer: true,
                classes: "table table-striped table-hover table-condensed"
            },
            columns: [{
                title: "Id",
                field: "id",
                checkbox: true,
                visible: false
            },{
                title: "Credit Acc",
                field: "creditAccount",
                formatter: (e) => shortify(e, 64)
            },{
                title: "Sub Acc",
                field: "subAccountName"
            },{
                title: "Debit Acc",
                field: "debitAccount",
                formatter: (e) => shortify(e, 64)
            },{
                title: "Txn Ref",
                field: "transactionReference",
                footerFormatter: () => "Trans Count: "
            },{
                title: "Txn Date",
                field: "transactionDate",
                formatter: (e) => moment(e).format("YYYY-MMM-DD"),
                footerFormatter: (row) => {
                    return this.transactionBatch.transactionsCount
                }
            },{
                title: "Txn Details",
                field: "description",
                formatter: (e) => shortify(e, 64),
                footerFormatter: () => "Control Total: "
            },{
                title: "Amount ($)",
                field: "amount",
                formatter: (value) => formatNumber(value),
                footerFormatter: () => {
                    return formatNumber(this.transactionsTotal)
                }
            },{
                title: '<span class="display-block"><i class="fa fa-trash-o"></i></span>',
                field: "action",
                clickToSelect: false,
                visible: this.showDelete,
                formatter: (e, v, t) => {
                    return '<a type="button" class="btn btn-xs"><i class="fa fa-times font-red-haze"></i></a>';
                },
            }]
        }
    },
    computed: {
        transactionsTotal() {
            return this.transactionBatch.transactionsTotal;
        }
    },
    methods: {
        handleClickRow(row, $el, field) {
            if(field === 'action')
                return;

            this.$emit("on-click-transaction", row.id);
        },
        handleClickCell(field, value, row, $el) {
            if(field === 'action') {
                this.$emit("delete-transaction", row.id);
            }
        },
        fetchTransactions() {
            if(this.transactionBatch) {
                console.log('what: ', this.transactionBatch);

                transTmpService.listBatchTransactions(this.transactionBatch.id).then((resp) => {
                    this.transactions = resp.data;

                    this.transactions = this.transactions.map((txn) => {
                        txn.creditAccount = txn.creditAccount + " : " + txn.creditAccountName;
                        txn.debitAccount = txn.debitAccount + " : " + txn.debitAccountName;

                        return txn;
                    })
                });
            }
        }
    },
    created() {
        this.fetchTransactions();
    }
});
