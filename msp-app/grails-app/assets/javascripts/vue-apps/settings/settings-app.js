/**
 * Created by lebohof on 01/09/2020.
 */

//= require global/plugins/lodash.min

const settingsService = {
    list(params) {
        return axios.get(config.API_URL+'/settings/list', {params: params});
    }
};

Vue.component('page-bar', {
    template: "#page-bar",
    data() {
        return {
            subTitle: null
        }
    }
});

const SettingsApp = Vue.component('settings-app', {
    template: '#settings-app'
});

const SettingsList = Vue.component("settings-list", {
    template: "#settings-list",
    data() {
        return {
            settings: [],
            queryParams: {}
        }
    },
    created() {
        this.fetchList();
    },
    methods: {
        search: function() {
            this._search(this, this.params)
        },
        _search: _.debounce((vm, params) => {
            vm.fetchList()
        }, 350),
        refresh() {
            this.queryParams = {
                query: null
            };
            this.fetchList();
        },
        fetchList() {
            let loader = overlayLoader.fullPageLoader();
            settingsService.list(this.queryParams).then((resp) => {
                this.settings = resp.data;
            }).finally(() => {
                loader.hide();
            })
        }
    }
});

const SettingsPage = Vue.component("settings-page", {
    template: "#settings-page",
    data() {
        return { }
    },
    created() { },
    methods: { }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: SettingsList},
    {path: "/config", name: 'settingsPage', component: SettingsPage}
];

const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueRouter);

var vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(SettingsApp)
});
