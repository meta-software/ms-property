/**
 * Created by lebohof on 12/28/2019.
 */
const API_URL = config.API_URL + "/flexcube-transaction";

const flexcubeTransactionService = {
    listPending: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined)
            queryParams.params = args;

        return axios.get(API_URL+'/pending', queryParams);
    },

    list: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined)
            queryParams.params = args;

        return axios.get(API_URL, queryParams);
    },

    get: function(id) {
        return axios.get(API_URL+"/show/"+id)
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const TransactionApp = Vue.component("app", {
    template: "#main-app"
});

const TransactionSearch = Vue.component('transaction-search', {
    template: '#transaction-search',
    data() {
        return {
            query: {
                txref: '',
                cracc: '',
                dbacc: ''
            },
            subAccountOptions: {
                placeholder: 'Sub Account...'
            }
        }
    },
    methods: {
        requestSearch() {
            this.$emit('search-request', this.query);
            this.$router.replace({path: "/", query: this.query});
        }
    },
    mounted() {
        this.$axios.get(config.API_URL + "/sub-account/list-options").then((resp) => {
            let subAccounts = resp.data.map((acc) => {
                return {
                    id: acc.accountNumber,
                    text: `${acc.accountNumber} : ${acc.accountName}`
                }
            });

            subAccounts.unshift({id: '-1', text: 'All Accounts..'});
            this.subAccountOptions = {
                placeholder: 'Sub Account...',
                data: subAccounts
            }
        });
    }
});

const TransactionList = Vue.component("transaction-list", {
    template: '#transaction-list',
    data() {
        return {
            transactions: [],
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },

    created() {
        //this.getList({})
    },

    methods: {
        getList(params) {
            this.loader.isLoading = true;

            flexcubeTransactionService.list(params).then((response) => {
                this.transactions = response.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        searchTransactions(params) {
            this.getList(params);
        }
    }
});

const TransactionShow = Vue.component("transaction-show", {
    template: "#transaction-show",
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            transaction: {
                id: "",
                subAccount: {},
                maker: {},
                checker: {},
                billingCycle: {}
            }
        }
    },

    created() {
        //this.getEntity();
    },

    methods: {
        getEntity() {
            this.loader.isLoading = true

            flexcubeTransactionService.get(this.id).then((response) => {
                this.transaction = response.data
            }).finally(() => {
                this.loader.isLoading = false
            });
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: TransactionList
    },
    {
        name: "show",
        path: "/:id",
        component: TransactionShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

jQuery(document).ready(function() {

    /**
     * The main application component
     */
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(TransactionApp)
    });

});

