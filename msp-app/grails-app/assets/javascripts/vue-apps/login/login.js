/**
 * Created by lebohof on 11/18/2018.
 */
//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

new Vue({
    el: "#login-app",
    data: {
        username: null,
        password: null,
        login: {
            isLoading: false
        }
    },
    methods: {
        authenticate: function(event) {
            const loginUrl = config.API_URL + "/auth/login";
            const loginData = {
                username: this.username,
                password: this.password
            };

            this.login.isLoading = true;

            this.$axios.post(loginUrl, loginData).then((resp) => {
                console.log(resp);
                alert('successfull logged in')
            }).catch((err) => {
                iziToast.error({
                    message: "Login attempt failed.",
                    title: "Login Fail",
                    position: 'topCenter'});

                console.error(err)
            });
        }
    },
    created: function(){
    }
});
