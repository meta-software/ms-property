/**
 * Created by lebohof on 12/29/2018.
 */

//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle'],
    methods: {
        createCycle() {
            this.$router.push("/create")
        }
    }
});

/**
 * The Billing Cycle service
 */
const billingCycleService = {
    API_URL: config.API_URL+'/billing-cycle',

    list: function(queryParams) {
        return axios.get(this.API_URL, {params: queryParams});
    },
    get(id) {
        return axios.get(this.API_URL+`/show/${id}`);
    },
    save(billingCycle) {
        return axios.post(this.API_URL+'/save', billingCycle);
    },
    open(id) {
        return axios.put(this.API_URL+'/open/'+id);
    },
    close(id) {
        return axios.put(this.API_URL+'/close/'+id);
    },
    activate(id) {
        return axios.put(this.API_URL+'/activate/'+id);
    }
};

/**
 * The main application component
 */
const BillingCycleApp = Vue.component('bc-app', {
    template: "#bc-app"
});

const BillingCycleList = Vue.component('bc-list',{
    template: "#bc-list",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            billingCycles: [],
            params: {}
        }
    },
    methods: {
        search() {
            this.fetchCycles(this.params);
        },
        reset() {
            this.params = {
                q: '',
                open: ''
            }
            this.fetchCycles(this.params);
        },
        fetchCycles() {
            this.loader.isLoading = true;

            billingCycleService.list(this.params).then((resp) => {
                this.billingCycles = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.fetchCycles();
    }
});

const BillingCycleShow = Vue.component('bc-show', {
    template: '#bc-show',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            billingCycle: {}
        }
    },
    computed: {
        id: function() {
            return this.$route.params.id;
        }
    },
    methods: {
        get() {
            this.loader.isLoading = true;
            billingCycleService.get(this.id).then((resp) => {
                this.billingCycle = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        close() {
            this.loader.isLoading = true;
            billingCycleService.close(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Billing Cycle closed successfully...'});
                this.get();
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        activate() {
            this.loader.isLoading = true;
            billingCycleService.activate(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Billing Cycle activated successfully...'});
                this.get()
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        open() {
            this.loader.isLoading = true;
            billingCycleService.open(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Billing Cycle opened successfully...'});
                this.get()
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        notImplemented() {
            iziToast.error({
                overlay: true,
                layout: 2,
                title: 'Alert !!!<br />',
                baloon: true,
                timeout: 0,
                message: 'This feature is not activated.<br />Please contact administrator for more details.',
                position: 'center'})
        }
    },
    created() {
        this.get();
    }
});

const BillingCycleCreate = Vue.component('bc-create', {
    template: "#bc-create",
    data: function() {
        return {
            billingCycle: {
                name: null,
                startDate: null,
                endDate: null,
                description: null,
                active: false
            }
        }
    },
    methods: {
        cancel() {
            //are you sure you want to cancel
            this.$router.push("/");
        },
        save() {
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            billingCycleService.save(this.billingCycle).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Billing Cycle created successfully...'});
                this.$router.push("/");
            }).finally(() => {
                loader.hide();
            });
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: BillingCycleList
    },
    {
        name: "show",
        path: "/show/:id",
        component: BillingCycleShow
    },
    {
        name: "create",
        path: "/create",
        component: BillingCycleCreate
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(BillingCycleApp)
});