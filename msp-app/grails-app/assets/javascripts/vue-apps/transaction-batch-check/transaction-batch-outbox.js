/**
 * Created by lebohof on 2/10/2020.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-options
//= require vue-apps/transactionBatchTmp/transaction-tmp-list

//Setup the plugins.
Vue.use(VeeValidate, { events: 'change' });

const API_URL = config.API_URL + "/transaction-batch-outbox"

const TransactionBatchOutboxList = Vue.component('trans-batch-outbox-list', {
    template: '#trans-batch-outbox-list',
    data: function() {
        return {
            transactionBatchChecks: [],
            params: {}
        }
    },
    methods: {
        reset() {
            this.params = {}
            this.fetchRequests();
        },
        search() {
            this.fetchRequests()
        },
        fetchRequests: function() {

            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            this.$axios.get(API_URL, {params: this.params}).then((response) => {
                console.log('success: ', response.data);
                this.transactionBatchChecks = response.data;
            }).catch((error) => {
                alert('error');
            }).finally(() => {
                loader.hide();
            })

        }
    },
    created: function() {
        this.fetchRequests();
    }
});

const TransactionBatchOutboxShow = Vue.component('trans-batch-outbox-show', {
    template: '#trans-batch-outbox-show',
    data: function() {
        return {
            transactionBatchCheck: {
                entity: {},
                transactionType: {},
                transactionBatchTmp: {},
                maker: {}
            }
        }
    },
    computed: {
        transactionHeader() {
            return this.transactionBatchCheck.transactionBatchTmp;
        },
        id() {
            return this.$route.params.id;
        }
    },
    methods: {
        onCancelled: function() {
            this.fetchRecord(this.id);
        },
        fetchRecord: function(id) {

            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            const url = API_URL + '/show/' + id;
            this.$axios.get(url).then((response) => {
                this.transactionBatchCheck = response.data;
            }).catch((error) => {
                iziToast.error({title: "Error...", message: "Error occurred while fetching Transactions from inbox"});
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created: function() {
        // read the passed id from the route
        const id = this.$route.params.id;
        this.fetchRecord(id);
    }
});

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const MainApp = Vue.component('app', {
    template: '#main-app',
    created: function() {
        console.log("started the main application")
    }
});

// Prepare the application routes here.
// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: TransactionBatchOutboxList},
    {path: "/show/:id", name: 'show', component: TransactionBatchOutboxShow}
];

// the application router.
const router = new VueRouter({
    routes: routes
});
// prepare the application routes here

const vm = new Vue({
    el: "#app",
    router: router,
    render: (h) => h(MainApp),
    created: function() {
        console.log('created the main application');
    }
});
