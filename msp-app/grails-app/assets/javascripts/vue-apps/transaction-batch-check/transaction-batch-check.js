/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker
//= require vue-apps/maker-options

//= require vue-apps/transactionBatchTmp/transaction-tmp-list

//Setup the plugins.
Vue.use(VeeValidate, { events: 'change' });

const API_URL = config.API_URL + "/transaction-batch-check"

const TransactionBatchCheckList = Vue.component('trans-batch-check-list', {
    template: '#trans-batch-check-list',
    data: function() {
        return {
            transactionBatchChecks : []
        }
    },
    methods: {
        list: function() {
            this.$axios.get(API_URL).then((response) => {
                console.log('success: ', response.data);
                this.transactionBatchChecks = response.data;
            }).catch((error) => {
                alert('error');
            })
        }
    },
    created: function() {
        this.list();
    }
});

const TransactionBatchCheckOutbox = Vue.component('trans-batch-check-outbox', {
    template: '#trans-batch-check-outbox',
    data: function() {
        return {
            transactionBatchChecks : []
        }
    },
    methods: {
        list: function() {
            this.$axios.get(API_URL+"/outbox", {'Content-Type': 'application/json'}).then((response) => {
                console.log('success: ', response.data);
                this.transactionBatchChecks = response.data;
            }).catch((error) => {
                alert('error');
            })
        }
    },
    created: function() {
        this.list();
    }
});

const TransactionBatchCheckShow = Vue.component('trans-batch-check-show', {
    template: '#trans-batch-check-show',
    data: function() {
        return {
            transactionBatchCheck: {
                entity: {},
                transactionType: {},
                transactionBatchTmp: {},
                maker: {}
            }
        }
    },
    methods: {
        onPosted: function() {
            this.$router.replace("/");
        },
        get: function(id) {
            const url = API_URL + '/show/' + id;
            this.$axios.get(url).then((response) => {
                this.transactionBatchCheck = response.data;
            }).catch((error) => {
                console.error(error)
            });
        }
    },
    created: function() {
        // read the passed id from the route
        const id = this.$route.params.id;
        this.get(id);
    },

    computed: {
        id: function() {
            return this.$route.params.id;
        },
        transactionHeader() {
            return this.transactionBatchCheck.transactionBatchTmp;
        }
    }
});

const TransactionBatchCheckOutboxItem = Vue.component('trans-batch-check-outbox-item', {
    template: '#trans-batch-check-outbox-item',
    data: function() {
        return {
            transactionBatchCheck: {
                entity: {},
                transactionType: {},
                transactionBatchTmp: {},
                maker: {}
            }
        }
    },
    methods: {
        get: function(id) {
            const url = API_URL + '/show/' + id;
            this.$axios.get(url).then((response) => {
                this.transactionBatchCheck = response.data;
            }).catch((error) => {
                iziToast.error({message: "Error occurred while fetching Transactions from inbox"});
            });
        }
    },
    created: function() {
        // read the passed id from the route
        const id = this.$route.params.id;
        this.get(id);
    }
});

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const MainApp = Vue.component('app', {
    template: '#main-app',
    created: function() {
        console.log("started the main application")
    }
});

// Prepare the application routes here.
// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: TransactionBatchCheckList},
    {path: "/outbox", name: 'outbox', component: TransactionBatchCheckOutbox},
    {path: "/show/:id", name: 'show', component: TransactionBatchCheckShow},
    {path: "/outbox/item/:id", name: 'outbox-item', component: TransactionBatchCheckOutboxItem}
];

// the application router.
const router = new VueRouter({
    routes: routes
});
// prepare the application routes here

jQuery(() => {
    const vm = new Vue({
        el: "#app",
        router: router,
        template: "<app></app>",
        created: function() {
            console.log('created the main application')
        }
    });

});
