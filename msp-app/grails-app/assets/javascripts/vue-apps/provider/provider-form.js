/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/components/country-select
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            accountType: 'Account Type',
            accountName: 'Account Name',
            phoneNumber: 'Phone Number',
            cellNumber: 'Cell Number',
            postalAddress: 'Postal Address',
            businessAddress: 'Business Address',
            physicalAddress: 'Physical Address',
            emailAddress: 'Email Address',
            vatNumber: 'VAT Number',
            bankAccountName: 'Account Name',
            bank: 'Bank',
            accountNumber: 'Account Number',
            branchCode: 'Branch Code',
            branch: 'Branch'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});
/**
 * The main application component
 */

var vm = new Vue({
    el: '#provider-form-app',
    data: function () {
        return {
            accountTypeOptions: {
                placeholder: 'Account Type...'
            },
            test: '',
            provider: {
                businessAddress: {
                    street:null,
                    suburb: null,
                    city: null,
                    country: null
                },
                postalAddress: {
                    street:null,
                    suburb: null,
                    city: null,
                    country: null
                },
                physicalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                bankAccount: {
                    bank: {
                        id: null
                    }
                },
                accountType: {}
            }

        }
    },
    methods: {
        initForm: function () {
            this.provider = {
                bankAccount: {
                    bank: {
                        id: null
                    }
                },
                businessAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                postalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                physicalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                accountType: {
                    id: null
                }
            }
        },
        onCancel: function() {
            this.$confirm({
                title: 'Cancel ?',
                content: 'Are you sure you want to cancel?'
            }).then(() =>{
                this.$loading.show({})
                window.location.assign(config.API_URL + "/provider");
            }).catch(() => {})
        },
        onSaveProvider: function() {
            const url = config.API_URL + "/provider/save";

            this.$validator.validate().then((result) => {
                if(result) {
                    // show the loader
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });

                    this.$axios.post(url, this.provider).then((response) => {
                        let provider = response.data;

                        this.loader.hide();

                        this.$alert({
                            content: "Supplier created successfully.",
                            title: 'Success'
                        }, () => {
                            this.loader = this.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(provider['_links']['self']['href']);
                        })

                    }).catch((error) => {
                        console.log("Error occured while trying to save provider entity", error);
                        this.loader.hide();

                        this.$alert({
                            content: 'Failed to save the provider entity',
                            title: 'Error !'
                        })
                    });

                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });
        }
    },

    created: function() {
        //initialise the form
        this.initForm();
    },

    mounted: function() {
        const vm = this;

        $(this.$refs.accountType).on('change', function() {
            vm.provider.accountType.id = $(this).val();
        });

        this.$axios.get(config.API_URL + '/account-type/list-options').then((response) => {
            let accountTypeList = response.data.map((object) => {
                return {
                    id: object.id,
                    text: object.name
                }
            });

            this.accountTypeOptions = {
                data: accountTypeList,
                placeholder: this.accountTypeOptions.placeholder
            }

        }).then((error) => {

        })
    }
});
