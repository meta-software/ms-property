/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            accountType: 'Account Type',
            accountName: 'Account Name',
            phoneNumber: 'Phone Number',
            postalAddress: 'Postal Address',
            businessAddress: 'Business Address',
            physicalAddress: 'Physical Address',
            emailAddress: 'Email Address',
            vatNumber: 'VAT Number',
            bankAccountName: 'Account Name',
            bank: 'Bank',
            accountNumber: 'Account Number',
            branchCode: 'Branch Code',
            branch: 'Branch'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});
/**
 * The main application component
 */

var vm = new Vue({
    el: '#provider-form-app',
    data: function () {
        return {
            accountTypeOptions: {
                placeholder: 'Account Type...'
            },
            provider: {
                bankAccount: {
                    bank: {
                        id: null
                    }
                },
                accountType: {}
            }
        }
    },
    methods: {
        initForm: function () {
            const url = config.API_URL + "/provider/edit/" + id;

            this.$axios.get(url).then((response) => {
                this.provider = response.data;

                // this is a fix for javascript
                if(this.provider.bankAccount === null) {
                    this.provider.bankAccount = {
                        bank: {}
                    }
                }

            }).catch((error) => {
                this.$notify({
                    type: 'danger',
                    content: 'Error occured while initiating object'
                });
            });
        },
        onCancel: function() {
            this.$confirm({
                title: 'Cancel ?',
                content: 'Are you sure you want to cancel?'
            }).then(() =>{
                this.$loading.show({})
                window.location.assign(config.API_URL + "/provider/show/"+this.provider.id);
            }).catch(() => {})
        },
        onSaveSupplier() {
            const url = config.API_URL + "/provider/update/" + this.provider.id;

            this.$validator.validate().then((result) => {
                if(result) {
                    // show the loader
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });

                    this.$axios.put(url, this.provider).then((response) => {
                        let provider = response.data;

                        this.loader.hide();

                        this.$alert({
                            content: "Supplier updated successfully.",
                            title: 'Success'
                        }, () => {
                            this.loader = this.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(provider['_links']['self']['href']);
                        })

                    }).catch((error) => {
                        console.log("Error occured while trying to save provider entity", error);
                        this.loader.hide();

                        this.$alert({
                            content: 'Failed to save the provider entity',
                            title: 'Error !'
                        })
                    });

                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });

        }
    },

    created: function() {
        //initialise the form
        this.initForm();
    },

    mounted: function() {
        const vm = this;

        $(this.$refs.accountType).on('change', function() {
            vm.provider.accountType = $(this).val();
        });
    }
});
