/**
 * Created by lebohof on 01/15/2019.
 */
//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

const ProviderFormComp = Vue.component("provider-form", {
    template: "#provider-form",
    data: function () {
        return {
            loader: null,
            openModal: false,
            providerForm: {
                accountType: null,
                phoneNumber: '',
                accountName: '',
                physicalAddress: '',
                postalAddress: '',
                businessAddress: '',
                faxNumber: '',
                emailAddress: '',
                vatNumber: ''
            },
            accountTypeOptions: []
        }
    },
    methods: {
        save: function() {

            const vm = this;

            vm.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
            });

            this.$validator.validate().then(function (result) {

                if(result){
                    vm.$axios.post(config.API_URL + '/provider/save', vm.providerForm).then(function (response) {
                        const provider = response.data;
                        vm.loader.hide();

                        vm.$alert({
                            content: "Provider " + provider.accountNumber + " created successfully.",
                            title: 'Success'
                        }, function () {
                            vm.openModal = false; //close the modal

                            vm.loader = vm.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(provider['_links']['self']['href']);
                        })
                    }).catch(function (error) {
                        vm.loader.hide();
                        vm.$notify({
                            type: 'danger',
                            title: 'Error!!!',
                            content: 'failed to save the provider record'
                        });
                    })
                } else {
                    vm.loader.hide();
                }

            })
        },
        modalCallback: function(msg) { }
    },

    created: function() {
    },
    mounted: function() {
        const vm = this;
        $("#accountTypeItem").change(function () {
            vm.providerForm.accountType = $(this).val();
        });
    }
});

const ProviderMainApp = new Vue({

    el: '#main-app',
    data: {
        providerForm: false
    },
    methods: {

    }
});
