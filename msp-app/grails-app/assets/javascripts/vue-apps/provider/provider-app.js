
/**
 * Created by lebohof on 8/22/2018.
 */

// prepare the Provider Services
function ProviderService () {

    this.get = function(id, success, error) {
        axios.get(config.API_URL + "/provider/show/"+id).then((response) => {
            success(response);
         }).catch((response) => {
            console.log('error occured: ', response);
            error(response);
        });
    };

    this.save = function(provider, successCallback, errorCallback) {
        if(provider.id) {
            axios.put(config.API_URL + "/provider/update/"+id).then((response) => {
                successCallback(response);
            }).catch((response) => {
                console.log('error request response : ', response);
                errorCallback(response);
            })
        } else {
        }
    }
};
const providerService = new ProviderService();

var ProviderPlaceholderComponent = Vue.component("provider-placeholder", {
    data: function() {
        return {

        }
    },
    created: function() {
        console.log("created the ProviderPlaceholderComponent successfully.");
    }
});

//The provider list component
var ProviderListComp = Vue.component('provider-list-comp', {
    template: '#provider-list-comp',
    data: function () {
        return {
            providerList: []
        }
    },
    created: function () {
        this.$http.get(config.API_URL + "/provider/index")
            .then(function(response) {

                if (response.data) {
                    this.providerList = response.data;
                }

            }, function(response){
                console.error("error response returned: " + response)
            });
    }
});

// The Provider edit component
var ProviderEditComp = Vue.component('provider-edit-comp', {
    template: "#provider-edit-comp",
    data: function() {
        return {
            provider: {}
        }
    },
    methods: {
        initComponent: function() {
            const id = this.$route.params.id;

            //retrieve the
            this.$http.get(config.API_URL + "/provider/edit/"+id).then(function(response){
                console.infO("response: ", response)
                this.provider = response.data;
            },function(error){
                console.error("error occured: ", error);
            });
        }
    },
    created: function() {
        this.initComponent();
    }
});

// The Provider show component
var ProviderShowComp = Vue.component('provider-show-comp', {
    template: "#provider-show-comp",
    data: function() {
       return {
           provider: {
               accountType: {},
               accountCategory: {}
           }
       }
    },
    methods: {
        initComponent: function() {
            const id = this.$route.params.id;
            var vm = this;

            providerService.get(id, function(response){
                vm.provider = response.data;
            }, function(error){
                console.error('error: ', error);
            });

        }
    },
    created: function() {
        this.initComponent();
    }

});

/**
 * The main application component
 */
var ProviderApp = Vue.component('provider-app', {
    template: "#provider-app",
    data: function() {
        return {
            title: "Provider App"
        }
    },
    created: function() {
        console.log("Welcome to the Provider Vue App!!!")
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'index', component: ProviderListComp},
    {path: "/create",   name: 'create', component: ProviderPlaceholderComponent},
    {path: "/edit/:id", name: 'edit', component: ProviderEditComp},
    {path: "/show/:id", name: 'show', component: ProviderShowComp}
];

// the application router.
const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueResource);
Vue.use(VueRouter);

var vm = new Vue({
    el: '#app',
    router: router,
    template: "<provider-app></provider-app>"
});

