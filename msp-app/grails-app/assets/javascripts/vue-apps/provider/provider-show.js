
/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate.min
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

// property form component
const BankAccountFormComp = Vue.component("bank-account-form", {
    template: "#bank-account-form",
    data: function () {
        return {
            loader: null,
            openModal: false,
            loadingSubmit: false,
            bankAccountForm: {
                account: {
                    id: providerId,
                },
                accountName: null,
                accountNumber: null,
                branchName: null,
                branchCode: null
            },
            options: {
                placeholder: 'Bank...',
                ajax: {
                    url: config.API_URL + '/bank',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        'accepts': 'application/json'
                    },
                    delay: 300,
                    processResults: function (data) {

                        let results = {results: []}

                        for(var i = 0; i < data.length; i++  ) {
                            var obj = data[i];
                            var option = {id: obj.id, text: obj.bankName};
                            results['results'].push(option);
                        }

                        return results;
                    }
                }
            }

        }
    },
    methods: {
        saveBankAccount: function() {

            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });

            const vm = this;

            this.$validator.validate().then( (result) => {

                if(result){
                    vm.$axios.post(config.API_URL + '/bank-account/save', vm.bankAccountForm).then( (response) => {
                        vm.loader.hide();

                        vm.bankAccount = response.data;

                        vm.$alert({
                            content: "Bank "+vm.bankAccountForm.bankName + " created successfully."
                        }, function () {
                            vm.openModal = false; //close the modal
                            vm.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(response.data['_links']['self']['href']);
                        })
                    }).catch(function (error) {
                        vm.loader.hide();

                        vm.$notify({
                            type: 'danger',
                            title: 'Error',
                            content: "error while saving the Bank Account"
                        });
                    })
                } else {
                    vm.$notify({
                        type: 'danger',
                        title: 'Validation Error',
                        content: 'invalid form data submitted.'
                    });
                    vm.loader.hide();
                }
            })
        },
        callback: function (msg) {
            //this.$notify(`Modal dismissed with msg '${msg}'.`);
        }
    }
});

const providerShowApp = new Vue({
    el: '#provider-show-app',
    data: {
        loader: null,
        providerId: providerId,
        provider: {
            id: providerId
        },
    },
    methods: {
        terminateProvider: function() {
            const vm = this;

            this.provider.terminated = true;

            this.$axios.put(config.API_URL + "/provider/terminate/"+this.providerId, this.provider).then(function(response){
                console.log("successfully terminated  provider id " + vm.providerId);
                location.reload(true);
            }).catch(function(error){
                console.log(error.data);
            });
        },
    },
    created: function() {
        this.isLoading = true;
    }
});
