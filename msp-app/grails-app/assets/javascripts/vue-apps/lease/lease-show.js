/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker
//= require vue-apps/components/utils

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

const LeaseTerminateComponent = Vue.component('lease-terminate', {
    template: '#lease-terminate',
    props: ['leaseId', 'lease'],
    data: function() {
        return {
            loader: null,
            openModal: false,
            leaseStatusRequest :{
                lease: {
                    id: leaseId
                },
                actionDate: null,
                actionReason: null
            }
        }
    },
    mounted: function(){
        const vm = this;
        //@todo: prepare the datepicker component/directive
        $("[name=actionDate]").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        }).on('change', function(e){
            vm.leaseStatusRequest.actionDate = $(this).val();
        });

    },
    watch: {
        lease: function(lease) {
            this.requestStatusRequest.lease = lease;
        }
    },
    methods: {
        startLoader: function() {
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });
        },
        stopLoader: function() {
            this.loader.hide();
        },
        openModalForm: function() {
            this.openModal = true;
        },
        callback: function(msg){

            const vm = this;
            if(msg === 'ok') {

                vm.startLoader();

                this.$axios.post(config.API_URL + '/lease-terminate-request/create-request', this.leaseStatusRequest).then(function(resp){

                    iziToast.success({
                        message: 'Termination request sent successfully',
                        title: "Success"
                    });

                }).catch( (error) => {
                    console.log(error);
                }).finally(() => {
                    vm.stopLoader();
                });
            }
        }
    }
});

const LeaseEventHistory = Vue.component('lease-event-history', {
    template: '#lease-event-history',
    props: ['leaseId'],
    created() {
        console.log("#lease-event-history component started")
    }
});

const LeaseRequests = Vue.component('lease-requests', {
    template: '#lease-requests',
    props: ['leaseId'],
    data() {
        return {
            leaseRequests: []
        }
    },
    created() {
        axios.get(`${config.API_URL}/lease/terminate-requests/${this.leaseId}`).then((resp) => {
            this.leaseRequests = resp.data;
        })
    }
});

const LeaseRenewalComponent = Vue.component('lease-renewal', {
    template: '#lease-renewal',
    props: ['leaseId', 'lease'],
    data: function() {
        return {
            loader: null,
            openModal: false,
            leaseStatusRequest :{
                lease: {
                    id: leaseId
                },
                actionDate: null,
                leaseExpireDate: null,
                actionReason: null
            }
        }
    },
    mounted: function(){
        const vm = this;
        //@todo: prepare the datepicker component/directive
        $("[name=leaseExpireDate]").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        }).on('change', function(e){
            vm.leaseStatusRequest.leaseExpireDate = $(this).val();
        });

        $("[name=actionDate]").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        }).on('change', function(e){
            vm.leaseStatusRequest.actionDate = $(this).val();
        });

    },
    watch: {
        lease: function(lease) {
            this.requestStatusRequest.lease = lease;
        }
    },
    methods: {
        startLoader: function() {
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });
        },
        stopLoader: function() {
            this.loader.hide();
        },
        openModalForm: function() {
            this.openModal = true;
        },
        callback: function(msg){

            const vm = this;
            if(msg === 'ok') {

                vm.startLoader();

                this.$axios.post(config.API_URL + '/lease-renewal-request/create-request', this.leaseStatusRequest).then(function(response){
                    iziToast.success({message: "Lease Renewal request sent successfully", title: "Success", timeout: 10000});
                    vm.stopLoader();
                }).catch( (error) => {

                    if(error.status === 422) {
                        for (let i = 0; i < error.data.errors.length; i++) {
                            let errorObject = error.data.errors[i];

                            iziToast.error({
                                message: errorObject.message,
                                title: "Error !"
                            });
                        }
                    }

                    this.stopLoader();
                    iziToast.error({
                        message: 'Lease Renewal request failed !!!',
                        title: 'Error !!'
                    });
                });

            } else {
                iziToast.warn({
                    message: 'Cancelled Lease Renewal request',
                    position: "topCenter"
                });
            }
        }
    }
});

const LeaseItemListComp = Vue.component('lease-item-list', {
    props: ['lease'],
    template: '#lease-item-list',
    data: function() {
        return {
            leaseItems: [],
            loader: null
        }
    },
    methods: {
        openLoader: function() {
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });
        },
        hideLoader: function() {
            this.loader.hide();
        },
        getItems() {
            const vm = this;
            const url = config.API_URL + '/lease-item?leaseId='+vm.lease.id;

            let loader = this.$loading.show({
                container: this.$refs.leaseItemList, //full page overlay
                canCancel: false
            });

            vm.$axios.get(url).then(function(response){
                vm.leaseItems = response.data;
                loader.hide();
            }).catch(function(error){
                vm.$notify({
                    type: 'danger',
                    title: 'Error !!!',
                    content: 'Error occured while fetching lease items'
                });
                loader.hide();
            });
        }
    },
    mounted: function() {
        this.getItems();
    }

});

//lease-show main application.
const vm = new Vue({
    el: '#main-app',
    data: {
        leaseId: leaseId,
        approveLeaseComp: false,
        lease: {
            id: leaseId
        },
        leaseItemForm: null,
        item: null,
        options: {}
    },
    methods: {
        selected: function(){
        },
        initLeaseItemForm() {
            this.leaseItemForm = {
                endDate: '',
                startDate: '',
                subAccount: '',
                lease: {
                    id: leaseId
                }
            }
        },
        leaseItemAdded: function() {
            this.initLeaseItemForm();

            // Now re-run the listItemListComponent
            this.$refs.leaseItemList.getItems();
        },
    },
    created: function() {
        this.initLeaseItemForm()
    }
});
