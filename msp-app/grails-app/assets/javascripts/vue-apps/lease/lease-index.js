/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

const leaseMainApp = new Vue({
    el: '#main-app',
    data: {
        propertyForm: false
    },
    methods: {

    },
    created: function() {

    }
});
