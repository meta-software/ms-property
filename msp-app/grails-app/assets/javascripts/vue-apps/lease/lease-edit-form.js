/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            leaseStartDate: 'Start Date',
            leaseExpiryDate: 'End Date',
            tenant: 'Tenant',
            rentalUnit: 'Rental Unit',
            leaseType: 'Lease Type'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

Vue.component('lease-item-form', {
    props: {
        leaseItems: Array
    },
    template: "#lease-item-form",
    data: () => {
        return {
            leaseItem: {},
            subAccountOptions: []
        }
    },
    methods: {
        clearFormData: function() {
            this.leaseItem = {
                subAccount: {},
                startDate: null,
                endDate: null,
                amount: null
            }
        },
        removeLeaseItem(index){
            this.$emit('remove-item', index);
        },
        addLeaseItem : function (){
            //@todo: perform a bit of validation
            this.$validator.validate().then((result) => {
                if(result) {
                    let leaseItem = Object.assign({}, this.leaseItem);

                    this.clearFormData();
                    //@emit: the event and the object which has just been added.
                    this.$emit('add-item', leaseItem);
                }
            });

        }
    },
    mounted: function() {
        $(".datepicker").datepicker({autoclose: true, format: 'yyyy-mm-dd'}).on('changeDate', () => {
            vm.mainTransaction.transactionDate = $(this).val()
        })
    },
    created: function() {
        this.clearFormData();

        //subAccountOptions setup
        this.$axios.get(config.API_URL+"/sub-account/list-options").then( (response) => {

            let data = response.data.map((subAccount) => {
                return {
                    id: subAccount,
                    text: `${subAccount.accountNumber} : ${subAccount.accountName}`,
                    accountName: subAccount.accountName
                }
            });

            data.unshift({id: 0, text: 'Select Account...'});
            this.subAccountOptions = data;
        });
    }
})
;

/**
 * The main application component
 */
let vm = new Vue({
    el: '#lease-form',
    data: function () {
        return {
            rentalUnitOptions: {
                placeholder: 'Select RentalUnit...'
            },
            propertyOptions: {},
            tenantOptions: {},
            propertyId: "",

            leaseId: leaseId,
            lease: {
                id: leaseId,
                leaseItems: []
            },

        }
    },
    watch: {
        propertyId: function(val) {

            const url = config.API_URL + "/rental-unit";
            const vm = this;

            $(this.$refs.rentalUnitSelect).select2({
                placeholder: 'Rental Unit...',
                ajax: {
                    url: url,
                    dataType: 'json',
                    processResults: function(data) {
                        console.log(data);
                        let results =  data.map((result) => {
                            return {
                                id: result.id,
                                text: result.name
                            }
                        });

                        return {
                            results: results
                        }
                    },
                    data: function(params) {
                        let query = {
                            propertyId: val
                        };

                        return query;
                    }
                }
            }).on('change', function(e) {
                vm.lease.rentalUnit.id = $(this).val();
            });
        }
    },
    methods: {
        onCancel: function() {
            this.$confirm({
                title: 'Cancel ?',
                content: 'Are you sure you want to cancel?'
            }).then(() =>{
                this.loader = this.$loading.show({
                    container: null, //full page overlay
                    canCancel: false,
                });
                window.location.assign(config.API_URL + "/lease");
            }).catch(() => {})
        },
        onSaveLease: function() {
            console.log(this.lease.leaseItems);

            const url = config.API_URL + '/lease/update/'+this.lease.id;

            this.$validator.validate().then((result) => {
                if(result) {
                    // show the loader
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });

                    const postData = {
                        id: this.lease.id,
                        leaseItems: this.lease.leaseItems
                    }

                    this.$axios.put(url, this.lease).then((response) => {
                        let lease = response.data;
                        console.log(response)

                        //this.loader.hide();
                        //window.location.reload(true);
                        window.location.assign(config.API_URL + "/lease/show/"+lease.id);
                    }).catch((error) => {
                        this.loader.hide();

                        this.$alert({content: 'Error occured while updating Lease'})
                    })

                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });

        },
        onAddLeaseItem: function (leaseItem) {
            if(this.lease.leaseItems === undefined) {
                this.lease.leaseItems = []
            }

            this.lease.leaseItems.push(leaseItem)
        },
        onRemoveLeaseItem : function (index) {
            this.lease.leaseItems.splice(index, 1);
        },
        initForm: function () {

            // prepare the lease object (form object)
            //initiate the lease
            this.$axios.get(config.API_URL + "/lease/edit/"+leaseId).then( (response) => {
                this.lease = response.data;
            }).catch( (error) => {
                //@todo: this will cause a catastrophe man.
                iziToast.error({message: 'Error while loading page.'})
            });


            // initiate the property options
            this.$axios.get(config.API_URL + "/property/list-options").then((response) =>{
               let propertyOptions = response.data.map((option) => {
                   return {
                       id: option.id,
                       text: option.name
                   }
               });

               this.propertyOptions = {
                   placeholder: 'Select Property...',
                   data: propertyOptions
               };

            });
        }
    },
    mounted: function() {
        const vm = this;

        $(this.$refs.propertySelect).on('change', function(e) {
            vm.propertyId = $(this).val();
            vm.lease.property.id = vm.propertyId;
        });

        $(this.$refs.rentalUnitSelect).on('change', function(e) {
            vm.lease.rentalUnit.id = $(this).val();
        });

        $(this.$refs.tenantSelect).on('change', function(e) {
            vm.lease.tenant.id = $(this).val();
        })

        $(this.$refs.leaseTypeSelect).on('change', function(e) {
            vm.lease.leaseType.id = $(this).val();
        })
    },
    created: function() {
        //initialise the form
        this.initForm();
    }
});
