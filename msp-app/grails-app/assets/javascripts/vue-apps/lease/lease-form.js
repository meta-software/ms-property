/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/components/rental-unit-select
//= require vue-apps/components/currency-select
//= require vue-apps/components/account-select
//= require vue-apps/components/property-select
//= require vue-apps/components/sub-account-select
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            leaseStartDate: 'Start Date',
            leaseExpiryDate: 'End Date',
            tenant: 'Tenant',
            rentalUnit: 'Rental Unit',
            leaseType: 'Lease Type',
            leaseCurrency: 'Lease Currency',
            invoicingCurrency: 'Invoicing Currency'
        }
    }
};

const leaseService = {
    create: function() {
        return axios.get(config.API_URL+"/lease/create");
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

Vue.component('lease-item-form', {
    props: {
        leaseItems: Array
    },
    template: "#lease-item-form",
    data: () => {
        return {
            leaseItem: {},
            subAccountOptions: []
        }

    },
    methods: {
        clearFormData: function() {
            this.leaseItem = {
                subAccount: {},
                startDate: null,
                endDate: null,
                amount: null
            }
        },
        removeLeaseItem(index){
            this.$emit('remove-item', index);
        },
        addLeaseItem : function (){
            //@todo: perform a bit of validation
            this.$validator.validate().then((result) => {
                if(result) {
                    let leaseItem = Object.assign({}, this.leaseItem);

                    this.clearFormData();
                    //@emit: the event and the object which has just been added.
                    this.$emit('add-item', leaseItem);
                }
            });

        }
    },
    created: function() {
        this.clearFormData();
    }
});

jQuery(document).ready(function() {
    /**
     * The main application component
     */
    new Vue({
        el: '#lease-form-app',
        data: function () {
            return {
                rentalUnitOptions: {
                    placeholder: 'Rental Unit...'
                },
                propertyOptions: {
                    placeholder: 'Property...',
                    allowClear: true,
                    ajax: {
                        url: config.API_URL + '/property/list-options',
                        delay: 300
                    }
                },
                tenantOptions: {
                    placeholder: 'Tenant...',
                    allowClear: true,
                    ajax: {
                        url: config.API_URL + '/account/list',
                        delay: 300,
                        data: function(params) {
                            return {
                                q: params.term,
                                ac: 'tenant'
                            };
                        }
                    }
                },
                propertyId: null,
                lease: {
                    tenant: {id: null},
                    property: {id: null},
                    rentalUnit: {id: null},  //The rental Unit under which is being leased
                    signedDate: null,            // agreement date
                    leaseStartDate: null,        // commencement date
                    leaseExpiryDate: null,       // the lease expiry date
                    leaseType: {id: null},
                    leaseItems: [],
                    invoicingCurrency: {id: null},
                    leaseCurrency: {id: null}
                }
            }
        },
        methods: {
            onCancel: function() {
                this.$confirm({
                    title: 'Cancel ?',
                    content: 'Are you sure you want to cancel ?'
                }).then(() =>{
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });
                    window.location.assign(config.API_URL + "/lease");
                }).catch(() => {})
            },
            onSaveLease: function() {
                const url = config.API_URL + '/lease/save';

                this.$validator.validate().then((result) => {
                    if(result) {
                        // show the loader
                        this.loader = this.$loading.show({
                            container: null, //full page overlay
                            canCancel: false,
                        });

                        this.$axios.post(url, this.lease).then((response) => {
                            let lease = response.data;

                            this.loader.hide();
                            //window.location.reload(true);
                            window.location.assign(config.API_URL + "/lease/show/"+lease.id);
                        }).finally(() => this.loader.hide() );
                    }
                    else {
                        iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                    }
                });

            },
            onAddLeaseItem: function (leaseItem) {
                if(this.lease.leaseItems === undefined) {
                    this.lease.leaseItems = []
                }

                this.lease.leaseItems.push(leaseItem)
            },
            onRemoveLeaseItem : function (index) {
                this.lease.leaseItems.splice(index, 1);
            },
            initForm: function () {
                // prepare the lease object (form object)
                let loader = overlayLoader.fullPageLoader();
                leaseService.create().then((resp) => {
                    this.lease = Object.assign(this.lease, resp.data);

                    this.lease.leaseCurrency = {id: resp.data['leaseCurrency'].id}
                    this.lease.invoicingCurrency = {id: resp.data['invoicingCurrency'].id}
                }).finally(() => {
                    loader.hide();
                });
            }
        },
        mounted: function() {

            // initiate the property options
            this.$axios.get(config.API_URL + "/property/list-options").then((response) =>{
                let propertyOptions = response.data.map((option) => {
                    return {
                        id: option.id,
                        text: option.name
                    }
                });
                this.propertyOptions = {
                    placeholder: 'Select Property...',
                    data: propertyOptions
                };
            });
        },
        created: function() {
            //initialise the form
            this.initForm();
        }
    });

});
