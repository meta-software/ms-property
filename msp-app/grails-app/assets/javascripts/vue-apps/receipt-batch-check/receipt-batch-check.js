/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker
//= require vue-apps/maker-options

//Setup the plugins.
Vue.use(VeeValidate, { events: 'change' });

const API_URL = config.API_URL + "/receipt-check";

const ReceiptBatchCheckList = Vue.component('receipt-check-list', {
    template: '#receipt-check-list',
    data: function() {
        return {
            receiptBatchChecks : []
        }
    },
    methods: {
        list: function() {
            this.$axios.get(API_URL).then((response) => {
                console.log('success: ', response.data);
                this.receiptBatchChecks = response.data;
            }).catch((error) => {
            })
        }
    },
    created: function() {
        this.list();
    }
});

const ReceiptBatchCheckOutbox = Vue.component('receipt-check-outbox', {
    template: '#receipt-check-outbox',
    data: function() {
        return {
            receiptBatchChecks : []
        }
    },
    methods: {
        list: function() {
            this.$axios.get(API_URL+"/outbox", {'Content-Type': 'application/json'}).then((response) => {
                console.log('success: ', response.data);
                this.receiptBatchChecks = response.data;
            }).catch((error) => {
                alert('error');
            })
        }
    },
    created: function() {
        this.list();
    }
});

const ReceiptItems = {
    template: "#receipt-items",
    props: ['receiptBatchCheckId'],
    data: function() {
        return {
            receiptItems: []
        }
    },
    created() {
        console.log('here we go man');
        this.fetchReceiptItems();
    },
    methods:{
        fetchReceiptItems() {

            const loader = this.$loading.show({
                container: null,
                canCancel: false,
                placement: 'center'
            });

            this.$axios.get(config.API_URL+"/receipt-tmp/list-by-batch/"+this.receiptBatchCheckId).then((resp) => {
                this.receiptItems = resp.data;
            }).finally(() =>{
                loader.hide();
            })
        }
    },
    watch: {
        receiptBatchCheckId(receiptBatchCheckId) {
            this.fetchReceiptItems();
        }
    }
};

const ReceiptBatchCheckShow = Vue.component('receipt-check-show', {
    template: '#receipt-check-show',
    components: { ReceiptItems },
    data: function() {
        return {
            receiptBatchCheck: {
                entity: {},
                transactionType: {},
                receiptBatchTmp: {},
                maker: {}
            }
        }
    },
    methods: {
        onPosted: function() {
            this.$router.replace("/");
        },
        onRejected: function() {
            this.$router.replace("/");
        },
        get: function(id) {
            const url = API_URL + '/show/' + id;
            this.$axios.get(url).then((response) => {
                this.receiptBatchCheck = response.data;
            }).catch((error) => {
                console.error(error)
            });
        }
    },
    created: function() {
        // read the passed id from the route
        this.get(this.id);
    },

    computed: {
        id: function() {
            return this.$route.params.id;
        },
        transactionHeader() {
            return this.receiptBatchCheck.receiptBatchTmp;
        }
    }
});

const ReceiptBatchCheckOutboxItem = Vue.component('receipt-check-outbox-item', {
    template: '#receipt-check-outbox-item',
    data: function() {
        return {
            receiptBatchCheck: {
                entity: {},
                transactionType: {},
                receiptBatchTmp: {},
                maker: {}
            }
        }
    },
    methods: {
        get: function(id) {
            const url = API_URL + '/show/' + id;
            this.$axios.get(url).then((response) => {
                this.receiptBatchCheck = response.data;
            }).catch((error) => {
                iziToast.error({message: "Error occurred while fetching Transactions from inbox"});
            });
        }
    },
    created: function() {
        // read the passed id from the route
        const id = this.$route.params.id;
        this.get(id);
    }
});

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

Vue.component('app', {
    template: '#main-app',
    created: function() {
        console.log("started the main application")
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: ReceiptBatchCheckList},
    {path: "/outbox", name: 'outbox', component: ReceiptBatchCheckOutbox},
    {path: "/show/:id", name: 'show', component: ReceiptBatchCheckShow},
    {path: "/outbox/item/:id", name: 'outbox-item', component: ReceiptBatchCheckOutboxItem}
];

// the application router.
const router = new VueRouter({
    routes: routes
});

jQuery(() => {
    const vm = new Vue({
        el: "#app",
        router: router,
        template: "<app></app>"
    });

});
