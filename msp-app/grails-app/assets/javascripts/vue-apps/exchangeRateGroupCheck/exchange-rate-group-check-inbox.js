/**
 * Created by lebohof on 22/08/2021.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/components/currency-select
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle'],
    methods: {
        createRateGroup() {
            this.$router.push("/create")
        }
    }
});

/**
 * The Exchange Rate Group Check service
 */
const exchangeRateGroupCheckInboxService = {
    API_URL: config.API_URL+'/exchange-rate-group-check',

    inbox: function(queryParams) {
        return axios.get(this.API_URL+'/list-inbox', {params: queryParams});
    },
    get(id) {
        return axios.get(this.API_URL+`/inbox/show/${id}`);
    },
    save(exchangeRate) {
        return axios.post(this.API_URL+'/save', exchangeRate);
    },
    approve(id) {
        return axios.put(this.API_URL+'/approve/'+id);
    },
    reject(id) {
        return axios.put(this.API_URL+'/reject/'+id);
    }
};

/**
 * The main application component
 */
const ExchangeRateGroupCheckInboxApp = Vue.component('exg-check-inbox-app', {
    template: "#exg-check-inbox-app"
});

const ExchangeRateGroupCheckInbox = Vue.component('exg-check-list', {
    template: "#exg-check-list",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            exchangeRateGroupChecks: [],
            params: {},
            selectedExchangeRateGroupCheckId: null
        }
    },
    methods: {
        selectExchangeRateGroupCheckId(exchangeRateGroup) {
            this.selectedExchangeRateGroupCheckId = exchangeRateGroup.id;
        },
        fetchInbox() {
            this.loader.isLoading = true;

            exchangeRateGroupCheckInboxService.inbox(this.params).then((resp) => {
                this.exchangeRateGroupChecks = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.fetchInbox();
    }
});

const ExchangeRateGroupCheckShow = Vue.component('exg-check-show', {
    template: '#exg-check-show',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            exchangeRateGroupCheck: {
                entity: {}
            }
        }
    },
    computed: {
        exchangeRateGroup() {
            return this.exchangeRateGroupCheck.entity;
        },
        id: function() {
            return this.$route.params.id;
        }
    },
    methods: {
        get() {
            this.loader.isLoading = true;

            exchangeRateGroupCheckInboxService.get(this.id).then((resp) => {
                this.exchangeRateGroupCheck = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.get();
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "inbox",
        path: "/",
        component: ExchangeRateGroupCheckInbox
    },
    {
        name: "show",
        path: "/show/:id",
        component: ExchangeRateGroupCheckShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(ExchangeRateGroupCheckInboxApp)
});