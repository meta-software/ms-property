/**
 * Created by lebohof on 22/08/2021.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-options

Vue.use(VeeValidate, {
    events: 'change'
});

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle'],
    methods: {
        createRateGroup() {
            this.$router.push("/create")
        }
    }
});

/**
 * The Exchange Rate Group Check service
 */
const exchangeRateGroupCheckOutboxService = {
    API_URL: config.API_URL+'/exchange-rate-group-check',

    outbox: function(queryParams) {
        return axios.get(this.API_URL+'/list-outbox', {params: queryParams});
    },
    get(id) {
        return axios.get(this.API_URL+`/outbox/show/${id}`);
    },
    save(exchangeRate) {
        return axios.post(this.API_URL+'/save', exchangeRate);
    },
    approve(id) {
        return axios.put(this.API_URL+'/approve/'+id);
    },
    reject(id) {
        return axios.put(this.API_URL+'/reject/'+id);
    }
};

/**
 * The main application component
 */
const ExchangeRateGroupCheckOutboxApp = Vue.component('exg-check-outbox-app', {
    template: "#exg-check-outbox-app"
});

const ExchangeRateGroupCheckOutbox = Vue.component('exg-check-list', {
    template: "#exg-check-list",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            exchangeRateGroupChecks: [],
            params: {},
            selectedExchangeRateGroupCheckId: null
        }
    },
    methods: {
        selectExchangeRateGroupCheckId(exchangeRateGroup) {
            this.selectedExchangeRateGroupCheckId = exchangeRateGroup.id;
        },
        fetchOutbox() {
            this.loader.isLoading = true;

            exchangeRateGroupCheckOutboxService.outbox(this.params).then((resp) => {
                this.exchangeRateGroupChecks = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.fetchOutbox();
    }
});

const ExchangeRateGroupCheckShow = Vue.component('exg-check-show', {
    template: '#exg-check-show',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            exchangeRateGroupCheck: {
                entity: {}
            }
        }
    },
    computed: {
        exchangeRateGroup() {
            return this.exchangeRateGroupCheck.entity;
        },
        id: function() {
            return this.$route.params.id;
        }
    },
    methods: {
        get() {
            this.loader.isLoading = true;

            exchangeRateGroupCheckOutboxService.get(this.id).then((resp) => {
                this.exchangeRateGroupCheck = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.get();
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "outbox",
        path: "/",
        component: ExchangeRateGroupCheckOutbox
    },
    {
        name: "show",
        path: "/show/:id",
        component: ExchangeRateGroupCheckShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(ExchangeRateGroupCheckOutboxApp)
});