/**
 * Created by lebohof on 01/19/2020.
 */
//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

const APP_URL = config.API_URL + "/configs";
const configService = {
    list(params) {
        console.log(params)
        return axios.get(APP_URL, {params: params});
    },
    get(id) {
        return axios.get(APP_URL+"/show/"+id);
    },
    activate(id) {
        return axios.put(APP_URL+"/activate/"+id);
    },
    create(postData) {
        return axios.post(APP_URL+"/save", postData);
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar',
    methods: {
        create() {
            this.$router.push("/create")
        }
    }
});

const ConfigApp = Vue.component('config', {
    template: "#main-app",
    data: function () {
        return {}
    }
});

const ConfigCreate = Vue.component('conf-create', {
    template: "#conf-create",
    data: function () {
        return {
            configSetting: {
                configTypeId: null,
                value: null,
                startDate: null,
                endDate: null,
                active: false
            }
        }
    },
    methods: {
        save() {
            const loader = this.$loading.show({
                isFullPage: true
            });

            configService.create(this.configSetting).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Configuration created successfully.'});
                this.$router.replace("/");
            }).finally(() => {
                loader.hide();
            })
        },
        cancel() {
            this.$router.replace("/")
        }
    }
});

const ConfigList = Vue.component('config-list', {
    template: '#config-list',
    data: function() {
        return {
            params: {},
            configSettings: [],
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },
    methods: {
        search() {
            this.fetchConfigurations();
        },
        reset() {
            this.params = {};
            this.fetchConfigurations();
        },
        fetchConfigurations() {
            this.loader.isLoading = true;
            configService.list(this.params).then((resp) => {
                this.configSettings = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    created() {
        this.fetchConfigurations();
    }
});

const ConfigShow = Vue.component('conf-show', {
    template: '#conf-show',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            configSetting: {
                configType: {}
            }
        }
    },
    computed: {
        id: function() {
            return this.$route.params.id;
        }
    },
    methods: {
        get() {
            this.loader.isLoading = true;
            configService.get(this.id).then((resp) => {
                this.configSetting = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        activate() {
            this.loader.isLoading = true;
            configService.activate(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Configuration activated successfully...'});
                this.get();
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        notImplemented() {
            iziToast.error({
                overlay: true,
                layout: 2,
                title: 'Alert !!!<br />',
                baloon: true,
                timeout: 0,
                message: 'This feature is not activated.<br />Please contact administrator for more details.',
                position: 'center'})
        }
    },
    created() {
        this.get();
    }
});

//configure the application routes
const routes = [
    {
        component: ConfigList,
        name: 'home',
        path: '/'
    },
    {
        name: "show",
        path: "/show/:id",
        component: ConfigShow
    },
    {
        component: ConfigCreate,
        name: 'create',
        path: '/create'
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(ConfigApp)
});
