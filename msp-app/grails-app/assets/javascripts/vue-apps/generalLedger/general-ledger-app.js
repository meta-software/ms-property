/**
 * Created by lebohof on 7/27/2018.
 */

//= require global/plugins/vue/bootstrap-table/bootstrap-table-vue

const APP_URL = config.API_URL + '/general-ledger';

//the generalLedgerService class
const generalLedgerService = {
    list() {
        return axios.get(APP_URL+"?max=100");
    },

    outbox() {
        return axios.get(APP_URL+"/outbox?max=100");
    },

    get(id) {
        return axios.get(APP_URL + `/show/${id}`)
    },

    editRequest(id) {
        return axios.get(APP_URL + `/edit-request/${id}`)
    },

    save(generalLedger) {
        return axios.post(APP_URL + '/save', generalLedger);
    },

    update(id, generalLedger) {
        const url = APP_URL+`/update/${id}`;
        return axios.put(url, generalLedger);
    }
};

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle']
});

/**
 * The AccountTypeForm component
 */
const GeneralLedgerFormComp = Vue.component('acc-type-form-comp', {
    template: "#acc-type-form-comp",
    props:['accountType'],
    methods: {
        onSubmit: function(){
            console.log("GeneralLedgerFormComp::save()")
            this.$emit('save', this.accountType);
        },
        onUpdate: function() {
            console.log("GeneralLedgerFormComp::update()")
            this.$emit('save', this.accountType);
            return
        },
    },
    created: function() {
        alert('created')
    }
});

const GeneralLedgerForm = {
    template: '#general-ledger-form',
    props: {
        generalLedger: {
            type: Object,
            required: true
        }
    },
    data: function() {
        return {
            subAccountOptions: {
                placeholder: 'Sub Account...'
            },
            generalLedgerTypeOptions: {
                placeholder: 'GL Type...'
            }
        }
    },
    methods: {
        initForm() {
            // prepare the other form components
            this.subAccountOptions = {
                placeholder: 'Sub Account...',
                ajax: {
                    delay: 350,
                    url: config.API_URL + '/sub-account/list-options',
                    dataType: 'json',
                    processResults: function(data) {
                        console.log(data);

                        return {results: data.map((object) => {
                                return {
                                    id: object.id,
                                    text: object.accountNumber + ' : ' + object.accountName
                                }
                            }) }
                    }
                }
            }; // end subAccountOptions

            // prepare the other form components
            this.generalLedgerTypeOptions = {
                placeholder: 'General Ledger Type...',
                ajax: {
                    delay: 350,
                    url: config.API_URL + '/general-ledger-type/list-options',
                    dataType: 'json',
                    processResults: function(data) {
                        console.log(data);

                        return {results: data.map((object) => {
                                return {
                                    id: object.id,
                                    text: object.name
                                }
                            }) }
                    }
                }
            } // end generalLedgerTypeOptions
        }
    },
    mounted() {
        this.initForm();
    }
};

const GeneralLedgerCreate = Vue.component('general-ledger-create', {
    template: '#general-ledger-create',
    components: {
        'general-ledger-form': GeneralLedgerForm
    },
    data: function() {
        return {
            generalLedger: {
                generalLedgerType: {},
                subAccount: {},
                accountNumber: "",
                accountName: ""
            },
            loader: {
                isLoading: false
            }
        }
    },
    methods: {
        save: function() {

            this.loader.isLoading = true;

            generalLedgerService.save(this.generalLedger).then((resp) => {
                this.$router.replace('/');
            }).finally(() => {
                this.loader.isLoading = false;
            });
        },
        cancel() {
            this.$router.replace('/');
        },
    },
    created: function() { }
});

/**
 * The AccountTypeEdit component for editing an accountType entry
 */
const GeneralLedgerEditComp = Vue.component('acc-type-edit-comp', {
    template:"#acc-type-edit-comp",
    data: function() {
        return {
            accountType: {}
        }
    },
    methods: {
        save: function (accountType) {
            console.info("saving the object: ", accountType);
            this.$http.put(config.API_URL + "/account-type/update/" + accountType.id, accountType).then(function(response){
                console.info(response.data);
                this.$router.push({name: "home"})
            }, function(response){
                console.error("in the error");
                console.error(response);
            })

        }
    },
    created: function() {
        const id = this.$route.params.id;
        const vm = this
        if(id) {
            this.$http.get(config.API_URL + "/account-type/edit/"+id).then( function(response) {
                vm.accountType = response.data ;
            }, function(response) {
                console.log(response)
            });
        }
    },
    components: {'acct-type-form-comp': GeneralLedgerFormComp}
});

const BankAccount = {
    template: "#bank-account",
    props: {
        bankAccount: {
            type: Object,
            required: true
        }
    }
};

/**
 * The GeneralLedgerShow component for viewing a ledger account
 */
const GeneralLedgerShow = Vue.component('general-ledger-show', {
    template:"#general-ledger-show",
    components: {
        'general-ledger-form': GeneralLedgerForm,
        'bank-account': BankAccount
    },
    data: function() {
        return {
            loader: {
                isLoading: false
            },
            generalLedger: {
                subAccount: {},
                //bankAccount: {},
                generalLedgerType: {}
            }
        }
    },
    methods: {
        get: function () {
            this.loader.isLoading = true;
            generalLedgerService.get(this.id).then((response) => {
                this.generalLedger = response.data;
            }).finally(() => {
                this.loader.isLoading = false;
            });
        },
        editRequest: function() {
            this.loader.isLoading = true;
            generalLedgerService.editRequest(this.id).then((resp) => {
                this.generalLedger = response.data;
            }).catch((err) => {

            }).finally(() => this.loader.isLoading = false );
        }
    },
    computed: {
        id: function() {
            return this.$route.params.id;
        }
    },
    created: function() {
        this.get();
    }
});

const renderSubAccount = function(entry) {
    return entry
};

const renderGLType = function(entry) {
    return entry;
};

const renderStatus = function(entry) {
    return entry ? '<span class="label mc-status label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>';
};

const renderName = function(entry) {
    return entry;
};

/**
 * This components fetches the list of account-types from the server and renders them to the UI
 */
const GeneralLedgerApproved = Vue.component('general-ledger-approved', {
    template: '#general-ledger-approved',
    components: {
        BootstrapTable: BootstrapTable
    },
    data: function () {
        return {
            table: {
                options: {
                    toolbar: '#toolbar',
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    pagination: false,
                    height: 450,
                    sortable: true,
                    classes: "table table-bordered table-striped table-hover table-borderless table-condensed"
                },
                data: [],
                columns: [
                    {
                        title:"Account Name",
                        field: "accountName",
                        formatter: renderName
                    },
                    {
                        title: "Account Number",
                        field: "accountNumber"
                    },
                    {
                        title: "Sub Account",
                        field: "subAccount",
                        formatter: renderSubAccount

                    },
                    {
                        title: "General Ledger Type",
                        field: "generalLedgerType",
                        formatter: renderGLType
                    },
                    {
                        title: "Status",
                        field: "status",
                        formatter: renderStatus
                    },
                ]
            }
        }
    },
    methods: {
        list: function() {
            generalLedgerService.list().then((resp) => {
                this.table.data = resp.data.map((generalLedger) => {
                    return {
                        accountName: generalLedger.accountName,
                        accountNumber: generalLedger.accountNumber,
                        subAccount: generalLedger.subAccount.accountName,
                        generalLedgerType: generalLedger.generalLedgerType.name,
                        status: generalLedger.active,
                        id: generalLedger.id
                    }
                });
            });
        }
    },

    created() {
        this.list();
    }
});

/**
 * This components fetches the list of account-types from the server and renders them to the UI
 */
const GeneralLedgerOutbox = Vue.component('general-ledger-outbox', {
    template: '#general-ledger-outbox',
    components: {
        BootstrapTable: BootstrapTable
    },
    data: function () {
        return {
            table: {
                options: {
                    toolbar: '#toolbar',
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    pagination: true,
                    sortable: true,
                    classes: "table table-bordered table-striped table-hover table-borderless table-condensed"
                },
                data: [],
                columns: [
                    {
                        title:"Account Name",
                        field: "accountName",
                        formatter: renderName
                    },
                    {
                        title: "Account Number",
                        field: "accountNumber"
                    },
                    {
                        title: "Sub Account",
                        field: "subAccount",
                        formatter: renderSubAccount

                    },
                    {
                        title: "General Ledger Type",
                        field: "generalLedgerType",
                        formatter: renderGLType
                    },
                    {
                        title: "Status",
                        field: "status",
                        formatter: renderStatus
                    },
                ]
            }
        }
    },
    methods: {
        list: function() {
            generalLedgerService.outbox().then((resp) => {
                this.table.data = resp.data.map((generalLedger) => {
                    return {
                        accountName: generalLedger.accountName,
                        accountNumber: generalLedger.accountNumber,
                        subAccount: generalLedger.subAccount.accountName,
                        generalLedgerType: generalLedger.generalLedgerType.name,
                        status: generalLedger.active,
                        id: generalLedger.id
                    }
                });

            });
        }
    },
    created() {
        this.list();
    }
});

/**
 * The main application component
 */
const GeneralLedgerApp = Vue.component('general-ledger-app', {
    template: "#general-ledger-app",
    data: function() {
        return {
            title: "General Ledger`"
        }
    },
    created() {}

});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: {created() { this.$router.push("/approved") }}},
    {path: "/approved", name: 'approved', component: GeneralLedgerApproved},
    {path: "/outbox", name: 'outbox', component: GeneralLedgerOutbox},
    {path: "/create", name: 'create', component: GeneralLedgerCreate},
    {path: "/edit/:id", name: 'edit', component: GeneralLedgerEditComp},
    {path: "/show/:id", name: 'show', component: GeneralLedgerShow}
];

const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueRouter);

const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(GeneralLedgerApp)
});
