/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker
//= require vue-apps/maker-options
//= require vue-apps/components/currency-select
//= require vue-apps/components/exchange-rate

//Setup the plugins.
Vue.use(VeeValidate, { events: 'change' });
Vue.use(VueRouter);

const API_URL = config.API_URL + "/bulk-invoice";

const PageBar = Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

/**
 * Lists the bulkInvoices
 */
const BulkInvoiceInbox = Vue.component('bulk-invoice-inbox', {
    template: '#bulk-invoice-inbox',
    data: function() {
       return {
           loader: {
               isLoading: false,
               fullPage: true
           },
           bulkInvoices: [],
           params: {}
       }
    },
    methods: {
        search() {
            this.fetchInvoices();
        },
        reset() {
            this.params = {}
            this.fetchInvoices();
        },
        fetchInvoices() {
            this.loader.isLoading = true;
            this.$axios.get(API_URL, {params: this.params}).then((response) => {
                this.bulkInvoices = response.data;
            }).catch((error) => {
                iziToast.error({message: 'Internal error occured while retriving invoice items', timeout: 10000});
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    created: function() {
        this.fetchInvoices()
        console.log("created BulkInvoiceInbox:() successfully");
    },
    mounted: function() {
       console.log("mounted OK.");
    }
});

/**
 * Lists the bulkInvoices outbox (the ones created by this particular user)
 */
const BulkInvoiceOutbox = Vue.component('bulk-invoice-outbox', {
    template: '#bulk-invoice-outbox',
    data: function() {
       return {
           bulkInvoices: []
       }
    },
    methods: {
        fetchBulkInovoices() {
            const url = API_URL + '/outbox'

            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            this.$axios.get(url).then((response) => {
                this.bulkInvoices = response.data;
            }).catch((error) => {
                iziToast.error({message: 'Internal error occured while retriving invoice items', timeout: 10000});
            }).finally(()=>{
                loader.hide();
            });

        }
    },
    created: function() {
        this.fetchBulkInovoices()
       console.log("created BulkInvoiceInbox:() successfully");
    },
    mounted: function() {
       console.log("mounted OK.");
    }
});

/**
 * Lists the bulkInvoices outbox (the ones created by this particular user)
 */
const BulkInvoiceCancelled = Vue.component('bulk-invoice-cancelled', {
    template: '#bulk-invoice-cancelled',
    data: function() {
       return {
           bulkInvoices: []
       }
    },
    created: function() {
        const url = API_URL + '/trash'
        this.$axios.get(url).then((response) => {
            this.bulkInvoices = response.data;
        }).catch((error) => {
            iziToast.error({message: 'Internal error occured while retriving invoice items', timeout: 10000});
        });
    },
    mounted: function() {
       console.log("mounted OK.");
    }
});

const BulkInvoiceShow = Vue.component('bulk-invoice-show', {
    template: '#bulk-invoice-show',
    data: function() {
       return {
           bulkInvoice: {
               checkStatus: null,
               property: {},
               billingCycle: {},
               maker: {},
               bulkInvoiceItems : [],
               amount: 0
           }
       }
    },
    methods: {
        getBulkInvoice: function(id) {
            const url = API_URL + '/show/' + id;
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            this.$axios.get(url).then((response) => {
                this.bulkInvoice = response.data;
            }).catch((error) => {
                console.log('error: ', error)
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created: function() {
        const id = this.$route.params['id'];
        this.getBulkInvoice(id);
        console.log("created BulkInvoiceInbox:() successfully");
    }
});

const BulkInvoiceOutboxShow = Vue.component('bulk-invoice-outbox-show', {
    template: '#bulk-invoice-outbox-show',
    data: function() {
       return {
           bulkInvoice: {
               checkStatus: null,
               property: {},
               billingCycle: {},
               bulkInvoiceItems : [],
               amount: 0
           }

       }
    },
    created: function() {

        const id = this.$route.param['id'];
        const url = API_URL + "/show/" + id;

        this.$axios.get(url).then((response) => {
            console.log('response.data: ', response.data)

            this.bulkInvoices = response.data;
        }).catch((error) => {
            console.log('error: ', error)
        });

       console.log("created BulkInvoiceInbox:() successfully");
    },
    mounted: function() {
       console.log("mounted OK.");
    }
});

/**
 * List the simulated bulkInvoice(Items)
 */
const BulkInvoiceItems = Vue.component("bulk-invoice-items", {
    template: '#bulk-invoice-items',
    props: {
        bulkInvoiceItems: Array
    },
    data: function() {
        return {
        }
    },
    methods: {}
});

const BulkInvoiceCreate = Vue.component('bulk-invoice-create', {
    template: '#bulk-invoice-create',
    data: function() {
       return {
           subAccountOptions: {
               placeholder: 'Sub Account...'
           },
           propertyOptions: {
               placeholder: 'Property...'
           },
           billingCycleOptions: {
               placeholder: 'Billing Cycle...'
           },
           bulkInvoice: {
               property: {},
               billingCycle: {},
               subAccount: {},
               transactionDate: null,
               amount: null
           },
           bulkInvoiceItems: [],
           narrative: null
       }
    },
    methods: {
        onSaveBulkInvoice: function() {

            this.loader = this.$loading.show({
                canCancel: true,
                onCancel: this.onCancel,
            });

            const url = config.API_URL + '/bulk-invoice/save'

            this.$axios.post(url, this.bulkInvoice).then((response) => {
                this.loader.hide();
                iziToast.success({message: 'Invoice generated successfully.', timeout: 5000});

                this.$router.push("/");
            }).catch((errors) => {
                this.loader.hide();
                iziToast.error({message: 'Errors occured while saving bulk-invoice.', timeout: 10000});
            })
        },
        onCancelBulkInvoice: function() {
            this.$router.replace("/")
        },
        onSimulateBulkInvoice: function() {
            this.loader = this.$loading.show({
                canCancel: true,
                onCancel: this.onCancel,
            });

            const url = config.API_URL + "/bulk-invoice/simulate-allocations"

            this.$axios.post(url, this.bulkInvoice).then((response) => {
                this.bulkInvoiceItems = response.data['bulkInvoiceItems'];
                iziToast.success({message: 'Bulk invoice Allocation preview complete', timeout: 5000});
                this.loader.hide();
            }).catch( (error) => {
                this.loader.hide();
                iziToast.error({message: 'Error while Previewing Bulk Invoice', timeout: 10000});
            });
        }
    },
    created: function() {
        const url = API_URL + "/create";

        this.$axios.get(url).then((response) => {
            console.log('response.data: ', response.data)

        }).catch((error) => {
            console.log('error: ', error)
        });

        console.log("created BulkInvoiceCreate successfully");
    },
    mounted: function() {
        const vm = this;
        console.log("mounted OK.");

        // prepare the other form components
        this.subAccountOptions = {
            placeholder: 'Sub Account...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/sub-account/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data);

                    return {results: data.map((object) => {
                        return {
                            id: object.id,
                            text: object.accountNumber + ' : ' + object.accountName
                        }
                    }) }
                }
            }
        } // end subAccountOptions

        // prepare the other form components
        this.propertyOptions = {
            placeholder: 'Properties...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/property/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data);

                    return {results: data.map((object) => {
                        return {
                            id: object.id,
                            text: object.name
                        }
                    }) }
                }
            }
        } // end subAccountOptions

        this.billingCycleOptions = {
            placeholder: 'Billing Cycle...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/billing-cycle/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data);

                    return {results: data.map((object) => {
                        return {
                            id: object.id,
                            text: object.name
                        }
                    }) }
                }
            }
        } // end subAccountOptions

        //prepare jquery items
        $(this.$refs.transactionDate).datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true}).on(
            "changeDate", function() {
                vm.bulkInvoice.transactionDate = $(this).val()
            }
        );

    }
});

// Prepare the application routes here.
// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: BulkInvoiceInbox},
    {path: "/inbox", name: 'inbox', component: BulkInvoiceInbox},
    {path: "/inbox/:id", name: 'inboxItem', component: BulkInvoiceShow},
    {path: "/outbox", name: 'home', component: BulkInvoiceOutbox},
    {path: "/outbox/:id", name: 'outboxItem', component: BulkInvoiceOutboxShow},
    {path: "/trash", name: 'home', component: BulkInvoiceCancelled},
    {path: "/trash/:id", name: 'trashItem', component: BulkInvoiceCancelled},
    {path: "/create", name: 'create', component: BulkInvoiceCreate}
];

// the application router.
const router = new VueRouter({
    routes: routes
});
// prepare the application routes here

jQuery(document).ready(()=> {
//This is the main app
    const vm = new Vue({
        el: "#app",
        router: router,
        created: function() {
            console.log('created the main bulk-invoice Vue app successfully.')
        }
    });
});

