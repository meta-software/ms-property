/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

const bulkInvoiceShowApp = new Vue({
    el: '#bulkInvoiceShow',
    data: {
        loader: null,
        bulkInvoiceId: bulkInvoiceId,
        bulkInvoice: {
            id: bulkInvoiceId
        },
    },
    methods: {
        terminateBulkInvoice: function() {
            const vm = this;

            this.bulkInvoice.terminated = true;

            this.$axios.put(config.API_URL + "/bulkInvoice/terminate/"+this.bulkInvoiceId, this.bulkInvoice).then(function(response){
                console.log("successfully terminated  bulkInvoice id " + vm.bulkInvoiceId);
                location.reload(true);
            }).catch(function(error){
                console.log(error.data);
            });
        },
    },
    mounted: function() {
        this.isLoading = true;

        $('#inner-content-div').slimScroll({
            height: '250px'
        });
    }
});
