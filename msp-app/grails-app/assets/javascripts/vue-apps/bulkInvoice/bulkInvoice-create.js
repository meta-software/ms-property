/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex

Vue.use(VeeValidate, {
    events: 'change'
});

/**
 * The main application component
 */
const BulkInvoiceComp = Vue.component("bulk-invoice-list", {
    template: '#bulkInvoiceList',
    props: {
         bulkInvoiceItems: Array
    },
    data: function() {
        return { }
    },
    methods: {
        create: function() {
            const url = config.API_URL + "/bulk-invoice/create"

            this.$axios.get(url).then( (response) => {

                var transaction = response.data;

                // we want to push the router to editing this new transaction
                vm.$router.push({name: 'editTrans', params: {id: transaction.id}});

            }).catch(function(error){
                console.error(error)
            })
        }
    }
});

const vm = new Vue({
    el: '#app',
    data: {
        bulkInvoice: {
            property: {
                id: 2
            },
            billingCycle: {
                id: 6
            },
            subAccount: {
                id: 3
            },
            transactionDate: moment().format('YYYY-MM-DD', new Date()),
            transactionReference: '10000',
            amount: '1000'
        },
        bulkInvoiceItems: [],
        bulkInvoiceForm: {
            transactionTypes: []
        }
    },
    methods: {
        simulate: function() {

            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });

            const url = config.API_URL + "/bulk-invoice/simulate";
            this.$axios.post(url, this.bulkInvoice).then((response) => {
                this.bulkInvoiceItems = response.data['bulkInvoiceItems'];
                this.loader.hide();
            }).catch( (error) => {
                this.loader.hide();
                iziToast.error({message: 'Error while simulating Bulk Invoice', timeout: 10000});
            }).then(() => {
            })
        },
        save: function() {
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });

            const url = config.API_URL + "/bulk-invoice/save"
            this.$axios.post(url, this.bulkInvoice).then( (response) => {
                this.bulkInvoice = response.data;

                this.$notify({
                    title: 'Bulk Invoice Success!',
                    type: 'success',
                    content: "Bulk Invoice created successfully",
                    duration: 5000
                }).then(() => {
                    window.location.assign(response.data['_links']['self']['href']);
                });

                // now we refresh the window
                this.loader.hide();
            }).catch( (error) => {
                this.$notify({
                    title: 'Bulk Invoice Error!',
                    type: 'danger',
                    content: "Error during saving of Bulk Invoce",
                    duration: 10000
                });
                this.loader.hide();
            })
        }
    },
    created: function() {
    },
    mounted: function() {
        const vm = this;

        $("#transactionType").change(function(){
            vm.bulkInvoice.transactionType = $(this).val();
        });

        $("#property").change(function(){
            vm.bulkInvoice.property = $(this).val();
        });

        $("#billingCycle").change(function(){
            vm.bulkInvoice.billingCycle = $(this).val();
        });

    }

});
