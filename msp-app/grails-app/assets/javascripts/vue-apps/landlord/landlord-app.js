
/**
 * Created by lebohof on 8/22/2018.
 */

// prepare the Landlord Services
function LandlordService () {

    this.get = function(id, success, error) {
        axios.get(config.API_URL + "/landlord/show/"+id).then((response) => {
            success(response);
         }).catch((response) => {
            console.log('error occured: ', response);
            error(response);
        });
    };

    this.save = function(landlord, successCallback, errorCallback) {
        if(landlord.id) {
            axios.put(config.API_URL + "/landlord/update/"+id).then((response) => {
                successCallback(response);
            }).catch((response) => {
                console.log('error request response : ', response);
                errorCallback(response);
            })
        } else {
        }
    }
};
const landlordService = new LandlordService();

var LandlordPlaceholderComponent = Vue.component("landlord-placeholder", {
    data: function() {
        return {

        }
    },
    created: function() {
        console.log("created the LandlordPlaceholderComponent successfully.");
    }
});

//The landlord list component
var LandlordListComp = Vue.component('landlord-list-comp', {
    template: '#landlord-list-comp',
    data: function () {
        return {
            landlordList: []
        }
    },
    created: function () {
        this.$http.get(config.API_URL + "/landlord/index")
            .then(function(response) {

                if (response.data) {
                    this.landlordList = response.data;
                }

            }, function(response){
                console.error("error response returned: " + response)
            });
    }
});

// The Landlord edit component
var LandlordEditComp = Vue.component('landlord-edit-comp', {
    template: "#landlord-edit-comp",
    data: function() {
        return {
            landlord: {}
        }
    },
    methods: {
        initComponent: function() {
            const id = this.$route.params.id;

            //retrieve the
            this.$http.get(config.API_URL + "/landlord/edit/"+id).then(function(response){
                console.infO("response: ", response)
                this.landlord = response.data;
            },function(error){
                console.error("error occured: ", error);
            });
        }
    },
    created: function() {
        this.initComponent();
    }
});

// The Landlord show component
var LandlordShowComp = Vue.component('landlord-show-comp', {
    template: "#landlord-show-comp",
    data: function() {
       return {
           landlord: {
               accountType: {},
               accountCategory: {}
           }
       }
    },
    methods: {
        initComponent: function() {
            const id = this.$route.params.id;
            var vm = this;

            landlordService.get(id, function(response){
                vm.landlord = response.data;
            }, function(error){
                console.error('error: ', error);
            });

        }
    },
    created: function() {
        this.initComponent();
    }

});

/**
 * The main application component
 */
var LandlordApp = Vue.component('landlord-app', {
    template: "#landlord-app",
    data: function() {
        return {
            title: "Landlord App"
        }
    },
    created: function() {
        console.log("Welcome to the Landlord Vue App!!!")
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'index', component: LandlordListComp},
    {path: "/create",   name: 'create', component: LandlordPlaceholderComponent},
    {path: "/edit/:id", name: 'edit', component: LandlordEditComp},
    {path: "/show/:id", name: 'show', component: LandlordShowComp}
];

// the application router.
const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueResource);
Vue.use(VueRouter);

var vm = new Vue({
    el: '#app',
    router: router,
    template: "<landlord-app></landlord-app>"
});

