
/**
 * Created by lebohof on 8/22/2018.
 */

//= require global/plugins/lodash.min

Vue.component("current-receipts", {
    template: "#current-receipts",
    props: ['accountNumber'],
    data() {
        return {
            totalReceipts: 0.0
        }
    },
    created() {

        let queryParams = {
            report: 'current-receipts',
            account: this.accountNumber,
        };

        this.$axios.get(config.API_URL+"/landlord/dashboard", {params: queryParams}).then((resp)=>{
            this.totalReceipts = resp.data.totalReceipts;
        }).catch((err) => {
            iziToast.error({content: "Error occured while fetching data", title: "Error: <br \>"})
        })
    }
});

// property form component
const propertyFormComp = Vue.component("property-list", {
    template: "#property-list",
    props: ['landlordId'],
    data: function () {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            properties: [],
            queryParams: {
                query: null
            }
        }
    },
    methods: {
        search() {
            this.fetchProperties();
        },
        refresh() {
            this.queryParams.query = null;
            this.fetchProperties()
        },
        fetchProperties: function () {
            this.loader.isLoading = true;
            let params = {
                query: this.queryParams.query
            }

            this.$axios.get(config.API_URL+'/landlord/'+this.landlordId+'/property-list', {params: params}).then((resp) => {
                this.properties = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.fetchProperties();
    }
});

const landlordShowApp = new Vue({
    el: '#landlord-show-app',
    data: {
        loader: null,
        landlordId: landlordId,
        landlord: {
            id: landlordId,
            accountNumber: accountNumber
        },
    },
    methods: {
        terminateLandlord: function() {
            const vm = this;

            this.landlord.terminated = true;

            this.$axios.put(config.API_URL + "/landlord/terminate/"+this.landlordId, this.landlord).then(function(response){
                console.log("successfully terminated  landlord id " + vm.landlordId);
                location.reload(true);
            }).catch(function(error){
                console.log(error.data);
            });
        },
    },
    created: function() {
        this.isLoading = true;
    }
});
