/**
 * Created by lebohof on 01/15/2019.
 */
//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

const LandlordMainApp = new Vue({

    el: '#main-app',
    data: {
        landlordForm: false
    },
    methods: {

    }
});
