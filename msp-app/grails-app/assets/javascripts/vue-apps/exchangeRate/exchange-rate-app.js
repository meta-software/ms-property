/**
 * Created by lebohof on 12/29/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/components/currency-select

Vue.use(VeeValidate, {
    events: 'change'
});

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle'],
    methods: {
        createRate() {
            this.$router.push("/create")
        }
    }
});

/**
 * The Exchange Rate service
 */
const exchangeRateService = {
    API_URL: config.API_URL+'/exchange-rate',

    list: function(queryParams) {
        return axios.get(this.API_URL+'/list', {params: queryParams});
    },
    get(id) {
        return axios.get(this.API_URL+`/show/${id}`);
    },
    save(exchangeRate) {
        return axios.post(this.API_URL+'/save', exchangeRate);
    },
    open(id) {
        return axios.put(this.API_URL+'/open/'+id);
    },
    close(id) {
        return axios.put(this.API_URL+'/close/'+id);
    },
    activate(id) {
        return axios.put(this.API_URL+'/activate/'+id);
    }
};

/**
 * The main application component
 */
const ExchangeRateApp = Vue.component('er-app', {
    template: "#er-app"
});

const ExchangeRateList = Vue.component('er-list',{
    template: "#er-list",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            exchangeRates: [],
            params: {}
        }
    },
    methods: {
        search() {
            this.fetchRates(this.params);
        },
        reset() {
            this.params = {
                q: '',
                open: ''
            };
            this.fetchRates(this.params);
        },
        fetchRates() {
            this.loader.isLoading = true;

            exchangeRateService.list(this.params).then((resp) => {
                this.exchangeRates = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.fetchRates();
    }
});

const ExchangeRateShow = Vue.component('er-show', {
    template: '#er-show',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            exchangeRate: {}
        }
    },
    computed: {
        id: function() {
            return this.$route.params.id;
        }
    },
    methods: {
        get() {
            this.loader.isLoading = true;
            exchangeRateService.get(this.id).then((resp) => {
                this.exchangeRate = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        close() {
            this.loader.isLoading = true;
            exchangeRateService.close(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate closed successfully...'});
                this.get();
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        activate() {
            this.loader.isLoading = true;
            exchangeRateService.activate(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate activated successfully...'});
                this.get()
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        open() {
            this.loader.isLoading = true;
            exchangeRateService.open(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate opened successfully...'});
                this.get()
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        notImplemented() {
            iziToast.error({
                overlay: true,
                layout: 2,
                title: 'Alert !!!<br />',
                baloon: true,
                timeout: 0,
                message: 'This feature is not activated.<br />Please contact administrator for more details.',
                position: 'center'})
        }
    },
    created() {
        this.get();
    }
});

const ExchangeRateCreate = Vue.component('er-create', {
    template: "#er-create",
    data: function() {
        return {
            exchangeRate: {
                name: null,
                startDate: null,
                endDate: null,
                description: null,
                active: false
            }
        }
    },
    methods: {
        cancel() {
            //are you sure you want to cancel
            this.$router.push("/");
        },
        save() {
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            exchangeRateService.save(this.exchangeRate).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate created successfully...'});
                this.$router.push("/");
            }).finally(() => {
                loader.hide();
            });
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: ExchangeRateList
    },
    {
        name: "show",
        path: "/show/:id",
        component: ExchangeRateShow
    },
    {
        name: "create",
        path: "/create",
        component: ExchangeRateCreate
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(ExchangeRateApp)
});