/**
 * Created by lebohof on 05/19/2021.
 */

Vue.component('exchange-rate-preview', {
    template:  `
<div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-dollar"></i> Exchange Rates</h3>
    </div>
    <!-- List group -->
    <table class="table table-striped table-advanced">
        <thead>
            <tr>
                <th>StartDate</th>
                <th>EndDate</th>
                <th>From</th>
                <th>To</th>
                <th>Rate</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="exchangeRate in exchangeRates" :key="exchangeRate.id">
                <td>{{exchangeRate.startDate | formatDate('DD MMM, YYYY')}}</td>
                <td>{{exchangeRate.endDate | formatDate('DD MMM, YYYY')}}</td>
                <td>{{exchangeRate.fromCurrency.id}}</td>
                <td>{{exchangeRate.toCurrency.id}}</td>
                <td>{{exchangeRate.rate | formatNumber(4)}}</td>
            </tr>
            <tr v-if="errorMessage">
                <td colspan="5"><span class="error">{{errorMessage}}</span></td>
            </tr>
        </tbody>
    </table>
</div>
`,
    props: {
        groupId: {
            required: false
        },
        mode: {
            required: false,
            default: 'preview' // accepts either "preview" or "dashlet"
        }
    },
    data() {
        return {
            exchangeRates: []
        }
    },
    methods: {
        initComponent() {

            if(this.isValidInput) {
                const loader = overlayLoader.customerLoader({
                    canCancel: false,
                    isFullPage: true,
                    color: '#000000',
                    loader: 'spinner'
                });

                let params = {
                    'groupId': this.groupId
                };

                axios.get(config.API_URL+'/exchange-rate/list-active', {params: params}).then((resp) => {
                    this.exchangeRates = resp.data;
                }).finally(() => loader.hide())
            }

        }
    },
    created() {
        this.initComponent()
    },
    computed: {
        isValidInput() {
            return ( (this.mode === 'dashlet') ||
                (this.mode === 'preview' && this.groupId)
            )
        },
        errorMessage() {
            if(!this.isValidInput) {
                return 'No Data Selected'
            }
        }
    },
    watch: {
        groupId(groupId) {
            this.initComponent()
        }
    }
});
