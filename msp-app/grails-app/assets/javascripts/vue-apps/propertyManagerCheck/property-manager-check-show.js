/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate


/**
 * Created by lebohof on 11/11/2018.
 */

var makerCheckerTemplate = `
        <div v-if="status=='PENDING'" class="row">
            <div class="col-md-12">
                <div class="form-actions">
                    <button type="button" @click="approve" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Approve</button>
                    <button type="button" @click="reject" class="btn btn-sm yellow-gold"><i class="fa fa-times"></i> Reject</button>
                </div>
            </div>
        </div>

        <div :class="{'btn-xs': small, 'btn-sm': !small }" class="mt-action-buttons" v-if="false">
            <div class="btn-group btn-group-sm">
                <button type="button" @click="approve" class="btn btn-outline green btn-sm"><i class="fa fa-check"></i> Appove</button>
                <button type="button" @click="reject"  class="btn btn-outline red btn-sm"><i class="fa fa-times"></i> Reject</button>
            </div>
        </div>
        <span v-if="status=='rejected'" class="label label-sm label-danger" ><i class="fa fa-times"></i> {{status}} </span>
        <span v-if="status=='approved'" class="label label-sm label-primary" ><i class="fa fa-check"></i> {{status}} </span>

        <modal v-model="openRejectModal" id="maker-checker-reject" >
            <span slot="title"><i class="fa fa-info"></i> Reject Reason ?</span>

            <form>
                <!-- BEGIN FORM BODY-->
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Reject Comment</label>
                                <textarea v-validate="'required'" v-model="makerChecker.description" name="description" class="form-control"></textarea>
                                <span class="has-error help-block">{{ errors.first('description') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END FORM BODY-->
            </form>

            <div slot="footer">
                <btn @click="openRejectModal=false"><i class="fa fa-times"></i> Cancel</btn>
                <btn @click="rejectEntity" class="blue-hoki" ><i class="fa fa-save"></i> Submit</btn>
            </div>
        </modal>`;

const MakerCheckerComp = Vue.component('maker-checker', {
    props: ['status', 'itemId', 'small'],
    template: makerCheckerTemplate,
    data: function() {
        return {
            openRejectModal: false,
            makerChecker: {
                description: '',
                status: null
            }
        }
    },
    created: function(){
        this.makerChecker = {
            status: this.status
        }
    },
    watch: {
        itemId: function(itemId) {
            this.makerChecker.id = itemId;
        },
        status: function(status) {
            this.makerChecker.status = status;
        }
    },
    methods: {
        approve: function(event){
            const vm = this;

            this.$confirm({
                title: 'Confirm ?',
                content: 'Are you sure you want to Approve.\nContinue?'
            }).then(function(msg){
                vm.startLoader();

                const url = config.API_URL + '/property-manager-check/approve/' + vm.itemId;
                vm.makerChecker.status = 1;
                const payload = {
                    id: vm.itemId,
                    makerChecker: vm.makerChecker
                };

                vm.$axios.put(url, payload).then(function(response){
                    vm.stopLoader();
                    vm.$alert({
                        title: 'Successful.',
                        content: 'Item Approval was successful !!!'
                    }).then(function(){
                        vm.startLoader();
                        window.location.reload(true);
                    });
                }).catch(function(error){
                    vm.stopLoader();
                    vm.$notify({
                        type: 'danger',
                        duration: 10000,
                        title: 'Error!!!',
                        content: error.response.data['message']
                    })
                });
            });
        },
        reject: function(event){
            this.openRejectModal = true;
        },
        startLoader: function(){
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
            });
        },
        stopLoader: function() {
            this.loader.hide();
        },
        rejectEntity: function(event){

            const vm = this;
            const url = config.API_URL + '/property-manager-check/reject/' + vm.itemId;

            vm.makerChecker.status = 2;
            const payload = {
                id: vm.itemId,
                makerChecker: vm.makerChecker
            };

            vm.startLoader();
            vm.$axios.put(url, payload).then(function(response){
                vm.stopLoader();
                vm.$alert({
                    title: 'Successful',
                    content: 'Item rejection was successful'
                }).then(function(){
                    vm.startLoader();
                    vm.openRejectModal = false;
                    window.location.reload();
                })
            }).catch(function(error){
                vm.stopLoader();
                vm.$notify({
                    type: 'danger',
                    duration: 10000,
                    title: 'Error !!!',
                    content: error.response.data['message']
                });
            }) ;

        }
    }
});

Vue.use(VeeValidate, {
    events: 'change'
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app'
});
