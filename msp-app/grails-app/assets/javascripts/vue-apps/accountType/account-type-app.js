/**
 * Created by lebohof on 7/27/2018.
 */

/**
 * The AccountTypeForm component
 */
var AccTypeFormComp = Vue.component('acc-type-form-comp', {
    template: "#acc-type-form-comp",
    props:['accountType'],
    methods: {
        onSubmit: function(){
            console.log("AccTypeFormComp::save()")
            this.$emit('save', this.accountType);
        },
        onUpdate: function() {
            console.log("AccTypeFormComp::update()")
            this.$emit('save', this.accountType);
            return
        },
    },
    created: function() {
        
    }
});

var AccTypeCreateComp = Vue.component('acct-type-create-comp', {
    template: '#acc-type-create-comp',
    data: function() {
        return {
            accountType: {}
        }
    },
    methods: {
        save: function(accountType) {
            this.$http.post(config.API_URL + "/account-type/save", accountType).then( function(response) {
                console.log("object saved successfully: " + response.data);
                this.$router.push({name: "home"});
            }, function(response) {
                console.error("an error occured while saving: " + accountType);
                console.error(response)
            });
            console.log("saving object: " + accountType);
        }
    },
    created: function() {

        var vm = this;

        this.$http.get(config.API_URL + "/account-type/create").then( function(response) {
            vm.accountType = response.data
        }, function(response) {
            console.error('error occured: ', response);
        });
    },
    components: {'acc-type-form-comp': AccTypeFormComp}
})

/**
 * The AccountTypeEdit component for editing an accountType entry
 */
var AccTypeEditComp = Vue.component('acc-type-edit-comp', {
    template:"#acc-type-edit-comp",
    data: function() {
        return {
            accountType: {}
        }
    },
    methods: {
        save: function (accountType) {
            console.info("saving the object: ", accountType);
            this.$http.put(config.API_URL + "/account-type/update/" + accountType.id, accountType).then(function(response){
                console.info(response.data);
                this.$router.push({name: "home"})
            }, function(response){
                console.error("in the error");
                console.error(response);
            })

        }
    },
    created: function() {
        const id = this.$route.params.id;
        var vm = this
        if(id) {
            this.$http.get(config.API_URL + "/account-type/edit/"+id).then( function(response) {
                vm.accountType = response.data ;
            }, function(response) {
                console.log(response)
            });
        }
    },
    components: {'acct-type-form-comp': AccTypeFormComp}
})

/**
 * This components fetches the lisf of account-types from the server and renders them to the UI
 */
var AccTypeListComp = Vue.component('acc-type-list-comp', {
    template: '#acc-type-list-comp',
    data: function () {
        return {
            accountTypeList: []
        }
    },
    created: function () {
        this.$http.get(config.API_URL + "/account-type/index")
        .then(function(response) {
            response.status;

        if (response.data) {
            this.accountTypeList = response.data;
        }

        }, function(response){
            console.error("error response returned: " + response)
        });
    }
});

/**
 * The main application component
 */
var AcctTypeApp = Vue.component('acct-type-app', {
    template: "#acc-type-app",
    data: function() {
        return {
            title: "Account Type App"
        }
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: AccTypeListComp},
    {path: "/create", name: 'create', component: AccTypeCreateComp},
    {path: "/edit/:id", name: 'edit', component: AccTypeEditComp}
];

const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueResource);
Vue.use(VueRouter);

var vm = new Vue({
    el: '#app',
    router: router,
    template: "<acct-type-app></acct-type-app>"
});
