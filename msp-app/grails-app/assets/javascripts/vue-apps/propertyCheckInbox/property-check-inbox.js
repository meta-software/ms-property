/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker
//= require vue-apps/maker-options
//= require global/plugins/vue/bootstrap-table/bootstrap-table-vue

Vue.use(VeeValidate, {
    events: 'change'
});
Vue.use(VueRouter);

const API_URL = config.API_URL + "/property-check-inbox"

const PageBar = Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const PropertyTmp = Vue.component('property-tmp', {
    template: '#property-tmp',
    props: {
        propertyTmp: {
            required: true
        }
    },
    data: function() {
        return {}
    }
});

const RentalUnitList = Vue.component('rental-units', {
    template: '#rental-units',
    props: ['property'],
    components: {
        BootstrapTable: BootstrapTable
    },
    data: function() {
        return {
            rentalUnits: [],
            fetchRentalUnits: {

            },
            table: {
                columns: [
                    {
                        title: "Unit Reference",
                        field: "unitReference"
                    },
                    {
                        title:"Unit Name",
                        field: "name",
                        visible: true
                    },
                    {
                        title:"Area (Sqm)",
                        field: "area",
                        visible: true
                    }
                ],
                data: [],
                options: {
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    singleSelect: true,
                    height: 350,
                    pagination: true,
                    sortable: true,
                    classes: "table table-bordered table-striped table-hover table-borderless table-condensed"
                }
            },
        }

    },
    created: function() {
        this.loadRentalUnits();
    },
    methods: {
        loadRentalUnits: function() {
            const url = config.API_URL + '/rental-unit-tmp/index?propertyId='+this.property.id;

            this.$axios.get(url).then((response) => {

                this.table.data = response.data;

            }).catch((error) => {
                console.log(error)
                iziToast.error({message: 'Error while fetching property rental units...', title: 'Error...'})
            })
        }

    }
});

const PropertyCheckList = Vue.component('property-check-list', {
    template: '#property-check-list',
    data: function() {
        return {
            propertyChecks : []
        }
    },
    methods: {
        list: function() {
            const vm = this;
            this.$axios.get(API_URL, {'Content-Type': 'application/json'}).then((response) => {
                 console.log('success: ', response.data);
                vm.propertyChecks = response.data;
            }).catch((error) => {
                alert('error');
            })
        }
    },
    created: function() {
        this.list();
    }
});

const PropertyCheckShow = Vue.component('property-check-show', {
    template: '#property-check-show',
    data: function() {
        return {
            propertyCheck: {
                transactionSource: {},
                maker: {},
                checker: {},
                propertyTmp: {}
            }
        }
    },
    computed: {
        propertyTmp() {
            return this.propertyCheck.transactionSource;
        }
    },
    methods: {
        get: function(id) {
            const url = API_URL + '/show/' + id;

            this.$axios.get(url).then((response) => {
                this.propertyCheck = response.data;
            }).catch((error) => {
                console.error(error);
                this.$alert({
                    type: "danger",
                    content: "Error occurred while fetching Property record"
                })
            });
        }
    },
    created: function() {
        // read the passed id from the route
        const id = this.$route.params.id;
        this.get(id);
    }
});

const MainApp = Vue.component('app', {
    template: '#main-app',
    data: function() {
        return {
            logging: [],
            showFilter: true,
            showPicker: true,
            paginated: true,
            columns: [
                {
                    title:"id",
                },
                {
                    title:"name",
                    visible: true,
                    editable: true,
                },
                {
                    title:"age",
                    visible: true,
                    editable: true,
                },
                {
                    title:"country",
                    visible: true,
                    editable: true,
                }
            ],
            values: [
                {
                    "id": 1,
                    "name": "John",
                    "country": "UK",
                    "age": 25,
                },
                {
                    "id": 2,
                    "name": "Mary",
                    "country": "France",
                    "age": 30,
                },
                {
                    "id": 3,
                    "name": "Ana",
                    "country": "Portugal",
                    "age": 20,
                }
            ]
        }
    },
    created: function() {
        console.log("started the main application")
    }
});

// Prepare the application routes here.
// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: PropertyCheckList},
    {path: "/show/:id", name: 'show', component: PropertyCheckShow}
];

// the application router.
const router = new VueRouter({
    routes: routes
});
// prepare the application routes here


/**
 * The main application component
 */
const vm = new Vue({
    el: "#app",
    template: "<app></app>",
    router: router,
    created: function() {
        console.log('created the main application')
    }
});
