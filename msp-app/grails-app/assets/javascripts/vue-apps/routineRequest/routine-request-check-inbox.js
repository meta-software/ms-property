/**
 * Created by lebohof on 11/19/2019.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker

const API_URL = config.API_URL + "/routine-request-check-inbox"

const routineRequestCheckService = {
    list(params) {
        return axios.get(API_URL, {params: params});
    },

    get(id) {
        const url = API_URL+"/show/"+id
        return axios.get(url);
    },

    reject(id, data) {
        const url = API_URL + `/reject/${id}`
        return axios.put(url, data);
    },

    approve(id) {
        const url = API_URL + `/approve/${id}`
        return axios.put(url, {id: id});
    }
};

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueRouter);

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const RoutineRequestCheckApp = Vue.component("app", {
    template: "#main-app",
    data() {
        return {
        }
    }
});

const RoutineRequestCheckList = Vue.component("routine-request-check-list", {
    template: '#routine-request-check-list',
    data() {
        return {
            routineRequestChecks: [],
            params: {},
            loader: {
                isLoading: false,
                fullPage: true
            }
        }
    },

    created() {
        this.fetchRequests();
    },

    methods: {
        reset() {
            this.params = {}
            this.fetchRequests();
        },
        search() {
            this.fetchRequests();
        },
        fetchRequests() {
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            routineRequestCheckService.list(this.params).then((response) => {
                this.routineRequestChecks = response.data;
                console.log("created successfully")
            }).finally(() => {
                loader.hide();
            });
        }
    }
});

const RoutineRequestCheckshow = Vue.component("routine-request-check-show", {
    template: "#routine-request-check-show",
    data() {
        return {
            routineRequestCheck: {
                entity: {
                    routineType: {},
                    billingCycle: {}
                },
                maker: {},
                checker: {}
            }
        }
    },

    methods: {
        fetchRequest() {
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });

            routineRequestCheckService.get(this.id).then((response) => {
                this.routineRequestCheck = response.data;
            }).catch((err) => {
                this.$router.push("/");
            }).finally(() => {
                loader.hide();
            });
        }
    },

    created() {
        this.fetchRequest()
    },

    computed: {
        id() {
            return this.$route.params.id;
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        path: "/",
        component: RoutineRequestCheckList
    },
    {
        name: "show",
        path: "/:id",
        component: RoutineRequestCheckshow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router,
    render: (h) => h(RoutineRequestCheckApp)
});
