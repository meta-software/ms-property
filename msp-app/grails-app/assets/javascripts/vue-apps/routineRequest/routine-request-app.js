/**
 * Created by lebohof on 11/19/2019.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-options
// = require global/vue/bootstrap-table/bootstrap-table-vue

const API_URL = config.API_URL + "/routine-request"

const statusClass = function(status) {

    let labelClass = 'label-default'

    switch(status) {
        case 'success':
            labelClass = 'label-success'
            break

        case 'error':
            labelClass = 'label-danger'
            break

        case 'pending':
        default:
            labelClass = 'label-default'
            break
    }

    return labelClass
};


const routineRequestService = {
    list: function() {
        return axios.get(API_URL)
    },

    get: function(id) {
        const url = API_URL+"/show/"+id
        return axios.get(url)
    },

    listRoutineTransactions: function(requestRoutineId) {
        const url = API_URL+"/transactions/"+requestRoutineId
        return axios.get(url)
    },

    create: function(routineRequest) {
        const url = API_URL + "/create-request"
        return axios.post(url, routineRequest)
    },

    listOutbox() {
        const url = config.API_URL + "/routine-request-check-outbox"
        return axios.get(url)
    },

    getOutboxItem(id) {
        const url = config.API_URL + `/routine-request-check-outbox/show/${id}`
        return axios.get(url)
    }
};

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueRouter)

const PageBar = Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const RoutineRequestApp = Vue.component("app", {
    template: "#main-app",
    data: function() {
        return {
        }
    }
});

const RoutineRequestList = Vue.component("routine-request-list", {
    template: '#routine-request-list',
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            routineRequests: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                columns: [
                    {
                        title: '#',
                        field: 'id'
                    },
                    {
                        title: 'Type',
                        field: 'type'
                    },
                    {
                        title: 'Cycle',
                        field: 'cycle'
                    },
                    {
                        title: 'Status',
                        field: 'requestStatus',
                        formatter: this.statusFormatter
                    },
                    {
                        title: 'Run Date',
                        field: 'runDate'
                    },
                    {
                        title: 'Start Time',
                        field: 'startTime'
                    },
                    {
                        title: 'End Time',
                        field: 'endTime'
                    },
                    {
                        title: 'Requested By',
                        field: 'requestedBy'
                    }
                ],
                data: [],
                options: {
                    onClickRow: (row) => {this.$router.push("/"+row.id)},
                    toolbar: '#toolbar-list',
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    singleSelect: true,
                    height: 350,
                    pagination: true,
                    sortable: true,
                    classes: "table table-borderless table-striped table-hover table-condensed"
                }
            }
        }
    },

    created() {
        this.getList()
    },

    methods: {

        statusFormatter(data) {
            return `<span class="request-status display-block label label-sm ${statusClass(data)}">${data}</span>`
        },

        getList() {

            this.loader.isLoading = true;

            routineRequestService.list().then((response) => {
                this.routineRequests = response.data;

                this.table.data = this.routineRequests.map((routine) => {
                    return {
                        id: routine.id,
                        type: routine.routineType.name,
                        cycle: routine.billingCycle.name,
                        requestStatus: routine.requestStatus,
                        runDate: moment(routine.runDate).format('YYYY-MM-DD'),
                        startTime: moment(routine.startTime).format('HH:mm'),
                        endTime: moment(routine.endTime).format('HH:mm'),
                        requestedBy: routine.requestedBy.username
                    }
                });

                console.log(this.routineRequests)
            }).finally(() => {
                this.loader.isLoading = false
            })
        },
        statusClass(requestStatus) {
            return statusClass(requestStatus);
        }
    }
})

const RoutineRequestOutbox = Vue.component("routine-request-outbox", {
    template: '#routine-request-outbox',
    data() {
        return {
            routineRequestChecks: [],
            loader: {
                isLoading: true,
                fullPage: false
            }
        }
    },

    created() {
        this.getOutboxList()
    },

    methods: {
        getOutboxList() {
            routineRequestService.listOutbox().then((response) => {
                this.routineRequestChecks = response.data
             }).finally(() => {
                this.loader.isLoading = false
            })
        },

        statusClass(status) {
            return statusClass(status)
            statusClass(status)
        }
    }
})

Vue.component('routine-transactions', {
    template: "#routine-transactions",
    components: {
        'BootstrapTable': BootstrapTable
    },
    props: {
        routineRequestId: {
            required: true
        }
    },
    data() {
        return {
            transactions: {},
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                data: [],
                columns: [
                    {
                        title: 'Txn Type',
                        field: 'transactionType'
                    },
                    {
                        title: 'Property',
                        field: 'property'
                    },
                    {
                        title: 'Rental Unit',
                        field: 'rentalUnit'
                    },
                    {
                        title: 'Credit Acc',
                        field: 'accountNumberCr'
                    },
                    {
                        title: 'Reference',
                        field: 'transactionReference'
                    },
                    {
                        title: 'Amount',
                        field: 'transactionAmount'
                    }
                ],
                options: {
                    toolbar: '#toolbar',
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    height: 350,
                    pagination: true,
                    sortable: true,
                    classes: "table table-bordered table-striped table-hover table-borderless table-condensed"
                }
            }
        }
    },

    created() {
        this.getList()
    },

    methods: {
        getList() {
            this.loader.isLoading = true
            routineRequestService.listRoutineTransactions(this.routineRequestId).then((response) => {
                this.transactions = response.data;

                this.table.data = this.transactions.map((txn) => {
                    return {
                        transactionType: txn.transactionType.name,
                        accountNumberCr: txn.accountNumberCr,
                        property: (txn.property ? txn.property.name : '' ),
                        rentalUnit: txn.rentalUnit.name,
                        transactionReference: txn.transactionReference,
                        transactionAmount: txn.transactionAmount
                    }
                })

            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    }

})

const RoutineRequestShow = Vue.component("routine-request-show", {
    template: "#routine-request-show",
    data() {
        return {
            routineRequest: {
                id: "",
                routineType: {},
                maker: {},
                checker: {},
                billingCycle: {}
            },
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },

    created() {
        this.getEntity()
    },

    methods: {
        getEntity() {
            this.loader.isLoading = true

            routineRequestService.get(this.id).then((response) => {
                this.routineRequest = response.data
            }).finally(() => {
                this.loader.isLoading = false
            })

        },

        statusClass(status) {
            return statusClass(status)
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
})

const RoutineRequestOutboxShow = Vue.component("routine-outbox-show", {
    template: "#routine-outbox-show",
    data() {
        return {
            routineRequestCheck: {
                entity: {
                    routineType: {},
                    billingCycle: {}
                },
                maker: {},
                checker: {}
            },
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },

    created() {
        this.getEntity();
    },

    methods: {
        getEntity: function() {
            this.loader.isLoading = true;

            routineRequestService.getOutboxItem(this.id).then((response) => {
                this.routineRequestCheck = response.data
            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
})

const RoutineRequestCreate  = Vue.component('routine-request-create', {
    template: "#routine-request-create",
    data() {
        return {
            billingCycleOptions: {
                placeholder: 'Billing Cycle...'
            },
            routineTypeOptions: {
                placeholder: 'Routine Type...'
            },
            routineRequest: {
                routineType: {},
                billingCycle: {}
            }
        }
    },

    methods: {
        onCancelCreate() {
            console.log("cancel the creation process.")
            this.$router.go(-1)
        },
        onSaveRequest() {
            routineRequestService.create(this.routineRequest).then((response) => {
                console.log("routine request created successfully")
                this.$router.replace("/outbox")
            }).catch((error) => {
                console.error("error occurred during saving of routineRequest: ", error)
            })
        }
    },

    created() {

    },

    mounted() {
        const vm = this

        this.billingCycleOptions = {
            placeholder: 'Billing Cycle...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/billing-cycle/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data)

                    return {results: data.map((object) => {
                        return {
                            id: object.id,
                            text: object.name
                        }
                    }) }
                }
            }
        } // end billingCycleOptions

        this.routineTypeOptions = {
            placeholder: 'Routine Type...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/routine-type/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data)

                    return {results: data.map((object) => {
                        return {
                            id: object.id,
                            text: object.name
                        }
                    }) }
                }
            }
        } // end billingCycleOptions

    }
})

// prepare the VueRouter object and configurations
const routes = [
    {
        path: "/",
        component: RoutineRequestList
    },
    {
        name: "create",
        path: "/create",
        component: RoutineRequestCreate
    },
    {
        name: "outbox",
        path: "/outbox",
        component: RoutineRequestOutbox
    },
    {
        name: "outboxShow",
        path: "/outbox/:id",
        component: RoutineRequestOutboxShow
    },
    {
        name: "show",
        path: "/:id",
        component: RoutineRequestShow
    }
]

const router = new VueRouter({
    mode: 'hash',
    routes: routes
})

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router,
    render: (h) => h(RoutineRequestApp)
})
