/**
 * Created by lebohof on 12/27/2020.
 */

const bankService = {
    get(id) {
        axios.get(config.API_URL+'/bank/show/'+id)
    },
    fetchBanks(params) {
        return axios.get(config.API_URL+"/bank/list", {params: params})
    },
    saveBank(bank) {
        return axios.post(config.API_URL+'/bank/save', bank);
    },
    updateBank(id, bank) {
        return axios.put(config.API_URL+'/bank/update/'+id, bank);
    }
};

/**
 * The BankForm component
 */
const BankForm = {
    template: "#bank-form",
    props: ['bank'],
    methods: {
        onSubmit: function () {
            console.log("BankFormComp::save()")
            this.$emit('save', this.bank);
        },
        onUpdate: function () {
            console.log("BankFormComp::update()")
            this.$emit('save', this.bank);
        },
    }
};

var BankCreateComp = Vue.component('bank-create', {
    template: '#bank-create',
    data: function() {
        return {
            bank: {}
        }
    },
    methods: {
        save: function(bank) {
            let loader = overlayLoader.fullPageLoader();
            bankService.saveBank(bank).then((resp) => {
                console.log("object saved successfully: " + resp.data);
                this.$router.push({name: "home"});
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created: function() {
        let loader = overlayLoader.fullPageLoader();
        this.$axios.get(config.API_URL + "/bank/create").then( (resp) => {
            this.bank = resp.data
        }).finally(() => {
            loader.hide()
        });
    },
    components: {'bank-form': BankForm}
})

/**
 * The BankEdit component for editing an bank entry
 */
var BankEdit = Vue.component('bank-edit', {
    template:"#bank-edit",
    data: function() {
        return {
            bank: {
                bankName: null,
                bankCode: null
            }
        }
    },
    methods: {
        save: function (bank) {
            let loader = overlayLoader.fullPageLoader();
            console.info("saving the object: ", bank);

            bankService.updateBank(bank.id, bank).then((resp) => {
                console.info(resp.data);
                this.$router.push({name: "home"})
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created: function() {
        const id = this.$route.params.id;
        let loader = overlayLoader.fullPageLoader();

        if(id) {
            this.$axios.get(config.API_URL + "/bank/edit/"+id).then( (resp) => {
                this.bank = resp.data;
            }).catch( (err) => {
                console.log(err)
            }).finally(() => {
                loader.hide();
            });
        }
    },
    components: {'bank-form': BankForm}
});

/**
 * This components fetches the lisf of banks from the server and renders them to the UI
 */
var BankList = Vue.component('bank-list', {
    template: '#bank-list',
    data: function () {
        return {
            bankList: [],
            params: {}
        }
    },
    methods: {
        fetchBanks(params) {

            let loader = overlayLoader.fullPageLoader();

            bankService.fetchBanks(params).then((resp) => {
                this.bankList = resp.data;
            }).finally(() => {
                loader.hide();
            })
        },
        search() {
            this.fetchBanks(this.params)
        },
        refresh() {
            //clear the params
            this.params = {};
            this.fetchBanks(this.params);
        }
    },
    created () {
        this.fetchBanks(this.params);
    }
});

/**
 * The main application component
 */
var BankApp = Vue.component('bank-app', {
    template: "#bank-app",
    data: function() {
        return {
            title: "Banks"
        }
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: BankList},
    {path: "/create", name: 'create', component: BankCreateComp},
    {path: "/edit/:id", name: 'edit', component: BankEdit}
];

const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueRouter);

var vm = new Vue({
    el: '#app',
    router: router,
    template: "<bank-app></bank-app>"
});
