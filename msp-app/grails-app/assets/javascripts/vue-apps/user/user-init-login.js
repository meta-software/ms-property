/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            currentPassword: 'Current Password',
            password: 'New Password',
            confirmPassword: 'Confirm Password'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});


//Page application.
new Vue({
    el: '#main-app',
    data: {
        updatePasswordForm: {
            id: userId,
            currentPassword: null,
            password: null,
            confirmPassword: null
        }
    },
    methods: {

        updatePassword: function(event) {
            event.preventDefault();

            const vm = this;
            const actionUrl = this.$refs.updatePasswordForm.action;

            this.$validator.validate().then((result) => {
                if(result) {
                    vm.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });
                    this.$axios.put(actionUrl, vm.updatePasswordForm)
                        .then(function(response){
                            vm.$notify({
                                type: 'success',
                                title: 'Success',
                                content: 'Password updated successfully.'
                            });
                            vm.openModal = false;
                            vm.loader.hide();

                            window.location.assign(config.API_URL);
                        })
                        .catch(function(error){
                            vm.loader.hide();
                        });
                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });

        }
    },
    mounted: function() {
    }
});
