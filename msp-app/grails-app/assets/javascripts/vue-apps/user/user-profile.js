/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate

Vue.use(VeeValidate, {
    events: 'change'
});

//Page application.
new Vue({
    el: '#main-app',
    data: {
        openModal: false,
        updatePasswordForm: {
            id: userId,
            currentPassword: null,
            password: null,
            confirmPassword: null
        }
    },
    methods: {
        passwordModalCallback() {
            console.log('closing the update-password modal')
        },

        async updatePassword(event) {
            event.preventDefault();

            const actionUrl = this.$refs.updatePasswordForm.action;
            const loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
            });
            try {
                const resp = await axios.put(actionUrl, this.updatePasswordForm);
                iziToast.success({message: "Password changed successfully"});



                this.openModal = false;
            } finally {
                loader.hide();
            }
        }
    }
});
