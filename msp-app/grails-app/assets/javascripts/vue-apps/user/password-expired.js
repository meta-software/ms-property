/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            newPassword: 'New Password',
            confirmPassword: 'Confirm Password',
            currentPassword: 'Current Password'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change|blur',
    dictionary: dictionary
});

const PasswordExpiredApp = Vue.component("password-expired", {
    template:"#password-expired",
    data() {
        return {
            loader: {
                loading: false,
                fullPage: true
            },
            form: {
                username: username,
                currentPassword: '',
                newPassword: '',
                confirmPassword: ''
            }
        }
    },
    methods: {
        cancel() {
            window.location.assign(config.API_URL);
        },
        async updatePassword() {

            const result = await this.$validator.validate();
            if(result) {
                try {
                    this.loader.loading = true;
                    const resp = await axios.post(config.API_URL + "/user/update-expired", this.form);
                    iziToast.success({
                        message: "Password updated successfully.",
                        position: "topCenter",
                        overlay: true
                    });
                    window.location.assign(config.API_URL);
                } finally {
                    this.loader.loading = false;
                }
            }
        }
    },
    computed: {
        passwordsMatch() {
            // loop over all contents of the fields object and check if they exist and valid.
            return Object.keys(this.fields).every(field => {
                return this.fields[field] && this.fields[field].valid;
            });
        }
    }
});

jQuery(document).ready(function() {

//Page application.
    new Vue({
        el: '#app',
        render: (h) => h(PasswordExpiredApp)
    });

});
