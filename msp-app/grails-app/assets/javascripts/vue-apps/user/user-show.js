/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

// lease form component
const leaseFormComp = Vue.component("lease-form", {
    template: "#lease-form",
    data: function () {
        return {
            loader: null,
            openModal: false,
            loadingSubmit: false,
            leaseForm: {
                id: null,
                tenant: {
                    id: tenantId,
                },
                property: null,
                rentalUnit: null,
                leaseStartDate: null,
                leaseExpiryDate: null
            },
            rentalUnitSelect: {
                disabled: true,
                options: []
            },
            rentalUnitOptions: []
        };
    },
    mounted: function() {
        this.rentalUnitOptions = {
            placeholder: 'Select Unit...',
            ajax: {
                url: config.API_URL + '/rental-unit/index',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    accepts: 'application/json'
                },
                delay: 300,
                data: function (params) {
                    let query = {
                        q: params.term
                    };
                    return query;
                }
            }
        };

        this.propertyOptions = {
            placeholder: 'Property...',
            ajax: {
                url: config.API_URL + '/property/index',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    accepts: 'application/json'
                },
                delay: 300,
                data: function (params) {
                    let query = {
                        q: params.term
                    };
                    // Query parameters will be ?search=[term]&type=public
                    return query;
                }
            }
        };

        const vm = this;
        //prepare the datepickers: this is a hack to make it work.
        //@todo: prepare the datepicker component/directive
        $("[name=leaseStartDate]").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('change', function(e){
            vm.leaseForm.leaseStartDate = $(this).val();
        });

        //prepare the datepickers: this is a hack to make it work.
        //@todo: prepare the datepicker component/directive
        $("[name=leaseExpiryDate]").datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        }).on('change', function(e){
            vm.leaseForm.leaseExpiryDate = $(this).val();
        });

        $("#rentalUnitItem").on('change', function(){
            vm.leaseForm.rentalUnit = $(this).val();
        });
        $("#leaseType").on('change', function(){
            vm.leaseForm.leaseType = $(this).val();
        });
    },
    methods: {
        clearForm: function() {
            this.leaseForm = {
                id: null,
                    tenant: {
                    id: tenantId,
                },
                property: null,
                    rentalUnit: null,
                    leaseStartDate: null,
                    leaseExpiryDate: null
            };
            this.rentalUnitSelect = {
                disabled: true,
                    options: []
            };
            this.rentalUnitOptions = [];
        },
        saveLease: function() {

            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });

            const vm = this;

            console.log(this.leaseForm);

            this.$validator.validate().then(function (result) {

                if(result){
                    vm.$axios.post(config.API_URL + '/lease/save', vm.leaseForm).then(function (response) {
                        vm.loader.hide();

                        vm.lease = response.data;

                        vm.$alert({
                            content: "lease " + vm.lease.leaseNumber + " created successfully.",

                        }, function () {
                            vm.openModal = false; //close the modal
                            window.location.assign(response.data['_links']['self']['href']);
                        })
                    }).catch(function (error) {
                        vm.loader.hide();

                        vm.$notify({
                            type: 'danger',
                            title: 'Error',
                            content: "error while saving the lease"
                        });
                    })
                } else {
                    vm.$notify({
                        type: 'danger',
                        title: 'Validation Error',
                        content: 'invalid form data submitted.'
                    });
                    vm.loader.hide();
                }
            })
        },
        onSelectProperty: function() {
            let selected = this.leaseForm.property;
            console.log(selected);
        },
        callback: function (msg) {
            //this.$notify(`Modal dismissed with msg '${msg}'.`);
        }
    }
});

//Page application.
new Vue({
    el: '#main-app',
    data: {
        tenantId: tenantId,
        tenant: {
            id: tenantId
        },
        rentalUnitOptions: [],

    },
    methods: {
        terminateTenant: function() {
            const vm = this;

            this.tenant.terminated = true;

            this.$axios.put(config.API_URL + "/tenant/terminate/"+this.tenantId, this.tenant).then(function(response){
                console.log("successfully terminated  tenant id " + vm.tenantId);
                location.reload(true);
            }).catch(function(error){
                console.log(error.data);
            });
        },
    },
    mounted: function() {
        this.rentalUnitOptions = {
            placeholder: 'Select Unit...',
            ajax: {
                url: config.API_URL + '/rental-unit/index',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    accepts: 'application/json'
                },
                delay: 300,
                data: function (params) {
                    let query = {
                        q: params.term
                    };
                    return query;
                }
            }
        };

    }
});
