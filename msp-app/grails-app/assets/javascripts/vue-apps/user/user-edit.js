/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate

const dictionary = {
    en: {
        attributes: {
            password: 'Password',
            confirmPassword: 'Password'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change|blur',
    dictionary: dictionary
});

const ChangePasswordForm = Vue.component("change-password-form", {
    template: "#change-password-form",
    data() {
        return {
            openModal: false,
            updatePasswordForm: {
                id: userId,
                currentPassword: null,
                password: null,
                confirmPassword: null
            }
        }
    },
    methods: {
        passwordModalCallback() {
            console.log('closing the update-password modal')
        },

        async updatePassword(event) {
            event.preventDefault();

            const vm = this;
            const actionUrl = config.API_URL+"/user/update-password"

            this.$validator.validate().then(async (result) => {

                if(result) {
                    const loader = overlayLoader.fullPageLoader();

                    try {
                        const resp = await axios.put(actionUrl, vm.updatePasswordForm);
                        iziToast.success({position: 'top-right', message: 'User Password changed successfully.', title: 'Success'});
                        this.openModal = false;
                    } finally {
                        loader.hide();
                    }

                } else {
                    // do nothing for now.
                }
            });
        }
    },
    computed: {
        formValid() {
            // loop over all contents of the fields object and check if they exist and valid.
            return !Object.keys(this.fields).some(key => this.fields[key].invalid);
        }
    }
});

//Page application.
const app = new Vue({
    data: {
    }
});

$(document).ready(function() {
  app.$mount('#main-app');
})