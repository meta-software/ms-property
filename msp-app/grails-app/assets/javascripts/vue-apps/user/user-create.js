/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            username: 'Username',
            password: 'Password',
            employeeId: 'Employee ID',
            userType: 'User Type',
            authorities: 'Authorities',
            firstName: 'First Name',
            lastName: 'Last Name',
            emailAddress: 'Email Address',
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

jQuery(document).ready(function() {
    //Page application.
    const app = new Vue({
        el: '#main-app',
        data: {
            userForm: {
                username: undefined,
                password: undefined,
                firstName: undefined,
                lastName: undefined,
                emailAddress: undefined,
                employeeId: undefined,
                userType: undefined,
                enabled: true,
                passwordExpired: false,
                accountLocked: false,
                accountExpired: false,
                authorities: []
            },
            formSubmitted: false
        },
        computed: {
            formValid() {
                // loop over all contents of the fields object and check if they exist and valid.
                return !Object.keys(this.fields).some(key => this.fields[key].invalid);
            }
        },
        methods: {
            async onSaveUser() {
                const loader = overlayLoader.fullPageLoader();
                try {
                    const result = await this.$validator.validate();
                    this.formSubmitted = true;

                    if(result) {
                        const payload = {...this.userForm, accountExpired: false, passwordExpired: false};
                        const resp = await axios.post(`${config.API_URL}/user/save`, payload);
                        iziToast.success({message: "User Request created successfully"});
                        window.location.assign(config.API_URL+"/user/index");
                    }
                } catch(err) {
                  iziToast.error({message: "An error occurred while creating user."})
                } finally {
                    loader.hide();
                }
            }
        }
    });
});