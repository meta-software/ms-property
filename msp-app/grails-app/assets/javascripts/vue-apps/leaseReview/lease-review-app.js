/**
 * Created by lebohof on 12/29/2019.
 */
//= require global/plugins/lodash.min

const API_URL = config.API_URL + "/api/v1/lease-review";

const leaseReviewService = {
    list: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined)
            queryParams.params = args;

        return axios.get(API_URL, queryParams);
    },
    
    ignore: function(id) {
        return axios.put(API_URL+'/ignore/'+id);
    },

    get: function(id) {
        return axios.get(API_URL+"/show/"+id)
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const LeaseReviewApp = Vue.component("app", {
    template: "#lease-review-app"
});

Vue.component('lease-review-search', {
    template: '#lease-review-search',
    data() {
        return {
            query: {
                jobName: ''
            }
        }
    },
    methods: {
        requestSearch() {
            this.$emit('search-request', this.query);
            this.$router.replace({path: "/", query: this.query});
        }
    }
});

const LeaseReviewList = Vue.component("lease-review-list", {
    template: '#lease-review-list',
    data() {
        return {
            leaseReviews: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            params: {}
        }
    },
    created() {
        this.fetchLeaseReviews(this.params);
    },
    methods: {
        _search() {
            this.search();
        },
        reset() {
            this.params = {};
            this.search();
        },
        search() {
            this.fetchLeaseReviews(this.params);
        },
        fetchLeaseReviews(params) {
            this.loader.isLoading = true;

            leaseReviewService.list(params).then((response) => {
                this.leaseReviews = response.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    }
});

const LeaseReviewShow = Vue.component("lease-review-show", {
    template: "#lease-review-show",
    data() {
        return {
            loader: {
                isLoading: true,
                fullPage: false
            },
            leaseReview: {
                id: null,
                lease: {},
                leaseReviewType: {}
            }
        }
    },

    created() {
        this.getEntity();
    },

    methods: {
        review() {
            this.loader.fullPage = true;
            this.loader.isLoading = true;
            window.location.assign(config.API_URL+'/lease/review/'+this.leaseReview.lease.id);
        },
        ignore() {
            alert(this.id)
        },
        processed() {
            alert(this.id)
        },
        getEntity() {
            this.loader.isLoading = true;

            leaseReviewService.get(this.id).then((response) => {
                this.leaseReview = response.data
            }).finally(() => {
                this.loader.isLoading = false
            });
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: LeaseReviewList
    },
    {
        name: "show",
        path: "/show/:id",
        component: LeaseReviewShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(LeaseReviewApp)
});
