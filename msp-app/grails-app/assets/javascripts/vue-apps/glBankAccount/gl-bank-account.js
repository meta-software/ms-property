/**
 * Created by lebohof on 09/19/2021.
 */
//= require global/plugins/vue/vee-validate
//= require vue-apps/components/currency-select
//= require vue-apps/components/gl-account-select

Vue.use(VeeValidate, {
    events: 'change'
});

const APP_URL = config.API_URL + '/gl-bank-account';

//the generalLedgerService class
const glBankAccountService = {
    list() {
        let params = {'max': 20}
        return axios.get(APP_URL+'/list', {params: params});
    },

    get(id) {
        return axios.get(APP_URL + `/show/${id}`)
    },

    save(glBankAccount) {
        return axios.post(APP_URL + '/save', glBankAccount);
    },

    update(id, glBankAccount) {
        const url = APP_URL+`/update/${id}`;
        return axios.put(url, glBankAccount);
    }
};

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle']
});

/**
 * The GlBankAccountCreate component
 */
const GlBankAccountCreate = Vue.component('gl-bank-account-create', {
    template: "#gl-bank-account-create",
    data() {
        return {
            glBankAccount: {},
            bankOptions: {
                placeholder: 'Bank...'
            },
            loader: {
                isLoading: false,
                fullPage: true
            }
        }
    },
    methods: {
        onSubmit: function(){
            this.loader.isLoading = true;
            glBankAccountService.save(this.glBankAccount).then((resp) => {
                this.$router.push("/");
                iziToast.success({message: 'GL Bank Account added successfully.'});
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        onCancel() {

            const vm = this;

            vm.$confirm({
                title: "Cancel !!!",
                size: 'md',
                content: "Are you sure you want to proceed with cancel?"
            }).then(function(){
                vm.$router.push("/")
            });
        },

        prepareBankOptions() {
            this.$axios.get(config.API_URL + "/bank/list-options").then((resp) => {
                let bankOptions = resp.data.map((data) => {
                    return {
                        id: data.id,
                        text: data.bankName
                    }
                });

                this.bankOptions = {
                    placeholder: 'Bank...',
                    allowClear: false,
                    data: bankOptions
                }
            });
        },
        prepareCurrencyOptions() {
            this.$axios.get(config.API_URL + "/bank/list-options").then((resp) => {
                let bankOptions = resp.data.map((data) => {
                    return {
                        id: data.id,
                        text: data.bankName
                    }
                });

                this.bankOptions = {
                    placeholder: 'Bank...',
                    allowClear: false,
                    data: bankOptions
                }
            });
        }
    },
    computed: {
        valid() {
            return true;
        }
    },
    created() {
        this.prepareBankOptions();
    }
});

Vue.component('page-bar', {
    template: '#page-bar'
});

/**
 * The GlBankAccountCreate component
 */
const GlBankAccountList = Vue.component('gl-bank-account-list', {
    template: "#gl-bank-account-list",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            params: {},
            glBankAccounts: []
        }
    },
    methods: {
        fetch() {
            this.loader.isLoading = true;
            glBankAccountService.list().then((resp) => {
                this.glBankAccounts = resp.data;
            }).finally((resp) => {
                this.loader.isLoading = false;
            })
        }
    },
    created() {
        this.fetch();
    }
});

/**
 * The GlBankAccountCreate component
 */
const GlBankAccountEdit = Vue.component('gl-bank-account-edit', {
    template: "#gl-bank-account-edit",
    props: ['glBankAccount'],
    methods: {
        onSubmit: function(){
            this.$emit('save', this.glBankAccount);
        },
        onUpdate: function() {
            console.log("GeneralLedgerFormComp::update()")
            this.$emit('update', this.glBankAccount);
        },
    },
    created() {
        alert("Fetch the object from the backend.");
    }
});

/**
 * The GlBankAccountShow component for viewing a ledger account
 */
const GlBankAccountShow = Vue.component('gl-bank-account-show', {
    template:"#gl-bank-account-show",
    components: {},
    data: function() {
        return {
            loader: {
                isLoading: false
            },
            glBankAccount: {
                generalLedger: {}
            }
        }
    },
    methods: {
        get: function () {
            this.loader.isLoading = true;
            glBankAccountService.get(this.id).then((response) => {
                this.glBankAccount = response.data;
            }).then((error) => {
                //error
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    computed: {
        id: function() {
            return this.$route.params.id;
        }
    },
    created: function() {
        this.get();
    }
});

/**
 * The main application component
 */
const GlBankAccountApp = Vue.component('gl-bank-account-app', {
    template: "#gl-bank-account-app",
    data: function() {
        return {
            title: "GL Bank Account"
        }
    },
    created() {}
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'home', component: GlBankAccountList},
    {path: "/create", name: 'create', component: GlBankAccountCreate},
    {path: "/edit:/id", name: 'edit', component: GlBankAccountEdit},
    {path: "/show/:id", name: 'show', component: GlBankAccountShow}
];

const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueRouter);

const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(GlBankAccountApp)
});
