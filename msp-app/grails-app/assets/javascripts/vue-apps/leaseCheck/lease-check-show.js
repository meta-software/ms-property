/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker
//= require vue-apps/maker-options

Vue.use(VeeValidate, {
    events: 'change'
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app'
});
