/**
 * Created by lebohof on 12/29/2019.
 */
const API_URL = config.API_URL + "/job-schedule";

const jobScheduleService = {
    list: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined)
            queryParams.params = args;

        return axios.get(API_URL, queryParams);
    },

    get: function(id) {
        return axios.get(API_URL+"/show/"+id)
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const JobScheduleApp = Vue.component("app", {
    template: "#main-app"
});

const JobScheduleList = Vue.component("job-schedule-list", {
    template: '#job-schedule-list',
    data() {
        return {
            jobSchedules: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            params: {}
        }
    },
    created() {
        this.getList({})
    },
    methods: {
        getList() {
            this.loader.isLoading = true;

            jobScheduleService.list(this.params).then((response) => {
                this.jobSchedules = response.data;
            }).finally(() => {
                this.loader.isLoading = false;
            });
        },
        search() {
            this.getList();
        },
        reset() {
            this.params = {}
            this.getList();
        }
    }
});

const JobScheduleShow = Vue.component("job-schedule-show", {
    template: "#job-schedule-show",
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            jobSchedule: {
                id: "",
                jobType: {}
            }
        }
    },

    created() {
        this.getEntity();
    },

    methods: {
        getEntity() {
            this.loader.isLoading = true;

            jobScheduleService.get(this.id).then((response) => {
                this.jobSchedule = response.data
            }).finally(() => {
                this.loader.isLoading = false
            });
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: JobScheduleList
    },
    {
        name: "show",
        path: "/show/:id",
        component: JobScheduleShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(JobScheduleApp)
});
