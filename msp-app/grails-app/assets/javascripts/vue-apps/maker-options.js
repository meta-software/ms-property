/**
 * Created by lebohof on 11/11/2018.
 */
const makerOptionsTemplate = `
<div class="row maker-checker">
	<div class="col-md-12 col-lg-12 col-xs-12">
		<div v-if="status=='PENDING'" class="form-actions">
			<button type="button" @click="cancel()" class="btn btn-sm yellow-gold"><i class="fa fa-times"></i> Cancel</button>
		</div>
	</div>	

	<div class="col-md-12 col-lg-12 col-xs-12">
        <span v-if="status=='CANCELED'" class="label label-sm label-danger" ><i class="fa fa-times"></i> {{status}} </span>
        <span v-if="status=='APPROVED'" class="label label-sm label-primary" ><i class="fa fa-check"></i> {{status}} </span>
        <span v-if="status=='CANCELLED'" class="label label-sm label-warning" ><i class="fa fa-check"></i> {{status}} </span>
	</div>	
	

	<modal v-model="openCancelModal" id="mc-cancel" >
		<span slot="title"><i class="fa fa-info"></i> Cancel Reason ?</span>

		<form>
			<!-- BEGIN FORM BODY-->
			<div class="form-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Cancel Comment</label>
							<textarea v-validate="'required'" v-model="makerChecker.comment" name="comment" class="form-control"></textarea>
							<span class="has-error help-block">{{ errors.first('comment') }}</span>
						</div>
					</div>
				</div>
			</div>
			<!-- END FORM BODY-->
		</form>

		<div slot="footer">
			<btn @click="openCancelModal=false"><i class="fa fa-times"></i> Cancel</btn>
			<btn @click="cancelEntity" class="blue-hoki" ><i class="fa fa-save"></i> Submit</btn>
		</div>
	</modal>
</div>
`;

Vue.component('maker-options', {
    props: {

        status: {
            required: true
        },
        controller: {
            required: true
        },
        itemId: {
            required: true
        },
        reload: {
            default: true
        }
    },
    template: makerOptionsTemplate,
    data: function() {
        return {
            openCancelModal: false,
            makerChecker: {
                id: null,
                comment: ''
            }
        }
    },
    created: function(){
    },
    watch: {
        itemId: function(itemId) {
            this.makerChecker.id = itemId;
        }
    },
    methods: {
        cancel: function(event){
            this.openCancelModal = true;
        },
        startLoader: function(){
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
            });
        },
        stopLoader: function() {
            this.loader.hide();
        },
        cancelEntity: function(event){

            const vm = this;
            const url = config.API_URL + '/' + vm.controller + '/cancel/' + vm.itemId;

            vm.startLoader();
            vm.$axios.put(url, this.makerChecker).then( (resp) => {

                this.openCancelModal = false;

                if(vm.reload === false){
                    vm.stopLoader();
                    vm.$emit('cancelled', this.makerChecker);
                }

                iziToast.success({
                    title: "Successful",
                    message: "Item cancellation was successful",
                    overlay: true,
                    onClosing: () => {
                        if(vm.reload) {
                            window.location.reload(true);
                        }
                    }
                });

            }).finally( () => {
                vm.stopLoader();
            });

        }
    }
});
