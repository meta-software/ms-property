/**
 * Created by lebohof on 02/02/2020.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker

const leaseTerminateRequestInboxService = {

    API_URL: config.API_URL + "/lease-terminate-request-inbox",

    list: function(params) {
        return axios.get(this.API_URL, {params: params});
    },

    get: function(id) {
        const url = this.API_URL+"/show/"+id
        return axios.get(url)
    },

    create: function(leaseTerminateRequest) {
        const url = this.API_URL + "/create-request"
        return axios.post(url, leaseTerminateRequest)
    },

    listOutbox() {
        const url = config.API_URL + "/lease-terminate-request-outbox"
        return axios.get(url)
    },

    getOutboxItem(id) {
        const url = config.API_URL + `/lease-terminate-request-outbox/show/${id}`
        return axios.get(url)
    }
};

Vue.use(VeeValidate, {
    events: 'change'
});

const PageBar = Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const LeaseTerminateRequestInboxApp = Vue.component("app", {
    template: "#main-app",
    data: function() {
        return {
        }
    }
});

const LeaseTerminateRequestInboxList = Vue.component("lease-terminate-request-inbox-list", {
    template: '#lease-terminate-request-inbox-list',
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            leaseTerminateRequestInboxes: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            params: {}
        }
    },

    created() {
        this.fetchRequests()
    },

    methods: {
        search() {
            this.fetchRequests();
        },
        reset() {
            this.params = {};
            this.fetchRequests();
        },
        fetchRequests() {
            this.loader.isLoading = true;

            leaseTerminateRequestInboxService.list(this.params).then((resp) => {
                this.leaseTerminateRequestInboxes = resp.data;
            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    }
});

const LeaseTerminateRequestInboxShow = {
    template: "#lease-terminate-request-inbox-show",
    data() {
        return {
            loader: {
                isLoading: false,
                isFullPage: false
            },
            leaseTerminateRequestInbox: null
        }
    },
    created() {
        this.fetchLeaseTerminateRequest();
    },
    methods: {
        fetchLeaseTerminateRequest() {
            this.loader.isLoading = true;
            leaseTerminateRequestInboxService.get(this.id).then((resp) => {
                this.leaseTerminateRequestInbox = resp.data;
            }).catch((err) => {
                iziToast.error({
                    message: "Error occured while retrieving record",
                    title: "Error !",
                    overlay: true
                });
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    computed: {
        id() {
            return this.$route.params.id;
        },
        lease() {
            return this.leaseTerminateRequestInbox.lease;
        }
    }

};

// prepare the VueRouter object and configurations
const routes = [
    {
        path: "/",
        component: LeaseTerminateRequestInboxList
    },
    {
        name: "inboxShow",
        path: "/show/:id",
        component: LeaseTerminateRequestInboxShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
})

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router,
    render: (h) => h(LeaseTerminateRequestInboxApp)
})
