/**
 * Created by lebohof on 03/20/2019.
 */
//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

//lease-show main application.
const vm = new Vue({
    el: '#main-app',
    data: {
        leaseRenewalRequestId: leaseRenewalRequestId,
        leaseRenewalRequest: {
            id: leaseRenewalRequestId
        }
    },
    methods: {
    },
    mounted: function() {
    }
});
