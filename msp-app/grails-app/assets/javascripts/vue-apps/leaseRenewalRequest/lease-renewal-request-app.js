/**
 * Created by lebohof on 02/02/2020.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-options
    
const statusClass = function(status) {

    let labelClass = 'label-default'

    switch(status) {
        case 'success':
            labelClass = 'label-success'
            break

        case 'error':
            labelClass = 'label-danger'
            break

        case 'pending':
        default:
            labelClass = 'label-default'
            break
    }

    return labelClass
};


const leaseRenewalRequestService = {

    API_URL: config.API_URL + "/lease-renewal-request",

    list: function() {
        return axios.get(this.API_URL)
    },

    get: function(id) {
        const url = this.API_URL+"/show/"+id
        return axios.get(url)
    },

    create: function(leaseRenewalRequest) {
        const url = this.API_URL + "/create-request"
        return axios.post(url, leaseRenewalRequest)
    },

    listOutbox() {
        const url = config.API_URL + "/lease-renewal-request-outbox"
        return axios.get(url)
    },

    getOutboxItem(id) {
        const url = config.API_URL + `/lease-renewal-request-outbox/show/${id}`
        return axios.get(url)
    }
};

Vue.use(VeeValidate, {
    events: 'change'
});

const PageBar = Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const LeaseRenewalRequestApp = Vue.component("app", {
    template: "#main-app",
    data: function() {
        return {
        }
    }
});

const LeaseRenewalRequestList = Vue.component("lease-renewal-request-list", {
    template: '#lease-renewal-request-list',
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            leaseRenewalRequests: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                columns: [
                    {
                        title: 'Request #',
                        field: 'id'
                    },
                    {
                        title: 'Action Reason',
                        field: 'actionReason'
                    },
                    {
                        title: 'Lease No',
                        field: 'leaseNumber'
                    },
                    {
                        title: 'Tenant',
                        field: 'tenant'
                    },
                    {
                        title: 'Status',
                        field: 'checkStatus'
                    },
                    {
                        title: 'Expire Date',
                        field: 'expireDate'
                    },
                    {
                        title: 'Requested By',
                        field: 'requestedBy'
                    },
                    {
                        title: 'Actions'
                    }
                ],
                data: [],
                options: {
                    onClickRow: (row) => {this.$router.push("/"+row.id)},
                    toolbar: '#toolbar-list',
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    singleSelect: true,
                    height: 350,
                    pagination: true,
                    sortable: true,
                    classes: "table table-bordered table-striped table-hover table-borderless table-condensed"
                }
            }
        }
    },

    created() {
        this.fetchRequests()
    },

    methods: {

        fetchRequests() {
            this.loader.isLoading = true;

            leaseRenewalRequestService.list().then((response) => {
                this.leaseRenewalRequests = response.data;

                this.table.data = this.leaseRenewalRequests.map((leaseRenewal) => {
                    return {
                        id: leaseRenewal.id,
                        leaseNumber: leaseRenewal.lease.leaseNumber,
                        tenant: leaseRenewal.lease.tenant,
                        checkStatus: leaseRenewal.checkStatus,
                        requestedBy: leaseRenewal.maker.fullName
                    }
                });
            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    }
});

const LeaseRenewalRequestOutbox = Vue.component("lease-renewal-request-outbox", {
    template: '#lease-renewal-request-outbox',
    data() {
        return {
            leaseRenewalRequestChecks: [],
            loader: {
                isLoading: true,
                fullPage: false
            }
        }
    },

    created() {
        this.getOutboxList()
    },

    methods: {
        getOutboxList() {
            leaseRenewalRequestService.listOutbox().then((response) => {
                this.leaseRenewalRequestChecks = response.data
            }).finally(() => {
                this.loader.isLoading = false
            })
        },

        statusClass(status) {
            return statusClass(status)
            statusClass(status)
        }
    }
})

Vue.component('lease-renewal-transactions', {
    template: "#lease-renewal-transactions",
    components: {
        'BootstrapTable': BootstrapTable
    },
    props: {
        leaseRenewalRequestId: {
            required: true
        }
    },
    data() {
        return {
            transactions: {},
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                data: [],
                columns: [
                    {
                        title: 'Txn Type',
                        field: 'transactionType'
                    },
                    {
                        title: 'Property',
                        field: 'property'
                    },
                    {
                        title: 'Rental Unit',
                        field: 'rentalUnit'
                    },
                    {
                        title: 'Credit Acc',
                        field: 'accountNumberCr'
                    },
                    {
                        title: 'Reference',
                        field: 'transactionReference'
                    },
                    {
                        title: 'Amount',
                        field: 'transactionAmount'
                    }
                ],
                options: {
                    toolbar: '#toolbar',
                    IdField: 'id',
                    showColumns: false,
                    search: true,
                    showSearchClearButton: true,
                    searchOnEnterKey: true,
                    showButtonIcons: true,
                    showSearchButton: true,
                    height: 350,
                    pagination: true,
                    sortable: true,
                    classes: "table table-bordered table-striped table-hover table-borderless table-condensed"
                }
            }
        }

    },

    created() {
        this.getList()
    },

    methods: {
        getList() {
            this.loader.isLoading = true
            leaseRenewalRequestService.listLeaseRenewalTransactions(this.leaseRenewalRequestId).then((response) => {
                this.transactions = response.data;

                this.table.data = this.transactions.map((txn) => {
                    return {
                        transactionType: txn.transactionType.name,
                        accountNumberCr: txn.accountNumberCr,
                        property: (txn.property ? txn.property.name : '' ),
                        rentalUnit: txn.rentalUnit.name,
                        transactionReference: txn.transactionReference,
                        transactionAmount: txn.transactionAmount
                    }
                })

            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    }

})

const LeaseRenewalRequestShow = Vue.component("lease-renewal-request-show", {
    template: "#lease-renewal-request-show",
    data() {
        return {
            leaseRenewalRequest: {
                id: "",
                leaseRenewalType: {},
                maker: {},
                checker: {},
                billingCycle: {}
            },
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },

    created() {
        this.getEntity()
    },

    methods: {
        getEntity() {
            this.loader.isLoading = true

            leaseRenewalRequestService.get(this.id).then((response) => {
                this.leaseRenewalRequest = response.data
            }).finally(() => {
                this.loader.isLoading = false
            })

        },

        statusClass(status) {
            return statusClass(status)
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
})

const LeaseRenewalRequestOutboxShow = Vue.component("lease-renewal-outbox-show", {
    template: "#lease-renewal-outbox-show",
    data() {
        return {
            leaseRenewalRequestCheck: {
                entity: {
                    leaseRenewalType: {},
                    billingCycle: {}
                },
                maker: {},
                checker: {}
            },
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },

    created() {
        this.getEntity()
    },

    methods: {
        getEntity: function() {
            this.loader.isLoading = true

            leaseRenewalRequestService.getOutboxItem(this.id).then((response) => {
                this.leaseRenewalRequestCheck = response.data
            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
})

const LeaseRenewalRequestCreate  = Vue.component('lease-renewal-request-create', {
    template: "#lease-renewal-request-create",
    data() {
        return {
            billingCycleOptions: {
                placeholder: 'Billing Cycle...'
            },
            leaseRenewalTypeOptions: {
                placeholder: 'LeaseRenewal Type...'
            },
            leaseRenewalRequest: {
                leaseRenewalType: {},
                billingCycle: {}
            }
        }
    },

    methods: {
        onCancelCreate() {
            console.log("cancel the creation process.")
            this.$router.go(-1)
        },
        onSaveRequest() {
            leaseRenewalRequestService.create(this.leaseRenewalRequest).then((response) => {
                console.log("leaseRenewal request created successfully")
                this.$router.replace("/outbox")
            }).catch((error) => {
                console.error("error occurred during saving of leaseRenewalRequest: ", error)
            })
        }
    },

    created() {

    },

    mounted() {
        const vm = this

        this.billingCycleOptions = {
            placeholder: 'Billing Cycle...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/billing-cycle/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data)

                    return {results: data.map((object) => {
                            return {
                                id: object.id,
                                text: object.name
                            }
                        }) }
                }
            }
        } // end billingCycleOptions

        this.leaseRenewalTypeOptions = {
            placeholder: 'LeaseRenewal Type...',
            ajax: {
                delay: 350,
                url: config.API_URL + '/lease-renewal-type/list-options',
                dataType: 'json',
                processResults: function(data) {
                    console.log(data)

                    return {results: data.map((object) => {
                            return {
                                id: object.id,
                                text: object.name
                            }
                        }) }
                }
            }
        } // end billingCycleOptions

    }
})

// prepare the VueRouter object and configurations
const routes = [
    {
        path: "/",
        component: LeaseRenewalRequestList
    },
    {
        name: "create",
        path: "/create",
        component: LeaseRenewalRequestCreate
    },
    {
        name: "outbox",
        path: "/outbox",
        component: LeaseRenewalRequestOutbox
    },
    {
        name: "outboxShOw",
        path: "/outbox/:id",
        component: LeaseRenewalRequestOutboxShow
    },
    {
        name: "show",
        path: "/:id",
        component: LeaseRenewalRequestShow
    }
]

const router = new VueRouter({
    mode: 'hash',
    routes: routes
})

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router,
    render: (h) => h(LeaseRenewalRequestApp)
})
