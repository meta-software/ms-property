/**
 * Created by lebohof on 02/02/2020.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/vuex/vuex
//= require vue-apps/maker-checker

const leaseRenewalRequestInboxService = {

    API_URL: config.API_URL + "/lease-renewal-request-inbox",

    list: function(params) {
        return axios.get(this.API_URL, {params: params})
    },

    get: function(id) {
        const url = this.API_URL+"/show/"+id
        return axios.get(url)
    },

    create: function(leaseRenewalRequest) {
        const url = this.API_URL + "/create-request"
        return axios.post(url, leaseRenewalRequest)
    },

    listOutbox(params) {
        const url = config.API_URL + "/lease-renewal-request-outbox"
        return axios.get(url, {params: params})
    },

    getOutboxItem(id) {
        const url = config.API_URL + `/lease-renewal-request-outbox/show/${id}`
        return axios.get(url)
    }
};

Vue.use(VeeValidate, {
    events: 'change'
});

const PageBar = Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const LeaseRenewalRequestInboxApp = Vue.component("app", {
    template: "#main-app",
    data: function() {
        return {
        }
    }
});

const LeaseRenewalRequestInboxList = Vue.component("lease-renewal-request-inbox-list", {
    template: '#lease-renewal-request-inbox-list',
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            leaseRenewalRequestInboxes: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            params: {}
        }
    },

    created() {
        this.fetchRequests()
    },

    methods: {
        search() {
            this.fetchRequests();
        },
        reset() {
            this.params = {}
            this.fetchRequests()
        },
        fetchRequests() {
            this.loader.isLoading = true;

            leaseRenewalRequestInboxService.list(this.params).then((response) => {
                this.leaseRenewalRequestInboxes = response.data;
            }).finally(() => {
                this.loader.isLoading = false
            })
        }
    }
});

const LeaseRenewalRequestInboxShow = {
    template: "#lease-renewal-request-inbox-show",
    data() {
        return {
            loader: {
                isLoading: false,
                isFullPage: false
            },
            leaseRenewalRequestInbox: null
        }
    },
    created() {
        this.fetchLeaseRenewalRequest();
    },
    methods: {
        fetchLeaseRenewalRequest() {
            this.loader.isLoading = true;
            leaseRenewalRequestInboxService.get(this.id).then((resp) => {
                this.leaseRenewalRequestInbox = resp.data;
            }).catch((err) => {
                iziToast.error({
                    message: "Error occured while retrieving record",
                    title: "Error !",
                    overlay: true
                });
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    computed: {
        id() {
            return this.$route.params.id;
        },
        lease() {
            return this.leaseRenewalRequestInbox.lease;
        }
    }

};

// prepare the VueRouter object and configurations
const routes = [
    {
        path: "/",
        component: LeaseRenewalRequestInboxList
    },
    {
        name: "inboxShow",
        path: "/show/:id",
        component: LeaseRenewalRequestInboxShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
})

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router,
    render: (h) => h(LeaseRenewalRequestInboxApp)
})
