/**
 * Created by lebohof on 8/22/2018.
 */

//Page application.
new Vue({
    el: '#main-app',
    data: function() {
        return {
            selectedRoleIdx: '',
            selectedRole: {},
            userRoles: []
        }
    },
    methods: {
        onSelectRole(event) {
            this.selectedRole = this.userRoles[this.selectedRoleIdx];
        },
        getRoles: function() {
            const vm = this;
            this.$axios.get(config.API_URL + "/sec-role/list/").then(function(response){
                vm.userRoles = response.data;
//                alert("finisehd loading, " + this.userRoles.length)
            }).catch(function(error){
                console.log(error.data);
            });
        },
    },
    created: function() {
        this.getRoles();
    }
});
