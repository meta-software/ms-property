
/**
 * Created by lebohof on 8/22/2018.
 */

//Setup the plugins.
Vue.use(VueRouter);

// prepare the tenant Services
const tenantService = function () {

    this.get = function(id, success, error) {
        Vue.$axios.get(config.API_URL + "/tenant/show/"+id).then(function(response){

            success(response);
         }).catch(function(response){
            console.log('error occured: ', response);
            error(response);
        });
    };

    this.save = function(tenant, successCallback, errorCallback) {
        if(tenant.id) {
            Vue.$axios.put(config.API_URL + "/tenant/update/"+id).then(function(response){
                //console.log('request response: ', response);

                successCallback(response);
            }).catch(function(response){
                console.log('error request response : ', response);

                errorCallback(response);
            })
        } else {

        }
    }
}();

//The tenant list component
const TenantListComp = Vue.component('tenant-list-comp', {
    template: '#tenant-list-comp',
    data: function () {
        return {
            tenantList: []
        }
    },
    created: function () {
        this.$axios.get(config.API_URL + "/tenant/index")
            .then((response) => {
                if (response.data) {
                    this.tenantList = response.data;
                }
            }).catch ( (error) => {
                console.error("error response returned: " + error)
            });
    }
});

// The tenant edit component
const TenantEditComp = Vue.component('tenant-edit-comp', {
    template: "#tenant-edit-comp",
    data: function() {
        return {
            tenant: {}
        }
    },
    methods: {
        initComponent: function() {
            const id = this.$route.params.id;

            //retrieve the
            this.$http.get(config.API_URL + "/tenant/edit/"+id).then(function(response){
                console.infO("response: ", response)
                this.tenant = response.data;
            },function(error){
                console.error("error occured: ", error);
            });
        }
    },
    created: function() {
        this.initComponent();
    }
});

// The tenant show component
const TenantShowComp = Vue.component('tenant-show-comp', {
    template: "#tenant-show-comp",
    data: function() {
       return {
           tenant: {
               accountType: {},
               accountCategory: {}
           }
       }
    },
    methods: {
        initComponent: function() {
            const id = this.$route.params.id;
            var vm = this;

            tenantService.get(id, function(response){
                vm.tenant = response.data;
            }, function(error){
                console.error('error: ', error);
            });

        }
    },
    created: function() {
        this.initComponent();
    }

});

/**
 * The main application component
 */
const TenantApp = Vue.component('tenant-app', {
    template: "#tenant-app",
    data: function() {
        return {
            title: "Tenant App"
        }
    },
    created: function() {
        //this.$axios.get(config.API_URL +"/")
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'index', component: TenantListComp},
    {path: "/edit/:id", name: 'edit', component: TenantEditComp},
    {path: "/show/:id", name: 'show', component: TenantShowComp}
];

// the application router.
const router = new VueRouter({
    routes: routes
});

new Vue({
    el: '#app',
    router: router,
    render: h => h(TenantApp)
});

