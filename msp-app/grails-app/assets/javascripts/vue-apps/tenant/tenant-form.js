/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            accountType: 'Account Type',
            accountName: 'Account Name',
            phoneNumber: 'Phone Number',
            cellNumber: 'Cell Number',
            postalAddress: 'Postal Address',
            businessAddress: 'Business Address',
            physicalAddress: 'Physical Address',
            emailAddress: 'Email Address',
            vatNumber: 'VAT Number',
            bankAccountName: 'Account Name',
            bank: 'Bank',
            accountNumber: 'Account Number',
            branchCode: 'Branch Code',
            branch: 'Branch'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

/**
 * The main application component
 */

const vm = new Vue({
    el: '#tenant-form-app',
    data: function () {
        return {
            bankOptions: {
                placeholder: 'Bank ...',
                data: [{id: 90, text: 'Placeholder'}, {id: 98, text: 'He is God'}]
            },
            tenant: {
                bankAccount: {
                    bank: {
                        id: null
                    }
                },
                businessAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                postalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                physicalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                accountType: {}
            }
        }
    },
    methods: {
        initForm: function () {
            this.tenant = {
                bankAccount: {
                    bank: {
                        id: null
                    }
                },
                businessAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                postalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                physicalAddress: {
                    street: null,
                    suburb: null,
                    city: null,
                    country: null
                },
                accountType: {}
            }
        },
        onCancel: function() {
            this.$confirm({
                title: 'Cancel ?',
                content: 'Are you sure you want to cancel?'
            }).then(() =>{
                window.location.assign(config.API_URL + "/tenant");
            }).catch(() => {})
        },
        onSaveTenant() {
            const url = config.API_URL + "/tenant/save";

            this.$validator.validate().then((result) => {
                if(result) {
                    // show the loader
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });

                    this.$axios.post(url, this.tenant).then((response) => {
                        let tenant = response.data;

                        this.loader.hide();

                        this.$alert({
                            content: "Tenant created successfully.",
                            title: 'Success'
                        }, () => {
                            this.loader = this.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(tenant['_links']['self']['href']);
                        })

                    }).catch((error) => {
                        console.log("Error occured while trying to save tenant entity", error);
                        this.loader.hide();

                        this.$alert({
                            content: 'Failed to save the tenant entity.',
                            title: 'Error !'
                        })
                    });

                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });

        },
        onSelectBank() {
        }
    },

    created: function() {
        //initialise the form
        this.initForm();

    },

    mounted: function() {
        const vm = this;

        this.bankOptions = {
            placeholder: 'Credit Account...',
            ajax: {
                url: config.API_URL + '/provider/list-options',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    'accepts': 'application/json'
                },
                delay: 300,
                processResults: function (data) {
                    // Transforms the top-level key of the response object from 'items' to 'results'

                    let options = data.map((option) => {
                        return {
                            id: option.accountNumber,
                            text: `${option.accountNumber} : ${option.accountName}`
                        };
                    });

                    return {
                        results: options
                    };
                },
                data: function (params) {
                    return {
                        q: params.term
                    };
                }
            }
        }

        $(this.$refs.accountType).on('change', function() {
            vm.tenant.accountType.id = $(this).val();
        });

    }
});
