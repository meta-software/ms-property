/**
 * Created by lebohof on 8/22/2018.
 */
//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);

// property form component
const BankAccountFormComp = Vue.component("bank-account-form", {
    template: "#bank-account-form",
    data: function () {
        return {
            loader: null,
            openModal: false,
            loadingSubmit: false,
            bankAccountForm: {
                account: {
                    id: tenantId,
                },
                accountName: null,
                accountNumber: null,
                branchName: null,
                branchCode: null
            },
            options: {
                placeholder: 'Bank...',
                ajax: {
                    url: config.API_URL + '/bank',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        'accepts': 'application/json'
                    },
                    delay: 300,
                    processResults: function (data) {
                        let results = {results: []}

                        for(let i = 0; i < data.length; i++  ) {
                            let obj = data[i];
                            let option = {id: obj.id, text: obj.bankName};
                            results['results'].push(option);
                        }

                        return results;
                    }
                }
            }

        }
    },
    methods: {
        saveBankAccount: function() {

            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
                placement: 'bottom-right'
            });

            const vm = this;

            this.$validator.validate().then( (result) => {

                if(result){
                    vm.$axios.post(config.API_URL + '/bank-account/save', vm.bankAccountForm).then( (response) => {
                        vm.loader.hide();

                        vm.bankAccount = response.data;

                        vm.$alert({
                            content: "Bank Account "+vm.bankAccount.accountNumber + " created successfully."
                        }, function () {
                            vm.openModal = false; //close the modal
                            vm.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.reload(true);
                        })
                    }).catch(function (error) {
                        vm.loader.hide();

                        vm.$notify({
                            type: 'danger',
                            title: 'Error',
                            content: "error while saving the Bank Account"
                        });
                    })
                } else {
                    vm.$notify({
                        type: 'danger',
                        title: 'Validation Error',
                        content: 'invalid form data submitted.'
                    });
                    vm.loader.hide();
                }
            })
        },
        callback: function (msg) {
            //this.$notify(`Modal dismissed with msg '${msg}'.`);
        }
    }
});

// account receipts component
const AccountTransactions = Vue.component("account-transactions", {
    template: "#account-transactions",
    props: ["accountId"],
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            transactions: []
        }
    },
    methods: {
        retrieveTransactions: function() {
            const url = config.API_URL + '/transaction/account-transactions';
            console.log('retrieve from: ', url);

            this.loader.isLoading = true;
            this.$axios.get(url, {params: {accountId: this.accountId} }).then((response) => {
                console.log(response.data);
                this.transactions = response.data;
            }).catch((error) => {
                iziToast.error({
                    message: "Error occurred during invoices retrieval",
                    title: "Error"
                });
                console.error(error)
            }).finally(() => this.loader.isLoading = false )
        }
    }
});

//Page application.
new Vue({
    el: '#main-app',
    data: {
        tenantId: tenantId,
        tenant: {
            id: tenantId,
            accountNumber: ""
        },
        rentalUnitOptions: {},
        accountTransactions: []
    },
    methods: {
        terminateTenant: function() {
            const vm = this;

            this.tenant.terminated = true;

            this.$axios.put(config.API_URL + "/tenant/terminate/"+this.tenantId, this.tenant).then(function(response){
                console.log("successfully terminated  tenant id " + vm.tenantId);
                location.reload(true);
            }).catch(function(error){
                console.log(error.data);
            });
        },

        retrieveTransactions: function() {
            this.$refs.transactionsComponent.retrieveTransactions();
        }
    },
    mounted: function() {

        this.rentalUnitOptions = {
            placeholder: 'Select Unit...',
            ajax: {
                url: config.API_URL + '/rental-unit/index',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: {
                    accepts: 'application/json'
                },
                delay: 300,
                data: function (params) {
                    let query = {
                        q: params.term
                    };
                    return query;
                }
            }
        };

    }
});
