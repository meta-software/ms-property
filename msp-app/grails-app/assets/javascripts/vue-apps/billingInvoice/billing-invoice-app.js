/**
 * Created by lebohof on 03/05/2020.
 */
//= require vue-apps/address

const API_URL = config.API_URL + "/billing-invoice";

const billingInvoiceService = {
    list: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined)
            queryParams.params = args;

        return axios.get(API_URL, queryParams);
    },

    get: function(id) {
        return axios.get(API_URL+"/show-report/"+id)
    },

    printInvoice(id) {
        return axios.get(API_URL+"/print-invoice/"+id, {responseType: 'arraybuffer'});
    },

    emailInvoice(id) {
        let putData = {id: id};
        return axios.put(API_URL+"/email-invoice/"+id, putData);
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const BillingInvoiceApp = Vue.component("app", {
    template: "#main-app"
});

const TransactionSearch = Vue.component('transaction-search', {
    template: '#transaction-search',
    data() {
        return {
            query: {
                tenant: '',
                cycle: ''
            },
            billingCycleOptions: {
                placeholder: 'Billing Cycle...'
            }
        }
    },
    methods: {
        requestSearch() {
            this.$emit('search-request', this.query);
            this.$router.replace({path: "/", query: this.query});
        },

        prepareBillingCycleOptions() {
            this.$axios.get(config.API_URL + "/billing-cycle/list-options").then((resp) => {
                let cycles = resp.data.map((acc) => {
                    return {
                        id: acc.name,
                        text: acc.name
                    }
                });

                this.billingCycleOptions = {
                    placeholder: 'Billing Cycle...',
                    allowClear: true,
                    data: cycles
                }
            });
        }

    },
    mounted() {
        this.prepareBillingCycleOptions();
    }
});

const BillingInvoiceList = Vue.component("billing-invoice-list", {
    template: '#billing-invoice-list',
    data() {
        return {
            billingInvoices: [],
            loader: {
                isLoading: false,
                fullPage: true
            }
        }
    },

    created() {
        //this.getList();
    },

    methods: {
        getList(params) {
            this.loader.isLoading = true;

            billingInvoiceService.list(params).then((response) => {
                this.billingInvoices = response.data;
                console.log(this.billingInvoices);
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        searchInvoices(params) {
            this.getList(params);
        }
    }
});

const BillingInvoiceShow = Vue.component("billing-invoice-show", {
    template: "#billing-invoice-show",
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            billingInvoice: null
        }
    },

    created() {
        this.getEntity();
    },

    methods: {
        emailInvoice() {
            this.loader.isLoading = true;
            // show modal to confirm
            billingInvoiceService.emailInvoice(this.id).then((resp) => {
                iziToast.success({message: "Email request was sent successfully", overlay: true, timeout: 0, position: "topCenter"});
            }).catch((err) => {
                iziToast.error({message: "An error occurred while sending the email request. <br \> Email sending was not succesful"});
            }).finally(()=>{
                this.loader.isLoading = false;
            });
        },

        printInvoice() {
            this.loader.isLoading = true;

            billingInvoiceService.printInvoice(this.id).then((resp) => {
                console.log(resp);
                let blob = new Blob([resp.data], { type: 'application/pdf' })
                let link = document.createElement('div')
                url = window.URL.createObjectURL(blob);
                window.open(url, '_blank');
            }).finally(() => this.loader.isLoading = false);
        },

        getEntity() {
            this.loader.isLoading = true;

            billingInvoiceService.get(this.id).then((resp) => {
                this.billingInvoice = resp.data;
            }).finally(() => {
                this.loader.isLoading = false
            });
        }
    },

    computed: {
        invoiceTotal() {
            return (this.billingInvoice.vatTotal + this.billingInvoice.invoiceSubTotal);
        },
        id() {
            return this.$route.params.id;
        },
        tenant() {
            if(this.billingInvoice.tenant) {
                return this.billingInvoice.tenant;
            } else {
                return {}
            }
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: BillingInvoiceList
    },
    {
        name: "show",
        path: "/:id",
        component: BillingInvoiceShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

jQuery(document).ready(function() {

    /**
     * The main application component
     */
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(BillingInvoiceApp)
    });

});

