/**
 * Created by lebohof on 11/11/2018.
 */
const makerCheckerTemplate = `
<div class="row maker-checker">
	<div class="col-md-12 col-lg-12 col-xs-12">
		<div v-if="status=='PENDING' && status != undefined && !isMaker" class="form-actions">
			<button type="button" @click="approve" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Approve</button>
			<button type="button" @click="reject" class="btn btn-sm yellow-gold"><i class="fa fa-ban"></i> Reject</button>
		</div>
		<div v-if="status=='PENDING' && status != undefined && isMaker" class="form-actions">
			<button type="button" disabled="disabled" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Approve</button>
			<button type="button" disabled="disabled" class="btn btn-sm yellow-gold"><i class="fa fa-ban"></i> Reject</button>
		</div>
	</div>

	<div class="col-md-12 col-lg-12 col-xs-12">
        <div class="col-md-12 col-lg-12 col-xs-12" v-show="status=='REJECTED'"><span class="label label-danger" ><i class="fa fa-ban"></i> {{status}}</span></div>
        <div class="col-md-12 col-lg-12 col-xs-12" v-show="status=='APPROVED'"><span class="label label-success" ><i class="fa fa-check"></i> {{status}}</span></div>
        <div class="col-md-12 col-lg-12 col-xs-12" v-show="status=='CANCELLED'"><span class="label label-warning" ><i class="fa fa-check"></i> {{status}}</span></div>
	</div>	

	<modal v-model="openRejectModal" id="mc-reject" >
		<span slot="title"><i class="fa fa-ban"></i> Reject Reason ?</span>

		<form>
			<!-- BEGIN FORM BODY-->
			<div class="form-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">Reject Comment</label>
							<textarea v-validate="'required'" v-model="makerChecker.comment" name="comment" class="form-control"></textarea>
							<span class="has-error help-block">{{ errors.first('comment') }}</span>
						</div>
					</div>
				</div>
			</div>
			<!-- END FORM BODY-->
		</form>

		<div slot="footer">
			<btn @click="openRejectModal=false"><i class="fa fa-times"></i> Cancel</btn>
			<btn @click="rejectEntity" class="blue-hoki" ><i class="fa fa-save"></i> Submit</btn>
		</div>
	</modal>
</div>`;

const makerCheckerService = {
    approve: function(controller, itemId) {
        const url = config.API_URL + '/' + controller + '/approve/' + itemId;
        return axios.put(url, {id: itemId});
    },
    reject: function(controller, itemId) {
        const url = config.API_URL + '/' + controller + '/reject/' + itemId;
        return axios.put(url, {id: itemId});
    }
} ;

Vue.component('maker-checker', {
    template: makerCheckerTemplate,
    props: {
        status: {
            required: true
        },
        controller: {
            required: true
        },
        itemId: {
            required: true
        },
        makerid: {
            required: true
        },
        reload: {
            default: true
        }
    },
    data: function() {
        return {
            openRejectModal: false,
            makerChecker: {
                id: null,
                comment: ''
            }
        }
    },
    computed: {
        userId: function() {
            return this.$cookies.get('userid')
        },
        isMaker: function() {
            return this.userId === this.makerid
        }
    },
    watch: {
        itemId: function(itemId) {
            this.makerChecker.id = itemId;
        }
    },
    methods: {
        approve: function(event){
            const vm = this;
            const url = config.API_URL + '/' + this.controller + '/approve/' + vm.itemId;

            this.$confirm({
                title: 'Confirm ?',
                content: "Are you sure you want to Approve.\nContinue?"
            }).then( (msg) => {
                this.startLoader();
                makerCheckerService.approve(this.controller, this.itemId).then( (resp) => {

                    if(vm.reload === false){
                        vm.stopLoader();
                        vm.$emit('approved', this.makerChecker);
                    }

                    iziToast.success({
                        message: "Item Approval was successful.",
                        title: "Success",
                        onClosing: () => {
                            if(this.reload) {
                                window.location.reload(true);
                            }
                        }
                    });
                }).finally( (err) => {
                    vm.stopLoader();
                });
            });
        },
        reject: function(event){
            this.openRejectModal = true;
        },
        startLoader: function(){
            this.loader = this.$loading.show({
                container: null, //full page overlay
                canCancel: false,
            });
        },
        stopLoader: function() {
            this.loader.hide();
        },
        rejectEntity: function(event){

            const vm = this;
            const url = config.API_URL + '/' + vm.controller + '/reject/' + vm.itemId;

            vm.startLoader();
            axios.put(url, this.makerChecker).then( (resp) => {
                vm.stopLoader();
                vm.openRejectModal = false;

                if(vm.reload === false) {
                    this.$emit('rejected', this.makerChecker);
                }

                iziToast.success({
                    message: "Item was rejected successfully",
                    title: "Success",
                    onClosing: () => {
                        if(vm.reload) {
                            vm.startLoader();
                            window.location.reload();
                        }
                    }
                });

            }).catch((err) => {
                vm.stopLoader();
            });
        }
    }
});
