/**
 * Created by lebohof on 11/11/2018.
 */

const NotificationApp = new Vue({
    template: "#notifications",
    data: function() {
        return {
            notifications: [],
            makerChecker: {
                description: '',
                status: null
            }
        }
    },
    created: function(){
        this.makerChecker = {
            status: this.status
        }
    },
    methods: {
        getNotifications: function() {
            const vm = this;
            const url = confirm.API_URL + '/notification';

            vm.$axios.get(url).then(function (response) {
                const notifications = response.data;

                //@todo: check if there are any new notifications from the ones already in memory.
                //@todo: this is a good candidate for the vuex plugin.

            }).catch(function (errors) {
                console.error('error occured fecthing notifications.');
                console.error(errors);
            });
        }
    }
});

