/**
 * Created by lebohof on 12/27/2019.
 */

const transactionService = {
    listByBatch(id) {
        return axios.get(API_URL+`/main-transaction/transaction-batch/${id}`)
    }
};

const template = `
<section>
    <loading :active.sync="loader.isLoading"
             :is-full-page="loader.fullPage" ></loading>
    <div class="inbox margin-top-10">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <bootstrap-table :data="mainTransactions" :columns="table.columns" :options="table.options"></bootstrap-table>
            </div>
        </div>
    </div>
</section>
`;

Vue.component("transaction-list", {
    template: template,
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            transactions: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                data: [],
                columns: [
                    {
                        field: 'transactionType',
                        title: 'Txn Type'
                    },
                    {
                        field: 'accountNumberCr',
                        title: 'Credit Acc',
                        formatter: (v) => shortify(v, 32)
                    },
                    {
                        field: 'subAccountNumberCr',
                        title: 'Sub Acc'
                    },
                    {
                        field: 'accountNumberDb',
                        title: 'Debit Acc',
                        formatter: (v) => shortify(v, 32)
                    },
                    {
                        field: 'transactionReference',
                        title: 'Reference'
                    },
                    {
                        field: 'transactionDate',
                        title: 'Txn Date'
                    },
                    {
                        field: 'transactionDetails',
                        title: 'Txn Details',
                        formatter: (v) => shortify(v, 32)
                    },
                    {
                        field: 'transactionAmount',
                        title: 'Amount',
                        formatter: (v) => formatNumber(v)
                    },
                ],
                options: {
                    IdField: 'id',
                    showColumns: false,
                    height: 450,
                    pagination: false,
                    sortable: true,
                    classes: "table table-striped table-hover table-borderless table-condensed"
                }
            }
        }
    },

    computed: {
        mainTransactions() {
            return this.transactions.map((txn) => {
                return {
                    id: txn.id,
                    transactionType: txn.transactionType,
                    accountNumberCr: txn.accountNumberCr,
                    accountNumberDb: txn.accountNumberDb,
                    subAccountNumberCr: txn.subAccountNumberCr,
                    subAccountNumberDb: txn.subAccountNumberDb,
                    transactionReference: txn.transactionReference,
                    transactionDetails: shortify(txn.description, 64),
                    transactionAmount: txn.transactionAmount,
                    transactionDate: moment(txn.transactionDate).format('YYYY-MM-DD')
                }
            })
        }
    },

    methods: {
        fetchTransactions(params) {
            this.loader.isLoading = true;
            transactionService.listByBatch(this.transactionBatch.id).then((response) => {
                this.transactions = response.data;

                this.transactions = this.transactions.map((txn) => {
                    txn.accountNumberCr = txn.creditAccount + " : " + txn.creditAccountName
                    txn.accountNumberDb = txn.debitAccount + " : " + txn.debitAccountName
                    txn.subAccountNumberCr = txn.subAccountNumberCr + " : " + txn.subAccountNameCr
                    return txn;
                })

            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    }
});

