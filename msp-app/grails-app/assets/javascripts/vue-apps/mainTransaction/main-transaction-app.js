/**
 * Created by lebohof on 12/27/2019.
 */

//= require global/plugins/vue/bootstrap-table/bootstrap-table-vue

const API_URL = config.API_URL + "/main-transaction"

const transactionService = {
    list: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined) {
            queryParams.params = args;
        }
        return axios.get(API_URL+'/search', queryParams);
    },

    get: function(id) {
        return axios.get(API_URL+"/show/"+id)
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const TransactionApp = Vue.component("app", {
    template: "#main-app"
});

Vue.component('transaction-search', {
    template: '#transaction-search',
    data() {
        return {
            query: {
                txref: '',
                cracc: '',
                dbacc: ''
            },
            subAccountOptions: {
                placeholder: 'Sub Account...'
            }
        }
    },
    methods: {
        requestSearch() {
            this.$emit('search-request', this.query);
            this.$router.replace({path: "/", query: this.query});
        },
        prepareSubAccountOptions() {
            this.$axios.get(config.API_URL + "/sub-account/list-options").then((resp) => {
                let subAccounts = resp.data.map((acc) => {
                    return {
                        id: acc.accountNumber,
                        text: `${acc.accountNumber} : ${acc.accountName}`
                    }
                });

                subAccounts.unshift({id: '-1', text: 'All Accounts..'});
                this.subAccountOptions = {
                    placeholder: 'Sub Account...',
                    data: subAccounts
                }
            });
        }
    },
    mounted() {
        this.prepareSubAccountOptions();
    }
});

const TransactionList = Vue.component("transaction-list", {
    template: '#transaction-list',
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            transactions: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                data: [],
                columns: [
                    {
                        field: 'transactionType',
                        title: 'Txn Type'
                    },
                    {
                        field: 'accountNumberCr',
                        title: 'Credit Acc',
                        formatter: (v) => shortify(v, 32)
                    },
                    {
                        field: 'subAccountNumberCr',
                        title: 'Sub Acc'
                    },
                    {
                        field: 'accountNumberDb',
                        title: 'Debit Acc',
                        formatter: (v) => shortify(v, 32)
                    },
                    {
                        field: 'transactionReference',
                        title: 'Reference'
                    },
                    {
                        field: 'transactionDate',
                        title: 'Txn Date'
                    },
                    {
                        field: 'transactionDetails',
                        title: 'Txn Details',
                        formatter: (v) => shortify(v, 32)
                    },
                    {
                        field: 'transactionAmount',
                        title: 'Amount',
                        formatter: (v) => formatNumber(v)
                    },
                ],
                options: {
                    IdField: 'id',
                    showColumns: false,
                    height: 450,
                    pagination: false,
                    sortable: true,
                    classes: "table table-striped table-hover table-borderless table-condensed"
                }
            }
        }
    },

    computed: {
        mainTransactions() {
            return this.transactions.map((txn) => {
                return {
                    id: txn.id,
                    transactionType: txn.transactionType,
                    accountNumberCr: txn.accountNumberCr,
                    accountNumberDb: txn.accountNumberDb,
                    subAccountNumberCr: txn.subAccountNumberCr,
                    subAccountNumberDb: txn.subAccountNumberDb,
                    transactionReference: txn.transactionReference,
                    transactionDetails: shortify(txn.description, 64),
                    transactionAmount: txn.transactionAmount,
                    transactionDate: moment(txn.transactionDate).format('YYYY-MM-DD')
                }
            })
        }
    },

    methods: {
        fetchTransactions(params) {
            this.loader.isLoading = true;
            transactionService.list(params).then((response) => {
                this.transactions = response.data;

                this.transactions = this.transactions.map((txn) => {
                    txn.accountNumberCr = txn.creditAccount + " : " + txn.creditAccountName
                    txn.accountNumberDb = txn.debitAccount + " : " + txn.debitAccountName
                    txn.subAccountNumberCr = txn.subAccountNumberCr + " : " + txn.subAccountNameCr
                    return txn;
                })

            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        searchTransactions(params) {
            this.fetchTransactions(params);
        }
    }
});

const TransactionShow = Vue.component("transaction-show", {
    template: "#transaction-show",
    data() {
        return {
            transaction: {
                id: "",
                subAccount: {},
                maker: {},
                checker: {},
                billingCycle: {}
            },
            loader: {
                isLoading: false,
                fullPage: false
            }
        }
    },

    created() {
        this.fetchTransaction()
    },

    methods: {
        fetchTransaction() {
            this.loader.isLoading = true

            transactionService.get(this.id).then((response) => {
                this.transaction = response.data
            }).finally(() => {
                this.loader.isLoading = false
            });
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: TransactionList
    },
    {
        name: "show",
        path: "/:id",
        component: TransactionShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

$(document).ready(() =>{
    /**
     * The main application component
     */
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(TransactionApp)
    });
});

