/**
 * Created by lebohof on 7/28/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker

Vue.use(VeeValidate, {
    events: 'change'
});

$( document ).ready(function() {

    /**
     * The main application component
     */
    const vm = new Vue({
        el: '#app'
    });
});
