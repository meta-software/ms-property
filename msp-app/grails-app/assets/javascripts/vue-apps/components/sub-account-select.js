/**
 * Created by lebohof on 04/05/2021.
 */
Vue.component('SubAccountSelect', {
    template: `<select style="width: 100%" :value="value" ><slot></slot></select>`,
    props: {
        value: {
            required: true,
        },
        type: {
            required: false,
            default: 'all'
        }
    },
    computed: {
        url() {
            if(this.type === 'expenses')  {
                return config.API_URL + '/sub-account/list-expenses/'
            } else if (this.type === 'cost') {
                return config.API_URL + '/sub-account/list-costs/'
            } else {
                return config.API_URL + '/sub-account/list-options/'
            }
         }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        },
        initComponent() {
            return axios.get(this.url).then((response) => {
                return response.data.map((subAccount) => {
                    return {id: subAccount.id, text: `${subAccount.accountNumber} : ${subAccount.accountName}`};
                });
            });
        }
    },
    mounted: function() {

        $(this.$el)
            .select2({placeholder: 'Sub Account...'})
            .val(this.value)
            .on('change', (event) => this.updateValue(event));

        this.initComponent().then((resp) => {
            this.select2 = {
                placeholder: {
                    id: 0,
                    text: "Sub Account..."
                },
                allowClear: true,
                data: resp
            };

            $(this.$el)
                .empty()
                .select2(this.select2)
                .val(null)
                .trigger('change');
        });
    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2')
            ;
        }
    }
});
