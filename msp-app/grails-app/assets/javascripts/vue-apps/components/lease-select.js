/**
 * Created by lebohof on 04/05/2021.
 */

Vue.component('LeaseSelect', {
    template: `<select style="width: 100%" :value="value" ref="tag"><slot></slot></select>`,
    props: {
        value: {
            required: true
        },
        tenantNumber: {
            required: false,
            type: String
        },
        tenantId: {
            required: false
        }
    },
    data() {
        return {
            select2: {

                placeholder: 'Lease...',
                ajax: {
                    url: config.API_URL + '/tenant/leases',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        'accepts': 'application/json'
                    },
                    delay: 300,
                    data: (params) => {

                        if(this.tenantId) {
                            return {
                                q: params.term,
                                accountId: this.tenantId,
                            };
                        }

                        return {
                            q: params.term,
                            accountNumber: this.tenantNumber,
                        };
                    },
                    processResults: function (data) {
                        let results = {results: []}

                        for(let i = 0; i < data.length; i++  ) {
                            let obj = data[i];
                            let rentalUnit = obj.rentalUnit;
                            let option = {id: obj.id, text: `${obj.leaseNumber} - ${rentalUnit.unitReference} : ${rentalUnit.name}`};
                            results['results'].push(option);
                        }

                        return results;
                    }
                }
            }
        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        }
    },
    mounted: function() {
        $(this.$el)
            .select2(this.select2)
            .val(this.value)
            .on('change', (event) => this.updateValue(event));
    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        accountNumber(accountNumber) {
            this.accountNumber = accountNumber;

            $(this.$el)
                .empty()
                .select2(this.select2)
                .trigger('change');
        },
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2');
        }
    }
});
