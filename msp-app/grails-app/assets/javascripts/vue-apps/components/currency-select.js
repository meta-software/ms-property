/**
 * Created by lebohof on 04/08/2021
 */

Vue.component('CurrencySelect', {
    template: '<select style="width: 100%" :value="value" ref="tag"><slot></slot></select>',
    props: {
        value: {
            required: true
        }
    },
    data() {
        return {
            select2: {}
        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        }
    },
    mounted: function() {

        axios.get(config.API_URL+'/currency/list-options').then((resp) => {
            let countries = resp.data.map((currency) => {
                return { id: currency.id, text: currency.currencyName }
            });
            countries.unshift({id: '', text: 'Currency..'});

            this.select2 = {
                placeholder: 'Currency...',
                allowClear: true,
                data: countries
            };

            $(this.$el)
                .select2(this.select2)
                .val(this.value)
                .on('change', (event) => this.updateValue(event))
                .on('change.select2', (event) => this.updateValue(event));
        });

    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2')
            ;
        }
    }
});
