/**
 * Created by lebohof on 04/05/2021.
 */

Vue.component('CountrySelect', {
    template: '<select style="width: 100%" :value="value" ref="tag"><slot></slot></select>',
    props: {
        value: {
            required: true
        }
    },
    data() {
        return {
            select2: {}
        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        }
    },
    mounted: function() {

        axios.get(config.API_URL+'/country/list-options').then((resp) => {
            let countries = resp.data.map((country) => {
                return { id: country.id, text: country.countryName }
            });
            countries.unshift({id: '', text: 'Countries..'});

            this.select2 = {
                placeholder: 'Countries...',
                allowClear: true,
                data: countries
            };

            $(this.$el)
                .select2(this.select2)
                .val(this.value)
                .on('change', (event) => this.updateValue(event));
        });

    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2')
            ;
        }
    }
});
