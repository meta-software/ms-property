/**
 * Created by lebohof on 05/19/2021.
 */

//= require vue-apps/components/utils

const bcTemplate = `
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-calendar"></i> Open Billing Cycles</h3>
        </div>
        <!-- List group -->
        <table class="table table-striped table-advanced">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>StartDate</th>
                    <th>EndDate</th>
                    <th>Active</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="billingCycle in billingCycles" :key="billingCycle.id">
                    <td>{{billingCycle.name}}</td>
                    <td>{{billingCycle.startDate | formatDate('DD MMM, YYYY')}}</td>
                    <td>{{billingCycle.endDate | formatDate('DD MMM, YYYY')}}</td>
                    <td><bool-label :show-icon-only="true" :bool-value="billingCycle.active" /></td>
                </tr>
            </tbody>
        </table>
    </div>
`

Vue.component('billing-cycles', {
    template: bcTemplate,
    data() {
        return {
            billingCycles: []
        }
    },
    methods: {
        initComponent() {
            const loader = overlayLoader.customerLoader({
                canCancel: false,
                isFullPage: true,
                color: '#000000',
                loader: 'spinner'
            });

            let params = {
                open: 1
            };

            axios.get(config.API_URL+'/billing-cycle?', {params: params}).then((resp) => {
                this.billingCycles = resp.data;
            }).finally(() => loader.hide())
        }
    },
    created() {
        this.initComponent()
    }
});
