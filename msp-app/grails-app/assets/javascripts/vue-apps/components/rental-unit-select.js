/**
 * Created by lebohof on 04/05/2021.
 */

Vue.component('RentalUnitSelect', {
    template: `<select style="width: 100%" :value="value" ref="tag"><slot></slot></select>`,
    props: {
        value: {
            required: true
        },
        propertyId: {
            required: false
        }
    },
    data() {
        return {
            select2: {

                placeholder: 'Rental Unit...',
                ajax: {
                    url: config.API_URL + '/rental-unit/list-options',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    headers: {
                        'accepts': 'application/json'
                    },
                    delay: 300,
                    data: (params) => {
                        return {
                            q: params.term,
                            propertyId: this.propertyId,
                        };
                    },
                    processResults: function(data) {
                        console.log(data);
                        let results =  data.map((result) => {
                            return {
                                id: result.id,
                                text: result.unitReference+':'+result.name
                            }
                        });

                        return { results: results }
                    }
                }
            }
        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        }
    },
    mounted: function() {
        $(this.$el)
            .select2(this.select2)
            .val(this.value)
            .on('change', (event) => this.updateValue(event))
            .on('change.select2', (event) => this.updateValue(event));
    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        propertyId(propertyId) {
            this.propertyId = propertyId;

            $(this.$el)
                .empty()
                .select2(this.select2)
                .trigger('change');
        },

        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2');
        }
    }
});
