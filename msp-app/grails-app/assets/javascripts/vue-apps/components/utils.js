/**
 * Created by lebohof on 05/19/2021.
 */

Vue.component('bool-label', {
    template: `
    <span :class="labelClass" class="label mc-status">
        <i v-if="showIcon | showIconOnly" class="fa" :class="iconClass" ></i> <template v-if="!showIconOnly">{{booleanText}}</template>
    </span>
`,
    props: {
        boolValue: {
            type: Boolean,
            required: true
        },
        showIcon: {
            type: Boolean,
            required: false,
            default: false
        },
        showIconOnly: {
            type: Boolean,
            required: false,
            default: false
        },
        boolText: {
            type: Object,
            required: false,
            default: () => { return { trueValue: 'Yes', falseValue: 'No' } }
        }
    },
    computed: {
        iconClass() {
            return this.boolValue ? 'fa-check' : 'fa-times'
        },
        labelClass() {
            return this.boolValue ? 'label-success' : 'label-danger'
        },
        booleanText() {
            return this.boolValue ? this.boolText.trueValue : this.boolText.falseValue;
        }
    }
});

Vue.component('bs-label', {
    template: '<span :class="labelClass" class="label "><slot></slot></span>',
    props: {
        type: {
            default: 'info'
        },
        size: {
            default: 'sm'
        }
    },
    computed: {
        labelClass() {
            let classes = []
            classes.push(`label-${this.type}`)
            //classes.push(`label-${this.size}`)
            return classes;
        }
    }
})