/**
 * Created by lebohof on 04/05/2021.
 */
Vue.component('ExchangeRateView', {
    template: `
<div class="exchange-rate-view">
    <div class="exchange-rate-view-body" >
        <label>Reporting Value - {{exchangeRateValue.currency}}: </label>
        <template v-if="!error">
        <div class="exchange-rate-view-value red" v-if="loading">Loading...</div>    
        <div class="exchange-rate-view-value" v-else-if="isValidParams && exchangeRateValue.value">{{exchangeRateValue.value | formatNumber}}</div>    
        <div class="exchange-rate-view-value red" v-else>Waiting for input...</div>
        </template>
        <div v-if="error" class="exchange-rate-view-value " style="color: red;">Error</div>
    </div>
</div>`,
    props: {
        value: {
            required: true
        },
        sourceCurrency: {
            required: true
        },
        eventDate: {
            required: true
        }
    },
    data() {
        return {
            exchangeRateValue: {
                value: null,
                currency: null,
                eventDate: null
            },
            error: false,
            loading: false
        }
    },
    methods: {
        async updateValue(event) {

            if(this.isValidParams) {
                this.loading = true;

                let queryParams = {
                    value: this.value,
                    fromCurrency: this.sourceCurrency,
                    date: this.eventDate
                };

                try {
                    this.error = false;
                    const resp = await axios.get(config.API_URL+'/exchange-rate/convert', {params: queryParams});
                    this.exchangeRateValue = resp.data;
                } catch(err){
                    this.error = true;
                    this.errorMessage = "Error";
                } finally {
                    this.loading = false;
                }
            }
        }
    },
    created: function() {
        this.updateValue();
    },
    computed: {
        isValidParams() {
            return (this.value && this.sourceCurrency && this.eventDate)
        }
    },
    watch: {
        value(newValue, oldValue) {
            if(newValue !== oldValue) {
                this.updateValue();
            }
        },
        sourceCurrency(newCurrency, oldCurrency) {
            if(newCurrency !== oldCurrency) {
                this.updateValue();
            }
        },
        eventDate(newDate, oldDate) {
            if(newDate !== oldDate) {
                this.updateValue();
            }
        }
    }
});
