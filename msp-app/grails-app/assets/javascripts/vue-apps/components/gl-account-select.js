/**
 * Created by lebohof on 04/05/2021.
 */
const glAccountSelectTemplate = `<select style="width: 100%" :value="value" ref="tag"><slot></slot></select>`;

Vue.component('GlAccountSelect', {
    template: glAccountSelectTemplate,
    props: {
        value: {
            required: true
        },
        accountType: {
            required: false,
            default: null
        },
        idValue: {
            required: false,
            default: 'id'
        },
        params: {
            required: false,
            type: Object
        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        },
        initComponent() {
            const vm = this;
            let params = {
                accountType: this.accountType
            };
            Object.assign(params, params, this.params)
            
            return axios.get(config.API_URL + '/general-ledger/list-options', {params: params}).then((response) => {
                
                return response.data.map((glAccount) => {
                    let id = glAccount.id;
                    switch(vm.idValue) {
                        case 'accountNumber':
                            id = glAccount.accountNumber;
                            break;
                    }

                    return {id: id, text: `${glAccount.accountNumber} : ${glAccount.accountName}`};
                });
            });
        }
    },
    mounted: function() {

        $(this.$el)
            .select2({placeholder: 'GL Account...'})
            .val(this.value)
            .on('change', (event) => this.updateValue(event));

        this.initComponent().then((resp) => {
            this.select2 = {
                placeholder: {
                    id: 0,
                    text: "GL Account..."
                },
                allowClear: true,
                data: resp
            };

            $(this.$el)
                .empty()
                .select2(this.select2)
                .val(null)
                .trigger('change');
        });
    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2')
            ;
        }
    }
});
