/**
 * Created by lebohof on 04/05/2021.
 */

Vue.component('PropertySelect', {
    template: `<select style="width: 100%" :value="value" ref="tag"><slot></slot></select>`,
    props: {
        select2: {
            required: true,
            type: Object
        },
        value: {
            required: true
        }
    },
    data() {
        return {

        }
    },
    methods: {
        updateValue(event) {
            this.$emit('input', event.target.value)
        },
        reset: function () {
            $(this.$el).value(null).trigger('change');
        }
    },
    mounted: function() {
        $(this.$el)
            .select2(this.select2)
            .val(this.value)
            .on('change', (event) => this.updateValue(event));
    },
    destroyed: function () {
        $(this.$el)
            .off()
            .select2('destroy');
    },
    watch: {
        select2(select2) {
            $(this.$el)
                .empty()
                .select2(select2)
                .trigger('change');
        },
        value(value) {
            $(this.$el)
                .val(value)
                .trigger('change.select2')
            ;
        }
    }
});
