/**
 * Created by lebohof on 12/29/2018.
 */

//= require global/plugins/vue/vee-validate
//= require vue-apps/components/currency-select
//= require vue-apps/exchangeRate/exchange-rate-preview
//= require vue-apps/components/utils

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            startDate: 'Start Date',
            endDate: 'End Date',
            exchangeRates: 'Exchange Rates',
            exRate: 'Exchange Rate'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle'],
    methods: {
        createRateGroup() {
            this.$router.push("/create")
        }
    }
});

/**
 * The Exchange Rate Group service
 */
const exchangeRateGroupService = {
    API_URL: config.API_URL+'/exchange-rate-group',

    list: function(queryParams) {
        return axios.get(this.API_URL+'/list', {params: queryParams});
    },
    get(id) {
        return axios.get(this.API_URL+`/show/${id}`);
    },
    save(exchangeRate) {
        return axios.post(this.API_URL+'/save', exchangeRate);
    },
    open(id) {
        return axios.put(this.API_URL+'/open/'+id);
    },
    delete(id) {
        return axios.delete(this.API_URL+'/delete/'+id);
    },
    activate(id) {
        return axios.put(this.API_URL+'/activate/'+id);
    },
    create() {
        return axios.get(this.API_URL+'/create')
    }
};

/**
 * The main application component
 */
const ExchangeRateGroupApp = Vue.component('erg-app', {
    template: "#erg-app"
});

const ExchangeRateGroupList = Vue.component('erg-list',{
    template: "#erg-list",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            exchangeRateGroups: [],
            params: {},
            selectedExchangeRateGroupId: null
        }
    },
    methods: {
        selectExchangeRateGroupId(exchangeRateGroup) {
            //this.$router.push({name: 'erg_show', params: {id: exchangeRateGroup.id}})
            this.selectedExchangeRateGroupId = exchangeRateGroup.id;
        },
        search() {
            this.fetchRates(this.params);
        },
        reset() {
            this.params = {
                q: '',
                open: ''
            };
            this.fetchRates(this.params);
        },
        fetchRates() {
            this.loader.isLoading = true;

            exchangeRateGroupService.list(this.params).then((resp) => {
                this.exchangeRateGroups = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        deleteExchangeRateGroup(id) {

            const vm = this;

            vm.$confirm({
                title: "Delete Exchange Rate Group",
                size: 'md',
                content: "Are you Sure you want to proceed with deleting the ExchangeRateGroup?"
            }).then(function(){
                vm.loader.isLoading = true;

                exchangeRateGroupService.delete(id).then((resp) => {
                    iziToast.success({message: "Exchange rate was deleted successfully."})
                    vm.fetchRates();
                }).finally(() => {
                    vm.loader.isLoading = false;
                })
            });
        }
    },
    created() {
        this.fetchRates();
    }
});

const ExchangeRateGroupShow = Vue.component('erg-show', {
    template: '#erg-show',
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            exchangeRateGroup: {}
        }
    },
    computed: {
        id: function() {
            return this.$route.params.id;
        }
    },
    methods: {
        get() {
            this.loader.isLoading = true;
            exchangeRateGroupService.get(this.id).then((resp) => {
                this.exchangeRateGroup = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        close() {
            this.loader.isLoading = true;
            exchangeRateGroupService.close(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate Group closed successfully...'});
                this.get();
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        activate() {
            this.loader.isLoading = true;
            exchangeRateGroupService.activate(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate Group activated successfully...'});
                this.get()
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        open() {
            this.loader.isLoading = true;
            exchangeRateGroupService.open(this.id).then((resp) => {
                iziToast.success({position: 'topRight', message: 'Exchange Rate Group opened successfully...'});
                this.get()
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        notImplemented() {
            iziToast.error({
                overlay: true,
                layout: 2,
                title: 'Alert !!!<br />',
                baloon: true,
                timeout: 0,
                message: 'This feature is not activated.<br />Please contact administrator for more details.',
                position: 'center'})
        }
    },
    created() {
        this.get();
    }
});

const ExchangeRateGroupCreate = Vue.component('erg-create', {
    template: "#erg-create",
    data: function() {
        return {
            exchangeRateGroup: {
                startDate: null,
                endDate: null,
                active: false,
                exchangeRates: []
            }
        }
    },
    created() {
        const loader = overlayLoader.fullPageLoader();

        exchangeRateGroupService.create().then((resp) => {
            this.exchangeRateGroup = resp.data;
        }).finally(() => {
            loader.hide();
        })
    },
    methods: {
        cancel() {
            //are you sure you want to cancel
            this.$router.push("/");
        },
        async save() {
            const loader = overlayLoader.fullPageLoader();
            const result = await this.$validator.validate();
            try {
                if (result) {
                    const resp = await exchangeRateGroupService.save(this.exchangeRateGroup);
                    iziToast.success({
                        position: 'topRight',
                        message: 'Exchange Rate Group Request created successfully...'
                    });
                    this.$router.replace("/");
                }
            } finally {
                loader.hide();
            }
        }
    }
});

const ExchangeRateForm = Vue.component('er-form', {
    template: `<tr>
    <td><div class="form-control input-sm" disabled="disabled" >{{value.fromCurrency.id}}</div></td>
    <td><div class="form-control input-sm" disabled="disabled" >{{value.toCurrency.id}}</div></td>
    <td><input type="text" class="form-control input-sm" :value="value.rate" @change="updateInput" /></td>
</tr>`,
    props: {
        value: {
            required: true
        }
    },
    data() {
        return {}
    },
    methods: {
        updateInput(evt) {
            this.value.rate = evt.target.value;
            this.$emit('input', this.value);
        }
    },
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: ExchangeRateGroupList
    },
    {
        name: "erg_show",
        path: "/show/:id",
        component: ExchangeRateGroupShow
    },
    {
        name: "create",
        path: "/create",
        component: ExchangeRateGroupCreate
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

$(document).ready(function() {
    /**
     * The main application component
     */
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(ExchangeRateGroupApp)
    });
})
