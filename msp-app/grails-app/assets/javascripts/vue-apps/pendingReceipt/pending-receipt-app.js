/**
 * Created by lebohof on 12/04/2020.
 */
//= require global/plugins/vue/vee-validate
//= require global/plugins/lodash.min

const TRANS_API = config.API_URL+"/receipt-allocation";
const API_URL   = config.API_URL+"/pending-receipt";

const pendingReceiptService = {
    receiptExists(txnRef) {
        return axios.get(config.API_URL+"/transaction-exists/" + txnRef);
    },
    listUnallocated(params) {
        return axios.get(API_URL+"/unallocated", {params: params});
    },
    postReceiptAllocations(receiptId) {
        return axios.put(API_URL+'/post/' + receiptId, {});
    },
    getReceipt(id) {
        return axios.get(API_URL+"/show/"+id);
    },
    postAllocation(allocation) {
        return axios.post(TRANS_API+'/save', allocation);
    },
    deleteAllocation(id) {
        return axios.delete(TRANS_API+"/delete/"+id);
    },
    listAllocations(receiptId) {
        return axios.get(API_URL + "/"+ receiptId + "/receipt-allocation");
    },
    clearAllocations(pendingReceiptId) {
        return axios.put(API_URL + '/'+pendingReceiptId+'/clear-allocations');
    }
};

const transactionReferenceValidator = {
    getMessage(field, args) {
        return "A record with provided " + field + " value already exists";
    },
    validate(value, args) {
       return pendingReceiptService.receiptExists(args).then( (resp) => {
            return (resp.data['exists'] === false);
        });
    }
};

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            tenant: 'Tenant',
            subAccount: 'Sub Account',
            transactionReference: 'Reference',
            transactionDate: 'Transaction Date',
            amount: 'Amount',
            receiptAmount: 'Amount',
            lease: 'Lease',
            debitAccount: 'Debit Account'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

VeeValidate.Validator.extend('validReference', transactionReferenceValidator);

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

Vue.component('pending-receipt-filter', {
    template: '#pending-receipt-filter',
    data: function() {
        return {
            params: {}
        }
    },
    methods: {
        search() {
            this.$emit('search', this.params);
        },
        reset() {
            this.params = {}
            this.$emit('reset');
        }
    }
});

const PendingReceiptList = Vue.component("pending-receipt-list", {
   template: "#pending-receipt-list",
   data() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           pendingReceipts: []
       }
   },
    methods: {
        _onSearch: function(params) {
            this.search(params, true);
        },
        _search: _.debounce( (vm, params) => {
            vm.search(params, false)
        }, 350),
        search(params, feedback=false) {
            this.loader.isLoading = feedback;
            pendingReceiptService.listUnallocated(params).then((resp) => {
                this.pendingReceipts = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        reset(){
            this.fetchReceipts();
        },
        fetchReceipts() {
            this.loader.isLoading = true;
            pendingReceiptService.listUnallocated().then((resp) => {
                this.pendingReceipts = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        showPendingReceipt(id) {
            router.push({ name: 'show-pending-receipt', params: { id: id } })
        }
    },
    created () {
       this.fetchReceipts();
    }
});

const PendingReceiptShow = Vue.component("pending-receipt-show", {
    template: "#pending-receipt-show",
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            pendingReceipt: {}
        }   
    },
    methods: {
        discardReceiptBatch(pendingReceiptId) {
            this.$router.go(-1);
        },
        clearAllocations() {
            const pendingReceiptId = this.pendingReceipt.id;

            this.loader.isLoading = true;
            pendingReceiptService.clearAllocations(pendingReceiptId).then((resp) => {
                this.fetchPendingReceipt();
                this.$refs.receiptAllocationList.fetchAllocationItems();

            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        fetchPendingReceipt() {
            this.loader.isLoading = true;

            pendingReceiptService.getReceipt(this.id).then((resp) => {
                this.pendingReceipt = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        postReceiptAllocations() {
            this.loader.isLoading = true;

            pendingReceiptService.postReceiptAllocations(this.id).then((resp) => {
                iziToast.success({message: "Pending Receipt posted successfully.", layout: 2, title: 'Success', position: 'topRight'});
                this.$router.replace("/");
            }).finally(() => {
                this.loader.isLoading = false;
            });
        },
        receiptItemDeleted() {
            this.fetchPendingReceipt();
        },
        allocationItemAdded() {
            this.fetchPendingReceipt();

            this.$refs.receiptAllocationList.fetchAllocationItems();
        }
    },
    computed: {
        id() {
            return this.$route.params['id'];
        },
        isAmountFullyAllocated() {
            return this.pendingReceipt.receiptAmount === this.pendingReceipt.amountAllocated
        }
    },
    created() {
        this.fetchPendingReceipt();
    }
});

Vue.component('receipt-allocation-list', {
    template: "#receipt-allocation-list",
    props: ['receiptId', 'pendingReceipt'],
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            openAllocationForm: false,
            isAllocated: false,
            receiptAllocations: []
        }
    },
    methods: {
        clearAllocations() {
            this.$emit('clear-allocations', this.receiptId);
        },
        fetchAllocationItems() {
            this.loader.isLoading = true;
            pendingReceiptService.listAllocations(this.receiptId).then((resp) => {
                this.receiptAllocations = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        deleteAllocation(evt, receipt) {
            pendingReceiptService.deleteAllocation(receipt.id).then( (resp) => {
                //now we refresh the List component array
                this.$emit('item-deleted', receipt.id);
                iziToast.success({message: "Item removed successfully.", position: "topRight"});
                this.fetchAllocationItems();
            }).catch( (err) => {
                iziToast.error({position: "bottomRight", message: "Failed to remove item."})
            });
        }
    },
    created() {
        this.fetchAllocationItems();
    },
    mounted() {
    },
    watch: {
        receiptId: function(receiptId) {
            this.fetchAllocationItems();
        }
    },
    computed: {
        lease() {
            if(this.selectedLease != null && this.selectedLease !== "") {
                return JSON.parse(this.selectedLease);
            }
            return null;
        },
        subAccount() {
            if(this.selectedSubAccount != null && this.selectedSubAccount !== "") {
                return JSON.parse(this.selectedSubAccount);
            }
            return null;
        }
    }
});

Vue.component('receipt-allocation-form', {
    template: "#receipt-allocation-form",
    props: ['pendingReceipt'],
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: true
            },
            debitAccountOptions: {
                data: []
            },
            subAccountOptions: {
                data: []
            },
            leaseOptions: {
                data: []
            },
            selectedDebitAccount: null,
            debitAccountSelected: null,
            selectedLease: null,
            selectedSubAccount: null,
            receiptAllocation: {},
            formSubmitted: false
        }
    },
    computed: {
        lease() {
            if(this.selectedLease != null && this.selectedLease !== "") {
                return JSON.parse(this.selectedLease);
            }
            return null;
        },
        subAccount() {
            if(this.selectedSubAccount != null && this.selectedSubAccount !== "") {
                return JSON.parse(this.selectedSubAccount);
            }
            return null;
        },
        tenant() {
            return this.pendingReceipt.tenant;
        },
        isAmountFullyAllocated() {
            return this.pendingReceipt.receiptAmount === this.pendingReceipt.amountAllocated
        },
        isFormValid() {
            // loop over all contents of the fields object and check if they exist and valid.
            return !Object.keys(this.fields).some(key => this.fields[key].invalid) && this.formSubmitted;
        }
    },
    methods: {
        resetForm() {
            //initialise the form items
            this.selectedSubAccount = "";
            this.selectedLease = "";
            this.receiptAllocation.allocatedAmount = null;

            this.$validator.pause();
            this.$nextTick(() => {
                this.$validator.errors.clear();
                this.$validator.fields.items.forEach(field => field.reset());
                this.$validator.fields.items.forEach(field => this.errors.remove(field));
                this.$validator.resume();
            });
        },
        cancelAllocation() {
            this.resetForm();
        },
        async postAllocation() {

            if(this.isAmountFullyAllocated) {
                return;
            }

            try {
                this.loader.isLoading = true;
                this.formSubmitted = true;

                const result = await this.$validator.validate();
                if(result) {
                    this.receiptAllocation.leaseId = this.lease.id;
                    this.receiptAllocation.subAccountId = this.subAccount.id;
                    this.receiptAllocation.pendingReceiptId = this.pendingReceipt.id;

                    this.loader.isLoading = true;
                    const resp = await pendingReceiptService.postAllocation(this.receiptAllocation);
                    iziToast.success({message: "Receipt allocation item successfully added", timeout: 2500, layout: 2, position: "topRight"});
                    const allocation = {...this.receiptAllocation};
                    this.$emit('allocation-added', allocation);
                    this.resetForm();
                }
            } finally {
                this.loader.isLoading = false;
            }

        },
        initComponent() {
            this.selectedSubAccount = "";
            axios.get(config.API_URL + "/sub-account/list-options").then((resp) => {

                const excludes= ['0','1','7','8','9']

                let options = resp.data.filter((acct) => !excludes.includes(acct.accountNumber) ).map((option) => {
                    let obj = {
                        id: option.id,
                        text: option.accountNumber + " : " + option.accountName
                    };
                    return {
                        id: JSON.stringify(obj),
                        text: obj.text
                    }
                });

                this.subAccountOptions = {
                    placeholder: "Sub Account...",
                    allowClear: true,
                    data: options
                };

            });

            this.selectedLease = "";
            const leasesUrl = config.API_URL + "/tenant/tenant-leases/"+this.pendingReceipt.tenant.id;
            axios.get(leasesUrl).then((resp) => {
                let options = resp.data.map((option) => {
                    let obj = {
                        id: option.id,
                        text: option.leaseNumber + " : " + option.rentalUnit.name
                    };

                    return {
                        id: JSON.stringify(obj),
                        text: obj.text
                    }
                });

                this.leaseOptions = {
                    placeholder: 'Lease Unit...',
                    allowClear: true,
                    data: options
                };

                if(options.length === 1){
                    this.selectedLease = options[0].id;
                }

            });

        }
    },
    watch: {
        tenant(tenant) {
            if(tenant) {
                this.initComponent();
            }
        },
        selectedLease(selectedLease) {
            this.receiptAllocation.leaseId = this.lease;
        },
        selectedSubAccount(selectedSubAccount) {
            this.receiptAllocation.subAccountId = this.subAccount;
        }
    },
    mounted() {
        this.initComponent();
    }
});

Vue.component('receipt-header', {
    template: "#receipt-header",
    props: ['pendingReceipt'],
    data() {
        return { }
    },
    methods: {
        allocateReceipt() {
            this.loader = this.$loading.show({
                container: null,
                canCancel: false,
                placement: 'center'
            });

            pendingReceiptService.postReceipt(this.pendingReceipt).then((resp) => {
                iziToast.success({message: "Receipt posted successfully.", layout: 2, title: "Success", targetFirst: true});

            }).finally(() => {
                this.loader.hide();
            });
        }
    }
});

const PendingReceiptApp = {
    template: "#pending-receipt-app"
};

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: PendingReceiptList
    },
    {
        name: "show-pending-receipt",
        path: "/show/:id",
        component: PendingReceiptShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

jQuery(document).ready(function() {
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(PendingReceiptApp)
    });
});
