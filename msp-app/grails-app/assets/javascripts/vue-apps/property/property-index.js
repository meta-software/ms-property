
/**
 * Created by lebohof on 8/22/2018.
 */

//= require global/plugins/vue/vee-validate
//= require global/plugins/lodash.min

Vue.use(VeeValidate, {
    events: 'change'
});

Vue.use(VueLoading);
