/**
 * Created by lebohof on 8/22/2018.
 * /
// = require application-vue */
//= require global/plugins/vue/vee-validate
//= require vue-apps/maker-checker
//= require vue-apps/components/utils

Vue.use(VeeValidate, {
    events: 'change'
});

jQuery(document).ready(function() {
    Vue.component('property-insurance', {
        template: "#property-insurance",
        data: function() {
            return {
                openModal: false,
                insurance : {
                    property: {
                        id: propertyId
                    },
                    insurerName: "",
                    insuranceNumber: "",
                    renewalDate: "",
                    sumAssured: ""
                }
            }
        },
        methods: {
            saveInsurance: function() {
                const vm = this;

                this.loader = this.$loading.show({
                    container: null, //full page overlay
                    canCancel: false,
                });

                this.$validator.validate().then(function (result) {
                    if(result){
                        vm.$axios.post(config.API_URL + '/property-insurance/save', vm.insurance).then(function(response){
                            vm.loader.hide();
                            vm.$alert({
                                content: "Insurance record created successfully.",
                                title: 'Success'
                            }, function () {
                                vm.openModal = false; //close the modal
                                window.location.reload(true);
                            })
                        }).catch(function (error) {
                            vm.loader.hide();
                            vm.$notify({
                                type: 'danger',
                                title: 'Error!!!',
                                content: 'failed to save the insurance record'
                            });
                        })
                    } else {
                        vm.loader.hide();
                    }
                });
            },
            callback: function (msg) {
                //
            }
        },
        created: function() {
            console.log('created insurance component')
        }
    })

    Vue.component("rental-unit", {
        template: "#rental-unit",
        data: function () {
            return {
                loader: null,
                openModal: false,
                loadingSubmit: false,
                rentalUnit: {
                    property: {
                        id: propertyId,
                    },
                    name: "",
                    unitReference: "",
                    area: ""
                }
            }
        },
        methods: {
            saveRentalUnit: function() {

                const vm = this;

                this.loader = this.$loading.show({
                    container: null, //full page overlay
                    canCancel: false,
                });

                this.$validator.validate().then(function (result) {
                    if(result){
                        vm.$axios.post(config.API_URL + '/rental-unit/save', vm.rentalUnit).then(function (response) {
                            vm.loader.hide();
                            vm.$alert({
                                content: "Rental Unit created successfully.",
                                title: 'Success'
                            }, function () {
                                vm.openModal = false; //close the modal
                                window.location.reload(true);
                            })
                        }).catch(function (error) {
                            vm.loader.hide();
                            vm.$notify({
                                type: 'danger',
                                title: 'Error!!!',
                                content: 'failed to save the rental-unit'
                            });
                        })
                    } else {
                        vm.loader.hide();
                    }

                })
            },
            callback: function (msg) {
                //
            }
        }
    });

// rentalUnits list component
    const RentalUnitList = Vue.component("rental-unit-list", {
        template: "#rental-unit-list",
        props: ['propertyId'],
        data: function () {
            return {
                loader: {
                    isLoading: false,
                    fullPage: false
                },
                rentalUnits: [],
                queryParams: {
                    query: null,
                    isVacant: "no"
                }
            }
        },
        methods: {
            search() {
                this.fetchProperties();
            },
            refresh() {
                this.queryParams.query = null;
                this.fetchProperties()
            },
            fetchProperties: function () {
                this.loader.isLoading = true;
                let params = {
                    query: this.queryParams.query,
                    isVacant: this.queryParams.isVacant
                }

                this.$axios.get(config.API_URL+'/rental-unit/property/'+this.propertyId, {params: params}).then((resp) => {
                    this.rentalUnits = resp.data;
                }).finally(() => {
                    this.loader.isLoading = false;
                })
            }
        },
        created() {
            this.fetchProperties();
        }
    });
    
    const propertyShowApp = new Vue({
        el: '#main-app',
        data: {
            propertyId: propertyId,
            confDisabled: false,
            property: {
                id: propertyId
            }
        },
        mounted: function(){

        },
        methods: {
            configureProperty: function() {
                const vm = this;
                this.confDisabled = true;
                this.$axios.get(config.API_URL + "/property/configure/"+this.propertyId).then((response) => {
                    vm.$notify({
                        duration: 6000,
                        title: 'Success',
                        type: 'success',
                        content: response.data.message
                    });
                }).catch((error) => {
                    error.data.errors.forEach((error) => {
                        vm.$notify({
                            duration: 6000,
                            type: 'danger',
                            content: error.message
                        })
                    })
                }).then(() => {
                    vm.confDisabled = false;
                })
            },
            terminateProperty: function() {
                const vm = this;

                vm.$confirm({
                    title: "Terminate Property",
                    size: 'md',
                    content: "Are you Sure you want to proceed with termination?"
                }).then(function(){
                    vm.$alert("continue with termination");
                }).catch(function(){
                    vm.$notify({
                        type: 'primary',
                        content: 'You cancelled the property termination request.'
                    })
                });

                this.property.terminated = true;

                this.$axios.put(config.API_URL + "/property/terminate/"+this.propertyId, this.property).then(function(response){
                    location.reload(true);
                }).catch(function(error){
                    console.log(error.data);
                });
            },
            approveProperty: function() {
                const vm = this;

                vm.$confirm({
                    title: "Approve Property",
                    size: 'md',
                    content: "Are you Sure you want to proceed?"
                }).then(function(){
                    alert("continue with approval");
                }).catch(function(){
                    vm.$notify({
                        type: 'danger',
                        content: 'You cancelled the approval'
                    })
                });
            }
        },
        created: function() {
        }
    });
})
