/**
 * Created by lebohof on 7/24/2018.
 */

//= require global/plugins/vue/vee-validate
//= require_self

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            landlord: 'Landlord',
            name: 'Name',
            propertyType: 'Property Type',
            propertyManager: 'Property Manager',
            standNumber: 'Stand Number',
            area: 'Area',
            city: 'City',
            street: 'Street',
            country: 'Country',
            suburb: 'Suburb',
            commission: 'Commission',
            unitReference: 'Unit Reference'
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

Vue.component('rental-unit-form', {
    props: {
        rentalUnits: Array
    },
    template: "#rental-unit-form",
    data: () => {
        return {
            rentalUnit: {
                name: "",
                unitReference: "",
                area: null
            }
        }
    },
    methods: {
        clearFormData: function() {
            this.rentalUnit = {
                name: "",
                unitReference: "",
                area: null
            }
        },
        removeUnit(index){
            this.$emit('remove-unit', index);
        },
        addUnit : function (){
            //@todo: perform a bit of validation

            this.$validator.validate().then((result) => {
                if(result) {
                    let rentalUnit = Object.assign({}, this.rentalUnit);

                    rentalUnit.area = Number(rentalUnit.area);

                    this.clearFormData();
                    //@emit: the event and the object which has just been added.
                    this.$emit('input', rentalUnit);
                } else {
                }
            });
        }
    },
    created: function() {
        this.clearFormData();
    }
})
;

/**
 * The main application component
 */
let vm = new Vue({
    el: '#property-form',
    data: function () {
        return {
            property: {
                id: propertyId,
                rentalUnits: [],
                landlord: {},
                propertyManager: {},
                propertyType: {},
                address: {}
            }
        }
    },
    methods: {
        onCancel: function() {
            this.$confirm({
                title: 'Cancel ?',
                content: 'Are you sure you want to cancel?'
            }).then(() =>{
                this.loader = this.$loading.show({
                    container: null, //full page overlay
                    canCancel: false,
                });
                window.location.assign(config.API_URL + "/provider/show/" + this.property.id);
            }).catch(() => {} )
        },
        onSaveProperty: function() {
            console.log(this.property);
            const url = config.API_URL + '/property/update/' + this.property.id;

            this.$validator.validate().then((result) => {
                if(result) {
                    // show the loader
                    this.loader = this.$loading.show({
                        container: null, //full page overlay
                        canCancel: false,
                    });

                    this.$axios.put(url, this.property).then((response) => {
                        let property = response.data;

                        this.loader.hide();

                        this.$alert({
                            content: "Property updated successfully.",
                            title: 'Success'
                        }, () => {
                            this.loader = this.$loading.show({
                                container: null, //full page overlay
                                canCancel: false,
                            });
                            window.location.assign(property['_links']['self']['href']);
                        })

                    }).catch((error) => {
                        console.log("Error occured while trying to save property entity", error);
                        this.loader.hide();

                        this.$alert({
                            content: 'Failed to save the property entity',
                            title: 'Error !'
                        })
                    });

                }
                else {
                    iziToast.error({message: "Errors exist in form.", timeout: 5000, position: 'topCenter'});
                }
            });

        },
        onAddRentalUnit: function (rentalUnit) {
            console.log(rentalUnit);
            this.property.rentalUnits.push(rentalUnit)
        },
        onRemoveRentalUnit : function (index) {
            let rentalUnit = this.property.rentalUnits[index];
            this.property.rentalUnits.splice(index, 1);
        },
        initForm: function () {

        }
    },

    computed: {
        totalUnitsArea: function() {
            return this.property.rentalUnits.reduce((a, {area}) => a + area, 0);
        }
    },
    created: function() {
        this.$axios.get(config.API_URL + "/property/edit/"+propertyId).then( (response) => {
            this.property = response.data;
        }).catch( (error) => {
            console.log("error occured: ", error);
            //@todo: this will cause a catastrophe man.
            iziToast.error({message: 'Error while loading page.'})
        });

    }
});
