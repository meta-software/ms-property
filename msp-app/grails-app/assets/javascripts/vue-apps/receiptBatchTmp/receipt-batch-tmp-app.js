/**
 * Created by lebohof on 03/28/2020.
 */
//= require global/plugins/vue/vee-validate
//= require vue-apps/components/currency-select
//= require vue-apps/components/exchange-rate
//= require vue-apps/components/sub-account-select

const TRANS_API = config.API_URL+"/receipt-tmp";
const API_URL = config.API_URL+"/receipt-batch-tmp";
const receiptBatchTmpService = {
    receiptExists(txnRef) {
        return axios.get(config.API_URL+"/transaction/transaction-exists/"+txnRef);
    },
    pending() {
        return axios.get(API_URL+"/list-pending");
    },
    postReceipt(receipt) {
        return axios.post(TRANS_API+'/save', receipt);
    },
    deleteReceipt(id) {
        return axios.delete(TRANS_API+'/delete/'+id);
    },
    postBatch(id) {
        return axios.put('/receipt-batch-tmp/post-batch/'+id, {});
    },
    createBatch(postData) {
        return axios.post(API_URL+'/create', postData)
    },
    deleteBatch(id) {
        return axios.delete(API_URL+"/delete/"+id);
    },
    deleteTransaction(id) {
        return axios.delete(TRANS_API+"/delete/"+id);
    },
    getBatch(id) {
        return axios.get(API_URL+"/show/"+id);
    },
    listReceipts(batchId) {
        return axios.get(TRANS_API+"/list-by-batch/"+batchId);
    }
};

const transactionReferenceValidator = {
    getMessage(field, args) {
        return "A record with provided " + field + " value already exists";
    },
    validate(value, args) {
       return receiptBatchTmpService.receiptExists(args).then( (resp) => {
            return (resp.data['exists'] === false);
        });
    }
};

// VeeValidate dictionary setup
const dictionary = {
    en: {
        attributes: {
            tenant: 'Tenant',
            subAccount: 'Sub Account',
            transactionReference: 'Reference',
            transactionDate: 'Transaction Date',
            amount: 'Amount',
            bankAccount: 'Bank Account',
            receiptAmount: 'Amount',
            lease: 'Lease',
            debitAccount: 'Debit Account',
        }
    }
};

Vue.use(VeeValidate, {
    events: 'change',
    dictionary: dictionary
});

VeeValidate.Validator.extend('validReference', transactionReferenceValidator);

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

Vue.component('receipt-batch-summary', {
    props: {
        receiptBatch: Object
    },
    template: '#receipt-batch-summary'
});

const ReceiptBatchList = Vue.component("receipt-batch-list", {
   template: "#receipt-batch-list",
   data() {
       return {
           loader: {
               isLoading: false,
               fullPage: false
           },
           receiptBatches: []
       }
   },
    methods: {
        fetchBatches() {
            this.loader.isLoading = true;
            receiptBatchTmpService.pending().then((resp) => {
                this.receiptBatches = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        removeReceiptBatch(receiptBatchId) {
            this.loader.isLoading = true;

            receiptBatchTmpService.deleteBatch(receiptBatchId).then((resp) => {
                iziToast.success({message: "Receipt Batch was deleted successfully", position: "bottomRight"});
                this.fetchBatches();
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        createReceiptBatch() {
            this.loader.isLoading = true;

            receiptBatchTmpService.createBatch({}).then((resp) => {
                //iziToast.success({message: "Receipt Batch created successfully.", position: "bottomRight"});
                this.$router.push("/show/"+resp.data.id);
            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    },
    created () {
       this.fetchBatches();
    }
});

const ReceiptBatchShow = Vue.component("receipt-batch-show", {
    template: "#receipt-batch-show",
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            receiptBatch: {
                batchCount: 0
            }
        }
    },
    methods: {
        receiptDeleted(){
            this.fetchBatch();
        },
        receiptPosted() {
            this.fetchBatch();
            this.$refs.receiptsList.fetchReceipts();
        },
        discardReceipt(object) {
            console.log(JSON.stringify(object));

            let index = this.receiptBatch.receipts.indexOf(object);
            this.receiptBatch.receipts = this.receiptBatch.receipts.splice(index, 1);
        },
        fetchBatch() {
            this.loader.isLoading = true;

            receiptBatchTmpService.getBatch(this.id).then((resp) => {
                this.receiptBatch = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        discardReceiptBatch(batchId) {
            this.$confirm({
                title: 'Confirm Discard?',
                content: "Are you sure you want to Discard the Receipt Batch? The action is not reversible.",
                backdrop: false
            }).then( (msg) => {
                this.deleteBatch(batchId);
            });
        },
        deleteBatch(batchId) {
            this.loader.isLoading = true;
            receiptBatchTmpService.deleteBatch(batchId).then((resp) => {
                iziToast.success({message: "Receipt batch deleted successfully.", layout: 2, title: 'Success', position: 'topRight'});
                this.$router.replace("/")
            }).finally(() => {
                this.loader.isLoading = false;
            });
        },
        postReceiptBatch(batchId) {
            this.loader.isLoading = true;

            receiptBatchTmpService.postBatch(batchId).then((resp) => {
                iziToast.success({message: "Receipt batch posted successfully.", layout: 2, title: 'Success', position: 'topRight'});
                this.$router.replace("/");
            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    computed: {
        id() {
            return this.$route.params['id'];
        },
        postable() {
            return (this.receiptBatch.batchCount > 0)
        }
    },
    created() {
        this.fetchBatch();
    }
});

Vue.component('receipt-list', {
    template: "#receipt-list",
    props: ['batchId'],
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            receipts: []
        }
    },
    methods: {
        fetchReceipts() {

            if(!this.batchId) {
                return;
            }

            this.loader.isLoading = true;
            receiptBatchTmpService.listReceipts(this.batchId).then((resp) => {
                this.receipts = resp.data;
            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        deleteTransaction(evt, receipt) {
            console.log('the delete request: ', receipt)
            receiptBatchTmpService.deleteTransaction(receipt.id).then( (resp) => {
                //now we refresh the List component array
                this.$emit('receipt-deleted', receipt.id);
                iziToast.success({message: "Receipt deleted successfuly.", position: "topCenter"});

                this.fetchReceipts();
            }).catch( (err) => {
                console.log(err);
                iziToast.error({position: "topCenter", message: "Failed to delete the transaction."})
            });
        }
    },
    created() {
        this.fetchReceipts();
    },
    watch: {
        batchId: function(batchId) {
            this.fetchReceipts();
        }
    },
});

Vue.component('receipt-allocation-form', {
    template: "#receipt-allocation-form",
    props: ['receipt', 'tenant'],
    data() {
        return {
            subAccountOptions: {
                data: []
            },
            leaseOptions: {
                data: []
            },
            selectedLease: null,
            selectedSubAccount: null,
            receiptAllocation: {}
        }
    },
    computed: {
        lease() {
            if(this.selectedLease != null && this.selectedLease !== "") {
                return JSON.parse(this.selectedLease);
            }
            return null;
        },
        subAccount() {
            if(this.selectedSubAccount != null && this.selectedSubAccount !== "") {
                return JSON.parse(this.selectedSubAccount);
            }
            return null;
        }
    },
    methods: {
        resetForm() {
            //initialise the form items
            this.selectedSubAccount = null;
            this.selectedLease = null;
            this.receiptAllocation.allocatedAmount = null;

            this.$validator.pause();

            this.$nextTick(() => {
                this.$validator.errors.clear();
                this.$validator.fields.items.forEach(field => field.reset());
                this.$validator.fields.items.forEach(field => this.errors.remove(field));
                this.$validator.resume();
            });
        },
        cancelAllocation() {
            this.resetForm();
        },
        postAllocation() {

            this.$validator.validate().then( (result) => {
               if(result) {
                   let allocation = Object.assign({}, this.receiptAllocation);
                   this.$emit('allocation-added', allocation);
                   iziToast.success({message: "Receipt allocation item succesfully added", timeout: 2500, layout: 2, position: "topRight"});
                   this.resetForm()
               }
            });

        },
        initComponent() {
            this.selectedLease = "";
            const leasesUrl = config.API_URL + "/tenant/tenant-leases/"+this.tenant.id;
            this.$axios.get(leasesUrl).then((resp) => {
                let options = resp.data.map((option) => {
                    let obj = {
                        id: option.id,
                        text: option.leaseNumber + " : " + option.rentalUnit.name
                    };

                    return {
                        id: JSON.stringify(obj),
                        text: obj.text
                    }
                });

                this.leaseOptions = {
                    placeholder: 'Lease Unit...',
                    allowClear: true,
                    data: options
                };

                if(options.length === 1){
                    this.selectedLease = options[0].id;
                }

            });
        }
    },
    watch: {
        tenant(tenant) {
            if(tenant) {
                this.initComponent();
            }
        },
        selectedLease(selectedLease) {
            this.receiptAllocation.lease = this.lease;
        },
        selectedSubAccount(selectedSubAccount) {
            this.receiptAllocation.subAccount = this.subAccount;
        },
    },
    mounted() {
        this.selectedSubAccount = "";

        this.$axios.get(config.API_URL + "/sub-account/list-options").then((resp) => {
            let options = resp.data.map((option) => {
                let obj = {
                    id: option.id,
                    text: option.accountNumber + " : " + option.accountName
                };
                return {
                    id: JSON.stringify(obj),
                    text: obj.text
                }
            });

            this.subAccountOptions = {
                placeholder: "Sub Account...",
                allowClear: true,
                data: options
            };

        });

        this.initComponent();
    }
});

Vue.component('receipt-form', {
    template: "#receipt-form",
    props: ['batchId', 'batchNumber'],
    data() {
        return {
            tenantOptions: {},
            debitAccountOptions: {},
            debitAccounts: [],
            openAllocations: false,
            receiptAllocations: [],
            selectedTenant: null,
            selectedDebitAccount: null,
            blankReceipt: {
                tenant: null,
                transactionReference: null,
                receiptAmount: null
            },
            receipt: {
                tenant: null,
                transactionReference: null,
                receiptAmount: null
            }
        }
    },
    computed: {
        isAllocated() {
            return (this.allocatedTotal === this.receipt.receiptAmount);
        },
        allocatedTotal() {
            return this.receiptAllocations.reduce((total, allocation ) => {
                return total + allocation.allocatedAmount;
            }, 0);
        },
        receiptTenant() {
            if(this.selectedTenant != null && this.selectedTenant !== "") {
                return JSON.parse(this.selectedTenant);
            }
            return null;
        },
        receiptDebitAccount() {
            if(this.selectedDebitAccount != null && this.selectedDebitAccount !== "") {
                return JSON.parse(this.selectedDebitAccount);
            }
            return null;
        },
        transactionCurrency() {
            return this.receipt.transactionCurrency
        }
    },
    methods: {
        removeAllocation(receiptAllocation, idx) {
            this.receiptAllocations.splice(idx, 1);
        },
        tenantSelected() {
            this.receipt.tenant = this.receiptTenant;
        },
        debitAccountSelected() {
            this.receipt.debitAccount = this.receiptDebitAccount;
        },
        addAllocation(allocation) {
            this.receiptAllocations.push(allocation);
        },
        allocateReceipt() {
            this.$validator.validate().then((result) => {
                if(result) {
                    this.openAllocations = true;
                } else {
                    iziToast.error({message: "The form is invalid and contains errors", layout: 2, position: "topCenter"})
                }
            })
        },
        clearForm() {
            this.resetForm();
            this.$emit('receipt-deleted', this.receipt);
            this.openAllocations = false;
        },
        cancelAllocations(){
            iziToast.error({message: "Cancel"})
        },
        discardReceipt(){
            if(this.receiptAllocations.length === 0) {
                this.clearForm();
                return;
            }

            const $vm = this;
            this.$confirm({
                title: 'Confirm Discard?',
                content: "Are you sure you want to Discard the Receipt? The receipt will be deleted.",
                backdrop: false
            }).then( (msg) => {
                this.clearForm();
            });
        },
        postReceipt() {
            this.loader = this.$loading.show({
                container: null,
                canCancel: false,
                placement: 'center'
            });

            let postData = Object.assign({}, this.receipt);
            postData.receiptBatchTmp = {
                id: this.batchId
            };
            postData.receiptItemTmps = this.receiptAllocations;

            receiptBatchTmpService.postReceipt(postData).then((resp) => {
                iziToast.success({message: "Receipt posted successfully.", layout: 2, title: "Success", targetFirst: true});
                this.openAllocations = false;
                this.resetForm();
                this.$emit("receipt-posted", resp.data);
            }).finally(() => {
                this.loader.hide();
            });
        },
        resetForm() {
            //initialise the form items
            this.receipt = Object.assign({}, this.blankReceipt);
            this.selectedTenant = null;
            this.selectedDebitAccount= null;
            this.receiptAllocations = []

            this.$validator.pause();
            this.$nextTick(() => {
                this.$validator.errors.clear();
                this.$validator.fields.items.forEach(field => field.reset());
                this.$validator.fields.items.forEach(field => this.errors.remove(field));
                this.$validator.resume();
            });
        }
    },
    watch: {
        transactionCurrency: function(newValue, oldValue) {
            if (newValue != oldValue) {
                let filteredAccounts = this.debitAccounts.filter((val) => {
                    if(val && val.bankAccount !== undefined) {
                        return val.bankAccount.currencyId === newValue
                    }
                });

                let options = filteredAccounts.map((option) => {
                    let obj = {id: option.id, text: option.accountNumber + " : " + option.accountName};
                    return {id: JSON.stringify(obj), text: obj.text}
                });

                this.debitAccountOptions = {
                    placeholder: 'Debit Account...',
                    allowClear: true,
                    data: options
                };

                if(options.length === 1) {
                    this.$nextTick(() => {
                        this.selectedDebitAccount = options[0].id;
                        this.debitAccountSelected();
                    })
                }

            }
        }
    },
    mounted() {
        this.tenantOptions = {
            placeholder: "Tenant...",
            allowClear: true,
            ajax: {
                url: config.API_URL + '/tenant/list-options',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                headers: { accepts: 'application/json' },
                delay: 300,
                processResults: function (data) {
                    let options = data.map((option) => {
                        let obj = {id: option.id, text: `${option.accountNumber} : ${option.accountName}`}

                        return {
                            id: JSON.stringify(obj),
                            text: obj.text
                        };
                    });
                    return {
                        results: options
                    };
                },
                data: function (params) {
                    return {
                        q: params.term
                    };
                }
            }
        };

        // credit account options
        this.$axios.get(config.API_URL +'/general-ledger/banks').then((response) => {

        });
    },
    created() {
        this.$axios.get(config.API_URL +'/general-ledger/banks').then((response) => {
            this.debitAccounts = response.data;
        });
    }
});

const ReceiptBatchApp = {
    template: "#receipt-batch-app"
};

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: ReceiptBatchList
    },
    {
        name: "show",
        path: "/show/:id",
        component: ReceiptBatchShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

jQuery(document).ready(function() {
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(ReceiptBatchApp)
    });
});
