/**
 * Created by lebohof on 01/03/2021.
 */
const addressTemplate = `<div class="address" v-if="isValid">
                    <div class="row">
                        <div class="col-md-12">{{address.street}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">{{address.suburb}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">{{address.city}},&nbsp;{{address.country.countryName}}</div>
                    </div>
                </div>
                <div class="address" v-if="!isValid">...</div>
`;

Vue.component('address-show', {
    template: addressTemplate,
    props: {
        address: {
            required: true,
            type: Object
        }
    },
    computed: {
        isValid() {
            if (this.address == null) {
                return false;
            }
            return true;
        }
    }
});
