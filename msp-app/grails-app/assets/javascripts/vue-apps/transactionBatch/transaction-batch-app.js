/**
 * Created by lebohof on 12/27/2019.
 */

const API_URL = config.API_URL + "/transaction-batch";

const transactionBatchService = {
    list: function(args) {
        let queryParams = {
            params: {}
        };

        if(args !== undefined) {
            queryParams.params = args;
        }
        return axios.get(API_URL+'/search', queryParams);
    },

    get: function(id) {
        return axios.get(API_URL+"/show/"+id)
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const TransactionBatchApp = Vue.component("app", {
    template: "#main-app"
});

Vue.component('batch-search', {
    template: '#batch-search',
    data() {
        return {
            query: {
                txref: '',
                cracc: '',
                dbacc: ''
            },
            transactionTypeOptions: {
                placeholder: 'Transaction Type...'
            }
        }
    },
    methods: {
        requestSearch() {
            this.$emit('search-request', this.query);
            this.$router.replace({path: "/", query: this.query});
        },
        prepareSubAccountOptions() {
            this.$axios.get(config.API_URL + "/transaction-type/list-options").then((resp) => {
                let subAccounts = resp.data.map((acc) => {
                    return {
                        id: acc.id,
                        text: acc.name
                    }
                });

                subAccounts.unshift({id: '-1', text: 'All Types..'});
                this.transactionTypeOptions = {
                    placeholder: 'Transaction Type...',
                    data: subAccounts
                }
            });
        }
    },
    mounted() {
        this.prepareSubAccountOptions();
    }
});

const TransactionBatchList = Vue.component("batch-list", {
    template: '#batch-list',
    components: {
        'BootstrapTable': BootstrapTable
    },
    data() {
        return {
            transactionBatches: [],
            loader: {
                isLoading: false,
                fullPage: false
            },
            table: {
                data: [],
                columns: [
                    {
                        field: 'batchNumber',
                        title: 'Batch Number'
                    },
                    {
                        field: 'transactionType',
                        title: 'Transaction Type',
                        formatter: (val) => val.name
                    },
                    {
                        field: 'batchReference',
                        title: 'Batch Reference'
                    },
                    {
                        field: 'dateCreated',
                        title: 'Date Created',
                        formatter: (val) => moment(val).format("YYYY-MM-DD")
                    },
                    {
                        field: 'batchTotal',
                        title: 'Transactions Total',
                        visible: false
                    },
                    {
                        field: 'transactionsCount',
                        title: 'Transactions Count'
                    },
                    {
                        field: 'autoCommited',
                        title: 'Auto Committed',
                        formatter: (val) => formatBoolean(val)
                    },
                    {
                        field: 'maker',
                        title: 'Created By',
                        formatter: (val) => val.fullName
                    }
                ],
                options: {
                    onClickRow: this.handleClickRow ,
                    IdField: 'id',
                    showColumns: false,
                    search: false,
                    height: 450,
                    pagination: false,
                    sortable: true,
                    classes: "table table-striped table-hover table-borderless table-condensed"
                }
            }
        }
    },

    methods: {
        handleClickRow(row, $el, field) {
            this.$router.push({path: "/"+row.id});
            this.$emit("onClickBatch", row.id);
        },
        fetchBatchTransactions(params) {
            this.loader.isLoading = true;
            transactionBatchService.list(params).then((resp) => {
                this.transactionBatches = resp.data.map((batch) => {
                    return batch;
                });

            }).finally(() => {
                this.loader.isLoading = false;
            })
        },
        searchBatches(params) {
            this.fetchBatchTransactions(params);
        }
    }
});

const TransactionBatchShow = Vue.component("batch-show", {
    template: "#batch-show",
    data() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            transactionBatch: {
            }
        }
    },

    created() {
        this.fetchBatch()
    },

    methods: {
        fetchBatch() {
            this.loader.isLoading = true;
            transactionBatchService.get(this.id).then((resp) => {
                this.transactionBatch = resp.data
            }).finally(() => {
                this.loader.isLoading = false
            });
        }
    },

    computed: {
        id() {
            return this.$route.params.id
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: TransactionBatchList
    },
    {
        name: "show",
        path: "/:id",
        component: TransactionBatchShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

jQuery(document).ready(() => {
    /**
     * The main application component
     */
    const vm = new Vue({
        el: '#app',
        router: router,
        render: (h) => h(TransactionBatchApp)
    });
});

