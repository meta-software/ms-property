/**
 * Created by lebohof on 01/04/2021.
 */

// prepare the Provider Services
const rentalUnitService = {
    baseUrl: config.API_URL+'/rental-unit',
    get(id) {
        return axios.get(this.baseUrl+'/'+id);
    },
    fetchRentalUnits(params) {
        return axios.get(this.baseUrl, {params: params});
    }
};

// the PageBar component
Vue.component('page-bar', {
    template: '#page-bar',
    props: ['subTitle', 'pageTitle']
});


//The provider list component
var RentalUnitList = Vue.component('rental-unit-list', {
    template: '#rental-unit-list',
    data: function () {
        return {
            params: {},
            rentalUnits: []
        }
    },
    methods: {
        search() {
            this.fetchRentalUnits();
        },
        reset() {
            this.params = {}
            this.fetchRentalUnits();
        },
        fetchRentalUnits() {
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });
            rentalUnitService.fetchRentalUnits(this.params).then((resp) => {
                this.rentalUnits = resp.data;
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created: function () {
        this.fetchRentalUnits();
    }
});

// The Rental Unit show component
var RentalUnitShow = Vue.component('rental-unit-show', {
    template: "#rental-unit-show",
    data: function() {
       return {
           rentalUnit: null
       }
    },
    methods: {
        initComponent: function() {
            const loader = this.$loading.show({
                isFullPage: true,
                container: null
            });
            rentalUnitService.get(this.id).then((resp) => {
                this.rentalUnit = resp.data;
            }).finally(() => {
                loader.hide();
            });
        }
    },
    created: function() {
        this.initComponent();
    },
    computed: {
        id() { return this.$route.params.id; }
    }
});

/**
 * The rental-unit-app main application component
 */
var RentalUnitApp = Vue.component('rental-unit-app', {
    template: "#rental-unit-app",
    data: function() {
        return {
            title: "Rental Unit App"
        }
    }
});

// The applicable routes for the application.
const routes = [
    {path: "/", name: 'index', component: RentalUnitList},
    {path: "/show/:id", name: 'show', component: RentalUnitShow}
];

// the application router.
const router = new VueRouter({
    routes: routes
});

//Setup the plugins.
Vue.use(VueRouter);

var vm = new Vue({
    el: '#app',
    router: router,
    template: "<rental-unit-app></rental-unit-app>"
});

