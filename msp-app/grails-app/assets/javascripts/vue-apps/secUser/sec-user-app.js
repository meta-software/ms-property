/**
 * Created by lebohof on 01/09/2020.
 */
//= require global/plugins/vue/vee-validate
//= require global/plugins/vue/bootstrap-table/bootstrap-table-vue

Vue.use(VeeValidate, {
    events: 'change'
});

const API_URL = config.API_URL + "/user";
const USER_CHECK_API_URL = config.API_URL + "/user-check";

const userService = {
    list: function(args) {
        let queryParams = {
            params: {}
        };
        if(args !== undefined) {
            queryParams.params = args;
        }
        return axios.get(API_URL, queryParams);
    },

    pending: function(args) {
        let queryParams = {
            params: {}
        };
        if(args !== undefined) {
            queryParams.params = args;
        }
        return axios.get(USER_CHECK_API_URL, queryParams);
    },

    get: function(id) {
        return axios.get(API_URL+"/show/"+id)
    }
};

Vue.component('page-bar', {
    props: ['subTitle', 'pageTitle'],
    template: '#page-bar'
});

const UserApp = Vue.component("app", {
    template: "#main-app"
});

const UserCheckList = Vue.component('user-check-list', {
    template: "#user-check-list",
    props: {
        userChecks: {
            required: true
        }
    },
    components: {
        BootstrapTable: BootstrapTable
    },
    data: function() {
        return {
            columns: [
                {
                    title: 'ID#',
                    field: 'id'
                },
                {
                    title: 'Entity',
                    field: 'username'
                },
                {
                    title: 'Date Created',
                    field: 'makeDate'
                },
                {
                    title: 'Status',
                    field: 'checkStatus'
                },
                {
                    title: 'Action',
                    field: 'actionName'
                },
                {
                    title: 'Created By',
                    field: 'maker'
                }
            ],
            data: [],
            options: {
                onClickRow: (row) => { this.$emit('onClick', row.id) },
                toolbar: '#user-pending-toolbar',
                IdField: 'id',
                showColumns: false,
                search: true,
                showSearchClearButton: true,
                searchOnEnterKey: true,
                showButtonIcons: true,
                showSearchButton: true,
                singleSelect: true,
                pagination: true,
                sortable: true,
                classes: "table table-striped table-hover table-borderless table-condensed"
            }

        }
    },
    created() {
        this.prepareComponent();
    },
    methods: {
        prepareComponent() {
            this.data = this.userChecks.map((user) => {
                console.log('id =',user.id)
                return {
                    id: user.id,
                    username: user.username,
                    checkStatus: user.checkStatus,
                    makeDate: user.makeDate,
                    actionName: user.actionName,
                    maker: user.maker.username
                }
            });

            console.log("check: ", this.data)
        }
    },
    watch: {
        userChecks: function(userChecks) {
            this.prepareComponent();
        }
    }
});

const UserList = Vue.component('user-list', {
    template: "#user-list",
    props: {
        users: {
            required: true
        }
    },
    components: {
        BootstrapTable: BootstrapTable
    },
    data: function() {
        return {
            columns: [
                {
                    title: 'Name',
                    field: 'fullName'
                },
                {
                    title: 'User Name',
                    field: 'username'
                },
                {
                    title: 'Email Address',
                    field: 'emailAddress'
                },
                {
                    title: 'Enabled',
                    field: 'enabled',
                    formatter: (e, value, row) => formatBoolean(e)
                },
                {
                    title: 'Expired',
                    field: 'accountExpired',
                    formatter: (e, value, row) => formatBoolean(e)
                },
                {
                    title: 'Locked',
                    field: 'accountLocked',
                    formatter: (e, value, row) => formatBoolean(e)
                },
                {
                    title: 'Status',
                    field: 'dirtyStatus',
                    formatter: this.statusFormatter
                }
            ],
            data: [],
            options: {
                onClickRow: (row) => { this.$emit('on-click', row.id) },
                toolbar: '#user-list-toolbar',
                IdField: 'username',
                showColumns: false,
                search: true,
                showSearchClearButton: true,
                searchOnEnterKey: true,
                showButtonIcons: true,
                showSearchButton: true,
                singleSelect: true,
                pagination: true,
                sortable: true,
                classes: "table table-striped table-hover table-borderless table-condensed"
            }

        }
    },
    created() {
        this.prepareComponent();
    },
    methods: {
        prepareComponent() {
            this.data = this.users.map((user) => {
                return {
                    id: user.id,
                    fullName: `${user.firstName} ${user.lastName}`,
                    username: user.username,
                    emailAddress: user.emailAddress,
                    enabled: user.enabled,
                    accountExpired: user.accountExpired,
                    accountLocked: user.accountLocked,
                    dirtyStatus: user.dirtyStatus
                }
            })
        }
    },
    watch: {
        users: function(users) {
            this.prepareComponent();
        }
    }
});

const UserApproved = Vue.component("user-approved", {
    template: "#user-approved",
    components: {
        UserList: UserList
    },
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            users: []
        }
    },
    created() {
        this.getList();
    },
    methods: {
        getList() {
            userService.list().then((resp) => {
                this.users = resp.data;
            }).catch((err) => {

            }).finally(() => {
                this.loader.isLoading = false;
            })
        },

        onRowClick(id) {
            console.log(id);
            alert(id);
            this.$router.push({path: '/'+id})
        }
    }
});

const UserShow = Vue.component("user-show", {
    template: "#user-show",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            user: {}
        }
    },
    methods: {
        get() {
            userService.get(this.id).then((resp) => {
                this.user = resp.data;
            }).catch((err) => {

            }).finally(() => {
                this.loader.isLoading = false;
            });
        }
    },
    computed: {
        id: function() {
            return this.$route.params['id'];
        }
    },
    created() {
        this.get();
    }
});

const UserPending = Vue.component("user-pending", {
    template: "#user-pending",
    data: function() {
        return {
            loader: {
                isLoading: false,
                fullPage: false
            },
            userChecks: []
        }
    },
    created() {
        this.getList();
    },
    methods: {
        getList() {
            userService.pending().then((resp) => {
                this.userChecks = resp.data;
            }).catch((err) => {

            }).finally(() => {
                this.loader.isLoading = false;
            })
        }
    }
});

// prepare the VueRouter object and configurations
const routes = [
    {
        name: "home",
        path: "/",
        component: UserApproved
    },
    {
        name: "home",
        path: "/pending",
        component: UserPending
    },
    {
        name: "show",
        path: "/:id",
        component: UserShow
    }
];

const router = new VueRouter({
    mode: 'hash',
    routes: routes
});

/**
 * The main application component
 */
const vm = new Vue({
    el: '#app',
    router: router,
    render: (h) => h(UserApp)
});
