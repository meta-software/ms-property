/**
 * Created by lebohof on 6/5/2019.
 */
//= require vue-apps/components/account-select
//= require vue-apps/components/sub-account-select
//= require vue-apps/components/lease-select

jQuery(document).ready(function() {
    new Vue({
        el: "#main-app",
        data: function() {
            return {
                name: "3",
                options: {
                    placeholder: 'Placeholder...',
                    ajax: {
                        url: config.API_URL + '/account/list',
                        delay: 300
                    }
                }
            }
        },
        created() {
            console.log("created the main app")
        },
        methods: {
            sample() {
                this.options.ajax.data = (params) => {
                    return {
                        q: params.term,
                        ac: 'tenant'
                    }
                };
                this.options = Object.assign({}, this.options);
            }
        }
    });
});
