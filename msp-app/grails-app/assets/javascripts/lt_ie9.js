// This is a manifest file that'll be compiled into application.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require global/plugins/respond.min
//= require global/plugins/excanvas.min
//= require global/plugins/ie8.fix.min
//
