/**
 * Created by lebohof on 12/24/2021.
 */
//= require_self

const config = {
    API_URL: 'http://localhost:8088'
};

//Vue global configurations
Vue.config.productionTip = false;
Vue.config.devtools = true;

console.log("we are in the development environment")