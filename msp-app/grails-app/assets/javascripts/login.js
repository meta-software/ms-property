// This is a manifest file that'll be compiled into application.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//BEGIN CORE PLUGINS
//= require global/plugins/jquery/jquery
//= require global/plugins/bootstrap/js/bootstrap
//END CORE PLUGINS

//BEGIN THEME LAYOUT SCRIPTS
//  equire layouts/layout/scripts/layout
//END THEME LAYOUT SCRIPTS

// Nice to have basic plugins
//= require global/plugins/iziToast/iziToast

//= require_self