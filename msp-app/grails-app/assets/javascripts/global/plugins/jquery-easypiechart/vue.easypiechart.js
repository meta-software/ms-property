Vue.component('easy-pie-chart', {
    props: {
        size: {
            default: 80
        },
        percent: {
            required: true,
            default: 0
        },
        color: {
            default: 'blue'
        },
        legend: {
            required: true
        },
        chartClass: {
            default: 'icon-arrow-right'
        }
    },
    data: function() {
        return {
        }
    },
    template: `
<div class="easy-pie-chart">
    <div class="chart-item number" :data-percent="0">
        <span>0</span>% 
    </div>
    <span class="title" > {{legend}}
        <i :class="chartClass"></i>
    </span>
</div>`,
    mounted() {
        this.init();
    },
    methods: {
        init: function() {
            $(this.$el).children('.chart-item').easyPieChart({
                animate: 1200,
                size: this.size,
                lineWidth: 4,
                barColor: App.getBrandColor(this.color)
            });
            this.refresh();
        },
        refresh: function() {
            $(this.$el).children('.chart-item').data('easyPieChart').update(this.percentValue);
            $(this.$el).children('.chart-item').children('span').text(this.percentValue);
        }
    },
    computed: {
        percentValue: function () {
            return this.percent;
        }
    },
    watch: {
        percent: function(percent) {
            this.refresh();
        },
        color: function(color) {},
        chartClass: function(chartClass) {}
    },
    destroyed: function () {
        $(this.$el).off().easyPieChart('destroy')
    }
});
