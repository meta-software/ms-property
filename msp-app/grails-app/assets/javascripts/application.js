// This is a manifest file that'll be compiled into application.
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//BEGIN CORE PLUGINS
//= require global/plugins/jquery/jquery
//= require global/plugins/bootstrap/js/bootstrap
//= require global/plugins/jquery-slimscroll/jquery.slimscroll
//= require global/plugins/js.cookie.min

//= require global/plugins/moment/moment
//END CORE PLUGINS
//= require global/plugins/select2/js/select2

//= require global/plugins/icheck/icheck
//= require global/plugins/bootstrap-checkbox-1.4.0/dist/js/bootstrap-checkbox
//= require global/plugins/jquery-serialize-object/jquery.serialize-object
//= require global/plugins/bootstrap-datepicker/js/bootstrap-datepicker
//= require global/plugins/bootstrap-table/bootstrap-table
//= require global/plugins/numeraljs/numeraljs.min

//BEGIN THEME GLOBAL SCRIPTS
//= require global/scripts/app
//END THEME GLOBAL SCRIPTS

//BEGIN THEME LAYOUT SCRIPTS
//= require layouts/layout4/scripts/layout
//= require layouts/global/scripts/quick-sidebar
//END THEME LAYOUT SCRIPTS

//= require pages/scripts/components-select2
//= require pages/scripts/form-icheck

//Metasoft application pages.
