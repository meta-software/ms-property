import grails.plugin.springsecurity.SpringSecurityUtils
import groovy.sql.Sql
import metasoft.property.MsAjaxAwareAuthenticationEntryPoint
import metasoft.property.core.EventLoggableListener
import metasoft.property.core.LeaseEntityListener
import metasoft.property.core.MakerCheckerEntityListener
import metasoft.property.core.MsUserDetailsService
import metasoft.property.core.SecUserPasswordEncoderListener
import metasoft.property.core.SharedSessionAuthenticationFilter
import metasoft.property.core.SharedSessionAuthenticationProvider
import metasoft.property.core.security.MetasoftSecurityEventAuthenticationFailureListener
import metasoft.property.core.security.MetasoftSecurityEventListener
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy

// Place your Spring DSL code here
beans = {
    secUserPasswordEncoderListener(SecUserPasswordEncoderListener)
    makerCheckerEntityListener(MakerCheckerEntityListener)
    userDetailsService(MsUserDetailsService)
    metasoftSecurityEventListener(MetasoftSecurityEventListener)
    metasoftSecurityEventAuthenticationFailureListener(MetasoftSecurityEventAuthenticationFailureListener)
    //eventLoggableListener(EventLoggableListener)
    leaseEntityListener(LeaseEntityListener)
    // custom shared authentication setup
    sharedSessionAuthenticationFilter(SharedSessionAuthenticationFilter){
        filterProcessesUrl = 'sh-auth'

        authenticationSuccessHandler = ref('authenticationSuccessHandler')
        authenticationFailureHandler = ref('authenticationFailureHandler')
        authenticationManager = ref('authenticationManager')
        sessionAuthenticationStrategy = ref('sessionAuthenticationStrategy')
        sharedSessionAuthenticatorService = ref('sharedSessionAuthenticatorService')
    }

    // custom authentication provider
    sharedSessionAuthenticationProvider(SharedSessionAuthenticationProvider){
        sharedSessionAuthenticatorService = ref('sharedSessionAuthenticatorService')
        userDetailsService = ref('userDetailsService')
    }
    sessionAuthenticationStrategy(NullAuthenticatedSessionStrategy)

    // prepare the token authentication provider.
    groovySql(Sql, ref('dataSource'))
}
