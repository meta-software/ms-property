// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'metasoft.property.core.SecUser'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'metasoft.property.core.SecUserSecRole'
grails.plugin.springsecurity.authority.className = 'metasoft.property.core.SecRole'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        //[pattern: '/', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/index', access: ['permitAll']],
        [pattern: '/index.gsp', access: ['permitAll']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/auth/**', access: ['permitAll']],
        [pattern: '/login/**', access: ['permitAll']],
        [pattern: '/api/logout/**', access: ['permitAll']], // for the rest api logout
        [pattern: '/dbconsole/**', access: ['permitAll']],

        // application staticRules
        [pattern: '/**/list-options', access: ['permitAll']]    //change this to authenticated
]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],

        //api configurations for the external api interface
        [pattern: '/api/**', filters: 'JOINED_FILTERS,-anonymousAuthenticationFilter,-exceptionTranslationFilter,-authenticationProcessingFilter,-rememberMeAuthenticationFilter,-sharedSessionAuthenticationFilter'],
        [pattern: '/**', filters: 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter']
]

grails.plugin.springsecurity.useSecurityEventListener = true
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.successHandler.alwaysUseDefault = true
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/'
grails.plugin.springsecurity.password.algorithm = 'bcrypt'

grails.plugin.springsecurity.providerNames = ['sharedSessionAuthenticationProvider',
                                              'restAuthenticationProvider',
                                              'daoAuthenticationProvider'
                                               ]

grails.plugin.springsecurity.failureHandler.exceptionMappings = [
        [exception: org.springframework.security.authentication.CredentialsExpiredException.name, url: '/user/password-expired']
]

// Added by the Audit-Logging plugin:
grails.plugin.auditLog.auditDomainClassName = 'metasoft.property.core.AuditLogging'
grails.plugin.auditLog.defaultActor = 'system'
grails.plugin.auditLog.excluded = ['createdBy','updatedBy', 'dateCreated', 'lastUpdated']

// Rest authentication configuration
grails.plugin.springsecurity.rest.login.active = true
grails.plugin.springsecurity.rest.login.endpointUrl = "/auth/login"
grails.plugin.springsecurity.rest.login.failureStatusCode=402

grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = "metasoft.property.api.RestAuthenticationToken"
grails.plugin.springsecurity.rest.token.storage.gorm.tokenValuePropertyName = "authToken"
grails.plugin.springsecurity.rest.token.storage.gorm.usernamePropertyName = "username"
grails.plugin.springsecurity.rest.token.rendering.authoritiesPropertyName="authorities"
grails.plugin.springsecurity.rest.token.storage.useGorm = true

grails.gorm.default.constraints = {
    numberConstraint(scale: 4, max: 9999999999999999.9999)
}

//
metasoft.app.makerChecker.autocommit=false // for cbz this is not enabled.
