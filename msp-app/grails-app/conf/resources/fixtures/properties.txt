name;landlord;property_type;charge_vat;stand_number;area;area-check;commission;property_code;description;property_manager;address_street;address_suburb;address_city;address_country
Kwekwe Beverley house;CBZ Holdings Ltd;commercial;yes;;1205.37;1205.37;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Kwekwe;ZIMBABWE
Apollo Court 153;CBZ Holdings Ltd;commercial;yes;;1403.27;1403.27;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Gweru;ZIMBABWE
Beverley Medical Center 10 MAGAMBA WAY  CHINHOYI ;CBZ Holdings Ltd;commercial;yes;;299.93;299.93;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Chinhoyi;ZIMBABWE
Desai Buliding 383 YORK STREET  CHIVHU ;CBZ Holdings Ltd;commercial;yes;;722.70;722.70;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Chivhu;ZIMBABWE
beverly house Gokwe;CBZ Holdings Ltd;commercial;yes;;289.80;289.80;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Gokwe;ZIMBABWE
274 Hebert Chitepo Town House Chitepo;CBZ Holdings Ltd;residential;no;;1200.00;1200.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
No.40 Hofmeyer Street City Centre  Masvingo;CBZ Holdings Ltd;commercial;yes;;586.33;586.33;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Masvingo;ZIMBABWE
Federal Centre 10th Ave  - Fort St  BULAWAYO ;CBZ Holdings Ltd;commercial;yes;;1421.51;1421.51;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Bulawayo;ZIMBABWE
Southend 144A Main St -  15th Ave  BULAWAYO ;CBZ Holdings Ltd;commercial;yes;;1268.46;1268.46;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Bulawayo;ZIMBABWE
No,3 Aurora Court ;CBZ Holdings Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Bulawayo;ZIMBABWE
No,4 Aurora Court ;CBZ Holdings Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Bulawayo;ZIMBABWE
Fife Street No 101 Fife street -10th Avenue,  BULAWAYO ;CBZ Holdings Ltd;commercial;yes;;2131.16;2131.16;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Bulawayo;ZIMBABWE
Frampton House Cnr 6th Ave   Herbert Chitepo St  BULAWAYO ;CBZ Holdings Ltd;commercial;yes;;1507.63;1507.63;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Bulawayo;ZIMBABWE
Beverley House 116 QUEEN STREET  CHEGUTU ;CBZ Holdings Ltd;commercial;yes;;145.13;145.13;7;;Multi storey building in CBD  area;Blessing Kamutema;;;chegutu;ZIMBABWE
15 George Ave, Chegutu;CBZ Holdings Ltd;commercial;yes;;619.00;619.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;chegutu;ZIMBABWE
Beverley House 225 CHIMANIMANI TOWNSHIP  CHIMANIMANI ;CBZ Holdings Ltd;commercial;yes;;85.50;85.50;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Chimanimani;ZIMBABWE
CBZ Hse, Chiredzi;CBZ Holdings Ltd;commercial;yes;;170.52;170.52;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Chiredzi;ZIMBABWE
Beverly House 537 YORK STREET  CHIVHU ;CBZ Holdings Ltd;commercial;yes;;234.00;234.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Chivhu;ZIMBABWE
Main Branch;CBZ Holdings Ltd;commercial;yes;;65.50;65.50;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Gweru;ZIMBABWE
169, 6th Street mhangura;CBZ Holdings Ltd;residential;no;;180.00;180.00;11;;single tenant residential;Blessing Kamutema;;;Mhangura;ZIMBABWE
NORTHWORLD FLATS CNR 3RD AND TONGOGARA;CBZ Holdings Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
No.8 Queensgate Mtpleasant House;CBZ Holdings Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
EASTLEA  106 McChelry Eastlea Harare ;CBZ Holdings Ltd;commercial;yes;;180.00;180.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
City Center 2 83 ROBERT MUGABE RD  HARARE ;CBZ Holdings Ltd;commercial;yes;;115.00;115.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
No.32 Manhattan Building Cnr R. Mugabe &LTakawira ;CBZ Holdings Ltd;commercial;yes;;501.00;501.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
Sapphire House Corner Angwa and Speke;CBZ Holdings Ltd;commercial;yes;;16.00;16.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
 Beverley Corner 11 SELOUS AVE  HARARE ;CBZ Holdings Ltd;commercial;yes;;1114.08;1114.08;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
 Kamfinsa  1242 GREENDALE  HARARE ;CBZ Holdings Ltd;commercial;yes;;117.70;117.70;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
Manhattan Building CBZ Selous;CBZ Holdings Ltd;commercial;yes;;200.00;200.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
HOUSE No.19 CHIKANGWE Karoi;CBZ Holdings Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;karoi;ZIMBABWE
Beverley Corner STAND 252 FREDDY JAMESON STREET  KAROI ;CBZ Holdings Ltd;commercial;yes;;194.30;194.30;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;karoi;ZIMBABWE
Beverley House STAND NUMBER 395 MUREWA;CBZ Holdings Ltd;commercial;yes;;351.00;351.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;karoi;ZIMBABWE
Beverley Downtown House 2766 MTRE TOWNSHIP  MUTARE ;CBZ Holdings Ltd;commercial;yes;;480.00;480.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Mutare;ZIMBABWE
Beverley House 18 ROBERT MUGABE STREET  RUSAPE ;CBZ Holdings Ltd;commercial;yes;;169.32;169.32;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
Beverley House 694 Turner Road  ZVISHAVANE ;CBZ Holdings Ltd;commercial;yes;;497.99;497.99;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Zvishavane;ZIMBABWE
No.41 Victoria  Road Beverley Corner Newlands;CBZ Pension Fund;commercial;no;;1360.70;1360.70;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
No.17 Conald rd Graniteside Harare;CBZ Pension Fund;commercial;no;;2392.00;2392.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
No.21 Burnley Road,Workington HARARE ;CBZ Pension Fund;commercial;no;;445.25;445.25;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
No.09 stonechat Greystone Park;CBZ Pension Fund;commercial;no;;200.00;200.00;7;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
No.9-13 Craster Road Southerton Harare;CBZ Pension Fund;commercial;no;;161.00;161.00;7;;SINGLE STOREY COMMERCIAL;Blessing Kamutema;;;Harare;ZIMBABWE
Radom Court, 57, 6th Street, Harare;CBZ Pension Fund;residential;no;;800.00;800.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
15 Hornister Road Helensvale ;CBZ Pension Fund;residential;no;;1200.00;1200.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
Beverley House STAND 350 Beitbridge;CBZ Pension Fund;commercial;no;;220.00;220.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Beitbridge;ZIMBABWE
Cnr.J.Chinamano/leeds road No.134;CBZ Bank Ltd;commercial;yes;;338.00;338.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Bulawayo;ZIMBABWE
Stand 23, Falcon, Belmont;CBZ Bank Ltd;commercial;yes;;616.80;616.80;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Bulawayo;ZIMBABWE
No.9-9th Street Mhangura;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Mhangura;ZIMBABWE
11, 1st Street,MHANGURA;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Mhangura;ZIMBABWE
Plot 15 lake Manyame Darwendale;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Darwendale;ZIMBABWE
Stand 2407 Mainway Meadows Waterfalls;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
No.31 Sally Mugabe Redcliff Kwekwe;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Kwekwe;ZIMBABWE
No.343 Borrowdale brook Harare;CBZ Bank Ltd;residential;yes;;200.00;200.00;11;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
59 The Chase, Mt Pleasant;CBZ Bank Ltd;commercial;yes;;200.00;200.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
Stand 583 Mukonono, New Marimba;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
NO. 73 CENTRAL AVENUE Cnr CENTRAL/7th STREET HARARE ;CBZ Bank Ltd;commercial;yes;;892.00;892.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
Stand No.312 Corner Edinburgh/Campbell Roads;CBZ Bank Ltd;commercial;yes;;719.40;719.40;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Harare;ZIMBABWE
Sakubva Complex Stand 10761 Nyakamate Industrial Site;CBZ Bank Ltd;commercial;yes;;1950.00;1950.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Mutare;ZIMBABWE
CBZ HOTEL Stand 25A corner  Second Street/7th Avenue;CBZ Bank Ltd;commercial;yes;;4460.00;4460.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Mutare;ZIMBABWE
Stand 343 A & B Mtora, Nembudziya;CBZ Bank Ltd;commercial;yes;;200.00;200.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Nembudziya;ZIMBABWE
Rockingstone Farm, Rusape;CBZ Bank Ltd;commercial;yes;;89100.00;89100.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Mutare;ZIMBABWE
No.268, Ngangu Township;CBZ Bank Ltd;commercial;yes;;200.00;200.00;7;;Multi storey building in CBD  area;Blessing Kamutema;;;Chimanimani;ZIMBABWE
FBC Masotsha Ndlovhu Cluster Houses;CBZ Bank Ltd;residential;no;;800.00;800.00;11;;;Blessing Kamutema;;;Harare;ZIMBABWE
Beverley House Chipinge;CBZ Holdings Ltd;residential;no;;298.00;298.00;11;;;Blessing Kamutema;;;Chipinge;ZIMBABWE
Sapphire House;CBZ Holdings Ltd;commercial;yes;;32.00;32.00;7;;;Blessing Kamutema;;;Harare;ZIMBABWE
Marondera Beverley House;CBZ Holdings Ltd;commercial;yes;;16.00;16.00;7;;;Blessing Kamutema;;;Marondera;ZIMBABWE
Selous Manhattan Building;CBZ Holdings Ltd;commercial;yes;;16.00;16.00;7;;;Blessing Kamutema;;;Harare;ZIMBABWE
CBZ BANK PROPERTY 7 SELOUS AVE HARARE;CBZ Holdings Ltd;commercial;yes;;283.00;283.00;7;;;Blessing Kamutema;;;Harare;ZIMBABWE
CBZ BANK PROPERTY 168 GREENVALLEY GOKWE ZIMBABWE;CBZ Bank Ltd;commercial;yes;;300.00;300.00;7;;;Blessing Kamutema;;;Gokwe;ZIMBABWE
CBZ BANK PROPERTY GIMBOKI FARM LOT 2 OF KENTUCKY OF HILLANDALE HILLANDALE, MUTARE;CBZ Bank Ltd;commercial;yes;;10000.00;10000.00;7;;;Blessing Kamutema;;;Mutare;ZIMBABWE
Stand 7, 1st Street,MHANGURA;CBZ Bank Ltd;residential;no;;200.00;200.00;11;;;Blessing Kamutema;;;Mhangura;ZIMBABWE
SUBLEASED PROPERTY   STAND NUMBER 19297  COVENTRY ROAD HARARE ;CBZ Bank Ltd;commercial;yes;;200.00;200.00;7;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
No 92 NORFOLK ROAD MT PLEASANT HARARE ;CBZ Bank Ltd;commercial;yes;;1;1;7;;single tenant residential;Blessing Kamutema;;;Harare;ZIMBABWE
