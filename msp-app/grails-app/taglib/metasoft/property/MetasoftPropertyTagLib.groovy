package metasoft.property

import grails.gsp.TagLib
import grails.plugin.springsecurity.SpringSecurityService
import groovy.transform.CompileStatic
import metasoft.property.core.Address
import metasoft.property.core.CheckStatus
import metasoft.property.core.MakerCheckable
import metasoft.property.core.MakerChecker
import metasoft.property.core.SecUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.servlet.support.RequestContextUtils as RCU

@TagLib
class MetasoftPropertyTagLib {

    @Autowired
    SpringSecurityService springSecurityService;

    static defaultEncodeAs = [taglib:'raw']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "mp";

    def address = { attrs, body ->
        Address address = attrs.address;

        out << """<div class="address">
                    <div class="address-line">${address?.street}</div>
                    <div class="address-line">${address?.suburb}</div>
                    <div class="address-line">${address?.city},&nbsp;${address?.country}</div>
                  </div>"""
    }

    def makerChecker = { attrs, body ->
        String labelElement = ""

        MakerChecker value = attrs.value;

        out << stateStatusLabel(status: value.status, text: value.status, cssClass: 'mc-status')
    }

    def makerCheck = { attrs, body ->
        String labelElement = ""

        CheckStatus value = attrs.value;
        String classes = attrs.classes;

        out << stateStatusLabel(status: value.toString()?.toLowerCase(), text: value.toString()?.toLowerCase(), cssClass: "${classes} mc-status")
    }

    def requestStatusLabel = { attrs, body ->
        String labelElement = ""

        Map classMapper = [
                           'error': 'label-danger',
                           'canceled': 'label-danger',
                           'pending': 'label-default',
                           'success': 'label-success',
                           'running': 'label-primary'
        ]
        String labelClass = classMapper.get(attrs.status) ?: 'label-info'; //default to info
        String cssClass = attrs.cssClass ?: '';

        labelElement = "<span class='request-status label ${labelClass} ${cssClass}'>${attrs.text}</span>"

        out << labelElement
    }

    def stateStatusLabel = { attrs, body ->
        String labelElement = ""

        Map classMapper = ['new':    'label-info',
                           'active': 'label-primary',
                           'suspended':  'label-warning',
                           'cancelled':  'label-warning',
                           'terminated': 'label-danger',
                           'error':    'label-danger',
                           'canceled': 'label-danger',
                           'pending':  'label-default',
                           'editing':  'label-info',
                           'success':  'label-success',
                           'running':  'label-primary',
                           'rejected': 'label-danger',
                           'approved': 'label-success',
                           'review':   'label-warning',
        ]
        String labelClass = classMapper.get(attrs.status) ?: 'label-info'; //default to info
        String cssClass = attrs.cssClass ?: '';

        labelElement = "<span class='mc-status label ${labelClass} ${cssClass}'>${attrs.text}</span>"

        out << labelElement
    }

    /**
     * Returns the class name for the provided property status
     */
    def propertyStatusClass = { attrs, body ->

        String classValue = "default";

        switch(attrs.status) {
            case "occupied":
                classValue = "primary";
                break;
            case "un-occupied":
                classValue = "danger"
                break;
        }

        out << classValue;
    }

    /**
     * checks for an error existence for a form-control item
     */
    //@todo: add application code

    /**
     * Returns the class name for the provided property status
     */
    def rentalUnitStatusClass = { attrs, body ->

        String classValue = "defualt";

        switch(attrs.status) {
            case "occupied":
                classValue = "primary";
                break;
            case "un-occupied":
                classValue = "danger"
                break;
        }

        out << classValue;
    }

    /**
     * Returns the class name for the provided property status
     */
    def boolStatusClass = { attrs, body ->

        String classValue = "default";

        switch(attrs.status) {
            case true:
                classValue = "primary";
                break;
            case false:
                classValue = "danger"
                break;
        }

        out << classValue;
    }

    def stateStatusClass = {attrs, body ->

        String clazz = 'info';

        switch(attrs.status) {
            case 'new':
                clazz = 'success'
                break;
            case 'terminated':
                clazz = 'danger';
                break;
            case 'expired':
                clazz = 'warning';
                break;
            case 'active':
                clazz = 'primary';
                break;
        }

        String classValue = "label-"+ clazz;

        String textValue = attrs.text;

        String labelElement = "<span class='mc-status label ${classValue}' >${textValue}</span>"

        out << labelElement
    }

    /**
     * Returns the bootstrap labelled element for the provided property status
     */
    def boolStatusLabel = { attrs, body ->
        String classValue = "label-"+boolStatusClass(status: attrs.status).toString();

        String textValue = "No";

        if(attrs.status) {
            textValue = "Yes";
        }

        String labelElement = "<span class='mc-status label ${classValue}' >${textValue}</span>"

        out << labelElement
    }

    /*
     * This g:paginate tag fix is based on:
     * https://github.com/grails/grails-core/blob/master/grails-plugin-gsp/src/main/groovy/org/codehaus/groovy/grails/plugins/web/taglib/RenderTagLib.groovy
     */

    /**
     * Creates next/previous links to support pagination for the current controller.<br/>
     *
     * &lt;g:paginate total="${Account.count()}" /&gt;<br/>
     *
     * @emptyTag
     *
     * @attr total REQUIRED The total number of results to paginate
     * @attr action the name of the action to use in the link, if not specified the default action will be linked
     * @attr controller the name of the controller to use in the link, if not specified the current controller will be linked
     * @attr id The id to use in the link
     * @attr params A map containing request parameters
     * @attr prev The text to display for the previous link (defaults to "Previous" as defined by default.paginate.prev property in I18n messages.properties)
     * @attr next The text to display for the next link (defaults to "Next" as defined by default.paginate.next property in I18n messages.properties)
     * @attr max The number of records displayed per page (defaults to 10). Used ONLY if params.max is empty
     * @attr maxsteps The number of steps displayed for pagination (defaults to 10). Used ONLY if params.maxsteps is empty
     * @attr offset Used only if params.offset is empty
     * @attr fragment The link fragment (often called anchor tag) to use
     */
    def pagination = { attrs ->
        /*
        def configTabLib = grailsApplication.config.grails.plugins.twitterbootstrap.fixtaglib
        if (!configTabLib) {
            def renderTagLib = grailsApplication.mainContext.getBean('org.codehaus.groovy.grails.plugins.web.taglib.UrlMappingTagLib')
            renderTagLib.paginate.call(attrs)
            return
        } //*/

        def writer = out
        if (attrs.total == null) {
            throwTagError("Tag [paginate] is missing required attribute [total]")
        }
        def messageSource = grailsAttributes.messageSource
        def locale = RCU.getLocale(request)

        def total = attrs.int('total') ?: 0
        def action = (attrs.action ? attrs.action : (params.action ? params.action : "index"))
        def offset = params.int('offset') ?: 0
        def max = params.int('max')
        def maxsteps = (attrs.int('maxsteps') ?: 10)

        if (!offset) offset = (attrs.int('offset') ?: 0)
        if (!max) max = (attrs.int('max') ?: 10)

        def linkParams = [:]
        if (attrs.params) linkParams.putAll(attrs.params)
        linkParams.offset = offset - max
        linkParams.max = max
        if (params.sort) linkParams.sort = params.sort
        if (params.order) linkParams.order = params.order

        def linkTagAttrs = [action:action]
        if (attrs.namespace) {
            linkTagAttrs.namespace = attrs.namespace
        }
        if (attrs.controller) {
            linkTagAttrs.controller = attrs.controller
        }
        if (attrs.id != null) {
            linkTagAttrs.id = attrs.id
        }
        if (attrs.fragment != null) {
            linkTagAttrs.fragment = attrs.fragment
        }
        //add the mapping attribute if present
        if (attrs.mapping) {
            linkTagAttrs.mapping = attrs.mapping
        }

        linkTagAttrs.params = linkParams

        def cssClasses = "pagination"
        if (attrs.class) {
            cssClasses = "pagination " + attrs.class
        }


        // determine paging variables
        def steps = maxsteps > 0
        int currentstep = (offset / max) + 1
        int firststep = 1
        int laststep = Math.round(Math.ceil(total / max))

        writer << "<ul class=\"${cssClasses}\">"

        // display previous link when not on firststep
        if (currentstep > firststep) {
            linkParams.offset = offset - max
            writer << '<li class="prev">'
            writer << link(linkTagAttrs.clone()) {
                (attrs.prev ?: messageSource.getMessage('paginate.prev', null, '&laquo;', locale))
            }
            writer << '</li>'
        }
        else {
            writer << '<li class="prev disabled">'
            writer << '<span>'
            writer << (attrs.prev ?: messageSource.getMessage('paginate.prev', null, '&laquo;', locale))
            writer << '</span>'
            writer << '</li>'
        }

        // display steps when steps are enabled and laststep is not firststep
        if (steps && laststep > firststep) {
            linkTagAttrs.class = 'step'

            // determine begin and endstep paging variables
            int beginstep = currentstep - Math.round(maxsteps / 2) + (maxsteps % 2)
            int endstep = currentstep + Math.round(maxsteps / 2) - 1

            if (beginstep < firststep) {
                beginstep = firststep
                endstep = maxsteps
            }
            if (endstep > laststep) {
                beginstep = laststep - maxsteps + 1
                if (beginstep < firststep) {
                    beginstep = firststep
                }
                endstep = laststep
            }

            // display firststep link when beginstep is not firststep
            if (beginstep > firststep) {
                linkParams.offset = 0
                writer << '<li>'
                writer << link(linkTagAttrs.clone()) {firststep.toString()}
                writer << '</li>'
                writer << '<li class="disabled"><span>...</span></li>'
            }

            // display paginate steps
            (beginstep..endstep).each { i ->
                if (currentstep == i) {
                    writer << "<li class=\"active\">"
                    writer << "<span>${i}</span>"
                    writer << "</li>";
                }
                else {
                    linkParams.offset = (i - 1) * max
                    writer << "<li>";
                    writer << link(linkTagAttrs.clone()) {i.toString()}
                    writer << "</li>";
                }
            }

            // display laststep link when endstep is not laststep
            if (endstep < laststep) {
                writer << '<li class="disabled"><span>...</span></li>'
                linkParams.offset = (laststep -1) * max
                writer << '<li>'
                writer << link(linkTagAttrs.clone()) { laststep.toString() }
                writer << '</li>'
            }
        }

        // display next link when not on laststep
        if (currentstep < laststep) {
            linkParams.offset = offset + max
            writer << '<li class="next">'
            writer << link(linkTagAttrs.clone()) {
                (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, '&raquo;', locale))
            }
            writer << '</li>'
        }
        else {
            linkParams.offset = offset + max
            writer << '<li class="disabled">'
            writer << '<span>'
            writer << (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, '&raquo;', locale))
            writer << '</span>'
            writer << '</li>'
        }

        writer << '</ul>'
    }

    def activeController = { attrs ->

        String actionName = attrs?.actionName;

        if(attrs.controllerName instanceof List) {
            if(attrs.controllerName.contains(controllerName)) {
                out << "active open"
            }
        }
        else if(controllerName == attrs.controllerName ) {
            out << "active"
        }
    }

    def navItemSelected = { attrs ->
        if(attrs.controllerName instanceof List) {
            if(attrs.controllerName.contains(controllerName)) {
                out << "<span class='selected'></span>"
            }
        }
        else if(controllerName == attrs.controllerName ) {
            out << "<span class='selected'></span>"
        }
    }

    def noRecordsFound = { attrs ->
        out << g.message(message: 'default.records.notfound')
    }

    /**
     * Creates a new password field.
     *
     * @attr bean REQUIRED the MakerCheckable bean
     */
    Closure editRecord = { attrs, body ->
        SecUser currentUser = (SecUser)springSecurityService.currentUser;
        MakerCheckable object = attrs.bean;
        SecUser maker = object.dirtMaker;

        // go ahead and create the edit tag
        if((maker == currentUser && object.isRecordDirty()) || !object.isRecordDirty()) {
            out << g.link(attrs, body);
        } else { // this user is not the owner of the record, do not create a valid tab
            out << """<button type="button" class="disabled ${attrs.class}">
                    ${body}
                    </button>"""
        }
    }
}
