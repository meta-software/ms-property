package metasoft.property

class MetasoftPentahoTagLib {

    static namespace = 'pentaho';

    static defaultEncodeAs = [taglib:'raw'];
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def report = { attrs, body ->

        String reportApi = grailsApplication.config.getProperty("metasoft.config.reportserver.pentaho.apiUrl")
        String reportPath = attrs.src;
        String reportUrl = "${reportApi}/${reportPath}/generatedContent";

        out << "<iframe scrolling='yes' @load='loadIframe()' name='_reportIframe' ref='reportIframe' src=''" +
               " style='border: 1px solid #ddd; position:relative;top:0px;width:100%;height:85vh; '>\n" +
               "</iframe>";
    }

    /**
     * Retrieves the report parameters for a given report (prpt) file from the configured report server
     *
     */
    def parameter = { attrs, body ->
        String reportApi = grailsApplication.config.getProperty("metasoft.config.reportserver.pentaho.apiUrl")
        String reportPath = attrs.src;
        String reportUrl = "${reportApi}/${reportPath}/parameter"; // suppress the pentaho

        //@todo: you need to be logged in to be able to query for the report parameters.
    }

    /**
     * Returns the pentahoUrl to use for posting a url to the REST resource on the pentaho server
     */
    def reportUrl =  { attrs ->
        String reportApi = grailsApplication.config.getProperty("metasoft.config.reportserver.pentaho.apiUrl")
        String reportPath = attrs.src;
        String url = "${reportApi}/${reportPath}/generatedContent"; // suppress the pentaho

        out << url;
    }
}
