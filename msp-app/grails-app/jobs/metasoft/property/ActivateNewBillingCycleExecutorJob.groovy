
package metasoft.property

import groovy.util.logging.Slf4j
import metasoft.property.core.BillingCycle
import metasoft.property.core.BillingCycleService
import org.quartz.impl.JobDetailImpl
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class ActivateNewBillingCycleExecutorJob {

    @Autowired
    BillingCycleService billingCycleService

    String name = "ActivateNewBillingCycle"

    static concurrent = false;  // we just one instance of the job at any time running.

    static triggers = {
        cron name:'activateNewBillingCycleCronTrigger', cronExpression: '0 0 1 1 * ? *' //Fire on first day of each month at 0100AM
        //cron name:'activateNewBillingCycleCronTriggers', cronExpression: '0/10 * * * * ? *' //Fire on first day of each month at 0200AM
    }

    def execute() {
        //BillingCycle billingCycle = billingCycleService.activateNewBillingCycle();
        log.info("Successfully activated the billingCycle @");
        //@todo: add code here to catch and act on what happens when errors are encountered.
    }
}
