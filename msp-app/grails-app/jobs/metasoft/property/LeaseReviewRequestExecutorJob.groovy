package metasoft.property

import metasoft.property.core.LeaseReviewService
import org.springframework.beans.factory.annotation.Autowired

class LeaseReviewRequestExecutorJob {

    @Autowired
    LeaseReviewService leaseReviewService

    String name = "LeaseReviewRequest";
    static concurrent = false;  // we just one instance of the job at any time running.

    static triggers = {
        cron name:'leaseReviewRequestTrigger', startDelay:30000, cronExpression: '0 30 1 * * ? *' //Fire daily @0130
    }

    def execute() {
        log.info("@${new Date().format('yyyyMMddHHmmss')} :::: Running the LeaseReviewRequestExecutor::execute() method!")
        leaseReviewService.processReviewLeases();
    }
}
