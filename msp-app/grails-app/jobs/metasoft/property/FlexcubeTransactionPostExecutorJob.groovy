package metasoft.property

import groovy.util.logging.Slf4j
import metasoft.property.api.v1.FlexcubeTransactionService
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class FlexcubeTransactionPostExecutorJob {

    @Autowired
    FlexcubeTransactionService flexcubeTransactionService

    // disable concurrent jobs
    static concurrent = false;

    String name = "flexcubeTransactionPostJob";

    static triggers = {
        //cron name: 'flexcubeTransactionCronTrigger', cronExpression: "0 0/10 * * * ?", startDelay: 10000; //run every 10 minutes
        //simple repeatInterval: 300_000L, startDelay: 300_000L // execute job once in 5 minutes //seconds
    }

    def execute() {
        log.info("START making the post-requests for the pending flexcube transactions");
        flexcubeTransactionService.postTransactions();
    }
}
