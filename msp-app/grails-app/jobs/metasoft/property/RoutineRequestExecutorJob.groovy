package metasoft.property

import metasoft.property.core.RoutineRequestService
import org.springframework.beans.factory.annotation.Autowired

class RoutineRequestExecutorJob {

    @Autowired
    RoutineRequestService routineRequestService

    static concurrent = false;  // we just one instance of the job at any time running.
    //static name = "ROUTINE_REQUEST";
    //static group = "RoutineRequestGroup";
    //static description = "Processes the requested routine requests"

    static triggers = {
        //cron name:'routineRequestCronTrigger', startDelay:5000, cronExpression: '0/10 * * * * ? *' //Fire every 2 hours daily
        cron name:'routineRequestCronTrigger', startDelay:30000, cronExpression: '0 0 0/2 * * ? *' //Fire every 2 hours daily
    }

    def execute() {
        //log.info("process pending routine requests")
        routineRequestService.processRequests()
    }
}

//00 15 10 15 ** ?? // -- run at 10:15AM on the 15 day of each month
//SS MM HH DD MM WW