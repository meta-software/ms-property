package metasoft.property

import metasoft.property.core.BillingCycleService
import org.springframework.beans.factory.annotation.Autowired

class CloseBillingCycleExecutorJob {

    @Autowired
    BillingCycleService billingCycleService

    String name = "CloseBillingCycle";
    String description = "Close the latest previous billing cycle automatically";
    String group = "BillingCycleGroup"

    static concurrent = false;  // we just one instance of the job at any time running.

    static triggers = {
        cron name:'closeBillingCycleCronTrigger', startDelay:60000, cronExpression: '0 0 5 8 * ? *' //Fire on 8th day of each month @0500 AM
    }

    def execute() {
        log.info("executing the job to close the previous billing cycle")
        billingCycleService.closePreviousBillingCycle();
    }
}
