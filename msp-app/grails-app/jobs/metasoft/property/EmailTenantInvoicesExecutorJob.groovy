package metasoft.property

import metasoft.property.core.TenantInvoiceEmailService
import org.springframework.beans.factory.annotation.Autowired

class EmailTenantInvoicesExecutorJob {

    @Autowired
    TenantInvoiceEmailService tenantInvoiceEmailService

    static concurrent = false;  // we just one instance of the job at any time running.
    String name = "EmailTenantBillingInvoice";
    String description = "Prepares email requests for all applicable tenant invoices"

    static triggers = {
        cron name:'emailTenantInvoiceCronTrigger', startDelay:30000, cronExpression: '0 0 2 2 * ? *' //Fire on 2nd day of each month at 2 AM
    }

    def execute() {
        log.debug("START the job to automatically send BillingInvoice emails to tenant accounts")
        tenantInvoiceEmailService.sendTenantBillingInvoices();
    }
}
