package metasoft.property

import metasoft.property.core.BillingCycleService
import metasoft.property.core.RoutineRequestService
import org.springframework.beans.factory.annotation.Autowired

class RentDueRunExecutorJob {

    @Autowired
    RoutineRequestService routineRequestService;

    @Autowired
    BillingCycleService billingCycleService;

    static concurrent = false;  // we just one instance of the job at any time running.
    //static name = "RENT_DUE_RUN";

    static triggers = {
        //cron name:'rentDueRunTrigger', startDelay:300_000L, cronExpression: '0 0 1 1 * ? *' //Fire on the 1st day of each month @0100AM
    }

    def execute() {
        log.info("@Request to process the executeRentDueRun:run action for all eligible accounts");

        try{
            routineRequestService.executeRentDueRun();
        } catch(Exception e) {
            log.error("ERROR: error occurred while executing the rent-due-run: " + e.message);
            e.printStackTrace();
        }
    }
}
