package metasoft.property

import groovy.util.logging.Slf4j
import metasoft.property.core.EmailSendRequestService
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class EmailSendRequestExecutorJob {

    EmailSendRequestService emailSendRequestService

    // disable concurrent jobs
    static concurrent = false;

    String name = "DispatchEmailRequests";
    String group = "Emailing"
    String description = "Dispatches the pending email requests"

    static triggers = {
        cron name: 'emailSendRequestCronTrigger', cronExpression: "0 0/5 * * * ?", startDelay: 10000; //run every 5 minutes
        //simple repeatInterval: 10_000L, startDelay: 10_000L // execute job once in 5 minutes //seconds
    }

    def execute() {
        log.trace("execute the EmailSendRequestJob: process pending email requests");
        emailSendRequestService.processPendingRequests();
    }
}
