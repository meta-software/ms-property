package metasoft.property

import metasoft.property.core.BulkAdjustService
import org.springframework.beans.factory.annotation.Autowired

class BulkAdjustmentExecutorJob {

    @Autowired
    BulkAdjustService bulkAdjustService

    String name = "BulkAdjust";
    String description = "Bulk adjustment of least item settings values"
    String group = "LeaseGroup"

    static concurrent = false;  // we just one instance of the job at any time running.

    static triggers = {
        cron name:'bulkAdjustCronTrigger', startDelay:30000, cronExpression: '0 0 0/2 * * ? *' //Fire at 02:30AM every day
    }

    def execute() {
        log.info("executing the BulkAdjustmentJob")
        bulkAdjustService.processAdjustments();
    }
}
