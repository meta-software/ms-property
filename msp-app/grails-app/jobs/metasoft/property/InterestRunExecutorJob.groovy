package metasoft.property

import groovy.util.logging.Slf4j
import metasoft.property.core.RoutineRequestService
import metasoft.property.core.RoutineType
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
class InterestRunExecutorJob {

    @Autowired
    RoutineRequestService routineRequestService

    static concurrent = false;  // we just one instance of the job at any time running.
    String name = "InterestRun";
    String group = "RoutineRequestGroup";

    static triggers = {
        cron name:'interestRunTrigger', cronExpression: '0 30 3 8 * ? *' //Fire on the 8th day of each month @0330AM
    }

    def execute() {
        log.info("@Request to run the interest:run for all eligible accounts");

        RoutineType routineType = RoutineType.findByName("Interest Run");
        try {
            routineRequestService.executeRoutine(routineType);
        } catch(Exception e) {
            log.error("error occurred while executing the interest-run: " + e.message);
            e.printStackTrace();
        }
    }
}
