package metasoft.property

import metasoft.property.core.LeaseService
import org.springframework.beans.factory.annotation.Autowired

/**
 * processes all leases that have expired and updates the expired flag and the status accordingly.
 *
 */
class LeaseExpiredExecutorJob {

    @Autowired
    LeaseService leaseService ;

    String description = "Expire all eligible leases"
    String name = "leaseExpireJob";
    static concurrent = false;  // we just one instance of the job at any time running.


    static triggers = {
        cron name:'leaseExpireCronTrigger', cronExpression: '0 0 3 * * ? *' //Fire every day @0300AM
    }

    def execute() {
        log.info("START: process all the expired leases by date")
        leaseService.processExpiredLeases()
    }
}
