package metasoft.property

import metasoft.property.core.LeaseTerminateRequestService
import org.springframework.beans.factory.annotation.Autowired

class LeaseTerminateRequestExecutorJob {

    @Autowired
    LeaseTerminateRequestService leaseTerminateRequestService

    String name = "LeaseTerminateRequest";
    static concurrent = false;  // we just want one instance of the job at any time running.

    void setName(String name) {
        this.name = name;
    }

    static triggers = {
        cron name:'leaseReviewRequestTrigger', startDelay:300000, cronExpression: '0 0 0/1 * * ? *' //Fire every 1 hour
    }

    def execute() {
        log.info("@LeaseTerminateRequestExecutorJob::execute() process all pending lease terminate requests")
        leaseTerminateRequestService.processRequests()
    }
}
