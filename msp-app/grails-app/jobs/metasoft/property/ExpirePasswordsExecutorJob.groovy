package metasoft.property

import metasoft.property.core.UserService
import org.springframework.beans.factory.annotation.Autowired

class ExpirePasswordsExecutorJob {

    @Autowired
    UserService userService

    static concurrent = false;  // we just one instance of the job at any time running.
    String name = "ExpirePasswords";
    String group = "SecurityGroup"

    static triggers = {
        cron name: 'expirePasswordsCronTrigger', cronExpression: "0 0 3 * * ?"; //run once daily @0300AM
        /*
        cronExpression: "s m h D M W Y"
                         | | | | | | `- Year [optional]
                         | | | | | `- Day of Week, 1-7 or SUN-SAT, ?
                         | | | | `- Month, 1-12 or JAN-DEC
                         | | | `- Day of Month, 1-31, ?
                         | | `- Hour, 0-23
                         | `- Minute, 0-59
                         `- Second, 0-59
        */
    }

    def execute() {
        log.info("start job to expire passwords expiring on @${new Date().format("yyyy-MM-dd")}")
        userService.expirePasswords();
    }
}
