package metasoft.property.core

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class RentalUnitTmpServiceSpec extends Specification {

    RentalUnitTmpService rentalUnitTmpService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new RentalUnitTmp(...).save(flush: true, failOnError: true)
        //new RentalUnitTmp(...).save(flush: true, failOnError: true)
        //RentalUnitTmp rentalUnitTmp = new RentalUnitTmp(...).save(flush: true, failOnError: true)
        //new RentalUnitTmp(...).save(flush: true, failOnError: true)
        //new RentalUnitTmp(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //rentalUnitTmp.id
    }

    void "test get"() {
        setupData()

        expect:
        rentalUnitTmpService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<RentalUnitTmp> rentalUnitTmpList = rentalUnitTmpService.list(max: 2, offset: 2)

        then:
        rentalUnitTmpList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        rentalUnitTmpService.count() == 5
    }

    void "test delete"() {
        Long rentalUnitTmpId = setupData()

        expect:
        rentalUnitTmpService.count() == 5

        when:
        rentalUnitTmpService.delete(rentalUnitTmpId)
        sessionFactory.currentSession.flush()

        then:
        rentalUnitTmpService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        RentalUnitTmp rentalUnitTmp = new RentalUnitTmp()
        rentalUnitTmpService.save(rentalUnitTmp)

        then:
        rentalUnitTmp.id != null
    }
}
