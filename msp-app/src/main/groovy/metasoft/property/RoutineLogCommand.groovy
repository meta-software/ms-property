package metasoft.property

import grails.validation.Validateable
import metasoft.property.core.RoutineLog
import metasoft.property.core.BillingCycle

/**
 * Created by lebohof on 10/21/2018.
 */
class RoutineLogCommand implements Validateable {

    BillingCycle period; //The billing cycle for which this run was for

    String routineType;

    static constraints = {
        routineType inList: ['interest', 'rent', 'op-cos']
    }

    Object asType(Class type) {

        if(type == RoutineLog.class) {
            RoutineLog automaticRoutineLog = new RoutineLog(this.properties);

            return automaticRoutineLog;
        }

        return null;
    }

}
