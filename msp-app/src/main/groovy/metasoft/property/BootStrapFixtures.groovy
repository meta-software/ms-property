package metasoft.property

import grails.core.GrailsApplication
import groovy.util.logging.Slf4j
import metasoft.property.core.AccountCategory
import metasoft.property.core.AccountType
import metasoft.property.core.Address
import metasoft.property.core.CheckStatus
import metasoft.property.core.Country
import metasoft.property.core.Currency
import metasoft.property.core.CurrencyService
import metasoft.property.core.GeneralLedger
import metasoft.property.core.GeneralLedgerService
import metasoft.property.core.Landlord
import metasoft.property.core.LandlordService
import metasoft.property.core.Lease
import metasoft.property.core.LeaseItem
import metasoft.property.core.LeaseItemService
import metasoft.property.core.LeaseNumber
import metasoft.property.core.LeaseService
import metasoft.property.core.LeaseStatus
import metasoft.property.core.LeaseType
import metasoft.property.core.MetasoftUtil
import metasoft.property.core.Property
import metasoft.property.core.PropertyManager
import metasoft.property.core.PropertyService
import metasoft.property.core.PropertyType
import metasoft.property.core.Provider
import metasoft.property.core.ProviderService
import metasoft.property.core.ReceiptBatchCheck
import metasoft.property.core.ReceiptBatchCheckService
import metasoft.property.core.ReceiptBatchService
import metasoft.property.core.ReceiptBatchTmp
import metasoft.property.core.ReceiptBatchTmpService
import metasoft.property.core.ReceiptItemTmp
import metasoft.property.core.ReceiptTmp
import metasoft.property.core.ReceiptTmpService
import metasoft.property.core.RentalUnit
import metasoft.property.core.RentalUnitStatus
import metasoft.property.core.SubAccount
import metasoft.property.core.Tenant
import metasoft.property.core.TenantService
import metasoft.property.core.TransactionBatch
import metasoft.property.core.TransactionBatchTmp
import metasoft.property.core.TransactionBatchTmpService
import metasoft.property.core.TransactionTmpService
import metasoft.property.core.TransactionType
import metasoft.property.core.UserService

import org.grails.datastore.mapping.validation.ValidationException
import org.hibernate.Session
import org.hibernate.Transaction
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.TransactionStatus
import grails.gorm.transactions.Transactional

@Slf4j
class BootStrapFixtures {

    GrailsApplication grailsApplication;
    TenantService tenantService;
    LandlordService landlordService;
    ProviderService providerService;
    PropertyService propertyService;
    LeaseService leaseService;
    UserService userService
    TransactionTmpService transactionTmpService;
    TransactionBatchTmpService transactionBatchTmpService;
    ReceiptTmpService receiptTmpService

    @Autowired
    ReceiptBatchTmpService receiptBatchTmpService
    ReceiptBatchCheckService receiptBatchCheckService

    def loadLedgerBalances() {
        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def balancesFilepath = "resources/fixtures/ledger-balances.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${balancesFilepath}");

        def counter = 0;
        File errorsFile = new File(System.getProperty('user.home')+"/ledger.errors.txt")
        errorsFile.write("")

        fixturesResource.file.splitEachLine(";") { fields ->
            counter++;

            if(!fields[1]){
                println(fields)
                return
            }

            TransactionType transactionType = TransactionType.get(7);
            TransactionBatchTmp batch = null;
            TransactionBatchTmp.withNewTransaction { TransactionStatus status ->
                batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
                log.info "finished saving: {}", batch.id
            }

            def ledgerAccount = fields[0]
            def accountName = fields[1]
            def txnAmount = fields[3] as BigDecimal
            def txnCurrency = fields[2]

            def balanceParams = [
                    creditAccount: ledgerAccount,
                    debitAccount: ledgerAccount,
                    description: "Balance Migration for ledger [${accountName}] : [${accountName}]",
                    transactionBatchTmp: batch.id,
                    transactionCurrency: txnCurrency,
                    transactionType: [id: transactionType.id],
                    transactionDate: new Date().parse("yyyy-MM-dd", '2023-02-28'),
                    amount: txnAmount
            ]

            //now we create the transactions
            transactionTmpService.balance(balanceParams);

            //*/
            //now post the batch here.
            //post the transactionBatch.
            batch.refresh();
            log.info "posting the transaction batch with items-count: ${batch.transactionTmps}"
            TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(batch);

            log.info "finished with transaction-batch ::: [${transactionBatch.id}] ",
            batch.autoCommitted = true;
            batch.postedBy = "system";
            batch.datePosted = new Date();
            batch.save(flush: true);

            log.info "SUCCESSFULLY FINISHED loading ledger-balance${fields}]"

        }
        log.info "FINISHED loading balances data @${new Date().format('yyyy-MM-dd HH:mm:ss')} "

    }

    def loadBalances() {
        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def balancesFilepath = "resources/fixtures/tenant-balances.txt";
        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${balancesFilepath}");

        def counter = 0;
        File errorsFile = new File(System.getProperty('user.home')+"/balances.errors.txt")
        errorsFile.write("")

        fixturesResource.file.splitEachLine(";") { fields ->
            counter++;

            if(!fields[1]){
                println(fields)
                return
            }

            //log.info "loading the balance details for tenant = [${fields[0].trim()}], rental-unit=[${fields[1]}]"
            //*
            //get the tenant information
            Lease lease = Lease.findByLeaseNumber(fields[1].trim(), [readOnly: true]);
            if(lease == null) {
                log.error "[lease] the [leaseNumber:${fields[1].trim()}] was not found."
                return
            }
            Tenant eventTenant = lease.tenant;

            TransactionType transactionType = TransactionType.get(7);
            TransactionBatchTmp batch = null;
            TransactionBatchTmp.withNewTransaction { TransactionStatus status ->
                batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
                log.info "finished saving: {}", batch.id
            }

            BigDecimal amount = 0;

            try {
                String value = fields[0]?.trim()
                amount = new BigDecimal(value)
            } catch(NumberFormatException nfe) {
                log.error "error loading balance for lease [${lease.leaseNumber}] for value amount = [${fields[0]}]"
            }

            //retrieve the rentalUnit
            RentalUnit rentalUnit = lease.rentalUnit;
            def balanceParams = [
                    rentalUnit: [id: rentalUnit.id],
                    lease: [id: lease.id],
                    transactionType: [id: transactionType.id],
                    entryNumber: null,
                    transactionDate: new Date().parse("yyyy-MM-dd", '2022-08-31'),
                    creditAccount: eventTenant.accountNumber,
                    debitAccount: eventTenant.accountNumber,
                    amount: amount,
                    transactionCurrency: 'ZWL',
                    description: "BAL Migration for rental-unit: [${lease.rentalUnit.unitReference}]",
                    transactionBatchTmp: [id: batch.id]
            ];

            //log.error "processing: {}", balanceParams
            //now we create the transactions
            transactionTmpService.balance(balanceParams);

            //*/
            //now post the batch here.
            //post the transactionBatch.
            batch.refresh();
            log.info "posting the transaction batch with items-count: ${batch.transactionTmps}"
            TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(batch);
            log.info "finished with transaction-batch ::: [${transactionBatch.id}] ",

            batch.autoCommitted = true;
            batch.postedBy = "system";
            batch.datePosted = new Date();
            batch.save(flush: true);

            log.info "SUCCESSFULLY FINISHED loading date for[lease=${fields[1].trim()}:tenant=${eventTenant.accountName}]"

            //@do another for the landlord
            loadLandlordBalances(balanceParams, lease);

        }
        log.info "FINISHED loading balances data @${new Date().format('yyyy-MM-dd HH:mm:ss')} "

    }

    def loadLandlordBalances(Map balanceParams, Lease lease) {
        //Read the sample.fix fixtures file for testing purposes.
        log.info("START: preparing the balance transactions for lease=[${lease.leaseNumber}]")

        Landlord landlord = lease.landlord;
        TransactionType transactionType = TransactionType.get(7);

        TransactionBatchTmp batch = null;
        TransactionBatchTmp.withNewTransaction { TransactionStatus status ->
            batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
            log.info "finished saving: {}", batch.id
        }

         //retrieve the rentalUnit
        RentalUnit rentalUnit = lease.rentalUnit;
        balanceParams['creditAccount'] = landlord.accountNumber;
        balanceParams['debitAccount'] = landlord.accountNumber;
        balanceParams['description'] = "Balance Migration for landlord [${landlord.accountName}] for rental-unit: [${rentalUnit.unitReference}]";
        balanceParams['transactionBatchTmp'] = [id: batch.id]

        //log.error "processing: {}", balanceParams
        //now we create the transactions
        transactionTmpService.balance(balanceParams);

        //now post the batch here.
        //post the transactionBatch.
        batch.refresh();
        log.info "posting the transaction batch with items-count: ${batch.transactionTmps}"
        TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(batch);
        log.info "finished with transaction-batch ::: [${transactionBatch.id}] ",

        batch.autoCommitted = true;
        batch.postedBy = "system";
        batch.datePosted = new Date();
        batch.save(flush: true);

        log.info "SUCCESSFULLY FINISHED loading the landlord details for [${lease}]"
    }

    def loadTenants() {

        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def personalAccountsFilepath = "resources/fixtures/personal.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${personalAccountsFilepath}");

        def counter = 0;
        AccountCategory category = AccountCategory.findByCode('tenant');

        File errorsFile = new File(System.getProperty('user.home')+"/personal.errors.txt")
        errorsFile.write("")

        fixturesResource.file.splitEachLine(",") { fields ->

            log.info "loading the tenant details = [${fields[1]?.trim()}]"

            AccountType acctType = AccountType.findByCode(fields[8] ?: 'company')

            // clean the email address
            String emailAddress = fields[10]
            if(emailAddress?.contains(";")) {
                emailAddress = emailAddress.replaceAll('("|\')', "").tokenize(';')[0]
            }

            // clean the phoneNumber
            String phoneNumber = fields[14]
            if(phoneNumber?.contains("/")) {
                phoneNumber = phoneNumber.replaceAll('("|\')', "").tokenize('/')[0]
            }

            // clean the mobileNumber
            String mobileNumber = fields[13]
            if(mobileNumber?.contains("/")) {
                mobileNumber = mobileNumber.replaceAll('("|\')', "").tokenize('/')[0]
            }

            String accountName = fields[1].replaceAll("[^0-9A-Za-z-]", " ")
            accountName = accountName.replaceAll("\\s+", " ")

            Map record = [
                    //postalAddress: fields[2],
                    accountName: accountName,
                    phoneNumber: phoneNumber,
                    //physicalAddress: fields[5],
                    accountType: acctType,
                    accountCategory: category,
                    vatNumber: fields[9],
                    emailAddress: emailAddress,
                    bpNumber: fields[12],
                    mobileNumber: mobileNumber,
                    bankAccountNumber: fields[0]
                    //businessAddress: fields[11]
            ];

            Tenant tenant = new Tenant(record);

            if(!tenant.validate()) {

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << tenant.errors.toString()?.replaceAll("\n", "|");
                errorsFile << fields1.join("\t") + "\n";

                tenant.discard()
            } else {
                tenant.maker     = userService.systemUser;
                tenant.makeDate  = new Date();
                tenant.checker   = userService.systemUser;
                tenant.checkDate = new Date();
                tenant.checkStatus = CheckStatus.APPROVED;
                tenant.dirtyStatus = CheckStatus.APPROVED;
                tenant.dirtMaker = userService.systemUser;

                try{
                    tenantService.save(tenant)
                } catch(ValidationException e) {
                    log.info "error while trying to save tenant [$tenant]"
                    println  "the location: ${System.getProperty('user.dir')}"
                    println "###### /> " + e.message
                }
            }

            log.info "SUCCESSFULLY FINISHED loading the tenant details = [${fields[1]?.trim()}]"
        }

        log.info("finished loading ALL tenant data: loaded $counter tenants successfully.")
    }

    def loadLandlords() {

        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def personalAccountsFilepath = "resources/fixtures/landlord.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${personalAccountsFilepath}");

        def counter = 0;
        fixturesResource.file.splitEachLine("\t") { fields ->
            log.info "loading the landlord details = [${fields[0].trim()}]"

            if(counter++ < 1) return;

            AccountType acctType = AccountType.findByCode(fields[6] ?: 'company')
            AccountCategory category = AccountCategory.findByCode('landlord');

            // clean the email address
            String emailAddress = fields[8]
            if(emailAddress?.contains(";")) {
                emailAddress = emailAddress.replaceAll('"', "").tokenize(';')[0]
            }

            Map record = [
                    postalAddress: fields[2],
                    accountName: fields[3],
                    phoneNumber: fields[4],
                    physicalAddress: fields[5],
                    accountType: acctType,
                    accountCategory: category,
                    vatNumber: fields[7],
                    emailAddress: emailAddress,
                    bpNumber: fields[9],
                    mobileNumber: fields[10],
                    businessAddress: fields[11]
            ];

            Landlord landlord = new Landlord(record);

            if(!landlord.validate()) {
                println(landlord.errors);
            } else {
                landlord.maker     = userService.systemUser;
                landlord.makeDate  = new Date();
                landlord.checker   = userService.systemUser;
                landlord.checkDate = new Date();
                landlord.checkStatus = CheckStatus.APPROVED;
                landlord.dirtyStatus = CheckStatus.APPROVED;
                landlord.dirtMaker = userService.systemUser;

                try{
                    landlordService.save(landlord)
                } catch(ValidationException e) {
                    log.error "error while trying to save landlord record [$landlord]"
                    log.error(e.message)
                }
            }

            //landlord.discard();
            log.info "SUCCESSFULLY FINISHED loading the landlord details = [${fields[0].trim()}]"
        }

        log.info("finished loading ALL landlords, loaded: $counter landlords.")
    }

    def loadSuppliers() {

        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def supplierAccountsFilepath = "resources/fixtures/suppliers.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${supplierAccountsFilepath}");

        def counter = 0;
        AccountCategory category = AccountCategory.findByCode('trust');

        File errorsFile = new File(System.getProperty('user.home')+"/provider.errors.txt")
        errorsFile.write("")

        fixturesResource.file.splitEachLine("\t") { fields ->
            log.info "loading the supplier details = [${fields[0].trim()}]"

            AccountType acctType = AccountType.findByCode(fields[6] ?: 'company')

            // clean the email address
            String emailAddress = fields[8]
            if(emailAddress?.contains(";")) {
                emailAddress = emailAddress.replaceAll('("|\')', "").tokenize(';')[0]
            }

            // clean the phoneNumber
            String phoneNumber = fields[4]
            if(phoneNumber?.contains("/")) {
                phoneNumber = phoneNumber.replaceAll('("|\')', "").tokenize('/')[0]
            }

            // clean the mobileNumber
            String mobileNumber = fields[10]
            if(mobileNumber?.contains("/")) {
                mobileNumber = mobileNumber.replaceAll('("|\')', "").tokenize('/')[0]
            }

            Map record = [
                    postalAddress: fields[2],
                    accountName: fields[3],
                    phoneNumber: phoneNumber,
                    physicalAddress: fields[5],
                    accountType: acctType,
                    accountCategory: category,
                    vatNumber: fields[7],
                    emailAddress: emailAddress,
                    bpNumber: fields[9],
                    mobileNumber: mobileNumber,
                    businessAddress: fields[11]
            ];

            Provider provider = new Provider(record);

            if(!provider.validate()) {

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << provider.errors.toString()?.replaceAll("\n", "|");
                errorsFile << fields1.join("\t") + "\n";

                provider.discard()
            } else {
                provider.maker     = userService.systemUser;
                provider.makeDate  = new Date();
                provider.checker   = userService.systemUser;
                provider.checkDate = new Date();
                provider.checkStatus = CheckStatus.APPROVED;
                provider.dirtyStatus = CheckStatus.APPROVED;
                provider.dirtMaker = userService.systemUser;

                try{
                    providerService.save(provider)
                } catch(ValidationException e) {
                    log.info "error while trying to save provider [$provider]"
                    println "###### /> " + e.message
                }
            }

            log.info "SUCCESSFULLY FINISHED supplier the tenant details = [${fields[0].trim()}]"
        }

        log.info("finished loading ALL supplier data: loaded $counter suppliers successfully.")
    }

    def loadProperties() {
        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def propertiesFilepath = "resources/fixtures/properties.txt";
        def rentalUnitsFilepath = "resources/fixtures/rental-units.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${propertiesFilepath}");

        File errorsFile = new File(System.getProperty('user.home')+"/properties.errors.txt")
        errorsFile.write("")

        def counter = 0;
        fixturesResource.file.splitEachLine(";") { fields ->
            log.info "loading the property details = [${fields[0].trim()}]"

            PropertyType propertyType = PropertyType.findByCode(fields[2])

            if(propertyType == null) {
                propertyType = PropertyType.findByCode('commercial');
            }

            boolean chargeVat = 'yes'.equalsIgnoreCase(fields[3]?.trim());

            BigDecimal propertyArea = 0.0
            BigDecimal propertyCommission = 0.0 ;

            try {
                String areaString = fields[5]?.trim();
                if(areaString == null || areaString.empty) {
                    areaString = '1.0'
                }
                propertyArea = new BigDecimal(areaString);
            } catch(NumberFormatException e) {
                log.error "[propertyArea] ${e.getClass().name} with error: ${e.message} for value ${fields[5]}"
            }

            try {
                propertyCommission = new BigDecimal(fields[7] ?: "0.0");
            } catch(NumberFormatException e) {
                log.error "[propertyCommission] ${e.getClass().name} with error: ${e.message} for value ${fields[7]}"
            }

            Landlord landlord = Landlord.findByAccountName(fields[1]);
            PropertyManager propertyManager = PropertyManager.findByFirstName('Rodney');

            Address address = new Address(
                    street: fields[11],
                    suburb: fields[12],
                    country: Country.get(1),
                    city: fields[14]
            )

            Map record = [
                    propertyType: propertyType,
                    name: fields[0],
                    chargeVat: chargeVat,
                    standNumber: fields[4] ?: '0000',
                    area: propertyArea,
                    commission: propertyCommission,
                    propertyCode: fields[8],
                    description: fields[9] ?: fields[0],
                    landlord: landlord,
                    propertyManager: propertyManager,
                    address: address
            ]

            Property property = new Property(record);

            if(!property.validate()) {

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << property.errors.toString()?.replaceAll("\n", "|");
                errorsFile << fields1.join("\t") + "\n";

                println(property.errors);
                property.discard();
            } else {


                property.maker     = userService.systemUser;
                property.makeDate  = new Date();
                property.checker   = userService.systemUser;
                property.checkDate = new Date();
                property.checkStatus = CheckStatus.APPROVED;
                property.dirtyStatus = CheckStatus.APPROVED;
                property.dirtMaker = userService.systemUser;

                //property.save(flush: true);
                //@todo: now add the rental units to this property

                log.info "loading the rentalUnits for the property = [${fields[0].trim()}]"
                loadRentalUnits(property);
                log.info "SUCCESSFULLY FINISHED loading the rentalUnits for the property = [${fields[0].trim()}]"
            }

            log.info "SUCCESSFULLY FINISHED loading the property details = [${fields[0].trim()}]"
        }
    }

    def loadRentalUnits(Property property) {
        //Read the sample.fix fixtures file for testing purposes.
        def rentalUnitsFilepath = "resources/fixtures/rental-units.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${rentalUnitsFilepath}");

        def counter = 0;

        fixturesResource.file.splitEachLine(";") { fields ->
            // if we are handling this property then continue.

            if( !property.name.trim().equalsIgnoreCase(fields[1]?.trim()) ) return;

            BigDecimal unitArea = 1.0;

            String areaValue = fields[4]?.trim();

            try{
                if(areaValue == null || areaValue.empty){
                    areaValue = '1.0';
                }

                unitArea = new BigDecimal(areaValue);
            } catch(NumberFormatException e) {
                log.error "error loading rentalUnit -> ${e.getClass().name} with error: ${e.message} for value #${areaValue}#"
                e.printStackTrace()
            }

            Map record = [
                    property: property,
                    unitReference: fields[0],
                    name: fields[2],
                    area: unitArea
            ];

            RentalUnit rentalUnit = new RentalUnit(record);

            if(!rentalUnit.validate()) {
                File errorsFile = new File(System.getProperty('user.home')+"/rental-units.${property.name.replaceAll("[,/]", "_")}.errors.txt")
                errorsFile.write("")

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << rentalUnit.errors.toString()?.replaceAll("\n", "| -->" + property.name);
                errorsFile << fields1.join("\t") + "\n";
                return;
            }

            property.addToRentalUnits(rentalUnit);
            counter++;
        }

        try{
           // property.discard()
            propertyService.save(property)
        } catch(ValidationException e) {
            log.error("error while trying to save property object [$property]")
            log.error e.message
        }

    }

    def loadRentalUnits() {
        //Read the sample.fix fixtures file for testing purposes.
        def rentalUnitsFilepath = "resources/fixtures/rental-units.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${rentalUnitsFilepath}");

        def counter = 0;
        Property propertyArg = null;

        fixturesResource.file.splitEachLine(";") { fields ->
            // if we are handling this propertyArg then continue.

            def units = RentalUnit.where {
                unitReference == fields[0]
                property.name == fields[1].trim()
            }.count()

            if(units > 0) {
                log.error("The rental-unit ${fields[1]} : ${fields[0]} already exists ");
                return;
            }

           propertyArg = Property.findByName(fields[1]?.trim());

            if(propertyArg == null) {
                log.error("The propertyArg ${fields[1]?.trim()} was not found");
                return;
            }

            BigDecimal unitArea = 1.0;

            String areaValue = fields[4]?.trim();

            try{
                if(areaValue == null || areaValue.empty){
                    areaValue = '1.0';
                }

                unitArea = new BigDecimal(areaValue);
            } catch(NumberFormatException e) {
                log.error "error loading rentalUnit -> ${e.getClass().name} with error: ${e.message} for value #${areaValue}#"
                e.printStackTrace()
            }

            Map record = [
                    property: propertyArg,
                    unitReference: fields[0],
                    name: fields[2],
                    area: unitArea
            ];

            RentalUnit rentalUnit = new RentalUnit(record);

            if(!rentalUnit.validate()) {
                File errorsFile = new File(System.getProperty('user.home')+"/rental-units.${propertyArg.name.replaceAll("[,/]", "_")}.errors.txt")
                errorsFile.write("")

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << rentalUnit.errors.toString()?.replaceAll("\n", "| -->" + propertyArg.name);
                errorsFile << fields1.join("\t") + "\n";
                return;
            }

            propertyArg.addToRentalUnits(rentalUnit);
            counter++;
        }

        try{
           // propertyArg.discard()
            propertyService.save(propertyArg)
        } catch(ValidationException e) {
            log.error("error while trying to save propertyArg object [$propertyArg]")
            log.error e.message
        }
    }

    def loadLeases(def leaseItemsErrorsFile) {
        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def propertiesFilepath = "resources/fixtures/leases.txt";

        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${propertiesFilepath}");

        File errorsFile = new File(System.getProperty('user.home')+"/leases.errors.txt")
        errorsFile.write("")

        def counter = 0;
        fixturesResource.file.splitEachLine(";") { fields ->
            log.info "loading the lease details = [${fields}]"
            RentalUnit rentalUnit = RentalUnit.findByUnitReference(fields[2]);

            if(rentalUnit == null) {
                log.error "rental unit with unit-reference [${fields[2]}] was not found for lease.leaseNumber = [${fields[1]}]"
                return;
            }

            rentalUnit.unitStatus = RentalUnitStatus.findByCode('occupied')
            Property property = rentalUnit.property;

            LeaseType leaseType = LeaseType.list()[0];
            LeaseStatus leaseStatus = LeaseStatus.findByCode('active');

            Tenant tenant = Tenant.findByBankAccountNumber(fields[0].trim());
            if(tenant == null) {
                log.error "[lease] the tenant [${fields[0].trim()}] was not found for the lease.leaseNumber  = [${fields[1]}]"
                return
            }

            Landlord landlord = property.landlord;

            Date leaseStartDate = new Date().parse('yyyy-MM-dd', fields[4]);
            Date leaseExpiryDate = new Date().parse('yyyy-MM-dd', fields[5]);
            BigDecimal leaseBalance = fields[8].trim() as BigDecimal
            Date lastReviewDate = new Date() - 30;
            Date nextReviewDate = new Date() + 30;

            Map record = [
                    landlord: landlord,
                    rentalUnit: rentalUnit,
                    lesseeNumber: tenant.accountNumber,
                    property: property,
                    signedDate: new Date(),
                    leaseStartDate: leaseStartDate,
                    leaseExpiryDate: leaseExpiryDate,
                    leaseStatus: leaseStatus,
                    leaseType: leaseType,
                    lastReviewDate: lastReviewDate,
                    nextReviewDate: nextReviewDate,
                    leaseCurrency: Currency.read('USD'),
                    invoicingCurrency: Currency.read('ZWL'),
                    tenant: tenant
                    //,leaseNumber: fields[4] //save this temporarily for use later with marrying to a leaseItem
            ]

            Lease lease = new Lease(record)
            SubAccount subAccount = SubAccount.findByAccountName("Rent", [readOnly: true]);

            if(!lease.validate()) {

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << property.errors.toString()?.replaceAll("\n", "|");
                errorsFile << fields1.join("\t") + "\n";

                System.err.println("ERRORS: " + lease.errors);

                lease.discard();
            } else if (fields[7] != null) {
                /// save the lease item

                Map leaseItemMap = [
                        startDate: lease.leaseStartDate,
                        endDate: lease.leaseExpiryDate,
                        subAccount: subAccount,
                        amount: fields[7].trim(),
                        lease: lease
                ]

                LeaseItem leaseItem = new LeaseItem(leaseItemMap);

                if( !leaseItem.validate() ) {
                    println(leaseItem.errors)
                    return;
                }

                lease.addToLeaseItems(leaseItem);

            }

            LeaseNumber leaseNumber = new LeaseNumber().save(flush: true)
            lease.leaseNumber = String.format("110%06d", leaseNumber.id);
            lease.save(flush: true);
            //////

            //now load the lease balance

            // generate the deposit invoice for the tenant
            TransactionType transactionType = TransactionType.get(8);
            TransactionBatchTmp batch = null;
            TransactionBatchTmp.withNewTransaction { TransactionStatus status ->
                batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
                batch.save(flush: true)
                status.flush()
            }

            def balanceParams = [
                rentalUnit: [id: rentalUnit.id],
                lease: [id: lease.id],
                //transactionType: [id: transactionType.id],
                entryNumber: null,
                transactionDate: new Date().parse("yyyy-MM-dd", '2023-02-28'),
                creditAccount: tenant.accountNumber,
                debitAccount: tenant.accountNumber,
                creditSubAccount: subAccount.accountNumber,
                amount: leaseBalance,
                transactionCurrency: fields[9].trim(),
                description: "Balance Migration for rental-unit: [${lease.rentalUnit.unitReference}]",
                transactionBatchTmp: [id: batch.id]
            ];

            if("USD" == fields[9].trim()) {
                log.error("We found a USD balances record here {}", fields);
                //System.err.println("found a USD: " + fields);
            }

            //log.error "processing: {}", balanceParams
            //now we create the transactions
            transactionTmpService.balance(balanceParams);

            //*/
            //now post the batch here.
            //post the transactionBatch.
            batch.refresh();
            log.info "posting the transaction batch with items-count: ${batch.transactionTmps}"
            TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(batch);
            log.info "finished with transaction-batch ::: [${transactionBatch.id}] "

            batch.autoCommitted = true;
            batch.postedBy = "system";
            batch.datePosted = new Date();
            batch.save(flush: true);

            log.info "SUCCESSFULLY FINISHED loading date for[lease=${fields[1].trim()}:bankReference=${tenant.bankAccountNumber}:tenant=${tenant.accountName}]"

            //@do another for the landlord
            loadLandlordBalances(balanceParams, lease);

            //now load the lease balance
            log.info "SUCCESSFULLY FINISHED loading the lease details = [${fields}]"
        }

        leaseService.processExpiredLeases();
    }

    def loadLeaseItems(Lease lease, String leaseNumber, def leaseItemsErrorsFile) {
        //log.info"start preparations to load the leaseItems for the leaseNumber=[$leaseNumber]"
        //Read the sample.fix fixtures file for testing purposes.
        def leaseItemsFilepath = "resources/fixtures/lease-items.txt";
        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${leaseItemsFilepath}");

        def counter = 0;
        fixturesResource.file.splitEachLine("\t") { fields ->
            counter++;
            if(leaseNumber == fields[0]?.trim() ) {
                if(fields[3]?.trim()?.empty) {
                   log.error ("skip processing the lease-item because amount is not set {${fields}");
                    return;
                }
            }//if the lease does not exist, we are not saving the lease item neither.
            else {
                return ;
            }

            //log.info "start loding the leaseItems for leaseNumber=[${leaseNumber}]"
            BigDecimal amount = 0.0;

            try{
                amount = new BigDecimal(fields[3])
            } catch(NumberFormatException e) {
                log.error "[leaseItem.amount] error loading leaseItem -> ${e.getClass().name} with error: ${e.message} for value ${fields[3]}"
                return
            }

            Date startDate = new Date().parse('yyyy-MM-dd', fields[1])
            Date endDate = new Date().parse('yyyy-MM-dd', fields[2])
            SubAccount subAccount = SubAccount.findByAccountName(fields[4].trim());

            if(subAccount == null) {
                log.error "sub account for [${fields[3]}] was not found";
                return;
            }

            Map record = [
                    startDate: startDate,
                    endDate: endDate,
                    subAccount: subAccount,
                    amount: amount,
                    lease: lease
            ]

            LeaseItem leaseItem = new LeaseItem(record);

            if( !leaseItem.validate() ) {

                List fields1 = []
                fields.each{
                    fields1 << it?.replace("\n", "|");
                }

                fields1 << leaseItem.errors.toString()?.replaceAll("\n", "|");
                leaseItemsErrorsFile << fields1.join("\t") + "\n";
                return;
            }

            lease.addToLeaseItems(leaseItem);
        }

        try{
            leaseService.save(lease)
        } catch(grails.validation.ValidationException e) {
            log.error("error while trying to save lease object leaseNumber= [$leaseNumber] -> ", e.message)
        }

    }

    @Transactional
    def loadTenantDeposits() {

        def processInvoice = false;

        //Read the sample.fix fixtures file for testing purposes.
        def fixturesDirPath = "resources/fixtures";
        def depositFilepath = "resources/fixtures/tenant-deposits.txt";
        Resource fixturesResource = grailsApplication.mainContext.getResource("classpath:${depositFilepath}");

        def counter = 0;
        File errorsFile = new File(System.getProperty('user.home')+"/tenant-deposit.errors.txt")

        // retrieve the deposit SubAccount
        def depositSubAccount = SubAccount.findByAccountNumber(MetasoftUtil.DEPOSIT_ACCOUNT_NUMBER);


        fixturesResource.file.splitEachLine(";") { fields ->
            counter++;

            String bankAccountNumber = fields[0]?.trim()
            String unitRef = fields[1]?.trim()
            BigDecimal amount = 0

            try {
                amount = fields[4]?.trim() as BigDecimal
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }

            Tenant tenantArg = Tenant.findByBankAccountNumber(bankAccountNumber)

            //get the tenant information
            Lease lease = Lease.where({
                tenant == tenantArg
                rentalUnit.unitReference == unitRef
            }).get();

            if(tenantArg == null) {
                log.error "[tenant] the tenant [${bankAccountNumber}] was not found for the tenant.accountName = [${bankAccountNumber}]"
                errorsFile <<"[tenant] the tenant [${bankAccountNumber}] was not found for the tenant.accountName = [${bankAccountNumber}]\n"
                return
            }

            if(lease == null) {
                log.error "no [lease] was found for the tenant [${bankAccountNumber}] and rentalUnit.unitReference = [${unitRef}]";
                errorsFile << "no [lease] was found for the tenant [${bankAccountNumber}] and rentalUnit.unitReference = [${unitRef}]\n"
                return
            }

            CurrencyService currencyService = (CurrencyService)grailsApplication.parentContext.getBean("currencyService")
            Currency zwlCurrency = currencyService.get("ZWL"); // Currency.get("ZWL");

            if(processInvoice == false) {

                // generate the deposit invoice for the tenant (Put each in each batch) 
                TransactionType transactionType = TransactionType.get(2); // invoice
                TransactionBatchTmp batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
                batch = transactionBatchTmpService.create(new TransactionBatchTmp(transactionType: transactionType));
                batch.save(flush: true)

                //retrieve the rentalUnit
                RentalUnit rentalUnit = lease.rentalUnit;
                def transactionParams = [
                        //rentalUnit: [id: rentalUnit.id],
                        lease: lease,
                        //transactionType: transactionType,
                        //entryNumber: null,
                        transactionDate:  new Date().parse("yyyy-MM-dd", "2022-08-31"),
                        creditAccount: tenantArg.accountNumber,
                        debitAccount: tenantArg.accountNumber,
                        subAccount: depositSubAccount,
                        amount: amount,
                        transactionCurrency: "ZWL",
                        //bankAccount: "100603",
                        description: "Deposit Mgt Invoice - ${rentalUnit.unitReference}",
                        transactionBatchTmp: batch
                ];
                InvoiceCommand depositInvoiceCommand = new InvoiceCommand(transactionParams)

                transactionTmpService.invoice(depositInvoiceCommand);

                //items to discard
                rentalUnit.discard();
                zwlCurrency.discard();
                lease.discard();
                tenantArg.discard();

                //*/
                //now post the batch here.
                //post the transactionBatch.
                batch.refresh();
                log.info "posting the transaction batch with items-count: ${batch.transactionTmps}"
                TransactionBatch transactionBatch = transactionBatchTmpService.postTransactionBatch(batch);
                log.info "finished with transaction-batch ::: [${transactionBatch.id}] ";
                
                batch.autoCommitted = true;
                batch.postedBy = "system";
                batch.datePosted = new Date();
                batch.save(flush: true);

                log.info "SUCCESSFULLY FINISHED deposit invoice loading the tenant details for [${bankAccountNumber}]"

            } else {

                //now we prepare the deposit receipt (security deposit)
                receiptBatchTmpService = (ReceiptBatchTmpService)grailsApplication.parentContext.getBean("receiptBatchTmpService")
                ReceiptBatchTmp depositReceiptBatchTmp = receiptBatchTmpService.createNewBatch();

                //prepare the receipt command object
                ReceiptCreateCommand receiptCreateCommand = new ReceiptCreateCommand()
                receiptCreateCommand.transactionReference = String.format("RCD2%05d", counter);
                receiptCreateCommand.narrative = "MGT://Deposit Rcpt for - ${rentalUnit.unitReference}"
                receiptCreateCommand.transactionDate = new Date().parse("yyyy-MM-dd", '2022-08-31')
                receiptCreateCommand.tenant = tenantArg;
                receiptCreateCommand.receiptAmount = amount;
                receiptCreateCommand.debitAccount = metasoft.property.core.GeneralLedger.findByAccountNumber("100603");
                receiptCreateCommand.transactionCurrency = zwlCurrency;
                receiptCreateCommand.receiptBatchTmp = depositReceiptBatchTmp;

                ReceiptItemTmp receiptItemTmp = new ReceiptItemTmp()
                receiptItemTmp.subAccount = depositSubAccount;
                receiptItemTmp.allocatedAmount = amount;
                receiptItemTmp.lease = lease
                receiptCreateCommand.receiptItemTmps.add(receiptItemTmp);

                receiptTmpService = (ReceiptTmpService)grailsApplication.parentContext.getBean("receiptTmpService")
                receiptTmpService.createReceipt(receiptCreateCommand);

                depositReceiptBatchTmp.refresh();
                ReceiptBatchCheck receiptBatchCheck = this.receiptBatchTmpService.postReceiptBatchRequest(depositReceiptBatchTmp)

                receiptBatchCheckService = (ReceiptBatchCheckService)grailsApplication.parentContext.getBean("receiptBatchCheckService")
                receiptBatchCheckService.approve(receiptBatchCheck);
                //now we prepare the deposit receipt (security deposit)

            }

        }

    }

}
