package metasoft.property

import grails.core.GrailsApplication
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.web.authentication.AjaxAwareAuthenticationEntryPoint
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.RedirectStrategy

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse;

@Slf4j
class MsAjaxAwareAuthenticationEntryPoint extends AjaxAwareAuthenticationEntryPoint{

    /** Dependency injection for the RedirectStrategy. */
    @Autowired
    RedirectStrategy redirectStrategy

    @Autowired
    GrailsApplication grailsApplication

    /**
     * @param loginFormUrl URL where the login page can be found. Should either be relative to the web-app context path
     * (include a leading {@code /}) or an absolute URL.
     */
    MsAjaxAwareAuthenticationEntryPoint(String loginFormUrl) {
        super(loginFormUrl)

        RedirectStrategy redirectStrategy = grailsApplication.mainContext.getBean(RedirectStrategy.class);

        println "the value of loginFormUrl- = [${SpringSecurityUtils.securityConfig.auth.loginFormUrl}]"
    }

    @Override
    void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException, ServletException {

        println "${request == null} - ${response == null} ${e == null} st [${redirectStrategy == null}]"

        if('application/json'.equalsIgnoreCase(request.getHeader("Content-Type"))
            || 'XMLHttpRequest'.equalsIgnoreCase(request.getHeader('X-Requested-With'))) {
            response.sendError HttpServletResponse.SC_UNAUTHORIZED
            return
        }

        super.commence(request, response, e);
    }
}
