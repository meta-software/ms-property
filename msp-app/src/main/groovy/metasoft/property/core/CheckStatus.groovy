package metasoft.property.core

enum CheckStatus {

    NEW,
    PENDING,
    APPROVED,
    REJECTED,
    CANCELLED,  //@todo: subject to change maybe [only maker can effect cancellation of an item]
    EDITING     //this mode dictates an approved object but there are applied changes to them.

}