package metasoft.property.core

import grails.validation.ValidationException
import org.springframework.validation.Errors

/**
 * Created by lebohof on 07/12/18.
 */
class MspValidationException extends ValidationException {

    Object _obj;

    MspValidationException(String msg, Errors errors, Object obj) {
        super(msg, errors);
        this._obj = obj;
    }
}
