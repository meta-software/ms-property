package metasoft.property.core

import grails.validation.Validateable
import org.grails.datastore.gorm.GormEntity

import javax.persistence.Transient

class MakerChecker implements Validateable {

    String status = 'pending';
    String comment;
    Date makeDate = new Date();
    Date checkDate;
    SecUser checker;
    SecUser maker;

    static mapWith = "none";

    static constraints = {
        status nullable: true, inList: ['pending', 'review', 'approved', 'rejected']
        comment nullable: true
        checker nullable: true , validator: { val, obj, errors ->

            if(val != null && val.id == obj.maker?.id) {
                errors.rejectValue('checker', 'makerChecker.checker.sameMaker', 'Same maker same checker not allowed.')
            }

        }
        maker nullable: true
        checkDate nullable: true
        makeDate nullable: true
    }

    static def listStatuses () {
        return ['pending',
                //'review',
                'approved',
                'rejected'];
    }

    @Transient
    Boolean isApproved() {
        return 'approved' == status;
    }

    @Transient
    Boolean isRejected() {
        return 'rejected' == status;
    }

    @Transient
    Boolean isPending() {
        return 'pending' == status;
    }

    @Transient
    Boolean isChecked() {
        return status != null && status != 'pending';
    }
}