package metasoft.property.core

import org.grails.core.exceptions.GrailsException

/**
 * Created by lebohof on 07/12/18.
 */
class InvalidBillingCycleException extends GrailsException {

    InvalidBillingCycleException(String msg) {
        super(msg)
    }
}
