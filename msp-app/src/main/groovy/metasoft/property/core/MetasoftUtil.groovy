package metasoft.property.core

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

import java.text.ParseException

@Slf4j
@CompileStatic
final class MetasoftUtil {
    
    public final static String SA_RENT_CHARGE   = '2';
    public final static String DEPOSIT_SUSPENSE_ACCOUNT_NUMBER = '12';
    public final static String DEPOSIT_ACCOUNT_NUMBER = '4';
    public final static String DEPOSIT_HELD_ACCOUNT_NUMBER = '13';

    //public final String SA_OPCOS = '4';

    static Date parseDate(String dateString, String format='yyyy-MM-dd') {

        if( dateString == null || dateString.isEmpty()) {
            return null;
        }

        Date dateValue = null;

        try{
            dateValue = new Date().parse(format, dateString);
        } catch(ParseException e) {
            log.warn("Invalid date string provided [{}] for the format [{}]", dateString, format);
            e.printStackTrace();
        }

        return dateValue;
    }

    static Map copyRequest(def args) {
        Map jsonRequest = new JsonSlurper().parseText(new JsonBuilder(args).toString() ) as Map;
        return jsonRequest;
    }

    static Date today() {
        Date today = new Date().clearTime()
        return today;
    }
}
