package metasoft.property.core

import grails.plugin.springsecurity.userdetails.GrailsUser
import org.springframework.security.core.GrantedAuthority

/**
 * Created by lebohof on 4/27/2018.
 */
class MsUserDetails extends GrailsUser {

    final Long userId;
    final String fullName;
    final String emailAddress;
    final String employeeId;
    final String userType;

    MsUserDetails(String username, String password, boolean enabled,
                  boolean accountNonExpired, boolean credentialsNonExpired,
                  boolean accountNonLocked,
                  Collection<GrantedAuthority> authorities,
                  long id, String fullName, String emailAddress, String employeeId, String userType) {
        super(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities, id)

        this.userId = id;
        this.fullName = fullName
        this.emailAddress = emailAddress;
        this.employeeId = employeeId;
        this.userType = userType;
    }


}
