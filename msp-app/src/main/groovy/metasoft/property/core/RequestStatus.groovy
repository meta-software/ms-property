package metasoft.property.core

enum RequestStatus {

    PENDING,
    SUCCESS,
    RUNNING,
    ERROR

}