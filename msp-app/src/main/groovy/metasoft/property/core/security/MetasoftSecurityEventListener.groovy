package metasoft.property.core.security

import grails.plugin.cookie.CookieService
import groovy.json.JsonBuilder
import groovy.transform.CompileStatic
import metasoft.property.core.LoginAttempt
import metasoft.property.core.MsUserDetails
import metasoft.property.core.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent
import org.springframework.security.authentication.event.AuthenticationSuccessEvent

@CompileStatic
class MetasoftSecurityEventListener
        implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    CookieService cookieService;

    @Autowired
    UserService userService;

    void onApplicationEvent(AuthenticationSuccessEvent event) {

        // handle the event, we want to write cookies in the user browser
        MsUserDetails userDetails = event.authentication.principal as MsUserDetails;
        cookieService.setCookie([name: 'userid', value: userDetails.userId+"", path: "/", httpOnly: false] );
        //cookieService.setCookie([name: 'username', value: userDetails.username, path: "/", httpOnly: false] );

        // do work to update the user database record
        if(userDetails.username != 'flexcube') {
            userService.updateLoginData(userDetails.userId);
        }

        LoginAttempt.withNewTransaction {
            LoginAttempt loginAttempt = new LoginAttempt()
            loginAttempt.actor = userDetails.username;
            loginAttempt.setIpAddress("0.0.0.0")
            loginAttempt.setChannel("WEB")
            loginAttempt.attemptStatus = "SUCCESS";
            loginAttempt.setDescription("login successful")
            loginAttempt.save();
        }

    }

}
