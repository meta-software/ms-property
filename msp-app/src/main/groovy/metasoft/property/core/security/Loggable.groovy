package metasoft.property.core.security

import org.grails.datastore.gorm.GormEntity

import javax.persistence.Transient

/**
 * This trait turns on loggable behaviour of a given domain object.
 *
 * @param <D>
 */
trait Loggable<D> implements GormEntity<D> {
    
    static boolean loggable = true;

    /**
     * @return returns the id of the object by default, can override to return a natural key
     */
    @Transient
    String getLogEntityId() {
        if (this instanceof GormEntity) {
            return convertLoggedPropertyToString("id", ((GormEntity)this).ident())
        }
        if (this.respondsTo("getId")) {
            return convertLoggedPropertyToString("id", this.invokeMethod("getId", null))
        }

        throw new IllegalStateException("Could not determine the Id for ${getClass().name}, override getLogEntityId() to specify")
    }

    /**
     * Domain classes can override to apply special formatting on a per-property basis
     *
     * @param propertyName
     * @param value
     * @return
     */
    String convertLoggedPropertyToString(String propertyName, Object value) {
        if (value instanceof Enum) {
            return ((Enum)value).name()
        }
        if (value instanceof Loggable) {
            return "[id:${((Loggable)value).logEntityId}]$value"
        }
        if (value instanceof GormEntity) {
            return "[id:${((GormEntity)value).ident()}]$value"
        }
        if (value instanceof Collection) {
            if (logAssociatedIds) {
                return ((Collection)value).collect {
                    convertLoggedPropertyToString(propertyName, it)
                }.join(", ")
            }
            else {
                return "N/A"
            }
        }

        value?.toString()
    }

    /**
     * Enable logging of associated id changes in the format: "[id:<objId>]objDetails".
     */
    @Transient
    boolean isLogAssociatedIds() {
        return false;
    }
}
