package metasoft.property.core.security

import grails.compiler.GrailsCompileStatic
import grails.plugin.cookie.CookieService
import grails.util.GrailsNameUtils
import grails.util.GrailsUtil
import groovy.util.logging.Slf4j
import metasoft.property.core.LoginAttempt
import metasoft.property.core.SecUser
import metasoft.property.core.UserService
import org.grails.web.util.WebUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationListener
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent
import org.springframework.security.authentication.event.AuthenticationFailureCredentialsExpiredEvent

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession

@Slf4j
@GrailsCompileStatic
class MetasoftSecurityEventAuthenticationFailureListener
        implements ApplicationListener<ApplicationEvent> {

    @Autowired
    CookieService cookieService;

    @Autowired
    UserService userService;

    HttpServletRequest getRequest() {
        return WebUtils.retrieveGrailsWebRequest().currentRequest;
    }

    HttpSession getSession() {
        return this.request.session;;
    }

    void onApplicationEvent(ApplicationEvent event) {

        if(event instanceof AuthenticationFailureBadCredentialsEvent) {
            handleAuthenticationFailureBadCredentials((AuthenticationFailureBadCredentialsEvent)event)
            logLoginAttempt(event);
        } else if (event instanceof AuthenticationFailureCredentialsExpiredEvent) {
            handleAuthenticationFailureCredentialsExpired((AuthenticationFailureCredentialsExpiredEvent)event)
            logLoginAttempt(event);
        }

    }

    void logLoginAttempt(ApplicationEvent event) {

        LoginAttempt.withNewSession {
            String username = ((AbstractAuthenticationFailureEvent)event).authentication.principal as String;

            LoginAttempt loginAttempt = new LoginAttempt()
            loginAttempt.actor = username;
            loginAttempt.attemptStatus = "FAILURE";
            loginAttempt.setIpAddress("0.0.0.0")
            loginAttempt.setChannel("web")
            loginAttempt.setDescription("Failed Login Attempt :// ${GrailsNameUtils.getShortName(event.class)}")

            loginAttempt.save();
        }

    }

    //AuthenticationFailureBadCredentialsEvent
    void handleAuthenticationFailureBadCredentials(AuthenticationFailureBadCredentialsEvent event) {

        String username = event.authentication.principal as String;

        SecUser.withNewSession {
            SecUser secUser = SecUser.findByUsername(username);

            if(secUser && secUser.loginAttemptCount > 2 && !secUser.accountLocked) {
                secUser.accountLocked = true;
                secUser.lockReason = "Too many login attempts."
                secUser.save(flush: true);

                // also use cookies to lock out the browser for about an hour
                cookieService.setCookie([name: 'BLDDIDSESSID', value: '1', path: "/", httpOnly: false]);
            }

            if(secUser && secUser.loginAttemptCount < 3) {
                secUser.loginAttemptCount += 1;
                secUser.save(flush: true);
            }
        }

    }

    //AuthenticationFailureBadCredentialsEvent
    void handleAuthenticationFailureCredentialsExpired(AuthenticationFailureCredentialsExpiredEvent event) {
        // retrieve the username from the httpRequest object and set it into the session variable for later use.

        String username = event.authentication.principal;
        this.session.setAttribute('LAST_LOGIN_USERNAME', username);
    }
}

