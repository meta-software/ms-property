package metasoft.property.core

enum UserAction {
    CREATE, DELETE, UPDATE, TERMINATE
}
