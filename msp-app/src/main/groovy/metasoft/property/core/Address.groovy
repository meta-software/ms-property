package metasoft.property.core

import grails.validation.Validateable

class Address implements Validateable {

    String street;
    String suburb;
    String city;
    Country country;

    static constraints = {
        street nullable: true
        suburb nullable: true
        city nullable: true
        country nullable: true
    }

}