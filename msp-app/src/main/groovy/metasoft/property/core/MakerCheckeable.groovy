package metasoft.property.core;

import org.codehaus.groovy.transform.trait.Traits;
import org.grails.datastore.gorm.GormEntity;
import javax.persistence.Transient;

trait MakerCheckeable<D> extends GormEntity<D> {

    //MakerChecker makerChecker;

    @Traits.Implemented
    abstract MakerChecker getMakerChecker();

    @Traits.Implemented
    abstract void setMakerChecker(MakerChecker makerChecker);

    @Transient
    Boolean isApproved() {
        return 'approved' == makerChecker?.status;
    }

    @Transient
    Boolean isRejected() {
        return 'rejected' == makerChecker?.status;
    }

    @Transient
    Boolean isPending() {
        return 'pending' == makerChecker?.status;
    }

    @Transient
    Boolean isChecked() {
        return makerChecker?.status != null && makerChecker?.status != 'pending';
    }

    @Transient
    def getMaker() {
        return makerChecker?.maker;
    }

    @Transient
    def getChecker() {
        return makerChecker?.checker;
    }

    @Transient
    Date getCheckDate() {
        return makerChecker?.checkDate;
    }
    @Transient
    Date getMakeDate() {
        return makerChecker?.makeDate;
    }
}
