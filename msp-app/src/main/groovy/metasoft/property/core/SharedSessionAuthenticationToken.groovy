package metasoft.property.core

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.CredentialsContainer
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

import java.security.Principal

class SharedSessionAuthenticationToken extends AbstractAuthenticationToken {

    Object principal
    Object credentials
    Object sharedToken

    public SharedSessionAuthenticationToken(String sharedToken) {
        super(null);

        this.sharedToken = sharedToken;
        setAuthenticated(false);
    }

    /**
     * This constructor should only be used by <code>AuthenticationManager</code> or
     * <code>AuthenticationProvider</code> implementations that are satisfied with
     * producing a trusted (i.e. {@link #isAuthenticated()} = <code>true</code>)
     * authentication token.
     *
     * @param principal
     * @param credentials
     * @param authorities
     */
    public SharedSessionAuthenticationToken(Object principal, Object credentials,
                                            Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        super.setAuthenticated(true); // must use super, as we override
    }

    @Override
    String getName() {
        if (this.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) this.getPrincipal()).getUsername();
        }

        if (getPrincipal() instanceof Principal) {
            return ((Principal) getPrincipal()).getName();
        }

        return (this.getPrincipal() == null) ? "" : this.getPrincipal().toString();
    }

    @Override
    void eraseCredentials() {
        if (this.credentials instanceof CredentialsContainer) {
            ((CredentialsContainer)this.credentials).eraseCredentials();
        }
    }
}