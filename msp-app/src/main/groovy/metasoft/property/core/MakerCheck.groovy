package metasoft.property.core

import org.grails.datastore.gorm.GormEntity
import javax.persistence.Transient

trait MakerCheck<D> implements Loggable<D>, GormEntity<D> {

    String actionName;
    String entityName;
    String entityUrl;
    String jsonData;

    CheckStatus checkStatus = CheckStatus.PENDING;
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;
    String checkComment;

    abstract MakerCheckable getEntity();

    @Transient
    Boolean isApproved() {
        return checkStatus == CheckStatus.APPROVED
    }

    @Transient
    Boolean isRejected() {
        return checkStatus == CheckStatus.REJECTED
    }

    @Transient
    Boolean isPending() {
        return checkStatus == CheckStatus.PENDING
    }

    @Transient
    Boolean isChecked() {
        return ( checkStatus != null && !(checkStatus in [CheckStatus.PENDING, CheckStatus.EDITING]) )
    }

}
