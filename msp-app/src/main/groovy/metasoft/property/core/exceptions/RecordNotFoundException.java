package metasoft.property.core.exceptions;

import java.io.Serializable;

public class RecordNotFoundException extends RuntimeException {

    static final long serialVersionUID = 971907L;

    public RecordNotFoundException() {
        this("Requested record was not found");
    }

    public RecordNotFoundException(String msg) {
        super(msg);
    }

    static public void throwException(Serializable id) {
        throw new RecordNotFoundException();
    }

    static public void throwException(String message) {
        throw new RecordNotFoundException(message);
    }
}
