package metasoft.property.core.exceptions;

import grails.validation.ValidationException;
import org.springframework.validation.Errors;

public class EmptyTransactionBatchException extends ValidationException {

    public EmptyTransactionBatchException(String msg, Errors e) {
        super(msg, e);
    }
}
