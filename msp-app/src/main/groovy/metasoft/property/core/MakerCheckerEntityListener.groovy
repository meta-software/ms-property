package metasoft.property.core

import grails.events.annotation.gorm.Listener
import grails.plugin.springsecurity.SpringSecurityService
import groovy.transform.CompileStatic
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent
import org.grails.datastore.mapping.engine.event.PreInsertEvent
import org.springframework.beans.factory.annotation.Autowired

@CompileStatic
class MakerCheckerEntityListener {

    @Autowired
    SpringSecurityService springSecurityService

    @Listener(MakerCheckeable)
    void onPreInsertEvent(PreInsertEvent event) {
        prepareFieldsForEvent(event)
    }

    private void prepareFieldsForEvent(AbstractPersistenceEvent event) {
        if (event.entityObject instanceof MakerCheckeable) {
            MakerCheckeable entity = event.entityObject as MakerCheckeable;
            if(event instanceof PreInsertEvent) {
                prepareFields(entity);
            }
        }
    }

    private void prepareFields(MakerCheckeable entity) {
        entity.makerChecker = new MakerChecker();
        entity.makerChecker.status   = 'pending';
        entity.makerChecker.makeDate = new Date();
        entity.makerChecker.maker    = springSecurityService?.currentUser as SecUser;
        //println("if we're getting here, then this is some bull****")
    }
}
