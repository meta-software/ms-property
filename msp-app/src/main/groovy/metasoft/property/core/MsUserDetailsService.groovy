package metasoft.property.core

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService
import grails.plugin.springsecurity.userdetails.NoStackUsernameNotFoundException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException

/**
 * Created by lebohof on 4/28/2018.
 */
class MsUserDetailsService implements GrailsUserDetailsService {

/**
 * Some Spring Security classes (e.g. RoleHierarchyVoter) expect at least
 * one role, so we give a user with no granted roles this one which gets
 * past that restriction but doesn't grant anything.
 */
    static final List NO_ROLES = [new SimpleGrantedAuthority(SpringSecurityUtils.NO_ROLE)]

    UserDetails loadUserByUsername(String username, boolean loadRoles)
            throws UsernameNotFoundException {
        return loadUserByUsername(username)
    }

    @Transactional(readOnly = true, noRollbackFor = [IllegalArgumentException, UsernameNotFoundException])
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        SecUser user = SecUser.findByUsername(username)
        if (!user) throw new NoStackUsernameNotFoundException()

        def roles = user.authorities ;

        // or if you are using role groups:
        // def roles = user.authorities.collect { it.authorities }.flatten().unique()

        // in the event you are using the basic authorities
        /*
        def authorities = roles.collect {
            new SimpleGrantedAuthority(it.authority)
        }
        */

        def authorities = getAuthorities(roles);

        return new MsUserDetails(user.username, user.password, user.enabled,
                !user.accountExpired, !user.passwordExpired,
                !user.accountLocked, authorities ?: NO_ROLES, user.id,
                user.firstName + " " + user.lastName, user.emailAddress, user.employeeId, user.userType.authority)
    }

    private List<GrantedAuthority> getAuthorities(Set roles) {

        return getGrantedAuthorities( getPermissions(roles) );
    }

    private List<String> getPermissions(Set<SecRole> roles) {

        List<String> permissions = [];

        roles.each { SecRole role ->
                // make sure that the authority is also available.
                permissions << role.authority;

            role?.permissions?.each { SecPermission permission ->
                permissions << "ROLE_PERM_" + permission.name;
            }
        }

        return permissions;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> permissions) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : permissions) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
