package metasoft.property.core

import org.springframework.context.MessageSource
import org.springframework.context.MessageSourceAware
import org.springframework.context.support.MessageSourceAccessor
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.InternalAuthenticationServiceException
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.SpringSecurityMessageSource
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.util.Assert

/**
 * Part of the security filter chain for math-based authentication.
 */
class SharedSessionAuthenticationProvider implements AuthenticationProvider, MessageSourceAware {

    SharedSessionAuthenticatorService sharedSessionAuthenticatorService
    MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor()
    UserDetailsService userDetailsService;

    private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

    /**
     * Authenticates the application user and returns a populated
     * Authentication object if successful.
     * @param auth
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication auth)
            throws AuthenticationException {
        Assert.isInstanceOf(SharedSessionAuthenticationToken.class, auth,
                messages.getMessage(
                        "SharedSessionAuthenticationProvider.onlySupports",
                        "Only SharedSessionAuthenticationToken is supported"));

        // Determine the sharedToken
        SharedSessionAuthenticationToken authenticationToken = (SharedSessionAuthenticationToken) auth;
        String sharedToken = authenticationToken.sharedToken;

        UserDetails userDetails = null;

        try {
            UserSessionToken userSessionToken = sharedSessionAuthenticatorService.validateToken(sharedToken);

            // check if the userSessionToken is valid
            if (!userSessionToken) {

                throw new AuthenticationServiceException(messages.getMessage(
                        "SharedSessionAuthenticationProvider.invalidToken",
                        "Invalid authentication token"));
            }

            userDetails = retrieveUser(userSessionToken, authenticationToken)

        } catch (AuthenticationException authenticationException) {
            throw authenticationException
        }

        return createSuccessAuthentication(userDetails, authenticationToken, userDetails);
    }

    protected Authentication createSuccessAuthentication(Object principal,
                                                         Authentication authentication, UserDetails user) {
        // Ensure we return the original credentials the user supplied,
        // so subsequent attempts are successful even with encoded passwords.
        // Also ensure we return the original getDetails(), so that future
        // authentication events after cache expiry contain the details
        SharedSessionAuthenticationToken result = new SharedSessionAuthenticationToken(
                principal,
                authentication.getCredentials(),
                authoritiesMapper.mapAuthorities(user?.getAuthorities()));

        result.setDetails(authentication.getDetails());
        return result;
    }

    protected final UserDetails retrieveUser(UserSessionToken userSessionToken, Authentication authentication)
            throws AuthenticationException {
        UserDetails loadedUser;

        // retrive the username
        String username = userSessionToken.secUser.username;

        try {
            loadedUser = this.getUserDetailsService().loadUserByUsername(username);
        }
        catch (UsernameNotFoundException notFound) {
            throw notFound;
        }
        catch (Exception repositoryProblem) {
            throw new InternalAuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
        }

        if (loadedUser == null) {
            throw new InternalAuthenticationServiceException("UserDetailsService returned null, which is an interface contract violation");
        }
        return loadedUser;
    }

    /**
     * Tests whether this provider supports the Authentication
     * type being passed in.
     * @param authentication
     * @return
     */
    @Override
    boolean supports(Class<? extends Object> authentication) {
        return SharedSessionAuthenticationToken.class.isAssignableFrom(authentication)
    }

    @Override
    void setMessageSource(MessageSource messageSource) {
        this.messages = new MessageSourceAccessor(messageSource)
    }

}