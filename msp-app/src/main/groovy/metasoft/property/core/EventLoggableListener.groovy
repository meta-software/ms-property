package metasoft.property.core

import static grails.async.Promises.task
import grails.async.Promise
import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent
import org.grails.datastore.mapping.engine.event.PostDeleteEvent
import org.grails.datastore.mapping.engine.event.PostInsertEvent
import org.grails.datastore.mapping.engine.event.PreUpdateEvent

import grails.events.annotation.Subscriber
import grails.events.annotation.gorm.Listener
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
@CompileStatic
class EventLoggableListener {

    @Autowired
    EventLoggingService eventLoggingService;

    @Listener
    void afterInsert(PostInsertEvent event) {
        if(event.entityObject instanceof Loggable) {
            dispatchLogEvent(event, "INSERT", "INSERT");
        }
    }

    @Listener(Loggable.class)
    void  beforeUpdate(PreUpdateEvent event) {
        if(event.entityObject instanceof Loggable) {
            dispatchLogEvent(event, "UPDATE", "UPDATE");
        }
    }

    @Subscriber
    void afterDelete(PostDeleteEvent event) {
        if(event.entityObject instanceof Loggable) {
            dispatchLogEvent(event, "DELETE", "DELETE");
        }
    }

    private dispatchLogEvent(AbstractPersistenceEvent event, String eventName, String propertyName) {

        Promise p = task {
            EventLogging.withNewSession {
                eventLoggingService.logEvent(event, eventName, propertyName);
            }
        }

        p.onComplete { success ->
            log.info("successfully logged the event-logging {} for class {} entity {}", event, event.entityObject.class.canonicalName, event.entityObject);
        }

        p.onError { err ->
            log.error("failed to log the event [{}] for entity [{}] with error : {}", event, event.entityObject, err);
        }

    }

}
