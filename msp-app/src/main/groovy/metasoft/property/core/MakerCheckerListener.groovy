package metasoft.property.core

import grails.events.annotation.gorm.Listener
import grails.plugin.springsecurity.SpringSecurityService
import groovy.transform.CompileStatic
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent
import org.grails.datastore.mapping.engine.event.PreInsertEvent
import org.springframework.beans.factory.annotation.Autowired

@CompileStatic
class MakerCheckerListener {

    @Autowired
    SpringSecurityService springSecurityService

    @Listener(MakerCheckable)
    void onPreInsertEvent(PreInsertEvent event) {
        prepareFieldsForEvent(event)
    }

    private void prepareFieldsForEvent(AbstractPersistenceEvent event) {
        if (event.entityObject instanceof MakerCheckable) {
            MakerCheckable entity = event.entityObject as MakerCheckable;
            if(event instanceof PreInsertEvent) {
                prepareFields(entity);
            }
        }
    }

    private void prepareFields(MakerCheckable entity) {
        entity.checkStatus = entity.checkStatus ?: CheckStatus.PENDING;
        entity.makeDate = entity.makeDate ?: new Date();
        entity.maker    = entity.maker ?: springSecurityService?.currentUser as SecUser;
    }
}
