package metasoft.property.core

import grails.core.GrailsApplication
import grails.events.annotation.gorm.Listener
import grails.plugin.springsecurity.SpringSecurityService
import groovy.time.TimeCategory
import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent
import org.grails.datastore.mapping.engine.event.PreInsertEvent
import org.grails.datastore.mapping.engine.event.PreUpdateEvent
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
@CompileStatic
class LeaseEntityListener {

    @Autowired
    SpringSecurityService springSecurityService;

    @Autowired
    GrailsApplication grailsApplication;

    @Listener(Lease)
    void onPreInsertEvent(PreInsertEvent event) {
        updateReviewDate(event)
    }

    @Listener(Lease)
    void onPreUpdateEvent(PreUpdateEvent event) {
        updateReviewDate(event)
    }

    @CompileStatic(value = TypeCheckingMode.SKIP)
    private void updateReviewDate(AbstractPersistenceEvent event) {

        if (event.entityObject instanceof Lease) {
            Lease lease = event.entityObject as Lease;

            if (lease.lastReviewDate && ((event instanceof  PreInsertEvent) || (event instanceof PreUpdateEvent && lease.isDirty('lastReviewDate')))) {
                // prepare the next nextReviewDate value
                Integer reviewPeriod = (Integer)grailsApplication.config.getProperty("metasoft.app.lease.reviewPeriod", Integer.class, 3); // default to 3 months if nothing is configured

                //log.info "the reviewPeriod = ${reviewPeriod}"

                use(TimeCategory) {
                    Date nextReviewDate = lease.lastReviewDate + reviewPeriod.months;

                    //The next review date should not be after the expiry date of the lease.
                    if(nextReviewDate < lease.leaseExpiryDate) {
                        event.getEntityAccess().setProperty('nextReviewDate', nextReviewDate);
                    }
                }

            }
        }
    }

}
