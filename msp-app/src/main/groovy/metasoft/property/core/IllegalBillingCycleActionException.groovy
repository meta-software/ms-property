package metasoft.property.core

import grails.validation.ValidationException
import org.springframework.validation.Errors

/**
 * Created by lebohof on 07/12/18.
 */
class IllegalBillingCycleActionException extends ValidationException {

    IllegalBillingCycleActionException(String msg, Errors errors) {
        super(msg, errors)
    }
}
