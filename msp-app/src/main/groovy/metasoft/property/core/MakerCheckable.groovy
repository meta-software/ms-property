package metasoft.property.core

import javax.persistence.Transient

trait MakerCheckable<D> {

    CheckStatus checkStatus = CheckStatus.PENDING;

    // This follows to see if the the entity is in dirty state after successful approval before,
    // this field only takes on PENDING or APPROVED
    CheckStatus dirtyStatus = CheckStatus.PENDING;
    SecUser dirtMaker;
    SecUser maker;
    Date makeDate;
    SecUser checker;
    Date checkDate;

    @Transient
    Boolean isApproved() {
        return checkStatus == CheckStatus.APPROVED
    }

    @Transient
    Boolean isRecordDirty() {
        return dirtyStatus in [CheckStatus.PENDING, CheckStatus.EDITING]
    }

    @Transient
    Boolean isRejected() {
        return checkStatus == CheckStatus.REJECTED
    }

    @Transient
    Boolean isPending() {
        return checkStatus == CheckStatus.PENDING
    }

    @Transient
    Boolean isChecked() {
        return checkStatus != null && checkStatus != CheckStatus.PENDING
    }

    //abstract MakerCheck<?> getCheckEntity();

}
